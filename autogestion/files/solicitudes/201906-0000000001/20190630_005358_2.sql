-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 28-06-2019 a las 09:56:12
-- Versión del servidor: 10.1.40-MariaDB-0ubuntu0.18.04.1
-- Versión de PHP: 7.2.19-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `rapidpago_pedidos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividad_comercial`
--

CREATE TABLE `actividad_comercial` (
  `acome_codig` int(11) NOT NULL,
  `acome_descr` varchar(100) NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `acome_fcrea` date NOT NULL,
  `acome_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `actividad_comercial`
--

INSERT INTO `actividad_comercial` (`acome_codig`, `acome_descr`, `usuar_codig`, `acome_fcrea`, `acome_hcrea`) VALUES
(1, 'ACTIVIDAD 1', 3, '2019-06-24', '20:57:07'),
(2, 'ACTIVIDAD 2', 3, '2019-06-24', '20:57:07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banco`
--

CREATE TABLE `banco` (
  `banco_codig` int(11) NOT NULL,
  `banco_descr` varchar(200) DEFAULT NULL,
  `banco_value` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `banco`
--

INSERT INTO `banco` (`banco_codig`, `banco_descr`, `banco_value`) VALUES
(1, 'BANESCO BANCO UNIVERSAL S.A.C.A.', '0134'),
(2, 'BANCO PROVINCIAL, S.A. BANCO UNIVERSAL.', '0108'),
(35, 'BANCO CENTRAL DE VENEZUELA.', '0001'),
(36, 'BANCO INDUSTRIAL DE VENEZUELA, C.A. BANCO UNI', '0003'),
(37, 'BANCO DE VENEZUELA S.A.C.A. BANCO UNIVERSAL.', '0102'),
(38, 'VENEZOLANO DE CRÉDITO, S.A. BANCO UNIVERSAL.', '0104'),
(39, 'BANCO MERCANTIL, C.A S.A.C.A. BANCO UNIVERSAL', '0105'),
(40, 'BANCARIBE C.A. BANCO UNIVERSAL.', '0114'),
(41, 'BANCO EXTERIOR C.A. BANCO UNIVERSAL.', '0115'),
(42, 'BANCO OCCIDENTAL DE DESCUENTO, BANCO UNIVERSA', '0116'),
(43, 'BANCO CARONÍ C.A. BANCO UNIVERSAL.', '0128'),
(44, 'BANCO SOFITASA BANCO UNIVERSAL', '0137'),
(45, 'BANCO PLAZA BANCO UNIVERSAL', '0138'),
(46, 'BANCO DE LA GENTE EMPRENDEDORA C.A.', '0146'),
(47, 'BANCO DEL PUEBLO SOBERANO, C.A. BANCO DE DESA', '0149'),
(48, 'BFC BANCO FONDO COMÚN C.A BANCO UNIVERSAL', '0151'),
(49, '100% BANCO, BANCO UNIVERSAL C.A.', '0156'),
(50, 'DELSUR BANCO UNIVERSAL, C.A.', '0157'),
(51, 'BANCO DEL TESORO, C.A. BANCO UNIVERSAL', '0163'),
(52, 'BANCO AGRÍCOLA DE VENEZUELA, C.A. BANCO UNIVE', '0166'),
(53, 'BANCRECER, S.A. BANCO MICROFINANCIERO', '0168'),
(54, 'MI BANCO BANCO MICROFINANCIERO C.A.', '0169'),
(55, 'BANCO ACTIVO, C.A. BANCO UNIVERSAL', '0171'),
(56, 'BANCAMIGA BANCO MICROFINANCIERO C.A.', '0172'),
(57, 'BANCO INTERNACIONAL DE DESARROLLO, C.A. BANCO', '0173'),
(58, 'BANPLUS BANCO UNIVERSAL, C.A.', '0174'),
(59, 'BANCO BICENTENARIO BANCO UNIVERSAL C.A.', '0175'),
(60, 'BANCO ESPIRITO SANTO, S.A. SUCURSAL VENEZUELA', '0176'),
(61, 'BANCO DE LA FUERZA ARMADA NACIONAL BOLIVARIAN', '0177'),
(62, 'CITIBANK N.A.', '0190'),
(63, 'BANCO NACIONAL DE CRÉDITO, C.A. BANCO UNIVERS', '0191');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banco_tipo_cuenta`
--

CREATE TABLE `banco_tipo_cuenta` (
  `tcuen_codig` int(11) NOT NULL,
  `tcuen_descr` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `banco_tipo_cuenta`
--

INSERT INTO `banco_tipo_cuenta` (`tcuen_codig`, `tcuen_descr`) VALUES
(1, 'CORRIENTE'),
(2, 'AHORRO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudades`
--

CREATE TABLE `ciudades` (
  `ciuda_codig` int(11) NOT NULL,
  `estad_codig` int(11) NOT NULL,
  `paise_codig` int(11) NOT NULL,
  `ciuda_descr` text NOT NULL,
  `ciuda_capit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ciudades`
--

INSERT INTO `ciudades` (`ciuda_codig`, `estad_codig`, `paise_codig`, `ciuda_descr`, `ciuda_capit`) VALUES
(1, 1, 232, 'MAROA', 0),
(2, 1, 232, 'PUERTO AYACUCHO', 1),
(3, 1, 232, 'SAN FERNANDO DE ATABAPO', 0),
(4, 2, 232, 'ANACO', 0),
(5, 2, 232, 'ARAGUA DE BARCELONA', 0),
(6, 2, 232, 'BARCELONA', 1),
(7, 2, 232, 'BOCA DE UCHIRE', 0),
(8, 2, 232, 'CANTAURA', 0),
(9, 2, 232, 'CLARINES', 0),
(10, 2, 232, 'EL CHAPARRO', 0),
(11, 2, 232, 'EL PAO ANZOÁTEGUI', 0),
(12, 2, 232, 'EL TIGRE', 0),
(13, 2, 232, 'EL TIGRITO', 0),
(14, 2, 232, 'GUANAPE', 0),
(15, 2, 232, 'GUANTA', 0),
(16, 2, 232, 'LECHERÍA', 0),
(17, 2, 232, 'ONOTO', 0),
(18, 2, 232, 'PARIAGUÁN', 0),
(19, 2, 232, 'PÍRITU', 0),
(20, 2, 232, 'PUERTO LA CRUZ', 0),
(21, 2, 232, 'PUERTO PÍRITU', 0),
(22, 2, 232, 'SABANA DE UCHIRE', 0),
(23, 2, 232, 'SAN MATEO ANZOÁTEGUI', 0),
(24, 2, 232, 'SAN PABLO ANZOÁTEGUI', 0),
(25, 2, 232, 'SAN TOMÉ', 0),
(26, 2, 232, 'SANTA ANA DE ANZOÁTEGUI', 0),
(27, 2, 232, 'SANTA FE ANZOÁTEGUI', 0),
(28, 2, 232, 'SANTA ROSA', 0),
(29, 2, 232, 'SOLEDAD', 0),
(30, 2, 232, 'URICA', 0),
(31, 2, 232, 'VALLE DE GUANAPE', 0),
(43, 3, 232, 'ACHAGUAS', 0),
(44, 3, 232, 'BIRUACA', 0),
(45, 3, 232, 'BRUZUAL', 0),
(46, 3, 232, 'EL AMPARO', 0),
(47, 3, 232, 'EL NULA', 0),
(48, 3, 232, 'ELORZA', 0),
(49, 3, 232, 'GUASDUALITO', 0),
(50, 3, 232, 'MANTECAL', 0),
(51, 3, 232, 'PUERTO PÁEZ', 0),
(52, 3, 232, 'SAN FERNANDO DE APURE', 1),
(53, 3, 232, 'SAN JUAN DE PAYARA', 0),
(54, 4, 232, 'BARBACOAS', 0),
(55, 4, 232, 'CAGUA', 0),
(56, 4, 232, 'CAMATAGUA', 0),
(58, 4, 232, 'CHORONÍ', 0),
(59, 4, 232, 'COLONIA TOVAR', 0),
(60, 4, 232, 'EL CONSEJO', 0),
(61, 4, 232, 'LA VICTORIA', 0),
(62, 4, 232, 'LAS TEJERÍAS', 0),
(63, 4, 232, 'MAGDALENO', 0),
(64, 4, 232, 'MARACAY', 1),
(65, 4, 232, 'OCUMARE DE LA COSTA', 0),
(66, 4, 232, 'PALO NEGRO', 0),
(67, 4, 232, 'SAN CASIMIRO', 0),
(68, 4, 232, 'SAN MATEO', 0),
(69, 4, 232, 'SAN SEBASTIÁN', 0),
(70, 4, 232, 'SANTA CRUZ DE ARAGUA', 0),
(71, 4, 232, 'TOCORÓN', 0),
(72, 4, 232, 'TURMERO', 0),
(73, 4, 232, 'VILLA DE CURA', 0),
(74, 4, 232, 'ZUATA', 0),
(75, 5, 232, 'BARINAS', 1),
(76, 5, 232, 'BARINITAS', 0),
(77, 5, 232, 'BARRANCAS', 0),
(78, 5, 232, 'CALDERAS', 0),
(79, 5, 232, 'CAPITANEJO', 0),
(80, 5, 232, 'CIUDAD BOLIVIA', 0),
(81, 5, 232, 'EL CANTÓN', 0),
(82, 5, 232, 'LAS VEGUITAS', 0),
(83, 5, 232, 'LIBERTAD DE BARINAS', 0),
(84, 5, 232, 'SABANETA', 0),
(85, 5, 232, 'SANTA BÁRBARA DE BARINAS', 0),
(86, 5, 232, 'SOCOPÓ', 0),
(87, 6, 232, 'CAICARA DEL ORINOCO', 0),
(88, 6, 232, 'CANAIMA', 0),
(89, 6, 232, 'CIUDAD BOLÍVAR', 1),
(90, 6, 232, 'CIUDAD PIAR', 0),
(91, 6, 232, 'EL CALLAO', 0),
(92, 6, 232, 'EL DORADO', 0),
(93, 6, 232, 'EL MANTECO', 0),
(94, 6, 232, 'EL PALMAR', 0),
(95, 6, 232, 'EL PAO', 0),
(96, 6, 232, 'GUASIPATI', 0),
(97, 6, 232, 'GURI', 0),
(98, 6, 232, 'LA PARAGUA', 0),
(99, 6, 232, 'MATANZAS', 0),
(100, 6, 232, 'PUERTO ORDAZ', 0),
(101, 6, 232, 'SAN FÉLIX', 0),
(102, 6, 232, 'SANTA ELENA DE UAIRÉN', 0),
(103, 6, 232, 'TUMEREMO', 0),
(104, 6, 232, 'UNARE', 0),
(105, 6, 232, 'UPATA', 0),
(106, 7, 232, 'BEJUMA', 0),
(107, 7, 232, 'BELÉN', 0),
(108, 7, 232, 'CAMPO DE CARABOBO', 0),
(109, 7, 232, 'CANOABO', 0),
(110, 7, 232, 'CENTRAL TACARIGUA', 0),
(111, 7, 232, 'CHIRGUA', 0),
(112, 7, 232, 'CIUDAD ALIANZA', 0),
(113, 7, 232, 'EL PALITO', 0),
(114, 7, 232, 'GUACARA', 0),
(115, 7, 232, 'GUIGUE', 0),
(116, 7, 232, 'LAS TRINCHERAS', 0),
(117, 7, 232, 'LOS GUAYOS', 0),
(118, 7, 232, 'MARIARA', 0),
(119, 7, 232, 'MIRANDA', 0),
(120, 7, 232, 'MONTALBÁN', 0),
(121, 7, 232, 'MORÓN', 0),
(122, 7, 232, 'NAGUANAGUA', 0),
(123, 7, 232, 'PUERTO CABELLO', 0),
(124, 7, 232, 'SAN JOAQUÍN', 0),
(125, 7, 232, 'TOCUYITO', 0),
(126, 7, 232, 'URAMA', 0),
(127, 7, 232, 'VALENCIA', 1),
(128, 7, 232, 'VIGIRIMITA', 0),
(129, 8, 232, 'AGUIRRE', 0),
(130, 8, 232, 'APARTADEROS COJEDES', 0),
(131, 8, 232, 'ARISMENDI', 0),
(132, 8, 232, 'CAMURIQUITO', 0),
(133, 8, 232, 'EL BAÚL', 0),
(134, 8, 232, 'EL LIMÓN', 0),
(135, 8, 232, 'EL PAO COJEDES', 0),
(136, 8, 232, 'EL SOCORRO', 0),
(137, 8, 232, 'LA AGUADITA', 0),
(138, 8, 232, 'LAS VEGAS', 0),
(139, 8, 232, 'LIBERTAD DE COJEDES', 0),
(140, 8, 232, 'MAPUEY', 0),
(141, 8, 232, 'PIÑEDO', 0),
(142, 8, 232, 'SAMANCITO', 0),
(143, 8, 232, 'SAN CARLOS', 1),
(144, 8, 232, 'SUCRE', 0),
(145, 8, 232, 'TINACO', 0),
(146, 8, 232, 'TINAQUILLO', 0),
(147, 8, 232, 'VALLECITO', 0),
(148, 9, 232, 'TUCUPITA', 1),
(149, 24, 232, 'CARACAS', 1),
(150, 24, 232, 'EL JUNQUITO', 0),
(151, 10, 232, 'ADÍCORA', 0),
(152, 10, 232, 'BOCA DE AROA', 0),
(153, 10, 232, 'CABURE', 0),
(154, 10, 232, 'CAPADARE', 0),
(155, 10, 232, 'CAPATÁRIDA', 0),
(156, 10, 232, 'CHICHIRIVICHE', 0),
(157, 10, 232, 'CHURUGUARA', 0),
(158, 10, 232, 'CORO', 1),
(159, 10, 232, 'CUMAREBO', 0),
(160, 10, 232, 'DABAJURO', 0),
(161, 10, 232, 'JUDIBANA', 0),
(162, 10, 232, 'LA CRUZ DE TARATARA', 0),
(163, 10, 232, 'LA VELA DE CORO', 0),
(164, 10, 232, 'LOS TAQUES', 0),
(165, 10, 232, 'MAPARARI', 0),
(166, 10, 232, 'MENE DE MAUROA', 0),
(167, 10, 232, 'MIRIMIRE', 0),
(168, 10, 232, 'PEDREGAL', 0),
(169, 10, 232, 'PÍRITU FALCÓN', 0),
(170, 10, 232, 'PUEBLO NUEVO FALCÓN', 0),
(171, 10, 232, 'PUERTO CUMAREBO', 0),
(172, 10, 232, 'PUNTA CARDÓN', 0),
(173, 10, 232, 'PUNTO FIJO', 0),
(174, 10, 232, 'SAN JUAN DE LOS CAYOS', 0),
(175, 10, 232, 'SAN LUIS', 0),
(176, 10, 232, 'SANTA ANA FALCÓN', 0),
(177, 10, 232, 'SANTA CRUZ DE BUCARAL', 0),
(178, 10, 232, 'TOCOPERO', 0),
(179, 10, 232, 'TOCUYO DE LA COSTA', 0),
(180, 10, 232, 'TUCACAS', 0),
(181, 10, 232, 'YARACAL', 0),
(182, 11, 232, 'ALTAGRACIA DE ORITUCO', 0),
(183, 11, 232, 'CABRUTA', 0),
(184, 11, 232, 'CALABOZO', 0),
(185, 11, 232, 'CAMAGUÁN', 0),
(196, 11, 232, 'CHAGUARAMAS GUÁRICO', 0),
(197, 11, 232, 'EL SOCORRO', 0),
(198, 11, 232, 'EL SOMBRERO', 0),
(199, 11, 232, 'LAS MERCEDES DE LOS LLANOS', 0),
(200, 11, 232, 'LEZAMA', 0),
(201, 11, 232, 'ONOTO', 0),
(202, 11, 232, 'ORTÍZ', 0),
(203, 11, 232, 'SAN JOSÉ DE GUARIBE', 0),
(204, 11, 232, 'SAN JUAN DE LOS MORROS', 1),
(205, 11, 232, 'SAN RAFAEL DE LAYA', 0),
(206, 11, 232, 'SANTA MARÍA DE IPIRE', 0),
(207, 11, 232, 'TUCUPIDO', 0),
(208, 11, 232, 'VALLE DE LA PASCUA', 0),
(209, 11, 232, 'ZARAZA', 0),
(210, 12, 232, 'AGUADA GRANDE', 0),
(211, 12, 232, 'ATARIGUA', 0),
(212, 12, 232, 'BARQUISIMETO', 1),
(213, 12, 232, 'BOBARE', 0),
(214, 12, 232, 'CABUDARE', 0),
(215, 12, 232, 'CARORA', 0),
(216, 12, 232, 'CUBIRO', 0),
(217, 12, 232, 'CUJÍ', 0),
(218, 12, 232, 'DUACA', 0),
(219, 12, 232, 'EL MANZANO', 0),
(220, 12, 232, 'EL TOCUYO', 0),
(221, 12, 232, 'GUARÍCO', 0),
(222, 12, 232, 'HUMOCARO ALTO', 0),
(223, 12, 232, 'HUMOCARO BAJO', 0),
(224, 12, 232, 'LA MIEL', 0),
(225, 12, 232, 'MOROTURO', 0),
(226, 12, 232, 'QUÍBOR', 0),
(227, 12, 232, 'RÍO CLARO', 0),
(228, 12, 232, 'SANARE', 0),
(229, 12, 232, 'SANTA INÉS', 0),
(230, 12, 232, 'SARARE', 0),
(231, 12, 232, 'SIQUISIQUE', 0),
(232, 12, 232, 'TINTORERO', 0),
(233, 13, 232, 'APARTADEROS MÉRIDA', 0),
(234, 13, 232, 'ARAPUEY', 0),
(235, 13, 232, 'BAILADORES', 0),
(236, 13, 232, 'CAJA SECA', 0),
(237, 13, 232, 'CANAGUÁ', 0),
(238, 13, 232, 'CHACHOPO', 0),
(239, 13, 232, 'CHIGUARA', 0),
(240, 13, 232, 'EJIDO', 0),
(241, 13, 232, 'EL VIGÍA', 0),
(242, 13, 232, 'LA AZULITA', 0),
(243, 13, 232, 'LA PLAYA', 0),
(244, 13, 232, 'LAGUNILLAS MÉRIDA', 0),
(245, 13, 232, 'MÉRIDA', 1),
(246, 13, 232, 'MESA DE BOLÍVAR', 0),
(247, 13, 232, 'MUCUCHÍES', 0),
(248, 13, 232, 'MUCUJEPE', 0),
(249, 13, 232, 'MUCURUBA', 0),
(250, 13, 232, 'NUEVA BOLIVIA', 0),
(251, 13, 232, 'PALMARITO', 0),
(252, 13, 232, 'PUEBLO LLANO', 0),
(253, 13, 232, 'SANTA CRUZ DE MORA', 0),
(254, 13, 232, 'SANTA ELENA DE ARENALES', 0),
(255, 13, 232, 'SANTO DOMINGO', 0),
(256, 13, 232, 'TABÁY', 0),
(257, 13, 232, 'TIMOTES', 0),
(258, 13, 232, 'TORONDOY', 0),
(259, 13, 232, 'TOVAR', 0),
(260, 13, 232, 'TUCANI', 0),
(261, 13, 232, 'ZEA', 0),
(262, 14, 232, 'ARAGUITA', 0),
(263, 14, 232, 'CARRIZAL', 0),
(264, 14, 232, 'CAUCAGUA', 0),
(265, 14, 232, 'CHAGUARAMAS MIRANDA', 0),
(266, 14, 232, 'CHARALLAVE', 0),
(267, 14, 232, 'CHIRIMENA', 0),
(268, 14, 232, 'CHUSPA', 0),
(269, 14, 232, 'CÚA', 0),
(270, 14, 232, 'CUPIRA', 0),
(271, 14, 232, 'CURIEPE', 0),
(272, 14, 232, 'EL GUAPO', 0),
(273, 14, 232, 'EL JARILLO', 0),
(274, 14, 232, 'FILAS DE MARICHE', 0),
(275, 14, 232, 'GUARENAS', 0),
(276, 14, 232, 'GUATIRE', 0),
(277, 14, 232, 'HIGUEROTE', 0),
(278, 14, 232, 'LOS ANAUCOS', 0),
(279, 14, 232, 'LOS TEQUES', 1),
(280, 14, 232, 'OCUMARE DEL TUY', 0),
(281, 14, 232, 'PANAQUIRE', 0),
(282, 14, 232, 'PARACOTOS', 0),
(283, 14, 232, 'RÍO CHICO', 0),
(284, 14, 232, 'SAN ANTONIO DE LOS ALTOS', 0),
(285, 14, 232, 'SAN DIEGO DE LOS ALTOS', 0),
(286, 14, 232, 'SAN FERNANDO DEL GUAPO', 0),
(287, 14, 232, 'SAN FRANCISCO DE YARE', 0),
(288, 14, 232, 'SAN JOSÉ DE LOS ALTOS', 0),
(289, 14, 232, 'SAN JOSÉ DE RÍO CHICO', 0),
(290, 14, 232, 'SAN PEDRO DE LOS ALTOS', 0),
(291, 14, 232, 'SANTA LUCÍA', 0),
(292, 14, 232, 'SANTA TERESA', 0),
(293, 14, 232, 'TACARIGUA DE LA LAGUNA', 0),
(294, 14, 232, 'TACARIGUA DE MAMPORAL', 0),
(295, 14, 232, 'TÁCATA', 0),
(296, 14, 232, 'TURUMO', 0),
(297, 15, 232, 'AGUASAY', 0),
(298, 15, 232, 'ARAGUA DE MATURÍN', 0),
(299, 15, 232, 'BARRANCAS DEL ORINOCO', 0),
(300, 15, 232, 'CAICARA DE MATURÍN', 0),
(301, 15, 232, 'CARIPE', 0),
(302, 15, 232, 'CARIPITO', 0),
(303, 15, 232, 'CHAGUARAMAL', 0),
(305, 15, 232, 'CHAGUARAMAS MONAGAS', 0),
(307, 15, 232, 'EL FURRIAL', 0),
(308, 15, 232, 'EL TEJERO', 0),
(309, 15, 232, 'JUSEPÍN', 0),
(310, 15, 232, 'LA TOSCANA', 0),
(311, 15, 232, 'MATURÍN', 1),
(312, 15, 232, 'MIRAFLORES', 0),
(313, 15, 232, 'PUNTA DE MATA', 0),
(314, 15, 232, 'QUIRIQUIRE', 0),
(315, 15, 232, 'SAN ANTONIO DE MATURÍN', 0),
(316, 15, 232, 'SAN VICENTE MONAGAS', 0),
(317, 15, 232, 'SANTA BÁRBARA', 0),
(318, 15, 232, 'TEMBLADOR', 0),
(319, 15, 232, 'TERESEN', 0),
(320, 15, 232, 'URACOA', 0),
(321, 16, 232, 'ALTAGRACIA', 0),
(322, 16, 232, 'BOCA DE POZO', 0),
(323, 16, 232, 'BOCA DE RÍO', 0),
(324, 16, 232, 'EL ESPINAL', 0),
(325, 16, 232, 'EL VALLE DEL ESPÍRITU SANTO', 0),
(326, 16, 232, 'EL YAQUE', 0),
(327, 16, 232, 'JUANGRIEGO', 0),
(328, 16, 232, 'LA ASUNCIÓN', 1),
(329, 16, 232, 'LA GUARDIA', 0),
(330, 16, 232, 'PAMPATAR', 0),
(331, 16, 232, 'PORLAMAR', 0),
(332, 16, 232, 'PUERTO FERMÍN', 0),
(333, 16, 232, 'PUNTA DE PIEDRAS', 0),
(334, 16, 232, 'SAN FRANCISCO DE MACANAO', 0),
(335, 16, 232, 'SAN JUAN BAUTISTA', 0),
(336, 16, 232, 'SAN PEDRO DE COCHE', 0),
(337, 16, 232, 'SANTA ANA DE NUEVA ESPARTA', 0),
(338, 16, 232, 'VILLA ROSA', 0),
(339, 17, 232, 'ACARIGUA', 0),
(340, 17, 232, 'AGUA BLANCA', 0),
(341, 17, 232, 'ARAURE', 0),
(342, 17, 232, 'BISCUCUY', 0),
(343, 17, 232, 'BOCONOITO', 0),
(344, 17, 232, 'CAMPO ELÍAS', 0),
(345, 17, 232, 'CHABASQUÉN', 0),
(346, 17, 232, 'GUANARE', 1),
(347, 17, 232, 'GUANARITO', 0),
(348, 17, 232, 'LA APARICIÓN', 0),
(349, 17, 232, 'LA MISIÓN', 0),
(350, 17, 232, 'MESA DE CAVACAS', 0),
(351, 17, 232, 'OSPINO', 0),
(352, 17, 232, 'PAPELÓN', 0),
(353, 17, 232, 'PAYARA', 0),
(354, 17, 232, 'PIMPINELA', 0),
(355, 17, 232, 'PÍRITU DE PORTUGUESA', 0),
(356, 17, 232, 'SAN RAFAEL DE ONOTO', 0),
(357, 17, 232, 'SANTA ROSALÍA', 0),
(358, 17, 232, 'TURÉN', 0),
(359, 18, 232, 'ALTOS DE SUCRE', 0),
(360, 18, 232, 'ARAYA', 0),
(361, 18, 232, 'CARIACO', 0),
(362, 18, 232, 'CARÚPANO', 0),
(363, 18, 232, 'CASANAY', 0),
(364, 18, 232, 'CUMANÁ', 1),
(365, 18, 232, 'CUMANACOA', 0),
(366, 18, 232, 'EL MORRO PUERTO SANTO', 0),
(367, 18, 232, 'EL PILAR', 0),
(368, 18, 232, 'EL POBLADO', 0),
(369, 18, 232, 'GUACA', 0),
(370, 18, 232, 'GUIRIA', 0),
(371, 18, 232, 'IRAPA', 0),
(372, 18, 232, 'MANICUARE', 0),
(373, 18, 232, 'MARIGUITAR', 0),
(374, 18, 232, 'RÍO CARIBE', 0),
(375, 18, 232, 'SAN ANTONIO DEL GOLFO', 0),
(376, 18, 232, 'SAN JOSÉ DE AEROCUAR', 0),
(377, 18, 232, 'SAN VICENTE DE SUCRE', 0),
(378, 18, 232, 'SANTA FE DE SUCRE', 0),
(379, 18, 232, 'TUNAPUY', 0),
(380, 18, 232, 'YAGUARAPARO', 0),
(381, 18, 232, 'YOCO', 0),
(382, 19, 232, 'ABEJALES', 0),
(383, 19, 232, 'BOROTA', 0),
(384, 19, 232, 'BRAMON', 0),
(385, 19, 232, 'CAPACHO', 0),
(386, 19, 232, 'COLÓN', 0),
(387, 19, 232, 'COLONCITO', 0),
(388, 19, 232, 'CORDERO', 0),
(389, 19, 232, 'EL COBRE', 0),
(390, 19, 232, 'EL PINAL', 0),
(391, 19, 232, 'INDEPENDENCIA', 0),
(392, 19, 232, 'LA FRÍA', 0),
(393, 19, 232, 'LA GRITA', 0),
(394, 19, 232, 'LA PEDRERA', 0),
(395, 19, 232, 'LA TENDIDA', 0),
(396, 19, 232, 'LAS DELICIAS', 0),
(397, 19, 232, 'LAS HERNÁNDEZ', 0),
(398, 19, 232, 'LOBATERA', 0),
(399, 19, 232, 'MICHELENA', 0),
(400, 19, 232, 'PALMIRA', 0),
(401, 19, 232, 'PREGONERO', 0),
(402, 19, 232, 'QUENIQUEA', 0),
(403, 19, 232, 'RUBIO', 0),
(404, 19, 232, 'SAN ANTONIO DEL TACHIRA', 0),
(405, 19, 232, 'SAN CRISTOBAL', 1),
(406, 19, 232, 'SAN JOSÉ DE BOLÍVAR', 0),
(407, 19, 232, 'SAN JOSECITO', 0),
(408, 19, 232, 'SAN PEDRO DEL RÍO', 0),
(409, 19, 232, 'SANTA ANA TÁCHIRA', 0),
(410, 19, 232, 'SEBORUCO', 0),
(411, 19, 232, 'TÁRIBA', 0),
(412, 19, 232, 'UMUQUENA', 0),
(413, 19, 232, 'UREÑA', 0),
(414, 20, 232, 'BATATAL', 0),
(415, 20, 232, 'BETIJOQUE', 0),
(416, 20, 232, 'BOCONÓ', 0),
(417, 20, 232, 'CARACHE', 0),
(418, 20, 232, 'CHEJENDE', 0),
(419, 20, 232, 'CUICAS', 0),
(420, 20, 232, 'EL DIVIDIVE', 0),
(421, 20, 232, 'EL JAGUITO', 0),
(422, 20, 232, 'ESCUQUE', 0),
(423, 20, 232, 'ISNOTÚ', 0),
(424, 20, 232, 'JAJÓ', 0),
(425, 20, 232, 'LA CEIBA', 0),
(426, 20, 232, 'LA CONCEPCIÓN DE TRUJLLO', 0),
(427, 20, 232, 'LA MESA DE ESNUJAQUE', 0),
(428, 20, 232, 'LA PUERTA', 0),
(429, 20, 232, 'LA QUEBRADA', 0),
(430, 20, 232, 'MENDOZA FRÍA', 0),
(431, 20, 232, 'MESETA DE CHIMPIRE', 0),
(432, 20, 232, 'MONAY', 0),
(433, 20, 232, 'MOTATÁN', 0),
(434, 20, 232, 'PAMPÁN', 0),
(435, 20, 232, 'PAMPANITO', 0),
(436, 20, 232, 'SABANA DE MENDOZA', 0),
(437, 20, 232, 'SAN LÁZARO', 0),
(438, 20, 232, 'SANTA ANA DE TRUJILLO', 0),
(439, 20, 232, 'TOSTÓS', 0),
(440, 20, 232, 'TRUJILLO', 1),
(441, 20, 232, 'VALERA', 0),
(442, 21, 232, 'CARAYACA', 0),
(443, 21, 232, 'LITORAL', 0),
(444, 25, 232, 'ARCHIPIÉLAGO LOS ROQUES', 0),
(445, 22, 232, 'AROA', 0),
(446, 22, 232, 'BORAURE', 0),
(447, 22, 232, 'CAMPO ELÍAS DE YARACUY', 0),
(448, 22, 232, 'CHIVACOA', 0),
(449, 22, 232, 'COCOROTE', 0),
(450, 22, 232, 'FARRIAR', 0),
(451, 22, 232, 'GUAMA', 0),
(452, 22, 232, 'MARÍN', 0),
(453, 22, 232, 'NIRGUA', 0),
(454, 22, 232, 'SABANA DE PARRA', 0),
(455, 22, 232, 'SALOM', 0),
(456, 22, 232, 'SAN FELIPE', 1),
(457, 22, 232, 'SAN PABLO DE YARACUY', 0),
(458, 22, 232, 'URACHICHE', 0),
(459, 22, 232, 'YARITAGUA', 0),
(460, 22, 232, 'YUMARE', 0),
(461, 23, 232, 'BACHAQUERO', 0),
(462, 23, 232, 'BOBURES', 0),
(463, 23, 232, 'CABIMAS', 0),
(464, 23, 232, 'CAMPO CONCEPCIÓN', 0),
(465, 23, 232, 'CAMPO MARA', 0),
(466, 23, 232, 'CAMPO ROJO', 0),
(467, 23, 232, 'CARRASQUERO', 0),
(468, 23, 232, 'CASIGUA', 0),
(469, 23, 232, 'CHIQUINQUIRÁ', 0),
(470, 23, 232, 'CIUDAD OJEDA', 0),
(471, 23, 232, 'EL BATEY', 0),
(472, 23, 232, 'EL CARMELO', 0),
(473, 23, 232, 'EL CHIVO', 0),
(474, 23, 232, 'EL GUAYABO', 0),
(475, 23, 232, 'EL MENE', 0),
(476, 23, 232, 'EL VENADO', 0),
(477, 23, 232, 'ENCONTRADOS', 0),
(478, 23, 232, 'GIBRALTAR', 0),
(479, 23, 232, 'ISLA DE TOAS', 0),
(480, 23, 232, 'LA CONCEPCIÓN DEL ZULIA', 0),
(481, 23, 232, 'LA PAZ', 0),
(482, 23, 232, 'LA SIERRITA', 0),
(483, 23, 232, 'LAGUNILLAS DEL ZULIA', 0),
(484, 23, 232, 'LAS PIEDRAS DE PERIJÁ', 0),
(485, 23, 232, 'LOS CORTIJOS', 0),
(486, 23, 232, 'MACHIQUES', 0),
(487, 23, 232, 'MARACAIBO', 1),
(488, 23, 232, 'MENE GRANDE', 0),
(489, 23, 232, 'PALMAREJO', 0),
(490, 23, 232, 'PARAGUAIPOA', 0),
(491, 23, 232, 'POTRERITO', 0),
(492, 23, 232, 'PUEBLO NUEVO DEL ZULIA', 0),
(493, 23, 232, 'PUERTOS DE ALTAGRACIA', 0),
(494, 23, 232, 'PUNTA GORDA', 0),
(495, 23, 232, 'SABANETA DE PALMA', 0),
(496, 23, 232, 'SAN FRANCISCO', 0),
(497, 23, 232, 'SAN JOSÉ DE PERIJÁ', 0),
(498, 23, 232, 'SAN RAFAEL DEL MOJÁN', 0),
(499, 23, 232, 'SAN TIMOTEO', 0),
(500, 23, 232, 'SANTA BÁRBARA DEL ZULIA', 0),
(501, 23, 232, 'SANTA CRUZ DE MARA', 0),
(502, 23, 232, 'SANTA CRUZ DEL ZULIA', 0),
(503, 23, 232, 'SANTA RITA', 0),
(504, 23, 232, 'SINAMAICA', 0),
(505, 23, 232, 'TAMARE', 0),
(506, 23, 232, 'TÍA JUANA', 0),
(507, 23, 232, 'VILLA DEL ROSARIO', 0),
(508, 21, 232, 'LA GUAIRA', 1),
(509, 21, 232, 'CATIA LA MAR', 0),
(510, 21, 232, 'MACUTO', 0),
(511, 21, 232, 'NAIGUATÁ', 0),
(512, 25, 232, 'ARCHIPIÉLAGO LOS MONJES', 0),
(513, 25, 232, 'ISLA LA TORTUGA Y CAYOS ADYACENTES', 0),
(514, 25, 232, 'ISLA LA SOLA', 0),
(515, 25, 232, 'ISLAS LOS TESTIGOS', 0),
(516, 25, 232, 'ISLAS LOS FRAILES', 0),
(517, 25, 232, 'ISLA LA ORCHILA', 0),
(518, 25, 232, 'ARCHIPIÉLAGO LAS AVES', 0),
(519, 25, 232, 'ISLA DE AVES', 0),
(520, 25, 232, 'ISLA LA BLANQUILLA', 0),
(521, 25, 232, 'ISLA DE PATOS', 0),
(522, 25, 232, 'ISLAS LOS HERMANOS', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `clien_codig` int(11) NOT NULL,
  `tclie_codig` int(11) NOT NULL,
  `tdocu_codig` int(11) NOT NULL,
  `clien_ndocu` varchar(20) NOT NULL,
  `clien_denom` varchar(200) NOT NULL,
  `clien_ccont` varchar(20) NOT NULL,
  `clien_corre` varchar(50) NOT NULL,
  `clien_telef` varchar(15) NOT NULL,
  `clien_direc` text NOT NULL,
  `clien_obser` text NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `clien_fcrea` date NOT NULL,
  `clien_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`clien_codig`, `tclie_codig`, `tdocu_codig`, `clien_ndocu`, `clien_denom`, `clien_ccont`, `clien_corre`, `clien_telef`, `clien_direc`, `clien_obser`, `usuar_codig`, `clien_fcrea`, `clien_hcrea`) VALUES
(3, 1, 1, '123456', 'KEVIN FIGUERA', '12345', 'A@A.A', '12345', '1234', '', 3, '2018-01-22', '08:13:57'),
(4, 1, 1, '24900845', 'KEVIN FIGUERA', '', 'KEVINALEJANDRO3@GMAIL.COM', '4241941881', 'MONTALBAN', 'NINGUNO', 3, '2018-08-01', '19:36:54'),
(5, 1, 1, '13882284', 'IVEY MONTOYA', '', 'IVEYMONTOYA@GMAIL.COM', '5555555555', 'CALLE NEGRIN CON GARCIA LOS ALAMOS ', 'OTRO BANCO', 3, '2018-08-07', '22:13:40'),
(6, 1, 1, '24321991', 'KARLA NADALES', '', 'KARLANADALES12@GMAIL.COM', '04141111111', 'BARINAS', '2', 3, '2018-08-08', '05:24:10'),
(9, 1, 1, '18390585', 'DANNY MARIN', '', 'DMARIN@GMAIL.COM', '02121112222', 'A', '1', 3, '2018-08-08', '05:27:45'),
(12, 2, 4, '309386310', 'JVKA REDES Y SISTEMAS CA', '', 'KEVINALEJANDRO3@GMAIL.COM', '4241941881', 'MONTABAN', 'NINGUNA', 3, '2018-08-28', '05:23:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente_tipo`
--

CREATE TABLE `cliente_tipo` (
  `tclie_codig` int(11) NOT NULL,
  `tclie_descr` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cliente_tipo`
--

INSERT INTO `cliente_tipo` (`tclie_codig`, `tclie_descr`) VALUES
(2, 'JURIDICO'),
(1, 'NATURAL');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cobranza_tipo`
--

CREATE TABLE `cobranza_tipo` (
  `ctipo_codig` int(11) NOT NULL,
  `ctipo_descr` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cobranza_tipo`
--

INSERT INTO `cobranza_tipo` (`ctipo_codig`, `ctipo_descr`) VALUES
(2, 'CONCEPTO 2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colegio`
--

CREATE TABLE `colegio` (
  `coleg_codig` int(11) NOT NULL,
  `coleg_nombr` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `coleg_direc` text COLLATE utf8mb4_bin NOT NULL,
  `colec_telef` varchar(20) COLLATE utf8mb4_bin NOT NULL,
  `coleg_pgweb` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `coleg_rede1` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `coleg_rede2` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `coleg_rede3` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `coleg_obser` text COLLATE utf8mb4_bin NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `coleg_fcrea` date NOT NULL,
  `coleg_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Volcado de datos para la tabla `colegio`
--

INSERT INTO `colegio` (`coleg_codig`, `coleg_nombr`, `coleg_direc`, `colec_telef`, `coleg_pgweb`, `coleg_rede1`, `coleg_rede2`, `coleg_rede3`, `coleg_obser`, `usuar_codig`, `coleg_fcrea`, `coleg_hcrea`) VALUES
(2, 'COLEGIO LAS ACACIAS', '12345', '2345', '12345', '12345', '1234', '12344', '', 3, '2018-09-17', '21:15:59'),
(3, 'LARUN RAYUN', 'BHSAHDBSABDIASHDHIS', '0005657489', 'LARUNRAYUN.COM', 'AZA', 'XAXA', 'ASS', '', 3, '2018-11-21', '15:29:50'),
(5, 'LICEO INSTITUTO NACIONAL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(6, 'LICEO JAVIERA CARRERA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(7, 'LICEO ISAURA DINATOR DE GUZMAN', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(8, 'LICEO BICENTENARIO TERESA PRATS', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(9, 'LICEO NRO 2 MIGUEL LUIS AMUNATEGUI', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(10, 'LICEO DE APLICACION RECTOR JORGE E SCHNE', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(11, 'LICEO MANUEL BARROS BORGONO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(12, 'LICEO CONFEDERACION SUIZA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(13, 'LICEO JOS? DE SAN MARTIN', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(14, 'LICEO DARIO SALAS', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(15, 'INTERNADO NACIONAL BARROS ARANA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(16, 'LICEO INDUSTRIAL ELIODORO GARCIA ZEGERS', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(17, 'LICEO POLITEC. PDTE. GABRIEL GONZALEZ VIDELA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(18, 'INST.SUP.DE COMERCIO EDUARDO FREI M.', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(19, 'LICEO TECNICO A N? 27 CLELIA CLAVEL DINAT', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(20, 'LICEO POLIVALENTE A-28 EMILIA TORO DE BAL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(21, 'INSTITUTO COMERCIAL ELIODORO DOMINGUEZ DOMINGUEZ', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(22, 'LICEO COMERCIAL JOAQUIN VERA MORALES', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(23, 'ESC. BAS. REPUBLICA ORIENTAL DE URUGUAY', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(24, 'ESCUELA BASICA PROVINCIA DE CHILOE', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(25, 'ESCUELA BASICA REPUBLICA DE COLOMBIA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(26, 'ESCUELA BASICA REPUBLICA DE PANAMA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(27, 'CENTRO DIAGNOSTICO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(28, 'ESCUELA CADETE ARTURO PRAT CHACON', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(29, 'ESCUELA BASICA IRENE FREI DE CID', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(30, 'ESCUELA BASICA LIBERTADORES DE CHILE', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(31, 'ESCUELA BASICA BENJAMIN VICUNA MACKENNA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(32, 'LICEO REPUBLICA DE BRASIL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(33, 'ESCUELA REPUBLICA DE ISRAEL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(34, 'ESCUELA BASICA REPUBLICA DE HAITI', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(35, 'LICEO DE ADULTOS HERBERT VARGAS WALLIS', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(36, 'ESCUELA BASICA DR LUIS CALVO MACKENNA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(37, 'ESCUELA FERNANDO ALESSANDRI RODRIGUEZ', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(38, 'ESCUELA DIFERENCIAL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(39, 'ESC. ESP. LENG. CARDENAL RAUL SILVA H.', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(40, 'ESCUELA BAS SALVADOR SANFUENTES REP EEUU', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(41, 'LICEO MUNICIPAL METROPOLITANO DE ADULTOS', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(42, 'ESCUELA REPUBLICA EL LIBANO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(43, 'ESCUELA REPUBLICA DE ALEMANIA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(44, 'CIUDAD SANTIAGO DE CHILE', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(45, 'ESCUELA BASICA REPUBLICA DE MEXICO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(46, 'ESC. DIFERENCIAL EDMUNDO DE AMICIS', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(47, 'ESCUELA BASICA REPUBLICA DEL ECUADOR', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(48, 'LICEO DR HUMBERTO MATURANA ROMESIN', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(49, 'ESCUELA BASICA REINA VICTORIA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(50, 'ESCUELA BASICA REYES CATOLICOS', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(51, 'ESCUELA B?SICA F N? 64', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(52, 'ESC.DIFERENCIAL REPUBLICA DE SUDAFRICA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(53, 'ESC. CENTRO DE CAPACITACION LABORAL SANTIAGO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(54, 'ESCUELA DIFERENCIAL JUAN SANDOVAL C', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(55, 'LICEO JOSE MIGUEL INFANTE', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(56, 'LICEO COMERCIAL ARTURO PRAT', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(57, 'COLEGIO ALTO PALENA', 'MAIPÚ #85', '2 2682 0443', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(58, 'COLEGIO WUNMAN', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(59, 'CENTRO EDUCATIVO SALESIANOS ALAMEDA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(60, 'COLEGIO SANTA CRUZ', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(61, 'COLEGIO SAN IGNACIO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(62, 'COLEGIO NUESTRA SENORA DE ANDACOLLO', 'MAPOCHO #2341, SANTIAGO CENTRO', '+56965475529', '', '', '', '4TO_BABILONICO', '', 0, '0000-00-00', '00:00:00'),
(63, 'ESCUELA COLEGIO POLIVALENTE STA FAMILIA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(64, 'LICEO POLIT. SARA BLINDER DARGOLTZ', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(65, 'INSTITUTO DE ENS.MEDIA CLAUDIO MATTE', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(66, 'COLEGIO SAN SEBASTIAN', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(67, 'CENTRO EDUC. CASA TALLERES SAN VICENTE DE PAU', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(68, 'COLEGIO SANTA MARIA DE SANTIAGO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(69, 'LICEO FRANCES', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(70, 'LICEO Y COLEGIO NUEVO HISPANO CHILENO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(71, 'LICEO MANUEL BULNES DE SANTIAGO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(72, 'INSTITUTO COMERCIAL BLAS CANAS', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(73, 'COLEGIO POLIV. MANUEL BAQUEDANO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(74, 'COLEGIO LORENZO SAZIE', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(75, 'COLEGIO SAN ANTONIO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(76, 'INSTITUTO COMERCIAL PART.ARTURO PRAT', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(77, 'COLEGIO FRANCISCO ARRIARAN', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(78, 'ESCUELA PARTICULAR FRANCISCO ANDRES OLEA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(79, 'LICEO POLIVALENTE ITALIA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(80, 'ESCUELA PARTICULAR GUILLERMO MATTA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(81, 'ESCUELA PARTICULAR HERMANOS MATTE', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(82, 'ESCUELA BASICA N?43 PATRONATO SAN ANTONIO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(83, 'COLEGIO SANTA ISABEL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(84, 'ESCUELA BASICA BERNARDO O\'HIGGINS', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(85, 'ESCUELA PARTICULAR VICTORIA PRIETO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(86, 'COLEGIO FILIPENSE', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(87, 'ESCUELA PARTICULAR SAN LAZARO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(88, 'COLEGIO SANTA TERESITA DEL NINO JESUS', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(89, 'ESCUELA MANUEL JOSE IRARRAZAVAL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(90, 'COLEGIO ADVENTISTA PORVENIR', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(91, 'COLEGIO ORATORIO DON BOSCO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(92, 'ESC.BASICA PART.BRITANIA CENTRO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(93, 'ESCUELA BASICA N?1319 DAVID MATARASSO 2', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(94, 'ESCUELA BASICA ARTURO PRAT', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(95, 'ESCUELA PARTICULAR SAINT HENRY SCHOOL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(96, 'ESCUELA PARTICULAR ESPECIAL CEDIN', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(97, 'ESCUELA PARTICULAR DIVINA MARIA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(98, 'COLEGIO MARIA LUISA VILLALON', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(99, 'COLEGIO INSTITUTO ALONSO DE ERCILLA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(100, 'COLEGIO INSTITUTO ZAMBRANO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(101, 'COLEGIO HISPANO AMERICANO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(102, 'COLEGIO SAINT ROSE SCHOOL', 'HSSNSWHS', '04141008230', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(103, 'COLEGIO EXCELSIOR', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(104, 'COLEGIO DE LOS SAGRADOS CORAZONES ALAMEDA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(105, 'LICEO  MARIA AUXILIADORA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(106, 'COLEGIO SANTA CECILIA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(108, 'COLEGIO SANTA ELENA CARMELITAS MISIONERAS TERESIAN', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(109, 'COLEGIO DE ADULTOS CENTRO DE ESTUDIOS LA ARAUCANA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(110, 'COLEGIO JUAN BOSCO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(111, 'LICEO JOSE MIGUEL CARRERA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(112, 'COLEGIO CAMBRIDGE', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(113, 'COLEGIO LUIS ENRIQUE IZQUIERDO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(114, 'ESCUELA MANUEL MONTT', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(115, 'ESCUELA BASICA PARTICULAR STA BERNARDITA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(116, 'COLEGIO IGNACIO SERRANO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(117, 'ESCUELA BASICA ENGLISH HIGH SCHOOL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(118, 'ESCUELA BASICA CONTINENTAL SCHOOL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(120, 'COLEGIO DIEGO DE ALMAGRO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(121, 'ESCUELA BASICA ANTONIO BORQUEZ SOLAR', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(122, 'ESCUELA DE PARVULOS PELUSIN', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(123, 'ESCUELA DE PARVULOS N?1146 HEYDDIE', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(124, 'LICEO PROFESIONAL ABDON CIFUENTES', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(125, 'LICEO INDUSTRIAL DE LA CONSTRUCCION VICTOR BEZANIL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(126, 'LICEO INDUSTRIAL A-22 DE SANTIAGO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(127, 'ESCUELA ESPECIAL CTI.LOS COPIHUES   N.3', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(128, 'ESC.PARA SORDOS DR. OTTE GABLER', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(129, 'LICEO LUIS GALECIO CORVERA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(130, 'ESCUELA BASICA REBECA CATALAN VARGAS', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(131, 'LICEO BETSABE HORMAZABAL DE ALARCON', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(132, 'LICEO ANDRES BELLO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(133, 'ESCUELA DE LA INDUSTRIA GRAFICA HECTOR GO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(134, 'LICEO POLITECNICO DE SAN JOAQUIN', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(135, 'LICEO COMERCIAL INSTITUTO SUPERIOR DE COMERCIO DE ', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(136, 'ESCUELA  CONSOLIDADA DAVILA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(137, 'CENTRO EDUCACIONAL HORACIO ARAVENA A.', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(138, 'ESC. BAS. GABRIELA MISTRAL DE SAN MIGUEL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(139, 'ESCUELA GENERAL BASICA SANTA FE', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(140, 'COLEGIO CIUDAD DE FRANKFORT', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(141, 'ESCUELA TERRITORIO ANTARTICO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(142, 'ESCUELA POETAS DE CHILE', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(143, 'ESCUELA LA VICTORIA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(144, 'LICEO MUNICIPAL ENRIQUE BACKAUSSE', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(145, 'LICEO MUNICIPAL SAN JOAQUIN', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(146, 'ESCUELA CENTRO EDUC. REP. MEXICANA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(147, 'ESCUELA POETA GONZALO ROJAS', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(148, 'ESCUELA BASICA FRAY CAMILO HENRIQUEZ', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(149, 'ESCUELA LLANO SUBERCASEAUX', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(150, 'ESCUELA BSICA BERLIN', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(151, 'ESCUELA BASICA RICARDO E. LATCHMAN', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(152, 'ESCUELA POETA VICTOR DOMINGO SILVA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(153, 'CENTRO EDUC. PROVINCIA DE ?UBLE', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(154, 'ESCUELA VILLA SAN MIGUEL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(155, 'ESCUELA BASICA MUNICIPAL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(156, 'ESCUELA COEDUCACIONAL EUGENIO BROWNE', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(157, 'ESC. BAS. Y ESP. SU SANTIDAD JUAN XXIII', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(158, 'CENTRO EDUCACIONAL MUNICIPAL SAN JOAQUIN', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(159, 'ESCUELA BASICA DIEGO PORTALES PALAZUELOS', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(160, 'ESCUELA ESPECIAL LOS CEDROS DEL LIBANO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(161, 'ESCUELA DE PARVULOS HORACIO ARAVENA ANEXO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(162, 'ESCUELA DE PARVULOS RAYITO DE LUZ', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(163, 'ESCUELA REPUBLICA FEDERAL ALEMANIA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(164, 'ESCUELA ESPECIAL CHILE BRASIL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(165, 'ESCUELA B?SICA POETA NERUDA (EX-483)', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(166, 'ESCUELA BOROA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(167, 'ESCUELA ESPECIAL DE LISIADOS', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(168, 'INST. REG. EDUC ADULTOS SAN MIGUEL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(169, 'ESC. ESP. DE ADULTOS HUGO MORALES BIZAMA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(170, 'COLEGIO LA CASTRINA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(171, 'ESCUELA IND. SAN ANTONIO DE PADUA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(172, 'LICEO INDUSTRIAL GENERAL CARRERA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(173, 'LICEO SAINT PATRICKS SCHOOL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(174, 'LICEO POLITECNICO SAN LUIS', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(175, 'ESCUELA BAS.PART.N 23 COLEGIO SANTA ANA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(176, 'INSTIT.POLIT.SAN MIGUEL ARCANGEL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(177, 'ESCUELA ESPECIAL ANEXO SAN CLEMENTE', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(178, 'ESC. BASICA Y LICEO IND.SAINT LAWRENCE', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(179, 'COLEGIO PANAMERICAN COLLEGE', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(180, 'ESCUELA  E INSTITUTO DE MADRID', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(181, 'ESC. TECNICA E INST. COMER. SAINT LAWRENCE', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(182, 'COLEGIO SANTA ROSA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(183, 'ESCUELA PARTICULAR HANSEL Y GRETEL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(184, 'COLEGIO ESPIRITU SANTO DEL VERBO DIVINO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(185, 'ESCUELA INDUSTRIAL PART. LAUTARO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(186, 'COLEGIO CHILE ANEXO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(187, 'LICEO COMERCIAL JAVIERA Y J L CARRERA N1', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(188, 'ESCUELA PARTICULAR LUCILA GODOY', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(189, 'COLEGIO PART. NUESTRA SRA DEL HUERTO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(190, 'COLEGIO POLIV.ADVENTISTA SANTIAGO SUR', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(191, 'ESCUELA PARTICULAR PIO DOCE', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(192, 'ESCUELA PARTICULAR Y COLEGIO CHILE', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(193, 'ESCUELA PARTICULAR JOSE A.ALFONSO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(194, 'ESC.BAS.PROFESORA AIDA RAMOS DIAZ', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(195, 'ESCUELA PARTICULAR LAURA VICUNA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(196, 'COLEGIO PARTICULAR POBL. ALESSANDRI', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(197, 'COLEGIO SUBERCASEAUX COLLEGE', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(198, 'COLEGIO PART. ANTONIO ACEVEDO HERNANDEZ', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(199, 'ESCUELA PARTICULAR GRAN AVENIDA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(200, 'ESCUELA BASICA PARTIC JACQUELINE KENNEDY', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(201, 'ESCUELA PARTICULAR SPRING COLLEGE', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(202, 'LICEO INDUSTRIAL DEL VERBO DIVINO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(203, 'COLEGIO CRISTIANO NATANAEL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(204, 'COLEGIO PARTICULAR PADRE FIDEL ABARCA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(205, 'COLEGIO SANTO CURA DE ARS', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(206, 'PRINCESS MARGARET COLLEGE', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(207, 'COLEGIO DE LA PURISIMA PARA NINOS SORDOS', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(208, 'ESCUELA BASICA PART. COLEGIO INGLES', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(209, 'ESCUELA ROBERT BADEN POWELL SCHOOL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(210, 'ESCUELA BAS. ALFRED BINET SCHOOL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(211, 'ESCUELA PARTICULAR ECOLE NOEL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(212, 'ESCUELA PARTICULAR LA RAIZ Y LA ESPIGA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(213, 'ESCUELA PARTICULAR MIGUEL DAVILA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(214, 'ESCUELA BASICA PARTICULAR RAINBOW SCHOOL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(215, 'ESCUELA BASICA MAIN SCHOOL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(216, 'ESCUELA PART.CENTRO EDUC.SANTA MONICA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(217, 'ESCUELA PARTICULAR DIVINA GABRIELA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(218, 'ESC. PART. SANTA CATALINA DE SAN MIGUEL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(219, 'ESCUELA PARTICULAR SAN CARLOS', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(220, 'ESCUELA PARTICULAR EL GRECO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(221, 'ESCUELA PARTICULAR AMIGOS', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(222, 'ESCUELA BAS. PART. QUEEN & FLOWER COLLEGE', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(223, 'ESCUELA PART. FERNANDO DE ARAGON', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(224, 'ESCUELA ESPECIAL PARTICULAR PAPELUCHO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(225, 'ESCUELA PARTICULAR LA RONDA DE SN.MIGUEL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(226, 'COLEGIO KING?S SCHOOL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(227, 'ESCUELA DE PARVULOS PEDRO F.INIGUEZ', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(228, 'ESCUELA PARTICULAR MARQUEZ DE OVANDO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(229, 'CENTRO ED.ESPECIAL MARIA MONTESSORI', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(230, 'ESCUELA PARTICULAR ROCIO DE LOS ANGELES', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(231, 'ESCUELA BASICA SAINT LAWRENCE', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(232, 'COLEGIO PARROQUIAL SAN MIGUEL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(233, 'COLEGIO CLARETIANO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(234, 'COLEGIO CORAZON DE MARIA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(235, 'ESCUELA DE PARVULOS PRINC. FERNANDO DE ARAGON', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(236, 'INSTITUTO MIGUEL LEON PRADO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(237, 'ESCUELA PARTICULAR JAVIERA CARRERA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(238, 'ESC.PART.BENJAMIN VICUNA MACKENNA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(239, 'LICEO INDUSTRIAL DE SAN MIGUEL AGUSTIN ED', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(240, 'LICEO TECNICO A-100 DE SAN MIGUEL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(241, 'LICEO T?CNICO CLOTARIO BLEST RIFFO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(242, 'INSTITUTO COMERCIAL PADRE ALBERTO HURTADO DE PEDRO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(243, 'LICEO POLIVALENTE EUGENIO PEREIRA SALAS', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(244, 'ESCUELA LO VALLEDOR', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(245, 'ESCUELA CIUDAD DE BARCELONA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(246, 'ESCUELA VILLA SUR', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(247, 'COLEGIO PARQUE LAS AMERICAS', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(248, 'C.E.I.A.PEDRO AGUIRRE CERDA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(249, 'ESCUELA ESPECIAL DE ADULTOS', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(250, 'ESCUELA ESPECIAL NTA SENORA DE LAS PENAS', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(251, 'COMPLEJO EDUC.PART.MONSENOR LUIS A.PEREZ', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(252, 'COLEGIO GRACE SCHOOL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(253, 'ESCUELA PARTICULAR KAROL C.DE CRACOVIA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(255, 'COLEGIO DE ADULTOS INSTITUTO LUISA CARDIJIN', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(256, 'COLEGIO METODISTA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(257, 'ESCUELA ESPECIAL PARTICULAR LA ROSA AZUL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(258, 'COLEGIO INSTITUTO ART?STICO DE ESTUDIOS SECUNDARIO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(259, 'COLEGIO LORD COCHRANE', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(260, 'COLEGIO DE ADULTOS YANQUIRAY', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(261, 'LICEO SANTA MARIA EUFRASIA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(262, 'COLEGIO TOMAS MORO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(263, 'ESCUELA PARTICULAR ALVAREZ DE TOLEDO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(264, 'COLEGIO PESTALOZZI', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(265, 'ESCUELA ESPECIAL PARTICULAR SAN CLEMENTE', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(266, 'ESCUELA DE PARVULOS TIA LUCY', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(267, 'CENTRO EDUC. PARTICULAR SAN LUIS', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(268, 'ESC.BAS. PART. COLEGIO HAYDN DE SAN JOAQUIN', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(269, 'ESC. BASICA PART.CENTRO EDUC.AMANECER', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(270, 'ESCUELA BAS. LOS CARRERA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(271, 'ESCUELA PARTICULAR N?333 EL ANGEL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(272, 'INSTITUTO POLIVALENTE SAN MIGUEL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(273, 'COLEGIO INFOCAP', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(274, 'ESCUELA BASICA MARIA AUXILIADORA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(275, 'COLEGIO  PEDRO DE VALDIVIA-AGUSTINAS', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(276, 'COMPLEJO EDUCACIONAL AMERICANO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(277, 'ESCUELA DE PARVULOS MIS CARINOSITOS', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(278, 'ESCUELA PARTIC.DE PARVULOS MI PRINCESITA', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(279, 'ESCUELA MARIA TERESA SCHOOL NUM 2', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(280, 'ESCUELA ESPECIAL PART SANTIAGO SUR', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(281, 'COLEGIO LOS ANGELES SANTIAGO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(282, 'COLEGIO CHILE (ANEXO)', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(284, 'COLEGIO SAN FRANCISCO DE ASIS DE BELEN', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(285, 'ESCUELA ESPECIAL N? 2430 SAN ALBERTO HURTADO', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(286, 'COLEGIO  PAULO  FREIRE  DE  SAN  MIGUEL', '', '', '', '', '', '', '', 0, '0000-00-00', '00:00:00'),
(287, 'COLEGIO IBEROAMERICANO LA PINTANA', 'LA PINTANA, SANTA ROSA', ' +56 2 2542 2102', 'WWW.COLEGIOIBEROAMERICANO.CL/', '', '', '', '', 22, '2019-01-28', '15:09:30'),
(288, 'COLEGIO SAN CARLOS DE ARAGON', ' PUENTE ALTO, REGIÓN METROPOLITANA, CHILE', '+56 2 2268 4132', 'WWW.COLEGIOSANCARLOS.CL', '', '', '', '', 25, '2019-01-31', '09:38:33'),
(289, 'COLEGIO SOL DE CHILE', ' 340,, LEÓN DE LA BARRA 220, LO ESPEJO, REGIÓN METROPOLITANA, CHILE', '+56 2 2854 4936', 'WWW.COLEGIOSOLDECHILE.CL', '', '', '', '', 25, '2019-01-31', '10:24:45'),
(290, 'COLEGIO TERESA DE LOS ANDES ALGARROBO', 'EL MOLLE Nº1390', '35-2481057', '', '', '', '', '', 3, '2019-02-06', '09:24:52'),
(291, 'COLEGIO ALCÁNTARA DE LA CORDILLERA', 'WALKER MARTÍNEZ 1106, LA FLORIDA, REGIÓN METROPOLITANA, CHILE', '+56 2 2255 1657', 'WWW.ALCANTARA-ALICANTE.CL', '', '', '', '', 22, '2019-02-06', '15:55:19'),
(294, 'COLEGIO PEDRO DE VALDIVIA PROVIDENCIA', 'PEDRO DE VALDIVIA 1939', '974598338', 'WWW.CPDV.CL', '', '', '', '', 25, '2019-02-19', '14:18:27'),
(295, 'SAN RAMON', 'VENANCIA LEIVA 1331, LA PINTANA, SAN RAMÓN, REGIÓN METROPOLITANA', '22546 1021', 'WWW.POLITECNICOSANRAMON.CL/', '', '', '', '', 25, '2019-02-22', '10:15:58'),
(296, 'CENTRO POLITÉCNICO PARTICULAR SAN RAMÓN', 'SANTIAGO, LA CISTERNA, REGIÓN METROPOLITANA, CHILE', '+56 2 2596 7809', 'HTTP://POLITECNICOSANRAMON.CL/', '', 'HTTPS://WWW.FACEBOOK.COM/CENTRO.RAMON', '', '', 3, '2019-02-27', '12:20:20'),
(297, 'SANTA MARÍA DE LA CORDILLERA', 'GABRIELA PTE 662, PUENTE ALTO, REGIÓN METROPOLITANA, CHILE', '+56 2 2369 9238', 'HTTP://WWW.CSMC.CL/', '', '', '', '', 3, '2019-02-27', '12:22:25'),
(298, 'SAN MATEO', 'EL MEDIANERO 02081, PUENTE ALTO, REGIÓN METROPOLITANA', '228744791', 'HTTP://WWW.CORPORACIONEDUCSANMATEO.COM/', '', '', '', '', 25, '2019-03-04', '14:09:50'),
(299, 'ALTO PALENA', 'MAIPÚ 85, SANTIAGO, REGIÓN METROPOLITANA', '22682 0443', '', '', '', '', '', 25, '2019-03-11', '09:44:14'),
(300, 'ALTAS  CUMBRES', 'PUERTO VARAS', '(65) 2 515054', 'HTTP://WWW.COLEGIOALTASCUMBRESPV.CL/', '', '', '', '', 25, '2019-03-14', '15:56:32'),
(303, 'COLEGIO SAN JOSE CERRILLOS', 'COLEGIO SAN JOSÉ AV. DON ORIONE 7357', '962250264', 'WWW.COLEGIOSANJOSECERRILLOS.CL', '', '', '', '', 3, '2019-03-15', '10:00:32'),
(304, 'CENTRO EDUCATIVO PEUMAYEN', 'MAULE CL, CALLE BOMBERO GARRIDO 1173-B, CURICÓ, REGIÓN DEL MAULE', '75231 5321', '', '', '', '', '', 25, '2019-03-21', '11:24:54'),
(305, 'LICEO INDUSTRIAL CHILENO ALEMAN', 'CALLE DR JOHOW 357, SANTIAGO, REGIÓN METROPOLITANA', '22752 3913', 'WWW.LICHAN.CL', '', '', '', '', 25, '2019-03-26', '12:20:55'),
(306, 'LICEO EUGENIO MARÍA DE HOSTOS', 'AV. ECHEÑIQUE 8625, LA REINA, REGIÓN METROPOLITANA', '+562291243 00', 'WWW.CORP-LAREINA.CL/LICEO-EUGENIO-MARIA-DE-HOSTOS/', '', '', '', '', 25, '2019-03-27', '16:12:40'),
(307, 'PAULA JARAQUEDAMA ALQUÍZAR', 'LA CONCEPCION 568, PAINE, SANTIAGO', '225499269', 'HTTP://WWW.PAULAJARAQUEMADAPAINE.CL/', 'COLEGIOPJQAPAINE', '', 'COLEGIOPJQAPAINE', '', 25, '2019-03-31', '21:46:41'),
(308, 'SANTA ROSA DEL SUR', 'AV. STA. ROSA 12910, LA PINTANA, REGIÓN METROPOLITANA', '22542 1055', '', '', '', '', '', 25, '2019-03-31', '23:35:45'),
(309, 'COLEGIO PRECIOSA SANGRE', 'EDUARDO CASTILLO VELASCO 2525, ÑUÑOA, REGIÓN METROPOLITANA', '22343 7688', 'WWW.COLEGIOPRECIOSASANGRE.CL', '', '', '', '', 25, '2019-04-01', '10:02:03'),
(310, 'COLEGIO PIAMARTA', 'CURACAVI 51,  ESTACION CENTRAL, SANTIAGO', '', 'HTTPS://PIAMARTINOS.CL/COLEGIOPIAMARTA/', '', '', '', '', 25, '2019-04-01', '10:41:09'),
(311, 'YORK', 'EGAÑA 1350, PEÑALOLEN, STGO', '22715061', 'HTTP://COLEGIOYORK.CL/', '', '', '', '', 25, '2019-04-01', '20:18:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colegio_contacto`
--

CREATE TABLE `colegio_contacto` (
  `ccole_codig` int(11) NOT NULL,
  `coleg_codig` int(11) NOT NULL,
  `perso_codig` int(11) NOT NULL,
  `ccole_tipo` int(11) NOT NULL,
  `ccole_tofic` varchar(20) COLLATE utf8mb4_bin NOT NULL,
  `ccole_tcelu` varchar(20) COLLATE utf8mb4_bin NOT NULL,
  `ccole_curso` varchar(20) COLLATE utf8mb4_bin NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `ccole_fcrea` date NOT NULL,
  `ccole_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Volcado de datos para la tabla `colegio_contacto`
--

INSERT INTO `colegio_contacto` (`ccole_codig`, `coleg_codig`, `perso_codig`, `ccole_tipo`, `ccole_tofic`, `ccole_tcelu`, `ccole_curso`, `usuar_codig`, `ccole_fcrea`, `ccole_hcrea`) VALUES
(1, 287, 35, 1, '', '+56983702264', 'MAMA', 23, '2019-01-28', '15:20:48'),
(2, 287, 36, 2, '', '+56976152650', '4B', 23, '2019-01-28', '15:20:48'),
(3, 291, 40, 1, '', '+56956669021', '4MEDIOA', 27, '2019-02-07', '16:43:33'),
(4, 291, 41, 2, '', '+56973825005', '4MEDIOA', 27, '2019-02-07', '16:43:33'),
(5, 288, 42, 1, '', '+56990074272', 'IV D', 28, '2019-02-08', '16:34:40'),
(6, 288, 43, 2, '', '+56978925590', 'IV D', 28, '2019-02-08', '16:34:40'),
(7, 290, 44, 1, '', '+56990074255', '4 MEDIO', 29, '2019-02-10', '20:04:15'),
(8, 290, 45, 2, '', '+56983436141', '4 MEDIO', 29, '2019-02-10', '20:04:15'),
(11, 2, 52, 1, '', '+567895452', 'HGFR', 34, '2019-02-19', '14:24:35'),
(12, 2, 53, 2, '', '+563', '456', 34, '2019-02-19', '14:24:35'),
(13, 294, 49, 1, '', '+56974598338', 'IV', 33, '2019-02-19', '14:44:47'),
(14, 294, 54, 2, '', '+56994995503', 'IV', 33, '2019-02-19', '14:44:47'),
(15, 296, 57, 1, '', '+56945820617', 'CURSO	4 QUÍMICA INDU', 37, '2019-02-27', '12:23:52'),
(16, 296, 59, 2, '', '+56986516630', 'CURSO	4 QUÍMICA INDU', 37, '2019-02-27', '12:23:52'),
(17, 297, 58, 1, '', '+56950789821', ' 3ERO MEDIO B ', 38, '2019-02-27', '12:26:46'),
(18, 297, 60, 2, '', '+56950166580', ' 3ERO MEDIO B ', 38, '2019-02-27', '12:26:46'),
(19, 57, 62, 1, '', '+56989803693', '4A', 40, '2019-03-12', '18:49:38'),
(20, 57, 65, 2, '', '+569', '4A', 40, '2019-03-12', '18:49:38'),
(23, 303, 70, 1, '', '+56962250264', '4 ', 47, '2019-03-15', '10:01:58'),
(24, 303, 75, 2, '', '+56956600662', '4', 47, '2019-03-15', '10:01:58'),
(25, 300, 71, 1, '', '+56995559987', '4° MEDIO', 48, '2019-03-18', '16:11:08'),
(26, 300, 76, 2, '', '+56941793578', '4° MEDIO', 48, '2019-03-18', '16:11:08'),
(27, 304, 74, 1, '', '+560977436745', '4 MEDIO', 49, '2019-03-21', '12:51:48'),
(28, 304, 77, 2, '', '+56976981525', '4 MEDIO', 49, '2019-03-21', '12:51:48'),
(29, 306, 78, 1, '', '+56965856960', 'IV°A', 50, '2019-03-27', '16:38:12'),
(30, 306, 80, 2, '', '+56965817960', 'IV°A', 50, '2019-03-27', '16:38:12'),
(31, 308, 83, 1, '', '+56984724517', '4TO MEDIO C 2019', 52, '2019-04-01', '11:37:57'),
(32, 308, 84, 2, '', '+56988881896', '4TO MEDIO C 2019', 52, '2019-04-01', '11:37:57'),
(33, 309, 85, 1, '', '+56962062756', '4 MEDIO', 53, '2019-04-01', '17:22:31'),
(34, 309, 87, 2, '', '+56966056963', '4 MEDIO', 53, '2019-04-01', '17:22:31'),
(35, 305, 88, 1, '', '+56942688794', '4° MEDIO', 54, '2019-04-01', '20:36:14'),
(36, 305, 91, 2, '', '+56993107070', '4°MEDIO', 54, '2019-04-01', '20:36:14'),
(37, 62, 89, 1, '', '+56965475529', 'IV°B', 55, '2019-04-01', '21:45:37'),
(38, 62, 92, 2, '', '+56987517876', 'IV°B', 55, '2019-04-01', '21:45:37'),
(39, 311, 94, 1, '', '+56993935602', 'CUARTO MEDIO F', 57, '2019-04-02', '16:09:15'),
(40, 311, 95, 2, '', '+56956720075', 'CUARTO MEDIO F', 57, '2019-04-02', '16:09:15'),
(41, 298, 61, 1, '', '+56954725434', '4TO A', 39, '2019-04-02', '21:40:03'),
(42, 298, 97, 2, '', '+56954725434', '4TO A', 39, '2019-04-02', '21:40:03'),
(43, 307, 93, 1, '', '+56932426108', 'IV MEDIO A', 56, '2019-04-02', '23:28:15'),
(44, 307, 98, 2, '', '+56999108314', 'IV MEDIO A', 56, '2019-04-02', '23:28:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `depositos`
--

CREATE TABLE `depositos` (
  `depos_codig` bigint(20) UNSIGNED NOT NULL,
  `depos_solic` int(11) NOT NULL,
  `depos_ndepo` varchar(20) NOT NULL,
  `depos_fdepo` date NOT NULL,
  `depos_monto` decimal(10,0) NOT NULL,
  `depos_fregi` date NOT NULL,
  `depos_statu` int(11) NOT NULL,
  `depos_obser` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `depositos`
--

INSERT INTO `depositos` (`depos_codig`, `depos_solic`, `depos_ndepo`, `depos_fdepo`, `depos_monto`, `depos_fregi`, `depos_statu`, `depos_obser`) VALUES
(11, 100063, '35506842', '2017-03-09', '60000', '2017-03-09', 1, ''),
(12, 100079, '00436932', '2017-03-09', '60000', '2017-03-09', 1, 'Muchas Gracias.'),
(13, 100081, '33457107', '2107-03-09', '60000', '2017-03-09', 1, 'Muchas Gracias.'),
(15, 100082, '52300482891', '2017-03-09', '10000', '2017-03-09', 1, 'confirmado'),
(16, 100083, '52300483040', '2017-03-09', '10000', '2017-03-09', 1, 'confirmado'),
(17, 100084, '52300483141', '2017-03-09', '10000', '2017-03-09', 2, '1'),
(19, 100076, '52300711141', '2017-03-12', '60000', '2017-03-12', 1, 'Muchas Gracias.'),
(20, 100069, '0839347437', '2017-03-13', '60000', '2017-03-13', 1, 'Muchas Gracias.'),
(21, 100093, '0003130893', '2017-03-14', '60000', '2017-03-14', 1, 'Muchas Gracias.'),
(22, 100109, '0840536777', '2017-03-14', '60000', '2017-03-14', 1, 'Muchas Gracias.'),
(23, 100118, '0841142967', '2017-03-15', '60000', '2017-03-15', 1, 'Muchas Gracias.'),
(36, 100071, '52300399128', '2017-03-16', '60000', '2017-03-16', 0, NULL),
(41, 100145, '0003148346', '2017-03-17', '60000', '2017-03-17', 1, 'Muchas Gracias.'),
(42, 100064, '0843479337', '2017-03-17', '60000', '2017-03-17', 1, 'Muchas Gracias.'),
(43, 100137, '0843649432', '2017-03-17', '10000', '2017-03-17', 1, 'Muchas Gracias.'),
(44, 100136, '0843640359', '2017-03-17', '10000', '2017-03-17', 1, 'Muchas Gracias.'),
(45, 100117, '0844979695', '2017-03-19', '60000', '2017-03-19', 1, 'Muchas Gracias.'),
(46, 100091, '0000101924', '2017-03-20', '60000', '2017-03-20', 1, 'Muchas Gracias.'),
(47, 100147, '0000991596', '2017-03-21', '600', '2017-03-21', 1, 'Muchas Gracias.'),
(48, 100156, '00846642936', '2016-03-21', '10000', '2017-03-21', 1, 'Muchas Gracias.'),
(49, 100138, '11312880182', '2017-03-21', '5000', '2017-03-21', 1, 'Muchas gracias por su participación.'),
(51, 100132, '0847848894', '2017-03-22', '60000', '2017-03-22', 1, 'Muchas Gracias por su participación.'),
(55, 100150, '52300283107', '2017-03-23', '60000', '2017-03-23', 1, 'Muchas Gracias por su participación.'),
(56, 100101, 'TE0004066569', '2017-03-23', '60000', '2017-03-23', 1, 'Muchas Gracias por su inscripción.'),
(58, 100212, '0064142197', '2017-03-23', '150000', '2017-03-23', 1, 'Muchas gracias por su participación.'),
(59, 100213, '0064142197', '2017-03-23', '150000', '2017-03-23', 1, 'Muchas gracias por su participación.'),
(60, 100214, '0064142197', '2017-03-23', '150000', '2017-03-23', 1, 'Muchas gracias por su participación.'),
(61, 100215, '0064142197', '2017-03-23', '150000', '2017-03-23', 1, 'Muchas gracias por su participación.'),
(62, 100216, '0064142197', '2017-03-23', '150000', '2017-03-23', 1, 'Muchas gracias por su participación.'),
(63, 100217, '0064142197', '2017-03-23', '150000', '2017-03-23', 1, 'Muchas gracias por su participación.'),
(64, 100218, '0064142197', '2017-03-23', '150000', '2017-03-23', 1, 'Muchas gracias por su participación.'),
(65, 100219, '0064142197', '2017-03-23', '150000', '2017-03-23', 1, 'Muchas gracias por su participación.'),
(66, 100220, '0064142197', '2017-03-23', '150000', '2017-03-23', 1, 'Muchas Gracias'),
(67, 100221, '0064142197', '2017-03-23', '150000', '2017-03-23', 1, 'Muchas gracias por su participación.'),
(68, 100103, '0079309515', '2017-03-23', '25000', '2017-03-23', 0, NULL),
(69, 100098, '0079307487', '2017-03-23', '25000', '2017-03-23', 0, NULL),
(70, 100230, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por participar.'),
(71, 100066, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por participar.'),
(72, 100116, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por participar.'),
(73, 100108, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por participar.'),
(74, 100107, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por participar.'),
(75, 100106, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por participar.'),
(76, 100229, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por participar.'),
(77, 100165, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por participar.'),
(78, 100164, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por participar.'),
(79, 100163, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(80, 100161, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(81, 100162, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(82, 100185, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(83, 100182, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(84, 100179, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(85, 100192, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(86, 100187, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(87, 100188, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(88, 100190, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(89, 100189, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(90, 100224, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(91, 100225, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(92, 100226, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(93, 100223, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(94, 100222, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(95, 100228, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(96, 100210, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(97, 100104, '0097313805', '2017-03-23', '10000', '2017-03-24', 0, NULL),
(98, 100175, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(99, 100177, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(100, 100178, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(101, 100180, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(102, 100181, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(103, 100183, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(104, 100184, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(105, 100186, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(106, 100191, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(107, 100193, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(108, 100194, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(109, 100195, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(110, 100196, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(111, 100197, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(112, 100198, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(113, 100199, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(114, 100200, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(115, 100201, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(116, 100168, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(117, 100169, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(118, 100170, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(119, 100171, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(120, 100172, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(121, 100174, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(122, 100202, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(123, 100209, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(124, 100208, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(125, 100207, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(126, 100206, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(127, 100176, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(128, 100203, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(129, 100204, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(130, 100205, '0', '2017-03-24', '0', '2017-03-24', 3, 'Muchas gracias por su participación.'),
(131, 100167, '0849885981', '2017-03-24', '10', '2017-03-24', 1, 'Muchas gracias por su participación.'),
(132, 100166, '0849895153', '2017-03-24', '10', '2017-03-24', 1, 'Muchas gracias por su participación.'),
(133, 100275, '0097618618', '2017-03-25', '10000', '2017-03-25', 0, NULL),
(134, 100283, '0850317146', '2017-03-24', '10000', '2017-03-25', 1, 'Muchas gracias por su participación.'),
(135, 100266, '0097530346', '2017-03-24', '10000', '2017-03-25', 0, NULL),
(136, 100267, '0097530346', '2017-03-24', '10000', '2017-03-25', 0, NULL),
(137, 100268, '0074020357', '2017-03-22', '10000', '2017-03-25', 0, NULL),
(138, 100269, '0000065904', '2017-03-22', '10000', '2017-03-25', 0, NULL),
(139, 100286, '1113395810', '2017-03-24', '10000', '2017-03-25', 1, 'Muchas gracias por su participación.'),
(140, 100287, '1113395810', '2017-03-24', '10000', '2017-03-25', 1, 'Muchas gracias por su participación.'),
(141, 100288, '1113395810', '2017-03-24', '10000', '2017-03-25', 1, 'Muchas gracias por su participación.'),
(142, 100289, '1113395810', '2017-03-24', '10000', '2017-03-25', 1, 'Muchas gracias por su participación.'),
(143, 100290, '1113395810', '2017-03-24', '10000', '2017-03-25', 1, 'Muchas gracias por su participación.'),
(144, 100291, '1113395810', '2017-03-24', '10000', '2017-03-25', 1, 'Muchas gracias por su participación.'),
(145, 100252, '0850168061', '2017-03-24', '10000', '2017-03-25', 1, 'Muchas gracias por su participación.'),
(146, 100292, '1113395810', '2017-03-24', '10000', '2017-03-25', 1, 'Muchas gracias por su participación.'),
(147, 100251, '0850168061', '2017-03-24', '10000', '2017-03-25', 1, 'Muchas gracias por su participación.'),
(148, 100250, '0850168061', '2017-03-24', '10000', '2017-03-25', 1, 'Muchas gracias por su participación.'),
(149, 100293, '1113395810', '2017-03-24', '10000', '2017-03-25', 1, 'Muchas gracias por su participación.'),
(150, 100249, '0850168061', '2017-03-24', '10000', '2017-03-25', 1, 'Muchas gracias por su participación.'),
(151, 100280, '0097433172', '2017-03-24', '10000', '2017-03-25', 0, NULL),
(152, 100248, '0850168061', '2017-03-24', '10000', '2017-03-25', 1, 'Muchas gracias por su participación.'),
(153, 100247, '0850168061', '2017-03-24', '10000', '2017-03-25', 1, 'Muchas gracias por su participación.'),
(154, 100246, '0850168061', '2017-03-24', '10000', '2017-03-25', 1, 'Muchas gracias por su participación.'),
(155, 100299, '00851000132', '2017-03-26', '10000', '2017-03-26', 1, 'Muchas Gracias'),
(156, 100173, '1632342595', '2017-03-24', '10000', '2017-03-27', 1, 'Muchas gracias por su participación.'),
(157, 100227, '0', '2017-03-27', '0', '2017-03-27', 3, 'Muchas gracias, la participacion sera cambiada para el clasificatorio.'),
(158, 100092, '0', '2017-03-27', '0', '2017-03-27', 3, 'Muchas gracias por su participación.'),
(159, 100160, '0', '2017-03-27', '0', '2017-03-27', 3, 'Muchas gracias por su participación'),
(160, 100241, '0', '2017-03-27', '0', '2017-03-27', 3, 'Muchas gracias por su participación'),
(161, 100244, '0', '2017-03-27', '0', '2017-03-27', 3, 'Muchas gracias por su participación'),
(162, 100243, '0', '2017-03-27', '0', '2017-03-27', 3, 'Muchas gracias por su participación'),
(163, 100253, '0', '2017-03-27', '0', '2017-03-27', 3, 'Muchas gracias por su participación'),
(164, 100242, '0', '2017-03-27', '0', '2017-03-27', 3, 'Muchas gracias por su participación'),
(165, 100254, '0', '2017-03-27', '0', '2017-03-27', 3, 'Muchas gracias por su participación'),
(166, 100240, '0', '2017-03-27', '0', '2017-03-27', 3, 'Muchas gracias por su participación'),
(167, 100234, '0', '2017-03-27', '0', '2017-03-27', 3, 'Muchas gracias por su participación'),
(168, 100239, '0', '2017-03-27', '0', '2017-03-27', 3, 'Muchas gracias por su participación'),
(169, 100238, '0', '2017-03-27', '0', '2017-03-27', 3, 'Muchas gracias por su participación'),
(170, 100237, '0', '2017-03-27', '0', '2017-03-27', 3, 'Muchas gracias por su participación'),
(171, 100236, '0', '2017-03-27', '0', '2017-03-27', 3, 'Muchas gracias por su participación'),
(172, 100235, '0', '2017-03-27', '0', '2017-03-27', 3, 'Muchas gracias por su participación'),
(173, 100298, '0', '2017-03-27', '0', '2017-03-27', 3, 'Muchas gracias por su participación'),
(174, 100297, '0', '2017-03-27', '0', '2017-03-27', 3, 'Gracias por su participación.'),
(175, 100296, '0', '2017-03-27', '0', '2017-03-27', 3, 'Gracias por su participación.'),
(176, 100295, '0', '2017-03-27', '0', '2017-03-27', 3, 'Gracias por su participación.'),
(177, 100294, '0', '2017-03-27', '0', '2017-03-27', 3, 'Gracias por su participación.'),
(178, 100301, '0', '2017-03-27', '0', '2017-03-27', 3, 'Gracias por su participación.'),
(179, 100159, '0', '2017-03-27', '0', '2017-03-27', 3, 'Gracias por su participación.'),
(180, 100264, '0', '2017-03-27', '0', '2017-03-27', 3, 'Gracias por su participación.'),
(181, 100265, '0', '2017-03-27', '0', '2017-03-27', 3, 'Gracias por su participación.'),
(182, 100270, '0', '2017-03-27', '0', '2017-03-27', 3, 'Gracias por su participación.'),
(183, 100272, '0', '2017-03-27', '0', '2017-03-27', 3, 'Gracias por su participación.'),
(184, 100273, '0', '2017-03-27', '0', '2017-03-27', 3, 'Gracias por su participación.'),
(185, 100300, '0', '2017-03-27', '0', '2017-03-27', 3, 'Gracias por su participación.'),
(186, 100271, '0', '2017-03-27', '0', '2017-03-27', 3, 'Gracias por su participación.'),
(187, 100263, '0', '2017-03-27', '0', '2017-03-27', 3, 'Gracias por su participación.'),
(188, 100262, '0', '2017-03-27', '0', '2017-03-27', 3, 'Gracias por su participación.'),
(189, 100261, '0', '2017-03-27', '0', '2017-03-27', 3, 'Gracias por su participación.'),
(190, 100260, '0', '2017-03-27', '0', '2017-03-27', 3, 'Gracias por su participación.'),
(191, 100259, '0', '2017-03-27', '0', '2017-03-27', 3, 'Gracias por su participación.'),
(192, 100258, '0', '2017-03-27', '0', '2017-03-27', 3, 'Gracias por su participación.'),
(193, 100257, '0', '2017-03-27', '0', '2017-03-27', 3, 'Gracias por su participación.'),
(194, 100256, '0', '2017-03-27', '0', '2017-03-27', 3, 'Gracias por su participación.'),
(195, 100255, '0', '2017-03-27', '0', '2017-03-27', 3, 'Gracias por su participación.'),
(196, 100305, '0000739936', '2017-03-27', '60000', '2017-03-27', 0, NULL),
(197, 100308, '52300691122', '2017-03-27', '60000', '2017-03-27', 0, NULL),
(198, 100304, '0851806299', '2017-03-27', '60000', '2017-03-27', 0, NULL),
(199, 100094, '52300486438', '2017-03-24', '600', '2017-03-27', 0, NULL),
(200, 100096, '0', '2017-03-27', '0', '2017-03-27', 3, 'Gracias por su participacion'),
(201, 100233, '0', '2017-03-27', '0', '2017-03-27', 3, 'Gracias por su participacion'),
(202, 100303, '0097739795', '2017-03-27', '10000', '2017-03-29', 0, NULL),
(203, 100302, '0097736370', '2017-03-27', '10000', '2017-03-29', 0, NULL),
(205, 100084, '52300483141', '2017-03-09', '10000', '2017-03-29', 1, 'gracias'),
(206, 123457, '0', '2017-03-30', '0', '2017-03-30', 3, 'muchas'),
(207, 123456, '0', '2017-03-30', '0', '2017-03-30', 3, 'gracias');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `depositos_validar`
--

CREATE TABLE `depositos_validar` (
  `valid_codig` bigint(20) UNSIGNED NOT NULL,
  `depos_solic` int(11) NOT NULL,
  `estva_codig` int(11) NOT NULL,
  `valid_obser` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documento_tipo`
--

CREATE TABLE `documento_tipo` (
  `tdocu_codig` int(11) NOT NULL,
  `tdocu_descr` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `documento_tipo`
--

INSERT INTO `documento_tipo` (`tdocu_codig`, `tdocu_descr`) VALUES
(2, 'E'),
(5, 'G'),
(4, 'J'),
(3, 'P'),
(1, 'V');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `egresos`
--

CREATE TABLE `egresos` (
  `egres_codig` int(11) NOT NULL,
  `egres_motiv` varchar(20) NOT NULL,
  `banco_codig` int(11) NOT NULL,
  `egres_nfact` int(11) NOT NULL,
  `egres_refer` varchar(20) NOT NULL,
  `egres_monto` double NOT NULL,
  `egres_fegre` date NOT NULL,
  `egres_obser` text NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `egres_fcrea` date NOT NULL,
  `egres_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `egresos`
--

INSERT INTO `egresos` (`egres_codig`, `egres_motiv`, `banco_codig`, `egres_nfact`, `egres_refer`, `egres_monto`, `egres_fegre`, `egres_obser`, `usuar_codig`, `egres_fcrea`, `egres_hcrea`) VALUES
(2, 'PAGO A PROVEEDOR', 1, 100, '0', 10000000, '2018-02-04', '', 3, '2018-02-04', '12:11:42'),
(3, 'PAGO A EMPLEADOS', 1, 0, '100000', 1000000, '2018-02-22', '', 3, '2018-02-04', '12:12:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa_tipo`
--

CREATE TABLE `empresa_tipo` (
  `etipo_codig` int(11) NOT NULL,
  `etipo_descr` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `empresa_tipo`
--

INSERT INTO `empresa_tipo` (`etipo_codig`, `etipo_descr`) VALUES
(2, 'TIPO EMPRESA 1'),
(3, 'VENDEDOR');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `estad_codig` int(11) NOT NULL,
  `paise_codig` int(11) NOT NULL,
  `estad_descr` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`estad_codig`, `paise_codig`, `estad_descr`) VALUES
(1, 232, 'AMAZONAS'),
(2, 232, 'ANZOATEGUI'),
(3, 232, 'APURE'),
(4, 232, 'ARAGUA'),
(5, 232, 'BARINAS'),
(6, 232, 'BOLIVAR'),
(7, 232, 'CARABOBO'),
(8, 232, 'COJEDES'),
(9, 232, 'DELTA AMACURO'),
(10, 232, 'FALCON'),
(11, 232, 'GUARICO'),
(12, 232, 'LARA'),
(13, 232, 'MERIDA'),
(14, 232, 'MIRANDA'),
(15, 232, 'MONAGAS'),
(16, 232, 'NUEVA ESPARTA'),
(17, 232, 'PORTUGUESA'),
(18, 232, 'SUCRE'),
(19, 232, 'TACHIRA'),
(20, 232, 'TRUJILLO'),
(21, 232, 'VARGAS'),
(22, 232, 'YARACUY'),
(23, 232, 'ZULIA'),
(24, 232, 'DISTRITO CAPITAL'),
(25, 232, 'DEPENDENCIAS FEDERALES');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_civil`
--

CREATE TABLE `estado_civil` (
  `ecivi_codig` int(11) NOT NULL,
  `ecivi_descr` varchar(45) DEFAULT NULL,
  `ecivi_value` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `estado_civil`
--

INSERT INTO `estado_civil` (`ecivi_codig`, `ecivi_descr`, `ecivi_value`) VALUES
(1, 'SOLTERO', 'S'),
(2, 'CASADO', 'C'),
(3, 'VIUDO', 'V'),
(4, 'DIVORCIADO', 'D'),
(5, 'UNION ESTABLE DE HECHO', 'U');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estatus`
--

CREATE TABLE `estatus` (
  `estat_codig` int(11) NOT NULL,
  `estat_descr` varchar(100) NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `estat_fcrea` date NOT NULL,
  `estat_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `estatus`
--

INSERT INTO `estatus` (`estat_codig`, `estat_descr`, `usuar_codig`, `estat_fcrea`, `estat_hcrea`) VALUES
(1, 'INICIO', 3, '2019-06-24', '22:09:24'),
(2, 'PRE-REGISTRO', 3, '2019-06-24', '22:09:25'),
(3, 'DOCUMENTOS CARGADOS', 3, '2019-06-24', '22:10:55'),
(99, 'ANULADO', 3, '2019-06-25', '23:49:31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estatus_validar`
--

CREATE TABLE `estatus_validar` (
  `estval_codig` bigint(20) UNSIGNED NOT NULL,
  `estva_descr` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `factu_codig` int(11) NOT NULL,
  `factu_nfact` int(11) NOT NULL,
  `factu_ncont` varchar(255) NOT NULL,
  `traba_codig` int(11) NOT NULL,
  `clien_codig` int(11) NOT NULL,
  `tvent_codig` int(11) NOT NULL,
  `factu_femis` date NOT NULL,
  `factu_desc` double NOT NULL,
  `factu_monto` double NOT NULL,
  `factu_impue` double NOT NULL,
  `factu_comis` double NOT NULL,
  `iva_codig` int(11) NOT NULL,
  `ptipo_codig` int(11) NOT NULL,
  `efact_codig` int(11) NOT NULL,
  `efpag_codig` int(11) NOT NULL,
  `banco_codig` int(11) NOT NULL,
  `factu_refer` text NOT NULL,
  `factu_obser` text NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `factu_fcrea` date NOT NULL,
  `factu_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`factu_codig`, `factu_nfact`, `factu_ncont`, `traba_codig`, `clien_codig`, `tvent_codig`, `factu_femis`, `factu_desc`, `factu_monto`, `factu_impue`, `factu_comis`, `iva_codig`, `ptipo_codig`, `efact_codig`, `efpag_codig`, `banco_codig`, `factu_refer`, `factu_obser`, `usuar_codig`, `factu_fcrea`, `factu_hcrea`) VALUES
(29, 1, '1-00', 4, 4, 1, '2018-08-27', 0, 48200, 7712, 24100, 0, 5, 1, 1, 1, '1', 'NINGUNA', 3, '2018-08-27', '23:38:08'),
(30, 2, '2-00', 4, 4, 1, '2018-08-24', 1000, 15000, 2400, 7500, 2, 5, 1, 1, 35, '2', 'NINGUNO', 3, '2018-08-28', '04:13:07'),
(33, 3, '3-00', 6, 12, 1, '2018-08-27', 0, 54100, 8656, 541, 0, 4, 1, 1, 36, '3', 'NINGUNA', 3, '2018-08-28', '05:23:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura_estatu`
--

CREATE TABLE `factura_estatu` (
  `efact_codig` int(11) NOT NULL,
  `efact_descr` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `factura_estatu`
--

INSERT INTO `factura_estatu` (`efact_codig`, `efact_descr`) VALUES
(3, 'ANULADA'),
(2, 'APROBADA'),
(1, 'EMITIDA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura_estatu_pago`
--

CREATE TABLE `factura_estatu_pago` (
  `efpag_codig` int(11) NOT NULL,
  `efpag_descr` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `factura_estatu_pago`
--

INSERT INTO `factura_estatu_pago` (`efpag_codig`, `efpag_descr`) VALUES
(1, 'PAGO'),
(2, 'PENDIENTE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura_producto`
--

CREATE TABLE `factura_producto` (
  `fprod_codig` int(11) NOT NULL,
  `factu_codig` int(11) NOT NULL,
  `tprod_codig` int(11) NOT NULL,
  `inven_codig` int(11) NOT NULL,
  `fprod_canti` int(11) NOT NULL,
  `fprod_preci` double NOT NULL,
  `fprod_monto` double NOT NULL,
  `fprod_obser` text NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `fprod_fcrea` date NOT NULL,
  `fprod_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `factura_producto`
--

INSERT INTO `factura_producto` (`fprod_codig`, `factu_codig`, `tprod_codig`, `inven_codig`, `fprod_canti`, `fprod_preci`, `fprod_monto`, `fprod_obser`, `usuar_codig`, `fprod_fcrea`, `fprod_hcrea`) VALUES
(33, 29, 1, 4, 1, 15000, 15000, '', 3, '2018-08-27', '23:38:08'),
(34, 29, 1, 5, 1, 25000, 25000, '', 3, '2018-08-27', '23:38:08'),
(35, 29, 1, 6, 2, 4100, 8200, '', 3, '2018-08-27', '23:38:08'),
(36, 30, 1, 4, 1, 15000, 15000, '', 3, '2018-08-28', '04:13:07'),
(37, 33, 1, 4, 1, 15000, 15000, '', 3, '2018-08-28', '05:23:26'),
(38, 33, 1, 5, 1, 25000, 25000, '', 3, '2018-08-28', '05:23:26'),
(39, 33, 1, 6, 1, 4100, 4100, '', 3, '2018-08-28', '05:23:26'),
(40, 33, 2, 1, 1, 10000, 10000, '', 3, '2018-08-28', '05:23:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `genero`
--

CREATE TABLE `genero` (
  `gener_codig` int(11) NOT NULL,
  `gener_descr` varchar(20) NOT NULL,
  `gener_value` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `genero`
--

INSERT INTO `genero` (`gener_codig`, `gener_descr`, `gener_value`) VALUES
(1, 'MASCULINO', 'M'),
(2, 'FEMENINO', 'F');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `impuesto_iva`
--

CREATE TABLE `impuesto_iva` (
  `iva_codig` int(11) NOT NULL,
  `iva_descr` varchar(255) COLLATE utf8_bin NOT NULL,
  `iva_value` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `impuesto_iva`
--

INSERT INTO `impuesto_iva` (`iva_codig`, `iva_descr`, `iva_value`) VALUES
(1, 'IVA 12%', 12),
(2, 'IVA 19%', 19);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario`
--

CREATE TABLE `inventario` (
  `inven_codig` int(11) NOT NULL,
  `inven_cprod` varchar(45) DEFAULT NULL,
  `inven_descr` varchar(45) DEFAULT NULL,
  `inven_punid` varchar(45) DEFAULT NULL,
  `inven_canti` int(11) DEFAULT NULL,
  `moned_codig` int(11) NOT NULL,
  `tunid_codig` int(11) NOT NULL,
  `inven_obser` varchar(500) DEFAULT NULL,
  `usuar_codig` int(11) NOT NULL,
  `inven_fcrea` date NOT NULL,
  `inven_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `inventario`
--

INSERT INTO `inventario` (`inven_codig`, `inven_cprod`, `inven_descr`, `inven_punid`, `inven_canti`, `moned_codig`, `tunid_codig`, `inven_obser`, `usuar_codig`, `inven_fcrea`, `inven_hcrea`) VALUES
(4, '000000000000000000001', 'PRODUCTO 1', '15000', 11, 1, 1, 'QWQW', 3, '2018-08-27', '19:31:02'),
(5, '000000000000000000002', 'PRODUCTO2', '25000', 11, 1, 1, '', 3, '2018-08-27', '22:42:01'),
(6, '88888888', 'TINTE ', '41', 11, 2, 1, '', 3, '2018-08-27', '23:37:33');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario_entrada`
--

CREATE TABLE `inventario_entrada` (
  `ientr_codig` int(11) NOT NULL,
  `prove_codig` int(11) NOT NULL,
  `ientr_nfact` varchar(20) COLLATE utf8_bin NOT NULL,
  `ientr_ncont` varchar(20) COLLATE utf8_bin NOT NULL,
  `ientr_femis` date NOT NULL,
  `ientr_monto` double NOT NULL,
  `ientr_obser` text COLLATE utf8_bin NOT NULL,
  `ieest_codig` int(11) NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `ientr_fcrea` int(11) NOT NULL,
  `ientr_hcrea` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `inventario_entrada`
--

INSERT INTO `inventario_entrada` (`ientr_codig`, `prove_codig`, `ientr_nfact`, `ientr_ncont`, `ientr_femis`, `ientr_monto`, `ientr_obser`, `ieest_codig`, `usuar_codig`, `ientr_fcrea`, `ientr_hcrea`) VALUES
(6, 19, '10', '10-00', '2018-08-27', 100000, 'A', 2, 3, 2018, 19),
(7, 19, '12', '12-00', '2018-08-27', 550000, 'PRUEBA 2', 2, 3, 2018, 19);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario_entrada_estatus`
--

CREATE TABLE `inventario_entrada_estatus` (
  `ieest_codig` int(11) NOT NULL,
  `ieest_descr` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `inventario_entrada_estatus`
--

INSERT INTO `inventario_entrada_estatus` (`ieest_codig`, `ieest_descr`) VALUES
(1, 'EMITIDA'),
(2, 'APROBADA'),
(3, 'ANULADA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario_entrada_producto`
--

CREATE TABLE `inventario_entrada_producto` (
  `iepro_codig` int(11) NOT NULL,
  `ientr_codig` int(11) NOT NULL,
  `inven_codig` int(11) NOT NULL,
  `iepro_canti` int(11) NOT NULL,
  `iepro_preci` double NOT NULL,
  `iepro_monto` double NOT NULL,
  `iepro_obser` text NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `iepro_fcrea` date NOT NULL,
  `iepro_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `inventario_entrada_producto`
--

INSERT INTO `inventario_entrada_producto` (`iepro_codig`, `ientr_codig`, `inven_codig`, `iepro_canti`, `iepro_preci`, `iepro_monto`, `iepro_obser`, `usuar_codig`, `iepro_fcrea`, `iepro_hcrea`) VALUES
(34, 6, 4, 10, 10000, 100000, '', 3, '2018-08-27', '19:42:04'),
(35, 7, 5, 10, 20000, 200000, '', 3, '2018-08-27', '19:59:37'),
(36, 7, 6, 10, 35000, 350000, '', 3, '2018-08-27', '20:02:20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario_salida`
--

CREATE TABLE `inventario_salida` (
  `isali_codig` int(11) NOT NULL,
  `traba_codig` int(11) NOT NULL,
  `isali_femis` date NOT NULL,
  `isali_monto` double NOT NULL,
  `isali_obser` text COLLATE utf8_bin NOT NULL,
  `isest_codig` int(11) NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `isali_fcrea` int(11) NOT NULL,
  `isali_hcrea` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `inventario_salida`
--

INSERT INTO `inventario_salida` (`isali_codig`, `traba_codig`, `isali_femis`, `isali_monto`, `isali_obser`, `isest_codig`, `usuar_codig`, `isali_fcrea`, `isali_hcrea`) VALUES
(8, 4, '2018-08-27', 10000, 'ENTREGA DE MERCANCIA PARA TRABAJAR', 1, 3, 2018, 21),
(9, 4, '2018-08-27', 10000, 'ENTREGA DE MERCANCIA PARA TRABAJAR', 1, 3, 2018, 21);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario_salida_estatus`
--

CREATE TABLE `inventario_salida_estatus` (
  `isest_codig` int(11) NOT NULL,
  `isest_descr` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `inventario_salida_estatus`
--

INSERT INTO `inventario_salida_estatus` (`isest_codig`, `isest_descr`) VALUES
(1, 'EMITIDA'),
(2, 'APROBADA'),
(3, 'ANULADA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario_salida_producto`
--

CREATE TABLE `inventario_salida_producto` (
  `ispro_codig` int(11) NOT NULL,
  `isali_codig` int(11) NOT NULL,
  `inven_codig` int(11) NOT NULL,
  `ispro_canti` int(11) NOT NULL,
  `ispro_preci` double NOT NULL,
  `ispro_monto` double NOT NULL,
  `ispro_obser` text NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `ispro_fcrea` date NOT NULL,
  `ispro_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `inventario_salida_producto`
--

INSERT INTO `inventario_salida_producto` (`ispro_codig`, `isali_codig`, `inven_codig`, `ispro_canti`, `ispro_preci`, `ispro_monto`, `ispro_obser`, `usuar_codig`, `ispro_fcrea`, `ispro_hcrea`) VALUES
(39, 8, 4, 1, 10000, 10000, '', 3, '2018-08-27', '21:40:04'),
(40, 9, 4, 1, 10000, 10000, '', 3, '2018-08-27', '21:40:11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `meses`
--

CREATE TABLE `meses` (
  `meses_codig` int(11) NOT NULL,
  `meses_descr` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `meses`
--

INSERT INTO `meses` (`meses_codig`, `meses_descr`) VALUES
(4, 'ABRIL'),
(8, 'AGOSTO'),
(12, 'DICIEMBRE'),
(1, 'ENERO'),
(2, 'FEBRERO'),
(7, 'JULIO'),
(6, 'JUNIO'),
(3, 'MARZO'),
(5, 'MAYO'),
(11, 'NOVIEMBRE'),
(10, 'OCTUBRE'),
(9, 'SEPTIEMBRE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulos`
--

CREATE TABLE `modulos` (
  `modul_codig` int(10) NOT NULL,
  `modul_padre` int(10) NOT NULL,
  `modul_descr` varchar(100) NOT NULL,
  `modul_ruta` varchar(400) NOT NULL,
  `modul_orden` int(11) NOT NULL,
  `modul_bborr` tinyint(1) NOT NULL,
  `modul_bpadr` tinyint(1) NOT NULL,
  `modul_obser` varchar(100) NOT NULL,
  `modul_icono` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `modulos`
--

INSERT INTO `modulos` (`modul_codig`, `modul_padre`, `modul_descr`, `modul_ruta`, `modul_orden`, `modul_bborr`, `modul_bpadr`, `modul_obser`, `modul_icono`) VALUES
(1, 0, 'Inicio', '/site/inicio', 1, 0, 0, '', 'fa fa-home'),
(2, 0, 'Registro', '', 2, 0, 1, '', 'fa fa-edit'),
(3, 2, 'Acceso', '/registro/acceso/listado', 3, 0, 0, '', 'fa fa-sign-in'),
(4, 2, 'Datos', '/registro/datos/listado', 4, 0, 0, '', 'fa fa-edit'),
(10, 0, 'Registro y Documentación', '/registro/solicitudes/listado', 2, 0, 1, '', 'fa fa-edit'),
(21, 10, 'Solicitudes', '/registro/solicitudes/listado', 1, 0, 0, '', 'fa fa-edit'),
(30, 0, 'Tramitación', '', 30, 0, 1, '', 'fa fa-bank'),
(31, 30, 'Certificación del Banco', '/tramitacion/solicitudes/listado', 1, 0, 0, '', 'fa fa-bank'),
(32, 30, 'Cotización', '/tramitacion/cotizacion/listado', 2, 0, 0, '', 'fa fa-bank'),
(33, 30, 'Pagos', '/tramitacion/pagos/listado', 3, 0, 0, '', 'fa fa-bank'),
(34, 30, 'Confirmación del Banco', '/tramitacion/confirmacion/listado', 4, 0, 0, '', 'fa fa-bank'),
(50, 0, 'Configuración', '/configuracion/recepcion/listado', 50, 0, 1, '', 'fa fa-sliders'),
(51, 50, 'Recepción de Dispositivos', '/configuracion/recepcion/listado', 1, 0, 0, '', 'fa fa-sliders'),
(52, 50, 'Carga de Parametros', '/configuracion/parametros/listado', 2, 0, 0, '', 'fa fa-sliders'),
(53, 50, 'Factura', '/configuracion/cotizacion/listado', 3, 0, 0, '', 'fa fa-sliders'),
(54, 50, 'Traslado del POS', '/configuracion/traslado/listado', 4, 0, 0, '', 'fa fa-sliders'),
(100, 0, 'Parametros', 'Parametros', 1000, 0, 1, '', 'fa fa-gears'),
(101, 100, 'Persona', '/parametros/persona/listado', 1001, 0, 0, '', 'fa fa-user-plus'),
(102, 1100, 'Usuario', '/parametros/usuario/listado', 1002, 0, 0, '', 'fa fa-user'),
(103, 1100, 'Tipo de Usuario', '/parametros/tipousuario/listado', 1003, 0, 0, '', 'fa fa-users'),
(104, 100, 'Tipo de Empresa', '/parametros/tipoempresa/listado', 1004, 1, 0, '', 'fa fa-building'),
(105, 100, 'Tipo Trabajador', '/parametros/tipotrabajador/listado', 1005, 0, 0, '', 'fa fa-list'),
(106, 100, 'Moneda', '/parametros/moneda/listado', 1006, 0, 0, ' ', 'fa fa-money'),
(107, 100, 'Tipo de Pago', '/parametros/tipopago/listado', 1007, 0, 0, '', 'fa fa-list'),
(108, 100, 'Banco', '/parametros/banco/listado', 1008, 0, 0, '', 'fa fa-bank'),
(109, 100, 'Concepto de Cobranza', '/parametros/cobranza/listado', 1009, 0, 1, '', 'fa fa-list'),
(110, 100, 'Tipo de Cliente', '/parametros/tipocliente/listado', 1010, 0, 0, '', 'fa fa-list'),
(111, 100, 'Estado Civil', '/parametros/estadocivil/listado', 1011, 0, 0, '', 'fa fa-list'),
(112, 100, 'Tipo de Unidad', '/parametros/tipounidad/listado', 1012, 0, 0, '', 'fa fa-list'),
(113, 100, 'Impuesto del Valor Agregado', '/parametros/iva/listado', 1013, 0, 0, '', 'fa fa-bank'),
(114, 8000, 'Modulos', '/parametros/modulos/listado', 1013, 0, 0, '', 'fa fa-lock'),
(115, 9000, 'Permisos', '/parametros/permisos', 1015, 0, 0, '', 'fa fa-users'),
(116, 100, 'Prueba', '/parametros/prueba', 1015, 0, 0, 'MODULO DE PRUEBA', 'fa fa-circle-o'),
(117, 100, 'Estatus del Pedido', '/parametros/estatusPedido/listado', 117, 0, 0, '', 'fa fa-list'),
(118, 100, 'Precio', '/parametros/precio/listado', 118, 0, 0, '', 'fa fa-money'),
(119, 100, 'Colegio', '/parametros/colegio/listado', 119, 0, 0, '', 'fa fa-building'),
(120, 100, 'Talla', '/parametros/talla/listado', 120, 0, 0, '', 'fa fa-signal'),
(121, 100, 'Ajuste de talla', '/parametros/tallaAjuste/listado', 121, 0, 0, 'A', 'fa fa-gears'),
(122, 100, 'Paises', '/parametros/paises/listado', 1, 0, 0, '', 'fa fa-globe'),
(123, 100, 'Estado', '/parametros/estado/listado', 2, 0, 0, '', 'fa fa-globe'),
(124, 100, 'Municipio', '/parametros/municipio/listado', 3, 0, 0, '', 'fa fa-globe'),
(125, 100, 'Parroquia', '/parametros/parroquia/listado', 5, 0, 0, '', 'fa fa-globe'),
(126, 100, 'Ciudades', '/parametros/ciudad/listado', 6, 0, 0, '', 'fa fa-globe'),
(127, 100, 'Organigrama', '/parametros/organigrama/listado', 7, 0, 0, '', 'fa fa-bank'),
(128, 100, 'Organigrama Nivel', '/parametros/organigramaNivel/listado', 8, 0, 0, '', 'fa fa-bank'),
(200, 0, 'Almacen', '/Inventarios/listado', 40, 0, 1, '', 'fa fa-list'),
(201, 200, 'Entrada', '/inventario/Entrada/listado', 201, 0, 0, '', 'fa fa-sign-in'),
(202, 200, 'Salidas', '/inventario/Salidas/listado', 202, 0, 0, '', 'fa fa-sign-out'),
(203, 200, 'Articulos', '/inventario/Articulos/listado', 203, 0, 0, '', 'fa fa-archive'),
(300, 100, 'Trabajador', '/trabajador/listado', 500, 0, 0, '', 'fa fa-users'),
(400, 0, 'Cliente', '/cliente/listado', 100, 0, 0, '', 'fa fa-users'),
(500, 0, 'Servicios', '/servicios/listado', 400, 0, 0, '', 'fa fa-list'),
(600, 0, 'Factura', '/ventas/factura/listado', 200, 0, 0, '', 'fa fa-dollar'),
(700, 0, 'Reportes', 'Reportes', 700, 0, 1, '', 'fa fa-file-pdf-o'),
(701, 700, 'Relacion de Ventas', '/reportes/ventas/listado', 701, 0, 0, '', ''),
(702, 700, 'Reporte de Egresos', '/reportes/egresos/listado', 702, 0, 0, '', 'fa fa-file-pdf-o'),
(703, 700, 'Relación de Ingresos - Egresos', '/reportes/relacion/listado', 703, 0, 0, '', 'fa fa-file-pdf-o'),
(704, 700, 'Materiales de pedidos', '/reportes/materiales', 704, 0, 0, '', 'fa fa-file-pdf-o'),
(800, 0, 'Gastos', 'Gastos', 500, 0, 1, '', 'fa fa-money'),
(801, 800, 'Egresos', '/gastos/egresos/listado', 501, 0, 0, '', 'fa fa-money'),
(802, 800, 'Nomina', '/gastos/nomina/listado', 502, 0, 0, '', 'fa fa-users'),
(900, 0, 'Cambiar Contraseña', '/parametros/usuario/cambiarclave', 9000, 0, 0, '', 'fa fa-lock'),
(902, 0, 'Pedidos', 'pedidos', 102, 0, 1, '', 'fa fa-list'),
(903, 902, 'Pedido', '/pedidos/pedidos/listado', 102, 0, 0, '', 'fa fa-list'),
(905, 902, 'Gestión', '/pedidos/gestion/listado', 905, 0, 0, '', 'fa fa-users'),
(1000, 0, 'Modelos', '', 1000, 0, 1, '', ''),
(1001, 1000, 'Modelo', '/modelos/modelo/listado', 2, 0, 0, '', ''),
(1002, 1000, 'Categorias', '/modelos/categoria/listado', 3, 0, 0, '', ''),
(1003, 1000, 'Tipo', '/modelos/tipo/listado', 1, 0, 0, '', ''),
(1004, 1000, 'Variante', '/modelos/variante/listado', 4, 0, 0, '', ''),
(1005, 1000, 'Opcional', '/modelos/opcional/listado', 5, 0, 0, '', ''),
(1006, 1000, 'Conjunto', '/modelos/conjunto/listado', 6, 0, 0, '', ''),
(1007, 1000, 'Color', '/modelos/color/listado', 7, 0, 0, '', ''),
(1008, 100, 'Emoji', '/modelos/emoji/listado', 10, 0, 0, '', ''),
(1009, 100, 'Fuentes', '/modelos/fuente/listado', 11, 0, 0, '', ''),
(1010, 100, 'Simbolo SImple', '/modelos/simbolo/listado', 1010, 0, 0, '', ''),
(1011, 100, 'Costos Adicionales', '/parametros/costo/listado', 1010, 0, 0, '', ''),
(1012, 100, 'Deducciones', '/parametros/deduccion/listado', 1012, 0, 0, '', ''),
(1013, 100, 'Precio Electivos', '/parametros/PrecioElectivo/listado', 1013, 0, 0, '', ''),
(1100, 0, 'Usuarios', 'usuarios', 1100, 0, 1, '', 'fa fa-users'),
(1200, 902, 'Pago', '/pedidos/pagos/listado', 300, 0, 0, '', 'fa fa-money'),
(8000, 0, 'Seguridad', '', 8000, 0, 1, '', 'fa fa-lock'),
(8001, 8000, 'Permisos Usuarios', '/seguridad/permisosUsuarios/listado', 1, 0, 0, '', 'fa fa-lock'),
(8002, 8000, 'Permisos Nivel', '/seguridad/permisosNivel/listado', 2, 0, 0, '', 'fa fa-lock'),
(8003, 8000, 'Permisos Acceso', '/seguridad/permisosAcceso/listado', 3, 0, 0, '', 'fa fa-lock'),
(8004, 8000, 'Roles', '/seguridad/roles/listado', 4, 0, 0, '', 'fa fa-lock'),
(8005, 8000, 'Permisos Roles', '/seguridad/permisosRoles/listado', 0, 0, 0, '', 'fa fa-lock'),
(9000, 0, 'Administrador', '', 9000, 0, 1, '', 'fa fa-gears');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulos_estatus`
--

CREATE TABLE `modulos_estatus` (
  `emodu_codig` int(11) NOT NULL,
  `emodu_descr` varchar(50) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Volcado de datos para la tabla `modulos_estatus`
--

INSERT INTO `modulos_estatus` (`emodu_codig`, `emodu_descr`) VALUES
(0, 'ACTIVO'),
(1, 'INACTIVO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `moneda`
--

CREATE TABLE `moneda` (
  `moned_codig` int(11) NOT NULL,
  `moned_descr` varchar(20) NOT NULL,
  `moned_value` varchar(10) NOT NULL,
  `moned_cambi` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `moneda`
--

INSERT INTO `moneda` (`moned_codig`, `moned_descr`, `moned_value`, `moned_cambi`) VALUES
(1, 'BOLIVARES', 'BS', '1'),
(2, 'DOLAR', 'USD', '668'),
(3, 'PESO CHILENO', 'CLP', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipio`
--

CREATE TABLE `municipio` (
  `munic_codig` int(11) NOT NULL,
  `estad_codig` int(11) NOT NULL,
  `paise_codig` int(11) NOT NULL,
  `munic_descr` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `municipio`
--

INSERT INTO `municipio` (`munic_codig`, `estad_codig`, `paise_codig`, `munic_descr`) VALUES
(1, 1, 232, 'ALTO ORINOCO'),
(2, 1, 232, 'ATABAPO'),
(3, 1, 232, 'ATURES'),
(4, 1, 232, 'AUTANA'),
(5, 1, 232, 'MANAPIARE'),
(6, 1, 232, 'MAROA'),
(7, 1, 232, 'RÍO NEGRO'),
(8, 2, 232, 'ANACO'),
(9, 2, 232, 'ARAGUA'),
(10, 2, 232, 'MANUEL EZEQUIEL BRUZUAL'),
(11, 2, 232, 'DIEGO BAUTISTA URBANEJA'),
(12, 2, 232, 'FERNANDO PEÑALVER'),
(13, 2, 232, 'FRANCISCO DEL CARMEN CARVAJAL'),
(14, 2, 232, 'GENERAL SIR ARTHUR MCGREGOR'),
(15, 2, 232, 'GUANTA'),
(16, 2, 232, 'INDEPENDENCIA'),
(17, 2, 232, 'JOSÉ GREGORIO MONAGAS'),
(18, 2, 232, 'JUAN ANTONIO SOTILLO'),
(19, 2, 232, 'JUAN MANUEL CAJIGAL'),
(20, 2, 232, 'LIBERTAD'),
(21, 2, 232, 'FRANCISCO DE MIRANDA'),
(22, 2, 232, 'PEDRO MARÍA FREITES'),
(23, 2, 232, 'PÍRITU'),
(24, 2, 232, 'SAN JOSÉ DE GUANIPA'),
(25, 2, 232, 'SAN JUAN DE CAPISTRANO'),
(26, 2, 232, 'SANTA ANA'),
(27, 2, 232, 'SIMÓN BOLÍVAR'),
(28, 2, 232, 'SIMÓN RODRÍGUEZ'),
(29, 3, 232, 'ACHAGUAS'),
(30, 3, 232, 'BIRUACA'),
(31, 3, 232, 'MUÑÓZ'),
(32, 3, 232, 'PÁEZ'),
(33, 3, 232, 'PEDRO CAMEJO'),
(34, 3, 232, 'RÓMULO GALLEGOS'),
(35, 3, 232, 'SAN FERNANDO'),
(36, 4, 232, 'ATANASIO GIRARDOT'),
(37, 4, 232, 'BOLÍVAR'),
(38, 4, 232, 'CAMATAGUA'),
(39, 4, 232, 'FRANCISCO LINARES ALCÁNTARA'),
(40, 4, 232, 'JOSÉ ÁNGEL LAMAS'),
(41, 4, 232, 'JOSÉ FÉLIX RIBAS'),
(42, 4, 232, 'JOSÉ RAFAEL REVENGA'),
(43, 4, 232, 'LIBERTADOR'),
(44, 4, 232, 'MARIO BRICEÑO IRAGORRY'),
(45, 4, 232, 'OCUMARE DE LA COSTA DE ORO'),
(46, 4, 232, 'SAN CASIMIRO'),
(47, 4, 232, 'SAN SEBASTIÁN'),
(48, 4, 232, 'SANTIAGO MARIÑO'),
(49, 4, 232, 'SANTOS MICHELENA'),
(50, 4, 232, 'SUCRE'),
(51, 4, 232, 'TOVAR'),
(52, 4, 232, 'URDANETA'),
(53, 4, 232, 'ZAMORA'),
(54, 5, 232, 'ALBERTO ARVELO TORREALBA'),
(55, 5, 232, 'ANDRÉS ELOY BLANCO'),
(56, 5, 232, 'ANTONIO JOSÉ DE SUCRE'),
(57, 5, 232, 'ARISMENDI'),
(58, 5, 232, 'BARINAS'),
(59, 5, 232, 'BOLÍVAR'),
(60, 5, 232, 'CRUZ PAREDES'),
(61, 5, 232, 'EZEQUIEL ZAMORA'),
(62, 5, 232, 'OBISPOS'),
(63, 5, 232, 'PEDRAZA'),
(64, 5, 232, 'ROJAS'),
(65, 5, 232, 'SOSA'),
(66, 6, 232, 'CARONÍ'),
(67, 6, 232, 'CEDEÑO'),
(68, 6, 232, 'EL CALLAO'),
(69, 6, 232, 'GRAN SABANA'),
(70, 6, 232, 'HERES'),
(71, 6, 232, 'PIAR'),
(72, 6, 232, 'ANGOSTURA (RAÚL LEONI)'),
(73, 6, 232, 'ROSCIO'),
(74, 6, 232, 'SIFONTES'),
(75, 6, 232, 'SUCRE'),
(76, 6, 232, 'PADRE PEDRO CHIEN'),
(77, 7, 232, 'BEJUMA'),
(78, 7, 232, 'CARLOS ARVELO'),
(79, 7, 232, 'DIEGO IBARRA'),
(80, 7, 232, 'GUACARA'),
(81, 7, 232, 'JUAN JOSÉ MORA'),
(82, 7, 232, 'LIBERTADOR'),
(83, 7, 232, 'LOS GUAYOS'),
(84, 7, 232, 'MIRANDA'),
(85, 7, 232, 'MONTALBÁN'),
(86, 7, 232, 'NAGUANAGUA'),
(87, 7, 232, 'PUERTO CABELLO'),
(88, 7, 232, 'SAN DIEGO'),
(89, 7, 232, 'SAN JOAQUÍN'),
(90, 7, 232, 'VALENCIA'),
(91, 8, 232, 'ANZOÁTEGUI'),
(92, 8, 232, 'TINAQUILLO'),
(93, 8, 232, 'GIRARDOT'),
(94, 8, 232, 'LIMA BLANCO'),
(95, 8, 232, 'PAO DE SAN JUAN BAUTISTA'),
(96, 8, 232, 'RICAURTE'),
(97, 8, 232, 'RÓMULO GALLEGOS'),
(98, 8, 232, 'SAN CARLOS'),
(99, 8, 232, 'TINACO'),
(100, 9, 232, 'ANTONIO DÍAZ'),
(101, 9, 232, 'CASACOIMA'),
(102, 9, 232, 'PEDERNALES'),
(103, 9, 232, 'TUCUPITA'),
(104, 10, 232, 'ACOSTA'),
(105, 10, 232, 'BOLÍVAR'),
(106, 10, 232, 'BUCHIVACOA'),
(107, 10, 232, 'CACIQUE MANAURE'),
(108, 10, 232, 'CARIRUBANA'),
(109, 10, 232, 'COLINA'),
(110, 10, 232, 'DABAJURO'),
(111, 10, 232, 'DEMOCRACIA'),
(112, 10, 232, 'FALCÓN'),
(113, 10, 232, 'FEDERACIÓN'),
(114, 10, 232, 'JACURA'),
(115, 10, 232, 'JOSÉ LAURENCIO SILVA'),
(116, 10, 232, 'LOS TAQUES'),
(117, 10, 232, 'MAUROA'),
(118, 10, 232, 'MIRANDA'),
(119, 10, 232, 'MONSEÑOR ITURRIZA'),
(120, 10, 232, 'PALMASOLA'),
(121, 10, 232, 'PETIT'),
(122, 10, 232, 'PÍRITU'),
(123, 10, 232, 'SAN FRANCISCO'),
(124, 10, 232, 'SUCRE'),
(125, 10, 232, 'TOCÓPERO'),
(126, 10, 232, 'UNIÓN'),
(127, 10, 232, 'URUMACO'),
(128, 10, 232, 'ZAMORA'),
(129, 11, 232, 'CAMAGUÁN'),
(130, 11, 232, 'CHAGUARAMAS'),
(131, 11, 232, 'EL SOCORRO'),
(132, 11, 232, 'JOSÉ FÉLIX RIBAS'),
(133, 11, 232, 'JOSÉ TADEO MONAGAS'),
(134, 11, 232, 'JUAN GERMÁN ROSCIO'),
(135, 11, 232, 'JULIÁN MELLADO'),
(136, 11, 232, 'LAS MERCEDES'),
(137, 11, 232, 'LEONARDO INFANTE'),
(138, 11, 232, 'PEDRO ZARAZA'),
(139, 11, 232, 'ORTÍZ'),
(140, 11, 232, 'SAN GERÓNIMO DE GUAYABAL'),
(141, 11, 232, 'SAN JOSÉ DE GUARIBE'),
(142, 11, 232, 'SANTA MARÍA DE IPIRE'),
(143, 11, 232, 'SEBASTIÁN FRANCISCO DE MIRANDA'),
(144, 12, 232, 'ANDRÉS ELOY BLANCO'),
(145, 12, 232, 'CRESPO'),
(146, 12, 232, 'IRIBARREN'),
(147, 12, 232, 'JIMÉNEZ'),
(148, 12, 232, 'MORÁN'),
(149, 12, 232, 'PALAVECINO'),
(150, 12, 232, 'SIMÓN PLANAS'),
(151, 12, 232, 'TORRES'),
(152, 12, 232, 'URDANETA'),
(179, 13, 232, 'ALBERTO ADRIANI'),
(180, 13, 232, 'ANDRÉS BELLO'),
(181, 13, 232, 'ANTONIO PINTO SALINAS'),
(182, 13, 232, 'ARICAGUA'),
(183, 13, 232, 'ARZOBISPO CHACÓN'),
(184, 13, 232, 'CAMPO ELÍAS'),
(185, 13, 232, 'CARACCIOLO PARRA OLMEDO'),
(186, 13, 232, 'CARDENAL QUINTERO'),
(187, 13, 232, 'GUARAQUE'),
(188, 13, 232, 'JULIO CÉSAR SALAS'),
(189, 13, 232, 'JUSTO BRICEÑO'),
(190, 13, 232, 'LIBERTADOR'),
(191, 13, 232, 'MIRANDA'),
(192, 13, 232, 'OBISPO RAMOS DE LORA'),
(193, 13, 232, 'PADRE NOGUERA'),
(194, 13, 232, 'PUEBLO LLANO'),
(195, 13, 232, 'RANGEL'),
(196, 13, 232, 'RIVAS DÁVILA'),
(197, 13, 232, 'SANTOS MARQUINA'),
(198, 13, 232, 'SUCRE'),
(199, 13, 232, 'TOVAR'),
(200, 13, 232, 'TULIO FEBRES CORDERO'),
(201, 13, 232, 'ZEA'),
(223, 14, 232, 'ACEVEDO'),
(224, 14, 232, 'ANDRÉS BELLO'),
(225, 14, 232, 'BARUTA'),
(226, 14, 232, 'BRIÓN'),
(227, 14, 232, 'BUROZ'),
(228, 14, 232, 'CARRIZAL'),
(229, 14, 232, 'CHACAO'),
(230, 14, 232, 'CRISTÓBAL ROJAS'),
(231, 14, 232, 'EL HATILLO'),
(232, 14, 232, 'GUAICAIPURO'),
(233, 14, 232, 'INDEPENDENCIA'),
(234, 14, 232, 'LANDER'),
(235, 14, 232, 'LOS SALIAS'),
(236, 14, 232, 'PÁEZ'),
(237, 14, 232, 'PAZ CASTILLO'),
(238, 14, 232, 'PEDRO GUAL'),
(239, 14, 232, 'PLAZA'),
(240, 14, 232, 'SIMÓN BOLÍVAR'),
(241, 14, 232, 'SUCRE'),
(242, 14, 232, 'URDANETA'),
(243, 14, 232, 'ZAMORA'),
(258, 15, 232, 'ACOSTA'),
(259, 15, 232, 'AGUASAY'),
(260, 15, 232, 'BOLÍVAR'),
(261, 15, 232, 'CARIPE'),
(262, 15, 232, 'CEDEÑO'),
(263, 15, 232, 'EZEQUIEL ZAMORA'),
(264, 15, 232, 'LIBERTADOR'),
(265, 15, 232, 'MATURÍN'),
(266, 15, 232, 'PIAR'),
(267, 15, 232, 'PUNCERES'),
(268, 15, 232, 'SANTA BÁRBARA'),
(269, 15, 232, 'SOTILLO'),
(270, 15, 232, 'URACOA'),
(271, 16, 232, 'ANTOLÍN DEL CAMPO'),
(272, 16, 232, 'ARISMENDI'),
(273, 16, 232, 'GARCÍA'),
(274, 16, 232, 'GÓMEZ'),
(275, 16, 232, 'MANEIRO'),
(276, 16, 232, 'MARCANO'),
(277, 16, 232, 'MARIÑO'),
(278, 16, 232, 'PENÍNSULA DE MACANAO'),
(279, 16, 232, 'TUBORES'),
(280, 16, 232, 'VILLALBA'),
(281, 16, 232, 'DÍAZ'),
(282, 17, 232, 'AGUA BLANCA'),
(283, 17, 232, 'ARAURE'),
(284, 17, 232, 'ESTELLER'),
(285, 17, 232, 'GUANARE'),
(286, 17, 232, 'GUANARITO'),
(287, 17, 232, 'MONSEÑOR JOSÉ VICENTE DE UNDA'),
(288, 17, 232, 'OSPINO'),
(289, 17, 232, 'PÁEZ'),
(290, 17, 232, 'PAPELÓN'),
(291, 17, 232, 'SAN GENARO DE BOCONOÍTO'),
(292, 17, 232, 'SAN RAFAEL DE ONOTO'),
(293, 17, 232, 'SANTA ROSALÍA'),
(294, 17, 232, 'SUCRE'),
(295, 17, 232, 'TURÉN'),
(296, 18, 232, 'ANDRÉS ELOY BLANCO'),
(297, 18, 232, 'ANDRÉS MATA'),
(298, 18, 232, 'ARISMENDI'),
(299, 18, 232, 'BENÍTEZ'),
(300, 18, 232, 'BERMÚDEZ'),
(301, 18, 232, 'BOLÍVAR'),
(302, 18, 232, 'CAJIGAL'),
(303, 18, 232, 'CRUZ SALMERÓN ACOSTA'),
(304, 18, 232, 'LIBERTADOR'),
(305, 18, 232, 'MARIÑO'),
(306, 18, 232, 'MEJÍA'),
(307, 18, 232, 'MONTES'),
(308, 18, 232, 'RIBERO'),
(309, 18, 232, 'SUCRE'),
(310, 18, 232, 'VALDÉZ'),
(341, 19, 232, 'ANDRÉS BELLO'),
(342, 19, 232, 'ANTONIO RÓMULO COSTA'),
(343, 19, 232, 'AYACUCHO'),
(344, 19, 232, 'BOLÍVAR'),
(345, 19, 232, 'CÁRDENAS'),
(346, 19, 232, 'CÓRDOBA'),
(347, 19, 232, 'FERNÁNDEZ FEO'),
(348, 19, 232, 'FRANCISCO DE MIRANDA'),
(349, 19, 232, 'GARCÍA DE HEVIA'),
(350, 19, 232, 'GUÁSIMOS'),
(351, 19, 232, 'INDEPENDENCIA'),
(352, 19, 232, 'JÁUREGUI'),
(353, 19, 232, 'JOSÉ MARÍA VARGAS'),
(354, 19, 232, 'JUNÍN'),
(355, 19, 232, 'LIBERTAD'),
(356, 19, 232, 'LIBERTADOR'),
(357, 19, 232, 'LOBATERA'),
(358, 19, 232, 'MICHELENA'),
(359, 19, 232, 'PANAMERICANO'),
(360, 19, 232, 'PEDRO MARÍA UREÑA'),
(361, 19, 232, 'RAFAEL URDANETA'),
(362, 19, 232, 'SAMUEL DARÍO MALDONADO'),
(363, 19, 232, 'SAN CRISTÓBAL'),
(364, 19, 232, 'SEBORUCO'),
(365, 19, 232, 'SIMÓN RODRÍGUEZ'),
(366, 19, 232, 'SUCRE'),
(367, 19, 232, 'TORBES'),
(368, 19, 232, 'URIBANTE'),
(369, 19, 232, 'SAN JUDAS TADEO'),
(370, 20, 232, 'ANDRÉS BELLO'),
(371, 20, 232, 'BOCONÓ'),
(372, 20, 232, 'BOLÍVAR'),
(373, 20, 232, 'CANDELARIA'),
(374, 20, 232, 'CARACHE'),
(375, 20, 232, 'ESCUQUE'),
(376, 20, 232, 'JOSÉ FELIPE MÁRQUEZ CAÑIZALEZ'),
(377, 20, 232, 'JUAN VICENTE CAMPOS ELÍAS'),
(378, 20, 232, 'LA CEIBA'),
(379, 20, 232, 'MIRANDA'),
(380, 20, 232, 'MONTE CARMELO'),
(381, 20, 232, 'MOTATÁN'),
(382, 20, 232, 'PAMPÁN'),
(383, 20, 232, 'PAMPANITO'),
(384, 20, 232, 'RAFAEL RANGEL'),
(385, 20, 232, 'SAN RAFAEL DE CARVAJAL'),
(386, 20, 232, 'SUCRE'),
(387, 20, 232, 'TRUJILLO'),
(388, 20, 232, 'URDANETA'),
(389, 20, 232, 'VALERA'),
(390, 21, 232, 'VARGAS'),
(391, 22, 232, 'ARÍSTIDES BASTIDAS'),
(392, 22, 232, 'BOLÍVAR'),
(407, 22, 232, 'BRUZUAL'),
(408, 22, 232, 'COCOROTE'),
(409, 22, 232, 'INDEPENDENCIA'),
(410, 22, 232, 'JOSÉ ANTONIO PÁEZ'),
(411, 22, 232, 'LA TRINIDAD'),
(412, 22, 232, 'MANUEL MONGE'),
(413, 22, 232, 'NIRGUA'),
(414, 22, 232, 'PEÑA'),
(415, 22, 232, 'SAN FELIPE'),
(416, 22, 232, 'SUCRE'),
(417, 22, 232, 'URACHICHE'),
(418, 22, 232, 'JOSÉ JOAQUÍN VEROES'),
(441, 23, 232, 'ALMIRANTE PADILLA'),
(442, 23, 232, 'BARALT'),
(443, 23, 232, 'CABIMAS'),
(444, 23, 232, 'CATATUMBO'),
(445, 23, 232, 'COLÓN'),
(446, 23, 232, 'FRANCISCO JAVIER PULGAR'),
(447, 23, 232, 'PÁEZ'),
(448, 23, 232, 'JESÚS ENRIQUE LOSADA'),
(449, 23, 232, 'JESÚS MARÍA SEMPRÚN'),
(450, 23, 232, 'LA CAÑADA DE URDANETA'),
(451, 23, 232, 'LAGUNILLAS'),
(452, 23, 232, 'MACHIQUES DE PERIJÁ'),
(453, 23, 232, 'MARA'),
(454, 23, 232, 'MARACAIBO'),
(455, 23, 232, 'MIRANDA'),
(456, 23, 232, 'ROSARIO DE PERIJÁ'),
(457, 23, 232, 'SAN FRANCISCO'),
(458, 23, 232, 'SANTA RITA'),
(459, 23, 232, 'SIMÓN BOLÍVAR'),
(460, 23, 232, 'SUCRE'),
(461, 23, 232, 'VALMORE RODRÍGUEZ'),
(462, 24, 232, 'LIBERTADOR');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nacionalidad`
--

CREATE TABLE `nacionalidad` (
  `nacio_codig` int(11) NOT NULL,
  `nacio_descr` varchar(20) NOT NULL,
  `nacio_value` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `nacionalidad`
--

INSERT INTO `nacionalidad` (`nacio_codig`, `nacio_descr`, `nacio_value`) VALUES
(1, 'VENEZOLANO(A)', 'V'),
(2, 'EXTRANJERO(A)', 'E');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomina`
--

CREATE TABLE `nomina` (
  `nomin_codig` int(11) NOT NULL,
  `nomin_descr` varchar(50) NOT NULL,
  `enomi_codig` int(11) NOT NULL,
  `tnomi_codig` int(11) NOT NULL,
  `pnomi_codig` int(11) NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `nomin_fcrea` date NOT NULL,
  `nomin_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `nomina`
--

INSERT INTO `nomina` (`nomin_codig`, `nomin_descr`, `enomi_codig`, `tnomi_codig`, `pnomi_codig`, `usuar_codig`, `nomin_fcrea`, `nomin_hcrea`) VALUES
(1, 'EMPLEADOS', 1, 1, 1, 3, '2018-03-18', '22:47:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomina_asignacion`
--

CREATE TABLE `nomina_asignacion` (
  `asign_codig` int(11) NOT NULL,
  `nomin_codig` int(11) NOT NULL,
  `perio_codig` int(11) NOT NULL,
  `traba_codig` int(11) NOT NULL,
  `asing_dtrab` double NOT NULL,
  `asign_dferi` double NOT NULL,
  `asign_cdfer` double NOT NULL,
  `asign_hextr` double NOT NULL,
  `asign_chext` double NOT NULL,
  `asign_bonif` double NOT NULL,
  `asign_balim` double NOT NULL,
  `asign_ntrab` double NOT NULL,
  `asign_cntra` double NOT NULL,
  `asign_obser` text NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `asign_fcrea` date NOT NULL,
  `asign_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `nomina_asignacion`
--

INSERT INTO `nomina_asignacion` (`asign_codig`, `nomin_codig`, `perio_codig`, `traba_codig`, `asing_dtrab`, `asign_dferi`, `asign_cdfer`, `asign_hextr`, `asign_chext`, `asign_bonif`, `asign_balim`, `asign_ntrab`, `asign_cntra`, `asign_obser`, `usuar_codig`, `asign_fcrea`, `asign_hcrea`) VALUES
(13, 1, 14, 4, 15, 1, 0, 1, 0, 1, 1, 1, 0, '', 3, '2018-05-21', '22:45:39'),
(14, 1, 14, 5, 15, 1, 0, 1, 5, 1, 1, 1, 0, '', 3, '2018-05-21', '22:45:39'),
(16, 1, 15, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 3, '2018-05-21', '23:06:26'),
(17, 1, 15, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 3, '2018-05-21', '23:06:26'),
(18, 1, 15, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 3, '2018-05-21', '23:06:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomina_concepto`
--

CREATE TABLE `nomina_concepto` (
  `conce_codig` int(11) NOT NULL,
  `nomin_codig` int(11) NOT NULL,
  `conce_descr` text NOT NULL,
  `conce_value` text NOT NULL,
  `tconc_codig` int(11) NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `conce_fcrea` date NOT NULL,
  `conce_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `nomina_concepto`
--

INSERT INTO `nomina_concepto` (`conce_codig`, `nomin_codig`, `conce_descr`, `conce_value`, `tconc_codig`, `usuar_codig`, `conce_fcrea`, `conce_hcrea`) VALUES
(1, 1, 'SALARIO BASE', '$traba_salar', 1, 3, '2018-04-10', '09:23:00'),
(2, 1, 'HORAS EXTRAS', '((($traba_salar/30)/8)*1.5)*$hextr_canti', 1, 3, '2018-04-10', '09:27:16'),
(3, 1, 'DIAS FERIADOS', '$traba_salar*2', 2, 3, '2018-04-10', '09:28:11'),
(4, 1, 'BONIFICACIONES', '$bonif_canti', 1, 3, '2018-04-10', '09:41:35'),
(5, 1, 'BONO ALIMENTICIO', '(61*500)*30', 1, 3, '2018-04-10', '09:42:00'),
(6, 1, 'DIAS NO LABORADOS', '$ntrab_canti*501.7', 2, 3, '2018-04-10', '09:44:37'),
(7, 1, 'SEGURO SOCIAL', '$total_asign*0.02', 2, 3, '2018-04-10', '09:45:40'),
(8, 1, 'FAOV', '$total_asign*0.005', 2, 3, '2018-04-10', '09:46:59'),
(9, 1, 'INCE', '$total_asign*0.005', 2, 3, '2018-04-10', '09:48:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomina_concepto_tipo`
--

CREATE TABLE `nomina_concepto_tipo` (
  `tconc_codig` int(11) NOT NULL,
  `tconc_descr` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `nomina_concepto_tipo`
--

INSERT INTO `nomina_concepto_tipo` (`tconc_codig`, `tconc_descr`) VALUES
(1, 'ASIGNACIÓN'),
(2, 'DEDUCCIÓN');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomina_estatus`
--

CREATE TABLE `nomina_estatus` (
  `enomi_codig` int(11) NOT NULL,
  `enomi_descr` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `nomina_estatus`
--

INSERT INTO `nomina_estatus` (`enomi_codig`, `enomi_descr`) VALUES
(1, 'ACTIVA'),
(2, 'CERRADA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomina_periodo`
--

CREATE TABLE `nomina_periodo` (
  `perio_codig` int(11) NOT NULL,
  `nomin_codig` int(11) NOT NULL,
  `perio_ano` int(11) NOT NULL,
  `meses_codig` int(11) NOT NULL,
  `perio_finic` date NOT NULL,
  `perio_ffina` date NOT NULL,
  `perio_monto` double NOT NULL,
  `eperi_codig` int(11) NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `perio_fcrea` date NOT NULL,
  `perio_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `nomina_periodo`
--

INSERT INTO `nomina_periodo` (`perio_codig`, `nomin_codig`, `perio_ano`, `meses_codig`, `perio_finic`, `perio_ffina`, `perio_monto`, `eperi_codig`, `usuar_codig`, `perio_fcrea`, `perio_hcrea`) VALUES
(14, 1, 0, 1, '0000-00-00', '0000-00-00', 3763281.25, 2, 3, '2018-05-21', '22:45:39'),
(15, 1, 0, 2, '0000-02-01', '0000-00-00', 0, 2, 3, '2018-05-21', '23:06:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomina_periodo_estatus`
--

CREATE TABLE `nomina_periodo_estatus` (
  `eperi_codig` int(11) NOT NULL,
  `eperi_descr` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `nomina_periodo_estatus`
--

INSERT INTO `nomina_periodo_estatus` (`eperi_codig`, `eperi_descr`) VALUES
(1, 'ACTIVO'),
(3, 'CALCULADO'),
(2, 'CERRADO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomina_resumen`
--

CREATE TABLE `nomina_resumen` (
  `resum_codig` int(11) NOT NULL,
  `nomin_codig` int(11) NOT NULL,
  `perio_codig` int(11) NOT NULL,
  `traba_codig` int(11) NOT NULL,
  `conce_codig` int(11) NOT NULL,
  `resum_monto` double NOT NULL,
  `tconc_codig` int(11) NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `resum_fcrea` date NOT NULL,
  `resum_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomina_tipo`
--

CREATE TABLE `nomina_tipo` (
  `tnomi_codig` int(11) NOT NULL,
  `tnomi_descr` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `nomina_tipo`
--

INSERT INTO `nomina_tipo` (`tnomi_codig`, `tnomi_descr`) VALUES
(2, 'EXTRAORDINARIA'),
(1, 'ORDINARIA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nomina_tipo_pago`
--

CREATE TABLE `nomina_tipo_pago` (
  `pnomi_codig` int(11) NOT NULL,
  `pnomi_descr` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `nomina_tipo_pago`
--

INSERT INTO `nomina_tipo_pago` (`pnomi_codig`, `pnomi_descr`) VALUES
(4, 'DIARIO'),
(1, 'MENSUAL'),
(2, 'QUINCENAL'),
(3, 'SEMANAL');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operador_telefonico`
--

CREATE TABLE `operador_telefonico` (
  `otele_codig` int(11) NOT NULL,
  `otele_descr` varchar(100) NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `otele_fcrea` date NOT NULL,
  `otele_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `operador_telefonico`
--

INSERT INTO `operador_telefonico` (`otele_codig`, `otele_descr`, `usuar_codig`, `otele_fcrea`, `otele_hcrea`) VALUES
(1, 'MOVISTAR', 3, '2019-06-24', '23:05:51'),
(2, 'DIGITEL', 3, '2019-06-24', '23:05:51');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `organigrama`
--

CREATE TABLE `organigrama` (
  `organ_codig` int(11) NOT NULL,
  `organ_padre` int(11) NOT NULL,
  `organ_descr` varchar(50) NOT NULL,
  `onive_codig` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `organigrama`
--

INSERT INTO `organigrama` (`organ_codig`, `organ_padre`, `organ_descr`, `onive_codig`) VALUES
(0, 0, 'SIN PADRE', 1),
(1, 0, 'PRESIDENCIA', 1),
(2, 1, 'VICEPRESIDENCIA DE PRUEBA', 2),
(3, 2, 'GERENCIA DE PRUEBA', 3),
(4, 3, 'COORDINACIÓN DE PRUEBA', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `organigrama_nivel`
--

CREATE TABLE `organigrama_nivel` (
  `onive_codig` int(11) NOT NULL,
  `onive_descr` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `organigrama_nivel`
--

INSERT INTO `organigrama_nivel` (`onive_codig`, `onive_descr`) VALUES
(1, 'NIVEL 1'),
(2, 'NIVEL 2'),
(3, 'NIVEL 3'),
(4, 'NIVEL 4');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago_tipo`
--

CREATE TABLE `pago_tipo` (
  `ptipo_codig` int(11) NOT NULL,
  `ptipo_descr` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pago_tipo`
--

INSERT INTO `pago_tipo` (`ptipo_codig`, `ptipo_descr`) VALUES
(4, 'CHEQUE'),
(6, 'DEPOSITO BANCARIO'),
(1, 'EFECTIVO'),
(5, 'TRANSFERENCIA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paises`
--

CREATE TABLE `paises` (
  `paise_codig` int(11) NOT NULL,
  `paise_descr` varchar(80) DEFAULT NULL,
  `paise_value` char(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `paises`
--

INSERT INTO `paises` (`paise_codig`, `paise_descr`, `paise_value`) VALUES
(1, 'AFGANISTÁN', 'AF'),
(2, 'ISLAS GLAND', 'AX'),
(3, 'ALBANIA', 'AL'),
(4, 'ALEMANIA', 'DE'),
(5, 'ANDORRA', 'AD'),
(6, 'ANGOLA', 'AO'),
(7, 'ANGUILLA', 'AI'),
(8, 'ANTÁRTIDA', 'AQ'),
(9, 'ANTIGUA Y BARBUDA', 'AG'),
(10, 'ANTILLAS HOLANDESAS', 'AN'),
(11, 'ARABIA SAUDÍ', 'SA'),
(12, 'ARGELIA', 'DZ'),
(13, 'ARGENTINA', 'AR'),
(14, 'ARMENIA', 'AM'),
(15, 'ARUBA', 'AW'),
(16, 'AUSTRALIA', 'AU'),
(17, 'AUSTRIA', 'AT'),
(18, 'AZERBAIYÁN', 'AZ'),
(19, 'BAHAMAS', 'BS'),
(20, 'BAHRÉIN', 'BH'),
(21, 'BANGLADESH', 'BD'),
(22, 'BARBADOS', 'BB'),
(23, 'BIELORRUSIA', 'BY'),
(24, 'BÉLGICA', 'BE'),
(25, 'BELICE', 'BZ'),
(26, 'BENIN', 'BJ'),
(27, 'BERMUDAS', 'BM'),
(28, 'BHUTÁN', 'BT'),
(29, 'BOLIVIA', 'BO'),
(30, 'BOSNIA Y HERZEGOVINA', 'BA'),
(31, 'BOTSUANA', 'BW'),
(32, 'ISLA BOUVET', 'BV'),
(33, 'BRASIL', 'BR'),
(34, 'BRUNÉI', 'BN'),
(35, 'BULGARIA', 'BG'),
(36, 'BURKINA FASO', 'BF'),
(37, 'BURUNDI', 'BI'),
(38, 'CABO VERDE', 'CV'),
(39, 'ISLAS CAIMÁN', 'KY'),
(40, 'CAMBOYA', 'KH'),
(41, 'CAMERÚN', 'CM'),
(42, 'CANADÁ', 'CA'),
(43, 'REPÚBLICA CENTROAFRICANA', 'CF'),
(44, 'CHAD', 'TD'),
(45, 'REPÚBLICA CHECA', 'CZ'),
(46, 'CHILE', 'CL'),
(47, 'CHINA', 'CN'),
(48, 'CHIPRE', 'CY'),
(49, 'ISLA DE NAVIDAD', 'CX'),
(50, 'CIUDAD DEL VATICANO', 'VA'),
(51, 'ISLAS COCOS', 'CC'),
(52, 'COLOMBIA', 'CO'),
(53, 'COMORAS', 'KM'),
(54, 'REPÚBLICA DEMOCRÁTICA DEL CONGO', 'CD'),
(55, 'CONGO', 'CG'),
(56, 'ISLAS COOK', 'CK'),
(57, 'COREA DEL NORTE', 'KP'),
(58, 'COREA DEL SUR', 'KR'),
(59, 'COSTA DE MARFIL', 'CI'),
(60, 'COSTA RICA', 'CR'),
(61, 'CROACIA', 'HR'),
(62, 'CUBA', 'CU'),
(63, 'DINAMARCA', 'DK'),
(64, 'DOMINICA', 'DM'),
(65, 'REPÚBLICA DOMINICANA', 'DO'),
(66, 'ECUADOR', 'EC'),
(67, 'EGIPTO', 'EG'),
(68, 'EL SALVADOR', 'SV'),
(69, 'EMIRATOS ÁRABES UNIDOS', 'AE'),
(70, 'ERITREA', 'ER'),
(71, 'ESLOVAQUIA', 'SK'),
(72, 'ESLOVENIA', 'SI'),
(73, 'ESPAÑA', 'ES'),
(74, 'ISLAS ULTRAMARINAS DE ESTADOS UNIDOS', 'UM'),
(75, 'ESTADOS UNIDOS', 'US'),
(76, 'ESTONIA', 'EE'),
(77, 'ETIOPÍA', 'ET'),
(78, 'ISLAS FEROE', 'FO'),
(79, 'FILIPINAS', 'PH'),
(80, 'FINLANDIA', 'FI'),
(81, 'FIYI', 'FJ'),
(82, 'FRANCIA', 'FR'),
(83, 'GABÓN', 'GA'),
(84, 'GAMBIA', 'GM'),
(85, 'GEORGIA', 'GE'),
(86, 'ISLAS GEORGIAS DEL SUR Y SANDWICH DEL SUR', 'GS'),
(87, 'GHANA', 'GH'),
(88, 'GIBRALTAR', 'GI'),
(89, 'GRANADA', 'GD'),
(90, 'GRECIA', 'GR'),
(91, 'GROENLANDIA', 'GL'),
(92, 'GUADALUPE', 'GP'),
(93, 'GUAM', 'GU'),
(94, 'GUATEMALA', 'GT'),
(95, 'GUAYANA FRANCESA', 'GF'),
(96, 'GUINEA', 'GN'),
(97, 'GUINEA ECUATORIAL', 'GQ'),
(98, 'GUINEA-BISSAU', 'GW'),
(99, 'GUYANA', 'GY'),
(100, 'HAITÍ', 'HT'),
(101, 'ISLAS HEARD Y MCDONALD', 'HM'),
(102, 'HONDURAS', 'HN'),
(103, 'HONG KONG', 'HK'),
(104, 'HUNGRÍA', 'HU'),
(105, 'INDIA', 'IN'),
(106, 'INDONESIA', 'ID'),
(107, 'IRÁN', 'IR'),
(108, 'IRAQ', 'IQ'),
(109, 'IRLANDA', 'IE'),
(110, 'ISLANDIA', 'IS'),
(111, 'ISRAEL', 'IL'),
(112, 'ITALIA', 'IT'),
(113, 'JAMAICA', 'JM'),
(114, 'JAPÓN', 'JP'),
(115, 'JORDANIA', 'JO'),
(116, 'KAZAJSTÁN', 'KZ'),
(117, 'KENIA', 'KE'),
(118, 'KIRGUISTÁN', 'KG'),
(119, 'KIRIBATI', 'KI'),
(120, 'KUWAIT', 'KW'),
(121, 'LAOS', 'LA'),
(122, 'LESOTHO', 'LS'),
(123, 'LETONIA', 'LV'),
(124, 'LÍBANO', 'LB'),
(125, 'LIBERIA', 'LR'),
(126, 'LIBIA', 'LY'),
(127, 'LIECHTENSTEIN', 'LI'),
(128, 'LITUANIA', 'LT'),
(129, 'LUXEMBURGO', 'LU'),
(130, 'MACAO', 'MO'),
(131, 'ARY MACEDONIA', 'MK'),
(132, 'MADAGASCAR', 'MG'),
(133, 'MALASIA', 'MY'),
(134, 'MALAWI', 'MW'),
(135, 'MALDIVAS', 'MV'),
(136, 'MALÍ', 'ML'),
(137, 'MALTA', 'MT'),
(138, 'ISLAS MALVINAS', 'FK'),
(139, 'ISLAS MARIANAS DEL NORTE', 'MP'),
(140, 'MARRUECOS', 'MA'),
(141, 'ISLAS MARSHALL', 'MH'),
(142, 'MARTINICA', 'MQ'),
(143, 'MAURICIO', 'MU'),
(144, 'MAURITANIA', 'MR'),
(145, 'MAYOTTE', 'YT'),
(146, 'MÉXICO', 'MX'),
(147, 'MICRONESIA', 'FM'),
(148, 'MOLDAVIA', 'MD'),
(149, 'MÓNACO', 'MC'),
(150, 'MONGOLIA', 'MN'),
(151, 'MONTSERRAT', 'MS'),
(152, 'MOZAMBIQUE', 'MZ'),
(153, 'MYANMAR', 'MM'),
(154, 'NAMIBIA', 'NA'),
(155, 'NAURU', 'NR'),
(156, 'NEPAL', 'NP'),
(157, 'NICARAGUA', 'NI'),
(158, 'NÍGER', 'NE'),
(159, 'NIGERIA', 'NG'),
(160, 'NIUE', 'NU'),
(161, 'ISLA NORFOLK', 'NF'),
(162, 'NORUEGA', 'NO'),
(163, 'NUEVA CALEDONIA', 'NC'),
(164, 'NUEVA ZELANDA', 'NZ'),
(165, 'OMÁN', 'OM'),
(166, 'PAÍSES BAJOS', 'NL'),
(167, 'PAKISTÁN', 'PK'),
(168, 'PALAU', 'PW'),
(169, 'PALESTINA', 'PS'),
(170, 'PANAMÁ', 'PA'),
(171, 'PAPÚA NUEVA GUINEA', 'PG'),
(172, 'PARAGUAY', 'PY'),
(173, 'PERÚ', 'PE'),
(174, 'ISLAS PITCAIRN', 'PN'),
(175, 'POLINESIA FRANCESA', 'PF'),
(176, 'POLONIA', 'PL'),
(177, 'PORTUGAL', 'PT'),
(178, 'PUERTO RICO', 'PR'),
(179, 'QATAR', 'QA'),
(180, 'REINO UNIDO', 'GB'),
(181, 'REUNIÓN', 'RE'),
(182, 'RUANDA', 'RW'),
(183, 'RUMANIA', 'RO'),
(184, 'RUSIA', 'RU'),
(185, 'SAHARA OCCIDENTAL', 'EH'),
(186, 'ISLAS SALOMÓN', 'SB'),
(187, 'SAMOA', 'WS'),
(188, 'SAMOA AMERICANA', 'AS'),
(189, 'SAN CRISTÓBAL Y NEVIS', 'KN'),
(190, 'SAN MARINO', 'SM'),
(191, 'SAN PEDRO Y MIQUELÓN', 'PM'),
(192, 'SAN VICENTE Y LAS GRANADINAS', 'VC'),
(193, 'SANTA HELENA', 'SH'),
(194, 'SANTA LUCÍA', 'LC'),
(195, 'SANTO TOMÉ Y PRÍNCIPE', 'ST'),
(196, 'SENEGAL', 'SN'),
(197, 'SERBIA Y MONTENEGRO', 'CS'),
(198, 'SEYCHELLES', 'SC'),
(199, 'SIERRA LEONA', 'SL'),
(200, 'SINGAPUR', 'SG'),
(201, 'SIRIA', 'SY'),
(202, 'SOMALIA', 'SO'),
(203, 'SRI LANKA', 'LK'),
(204, 'SUAZILANDIA', 'SZ'),
(205, 'SUDÁFRICA', 'ZA'),
(206, 'SUDÁN', 'SD'),
(207, 'SUECIA', 'SE'),
(208, 'SUIZA', 'CH'),
(209, 'SURINAM', 'SR'),
(210, 'SVALBARD Y JAN MAYEN', 'SJ'),
(211, 'TAILANDIA', 'TH'),
(212, 'TAIWÁN', 'TW'),
(213, 'TANZANIA', 'TZ'),
(214, 'TAYIKISTÁN', 'TJ'),
(215, 'TERRITORIO BRITÁNICO DEL OCÉANO ÍNDICO', 'IO'),
(216, 'TERRITORIOS AUSTRALES FRANCESES', 'TF'),
(217, 'TIMOR ORIENTAL', 'TL'),
(218, 'TOGO', 'TG'),
(219, 'TOKELAU', 'TK'),
(220, 'TONGA', 'TO'),
(221, 'TRINIDAD Y TOBAGO', 'TT'),
(222, 'TÚNEZ', 'TN'),
(223, 'ISLAS TURCAS Y CAICOS', 'TC'),
(224, 'TURKMENISTÁN', 'TM'),
(225, 'TURQUÍA', 'TR'),
(226, 'TUVALU', 'TV'),
(227, 'UCRANIA', 'UA'),
(228, 'UGANDA', 'UG'),
(229, 'URUGUAY', 'UY'),
(230, 'UZBEKISTÁN', 'UZ'),
(231, 'VANUATU', 'VU'),
(232, 'VENEZUELA', 'VE'),
(233, 'VIETNAM', 'VN'),
(234, 'ISLAS VÍRGENES BRITÁNICAS', 'VG'),
(235, 'ISLAS VÍRGENES DE LOS ESTADOS UNIDOS', 'VI'),
(236, 'WALLIS Y FUTUNA', 'WF'),
(237, 'YEMEN', 'YE'),
(238, 'YIBUTI', 'DJ'),
(239, 'ZAMBIA', 'ZM'),
(240, 'ZIMBABUE', 'ZW');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parroquia`
--

CREATE TABLE `parroquia` (
  `parro_codig` int(11) NOT NULL,
  `munic_codig` int(11) NOT NULL,
  `estad_codig` int(11) NOT NULL,
  `paise_codig` int(11) NOT NULL,
  `parro_descr` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `parroquia`
--

INSERT INTO `parroquia` (`parro_codig`, `munic_codig`, `estad_codig`, `paise_codig`, `parro_descr`) VALUES
(1, 1, 1, 232, 'ALTO ORINOCO'),
(2, 1, 1, 232, 'HUACHAMACARE ACANAÑA'),
(3, 1, 1, 232, 'MARAWAKA TOKY SHAMANAÑA'),
(4, 1, 1, 232, 'MAVAKA MAVAKA'),
(5, 1, 1, 232, 'SIERRA PARIMA PARIMABÉ'),
(6, 2, 1, 232, 'UCATA LAJA LISA'),
(7, 2, 1, 232, 'YAPACANA MACURUCO'),
(8, 2, 1, 232, 'CANAME GUARINUMA'),
(9, 3, 1, 232, 'FERNANDO GIRÓN TOVAR'),
(10, 3, 1, 232, 'LUIS ALBERTO GÓMEZ'),
(11, 3, 1, 232, 'PAHUEÑA LIMÓN DE PARHUEÑA'),
(12, 3, 1, 232, 'PLATANILLAL PLATANILLAL'),
(13, 4, 1, 232, 'SAMARIAPO'),
(14, 4, 1, 232, 'SIPAPO'),
(15, 4, 1, 232, 'MUNDUAPO'),
(16, 4, 1, 232, 'GUAYAPO'),
(17, 5, 1, 232, 'ALTO VENTUARI'),
(18, 5, 1, 232, 'MEDIO VENTUARI'),
(19, 5, 1, 232, 'BAJO VENTUARI'),
(20, 6, 1, 232, 'VICTORINO'),
(21, 6, 1, 232, 'COMUNIDAD'),
(22, 7, 1, 232, 'CASIQUIARE'),
(23, 7, 1, 232, 'COCUY'),
(24, 7, 1, 232, 'SAN CARLOS DE RÍO NEGRO'),
(25, 7, 1, 232, 'SOLANO'),
(26, 8, 2, 232, 'ANACO'),
(27, 8, 2, 232, 'SAN JOAQUÍN'),
(28, 9, 2, 232, 'CACHIPO'),
(29, 9, 2, 232, 'ARAGUA DE BARCELONA'),
(30, 11, 2, 232, 'LECHERÍA'),
(31, 11, 2, 232, 'EL MORRO'),
(32, 12, 2, 232, 'PUERTO PÍRITU'),
(33, 12, 2, 232, 'SAN MIGUEL'),
(34, 12, 2, 232, 'SUCRE'),
(35, 13, 2, 232, 'VALLE DE GUANAPE'),
(36, 13, 2, 232, 'SANTA BÁRBARA'),
(37, 14, 2, 232, 'EL CHAPARRO'),
(38, 14, 2, 232, 'TOMÁS ALFARO'),
(39, 14, 2, 232, 'CALATRAVA'),
(40, 15, 2, 232, 'GUANTA'),
(41, 15, 2, 232, 'CHORRERÓN'),
(42, 16, 2, 232, 'MAMO'),
(43, 16, 2, 232, 'SOLEDAD'),
(44, 17, 2, 232, 'MAPIRE'),
(45, 17, 2, 232, 'PIAR'),
(46, 17, 2, 232, 'SANTA CLARA'),
(47, 17, 2, 232, 'SAN DIEGO DE CABRUTICA'),
(48, 17, 2, 232, 'UVERITO'),
(49, 17, 2, 232, 'ZUATA'),
(50, 18, 2, 232, 'PUERTO LA CRUZ'),
(51, 18, 2, 232, 'POZUELOS'),
(52, 19, 2, 232, 'ONOTO'),
(53, 19, 2, 232, 'SAN PABLO'),
(54, 20, 2, 232, 'SAN MATEO'),
(55, 20, 2, 232, 'EL CARITO'),
(56, 20, 2, 232, 'SANTA INÉS'),
(57, 20, 2, 232, 'LA ROMEREÑA'),
(58, 21, 2, 232, 'ATAPIRIRE'),
(59, 21, 2, 232, 'BOCA DEL PAO'),
(60, 21, 2, 232, 'EL PAO'),
(61, 21, 2, 232, 'PARIAGUÁN'),
(62, 22, 2, 232, 'CANTAURA'),
(63, 22, 2, 232, 'LIBERTADOR'),
(64, 22, 2, 232, 'SANTA ROSA'),
(65, 22, 2, 232, 'URICA'),
(66, 23, 2, 232, 'PÍRITU'),
(67, 23, 2, 232, 'SAN FRANCISCO'),
(68, 24, 2, 232, 'SAN JOSÉ DE GUANIPA'),
(69, 25, 2, 232, 'BOCA DE UCHIRE'),
(70, 25, 2, 232, 'BOCA DE CHÁVEZ'),
(71, 26, 2, 232, 'PUEBLO NUEVO'),
(72, 26, 2, 232, 'SANTA ANA'),
(73, 27, 2, 232, 'BERGANTÍN'),
(74, 27, 2, 232, 'CAIGUA'),
(75, 27, 2, 232, 'EL CARMEN'),
(76, 27, 2, 232, 'EL PILAR'),
(77, 27, 2, 232, 'NARICUAL'),
(78, 27, 2, 232, 'SAN CRSITÓBAL'),
(79, 28, 2, 232, 'EDMUNDO BARRIOS'),
(80, 28, 2, 232, 'MIGUEL OTERO SILVA'),
(81, 29, 3, 232, 'ACHAGUAS'),
(82, 29, 3, 232, 'APURITO'),
(83, 29, 3, 232, 'EL YAGUAL'),
(84, 29, 3, 232, 'GUACHARA'),
(85, 29, 3, 232, 'MUCURITAS'),
(86, 29, 3, 232, 'QUESERAS DEL MEDIO'),
(87, 30, 3, 232, 'BIRUACA'),
(88, 31, 3, 232, 'BRUZUAL'),
(89, 31, 3, 232, 'MANTECAL'),
(90, 31, 3, 232, 'QUINTERO'),
(91, 31, 3, 232, 'RINCÓN HONDO'),
(92, 31, 3, 232, 'SAN VICENTE'),
(93, 32, 3, 232, 'GUASDUALITO'),
(94, 32, 3, 232, 'ARAMENDI'),
(95, 32, 3, 232, 'EL AMPARO'),
(96, 32, 3, 232, 'SAN CAMILO'),
(97, 32, 3, 232, 'URDANETA'),
(98, 33, 3, 232, 'SAN JUAN DE PAYARA'),
(99, 33, 3, 232, 'CODAZZI'),
(100, 33, 3, 232, 'CUNAVICHE'),
(101, 34, 3, 232, 'ELORZA'),
(102, 34, 3, 232, 'LA TRINIDAD'),
(103, 35, 3, 232, 'SAN FERNANDO'),
(104, 35, 3, 232, 'EL RECREO'),
(105, 35, 3, 232, 'PEÑALVER'),
(106, 35, 3, 232, 'SAN RAFAEL DE ATAMAICA'),
(107, 36, 4, 232, 'PEDRO JOSÉ OVALLES'),
(108, 36, 4, 232, 'JOAQUÍN CRESPO'),
(109, 36, 4, 232, 'JOSÉ CASANOVA GODOY'),
(110, 36, 4, 232, 'MADRE MARÍA DE SAN JOSÉ'),
(111, 36, 4, 232, 'ANDRÉS ELOY BLANCO'),
(112, 36, 4, 232, 'LOS TACARIGUA'),
(113, 36, 4, 232, 'LAS DELICIAS'),
(114, 36, 4, 232, 'CHORONÍ'),
(115, 37, 4, 232, 'BOLÍVAR'),
(116, 38, 4, 232, 'CAMATAGUA'),
(117, 38, 4, 232, 'CARMEN DE CURA'),
(118, 39, 4, 232, 'SANTA RITA'),
(119, 39, 4, 232, 'FRANCISCO DE MIRANDA'),
(120, 39, 4, 232, 'MOSEÑOR FELICIANO GONZÁLEZ'),
(121, 40, 4, 232, 'SANTA CRUZ'),
(122, 41, 4, 232, 'JOSÉ FÉLIX RIBAS'),
(123, 41, 4, 232, 'CASTOR NIEVES RÍOS'),
(124, 41, 4, 232, 'LAS GUACAMAYAS'),
(125, 41, 4, 232, 'PAO DE ZÁRATE'),
(126, 41, 4, 232, 'ZUATA'),
(127, 42, 4, 232, 'JOSÉ RAFAEL REVENGA'),
(128, 43, 4, 232, 'PALO NEGRO'),
(129, 43, 4, 232, 'SAN MARTÍN DE PORRES'),
(130, 44, 4, 232, 'EL LIMÓN'),
(131, 44, 4, 232, 'CAÑA DE AZÚCAR'),
(132, 45, 4, 232, 'OCUMARE DE LA COSTA'),
(133, 46, 4, 232, 'SAN CASIMIRO'),
(134, 46, 4, 232, 'GÜIRIPA'),
(135, 46, 4, 232, 'OLLAS DE CARAMACATE'),
(136, 46, 4, 232, 'VALLE MORÍN'),
(137, 47, 4, 232, 'SAN SEBASTÍAN'),
(138, 48, 4, 232, 'TURMERO'),
(139, 48, 4, 232, 'AREVALO APONTE'),
(140, 48, 4, 232, 'CHUAO'),
(141, 48, 4, 232, 'SAMÁN DE GÜERE'),
(142, 48, 4, 232, 'ALFREDO PACHECO MIRANDA'),
(143, 49, 4, 232, 'SANTOS MICHELENA'),
(144, 49, 4, 232, 'TIARA'),
(145, 50, 4, 232, 'CAGUA'),
(146, 50, 4, 232, 'BELLA VISTA'),
(147, 51, 4, 232, 'TOVAR'),
(148, 52, 4, 232, 'URDANETA'),
(149, 52, 4, 232, 'LAS PEÑITAS'),
(150, 52, 4, 232, 'SAN FRANCISCO DE CARA'),
(151, 52, 4, 232, 'TAGUAY'),
(152, 53, 4, 232, 'ZAMORA'),
(153, 53, 4, 232, 'MAGDALENO'),
(154, 53, 4, 232, 'SAN FRANCISCO DE ASÍS'),
(155, 53, 4, 232, 'VALLES DE TUCUTUNEMO'),
(156, 53, 4, 232, 'AUGUSTO MIJARES'),
(157, 54, 5, 232, 'SABANETA'),
(158, 54, 5, 232, 'JUAN ANTONIO RODRÍGUEZ DOMÍNGUEZ'),
(159, 55, 5, 232, 'EL CANTÓN'),
(160, 55, 5, 232, 'SANTA CRUZ DE GUACAS'),
(161, 55, 5, 232, 'PUERTO VIVAS'),
(162, 56, 5, 232, 'TICOPORO'),
(163, 56, 5, 232, 'NICOLÁS PULIDO'),
(164, 56, 5, 232, 'ANDRÉS BELLO'),
(165, 57, 5, 232, 'ARISMENDI'),
(166, 57, 5, 232, 'GUADARRAMA'),
(167, 57, 5, 232, 'LA UNIÓN'),
(168, 57, 5, 232, 'SAN ANTONIO'),
(169, 58, 5, 232, 'BARINAS'),
(170, 58, 5, 232, 'ALBERTO ARVELO LARRIVA'),
(171, 58, 5, 232, 'SAN SILVESTRE'),
(172, 58, 5, 232, 'SANTA INÉS'),
(173, 58, 5, 232, 'SANTA LUCÍA'),
(174, 58, 5, 232, 'TORUMOS'),
(175, 58, 5, 232, 'EL CARMEN'),
(176, 58, 5, 232, 'RÓMULO BETANCOURT'),
(177, 58, 5, 232, 'CORAZÓN DE JESÚS'),
(178, 58, 5, 232, 'RAMÓN IGNACIO MÉNDEZ'),
(179, 58, 5, 232, 'ALTO BARINAS'),
(180, 58, 5, 232, 'MANUEL PALACIO FAJARDO'),
(181, 58, 5, 232, 'JUAN ANTONIO RODRÍGUEZ DOMÍNGUEZ'),
(182, 58, 5, 232, 'DOMINGA ORTIZ DE PÁEZ'),
(183, 59, 5, 232, 'BARINITAS'),
(184, 59, 5, 232, 'ALTAMIRA DE CÁCERES'),
(185, 59, 5, 232, 'CALDERAS'),
(186, 60, 5, 232, 'BARRANCAS'),
(187, 60, 5, 232, 'EL SOCORRO'),
(188, 60, 5, 232, 'MAZPARRITO'),
(189, 61, 5, 232, 'SANTA BÁRBARA'),
(190, 61, 5, 232, 'PEDRO BRICEÑO MÉNDEZ'),
(191, 61, 5, 232, 'RAMÓN IGNACIO MÉNDEZ'),
(192, 61, 5, 232, 'JOSÉ IGNACIO DEL PUMAR'),
(193, 62, 5, 232, 'OBISPOS'),
(194, 62, 5, 232, 'GUASIMITOS'),
(195, 62, 5, 232, 'EL REAL'),
(196, 62, 5, 232, 'LA LUZ'),
(197, 63, 5, 232, 'CIUDAD BOLÍVIA'),
(198, 63, 5, 232, 'JOSÉ IGNACIO BRICEÑO'),
(199, 63, 5, 232, 'JOSÉ FÉLIX RIBAS'),
(200, 63, 5, 232, 'PÁEZ'),
(201, 64, 5, 232, 'LIBERTAD'),
(202, 64, 5, 232, 'DOLORES'),
(203, 64, 5, 232, 'SANTA ROSA'),
(204, 64, 5, 232, 'PALACIO FAJARDO'),
(205, 65, 5, 232, 'CIUDAD DE NUTRIAS'),
(206, 65, 5, 232, 'EL REGALO'),
(207, 65, 5, 232, 'PUERTO NUTRIAS'),
(208, 65, 5, 232, 'SANTA CATALINA'),
(209, 66, 6, 232, 'CACHAMAY'),
(210, 66, 6, 232, 'CHIRICA'),
(211, 66, 6, 232, 'DALLA COSTA'),
(212, 66, 6, 232, 'ONCE DE ABRIL'),
(213, 66, 6, 232, 'SIMÓN BOLÍVAR'),
(214, 66, 6, 232, 'UNARE'),
(215, 66, 6, 232, 'UNIVERSIDAD'),
(216, 66, 6, 232, 'VISTA AL SOL'),
(217, 66, 6, 232, 'POZO VERDE'),
(218, 66, 6, 232, 'YOCOIMA'),
(219, 66, 6, 232, '5 DE JULIO'),
(220, 67, 6, 232, 'CEDEÑO'),
(221, 67, 6, 232, 'ALTAGRACIA'),
(222, 67, 6, 232, 'ASCENSIÓN FARRERAS'),
(223, 67, 6, 232, 'GUANIAMO'),
(224, 67, 6, 232, 'LA URBANA'),
(225, 67, 6, 232, 'PIJIGUAOS'),
(226, 68, 6, 232, 'EL CALLAO'),
(227, 69, 6, 232, 'GRAN SABANA'),
(228, 69, 6, 232, 'IKABARÚ'),
(229, 70, 6, 232, 'CATEDRAL'),
(230, 70, 6, 232, 'ZEA'),
(231, 70, 6, 232, 'ORINOCO'),
(232, 70, 6, 232, 'JOSÉ ANTONIO PÁEZ'),
(233, 70, 6, 232, 'MARHUANTA'),
(234, 70, 6, 232, 'AGUA SALADA'),
(235, 70, 6, 232, 'VISTA HERMOSA'),
(236, 70, 6, 232, 'LA SABANITA'),
(237, 70, 6, 232, 'PANAPANA'),
(238, 71, 6, 232, 'ANDRÉS ELOY BLANCO'),
(239, 71, 6, 232, 'PEDRO COVA'),
(240, 72, 6, 232, 'RAÚL LEONI'),
(241, 72, 6, 232, 'BARCELONETA'),
(242, 72, 6, 232, 'SANTA BÁRBARA'),
(243, 72, 6, 232, 'SAN FRANCISCO'),
(244, 73, 6, 232, 'ROSCIO'),
(245, 73, 6, 232, 'SALÓM'),
(246, 74, 6, 232, 'SIFONTES'),
(247, 74, 6, 232, 'DALLA COSTA'),
(248, 74, 6, 232, 'SAN ISIDRO'),
(249, 75, 6, 232, 'SUCRE'),
(250, 75, 6, 232, 'ARIPAO'),
(251, 75, 6, 232, 'GUARATARO'),
(252, 75, 6, 232, 'LAS MAJADAS'),
(253, 75, 6, 232, 'MOITACO'),
(254, 76, 6, 232, 'PADRE PEDRO CHIEN'),
(255, 76, 6, 232, 'RÍO GRANDE'),
(256, 77, 7, 232, 'BEJUMA'),
(257, 77, 7, 232, 'CANOABO'),
(258, 77, 7, 232, 'SIMÓN BOLÍVAR'),
(259, 78, 7, 232, 'GÜIGÜE'),
(260, 78, 7, 232, 'CARABOBO'),
(261, 78, 7, 232, 'TACARIGUA'),
(262, 79, 7, 232, 'MARIARA'),
(263, 79, 7, 232, 'AGUAS CALIENTES'),
(264, 80, 7, 232, 'CIUDAD ALIANZA'),
(265, 80, 7, 232, 'GUACARA'),
(266, 80, 7, 232, 'YAGUA'),
(267, 81, 7, 232, 'MORÓN'),
(268, 81, 7, 232, 'YAGUA'),
(269, 82, 7, 232, 'TOCUYITO'),
(270, 82, 7, 232, 'INDEPENDENCIA'),
(271, 83, 7, 232, 'LOS GUAYOS'),
(272, 84, 7, 232, 'MIRANDA'),
(273, 85, 7, 232, 'MONTALBÁN'),
(274, 86, 7, 232, 'NAGUANAGUA'),
(275, 87, 7, 232, 'BARTOLOMÉ SALÓM'),
(276, 87, 7, 232, 'DEMOCRACIA'),
(277, 87, 7, 232, 'FRATERNIDAD'),
(278, 87, 7, 232, 'GOAIGOAZA'),
(279, 87, 7, 232, 'JUAN JOSÉ FLORES'),
(280, 87, 7, 232, 'UNIÓN'),
(281, 87, 7, 232, 'BORBURATA'),
(282, 87, 7, 232, 'PATANEMO'),
(283, 88, 7, 232, 'SAN DIEGO'),
(284, 89, 7, 232, 'SAN JOAQUÍN'),
(285, 90, 7, 232, 'CANDELARIA'),
(286, 90, 7, 232, 'CATEDRAL'),
(287, 90, 7, 232, 'EL SOCORRO'),
(288, 90, 7, 232, 'MIGUEL PEÑA'),
(289, 90, 7, 232, 'RAFAEL URDANETA'),
(290, 90, 7, 232, 'SAN BLAS'),
(291, 90, 7, 232, 'SAN JOSÉ'),
(292, 90, 7, 232, 'SANTA ROSA'),
(293, 90, 7, 232, 'NEGRO PRIMERO'),
(294, 91, 8, 232, 'COJEDES'),
(295, 91, 8, 232, 'JUAN DE MATA SUÁREZ'),
(296, 92, 8, 232, 'TINAQUILLO'),
(297, 93, 8, 232, 'EL BAÚL'),
(298, 93, 8, 232, 'SUCRE'),
(299, 94, 8, 232, 'LA AGUADITA'),
(300, 94, 8, 232, 'MACAPO'),
(301, 95, 8, 232, 'EL PAO'),
(302, 96, 8, 232, 'EL AMPARO'),
(303, 96, 8, 232, 'LIBERTAD DE COJEDES'),
(304, 97, 8, 232, 'RÓMULO GALLEGOS'),
(305, 98, 8, 232, 'SAN CARLOS DE AUSTRIA'),
(306, 98, 8, 232, 'JUAN ÁNGEL BRAVO'),
(307, 98, 8, 232, 'MANUEL MANRIQUE'),
(308, 99, 8, 232, 'GENERAL EN JEFE JOSÉ LAURENCIO SILVA'),
(309, 100, 9, 232, 'CURIAPO'),
(310, 100, 9, 232, 'ALMIRANTE LUIS BRIÓN'),
(311, 100, 9, 232, 'FRANCISCO ANICETO LUGO'),
(312, 100, 9, 232, 'MANUEL RENAUD'),
(313, 100, 9, 232, 'PADRE BARRAL'),
(314, 100, 9, 232, 'SANTOS DE ABELGAS'),
(315, 101, 9, 232, 'IMATACA'),
(316, 101, 9, 232, 'CINCO DE JULIO'),
(317, 101, 9, 232, 'JUAN BAUTISTA ARISMENDI'),
(318, 101, 9, 232, 'MANUEL PIAR'),
(319, 101, 9, 232, 'RÓMULO GALLEGOS'),
(320, 102, 9, 232, 'PEDERNALES'),
(321, 102, 9, 232, 'LUIS BELTRÁN PRIETO FIGUEROA'),
(322, 103, 9, 232, 'SAN JOSÉ (DELTA AMACURO)'),
(323, 103, 9, 232, 'JOSÉ VIDAL MARCANO'),
(324, 103, 9, 232, 'JUAN MILLÁN'),
(325, 103, 9, 232, 'LEONARDO RUÍZ PINEDA'),
(326, 103, 9, 232, 'MARISCAL ANTONIO JOSÉ DE SUCRE'),
(327, 103, 9, 232, 'MONSEÑOR ARGIMIRO GARCÍA'),
(328, 103, 9, 232, 'SAN RAFAEL (DELTA AMACURO)'),
(329, 103, 9, 232, 'VIRGEN DEL VALLE'),
(330, 10, 2, 232, 'CLARINES'),
(331, 10, 2, 232, 'GUANAPE'),
(332, 10, 2, 232, 'SABANA DE UCHIRE'),
(333, 104, 10, 232, 'CAPADARE'),
(334, 104, 10, 232, 'LA PASTORA'),
(335, 104, 10, 232, 'LIBERTADOR'),
(336, 104, 10, 232, 'SAN JUAN DE LOS CAYOS'),
(337, 105, 10, 232, 'ARACUA'),
(338, 105, 10, 232, 'LA PEÑA'),
(339, 105, 10, 232, 'SAN LUIS'),
(340, 106, 10, 232, 'BARIRO'),
(341, 106, 10, 232, 'BOROJÓ'),
(342, 106, 10, 232, 'CAPATÁRIDA'),
(343, 106, 10, 232, 'GUAJIRO'),
(344, 106, 10, 232, 'SEQUE'),
(345, 106, 10, 232, 'ZAZÁRIDA'),
(346, 106, 10, 232, 'VALLE DE EROA'),
(347, 107, 10, 232, 'CACIQUE MANAURE'),
(348, 108, 10, 232, 'NORTE'),
(349, 108, 10, 232, 'CARIRUBANA'),
(350, 108, 10, 232, 'SANTA ANA'),
(351, 108, 10, 232, 'URBANA PUNTA CARDÓN'),
(352, 109, 10, 232, 'LA VELA DE CORO'),
(353, 109, 10, 232, 'ACURIGUA'),
(354, 109, 10, 232, 'GUAIBACOA'),
(355, 109, 10, 232, 'LAS CALDERAS'),
(356, 109, 10, 232, 'MACORUCA'),
(357, 110, 10, 232, 'DABAJURO'),
(358, 111, 10, 232, 'AGUA CLARA'),
(359, 111, 10, 232, 'AVARIA'),
(360, 111, 10, 232, 'PEDREGAL'),
(361, 111, 10, 232, 'PIEDRA GRANDE'),
(362, 111, 10, 232, 'PURURECHE'),
(363, 112, 10, 232, 'ADAURE'),
(364, 112, 10, 232, 'ADÍCORA'),
(365, 112, 10, 232, 'BARAIVED'),
(366, 112, 10, 232, 'BUENA VISTA'),
(367, 112, 10, 232, 'JADACAQUIVA'),
(368, 112, 10, 232, 'EL VÍNCULO'),
(369, 112, 10, 232, 'EL HATO'),
(370, 112, 10, 232, 'MORUY'),
(371, 112, 10, 232, 'PUEBLO NUEVO'),
(372, 113, 10, 232, 'AGUA LARGA'),
(373, 113, 10, 232, 'EL PAUJÍ'),
(374, 113, 10, 232, 'INDEPENDENCIA'),
(375, 113, 10, 232, 'MAPARARÍ'),
(376, 114, 10, 232, 'AGUA LINDA'),
(377, 114, 10, 232, 'ARAURIMA'),
(378, 114, 10, 232, 'JACURA'),
(379, 115, 10, 232, 'TUCACAS'),
(380, 115, 10, 232, 'BOCA DE AROA'),
(381, 116, 10, 232, 'LOS TAQUES'),
(382, 116, 10, 232, 'JUDIBANA'),
(383, 117, 10, 232, 'MENE DE MAUROA'),
(384, 117, 10, 232, 'SAN FÉLIX'),
(385, 117, 10, 232, 'CASIGUA'),
(386, 118, 10, 232, 'GUZMÁN GUILLERMO'),
(387, 118, 10, 232, 'MITARE'),
(388, 118, 10, 232, 'RÍO SECO'),
(389, 118, 10, 232, 'SABANETA'),
(390, 118, 10, 232, 'SAN ANTONIO'),
(391, 118, 10, 232, 'SAN GABRIEL'),
(392, 118, 10, 232, 'SANTA ANA'),
(393, 119, 10, 232, 'BOCA DEL TOCUYO'),
(394, 119, 10, 232, 'CHICHIRIVICHE'),
(395, 119, 10, 232, 'TOCUYO DE LA COSTA'),
(396, 120, 10, 232, 'PALMASOLA'),
(397, 121, 10, 232, 'CABURE'),
(398, 121, 10, 232, 'COLINA'),
(399, 121, 10, 232, 'CURIMAGUA'),
(400, 122, 10, 232, 'SAN JOSÉ DE LA COSTA'),
(401, 122, 10, 232, 'PÍRITU'),
(402, 123, 10, 232, 'SAN FRANCISCO'),
(403, 124, 10, 232, 'SUCRE'),
(404, 124, 10, 232, 'PECAYA'),
(405, 125, 10, 232, 'TOCÓPERO'),
(406, 126, 10, 232, 'EL CHARAL'),
(407, 126, 10, 232, 'LAS VEGAS DEL TUY'),
(408, 126, 10, 232, 'SANTA CRUZ DE BUCARAL'),
(409, 127, 10, 232, 'BRUZUAL'),
(410, 127, 10, 232, 'URUMACO'),
(411, 128, 10, 232, 'PUERTO CUMAREBO'),
(412, 128, 10, 232, 'LA CIÉNAGA'),
(413, 128, 10, 232, 'LA SOLEDAD'),
(414, 128, 10, 232, 'PUEBLO CUMAREBO'),
(415, 128, 10, 232, 'ZAZÁRIDA'),
(416, 113, 10, 232, 'CHURUGUARA'),
(417, 129, 11, 232, 'CAMAGUÁN'),
(418, 129, 11, 232, 'PUERTO MIRANDA'),
(419, 129, 11, 232, 'UVERITO'),
(420, 130, 11, 232, 'CHAGUARAMAS'),
(421, 131, 11, 232, 'EL SOCORRO'),
(422, 132, 11, 232, 'TUCUPIDO'),
(423, 132, 11, 232, 'SAN RAFAEL DE LAYA'),
(424, 133, 11, 232, 'ALTAGRACIA DE ORITUCO'),
(425, 133, 11, 232, 'SAN RAFAEL DE ORITUCO'),
(426, 133, 11, 232, 'SAN FRANCISCO JAVIER DE LEZAMA'),
(427, 133, 11, 232, 'PASO REAL DE MACAIRA'),
(428, 133, 11, 232, 'CARLOS SOUBLETTE'),
(429, 133, 11, 232, 'SAN FRANCISCO DE MACAIRA'),
(430, 133, 11, 232, 'LIBERTAD DE ORITUCO'),
(431, 134, 11, 232, 'CANTACLARO'),
(432, 134, 11, 232, 'SAN JUAN DE LOS MORROS'),
(433, 134, 11, 232, 'PARAPARA'),
(434, 135, 11, 232, 'EL SOMBRERO'),
(435, 135, 11, 232, 'SOSA'),
(436, 136, 11, 232, 'LAS MERCEDES'),
(437, 136, 11, 232, 'CABRUTA'),
(438, 136, 11, 232, 'SANTA RITA DE MANAPIRE'),
(439, 137, 11, 232, 'VALLE DE LA PASCUA'),
(440, 137, 11, 232, 'ESPINO'),
(441, 138, 11, 232, 'SAN JOSÉ DE UNARE'),
(442, 138, 11, 232, 'ZARAZA'),
(443, 139, 11, 232, 'SAN JOSÉ DE TIZNADOS'),
(444, 139, 11, 232, 'SAN FRANCISCO DE TIZNADOS'),
(445, 139, 11, 232, 'SAN LORENZO DE TIZNADOS'),
(446, 139, 11, 232, 'ORTIZ'),
(447, 140, 11, 232, 'GUAYABAL'),
(448, 140, 11, 232, 'CAZORLA'),
(449, 141, 11, 232, 'SAN JOSÉ DE GUARIBE'),
(450, 141, 11, 232, 'UVERAL'),
(451, 142, 11, 232, 'SANTA MARÍA DE IPIRE'),
(452, 142, 11, 232, 'ALTAMIRA'),
(453, 143, 11, 232, 'EL CALVARIO'),
(454, 143, 11, 232, 'EL RASTRO'),
(455, 143, 11, 232, 'GUARDATINAJAS'),
(456, 143, 11, 232, 'CAPITAL URBANA CALABOZO'),
(457, 144, 12, 232, 'QUEBRADA HONDA DE GUACHE'),
(458, 144, 12, 232, 'PÍO TAMAYO'),
(459, 144, 12, 232, 'YACAMBÚ'),
(460, 145, 12, 232, 'FRÉITEZ'),
(461, 145, 12, 232, 'JOSÉ MARÍA BLANCO'),
(462, 146, 12, 232, 'CATEDRAL'),
(463, 146, 12, 232, 'CONCEPCIÓN'),
(464, 146, 12, 232, 'EL CUJÍ'),
(465, 146, 12, 232, 'JUAN DE VILLEGAS'),
(466, 146, 12, 232, 'SANTA ROSA'),
(467, 146, 12, 232, 'TAMACA'),
(468, 146, 12, 232, 'UNIÓN'),
(469, 146, 12, 232, 'AGUEDO FELIPE ALVARADO'),
(470, 146, 12, 232, 'BUENA VISTA'),
(471, 146, 12, 232, 'JUÁREZ'),
(472, 147, 12, 232, 'JUAN BAUTISTA RODRÍGUEZ'),
(473, 147, 12, 232, 'CUARA'),
(474, 147, 12, 232, 'DIEGO DE LOZADA'),
(475, 147, 12, 232, 'PARAÍSO DE SAN JOSÉ'),
(476, 147, 12, 232, 'SAN MIGUEL'),
(477, 147, 12, 232, 'TINTORERO'),
(478, 147, 12, 232, 'JOSÉ BERNARDO DORANTE'),
(479, 147, 12, 232, 'CORONEL MARIANO PERAZA '),
(480, 148, 12, 232, 'BOLÍVAR'),
(481, 148, 12, 232, 'ANZOÁTEGUI'),
(482, 148, 12, 232, 'GUARICO'),
(483, 148, 12, 232, 'HILARIO LUNA Y LUNA'),
(484, 148, 12, 232, 'HUMOCARO ALTO'),
(485, 148, 12, 232, 'HUMOCARO BAJO'),
(486, 148, 12, 232, 'LA CANDELARIA'),
(487, 148, 12, 232, 'MORÁN'),
(488, 149, 12, 232, 'CABUDARE'),
(489, 149, 12, 232, 'JOSÉ GREGORIO BASTIDAS'),
(490, 149, 12, 232, 'AGUA VIVA'),
(491, 150, 12, 232, 'SARARE'),
(492, 150, 12, 232, 'BURÍA'),
(493, 150, 12, 232, 'GUSTAVO VEGAS LEÓN'),
(494, 151, 12, 232, 'TRINIDAD SAMUEL'),
(495, 151, 12, 232, 'ANTONIO DÍAZ'),
(496, 151, 12, 232, 'CAMACARO'),
(497, 151, 12, 232, 'CASTAÑEDA'),
(498, 151, 12, 232, 'CECILIO ZUBILLAGA'),
(499, 151, 12, 232, 'CHIQUINQUIRÁ'),
(500, 151, 12, 232, 'EL BLANCO'),
(501, 151, 12, 232, 'ESPINOZA DE LOS MONTEROS'),
(502, 151, 12, 232, 'LARA'),
(503, 151, 12, 232, 'LAS MERCEDES'),
(504, 151, 12, 232, 'MANUEL MORILLO'),
(505, 151, 12, 232, 'MONTAÑA VERDE'),
(506, 151, 12, 232, 'MONTES DE OCA'),
(507, 151, 12, 232, 'TORRES'),
(508, 151, 12, 232, 'HERIBERTO ARROYO'),
(509, 151, 12, 232, 'REYES VARGAS'),
(510, 151, 12, 232, 'ALTAGRACIA'),
(511, 152, 12, 232, 'SIQUISIQUE'),
(512, 152, 12, 232, 'MOROTURO'),
(513, 152, 12, 232, 'SAN MIGUEL'),
(514, 152, 12, 232, 'XAGUAS'),
(515, 179, 13, 232, 'PRESIDENTE BETANCOURT'),
(516, 179, 13, 232, 'PRESIDENTE PÁEZ'),
(517, 179, 13, 232, 'PRESIDENTE RÓMULO GALLEGOS'),
(518, 179, 13, 232, 'GABRIEL PICÓN GONZÁLEZ'),
(519, 179, 13, 232, 'HÉCTOR AMABLE MORA'),
(520, 179, 13, 232, 'JOSÉ NUCETE SARDI'),
(521, 179, 13, 232, 'PULIDO MÉNDEZ'),
(522, 180, 13, 232, 'LA AZULITA'),
(523, 181, 13, 232, 'SANTA CRUZ DE MORA'),
(524, 181, 13, 232, 'MESA BOLÍVAR'),
(525, 181, 13, 232, 'MESA DE LAS PALMAS'),
(526, 182, 13, 232, 'ARICAGUA'),
(527, 182, 13, 232, 'SAN ANTONIO'),
(528, 183, 13, 232, 'CANAGUA'),
(529, 183, 13, 232, 'CAPURÍ'),
(530, 183, 13, 232, 'CHACANTÁ'),
(531, 183, 13, 232, 'EL MOLINO'),
(532, 183, 13, 232, 'GUAIMARAL'),
(533, 183, 13, 232, 'MUCUTUY'),
(534, 183, 13, 232, 'MUCUCHACHÍ'),
(535, 184, 13, 232, 'FERNÁNDEZ PEÑA'),
(536, 184, 13, 232, 'MATRIZ'),
(537, 184, 13, 232, 'MONTALBÁN'),
(538, 184, 13, 232, 'ACEQUIAS'),
(539, 184, 13, 232, 'JAJÍ'),
(540, 184, 13, 232, 'LA MESA'),
(541, 184, 13, 232, 'SAN JOSÉ DEL SUR'),
(542, 185, 13, 232, 'TUCANÍ'),
(543, 185, 13, 232, 'FLORENCIO RAMÍREZ'),
(544, 186, 13, 232, 'SANTO DOMINGO'),
(545, 186, 13, 232, 'LAS PIEDRAS'),
(546, 187, 13, 232, 'GUARAQUE'),
(547, 187, 13, 232, 'MESA DE QUINTERO'),
(548, 187, 13, 232, 'RÍO NEGRO'),
(549, 188, 13, 232, 'ARAPUEY'),
(550, 188, 13, 232, 'PALMIRA'),
(551, 189, 13, 232, 'SAN CRISTÓBAL DE TORONDOY'),
(552, 189, 13, 232, 'TORONDOY'),
(553, 190, 13, 232, 'ANTONIO SPINETTI DINI'),
(554, 190, 13, 232, 'ARIAS'),
(555, 190, 13, 232, 'CARACCIOLO PARRA PÉREZ'),
(556, 190, 13, 232, 'DOMINGO PEÑA'),
(557, 190, 13, 232, 'EL LLANO'),
(558, 190, 13, 232, 'GONZALO PICÓN FEBRES'),
(559, 190, 13, 232, 'JACINTO PLAZA'),
(560, 190, 13, 232, 'JUAN RODRÍGUEZ SUÁREZ'),
(561, 190, 13, 232, 'LASSO DE LA VEGA'),
(562, 190, 13, 232, 'MARIANO PICÓN SALAS'),
(563, 190, 13, 232, 'MILLA'),
(564, 190, 13, 232, 'OSUNA RODRÍGUEZ'),
(565, 190, 13, 232, 'SAGRARIO'),
(566, 190, 13, 232, 'EL MORRO'),
(567, 190, 13, 232, 'LOS NEVADOS'),
(568, 191, 13, 232, 'ANDRÉS ELOY BLANCO'),
(569, 191, 13, 232, 'LA VENTA'),
(570, 191, 13, 232, 'PIÑANGO'),
(571, 191, 13, 232, 'TIMOTES'),
(572, 192, 13, 232, 'ELOY PAREDES'),
(573, 192, 13, 232, 'SAN RAFAEL DE ALCÁZAR'),
(574, 192, 13, 232, 'SANTA ELENA DE ARENALES'),
(575, 193, 13, 232, 'SANTA MARÍA DE CAPARO'),
(576, 194, 13, 232, 'PUEBLO LLANO'),
(577, 195, 13, 232, 'CACUTE'),
(578, 195, 13, 232, 'LA TOMA'),
(579, 195, 13, 232, 'MUCUCHÍES'),
(580, 195, 13, 232, 'MUCURUBÁ'),
(581, 195, 13, 232, 'SAN RAFAEL'),
(582, 196, 13, 232, 'GERÓNIMO MALDONADO'),
(583, 196, 13, 232, 'BAILADORES'),
(584, 197, 13, 232, 'TABAY'),
(585, 198, 13, 232, 'CHIGUARÁ'),
(586, 198, 13, 232, 'ESTÁNQUEZ'),
(587, 198, 13, 232, 'LAGUNILLAS'),
(588, 198, 13, 232, 'LA TRAMPA'),
(589, 198, 13, 232, 'PUEBLO NUEVO DEL SUR'),
(590, 198, 13, 232, 'SAN JUAN'),
(591, 199, 13, 232, 'EL AMPARO'),
(592, 199, 13, 232, 'EL LLANO'),
(593, 199, 13, 232, 'SAN FRANCISCO'),
(594, 199, 13, 232, 'TOVAR'),
(595, 200, 13, 232, 'INDEPENDENCIA'),
(596, 200, 13, 232, 'MARÍA DE LA CONCEPCIÓN PALACIOS BLANCO'),
(597, 200, 13, 232, 'NUEVA BOLIVIA'),
(598, 200, 13, 232, 'SANTA APOLONIA'),
(599, 201, 13, 232, 'CAÑO EL TIGRE'),
(600, 201, 13, 232, 'ZEA'),
(601, 223, 14, 232, 'ARAGÜITA'),
(602, 223, 14, 232, 'ARÉVALO GONZÁLEZ'),
(603, 223, 14, 232, 'CAPAYA'),
(604, 223, 14, 232, 'CAUCAGUA'),
(605, 223, 14, 232, 'PANAQUIRE'),
(606, 223, 14, 232, 'RIBAS'),
(607, 223, 14, 232, 'EL CAFÉ'),
(608, 223, 14, 232, 'MARIZAPA'),
(609, 224, 14, 232, 'CUMBO'),
(610, 224, 14, 232, 'SAN JOSÉ DE BARLOVENTO'),
(611, 225, 14, 232, 'EL CAFETAL'),
(612, 225, 14, 232, 'LAS MINAS'),
(613, 225, 14, 232, 'NUESTRA SEÑORA DEL ROSARIO'),
(614, 226, 14, 232, 'HIGUEROTE'),
(615, 226, 14, 232, 'CURIEPE'),
(616, 226, 14, 232, 'TACARIGUA DE BRIÓN'),
(617, 227, 14, 232, 'MAMPORAL'),
(618, 228, 14, 232, 'CARRIZAL'),
(619, 229, 14, 232, 'CHACAO'),
(620, 230, 14, 232, 'CHARALLAVE'),
(621, 230, 14, 232, 'LAS BRISAS'),
(622, 231, 14, 232, 'EL HATILLO'),
(623, 232, 14, 232, 'ALTAGRACIA DE LA MONTAÑA'),
(624, 232, 14, 232, 'CECILIO ACOSTA'),
(625, 232, 14, 232, 'LOS TEQUES'),
(626, 232, 14, 232, 'EL JARILLO'),
(627, 232, 14, 232, 'SAN PEDRO'),
(628, 232, 14, 232, 'TÁCATA'),
(629, 232, 14, 232, 'PARACOTOS'),
(630, 233, 14, 232, 'CARTANAL'),
(631, 233, 14, 232, 'SANTA TERESA DEL TUY'),
(632, 234, 14, 232, 'LA DEMOCRACIA'),
(633, 234, 14, 232, 'OCUMARE DEL TUY'),
(634, 234, 14, 232, 'SANTA BÁRBARA'),
(635, 235, 14, 232, 'SAN ANTONIO DE LOS ALTOS'),
(636, 236, 14, 232, 'RÍO CHICO'),
(637, 236, 14, 232, 'EL GUAPO'),
(638, 236, 14, 232, 'TACARIGUA DE LA LAGUNA'),
(639, 236, 14, 232, 'PAPARO'),
(640, 236, 14, 232, 'SAN FERNANDO DEL GUAPO'),
(641, 237, 14, 232, 'SANTA LUCÍA DEL TUY'),
(642, 238, 14, 232, 'CÚPIRA'),
(643, 238, 14, 232, 'MACHURUCUTO'),
(644, 239, 14, 232, 'GUARENAS'),
(645, 240, 14, 232, 'SAN ANTONIO DE YARE'),
(646, 240, 14, 232, 'SAN FRANCISCO DE YARE'),
(647, 241, 14, 232, 'LEONCIO MARTÍNEZ'),
(648, 241, 14, 232, 'PETARE'),
(649, 241, 14, 232, 'CAUCAGÜITA'),
(650, 241, 14, 232, 'FILAS DE MARICHE'),
(651, 241, 14, 232, 'LA DOLORITA'),
(652, 242, 14, 232, 'CÚA'),
(653, 242, 14, 232, 'NUEVA CÚA'),
(654, 243, 14, 232, 'GUATIRE'),
(655, 243, 14, 232, 'BOLÍVAR'),
(656, 258, 15, 232, 'SAN ANTONIO DE MATURÍN'),
(657, 258, 15, 232, 'SAN FRANCISCO DE MATURÍN'),
(658, 259, 15, 232, 'AGUASAY'),
(659, 260, 15, 232, 'CARIPITO'),
(660, 261, 15, 232, 'EL GUÁCHARO'),
(661, 261, 15, 232, 'LA GUANOTA'),
(662, 261, 15, 232, 'SABANA DE PIEDRA'),
(663, 261, 15, 232, 'SAN AGUSTÍN'),
(664, 261, 15, 232, 'TERESEN'),
(665, 261, 15, 232, 'CARIPE'),
(666, 262, 15, 232, 'AREO'),
(667, 262, 15, 232, 'CAPITAL CEDEÑO'),
(668, 262, 15, 232, 'SAN FÉLIX DE CANTALICIO'),
(669, 262, 15, 232, 'VIENTO FRESCO'),
(670, 263, 15, 232, 'EL TEJERO'),
(671, 263, 15, 232, 'PUNTA DE MATA'),
(672, 264, 15, 232, 'CHAGUARAMAS'),
(673, 264, 15, 232, 'LAS ALHUACAS'),
(674, 264, 15, 232, 'TABASCA'),
(675, 264, 15, 232, 'TEMBLADOR'),
(676, 265, 15, 232, 'ALTO DE LOS GODOS'),
(677, 265, 15, 232, 'BOQUERÓN'),
(678, 265, 15, 232, 'LAS COCUIZAS'),
(679, 265, 15, 232, 'LA CRUZ'),
(680, 265, 15, 232, 'SAN SIMÓN'),
(681, 265, 15, 232, 'EL COROZO'),
(682, 265, 15, 232, 'EL FURRIAL'),
(683, 265, 15, 232, 'JUSEPÍN'),
(684, 265, 15, 232, 'LA PICA'),
(685, 265, 15, 232, 'SAN VICENTE'),
(686, 266, 15, 232, 'APARICIO'),
(687, 266, 15, 232, 'ARAGUA DE MATURÍN'),
(688, 266, 15, 232, 'CHAGUAMAL'),
(689, 266, 15, 232, 'EL PINTO'),
(690, 266, 15, 232, 'GUANAGUANA'),
(691, 266, 15, 232, 'LA TOSCANA'),
(692, 266, 15, 232, 'TAGUAYA'),
(693, 267, 15, 232, 'CACHIPO'),
(694, 267, 15, 232, 'QUIRIQUIRE'),
(695, 268, 15, 232, 'SANTA BÁRBARA'),
(696, 269, 15, 232, 'BARRANCAS'),
(697, 269, 15, 232, 'LOS BARRANCOS DE FAJARDO'),
(698, 270, 15, 232, 'URACOA'),
(699, 271, 16, 232, 'ANTOLÍN DEL CAMPO'),
(700, 272, 16, 232, 'ARISMENDI'),
(701, 273, 16, 232, 'GARCÍA'),
(702, 273, 16, 232, 'FRANCISCO FAJARDO'),
(703, 274, 16, 232, 'BOLÍVAR'),
(704, 274, 16, 232, 'GUEVARA'),
(705, 274, 16, 232, 'MATASIETE'),
(706, 274, 16, 232, 'SANTA ANA'),
(707, 274, 16, 232, 'SUCRE'),
(708, 275, 16, 232, 'AGUIRRE'),
(709, 275, 16, 232, 'MANEIRO'),
(710, 276, 16, 232, 'ADRIÁN'),
(711, 276, 16, 232, 'JUAN GRIEGO'),
(712, 276, 16, 232, 'YAGUARAPARO'),
(713, 277, 16, 232, 'PORLAMAR'),
(714, 278, 16, 232, 'SAN FRANCISCO DE MACANAO'),
(715, 278, 16, 232, 'BOCA DE RÍO'),
(716, 279, 16, 232, 'TUBORES'),
(717, 279, 16, 232, 'LOS BALEALES'),
(718, 280, 16, 232, 'VICENTE FUENTES'),
(719, 280, 16, 232, 'VILLALBA'),
(720, 281, 16, 232, 'SAN JUAN BAUTISTA'),
(721, 281, 16, 232, 'ZABALA'),
(722, 283, 17, 232, 'CAPITAL ARAURE'),
(723, 283, 17, 232, 'RÍO ACARIGUA'),
(724, 284, 17, 232, 'CAPITAL ESTELLER'),
(725, 284, 17, 232, 'UVERAL'),
(726, 285, 17, 232, 'GUANARE'),
(727, 285, 17, 232, 'CÓRDOBA'),
(728, 285, 17, 232, 'SAN JOSÉ DE LA MONTAÑA'),
(729, 285, 17, 232, 'SAN JUAN DE GUANAGUANARE'),
(730, 285, 17, 232, 'VIRGEN DE LA COROMOTO'),
(731, 286, 17, 232, 'GUANARITO'),
(732, 286, 17, 232, 'TRINIDAD DE LA CAPILLA'),
(733, 286, 17, 232, 'DIVINA PASTORA'),
(734, 287, 17, 232, 'MONSEÑOR JOSÉ VICENTE DE UNDA'),
(735, 287, 17, 232, 'PEÑA BLANCA'),
(736, 288, 17, 232, 'CAPITAL OSPINO'),
(737, 288, 17, 232, 'APARICIÓN'),
(738, 288, 17, 232, 'LA ESTACIÓN'),
(739, 289, 17, 232, 'PÁEZ'),
(740, 289, 17, 232, 'PAYARA'),
(741, 289, 17, 232, 'PIMPINELA'),
(742, 289, 17, 232, 'RAMÓN PERAZA'),
(743, 290, 17, 232, 'PAPELÓN'),
(744, 290, 17, 232, 'CAÑO DELGADITO'),
(745, 291, 17, 232, 'SAN GENARO DE BOCONOITO'),
(746, 291, 17, 232, 'ANTOLÍN TOVAR'),
(747, 292, 17, 232, 'SAN RAFAEL DE ONOTO'),
(748, 292, 17, 232, 'SANTA FE'),
(749, 292, 17, 232, 'THERMO MORLES'),
(750, 293, 17, 232, 'SANTA ROSALÍA'),
(751, 293, 17, 232, 'FLORIDA'),
(752, 294, 17, 232, 'SUCRE'),
(753, 294, 17, 232, 'CONCEPCIÓN'),
(754, 294, 17, 232, 'SAN RAFAEL DE PALO ALZADO'),
(755, 294, 17, 232, 'UVENCIO ANTONIO VELÁSQUEZ'),
(756, 294, 17, 232, 'SAN JOSÉ DE SAGUAZ'),
(757, 294, 17, 232, 'VILLA ROSA'),
(758, 295, 17, 232, 'TURÉN'),
(759, 295, 17, 232, 'CANELONES'),
(760, 295, 17, 232, 'SANTA CRUZ'),
(761, 295, 17, 232, 'SAN ISIDRO LABRADOR'),
(762, 296, 18, 232, 'MARIÑO'),
(763, 296, 18, 232, 'RÓMULO GALLEGOS'),
(764, 297, 18, 232, 'SAN JOSÉ DE AEROCUAR'),
(765, 297, 18, 232, 'TAVERA ACOSTA'),
(766, 298, 18, 232, 'RÍO CARIBE'),
(767, 298, 18, 232, 'ANTONIO JOSÉ DE SUCRE'),
(768, 298, 18, 232, 'EL MORRO DE PUERTO SANTO'),
(769, 298, 18, 232, 'PUERTO SANTO'),
(770, 298, 18, 232, 'SAN JUAN DE LAS GALDONAS'),
(771, 299, 18, 232, 'EL PILAR'),
(772, 299, 18, 232, 'EL RINCÓN'),
(773, 299, 18, 232, 'GENERAL FRANCISCO ANTONIO VÁQUEZ'),
(774, 299, 18, 232, 'GUARAÚNOS'),
(775, 299, 18, 232, 'TUNAPUICITO'),
(776, 299, 18, 232, 'UNIÓN'),
(777, 300, 18, 232, 'SANTA CATALINA'),
(778, 300, 18, 232, 'SANTA ROSA'),
(779, 300, 18, 232, 'SANTA TERESA'),
(780, 300, 18, 232, 'BOLÍVAR'),
(781, 300, 18, 232, 'MARACAPANA'),
(782, 302, 18, 232, 'LIBERTAD'),
(783, 302, 18, 232, 'EL PAUJIL'),
(784, 302, 18, 232, 'YAGUARAPARO'),
(785, 303, 18, 232, 'CRUZ SALMERÓN ACOSTA'),
(786, 303, 18, 232, 'CHACOPATA'),
(787, 303, 18, 232, 'MANICUARE'),
(788, 304, 18, 232, 'TUNAPUY'),
(789, 304, 18, 232, 'CAMPO ELÍAS'),
(790, 305, 18, 232, 'IRAPA'),
(791, 305, 18, 232, 'CAMPO CLARO'),
(792, 305, 18, 232, 'MARAVAL'),
(793, 305, 18, 232, 'SAN ANTONIO DE IRAPA'),
(794, 305, 18, 232, 'SORO'),
(795, 306, 18, 232, 'MEJÍA'),
(796, 307, 18, 232, 'CUMANACOA'),
(797, 307, 18, 232, 'ARENAS'),
(798, 307, 18, 232, 'ARICAGUA'),
(799, 307, 18, 232, 'COGOLLAR'),
(800, 307, 18, 232, 'SAN FERNANDO'),
(801, 307, 18, 232, 'SAN LORENZO'),
(802, 308, 18, 232, 'VILLA FRONTADO (MUELLE DE CARIACO)'),
(803, 308, 18, 232, 'CATUARO'),
(804, 308, 18, 232, 'RENDÓN'),
(805, 308, 18, 232, 'SAN CRUZ'),
(806, 308, 18, 232, 'SANTA MARÍA'),
(807, 309, 18, 232, 'ALTAGRACIA'),
(808, 309, 18, 232, 'SANTA INÉS'),
(809, 309, 18, 232, 'VALENTÍN VALIENTE'),
(810, 309, 18, 232, 'AYACUCHO'),
(811, 309, 18, 232, 'SAN JUAN'),
(812, 309, 18, 232, 'RAÚL LEONI'),
(813, 309, 18, 232, 'GRAN MARISCAL'),
(814, 310, 18, 232, 'CRISTÓBAL COLÓN'),
(815, 310, 18, 232, 'BIDEAU'),
(816, 310, 18, 232, 'PUNTA DE PIEDRAS'),
(817, 310, 18, 232, 'GÜIRIA'),
(818, 341, 19, 232, 'ANDRÉS BELLO'),
(819, 342, 19, 232, 'ANTONIO RÓMULO COSTA'),
(820, 343, 19, 232, 'AYACUCHO'),
(821, 343, 19, 232, 'RIVAS BERTI'),
(822, 343, 19, 232, 'SAN PEDRO DEL RÍO'),
(823, 344, 19, 232, 'BOLÍVAR'),
(824, 344, 19, 232, 'PALOTAL'),
(825, 344, 19, 232, 'GENERAL JUAN VICENTE GÓMEZ'),
(826, 344, 19, 232, 'ISAÍAS MEDINA ANGARITA'),
(827, 345, 19, 232, 'CÁRDENAS'),
(828, 345, 19, 232, 'AMENODORO ÁNGEL LAMUS'),
(829, 345, 19, 232, 'LA FLORIDA'),
(830, 346, 19, 232, 'CÓRDOBA'),
(831, 347, 19, 232, 'FERNÁNDEZ FEO'),
(832, 347, 19, 232, 'ALBERTO ADRIANI'),
(833, 347, 19, 232, 'SANTO DOMINGO'),
(834, 348, 19, 232, 'FRANCISCO DE MIRANDA'),
(835, 349, 19, 232, 'GARCÍA DE HEVIA'),
(836, 349, 19, 232, 'BOCA DE GRITA'),
(837, 349, 19, 232, 'JOSÉ ANTONIO PÁEZ'),
(838, 350, 19, 232, 'GUÁSIMOS'),
(839, 351, 19, 232, 'INDEPENDENCIA'),
(840, 351, 19, 232, 'JUAN GERMÁN ROSCIO'),
(841, 351, 19, 232, 'ROMÁN CÁRDENAS'),
(842, 352, 19, 232, 'JÁUREGUI'),
(843, 352, 19, 232, 'EMILIO CONSTANTINO GUERRERO'),
(844, 352, 19, 232, 'MONSEÑOR MIGUEL ANTONIO SALAS'),
(845, 353, 19, 232, 'JOSÉ MARÍA VARGAS'),
(846, 354, 19, 232, 'JUNÍN'),
(847, 354, 19, 232, 'LA PETRÓLEA'),
(848, 354, 19, 232, 'QUINIMARÍ'),
(849, 354, 19, 232, 'BRAMÓN'),
(850, 355, 19, 232, 'LIBERTAD'),
(851, 355, 19, 232, 'CIPRIANO CASTRO'),
(852, 355, 19, 232, 'MANUEL FELIPE RUGELES'),
(853, 356, 19, 232, 'LIBERTADOR'),
(854, 356, 19, 232, 'DORADAS'),
(855, 356, 19, 232, 'EMETERIO OCHOA'),
(856, 356, 19, 232, 'SAN JOAQUÍN DE NAVAY'),
(857, 357, 19, 232, 'LOBATERA'),
(858, 357, 19, 232, 'CONSTITUCIÓN'),
(859, 358, 19, 232, 'MICHELENA'),
(860, 359, 19, 232, 'PANAMERICANO'),
(861, 359, 19, 232, 'LA PALMITA'),
(862, 360, 19, 232, 'PEDRO MARÍA UREÑA'),
(863, 360, 19, 232, 'NUEVA ARCADIA'),
(864, 361, 19, 232, 'DELICIAS'),
(865, 361, 19, 232, 'PECAYA'),
(866, 362, 19, 232, 'SAMUEL DARÍO MALDONADO'),
(867, 362, 19, 232, 'BOCONÓ'),
(868, 362, 19, 232, 'HERNÁNDEZ'),
(869, 363, 19, 232, 'LA CONCORDIA'),
(870, 363, 19, 232, 'SAN JUAN BAUTISTA'),
(871, 363, 19, 232, 'PEDRO MARÍA MORANTES'),
(872, 363, 19, 232, 'SAN SEBASTIÁN'),
(873, 363, 19, 232, 'DR. FRANCISCO ROMERO LOBO'),
(874, 364, 19, 232, 'SEBORUCO'),
(875, 365, 19, 232, 'SIMÓN RODRÍGUEZ'),
(876, 366, 19, 232, 'SUCRE'),
(877, 366, 19, 232, 'ELEAZAR LÓPEZ CONTRERAS'),
(878, 366, 19, 232, 'SAN PABLO'),
(879, 367, 19, 232, 'TORBES'),
(880, 368, 19, 232, 'URIBANTE'),
(881, 368, 19, 232, 'CÁRDENAS'),
(882, 368, 19, 232, 'JUAN PABLO PEÑALOSA'),
(883, 368, 19, 232, 'POTOSÍ'),
(884, 369, 19, 232, 'SAN JUDAS TADEO'),
(885, 370, 20, 232, 'ARAGUANEY'),
(886, 370, 20, 232, 'EL JAGUITO'),
(887, 370, 20, 232, 'LA ESPERANZA'),
(888, 370, 20, 232, 'SANTA ISABEL'),
(889, 371, 20, 232, 'BOCONÓ'),
(890, 371, 20, 232, 'EL CARMEN'),
(891, 371, 20, 232, 'MOSQUEY'),
(892, 371, 20, 232, 'AYACUCHO'),
(893, 371, 20, 232, 'BURBUSAY'),
(894, 371, 20, 232, 'GENERAL RIBAS'),
(895, 371, 20, 232, 'GUARAMACAL'),
(896, 371, 20, 232, 'VEGA DE GUARAMACAL'),
(897, 371, 20, 232, 'MONSEÑOR JÁUREGUI'),
(898, 371, 20, 232, 'RAFAEL RANGEL'),
(899, 371, 20, 232, 'SAN MIGUEL'),
(900, 371, 20, 232, 'SAN JOSÉ'),
(901, 372, 20, 232, 'SABANA GRANDE'),
(902, 372, 20, 232, 'CHEREGÜÉ'),
(903, 372, 20, 232, 'GRANADOS'),
(904, 373, 20, 232, 'ARNOLDO GABALDÓN'),
(905, 373, 20, 232, 'BOLIVIA'),
(906, 373, 20, 232, 'CARRILLO'),
(907, 373, 20, 232, 'CEGARRA'),
(908, 373, 20, 232, 'CHEJENDÉ'),
(909, 373, 20, 232, 'MANUEL SALVADOR ULLOA'),
(910, 373, 20, 232, 'SAN JOSÉ'),
(911, 374, 20, 232, 'CARACHE'),
(912, 374, 20, 232, 'LA CONCEPCIÓN'),
(913, 374, 20, 232, 'CUICAS'),
(914, 374, 20, 232, 'PANAMERICANA'),
(915, 374, 20, 232, 'SANTA CRUZ'),
(916, 375, 20, 232, 'ESCUQUE'),
(917, 375, 20, 232, 'LA UNIÓN'),
(918, 375, 20, 232, 'SANTA RITA'),
(919, 375, 20, 232, 'SABANA LIBRE'),
(920, 376, 20, 232, 'EL SOCORRO'),
(921, 376, 20, 232, 'LOS CAPRICHOS'),
(922, 376, 20, 232, 'ANTONIO JOSÉ DE SUCRE'),
(923, 377, 20, 232, 'CAMPO ELÍAS'),
(924, 377, 20, 232, 'ARNOLDO GABALDÓN'),
(925, 378, 20, 232, 'SANTA APOLONIA'),
(926, 378, 20, 232, 'EL PROGRESO'),
(927, 378, 20, 232, 'LA CEIBA'),
(928, 378, 20, 232, 'TRES DE FEBRERO'),
(929, 379, 20, 232, 'EL DIVIDIVE'),
(930, 379, 20, 232, 'AGUA SANTA'),
(931, 379, 20, 232, 'AGUA CALIENTE'),
(932, 379, 20, 232, 'EL CENIZO'),
(933, 379, 20, 232, 'VALERITA'),
(934, 380, 20, 232, 'MONTE CARMELO'),
(935, 380, 20, 232, 'BUENA VISTA'),
(936, 380, 20, 232, 'SANTA MARÍA DEL HORCÓN'),
(937, 381, 20, 232, 'MOTATÁN'),
(938, 381, 20, 232, 'EL BAÑO'),
(939, 381, 20, 232, 'JALISCO'),
(940, 382, 20, 232, 'PAMPÁN'),
(941, 382, 20, 232, 'FLOR DE PATRIA'),
(942, 382, 20, 232, 'LA PAZ'),
(943, 382, 20, 232, 'SANTA ANA'),
(944, 383, 20, 232, 'PAMPANITO'),
(945, 383, 20, 232, 'LA CONCEPCIÓN'),
(946, 383, 20, 232, 'PAMPANITO II'),
(947, 384, 20, 232, 'BETIJOQUE'),
(948, 384, 20, 232, 'JOSÉ GREGORIO HERNÁNDEZ'),
(949, 384, 20, 232, 'LA PUEBLITA'),
(950, 384, 20, 232, 'LOS CEDROS'),
(951, 385, 20, 232, 'CARVAJAL'),
(952, 385, 20, 232, 'CAMPO ALEGRE'),
(953, 385, 20, 232, 'ANTONIO NICOLÁS BRICEÑO'),
(954, 385, 20, 232, 'JOSÉ LEONARDO SUÁREZ'),
(955, 386, 20, 232, 'SABANA DE MENDOZA'),
(956, 386, 20, 232, 'JUNÍN'),
(957, 386, 20, 232, 'VALMORE RODRÍGUEZ'),
(958, 386, 20, 232, 'EL PARAÍSO'),
(959, 387, 20, 232, 'ANDRÉS LINARES'),
(960, 387, 20, 232, 'CHIQUINQUIRÁ'),
(961, 387, 20, 232, 'CRISTÓBAL MENDOZA'),
(962, 387, 20, 232, 'CRUZ CARRILLO'),
(963, 387, 20, 232, 'MATRIZ'),
(964, 387, 20, 232, 'MONSEÑOR CARRILLO'),
(965, 387, 20, 232, 'TRES ESQUINAS'),
(966, 388, 20, 232, 'CABIMBÚ'),
(967, 388, 20, 232, 'JAJÓ'),
(968, 388, 20, 232, 'LA MESA DE ESNUJAQUE'),
(969, 388, 20, 232, 'SANTIAGO'),
(970, 388, 20, 232, 'TUÑAME'),
(971, 388, 20, 232, 'LA QUEBRADA'),
(972, 389, 20, 232, 'JUAN IGNACIO MONTILLA'),
(973, 389, 20, 232, 'LA BEATRIZ'),
(974, 389, 20, 232, 'LA PUERTA'),
(975, 389, 20, 232, 'MENDOZA DEL VALLE DE MOMBOY'),
(976, 389, 20, 232, 'MERCEDES DÍAZ'),
(977, 389, 20, 232, 'SAN LUIS'),
(978, 390, 21, 232, 'CARABALLEDA'),
(979, 390, 21, 232, 'CARAYACA'),
(980, 390, 21, 232, 'CARLOS SOUBLETTE'),
(981, 390, 21, 232, 'CARUAO CHUSPA'),
(982, 390, 21, 232, 'CATIA LA MAR'),
(983, 390, 21, 232, 'EL JUNKO'),
(984, 390, 21, 232, 'LA GUAIRA'),
(985, 390, 21, 232, 'MACUTO'),
(986, 390, 21, 232, 'MAIQUETÍA'),
(987, 390, 21, 232, 'NAIGUATÁ'),
(988, 390, 21, 232, 'URIMARE'),
(989, 391, 22, 232, 'ARÍSTIDES BASTIDAS'),
(990, 392, 22, 232, 'BOLÍVAR'),
(991, 407, 22, 232, 'CHIVACOA'),
(992, 407, 22, 232, 'CAMPO ELÍAS'),
(993, 408, 22, 232, 'COCOROTE'),
(994, 409, 22, 232, 'INDEPENDENCIA'),
(995, 410, 22, 232, 'JOSÉ ANTONIO PÁEZ'),
(996, 411, 22, 232, 'LA TRINIDAD'),
(997, 412, 22, 232, 'MANUEL MONGE'),
(998, 413, 22, 232, 'SALÓM'),
(999, 413, 22, 232, 'TEMERLA'),
(1000, 413, 22, 232, 'NIRGUA'),
(1001, 414, 22, 232, 'SAN ANDRÉS'),
(1002, 414, 22, 232, 'YARITAGUA'),
(1003, 415, 22, 232, 'SAN JAVIER'),
(1004, 415, 22, 232, 'ALBARICO'),
(1005, 415, 22, 232, 'SAN FELIPE'),
(1006, 416, 22, 232, 'SUCRE'),
(1007, 417, 22, 232, 'URACHICHE'),
(1008, 418, 22, 232, 'EL GUAYABO'),
(1009, 418, 22, 232, 'FARRIAR'),
(1010, 441, 23, 232, 'ISLA DE TOAS'),
(1011, 441, 23, 232, 'MONAGAS'),
(1012, 442, 23, 232, 'SAN TIMOTEO'),
(1013, 442, 23, 232, 'GENERAL URDANETA'),
(1014, 442, 23, 232, 'LIBERTADOR'),
(1015, 442, 23, 232, 'MARCELINO BRICEÑO'),
(1016, 442, 23, 232, 'PUEBLO NUEVO'),
(1017, 442, 23, 232, 'MANUEL GUANIPA MATOS'),
(1018, 443, 23, 232, 'AMBROSIO'),
(1019, 443, 23, 232, 'CARMEN HERRERA'),
(1020, 443, 23, 232, 'LA ROSA'),
(1021, 443, 23, 232, 'GERMÁN RÍOS LINARES'),
(1022, 443, 23, 232, 'SAN BENITO'),
(1023, 443, 23, 232, 'RÓMULO BETANCOURT'),
(1024, 443, 23, 232, 'JORGE HERNÁNDEZ'),
(1025, 443, 23, 232, 'PUNTA GORDA'),
(1026, 443, 23, 232, 'ARÍSTIDES CALVANI'),
(1027, 444, 23, 232, 'ENCONTRADOS'),
(1028, 444, 23, 232, 'UDÓN PÉREZ'),
(1029, 445, 23, 232, 'MORALITO'),
(1030, 445, 23, 232, 'SAN CARLOS DEL ZULIA'),
(1031, 445, 23, 232, 'SANTA CRUZ DEL ZULIA'),
(1032, 445, 23, 232, 'SANTA BÁRBARA'),
(1033, 445, 23, 232, 'URRIBARRÍ'),
(1034, 446, 23, 232, 'CARLOS QUEVEDO'),
(1035, 446, 23, 232, 'FRANCISCO JAVIER PULGAR'),
(1036, 446, 23, 232, 'SIMÓN RODRÍGUEZ'),
(1037, 446, 23, 232, 'GUAMO-GAVILANES'),
(1038, 448, 23, 232, 'LA CONCEPCIÓN'),
(1039, 448, 23, 232, 'SAN JOSÉ'),
(1040, 448, 23, 232, 'MARIANO PARRA LEÓN'),
(1041, 448, 23, 232, 'JOSÉ RAMÓN YÉPEZ'),
(1042, 449, 23, 232, 'JESÚS MARÍA SEMPRÚN'),
(1043, 449, 23, 232, 'BARÍ'),
(1044, 450, 23, 232, 'CONCEPCIÓN'),
(1045, 450, 23, 232, 'ANDRÉS BELLO'),
(1046, 450, 23, 232, 'CHIQUINQUIRÁ'),
(1047, 450, 23, 232, 'EL CARMELO'),
(1048, 450, 23, 232, 'POTRERITOS'),
(1049, 451, 23, 232, 'LIBERTAD'),
(1050, 451, 23, 232, 'ALONSO DE OJEDA'),
(1051, 451, 23, 232, 'VENEZUELA'),
(1052, 451, 23, 232, 'ELEAZAR LÓPEZ CONTRERAS'),
(1053, 451, 23, 232, 'CAMPO LARA'),
(1054, 452, 23, 232, 'BARTOLOMÉ DE LAS CASAS'),
(1055, 452, 23, 232, 'LIBERTAD'),
(1056, 452, 23, 232, 'RÍO NEGRO'),
(1057, 452, 23, 232, 'SAN JOSÉ DE PERIJÁ'),
(1058, 453, 23, 232, 'SAN RAFAEL'),
(1059, 453, 23, 232, 'LA SIERRITA'),
(1060, 453, 23, 232, 'LAS PARCELAS'),
(1061, 453, 23, 232, 'LUIS DE VICENTE'),
(1062, 453, 23, 232, 'MONSEÑOR MARCOS SERGIO GODOY'),
(1063, 453, 23, 232, 'RICAURTE'),
(1064, 453, 23, 232, 'TAMARE'),
(1065, 454, 23, 232, 'ANTONIO BORJAS ROMERO'),
(1066, 454, 23, 232, 'BOLÍVAR'),
(1067, 454, 23, 232, 'CACIQUE MARA'),
(1068, 454, 23, 232, 'CARRACCIOLO PARRA PÉREZ'),
(1069, 454, 23, 232, 'CECILIO ACOSTA'),
(1070, 454, 23, 232, 'CRISTO DE ARANZA'),
(1071, 454, 23, 232, 'COQUIVACOA'),
(1072, 454, 23, 232, 'CHIQUINQUIRÁ'),
(1073, 454, 23, 232, 'FRANCISCO EUGENIO BUSTAMANTE'),
(1074, 454, 23, 232, 'IDELFONZO VÁSQUEZ'),
(1075, 454, 23, 232, 'JUANA DE ÁVILA'),
(1076, 454, 23, 232, 'LUIS HURTADO HIGUERA'),
(1077, 454, 23, 232, 'MANUEL DAGNINO'),
(1078, 454, 23, 232, 'OLEGARIO VILLALOBOS'),
(1079, 454, 23, 232, 'RAÚL LEONI'),
(1080, 454, 23, 232, 'SANTA LUCÍA'),
(1081, 454, 23, 232, 'VENANCIO PULGAR'),
(1082, 454, 23, 232, 'SAN ISIDRO'),
(1083, 455, 23, 232, 'ALTAGRACIA'),
(1084, 455, 23, 232, 'FARÍA'),
(1085, 455, 23, 232, 'ANA MARÍA CAMPOS'),
(1086, 455, 23, 232, 'SAN ANTONIO'),
(1087, 455, 23, 232, 'SAN JOSÉ'),
(1088, 456, 23, 232, 'DONALDO GARCÍA'),
(1089, 456, 23, 232, 'EL ROSARIO'),
(1090, 456, 23, 232, 'SIXTO ZAMBRANO'),
(1091, 457, 23, 232, 'SAN FRANCISCO'),
(1092, 457, 23, 232, 'EL BAJO'),
(1093, 457, 23, 232, 'DOMITILA FLORES'),
(1094, 457, 23, 232, 'FRANCISCO OCHOA'),
(1095, 457, 23, 232, 'LOS CORTIJOS'),
(1096, 457, 23, 232, 'MARCIAL HERNÁNDEZ'),
(1097, 458, 23, 232, 'SANTA RITA'),
(1098, 458, 23, 232, 'EL MENE'),
(1099, 458, 23, 232, 'PEDRO LUCAS URRIBARRÍ'),
(1100, 458, 23, 232, 'JOSÉ CENOBIO URRIBARRÍ'),
(1101, 459, 23, 232, 'RAFAEL MARIA BARALT'),
(1102, 459, 23, 232, 'MANUEL MANRIQUE'),
(1103, 459, 23, 232, 'RAFAEL URDANETA'),
(1104, 460, 23, 232, 'BOBURES'),
(1105, 460, 23, 232, 'GIBRALTAR'),
(1106, 460, 23, 232, 'HERAS'),
(1107, 460, 23, 232, 'MONSEÑOR ARTURO ÁLVAREZ'),
(1108, 460, 23, 232, 'RÓMULO GALLEGOS'),
(1109, 460, 23, 232, 'EL BATEY'),
(1110, 461, 23, 232, 'RAFAEL URDANETA'),
(1111, 461, 23, 232, 'LA VICTORIA'),
(1112, 461, 23, 232, 'RAÚL CUENCA'),
(1113, 447, 23, 232, 'SINAMAICA'),
(1114, 447, 23, 232, 'ALTA GUAJIRA'),
(1115, 447, 23, 232, 'ELÍAS SÁNCHEZ RUBIO'),
(1116, 447, 23, 232, 'GUAJIRA'),
(1117, 462, 24, 232, 'ALTAGRACIA'),
(1118, 462, 24, 232, 'ANTÍMANO'),
(1119, 462, 24, 232, 'CARICUAO'),
(1120, 462, 24, 232, 'CATEDRAL'),
(1121, 462, 24, 232, 'COCHE'),
(1122, 462, 24, 232, 'EL JUNQUITO'),
(1123, 462, 24, 232, 'EL PARAÍSO'),
(1124, 462, 24, 232, 'EL RECREO'),
(1125, 462, 24, 232, 'EL VALLE'),
(1126, 462, 24, 232, 'LA CANDELARIA'),
(1127, 462, 24, 232, 'LA PASTORA'),
(1128, 462, 24, 232, 'LA VEGA'),
(1129, 462, 24, 232, 'MACARAO'),
(1130, 462, 24, 232, 'SAN AGUSTÍN'),
(1131, 462, 24, 232, 'SAN BERNARDINO'),
(1132, 462, 24, 232, 'SAN JOSÉ'),
(1133, 462, 24, 232, 'SAN JUAN'),
(1134, 462, 24, 232, 'SAN PEDRO'),
(1135, 462, 24, 232, 'SANTA ROSALÍA'),
(1136, 462, 24, 232, 'SANTA TERESA'),
(1137, 462, 24, 232, 'SUCRE (CATIA)'),
(1138, 462, 24, 232, '23 DE ENERO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_corte`
--

CREATE TABLE `pedido_corte` (
  `corte_codig` int(11) NOT NULL,
  `pdlis_codig` int(11) NOT NULL,
  `traba_codig` int(11) NOT NULL,
  `corte_fentr` date NOT NULL,
  `corte_freci` date NOT NULL,
  `ecort_codig` int(11) NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `corte_fcrea` date NOT NULL,
  `corte_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_corte_estatu`
--

CREATE TABLE `pedido_corte_estatu` (
  `ecort_codig` int(11) NOT NULL,
  `ecort_descr` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `ecort_fcrea` date NOT NULL,
  `ecort_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Volcado de datos para la tabla `pedido_corte_estatu`
--

INSERT INTO `pedido_corte_estatu` (`ecort_codig`, `ecort_descr`, `usuar_codig`, `ecort_fcrea`, `ecort_hcrea`) VALUES
(1, 'PENDIENTE', 3, '2018-12-05', '13:00:09'),
(2, 'ENTREGADO', 3, '2018-12-05', '13:00:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_costo`
--

CREATE TABLE `pedido_costo` (
  `costo_codig` int(11) NOT NULL,
  `costo_descr` varchar(50) NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `costo_fcrea` date NOT NULL,
  `costo_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pedido_costo`
--

INSERT INTO `pedido_costo` (`costo_codig`, `costo_descr`, `usuar_codig`, `costo_fcrea`, `costo_hcrea`) VALUES
(3, 'BORDADO EN MANGAS', 3, '2019-01-17', '20:28:13'),
(4, 'TIRAS EN MANGAS', 3, '2019-01-17', '20:29:10'),
(5, 'URGENCIA', 25, '2019-01-31', '10:42:56'),
(7, 'RECARGO POR CANTIDAD INFERIOR A 20 UNIDADES', 25, '2019-02-22', '11:41:31'),
(8, 'INSIGNIA', 22, '2019-03-11', '12:55:00'),
(9, 'ESPECIALIDAD EN LA MANGA', 22, '2019-03-11', '12:55:34'),
(10, 'EXTRA', 25, '2019-04-01', '00:27:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_deduccion`
--

CREATE TABLE `pedido_deduccion` (
  `deduc_codig` int(11) NOT NULL,
  `deduc_descr` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `deduc_fcrea` date NOT NULL,
  `deduc_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Volcado de datos para la tabla `pedido_deduccion`
--

INSERT INTO `pedido_deduccion` (`deduc_codig`, `deduc_descr`, `usuar_codig`, `deduc_fcrea`, `deduc_hcrea`) VALUES
(1, 'DESCUENTO', 3, '2019-01-31', '16:40:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_detalle_listado_imagen`
--

CREATE TABLE `pedido_detalle_listado_imagen` (
  `pdlim_codig` int(11) NOT NULL,
  `pedid_codig` int(11) NOT NULL,
  `pdeta_codig` int(11) NOT NULL,
  `pdlis_codig` int(11) NOT NULL,
  `pdlim_timag` int(11) NOT NULL,
  `emoji_codig` int(11) NOT NULL,
  `ssimp_codig` int(11) NOT NULL,
  `pdlim_texto` text COLLATE utf8mb4_bin NOT NULL,
  `pdlim_rimag` text COLLATE utf8mb4_bin NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `pdlim_fcrea` date NOT NULL,
  `pdlim_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Volcado de datos para la tabla `pedido_detalle_listado_imagen`
--

INSERT INTO `pedido_detalle_listado_imagen` (`pdlim_codig`, `pedid_codig`, `pdeta_codig`, `pdlis_codig`, `pdlim_timag`, `emoji_codig`, `ssimp_codig`, `pdlim_texto`, `pdlim_rimag`, `usuar_codig`, `pdlim_fcrea`, `pdlim_hcrea`) VALUES
(37, 39, 45, 65, 1, 10, 0, '', '', 23, '2019-01-28', '15:35:59'),
(38, 39, 45, 66, 1, 27, 0, '', '', 23, '2019-01-28', '15:40:35'),
(39, 39, 45, 67, 1, 26, 0, '', '', 23, '2019-01-28', '15:42:31'),
(40, 39, 45, 68, 1, 24, 0, '', '', 23, '2019-01-28', '15:44:39'),
(41, 39, 45, 69, 4, 0, 0, '', '', 23, '2019-01-28', '15:52:40'),
(42, 39, 45, 70, 1, 7, 0, '', '', 23, '2019-01-28', '15:55:17'),
(43, 39, 45, 71, 1, 20, 0, '', '', 23, '2019-01-28', '15:57:09'),
(44, 39, 45, 72, 1, 31, 0, '', '', 23, '2019-01-28', '16:00:30'),
(45, 39, 45, 73, 1, 4, 0, '', '', 23, '2019-01-28', '16:04:24'),
(46, 39, 46, 74, 1, 14, 0, '', '', 23, '2019-01-28', '16:35:31'),
(47, 39, 46, 75, 1, 28, 0, '', '', 23, '2019-01-28', '16:38:45'),
(48, 39, 46, 76, 1, 6, 0, '', '', 23, '2019-01-28', '16:42:02'),
(49, 39, 46, 77, 1, 26, 0, '', '', 23, '2019-01-28', '16:46:10'),
(50, 39, 46, 78, 1, 14, 0, '', '', 23, '2019-01-28', '16:47:51'),
(51, 39, 46, 79, 1, 4, 0, '', '', 23, '2019-01-28', '16:49:38'),
(52, 39, 46, 80, 1, 23, 0, '', '', 23, '2019-01-28', '16:52:00'),
(53, 39, 46, 81, 1, 22, 0, '', '', 23, '2019-01-28', '16:54:18'),
(54, 39, 46, 82, 3, 0, 5, '', '', 23, '2019-01-28', '16:56:42'),
(55, 39, 46, 83, 1, 19, 0, '', '', 23, '2019-01-28', '16:58:35'),
(56, 39, 46, 84, 1, 16, 0, '', '', 23, '2019-01-28', '17:00:59'),
(57, 39, 46, 85, 1, 7, 0, '', '', 23, '2019-01-28', '17:02:13'),
(58, 39, 46, 86, 1, 7, 0, '', '', 23, '2019-01-28', '17:05:17'),
(59, 39, 46, 87, 1, 10, 0, '', '', 23, '2019-01-28', '17:07:04'),
(60, 39, 46, 88, 1, 18, 0, '', '', 23, '2019-01-28', '17:08:40'),
(61, 39, 46, 89, 1, 17, 0, '', '', 23, '2019-01-28', '17:11:05'),
(62, 39, 46, 90, 4, 0, 0, '', '', 23, '2019-01-28', '17:12:38'),
(63, 39, 46, 91, 1, 12, 0, '', '', 23, '2019-01-28', '17:14:08'),
(64, 39, 46, 92, 1, 15, 0, '', '', 23, '2019-01-28', '17:16:14'),
(65, 39, 46, 93, 1, 13, 0, '', '', 23, '2019-01-28', '17:18:53'),
(66, 39, 46, 94, 1, 9, 0, '', '', 23, '2019-01-28', '17:20:14'),
(67, 39, 46, 95, 1, 8, 0, '', '', 23, '2019-01-28', '17:22:04'),
(68, 39, 46, 96, 1, 4, 0, '', '', 23, '2019-01-28', '17:23:39'),
(69, 39, 46, 82, 1, 21, 0, '', '', 23, '2019-01-29', '21:18:24'),
(72, 47, 48, 99, 1, 33, 0, '', '', 22, '2019-02-05', '11:03:54'),
(73, 47, 48, 100, 1, 24, 0, '', '', 22, '2019-02-05', '11:05:46'),
(74, 47, 48, 101, 1, 28, 0, '', '', 22, '2019-02-05', '11:07:27'),
(75, 47, 48, 102, 1, 41, 0, '', '', 22, '2019-02-05', '11:13:05'),
(76, 47, 48, 103, 1, 71, 0, '', '', 22, '2019-02-05', '11:26:30'),
(77, 47, 48, 103, 1, 32, 0, '', '', 22, '2019-02-05', '13:36:21'),
(78, 47, 48, 104, 1, 37, 0, '', '', 22, '2019-02-05', '13:38:41'),
(79, 47, 48, 104, 1, 11, 0, '', '', 22, '2019-02-05', '13:38:41'),
(80, 47, 48, 105, 1, 64, 0, '', '', 22, '2019-02-05', '13:53:00'),
(81, 47, 48, 105, 1, 54, 0, '', '', 22, '2019-02-05', '13:53:00'),
(82, 47, 48, 106, 1, 8, 0, '', '', 22, '2019-02-05', '13:57:36'),
(83, 47, 48, 106, 1, 34, 0, '', '', 22, '2019-02-05', '13:57:36'),
(84, 47, 48, 107, 1, 8, 0, '', '', 22, '2019-02-05', '14:01:16'),
(85, 47, 48, 108, 1, 68, 0, '', '', 22, '2019-02-05', '14:03:45'),
(86, 47, 48, 109, 1, 66, 0, '', '', 22, '2019-02-05', '14:06:01'),
(87, 47, 48, 110, 1, 67, 0, '', '', 22, '2019-02-05', '14:07:29'),
(88, 47, 48, 111, 1, 65, 0, '', '', 22, '2019-02-05', '14:10:15'),
(89, 47, 48, 112, 1, 14, 0, '', '', 22, '2019-02-05', '14:19:12'),
(90, 47, 48, 112, 1, 69, 0, '', '', 22, '2019-02-05', '14:19:12'),
(91, 47, 48, 113, 1, 70, 0, '', '', 22, '2019-02-05', '14:21:14'),
(92, 47, 48, 113, 1, 65, 0, '', '', 22, '2019-02-05', '14:21:14'),
(93, 47, 48, 114, 1, 9, 0, '', '', 22, '2019-02-05', '14:25:52'),
(94, 47, 48, 115, 1, 76, 0, '', '', 22, '2019-02-05', '14:28:52'),
(95, 47, 48, 116, 1, 76, 0, '', '', 22, '2019-02-05', '14:30:00'),
(96, 47, 48, 117, 1, 16, 0, '', '', 22, '2019-02-05', '14:31:36'),
(97, 47, 48, 118, 1, 53, 0, '', '', 22, '2019-02-05', '14:44:57'),
(98, 47, 48, 119, 1, 58, 0, '', '', 22, '2019-02-05', '14:46:41'),
(99, 47, 48, 120, 1, 55, 0, '', '', 22, '2019-02-05', '14:50:49'),
(100, 47, 48, 121, 1, 57, 0, '', '', 22, '2019-02-05', '14:52:09'),
(101, 47, 48, 122, 1, 38, 0, '', '', 22, '2019-02-05', '14:54:24'),
(102, 47, 48, 123, 1, 28, 0, '', '', 22, '2019-02-05', '14:56:12'),
(103, 47, 48, 123, 1, 47, 0, '', '', 22, '2019-02-05', '14:56:12'),
(104, 47, 48, 124, 1, 33, 0, '', '', 22, '2019-02-05', '14:57:10'),
(105, 47, 48, 125, 4, 0, 0, '', '', 22, '2019-02-05', '14:58:39'),
(106, 47, 48, 126, 1, 61, 0, '', '', 22, '2019-02-05', '14:59:45'),
(107, 47, 48, 127, 1, 60, 0, '', '', 22, '2019-02-05', '15:21:49'),
(108, 47, 48, 128, 1, 56, 0, '', '', 22, '2019-02-05', '17:06:48'),
(109, 47, 48, 129, 1, 42, 0, '', '', 22, '2019-02-05', '17:08:26'),
(110, 47, 48, 130, 1, 49, 0, '', '', 22, '2019-02-05', '17:09:54'),
(111, 47, 48, 131, 1, 50, 0, '', '', 22, '2019-02-05', '17:11:20'),
(112, 47, 48, 132, 1, 46, 0, '', '', 22, '2019-02-05', '17:13:10'),
(113, 47, 48, 133, 1, 38, 0, '', '', 22, '2019-02-05', '17:16:00'),
(114, 47, 48, 133, 1, 51, 0, '', '', 22, '2019-02-05', '17:16:00'),
(115, 48, 49, 134, 4, 0, 0, '', '', 25, '2019-02-05', '20:08:52'),
(116, 47, 48, 135, 1, 52, 0, '', '', 22, '2019-02-05', '20:12:19'),
(117, 47, 48, 136, 1, 59, 0, '', '', 22, '2019-02-05', '20:15:37'),
(118, 47, 48, 136, 1, 54, 0, '', '', 22, '2019-02-05', '20:15:37'),
(119, 47, 48, 137, 1, 51, 0, '', '', 22, '2019-02-05', '20:29:47'),
(120, 47, 48, 138, 1, 74, 0, '', '', 22, '2019-02-05', '20:32:52'),
(121, 47, 48, 139, 1, 45, 0, '', '', 22, '2019-02-05', '20:35:11'),
(122, 47, 48, 140, 1, 77, 0, '', '', 22, '2019-02-05', '20:54:19'),
(123, 47, 48, 141, 1, 72, 0, '', '', 22, '2019-02-05', '20:57:39'),
(124, 47, 48, 142, 4, 0, 0, '', '', 22, '2019-02-05', '20:58:53'),
(125, 47, 48, 143, 1, 28, 0, '', '', 22, '2019-02-05', '21:00:36'),
(126, 47, 48, 143, 1, 75, 0, '', '', 22, '2019-02-05', '21:00:36'),
(127, 47, 48, 144, 1, 77, 0, '', '', 22, '2019-02-05', '21:03:51'),
(128, 47, 48, 145, 1, 73, 0, '', '', 22, '2019-02-05', '21:06:14'),
(129, 47, 48, 146, 1, 62, 0, '', '', 22, '2019-02-05', '21:09:22'),
(130, 47, 48, 147, 1, 43, 0, '', '', 22, '2019-02-05', '21:10:50'),
(131, 47, 48, 148, 1, 44, 0, '', '', 22, '2019-02-05', '21:11:56'),
(132, 47, 48, 149, 1, 63, 0, '', '', 22, '2019-02-05', '21:13:00'),
(133, 48, 49, 150, 4, 0, 0, '', '', 25, '2019-02-06', '17:17:26'),
(134, 48, 49, 151, 4, 0, 0, '', '', 25, '2019-02-06', '17:18:29'),
(135, 48, 49, 152, 4, 0, 0, '', '', 25, '2019-02-06', '17:19:08'),
(136, 48, 49, 153, 4, 0, 0, '', '', 25, '2019-02-06', '17:19:30'),
(137, 48, 49, 154, 4, 0, 0, '', '', 25, '2019-02-06', '17:20:45'),
(138, 48, 49, 155, 4, 0, 0, '', '', 25, '2019-02-06', '17:22:24'),
(139, 48, 49, 156, 4, 0, 0, '', '', 25, '2019-02-06', '17:22:57'),
(140, 48, 49, 157, 4, 0, 0, '', '', 25, '2019-02-06', '17:23:35'),
(141, 48, 49, 158, 4, 0, 0, '', '', 25, '2019-02-06', '17:24:14'),
(142, 48, 49, 159, 4, 0, 0, '', '', 25, '2019-02-06', '17:24:45'),
(143, 48, 49, 160, 4, 0, 0, '', '', 25, '2019-02-06', '17:25:19'),
(144, 48, 49, 161, 4, 0, 0, '', '', 25, '2019-02-06', '17:27:31'),
(150, 56, 54, 167, 2, 0, 0, '', 'files/detalle/listado/20190208_165911_0.jpg', 28, '2019-02-08', '16:59:11'),
(151, 56, 54, 168, 4, 0, 0, '', '', 28, '2019-02-08', '17:00:49'),
(152, 56, 54, 169, 4, 0, 0, '', '', 28, '2019-02-08', '17:01:57'),
(153, 56, 54, 170, 1, 76, 0, '', '', 28, '2019-02-08', '17:03:11'),
(155, 58, 56, 172, 4, 0, 0, '', '', 27, '2019-02-08', '19:21:20'),
(156, 58, 56, 173, 4, 0, 0, '', '', 27, '2019-02-08', '19:24:57'),
(157, 58, 56, 174, 4, 0, 0, '', '', 27, '2019-02-08', '19:26:55'),
(158, 58, 56, 175, 4, 0, 0, '', '', 27, '2019-02-08', '19:34:06'),
(159, 58, 56, 176, 4, 0, 0, '', '', 27, '2019-02-08', '19:35:43'),
(160, 58, 56, 177, 4, 0, 0, '', '', 27, '2019-02-08', '19:38:46'),
(161, 58, 56, 178, 4, 0, 0, '', '', 27, '2019-02-08', '19:40:19'),
(162, 58, 56, 179, 4, 0, 0, '', '', 27, '2019-02-08', '19:42:19'),
(163, 58, 56, 180, 4, 0, 0, '', '', 27, '2019-02-08', '19:44:26'),
(164, 58, 56, 181, 4, 0, 0, '', '', 27, '2019-02-08', '19:45:47'),
(165, 58, 56, 182, 4, 0, 0, '', '', 27, '2019-02-08', '19:47:30'),
(166, 58, 56, 183, 4, 0, 0, '', '', 27, '2019-02-08', '19:48:44'),
(167, 58, 56, 184, 4, 0, 0, '', '', 27, '2019-02-08', '19:55:10'),
(168, 58, 56, 185, 4, 0, 0, '', '', 27, '2019-02-08', '19:56:38'),
(190, 61, 59, 193, 4, 0, 0, '', '', 27, '2019-02-11', '03:06:03'),
(191, 61, 59, 194, 4, 0, 0, '', '', 27, '2019-02-11', '03:07:49'),
(192, 61, 59, 195, 4, 0, 0, '', '', 27, '2019-02-11', '03:09:39'),
(193, 61, 59, 196, 4, 0, 0, '', '', 27, '2019-02-11', '03:10:52'),
(194, 61, 59, 197, 4, 0, 0, '', '', 27, '2019-02-11', '03:12:49'),
(195, 61, 59, 198, 4, 0, 0, '', '', 27, '2019-02-11', '03:14:10'),
(196, 61, 59, 199, 4, 0, 0, '', '', 27, '2019-02-11', '03:15:28'),
(197, 61, 59, 200, 4, 0, 0, '', '', 27, '2019-02-11', '03:16:26'),
(198, 61, 59, 201, 4, 0, 0, '', '', 27, '2019-02-11', '03:17:53'),
(199, 61, 59, 202, 4, 0, 0, '', '', 27, '2019-02-11', '03:19:16'),
(200, 61, 59, 203, 4, 0, 0, '', '', 27, '2019-02-11', '03:20:20'),
(201, 61, 59, 204, 4, 0, 0, '', '', 27, '2019-02-11', '03:23:43'),
(202, 61, 59, 205, 4, 0, 0, '', '', 27, '2019-02-11', '03:25:10'),
(203, 61, 59, 206, 4, 0, 0, '', '', 27, '2019-02-11', '03:26:20'),
(204, 62, 60, 207, 4, 0, 0, '', '', 27, '2019-02-11', '03:41:50'),
(205, 62, 60, 208, 4, 0, 0, '', '', 27, '2019-02-11', '03:43:02'),
(206, 62, 60, 209, 4, 0, 0, '', '', 27, '2019-02-11', '03:44:01'),
(207, 56, 61, 210, 4, 0, 0, '', '', 28, '2019-02-14', '11:39:08'),
(208, 56, 62, 211, 4, 0, 0, '', '', 28, '2019-02-14', '11:40:37'),
(209, 56, 63, 212, 2, 0, 0, '', 'files/detalle/listado/20190214_114722_0.png', 28, '2019-02-14', '11:47:22'),
(210, 56, 63, 213, 2, 0, 0, '', 'files/detalle/listado/20190214_115630_0.png', 28, '2019-02-14', '11:56:30'),
(211, 56, 63, 214, 1, 21, 0, '', '', 28, '2019-02-14', '11:58:21'),
(212, 56, 63, 215, 2, 0, 0, '', 'files/detalle/listado/20190214_120142_0.png', 28, '2019-02-14', '12:01:42'),
(213, 56, 63, 215, 1, 76, 0, '', '', 28, '2019-02-14', '12:01:42'),
(214, 56, 63, 216, 2, 0, 0, '', 'files/detalle/listado/20190214_120624_0.jpeg', 28, '2019-02-14', '12:06:24'),
(215, 56, 63, 217, 2, 0, 0, '', 'files/detalle/listado/20190214_121031_0.png', 28, '2019-02-14', '12:10:31'),
(216, 56, 63, 218, 1, 12, 0, '', '', 28, '2019-02-14', '12:22:55'),
(217, 56, 63, 219, 1, 76, 0, '', '', 28, '2019-02-14', '12:24:56'),
(218, 56, 63, 220, 1, 10, 0, '', '', 28, '2019-02-14', '13:18:16'),
(219, 56, 63, 220, 2, 0, 0, '', 'files/detalle/listado/20190214_131816_1.png', 28, '2019-02-14', '13:18:16'),
(220, 56, 63, 221, 1, 10, 0, '', '', 28, '2019-02-14', '13:19:26'),
(221, 72, 64, 222, 4, 0, 0, '', '', 29, '2019-02-14', '16:25:45'),
(225, 72, 64, 223, 1, 114, 0, '', '', 29, '2019-02-14', '16:28:55'),
(229, 72, 64, 224, 1, 52, 0, '', '', 29, '2019-02-14', '16:32:09'),
(233, 72, 64, 225, 1, 14, 0, '', '', 29, '2019-02-14', '16:36:21'),
(237, 72, 64, 226, 4, 0, 0, '', '', 29, '2019-02-14', '16:39:37'),
(241, 72, 64, 227, 4, 0, 0, '', '', 29, '2019-02-14', '16:42:03'),
(245, 72, 64, 228, 4, 0, 0, '', '', 29, '2019-02-14', '16:43:40'),
(249, 72, 64, 229, 4, 0, 0, '', '', 29, '2019-02-14', '16:47:52'),
(253, 72, 64, 230, 4, 0, 0, '', '', 29, '2019-02-14', '16:50:04'),
(257, 72, 64, 231, 4, 0, 0, '', '', 29, '2019-02-14', '16:53:29'),
(261, 72, 64, 232, 4, 0, 0, '', '', 29, '2019-02-14', '16:59:49'),
(265, 72, 64, 222, 1, 81, 0, '', '', 29, '2019-02-14', '17:13:11'),
(266, 72, 64, 232, 1, 24, 0, '', '', 29, '2019-02-14', '17:14:18'),
(267, 72, 64, 232, 1, 60, 0, '', '', 29, '2019-02-14', '17:14:18'),
(268, 72, 64, 233, 4, 0, 0, '', '', 29, '2019-02-14', '17:16:31'),
(272, 72, 64, 234, 1, 143, 0, '', '', 29, '2019-02-14', '17:39:24'),
(276, 56, 65, 235, 3, 0, 5, '', '', 28, '2019-02-15', '12:02:09'),
(277, 56, 66, 236, 2, 0, 0, '', 'files/detalle/listado/20190215_120715_0.png', 28, '2019-02-15', '12:07:15'),
(278, 56, 67, 237, 2, 0, 0, '', 'files/detalle/listado/20190215_121426_0.png', 28, '2019-02-15', '12:14:26'),
(279, 56, 67, 238, 4, 0, 0, '', '', 28, '2019-02-15', '12:17:54'),
(280, 56, 74, 239, 2, 0, 0, '', 'files/detalle/listado/20190215_122610_0.png', 28, '2019-02-15', '12:26:10'),
(281, 56, 74, 240, 2, 0, 0, '', 'files/detalle/listado/20190215_122804_0.png', 28, '2019-02-15', '12:28:04'),
(282, 56, 75, 241, 3, 0, 5, '', '', 28, '2019-02-15', '12:30:17'),
(283, 56, 75, 242, 1, 14, 0, '', '', 28, '2019-02-15', '12:31:29'),
(284, 72, 64, 243, 4, 0, 0, '', '', 29, '2019-02-16', '18:24:15'),
(288, 72, 64, 244, 4, 0, 0, '', '', 29, '2019-02-16', '18:29:45'),
(292, 72, 64, 245, 4, 0, 0, '', '', 29, '2019-02-16', '18:37:51'),
(296, 72, 64, 246, 4, 0, 0, '', '', 29, '2019-02-16', '18:47:11'),
(300, 72, 64, 247, 4, 0, 0, '', '', 29, '2019-02-16', '19:01:56'),
(304, 72, 64, 248, 4, 0, 0, '', '', 29, '2019-02-16', '19:08:15'),
(308, 72, 64, 249, 4, 0, 0, '', '', 29, '2019-02-16', '19:30:41'),
(312, 72, 64, 250, 4, 0, 0, '', '', 29, '2019-02-16', '20:08:23'),
(316, 72, 64, 251, 4, 0, 0, '', '', 29, '2019-02-16', '20:18:48'),
(320, 72, 64, 252, 4, 0, 0, '', '', 29, '2019-02-16', '20:42:02'),
(324, 72, 64, 253, 4, 0, 0, '', '', 29, '2019-02-16', '21:15:56'),
(328, 72, 64, 254, 4, 0, 0, '', '', 29, '2019-02-16', '21:18:10'),
(332, 72, 64, 255, 4, 0, 0, '', '', 29, '2019-02-16', '21:42:44'),
(336, 72, 64, 256, 4, 0, 0, '', '', 29, '2019-02-16', '22:03:35'),
(340, 73, 76, 257, 4, 0, 0, '', '', 33, '2019-02-19', '16:37:29'),
(341, 73, 76, 258, 4, 0, 0, '', '', 33, '2019-02-19', '16:43:29'),
(342, 73, 76, 259, 4, 0, 0, '', '', 33, '2019-02-19', '16:44:56'),
(343, 73, 76, 260, 4, 0, 0, '', '', 33, '2019-02-19', '16:50:41'),
(344, 73, 76, 261, 4, 0, 0, '', '', 33, '2019-02-19', '16:51:24'),
(345, 73, 76, 262, 4, 0, 0, '', '', 33, '2019-02-19', '16:52:45'),
(346, 73, 76, 263, 4, 0, 0, '', '', 33, '2019-02-19', '16:54:52'),
(347, 73, 76, 264, 4, 0, 0, '', '', 33, '2019-02-19', '16:55:48'),
(348, 73, 76, 265, 4, 0, 0, '', '', 33, '2019-02-19', '16:56:48'),
(349, 73, 76, 266, 4, 0, 0, '', '', 33, '2019-02-19', '16:59:57'),
(350, 73, 76, 267, 4, 0, 0, '', '', 33, '2019-02-19', '17:00:41'),
(351, 73, 76, 268, 4, 0, 0, '', '', 33, '2019-02-19', '17:12:14'),
(352, 73, 76, 269, 4, 0, 0, '', '', 33, '2019-02-19', '17:20:04'),
(353, 73, 76, 270, 4, 0, 0, '', '', 33, '2019-02-19', '17:21:22'),
(354, 73, 76, 271, 4, 0, 0, '', '', 33, '2019-02-19', '17:22:28'),
(355, 73, 76, 272, 4, 0, 0, '', '', 33, '2019-02-19', '17:24:02'),
(356, 73, 76, 273, 4, 0, 0, '', '', 33, '2019-02-19', '17:30:25'),
(357, 73, 76, 274, 4, 0, 0, '', '', 33, '2019-02-19', '17:31:33'),
(358, 73, 76, 275, 4, 0, 0, '', '', 33, '2019-02-19', '17:33:04'),
(359, 73, 76, 276, 4, 0, 0, '', '', 33, '2019-02-19', '18:05:50'),
(360, 73, 76, 277, 4, 0, 0, '', '', 33, '2019-02-19', '18:26:26'),
(361, 73, 76, 278, 4, 0, 0, '', '', 33, '2019-02-19', '18:30:58'),
(362, 73, 76, 279, 4, 0, 0, '', '', 33, '2019-02-19', '18:34:10'),
(363, 73, 76, 280, 4, 0, 0, '', '', 33, '2019-02-19', '18:35:18'),
(364, 73, 76, 281, 4, 0, 0, '', '', 33, '2019-02-19', '18:40:26'),
(365, 73, 76, 282, 4, 0, 0, '', '', 33, '2019-02-19', '18:41:11'),
(366, 73, 76, 283, 4, 0, 0, '', '', 33, '2019-02-19', '18:41:42'),
(367, 73, 76, 284, 4, 0, 0, '', '', 33, '2019-02-19', '18:42:20'),
(368, 73, 76, 285, 4, 0, 0, '', '', 33, '2019-02-19', '18:45:19'),
(369, 73, 76, 286, 4, 0, 0, '', '', 33, '2019-02-19', '19:01:45'),
(370, 73, 77, 287, 4, 0, 0, '', '', 33, '2019-02-19', '19:03:16'),
(371, 73, 77, 288, 4, 0, 0, '', '', 33, '2019-02-19', '19:03:57'),
(372, 73, 77, 289, 4, 0, 0, '', '', 33, '2019-02-19', '19:04:35'),
(373, 73, 77, 290, 4, 0, 0, '', '', 33, '2019-02-19', '19:05:29'),
(374, 73, 77, 291, 4, 0, 0, '', '', 33, '2019-02-19', '19:06:30'),
(375, 73, 77, 292, 4, 0, 0, '', '', 33, '2019-02-19', '19:07:16'),
(376, 73, 77, 293, 4, 0, 0, '', '', 33, '2019-02-19', '19:08:21'),
(377, 73, 77, 294, 4, 0, 0, '', '', 33, '2019-02-19', '19:09:22'),
(378, 73, 77, 295, 4, 0, 0, '', '', 33, '2019-02-19', '19:14:41'),
(379, 73, 77, 296, 4, 0, 0, '', '', 33, '2019-02-19', '19:15:14'),
(380, 73, 77, 297, 4, 0, 0, '', '', 33, '2019-02-19', '19:18:46'),
(381, 72, 64, 298, 1, 106, 0, '', '', 29, '2019-02-19', '21:13:32'),
(385, 72, 64, 298, 1, 26, 0, '', '', 29, '2019-02-19', '21:13:32'),
(386, 72, 64, 299, 4, 0, 0, '', '', 29, '2019-02-20', '20:55:11'),
(390, 72, 64, 300, 4, 0, 0, '', '', 29, '2019-02-20', '20:57:48'),
(394, 72, 64, 301, 4, 0, 0, '', '', 29, '2019-02-20', '21:00:57'),
(398, 72, 64, 302, 4, 0, 0, '', '', 29, '2019-02-20', '21:13:12'),
(402, 72, 64, 303, 4, 0, 0, '', '', 29, '2019-02-20', '21:34:11'),
(406, 58, 78, 304, 4, 0, 0, '', '', 22, '2019-02-21', '08:50:27'),
(407, 58, 78, 305, 4, 0, 0, '', '', 22, '2019-02-21', '08:51:35'),
(408, 58, 78, 306, 4, 0, 0, '', '', 22, '2019-02-21', '08:52:46'),
(409, 58, 78, 307, 4, 0, 0, '', '', 22, '2019-02-21', '08:53:31'),
(410, 58, 78, 308, 4, 0, 0, '', '', 22, '2019-02-21', '08:54:35'),
(411, 58, 78, 309, 4, 0, 0, '', '', 22, '2019-02-21', '08:55:26'),
(412, 58, 78, 310, 4, 0, 0, '', '', 22, '2019-02-21', '08:56:05'),
(413, 58, 78, 311, 4, 0, 0, '', '', 22, '2019-02-21', '08:57:20'),
(414, 58, 78, 312, 4, 0, 0, '', '', 22, '2019-02-21', '08:57:59'),
(415, 58, 78, 313, 4, 0, 0, '', '', 22, '2019-02-21', '08:58:46'),
(416, 58, 78, 314, 4, 0, 0, '', '', 22, '2019-02-21', '08:59:31'),
(417, 58, 78, 315, 4, 0, 0, '', '', 22, '2019-02-21', '09:00:17'),
(418, 58, 78, 316, 4, 0, 0, '', '', 22, '2019-02-21', '09:01:00'),
(419, 58, 78, 317, 4, 0, 0, '', '', 22, '2019-02-21', '09:02:52'),
(420, 58, 79, 318, 4, 0, 0, '', '', 22, '2019-02-21', '09:10:28'),
(421, 58, 79, 319, 4, 0, 0, '', '', 22, '2019-02-21', '09:11:24'),
(422, 58, 79, 320, 4, 0, 0, '', '', 22, '2019-02-21', '09:12:21'),
(423, 75, 80, 321, 3, 0, 5, '', '', 25, '2019-03-04', '14:42:07'),
(424, 75, 80, 322, 3, 0, 6, '', '', 25, '2019-03-04', '14:45:42'),
(425, 75, 80, 323, 3, 0, 24, '', '', 25, '2019-03-04', '14:51:45'),
(426, 75, 80, 324, 3, 0, 18, '', '', 25, '2019-03-04', '14:59:35'),
(427, 75, 80, 325, 3, 0, 11, '', '', 25, '2019-03-04', '15:01:54'),
(428, 75, 80, 326, 3, 0, 3, '', '', 25, '2019-03-04', '15:05:54'),
(429, 75, 80, 327, 3, 0, 5, '', '', 25, '2019-03-04', '15:09:08'),
(430, 75, 81, 328, 3, 0, 5, '', '', 25, '2019-03-04', '15:12:12'),
(431, 75, 81, 329, 3, 0, 25, '', '', 25, '2019-03-04', '15:17:32'),
(432, 75, 81, 330, 3, 0, 26, '', '', 25, '2019-03-04', '15:23:00'),
(433, 75, 81, 331, 4, 0, 0, '', '', 25, '2019-03-04', '15:24:27'),
(434, 76, 82, 332, 3, 0, 10, '', '', 40, '2019-03-12', '19:03:05'),
(435, 76, 82, 333, 3, 0, 9, '', '', 40, '2019-03-13', '17:20:15'),
(436, 76, 82, 334, 3, 0, 3, '', '', 40, '2019-03-13', '17:22:25'),
(437, 76, 82, 335, 3, 0, 5, '', '', 40, '2019-03-13', '17:28:09'),
(438, 76, 82, 336, 4, 0, 0, '', '', 40, '2019-03-13', '17:29:55'),
(439, 76, 82, 337, 3, 0, 20, '', '', 40, '2019-03-13', '17:31:12'),
(440, 76, 82, 338, 3, 0, 22, '', '', 40, '2019-03-13', '17:33:37'),
(441, 76, 82, 339, 1, 89, 0, '', '', 40, '2019-03-13', '17:36:05'),
(442, 76, 82, 340, 3, 0, 11, '', '', 40, '2019-03-13', '17:38:06'),
(443, 76, 82, 341, 3, 0, 10, '', '', 40, '2019-03-13', '17:39:39'),
(444, 76, 82, 342, 1, 21, 0, '', '', 40, '2019-03-13', '17:42:06'),
(445, 76, 82, 343, 3, 0, 23, '', '', 40, '2019-03-13', '17:43:54'),
(446, 76, 82, 344, 3, 0, 23, '', '', 40, '2019-03-13', '17:45:46'),
(447, 76, 82, 345, 3, 0, 9, '', '', 40, '2019-03-13', '17:48:05'),
(449, 76, 82, 347, 3, 0, 5, '', '', 40, '2019-03-13', '17:51:03'),
(450, 76, 82, 348, 3, 0, 4, '', '', 40, '2019-03-13', '17:54:01'),
(451, 76, 82, 349, 3, 0, 25, '', '', 40, '2019-03-13', '17:55:43'),
(452, 76, 82, 350, 1, 10, 0, '', '', 40, '2019-03-13', '17:58:27'),
(453, 76, 82, 351, 3, 0, 14, '', '', 40, '2019-03-13', '18:00:57'),
(454, 76, 82, 352, 3, 0, 20, '', '', 40, '2019-03-13', '18:04:18'),
(455, 76, 84, 353, 1, 101, 0, '', '', 40, '2019-03-13', '18:32:09'),
(456, 76, 84, 354, 1, 102, 0, '', '', 40, '2019-03-13', '18:36:01'),
(457, 76, 84, 355, 1, 26, 0, '', '', 40, '2019-03-14', '10:54:49'),
(458, 76, 84, 356, 3, 0, 16, '', '', 40, '2019-03-14', '10:57:43'),
(460, 76, 84, 358, 3, 0, 20, '', '', 40, '2019-03-14', '12:37:13'),
(461, 76, 84, 359, 3, 0, 23, '', '', 40, '2019-03-14', '12:41:44'),
(462, 76, 84, 360, 3, 0, 17, '', '', 40, '2019-03-14', '12:44:05'),
(463, 76, 84, 361, 3, 0, 20, '', '', 40, '2019-03-14', '12:46:21'),
(464, 76, 84, 362, 1, 93, 0, '', '', 40, '2019-03-14', '12:48:44'),
(465, 76, 84, 363, 4, 0, 0, '', '', 40, '2019-03-14', '12:57:31'),
(466, 76, 84, 364, 4, 0, 0, '', '', 40, '2019-03-14', '13:15:46'),
(467, 76, 82, 365, 4, 0, 0, '', '', 40, '2019-03-14', '16:26:54'),
(468, 76, 84, 366, 4, 0, 0, '', '', 40, '2019-03-14', '16:39:02'),
(469, 76, 84, 367, 4, 0, 0, '', '', 40, '2019-03-14', '17:03:41'),
(472, 82, 87, 369, 4, 0, 0, '', '', 47, '2019-03-18', '10:50:59'),
(474, 82, 87, 369, 4, 0, 0, '', '', 47, '2019-03-18', '10:57:08'),
(475, 82, 87, 370, 4, 0, 0, '', '', 47, '2019-03-18', '11:04:28'),
(476, 82, 87, 370, 4, 0, 0, '', '', 47, '2019-03-18', '11:04:28'),
(477, 82, 87, 371, 4, 0, 0, '', '', 47, '2019-03-18', '11:06:36'),
(478, 82, 87, 371, 4, 0, 0, '', '', 47, '2019-03-18', '11:06:36'),
(479, 82, 88, 372, 4, 0, 0, '', '', 47, '2019-03-18', '11:10:14'),
(480, 82, 88, 372, 4, 0, 0, '', '', 47, '2019-03-18', '11:10:14'),
(481, 82, 88, 373, 4, 0, 0, '', '', 47, '2019-03-18', '11:14:18'),
(482, 82, 88, 373, 4, 0, 0, '', '', 47, '2019-03-18', '11:14:18'),
(483, 82, 88, 374, 4, 0, 0, '', '', 47, '2019-03-18', '11:17:16'),
(484, 82, 88, 374, 4, 0, 0, '', '', 47, '2019-03-18', '11:17:16'),
(485, 82, 88, 375, 4, 0, 0, '', '', 47, '2019-03-18', '11:21:12'),
(486, 82, 88, 375, 4, 0, 0, '', '', 47, '2019-03-18', '11:21:12'),
(487, 82, 88, 376, 4, 0, 0, '', '', 47, '2019-03-18', '11:40:28'),
(488, 82, 88, 376, 4, 0, 0, '', '', 47, '2019-03-18', '11:40:28'),
(489, 82, 88, 377, 4, 0, 0, '', '', 47, '2019-03-18', '11:44:54'),
(490, 82, 88, 377, 4, 0, 0, '', '', 47, '2019-03-18', '11:44:54'),
(491, 82, 88, 378, 4, 0, 0, '', '', 47, '2019-03-18', '11:46:05'),
(492, 82, 88, 378, 4, 0, 0, '', '', 47, '2019-03-18', '11:46:05'),
(493, 82, 88, 379, 4, 0, 0, '', '', 47, '2019-03-18', '11:47:54'),
(494, 82, 88, 379, 4, 0, 0, '', '', 47, '2019-03-18', '11:47:54'),
(495, 82, 88, 379, 4, 0, 0, '', '', 47, '2019-03-18', '11:47:54'),
(496, 82, 89, 380, 4, 0, 0, '', '', 47, '2019-03-18', '11:50:34'),
(497, 82, 89, 380, 4, 0, 0, '', '', 47, '2019-03-18', '11:50:34'),
(498, 82, 89, 381, 4, 0, 0, '', '', 47, '2019-03-18', '12:12:03'),
(499, 82, 89, 381, 4, 0, 0, '', '', 47, '2019-03-18', '12:12:03'),
(500, 82, 89, 382, 4, 0, 0, '', '', 47, '2019-03-18', '12:14:55'),
(501, 82, 89, 382, 4, 0, 0, '', '', 47, '2019-03-18', '12:14:55'),
(502, 82, 90, 383, 4, 0, 0, '', '', 47, '2019-03-18', '12:22:05'),
(503, 82, 90, 383, 4, 0, 0, '', '', 47, '2019-03-18', '12:22:05'),
(504, 82, 90, 384, 4, 0, 0, '', '', 47, '2019-03-18', '12:25:43'),
(505, 82, 90, 384, 4, 0, 0, '', '', 47, '2019-03-18', '12:25:43'),
(506, 82, 90, 385, 4, 0, 0, '', '', 47, '2019-03-18', '12:27:30'),
(507, 82, 90, 385, 4, 0, 0, '', '', 47, '2019-03-18', '12:27:30'),
(508, 82, 91, 386, 4, 0, 0, '', '', 47, '2019-03-18', '12:32:44'),
(509, 82, 91, 386, 4, 0, 0, '', '', 47, '2019-03-18', '12:32:44'),
(510, 82, 91, 387, 4, 0, 0, '', '', 47, '2019-03-18', '12:36:46'),
(511, 82, 91, 387, 4, 0, 0, '', '', 47, '2019-03-18', '12:36:46'),
(512, 82, 92, 388, 4, 0, 0, '', '', 47, '2019-03-18', '12:47:46'),
(513, 82, 92, 388, 4, 0, 0, '', '', 47, '2019-03-18', '12:47:46'),
(514, 82, 92, 389, 4, 0, 0, '', '', 47, '2019-03-18', '12:49:47'),
(515, 82, 92, 389, 4, 0, 0, '', '', 47, '2019-03-18', '12:49:47'),
(516, 82, 101, 390, 4, 0, 0, '', '', 47, '2019-03-19', '09:47:48'),
(517, 82, 101, 390, 4, 0, 0, '', '', 47, '2019-03-19', '09:47:48'),
(518, 82, 101, 391, 4, 0, 0, '', '', 47, '2019-03-19', '09:49:40'),
(519, 82, 101, 391, 4, 0, 0, '', '', 47, '2019-03-19', '09:49:40'),
(520, 82, 102, 392, 4, 0, 0, '', '', 47, '2019-03-19', '09:52:23'),
(521, 82, 102, 392, 4, 0, 0, '', '', 47, '2019-03-19', '09:52:23'),
(522, 82, 105, 393, 4, 0, 0, '', '', 47, '2019-03-19', '10:01:29'),
(523, 82, 105, 393, 4, 0, 0, '', '', 47, '2019-03-19', '10:01:29'),
(524, 82, 116, 394, 4, 0, 0, '', '', 47, '2019-03-19', '10:08:49'),
(525, 82, 116, 394, 4, 0, 0, '', '', 47, '2019-03-19', '10:08:49'),
(526, 82, 101, 395, 4, 0, 0, '', '', 47, '2019-03-19', '10:17:42'),
(527, 82, 101, 395, 4, 0, 0, '', '', 47, '2019-03-19', '10:17:42'),
(528, 82, 101, 396, 4, 0, 0, '', '', 47, '2019-03-19', '10:48:14'),
(529, 82, 101, 396, 4, 0, 0, '', '', 47, '2019-03-19', '10:48:14'),
(530, 82, 88, 397, 4, 0, 0, '', '', 47, '2019-03-19', '12:30:27'),
(531, 82, 88, 397, 4, 0, 0, '', '', 47, '2019-03-19', '12:30:27'),
(532, 82, 91, 398, 4, 0, 0, '', '', 47, '2019-03-19', '12:41:15'),
(533, 82, 91, 398, 4, 0, 0, '', '', 47, '2019-03-19', '12:41:15'),
(535, 87, 120, 400, 4, 0, 0, '', '', 49, '2019-03-22', '14:39:39'),
(536, 87, 120, 401, 4, 0, 0, '', '', 49, '2019-03-22', '14:41:45'),
(537, 87, 120, 402, 4, 0, 0, '', '', 49, '2019-03-22', '14:43:18'),
(538, 87, 120, 403, 4, 0, 0, '', '', 49, '2019-03-22', '14:49:04'),
(539, 87, 120, 404, 4, 0, 0, '', '', 49, '2019-03-22', '14:50:40'),
(540, 87, 120, 405, 4, 0, 0, '', '', 49, '2019-03-22', '14:51:48'),
(541, 87, 120, 406, 4, 0, 0, '', '', 49, '2019-03-22', '14:52:43'),
(542, 87, 120, 407, 4, 0, 0, '', '', 49, '2019-03-22', '14:53:38'),
(543, 87, 120, 408, 4, 0, 0, '', '', 49, '2019-03-22', '14:54:43'),
(544, 88, 171, 409, 1, 65, 0, '', '', 50, '2019-03-27', '18:06:08'),
(545, 88, 171, 410, 3, 0, 8, '', '', 50, '2019-03-27', '18:14:40'),
(546, 88, 171, 411, 3, 0, 13, '', '', 50, '2019-03-27', '18:21:58'),
(547, 88, 171, 412, 1, 31, 0, '', '', 50, '2019-03-27', '18:28:52'),
(548, 88, 171, 413, 3, 0, 14, '', '', 50, '2019-03-27', '18:34:04'),
(549, 88, 171, 414, 3, 0, 16, '', '', 50, '2019-03-27', '18:38:05'),
(550, 88, 171, 415, 3, 0, 5, '', '', 50, '2019-03-27', '18:42:19'),
(551, 88, 171, 416, 3, 0, 5, '', '', 50, '2019-03-27', '19:03:38'),
(552, 88, 171, 417, 4, 0, 0, '', '', 50, '2019-03-27', '19:06:43'),
(553, 88, 171, 418, 1, 21, 0, '', '', 50, '2019-03-27', '19:24:43'),
(554, 88, 171, 419, 4, 0, 0, '', '', 50, '2019-03-27', '19:29:28'),
(555, 88, 171, 420, 1, 51, 0, '', '', 50, '2019-03-27', '19:31:21'),
(556, 88, 171, 421, 4, 0, 0, '', '', 50, '2019-03-27', '19:33:39'),
(557, 88, 171, 422, 4, 0, 0, '', '', 50, '2019-03-27', '19:36:03'),
(558, 88, 172, 423, 3, 0, 22, '', '', 50, '2019-03-27', '19:42:52'),
(559, 88, 172, 424, 1, 112, 0, '', '', 50, '2019-03-27', '20:49:22'),
(560, 88, 172, 425, 4, 0, 0, '', '', 50, '2019-03-27', '20:51:20'),
(561, 88, 172, 426, 1, 113, 0, '', '', 50, '2019-03-27', '20:54:08'),
(562, 89, 173, 427, 1, 76, 0, '', '', 25, '2019-03-31', '22:05:17'),
(563, 89, 173, 428, 2, 0, 0, '', 'files/detalle/listado/20190331_220838_0.png', 25, '2019-03-31', '22:08:38'),
(564, 89, 173, 429, 1, 78, 0, '', '', 25, '2019-03-31', '22:09:18'),
(565, 89, 173, 430, 1, 103, 0, '', '', 25, '2019-03-31', '22:11:01'),
(566, 89, 173, 431, 2, 0, 0, '', 'files/detalle/listado/20190331_221507_0.png', 25, '2019-03-31', '22:15:07'),
(567, 89, 173, 432, 1, 104, 0, '', '', 25, '2019-03-31', '22:17:58'),
(568, 89, 173, 433, 1, 105, 0, '', '', 25, '2019-03-31', '22:26:13'),
(569, 89, 173, 434, 1, 107, 0, '', '', 25, '2019-03-31', '22:38:14'),
(570, 89, 173, 435, 1, 108, 0, '', '', 25, '2019-03-31', '22:42:29'),
(571, 89, 173, 436, 1, 109, 0, '', '', 25, '2019-03-31', '22:43:46'),
(572, 89, 173, 437, 1, 14, 0, '', '', 25, '2019-03-31', '22:46:52'),
(573, 89, 173, 438, 1, 110, 0, '', '', 25, '2019-03-31', '22:56:18'),
(574, 89, 173, 439, 1, 111, 0, '', '', 25, '2019-03-31', '23:01:14'),
(575, 89, 173, 440, 2, 0, 0, '', 'files/detalle/listado/20190401_102926_575.png', 25, '2019-03-31', '23:05:00'),
(576, 89, 173, 441, 2, 0, 0, '', 'files/detalle/listado/20190331_230818_0.png', 25, '2019-03-31', '23:08:18'),
(577, 89, 173, 442, 2, 0, 0, '', 'files/detalle/listado/20190331_232824_577.png', 25, '2019-03-31', '23:25:54'),
(578, 90, 174, 443, 3, 0, 2, '', '', 53, '2019-04-01', '18:01:44'),
(579, 90, 174, 444, 3, 0, 2, '', '', 53, '2019-04-01', '18:03:48'),
(580, 90, 174, 445, 3, 0, 17, '', '', 53, '2019-04-01', '18:08:31'),
(581, 91, 175, 446, 1, 115, 0, '', '', 25, '2019-04-01', '20:28:02'),
(582, 91, 175, 447, 1, 21, 0, '', '', 25, '2019-04-01', '21:35:34'),
(583, 91, 175, 448, 3, 0, 5, '', '', 25, '2019-04-01', '21:36:19'),
(584, 91, 175, 449, 1, 116, 0, '', '', 25, '2019-04-01', '21:37:29'),
(585, 91, 175, 450, 1, 28, 0, '', '', 25, '2019-04-01', '21:39:53'),
(586, 91, 175, 451, 1, 76, 0, '', '', 25, '2019-04-01', '21:40:31'),
(587, 91, 175, 452, 1, 117, 0, '', '', 25, '2019-04-01', '21:41:26'),
(588, 91, 175, 453, 1, 66, 0, '', '', 25, '2019-04-01', '21:43:50'),
(589, 91, 175, 454, 1, 118, 0, '', '', 25, '2019-04-01', '21:49:08'),
(590, 91, 175, 455, 1, 28, 0, '', '', 25, '2019-04-01', '21:50:05'),
(591, 91, 175, 456, 4, 0, 0, '', '', 25, '2019-04-01', '21:50:54'),
(592, 91, 175, 457, 1, 122, 0, '', '', 25, '2019-04-01', '21:52:01'),
(593, 91, 175, 458, 3, 0, 5, '', '', 25, '2019-04-01', '21:53:56'),
(594, 91, 175, 459, 1, 37, 0, '', '', 25, '2019-04-01', '21:55:25'),
(595, 91, 175, 460, 1, 120, 0, '', '', 25, '2019-04-01', '21:59:25'),
(596, 91, 175, 461, 4, 0, 0, '', '', 25, '2019-04-01', '22:01:01'),
(597, 91, 175, 462, 4, 0, 0, '', '', 25, '2019-04-01', '22:02:03'),
(598, 94, 177, 463, 1, 14, 0, '', '', 54, '2019-04-01', '22:04:58'),
(599, 94, 177, 464, 4, 0, 0, '', '', 54, '2019-04-01', '22:09:19'),
(600, 94, 177, 465, 4, 0, 0, '', '', 54, '2019-04-01', '22:11:19'),
(601, 94, 177, 466, 4, 0, 0, '', '', 54, '2019-04-01', '22:12:35'),
(602, 94, 177, 467, 1, 26, 0, '', '', 54, '2019-04-01', '22:13:56'),
(603, 94, 177, 468, 4, 0, 0, '', '', 54, '2019-04-01', '22:15:32'),
(604, 94, 177, 469, 4, 0, 0, '', '', 54, '2019-04-01', '22:19:11'),
(605, 94, 177, 470, 4, 0, 0, '', '', 54, '2019-04-01', '22:20:20'),
(606, 94, 177, 471, 4, 0, 0, '', '', 54, '2019-04-01', '22:21:53'),
(607, 94, 177, 472, 4, 0, 0, '', '', 54, '2019-04-01', '22:23:21'),
(608, 94, 177, 473, 4, 0, 0, '', '', 54, '2019-04-01', '22:24:58'),
(609, 94, 177, 474, 4, 0, 0, '', '', 54, '2019-04-01', '22:26:09'),
(610, 96, 178, 475, 4, 0, 0, '', '', 49, '2019-04-01', '22:34:37'),
(611, 96, 178, 476, 4, 0, 0, '', '', 49, '2019-04-01', '22:35:38'),
(612, 96, 178, 477, 4, 0, 0, '', '', 49, '2019-04-01', '22:36:28'),
(613, 96, 178, 478, 4, 0, 0, '', '', 49, '2019-04-01', '22:37:19'),
(614, 96, 178, 479, 4, 0, 0, '', '', 49, '2019-04-01', '22:38:08'),
(615, 93, 176, 480, 4, 0, 0, '', '', 55, '2019-04-01', '22:38:20'),
(616, 96, 178, 481, 4, 0, 0, '', '', 49, '2019-04-01', '22:38:59'),
(617, 96, 178, 482, 4, 0, 0, '', '', 49, '2019-04-01', '22:39:47'),
(618, 96, 178, 483, 4, 0, 0, '', '', 49, '2019-04-01', '22:40:47'),
(619, 96, 178, 484, 4, 0, 0, '', '', 49, '2019-04-01', '22:41:41'),
(620, 96, 178, 485, 4, 0, 0, '', '', 49, '2019-04-01', '22:42:32'),
(621, 96, 178, 486, 4, 0, 0, '', '', 49, '2019-04-01', '22:43:21'),
(622, 96, 178, 487, 4, 0, 0, '', '', 49, '2019-04-01', '22:44:35'),
(623, 96, 178, 488, 4, 0, 0, '', '', 49, '2019-04-01', '22:46:05'),
(624, 93, 176, 489, 1, 93, 0, '', '', 55, '2019-04-01', '22:46:47'),
(625, 96, 178, 490, 4, 0, 0, '', '', 49, '2019-04-01', '22:47:04'),
(626, 96, 178, 491, 4, 0, 0, '', '', 49, '2019-04-01', '22:47:46'),
(627, 96, 178, 492, 4, 0, 0, '', '', 49, '2019-04-01', '22:49:04'),
(628, 93, 176, 493, 1, 121, 0, '', '', 55, '2019-04-01', '22:52:05'),
(629, 93, 176, 493, 1, 14, 0, '', '', 55, '2019-04-01', '22:52:05'),
(630, 93, 176, 494, 3, 0, 27, '', '', 55, '2019-04-01', '22:58:58'),
(631, 93, 176, 495, 1, 11, 0, '', '', 55, '2019-04-01', '23:07:11'),
(632, 93, 176, 495, 1, 109, 0, '', '', 55, '2019-04-01', '23:07:11'),
(633, 93, 176, 496, 4, 0, 0, '', '', 55, '2019-04-01', '23:09:50'),
(634, 94, 179, 497, 4, 0, 0, '', '', 54, '2019-04-01', '23:13:56'),
(635, 93, 176, 498, 1, 123, 0, '', '', 55, '2019-04-01', '23:14:28'),
(636, 93, 176, 499, 1, 125, 0, '', '', 55, '2019-04-01', '23:19:47'),
(637, 93, 176, 499, 1, 28, 0, '', '', 55, '2019-04-01', '23:19:47'),
(638, 93, 176, 499, 4, 0, 0, '', '', 55, '2019-04-01', '23:19:47'),
(639, 94, 179, 500, 1, 39, 0, '', '', 54, '2019-04-01', '23:20:21'),
(640, 93, 176, 501, 4, 0, 0, '', '', 55, '2019-04-01', '23:22:08'),
(641, 94, 180, 502, 4, 0, 0, '', '', 54, '2019-04-01', '23:23:50'),
(642, 94, 180, 503, 4, 0, 0, '', '', 54, '2019-04-01', '23:24:48'),
(643, 94, 181, 504, 4, 0, 0, '', '', 54, '2019-04-01', '23:26:47'),
(644, 93, 176, 505, 1, 100, 0, '', '', 55, '2019-04-01', '23:31:03'),
(645, 93, 176, 505, 1, 76, 0, '', '', 55, '2019-04-01', '23:31:03'),
(646, 94, 182, 506, 4, 0, 0, '', '', 54, '2019-04-01', '23:33:15'),
(647, 94, 182, 507, 1, 52, 0, '', '', 54, '2019-04-01', '23:34:13'),
(648, 93, 176, 508, 3, 0, 11, '', '', 55, '2019-04-01', '23:35:21'),
(649, 94, 182, 509, 4, 0, 0, '', '', 54, '2019-04-01', '23:35:37'),
(650, 94, 182, 510, 1, 103, 0, '', '', 54, '2019-04-01', '23:37:08'),
(651, 93, 176, 511, 1, 136, 0, '', '', 55, '2019-04-01', '23:37:46'),
(652, 93, 176, 511, 1, 109, 0, '', '', 55, '2019-04-01', '23:37:46'),
(653, 94, 182, 512, 4, 0, 0, '', '', 54, '2019-04-01', '23:38:44'),
(654, 94, 182, 513, 4, 0, 0, '', '', 54, '2019-04-01', '23:39:42'),
(655, 94, 182, 514, 4, 0, 0, '', '', 54, '2019-04-01', '23:41:03'),
(656, 94, 182, 515, 1, 90, 0, '', '', 54, '2019-04-01', '23:42:24'),
(657, 94, 182, 516, 4, 0, 0, '', '', 54, '2019-04-01', '23:43:08'),
(658, 94, 182, 517, 4, 77, 0, '', '', 54, '2019-04-01', '23:44:02'),
(659, 94, 185, 518, 4, 0, 0, '', '', 54, '2019-04-01', '23:45:56'),
(660, 94, 185, 519, 4, 0, 0, '', '', 54, '2019-04-01', '23:47:04'),
(662, 94, 185, 521, 4, 0, 0, '', '', 54, '2019-04-01', '23:52:32'),
(663, 94, 189, 522, 4, 0, 0, '', '', 54, '2019-04-01', '23:53:31'),
(664, 94, 189, 523, 4, 0, 0, '', '', 54, '2019-04-01', '23:54:16'),
(665, 94, 189, 524, 4, 0, 0, '', '', 54, '2019-04-01', '23:54:57'),
(666, 94, 190, 525, 1, 80, 0, '', '', 54, '2019-04-01', '23:56:41'),
(667, 94, 190, 526, 1, 123, 0, '', '', 54, '2019-04-01', '23:57:25'),
(668, 93, 176, 527, 1, 140, 0, '', '', 55, '2019-04-02', '08:37:38'),
(669, 93, 176, 528, 1, 137, 0, '', '', 55, '2019-04-02', '08:41:31'),
(670, 93, 176, 528, 1, 109, 0, '', '', 55, '2019-04-02', '08:41:31'),
(671, 93, 176, 529, 4, 0, 0, '', '', 55, '2019-04-02', '08:43:45'),
(672, 93, 176, 530, 1, 131, 0, '', '', 55, '2019-04-02', '08:47:49'),
(673, 93, 191, 531, 3, 0, 4, '', '', 55, '2019-04-02', '08:51:14'),
(674, 93, 191, 532, 1, 129, 0, '', '', 55, '2019-04-02', '08:55:27'),
(675, 93, 191, 532, 1, 130, 0, '', '', 55, '2019-04-02', '08:55:27'),
(676, 93, 192, 533, 1, 124, 0, '', '', 55, '2019-04-02', '08:58:43'),
(677, 93, 192, 534, 1, 127, 0, '', '', 55, '2019-04-02', '09:01:46'),
(678, 93, 192, 534, 1, 76, 0, '', '', 55, '2019-04-02', '09:01:46'),
(679, 93, 192, 535, 4, 0, 0, '', '', 55, '2019-04-02', '09:03:14'),
(680, 93, 192, 536, 4, 0, 0, '', '', 55, '2019-04-02', '10:12:48'),
(681, 93, 192, 537, 1, 93, 0, '', '', 55, '2019-04-02', '10:14:45'),
(682, 93, 192, 538, 4, 0, 0, '', '', 55, '2019-04-02', '10:17:07'),
(683, 93, 192, 539, 4, 0, 0, '', '', 55, '2019-04-02', '10:18:21'),
(684, 93, 192, 540, 1, 126, 0, '', '', 55, '2019-04-02', '10:29:29'),
(685, 93, 192, 541, 1, 25, 0, '', '', 55, '2019-04-02', '10:37:28'),
(686, 93, 193, 542, 3, 0, 11, '', '', 55, '2019-04-02', '10:42:54'),
(687, 93, 193, 543, 1, 133, 0, '', '', 55, '2019-04-02', '10:51:58'),
(688, 93, 193, 543, 1, 133, 0, '', '', 55, '2019-04-02', '10:51:58'),
(689, 93, 193, 544, 1, 134, 0, '', '', 55, '2019-04-02', '10:55:52'),
(690, 93, 193, 544, 1, 132, 0, '', '', 55, '2019-04-02', '10:55:52'),
(691, 93, 193, 545, 3, 0, 27, '', '', 55, '2019-04-02', '10:57:14'),
(692, 93, 193, 546, 1, 142, 0, '', '', 55, '2019-04-02', '10:59:16'),
(693, 93, 193, 547, 4, 0, 0, '', '', 55, '2019-04-02', '11:00:34'),
(694, 93, 194, 548, 1, 80, 0, '', '', 55, '2019-04-02', '11:03:25'),
(695, 93, 194, 548, 1, 141, 0, '', '', 55, '2019-04-02', '11:03:25'),
(696, 93, 195, 549, 1, 125, 0, '', '', 55, '2019-04-02', '11:07:00'),
(697, 93, 195, 549, 1, 132, 0, '', '', 55, '2019-04-02', '11:07:00'),
(698, 93, 195, 550, 3, 0, 28, '', '', 55, '2019-04-02', '11:13:15'),
(699, 75, 80, 551, 4, 0, 0, '', '', 25, '2019-04-02', '17:37:14'),
(700, 98, 196, 552, 2, 0, 0, '', 'files/detalle/listado/20190403_094512_0.jpg', 56, '2019-04-03', '09:45:12'),
(701, 98, 196, 552, 5, 0, 0, 'Criticados, juzgados pero jamás superados.', '', 56, '2019-04-03', '09:45:12'),
(702, 98, 196, 553, 2, 0, 0, '', 'files/detalle/listado/20190403_094719_0.jpg', 56, '2019-04-03', '09:47:19'),
(703, 98, 196, 553, 5, 0, 0, 'Criticados, juzgados pero jamás superados', '', 56, '2019-04-03', '09:47:19'),
(704, 98, 196, 554, 2, 0, 0, '', 'files/detalle/listado/20190403_094832_0.jpg', 56, '2019-04-03', '09:48:32'),
(705, 98, 196, 554, 5, 0, 0, 'Criticados, juzgados pero jamás superados', '', 56, '2019-04-03', '09:48:32'),
(706, 98, 196, 555, 2, 0, 0, '', 'files/detalle/listado/20190403_095137_0.jpg', 56, '2019-04-03', '09:51:37'),
(707, 98, 196, 555, 5, 0, 0, 'Criticados, juzgados pero jamás superados', '', 56, '2019-04-03', '09:51:37'),
(708, 98, 196, 555, 1, 29, 0, '', '', 56, '2019-04-03', '09:51:37'),
(709, 98, 196, 556, 2, 0, 0, '', 'files/detalle/listado/20190403_095453_0.jpg', 56, '2019-04-03', '09:54:53'),
(710, 98, 196, 556, 5, 0, 0, 'Criticados, juzgados pero jamás superados', '', 56, '2019-04-03', '09:54:53'),
(711, 98, 196, 556, 1, 109, 0, '', '', 56, '2019-04-03', '09:54:53'),
(712, 98, 196, 557, 2, 0, 0, '', 'files/detalle/listado/20190403_095610_0.jpg', 56, '2019-04-03', '09:56:10'),
(713, 98, 196, 557, 5, 0, 0, 'Criticados, juzgados pero jamás superados', '', 56, '2019-04-03', '09:56:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_diseno`
--

CREATE TABLE `pedido_diseno` (
  `disen_codig` int(11) NOT NULL,
  `pedid_codig` int(11) NOT NULL,
  `disen_timag` int(11) NOT NULL,
  `disen_orden` int(11) NOT NULL,
  `pdlim_codig` int(11) NOT NULL,
  `traba_codig` int(11) NOT NULL,
  `disen_fentr` date NOT NULL,
  `disen_freci` date NOT NULL,
  `ecort_codig` int(11) NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `disen_fcrea` date NOT NULL,
  `disen_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_emoji`
--

CREATE TABLE `pedido_emoji` (
  `emoji_codig` int(11) NOT NULL,
  `emoji_descr` varchar(50) DEFAULT NULL,
  `emoji_ruta` text NOT NULL,
  `usuar_codig` int(11) DEFAULT NULL,
  `emoji_fcrea` date DEFAULT NULL,
  `emoji_hcrea` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pedido_emoji`
--

INSERT INTO `pedido_emoji` (`emoji_codig`, `emoji_descr`, `emoji_ruta`, `usuar_codig`, `emoji_fcrea`, `emoji_hcrea`) VALUES
(4, 'CARA SACANDO LA LENGUA	', 'files/emoji/20181024_180324.png	', 3, '2018-10-24', '16:56:59'),
(5, 'CARA SONRIENTE CON OJOS EN FORMA DE CORAZÓN	', 'files/emoji/20181030_154008.JPG	', 3, '2018-10-30', '15:40:08'),
(6, 'CARA SONRIENTE CON LAGRIMAS DE RISA	', 'files/emoji/20181030_154923.JPG	', 3, '2018-10-30', '15:49:23'),
(7, 'CARA SONRIENTE CON 3 CORAZONES', 'files/emoji/20190129_110718.png', 3, '2019-01-29', '11:07:18'),
(8, 'CRÁNEO', 'files/emoji/20190129_110859.png', 3, '2019-01-29', '11:08:59'),
(9, 'CINTA', 'files/emoji/20190129_111209.png', 3, '2019-01-29', '11:12:09'),
(10, 'CARA SONRIENTE CON CUERNOS', 'files/emoji/20190129_111220.JPG', 22, '2019-01-29', '11:12:20'),
(11, 'ARCOIRIS', 'files/emoji/20190129_111243.png', 22, '2019-01-29', '11:12:43'),
(12, 'CARA SONRIENTE CON GAFAS DE SOL', 'files/emoji/20190129_111314.png', 22, '2019-01-29', '11:13:14'),
(13, 'AMANECER', 'files/emoji/20190129_111326.png', 3, '2019-01-29', '11:13:26'),
(14, 'CORONA', 'files/emoji/20190129_111340.JPG', 22, '2019-01-29', '11:13:40'),
(15, 'CARA DE FIESTA', 'files/emoji/20190129_111435.png', 3, '2019-01-29', '11:14:35'),
(16, 'CORAZÓN CON ESTRELLAS', 'files/emoji/20190129_111623.png', 22, '2019-01-29', '11:16:23'),
(17, 'BIZCAR CARA CON LENGUA', 'files/emoji/20190129_111635.png', 3, '2019-01-29', '11:16:35'),
(18, 'CARA MAREADO ', 'files/emoji/20190129_111804.png', 3, '2019-01-29', '11:18:04'),
(19, 'CARA CON VAPOR DE NARIZ', 'files/emoji/20190129_112005.png', 3, '2019-01-29', '11:20:05'),
(20, 'CARA CON MONÓCULO', 'files/emoji/20190129_112106.png', 3, '2019-01-29', '11:21:06'),
(21, 'FUEGO', 'files/emoji/20190129_112346.png', 3, '2019-01-29', '11:23:46'),
(22, 'CARA ENGREÍDA', 'files/emoji/20190129_112531.png', 3, '2019-01-29', '11:25:31'),
(23, 'CARA DE SUPLICA', 'files/emoji/20190129_112616.png', 22, '2019-01-29', '11:26:16'),
(24, 'BOMBA', 'files/emoji/20190129_112629.png', 3, '2019-01-29', '11:26:29'),
(25, 'CARA ESTRELLADA', 'files/emoji/20190129_112801.png', 3, '2019-01-29', '11:28:01'),
(26, 'EXTRATERRESTRE', 'files/emoji/20190129_112904.png', 3, '2019-01-29', '11:29:04'),
(27, 'CRÁNEO Y HUESOS CRUZADOS', 'files/emoji/20190129_113022.png', 3, '2019-01-29', '11:30:22'),
(28, 'PELOTA DE FÚTBOL', 'files/emoji/20190129_113239.png', 22, '2019-01-29', '11:32:39'),
(29, 'BESO CORAZÓN', 'files/emoji/20190129_190933.png', 22, '2019-01-29', '19:09:33'),
(30, 'CRANEO1', 'files/emoji/20190129_191257.JPG', 22, '2019-01-29', '19:12:57'),
(31, 'BICEP', 'files/emoji/20190129_191743.png', 22, '2019-01-29', '19:17:43'),
(32, 'CARA DE OSO PANDA', 'files/emoji/20190204_144229.png', 22, '2019-02-04', '14:42:29'),
(33, 'COPA DE VINO', 'files/emoji/20190204_144917.png', 22, '2019-02-04', '14:49:17'),
(34, 'PIRULETA', 'files/emoji/20190204_145037.png', 22, '2019-02-04', '14:50:37'),
(35, 'CARA SONRIENTE CON LA BOCA ABIERTA Y LOS OJOS CERR', 'files/emoji/20190205_083926.png', 22, '2019-02-05', '08:39:26'),
(37, 'PRINCESA', 'files/emoji/20190205_084400.jpg', 22, '2019-02-05', '08:44:00'),
(38, 'MARIPOSA', 'files/emoji/20190205_084535.jpg', 22, '2019-02-05', '08:45:35'),
(39, 'CANNABIS', 'files/emoji/20190205_084608.png', 22, '2019-02-05', '08:46:08'),
(40, 'CARA DE PERRO', 'files/emoji/20190205_084734.png', 22, '2019-02-05', '08:47:34'),
(41, 'CARA DE MONO', 'files/emoji/20190205_084905.png', 22, '2019-02-05', '08:49:05'),
(42, 'MONO “NO QUIERO NI VERLO”', 'files/emoji/20190205_085006.png', 22, '2019-02-05', '08:50:06'),
(43, 'GRÁFICO CON TENDENCIA AL ALZA', 'files/emoji/20190205_085237.png', 22, '2019-02-05', '08:52:37'),
(44, 'LIBROS', 'files/emoji/20190205_085505.png', 22, '2019-02-05', '08:55:05'),
(45, 'GESTO DE TE QUIERO', 'files/emoji/20190205_090446.JPG', 22, '2019-02-05', '09:04:46'),
(46, 'FLAMENCA', 'files/emoji/20190205_090808.png', 22, '2019-02-05', '09:08:08'),
(47, 'MANZANA VERDE', 'files/emoji/20190205_090926.png', 22, '2019-02-05', '09:09:26'),
(48, 'MANZANA ROJA', 'files/emoji/20190205_090957.png', 22, '2019-02-05', '09:09:57'),
(49, 'CIGARRILLO', 'files/emoji/20190205_091235.png', 22, '2019-02-05', '09:12:35'),
(50, 'FLOR MARCHITA', 'files/emoji/20190205_091356.jpg', 22, '2019-02-05', '09:13:56'),
(51, 'FLOR DE CEREZO', 'files/emoji/20190205_091933.JPG', 22, '2019-02-05', '09:19:33'),
(52, 'BESO', 'files/emoji/20190205_092142.png', 22, '2019-02-05', '09:21:42'),
(53, 'CORAZÓN ATRAVESADO POR UNA FLECHA', 'files/emoji/20190205_092344.png', 22, '2019-02-05', '09:23:44'),
(54, 'CORAZÓN NEGRO ', 'files/emoji/20190205_092559.JPG', 22, '2019-02-05', '09:25:59'),
(55, 'ELEFANTE', 'files/emoji/20190205_092850.png', 22, '2019-02-05', '09:28:50'),
(56, 'DRAGON', 'files/emoji/20190205_093034.JPG', 22, '2019-02-05', '09:30:34'),
(57, 'PLANETA', 'files/emoji/20190205_093436.png', 22, '2019-02-05', '09:34:36'),
(58, 'HOJA DE OTOÑO', 'files/emoji/20190205_093602.JPG', 22, '2019-02-05', '09:36:02'),
(59, 'COALA', 'files/emoji/20190205_094243.png', 22, '2019-02-05', '09:42:43'),
(60, 'CARTA DE JOKER', 'files/emoji/20190205_095038.png', 22, '2019-02-05', '09:50:38'),
(61, 'OPEN BOOK', 'files/emoji/20190205_095537.png', 22, '2019-02-05', '09:55:37'),
(62, 'REINO UNIDO', 'files/emoji/20190205_095645.png', 22, '2019-02-05', '09:56:45'),
(63, 'ESCUADRA', 'files/emoji/20190205_100021.png', 22, '2019-02-05', '10:00:21'),
(64, 'BANDERA DE CUADROS', 'files/emoji/20190205_100721.JPG', 22, '2019-02-05', '10:07:21'),
(65, 'ROSA', 'files/emoji/20190205_101035.png', 22, '2019-02-05', '10:10:35'),
(66, 'CAMARA', 'files/emoji/20190205_101542.png', 22, '2019-02-05', '10:15:42'),
(67, 'ISLA', 'files/emoji/20190205_101609.png', 22, '2019-02-05', '10:16:09'),
(68, 'LENGUA', 'files/emoji/20190205_101724.jpg', 22, '2019-02-05', '10:17:24'),
(69, 'LOBO', 'files/emoji/20190205_102103.png', 22, '2019-02-05', '10:21:03'),
(70, 'SERPIENTE', 'files/emoji/20190205_102149.JPG', 22, '2019-02-05', '10:21:49'),
(71, 'PRINCIPE', 'files/emoji/20190205_102331.png', 22, '2019-02-05', '10:23:31'),
(72, 'GATO', 'files/emoji/20190205_102454.png', 22, '2019-02-05', '10:24:54'),
(73, 'MUJER', 'files/emoji/20190205_102626.png', 22, '2019-02-05', '10:26:26'),
(74, 'HIBISCUS', 'files/emoji/20190205_102716.png', 22, '2019-02-05', '10:27:16'),
(75, 'GUANTES', 'files/emoji/20190205_102813.png', 22, '2019-02-05', '10:28:13'),
(76, 'DOS CORAZONES', 'files/emoji/20190205_143113.png', 3, '2019-02-05', '14:31:13'),
(77, 'EXTRATERRESTRE VERDE', 'files/emoji/20190205_204606.png', 22, '2019-02-05', '20:46:06'),
(78, 'GUINDA', 'files/emoji/20190209_094548.jpg', 25, '2019-02-09', '09:45:48'),
(79, 'CORAZÓN ROJO', 'files/emoji/20190210_135349.png', 22, '2019-02-10', '13:53:49'),
(80, 'DIAMANTE', 'files/emoji/20190210_135421.png', 22, '2019-02-10', '13:54:21'),
(81, 'RAYO', 'files/emoji/20190210_135449.png', 22, '2019-02-10', '13:54:49'),
(82, 'CARA DE TIGRE', 'files/emoji/20190210_135658.png', 22, '2019-02-10', '13:56:58'),
(83, 'CORAZÓN AZUL', 'files/emoji/20190210_135930.png', 22, '2019-02-10', '13:59:30'),
(84, 'CARA GUIÑANDO UN OJO', 'files/emoji/20190211_070405.png', 22, '2019-02-11', '07:04:05'),
(85, 'ESMALTE PARA LAS UÑAS', 'files/emoji/20190211_071419.png', 22, '2019-02-11', '07:14:19'),
(86, 'EMPLEADA DE UN MOSTRADOR DE INFORMACIÓN ', 'files/emoji/20190211_072323.png', 22, '2019-02-11', '07:23:23'),
(87, 'CORAZÓN EN CRECIMIENTO', 'files/emoji/20190211_080834.png', 22, '2019-02-11', '08:08:34'),
(88, 'PULPO', 'files/emoji/20190211_081645.png', 22, '2019-02-11', '08:16:45'),
(89, 'LUNA CRECIENTE', 'files/emoji/20190211_081905.png', 22, '2019-02-11', '08:19:05'),
(90, 'DINERO CON ALAS', 'files/emoji/20190211_082142.png', 22, '2019-02-11', '08:21:42'),
(91, 'OM', 'files/emoji/20190211_082339.png', 22, '2019-02-11', '08:23:39'),
(92, 'HOJA DE ARCE', 'files/emoji/20190211_082754.png', 22, '2019-02-11', '08:27:54'),
(93, 'GIRASOL', 'files/emoji/20190211_082903.png', 22, '2019-02-11', '08:29:03'),
(94, 'HOMBRE BAILANDO', 'files/emoji/20190211_083311.png', 22, '2019-02-11', '08:33:11'),
(95, 'TORNADO', 'files/emoji/20190211_083652.png', 22, '2019-02-11', '08:36:52'),
(96, 'CARA DE CABALLO', 'files/emoji/20190214_120231.png', 22, '2019-02-14', '12:02:31'),
(97, 'CARA DE LUNA NUEVA', 'files/emoji/20190214_120401.png', 22, '2019-02-14', '12:04:01'),
(98, 'LUNA LLENA CON CARA', 'files/emoji/20190214_120820.png', 22, '2019-02-14', '12:08:20'),
(99, 'GOTAS DE SUDOR', 'files/emoji/20190214_121600.png', 22, '2019-02-14', '12:16:00'),
(100, 'PALMERA', 'files/emoji/20190214_122401.png', 22, '2019-02-14', '12:24:01'),
(101, 'FANTASMA', 'files/emoji/20190214_125118.jpg', 22, '2019-02-14', '12:51:18'),
(102, 'LÁPIZ PLUMA', 'files/emoji/20190314_214224.png', 25, '2019-03-14', '21:42:24'),
(103, 'COHETE', 'files/emoji/20190331_221216.jpg', 25, '2019-03-31', '22:12:16'),
(104, 'MANO 1', 'files/emoji/20190331_222104.jpg', 25, '2019-03-31', '22:21:04'),
(105, 'PIÑA', 'files/emoji/20190331_222551.png', 25, '2019-03-31', '22:25:51'),
(106, 'BARBERO', 'files/emoji/20190331_223628.png', 25, '2019-03-31', '22:36:28'),
(107, 'FRUTILLA', 'files/emoji/20190331_223853.png', 25, '2019-03-31', '22:38:53'),
(108, 'LUNA Y ESTRELLA', 'files/emoji/20190331_224100.png', 25, '2019-03-31', '22:41:00'),
(109, 'ESTRELLAS 3', 'files/emoji/20190331_224447.png', 25, '2019-03-31', '22:44:47'),
(110, 'IV', 'files/emoji/20190331_224836.png', 25, '2019-03-31', '22:48:36'),
(111, 'PATITAS', 'files/emoji/20190331_225907.png', 25, '2019-03-31', '22:59:07'),
(112, 'ZORRO', 'files/emoji/20190401_010337.jpg', 25, '2019-04-01', '01:03:37'),
(113, 'DIABLO SONRIENTE', 'files/emoji/20190401_010507.png', 25, '2019-04-01', '01:05:07'),
(114, 'PINGÜINO', 'files/emoji/20190401_124146.png', 25, '2019-04-01', '12:41:46'),
(115, '100', 'files/emoji/20190401_202735.png', 25, '2019-04-01', '20:27:35'),
(116, 'MONO', 'files/emoji/20190401_213826.png', 25, '2019-04-01', '21:38:26'),
(117, 'GATO OJOS DE CORAZON', 'files/emoji/20190401_214228.png', 25, '2019-04-01', '21:42:28'),
(118, 'SALIVA', 'files/emoji/20190401_214901.png', 25, '2019-04-01', '21:49:01'),
(119, 'COQUETO', 'files/emoji/20190401_215239.jpeg', 25, '2019-04-01', '21:52:39'),
(120, 'FLOR TROPICAL', 'files/emoji/20190401_215914.png', 25, '2019-04-01', '21:59:14'),
(121, 'TROFEO', 'files/emoji/20190401_225228.png', 25, '2019-04-01', '22:52:28'),
(122, 'CARA DE MOLESTO', 'files/emoji/20190401_232739.png', 25, '2019-04-01', '23:27:39'),
(123, 'ANDROIDE', 'files/emoji/20190401_233105.png', 25, '2019-04-01', '23:31:05'),
(124, 'BANDERA GAY+', 'files/emoji/20190401_233205.jpg', 25, '2019-04-01', '23:32:05'),
(125, 'PELOTA BÁSQUETBOL', 'files/emoji/20190401_233307.png', 25, '2019-04-01', '23:33:07'),
(126, 'BALLENA', 'files/emoji/20190401_233347.png', 25, '2019-04-01', '23:33:47'),
(127, 'MANO AMOR Y PAZ', 'files/emoji/20190401_233424.png', 25, '2019-04-01', '23:34:24'),
(128, 'RATON', 'files/emoji/20190401_233443.png', 25, '2019-04-01', '23:34:43'),
(129, 'MAMADERA', 'files/emoji/20190401_233513.png', 25, '2019-04-01', '23:35:13'),
(130, 'PALETA CARAMELO', 'files/emoji/20190401_233542.png', 25, '2019-04-01', '23:35:42'),
(131, 'AGUILA', 'files/emoji/20190401_233858.png', 25, '2019-04-01', '23:36:45'),
(132, 'JOYSTICK', 'files/emoji/20190401_233944.png', 25, '2019-04-01', '23:39:44'),
(133, 'CARA LENTES SOL', 'files/emoji/20190401_234022.png', 25, '2019-04-01', '23:40:22'),
(134, 'GUITARRA ELECTRICA', 'files/emoji/20190401_234129.png', 25, '2019-04-01', '23:41:29'),
(135, 'FLOR DE LIZ', 'files/emoji/20190401_234259.png', 25, '2019-04-01', '23:42:59'),
(136, 'PISCIS', 'files/emoji/20190401_234424.png', 25, '2019-04-01', '23:44:24'),
(137, 'LEO', 'files/emoji/20190401_234552.png', 25, '2019-04-01', '23:45:52'),
(138, 'CALABERA', 'files/emoji/20190401_234634.png', 25, '2019-04-01', '23:46:34'),
(139, 'CALABERA 2', 'files/emoji/20190401_234719.png', 25, '2019-04-01', '23:47:19'),
(140, 'FLOR DE CAMPO', 'files/emoji/20190401_234838.png', 25, '2019-04-01', '23:48:38'),
(141, 'ANCLA', 'files/emoji/20190401_235030.png', 25, '2019-04-01', '23:50:30'),
(142, 'SIRENA', 'files/emoji/20190401_235319.png', 25, '2019-04-01', '23:53:19'),
(143, 'TREBOL', 'files/emoji/20190402_221435.png', 25, '2019-04-02', '22:14:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_estatu`
--

CREATE TABLE `pedido_estatu` (
  `epedi_codig` int(11) NOT NULL,
  `epedi_descr` varchar(50) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Volcado de datos para la tabla `pedido_estatu`
--

INSERT INTO `pedido_estatu` (`epedi_codig`, `epedi_descr`) VALUES
(1, 'ACTIVO'),
(7, 'APROBADO'),
(6, 'ATRASADO'),
(4, 'DISPONIBLE PARA ENTREGA'),
(3, 'EN PROCESO DE PRODUCCIÓN'),
(5, 'ENTREGADO'),
(2, 'ENVIADO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_estatu_pago`
--

CREATE TABLE `pedido_estatu_pago` (
  `epago_codig` int(11) NOT NULL,
  `epago_descr` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `epago_fcrea` date NOT NULL,
  `epago_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Volcado de datos para la tabla `pedido_estatu_pago`
--

INSERT INTO `pedido_estatu_pago` (`epago_codig`, `epago_descr`, `usuar_codig`, `epago_fcrea`, `epago_hcrea`) VALUES
(1, 'PENDIENTE', 3, '2018-12-04', '11:26:40'),
(2, 'APROBADO', 3, '2018-12-04', '13:23:31'),
(3, 'RECHAZADO', 3, '2018-12-04', '13:23:31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_fuente`
--

CREATE TABLE `pedido_fuente` (
  `fuent_codig` int(11) NOT NULL,
  `fuent_descr` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `fuent_ruta` text COLLATE utf8mb4_bin NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `fuent_fcrea` date NOT NULL,
  `fuent_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Volcado de datos para la tabla `pedido_fuente`
--

INSERT INTO `pedido_fuente` (`fuent_codig`, `fuent_descr`, `fuent_ruta`, `usuar_codig`, `fuent_fcrea`, `fuent_hcrea`) VALUES
(1, 'ARIAL', 'files/fuente/20181030_210349.png', 3, '2018-10-30', '21:03:49'),
(2, 'FORTE', 'files/fuente/20190128_152050.jpg', 22, '2019-01-28', '15:20:50'),
(3, 'ROCKWELL', 'files/fuente/20190129_101847.gif', 3, '2019-01-29', '10:18:47'),
(4, 'LUCIDA CALLIGRAPHY', 'files/fuente/20190129_101929.gif', 3, '2019-01-29', '10:19:29'),
(5, 'ARIAL ROUNDED MT BOLD', 'files/fuente/20190129_102059.gif', 3, '2019-01-29', '10:20:59'),
(6, 'GUNNY MANUAL', 'files/fuente/20190129_102314.jpg', 3, '2019-01-29', '10:23:14'),
(7, 'LHANDW ', 'files/fuente/20190129_102539.png', 3, '2019-01-29', '10:25:39'),
(8, 'GABRIOLA', 'files/fuente/20190129_102631.gif', 3, '2019-01-29', '10:26:31'),
(9, 'MJF ZHAFIRA', 'files/fuente/20190129_102727.jpg', 3, '2019-01-29', '10:27:27'),
(10, 'MTCORSVA', 'files/fuente/20190129_103047.jpg', 3, '2019-01-29', '10:30:47'),
(11, 'SEGOE PRINT', 'files/fuente/20190129_103415.gif', 3, '2019-01-29', '10:34:15'),
(12, 'ALGER ', 'files/fuente/20190129_103558.jpg', 3, '2019-01-29', '10:35:58'),
(13, 'AR BLANCA ', 'files/fuente/20190129_103721.jpg', 3, '2019-01-29', '10:37:21'),
(14, 'COLLEGE ', 'files/fuente/20190129_103835.png', 3, '2019-01-29', '10:38:35'),
(15, 'CALIBRI', 'files/fuente/20190129_104107.gif', 3, '2019-01-29', '10:41:07'),
(16, 'ELEPHANT ', 'files/fuente/20190129_104203.gif', 3, '2019-01-29', '10:42:03'),
(17, 'BERNARD MT', 'files/fuente/20190206_094031.JPG', 22, '2019-02-06', '09:40:31'),
(18, 'MARKER FELT', 'files/fuente/20190206_100953.gif', 22, '2019-02-06', '10:09:53'),
(19, 'COMIC SANS ', 'files/fuente/20190206_114259.gif', 22, '2019-02-06', '11:42:59'),
(20, 'LUCIDA HANDWRITING', 'files/fuente/20190206_114340.gif', 22, '2019-02-06', '11:43:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_incluye`
--

CREATE TABLE `pedido_incluye` (
  `inclu_codig` int(11) NOT NULL,
  `inclu_descr` varchar(50) NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `inclu_fcrea` date NOT NULL,
  `inclu_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pedido_incluye`
--

INSERT INTO `pedido_incluye` (`inclu_codig`, `inclu_descr`, `usuar_codig`, `inclu_fcrea`, `inclu_hcrea`) VALUES
(1, 'CIERRE', 3, '2018-12-13', '09:45:12'),
(2, 'GORRO', 3, '2018-12-13', '09:45:12'),
(3, 'PUÑO Y PRETINA', 3, '2018-12-13', '09:45:59'),
(4, 'BROCHE', 3, '2018-12-13', '09:45:59'),
(5, 'VIVO EN EL BOLSILLO', 3, '2019-03-14', '11:16:13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_lista`
--

CREATE TABLE `pedido_lista` (
  `lista_codig` int(11) NOT NULL,
  `detal_model` int(11) DEFAULT NULL,
  `pedig_codig` int(11) DEFAULT NULL,
  `pedid_nume` varchar(50) DEFAULT NULL,
  `categ_codig` int(11) DEFAULT NULL,
  `lista_nombr` varchar(50) DEFAULT NULL,
  `lista_ident` varchar(50) DEFAULT NULL,
  `lista_talla` varchar(50) DEFAULT NULL,
  `lista_npers` varchar(50) DEFAULT NULL,
  `lista_aespa` varchar(50) DEFAULT NULL,
  `lista_elect` varchar(50) DEFAULT NULL,
  `lista_emoji` int(11) DEFAULT NULL,
  `lista_imag` varchar(150) DEFAULT NULL,
  `lista_obser` varchar(50) DEFAULT NULL,
  `usuar_codig` int(11) DEFAULT NULL,
  `lista_fcrea` date DEFAULT NULL,
  `lista_hcrea` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pedido_lista`
--

INSERT INTO `pedido_lista` (`lista_codig`, `detal_model`, `pedig_codig`, `pedid_nume`, `categ_codig`, `lista_nombr`, `lista_ident`, `lista_talla`, `lista_npers`, `lista_aespa`, `lista_elect`, `lista_emoji`, `lista_imag`, `lista_obser`, `usuar_codig`, `lista_fcrea`, `lista_hcrea`) VALUES
(12, 1, 3, 'Y1202', 2, 'ALDEMAR', '122', 'XL', 'EL PAPI', 'PAPIEADO', 'ELECTIVA', 0, 'C:/AppServ/www/files/20181016225609_WIN_20180725_214218 - copia.JPG', 'PRUEBA2', 3, '2018-10-16', '22:57:14'),
(13, 1, 3, 'Y1202', 2, '1212', '1212', '12121', '12121', '1212', '1212', 1, '', '1212', 3, '2018-10-17', '20:22:27'),
(14, 1, 3, 'Y1202', 2, 'KEVIN FIGUERA', '24900845', 'XL', 'KFIGUERA', 'KFIGUERA', 'A', 0, '', 'PRUEBA CON IMAGEN LIBRE', 3, '2018-10-17', '22:22:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_modelo`
--

CREATE TABLE `pedido_modelo` (
  `model_codig` int(11) NOT NULL,
  `model_descr` varchar(50) DEFAULT NULL,
  `model_gorro` int(11) NOT NULL,
  `model_preti` int(11) NOT NULL,
  `model_mensa` text,
  `model_obser` text NOT NULL,
  `usuar_codig` int(11) DEFAULT NULL,
  `model_fcrea` date DEFAULT NULL,
  `model_hcrea` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pedido_modelo`
--

INSERT INTO `pedido_modelo` (`model_codig`, `model_descr`, `model_gorro`, `model_preti`, `model_mensa`, `model_obser`, `usuar_codig`, `model_fcrea`, `model_hcrea`) VALUES
(1, 'CUELLO POLO', 2, 2, 'BORDADO ESTANDAR, TELAS NACIONALES', '20 O MAS - MENOS CONSULTAR PRECIO', 3, '2018-10-18', '10:01:00'),
(2, 'CUELLO CIERRE', 0, 0, 'Bordado Estandar, \r\n Cordones y ojetillos metálicos Telas\r\n nacionales.', '20 o mas - menos consultar precio', 3, '2018-10-18', '10:01:00'),
(3, 'CANGURO ', 1, 2, 'BORDADO ESTANDAR, \r\n CORDONES Y OJETILLOS METÁLICOS TELAS\r\n NACIONALES.', '20 O MAS - MENOS CONSULTAR PRECIO', 3, '2018-10-18', '10:01:00'),
(5, 'AMERICANO', 1, 1, 'BORDADO ESTANDAR, PUÑOS Y PRETINAS CON LINEAS, BROCHES O CIERRE FORRO INTERIOR DELANTERO, TELAS NACIONALES.', '20 O MAS - MENOS CONSULTAR PRECIO', 3, '2018-10-18', '10:01:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_modelo_categoria`
--

CREATE TABLE `pedido_modelo_categoria` (
  `mcate_codig` int(11) NOT NULL,
  `mcate_descr` varchar(50) DEFAULT NULL,
  `usuar_codig` int(11) DEFAULT NULL,
  `mcate_fcrea` date DEFAULT NULL,
  `mcate_hcrea` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pedido_modelo_categoria`
--

INSERT INTO `pedido_modelo_categoria` (`mcate_codig`, `mcate_descr`, `usuar_codig`, `mcate_fcrea`, `mcate_hcrea`) VALUES
(1, 'SIN CATEGORIA', 3, '2018-10-18', '10:22:00'),
(2, 'CERRADO CUELLO ALTO CON BROCHE', 3, '2018-10-18', '10:22:00'),
(3, 'CERRADO CORTE DIAGONAL DELANTERO', 3, '2018-10-18', '10:22:00'),
(4, 'CUELLO ALTO\r\n', 3, '2018-10-18', '10:22:00'),
(5, 'CON CAPUCHA', 22, '2018-12-11', '12:46:37'),
(6, 'SIN CAPUCHA', 22, '2018-12-11', '12:46:58'),
(7, 'BOLSILLO CASACA', 22, '2018-12-11', '12:47:13'),
(8, 'BOLSILLO CANGURO', 22, '2018-12-11', '12:47:26'),
(9, 'CON CIERRE', 22, '2018-12-12', '10:17:15'),
(10, 'CERRADO', 22, '2018-12-12', '10:17:37'),
(11, 'CIERRE CUELLO ALTO', 22, '2018-12-12', '10:18:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_modelo_color`
--

CREATE TABLE `pedido_modelo_color` (
  `mcolo_codig` int(11) NOT NULL,
  `mcolo_numer` varchar(50) NOT NULL,
  `mcolo_descr` varchar(50) NOT NULL,
  `mcolo_ruta` text NOT NULL,
  `tmode_codig` int(11) NOT NULL,
  `usuar_codig` int(11) NOT NULL DEFAULT '0',
  `mcolo_fcrea` date NOT NULL,
  `mcolo_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='colores de los articulos';

--
-- Volcado de datos para la tabla `pedido_modelo_color`
--

INSERT INTO `pedido_modelo_color` (`mcolo_codig`, `mcolo_numer`, `mcolo_descr`, `mcolo_ruta`, `tmode_codig`, `usuar_codig`, `mcolo_fcrea`, `mcolo_hcrea`) VALUES
(8, '4', 'AZUL REY', 'files/color/20190128_223742.JPG', 1, 22, '2019-01-28', '22:37:42'),
(9, '1', 'MARINO', 'files/color/20190128_223821.JPG', 1, 22, '2019-01-28', '22:38:21'),
(10, '2', 'FRANCIA', 'files/color/20190128_223848.JPG', 1, 22, '2019-01-28', '22:38:48'),
(11, '9', 'CALIPSO', 'files/color/20190128_223921.JPG', 1, 22, '2019-01-28', '22:39:21'),
(12, '11', 'AZUL PETROLEO', 'files/color/20190128_223958.JPG', 1, 22, '2019-01-28', '22:39:58'),
(13, '12', 'VERDE PETROLEO', 'files/color/20190128_224037.JPG', 1, 22, '2019-01-28', '22:40:37'),
(14, '15', 'VERDE TURQUEZA', 'files/color/20190128_224119.JPG', 1, 22, '2019-01-28', '22:41:19'),
(15, '17', 'VERDE ROMBOCOL', 'files/color/20190128_224204.JPG', 1, 22, '2019-01-28', '22:42:04'),
(16, '18', 'VERDE CATA', 'files/color/20190128_224238.JPG', 1, 22, '2019-01-28', '22:42:38'),
(17, '19', 'GRIS', 'files/color/20190128_224308.JPG', 1, 22, '2019-01-28', '22:43:08'),
(18, '23', 'MELANGE', 'files/color/20190128_224340.JPG', 1, 22, '2019-01-28', '22:43:40'),
(19, '25', 'MARENGO', 'files/color/20190128_224418.JPG', 1, 22, '2019-01-28', '22:44:18'),
(20, '26', 'NEGRO', 'files/color/20190128_224447.JPG', 1, 22, '2019-01-28', '22:44:47'),
(21, '27', 'ROSADO', 'files/color/20190128_224513.JPG', 1, 22, '2019-01-28', '22:45:13'),
(22, '28', 'AMARILLO PATO', 'files/color/20190128_224551.JPG', 1, 22, '2019-01-28', '22:45:51'),
(23, '42', 'MOSTAZA', 'files/color/20190128_224620.JPG', 1, 22, '2019-01-28', '22:46:20'),
(24, '43', 'OBISPO', 'files/color/20190128_224651.JPG', 1, 22, '2019-01-28', '22:46:51'),
(25, '44', 'BLANCO', 'files/color/20190128_224724.JPG', 1, 22, '2019-01-28', '22:47:24'),
(26, '30', 'LILA', 'files/color/20190128_224922.JPG', 1, 22, '2019-01-28', '22:49:22'),
(27, '32', 'NARANJO', 'files/color/20190128_224955.JPG', 1, 22, '2019-01-28', '22:49:55'),
(28, '34', 'CORAL', 'files/color/20190128_225035.JPG', 1, 22, '2019-01-28', '22:50:35'),
(29, '35', 'FUCSIA', 'files/color/20190128_225121.JPG', 1, 22, '2019-01-28', '22:51:21'),
(30, '36', 'ROJO ITALIANO', 'files/color/20190128_225153.JPG', 1, 22, '2019-01-28', '22:51:53'),
(31, '37', 'ROJO BANDERA', 'files/color/20190128_225229.JPG', 1, 22, '2019-01-28', '22:52:29'),
(32, '38', 'BURDEO', 'files/color/20190128_225256.JPG', 1, 22, '2019-01-28', '22:52:56'),
(33, '40', 'CRUDO', 'files/color/20190128_225336.JPG', 1, 22, '2019-01-28', '22:53:36'),
(34, '1', 'ACERO', 'files/color/20190129_093821.jpg', 2, 22, '2019-01-29', '09:38:21'),
(35, '2', 'AMARILLO ORO', 'files/color/20190129_093852.jpg', 2, 22, '2019-01-29', '09:38:52'),
(36, '3', 'AMARILLO PATO', 'files/color/20190129_093920.jpg', 2, 22, '2019-01-29', '09:39:20'),
(37, '6', 'AZUL PETROLEO', 'files/color/20190129_094109.jpg', 2, 22, '2019-01-29', '09:41:09'),
(38, '4', 'AZUL MARINO', 'files/color/20190129_094454.jpg', 2, 22, '2019-01-29', '09:44:54'),
(39, '5', 'AZUL REY', 'files/color/20190129_094557.jpg', 2, 22, '2019-01-29', '09:45:57'),
(40, '7', 'BEIGE', 'files/color/20190129_094640.jpg', 2, 22, '2019-01-29', '09:46:40'),
(41, '8', 'BLANCO', 'files/color/20190129_094714.jpg', 2, 22, '2019-01-29', '09:47:14'),
(42, '9', 'BURDEO', 'files/color/20190129_094740.jpg', 2, 22, '2019-01-29', '09:47:40'),
(43, '10', 'CALIPSO', 'files/color/20190129_094818.jpg', 2, 22, '2019-01-29', '09:48:18'),
(44, '11', 'CIAN', 'files/color/20190129_094848.jpg', 2, 22, '2019-01-29', '09:48:48'),
(45, '12', 'CRUDO', 'files/color/20190129_094919.jpg', 2, 22, '2019-01-29', '09:49:19'),
(46, '13', 'FRANCIA', 'files/color/20190129_101315.jpg', 2, 22, '2019-01-29', '10:13:15'),
(47, '14', 'GRAFITO', 'files/color/20190129_101551.jpg', 2, 22, '2019-01-29', '10:15:51'),
(48, '15', 'MAGENTA', 'files/color/20190129_101643.jpg', 2, 22, '2019-01-29', '10:16:43'),
(49, '16', 'NARANJO', 'files/color/20190129_101738.jpg', 2, 22, '2019-01-29', '10:17:38'),
(50, '17', 'NEGRO', 'files/color/20190129_101814.jpg', 2, 22, '2019-01-29', '10:18:14'),
(51, '17', 'OBISPO', 'files/color/20190129_101852.jpg', 2, 22, '2019-01-29', '10:18:52'),
(52, '18', 'PLATA', 'files/color/20190129_101926.jpg', 2, 22, '2019-01-29', '10:19:26'),
(53, '18', 'ROJO', 'files/color/20190129_102012.jpg', 2, 22, '2019-01-29', '10:20:12'),
(54, '19', 'ROMBOCAL', 'files/color/20190129_102143.jpg', 2, 22, '2019-01-29', '10:21:43'),
(55, '20', 'ROSADO', 'files/color/20190129_102231.jpg', 2, 22, '2019-01-29', '10:22:31'),
(56, '21', 'VERDE BOTELLA', 'files/color/20190129_102434.jpg', 2, 22, '2019-01-29', '10:24:34'),
(57, '22', 'VERDE PETROLEO', 'files/color/20190129_102622.jpg', 2, 22, '2019-01-29', '10:26:22'),
(58, '0', 'SIN LÍNEA', 'files/color/20190131_144423.png', 1, 3, '2019-01-31', '14:44:23'),
(59, '0', 'SIN LÍNEA', 'files/color/20190131_144459.png', 2, 3, '2019-01-31', '14:44:59'),
(60, '31', 'DAMASCO', 'files/color/20190206_155228.jpeg', 1, 25, '2019-02-06', '15:52:28'),
(61, '31', 'DAMASCO', 'files/color/20190206_155328.jpeg', 2, 25, '2019-02-06', '15:53:28'),
(62, '70', 'MILITAR COLORES', 'files/color/20190327_184653.jpg', 1, 25, '2019-03-27', '18:46:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_modelo_conjunto`
--

CREATE TABLE `pedido_modelo_conjunto` (
  `mconj_codig` int(11) NOT NULL,
  `tmode_codig` int(11) NOT NULL,
  `model_codig` int(11) NOT NULL,
  `mcate_codig` int(11) NOT NULL,
  `mvari_codig` int(11) NOT NULL,
  `mopci_codig` text COLLATE utf8mb4_bin NOT NULL,
  `mopci_codi2` text COLLATE utf8mb4_bin NOT NULL,
  `mconj_preci` double NOT NULL,
  `mconj_inclu` text COLLATE utf8mb4_bin NOT NULL,
  `mconj_ruta` text COLLATE utf8mb4_bin NOT NULL,
  `mconj_obser` text COLLATE utf8mb4_bin NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `mconj_fcrea` date NOT NULL,
  `mconj_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Volcado de datos para la tabla `pedido_modelo_conjunto`
--

INSERT INTO `pedido_modelo_conjunto` (`mconj_codig`, `tmode_codig`, `model_codig`, `mcate_codig`, `mvari_codig`, `mopci_codig`, `mopci_codi2`, `mconj_preci`, `mconj_inclu`, `mconj_ruta`, `mconj_obser`, `usuar_codig`, `mconj_fcrea`, `mconj_hcrea`) VALUES
(8, 1, 1, 1, 1, '[\"1\",\"6\"]', 'null', 18000, '[\"3\"]', 'files/conjunto/20181212_103420.png', '1', 3, '2018-10-30', '16:54:16'),
(11, 1, 3, 10, 1, '[\"1\",\"6\",\"8\"]', '[\"3\",\"6\"]', 21000, '[\"2\",\"3\"]', 'files/conjunto/20181212_102445.png', '', 3, '2018-11-08', '16:32:59'),
(12, 1, 3, 2, 1, '[\"1\",\"6\",\"8\"]', '[\"3\",\"6\"]', 26000, '[\"2\",\"4\"]', 'files/conjunto/20181212_102812.png', '', 3, '2018-11-08', '16:33:50'),
(13, 1, 3, 3, 1, '[\"1\",\"6\",\"8\"]', '[\"3\",\"6\"]', 25000, '[\"2\",\"3\"]', 'files/conjunto/20181212_102900.png', '', 3, '2018-11-08', '16:34:49'),
(17, 1, 5, 6, 1, '[\"2\",\"4\",\"5\",\"7\",\"8\"]', '', 27000, '[\"1\",\"3\",\"4\"]', 'files/conjunto/20181212_103648.png', '', 3, '2018-11-08', '16:39:25'),
(18, 2, 5, 5, 1, '[\"2\",\"4\",\"5\",\"7\"]', '', 31000, '[\"1\",\"2\",\"3\",\"4\"]', 'files/conjunto/20181212_103147.png', '', 3, '2018-11-08', '16:40:27'),
(20, 1, 2, 7, 2, '[\"1\",\"6\",\"8\"]', '', 23500, '[\"1\",\"3\"]', 'files/conjunto/20181212_103316.png', '', 3, '2018-11-27', '21:30:50'),
(21, 1, 2, 8, 3, '[\"1\",\"6\",\"8\"]', '[\"3\",\"6\"]', 22500, '[\"1\",\"3\"]', 'files/conjunto/20181212_103350.png', '', 3, '2018-11-27', '21:31:13'),
(22, 1, 3, 9, 1, '[\"1\",\"6\",\"8\"]', '[\"3\",\"6\"]', 22500, '[\"1\",\"2\"]', 'files/conjunto/20181212_102546.png', '', 22, '2018-12-12', '10:25:46'),
(23, 1, 3, 11, 1, '[\"1\",\"6\",\"8\"]', '[\"3\",\"6\"]', 25000, '[\"1\",\"2\",\"3\"]', 'files/conjunto/20181212_102714.png', '', 22, '2018-12-12', '10:27:14'),
(24, 2, 3, 9, 1, '[\"1\",\"6\"]', '[\"3\",\"6\"]', 27500, '[\"1\",\"2\",\"3\"]', 'files/conjunto/20181212_103009.png', '', 22, '2018-12-12', '10:30:09'),
(26, 1, 5, 5, 1, '[\"2\",\"4\",\"5\",\"7\",\"8\"]', '', 27000, '[\"1\",\"2\",\"3\",\"4\"]', 'files/conjunto/20181212_104238.png', '', 22, '2018-12-12', '10:42:38'),
(30, 2, 5, 6, 1, '[\"2\",\"4\",\"5\",\"7\"]', '', 31000, '[\"1\",\"3\",\"4\"]', 'files/conjunto/20190211_110340.jpg', '', 25, '2019-02-11', '11:03:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_modelo_opcional`
--

CREATE TABLE `pedido_modelo_opcional` (
  `mopci_codig` int(11) NOT NULL,
  `mopci_descr` varchar(50) DEFAULT NULL,
  `mopci_preci` double DEFAULT NULL,
  `mopci_inclu` text NOT NULL,
  `usuar_codig` int(11) DEFAULT NULL,
  `mopci_fcrea` date DEFAULT NULL,
  `mopci_hcrea` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pedido_modelo_opcional`
--

INSERT INTO `pedido_modelo_opcional` (`mopci_codig`, `mopci_descr`, `mopci_preci`, `mopci_inclu`, `usuar_codig`, `mopci_fcrea`, `mopci_hcrea`) VALUES
(1, 'PUÑOS Y PRETINAS CON LINEAS', 2500, '[\"3\"]', 3, '2018-10-18', '10:31:23'),
(2, 'BROCHE Y CIERRE JUNTOS', 2000, '[\"1\",\"4\"]', 3, '2018-10-18', '10:31:23'),
(3, 'VIVO EN EL BOLSILLO', 500, '[\"5\"]', 3, '2018-10-18', '10:31:23'),
(4, 'BROCHE', 0, '[\"4\"]', 22, '2018-12-12', '10:39:02'),
(5, 'CIERRE', 0, '[\"1\"]', 22, '2018-12-12', '10:39:16'),
(6, 'SIN ADICIONAL', 0, '', 22, '2018-12-12', '21:31:34'),
(7, 'PRETINA Y PUÑO SIN LINEA', 2500, '[\"3\"]', 22, '2019-02-01', '20:45:54'),
(8, 'MANGAS Y GORRO MILITAR/FLOREADO', 3000, '', 25, '2019-02-05', '15:22:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_modelo_tipo`
--

CREATE TABLE `pedido_modelo_tipo` (
  `tmode_codig` int(11) NOT NULL,
  `tmode_descr` varchar(50) DEFAULT NULL,
  `usuar_codig` int(11) DEFAULT NULL,
  `tmode_fcrea` date DEFAULT NULL,
  `tmode_hcrea` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pedido_modelo_tipo`
--

INSERT INTO `pedido_modelo_tipo` (`tmode_codig`, `tmode_descr`, `usuar_codig`, `tmode_fcrea`, `tmode_hcrea`) VALUES
(1, 'POLERON ALGODON', 3, '2018-10-18', '09:35:00'),
(2, 'CORTAVIENTO', 3, '2018-10-18', '09:35:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_modelo_variante`
--

CREATE TABLE `pedido_modelo_variante` (
  `mvari_codig` int(11) NOT NULL,
  `mvari_descr` varchar(50) DEFAULT NULL,
  `mvari_preci` double DEFAULT NULL,
  `usuar_codig` int(11) DEFAULT NULL,
  `mvari_fcrea` date DEFAULT NULL,
  `mvari_hcrea` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pedido_modelo_variante`
--

INSERT INTO `pedido_modelo_variante` (`mvari_codig`, `mvari_descr`, `mvari_preci`, `usuar_codig`, `mvari_fcrea`, `mvari_hcrea`) VALUES
(1, 'SIN VARIANTE', 0, 3, '2018-10-18', '11:41:20'),
(2, 'BOLSILLO CASACA', 1000, 3, '2018-10-18', '10:36:43'),
(3, 'BOLSILLO CANGURO', 0, 3, '2018-10-18', '10:36:43');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_pagos`
--

CREATE TABLE `pedido_pagos` (
  `pagos_codig` int(11) NOT NULL,
  `pagos_numer` varchar(20) COLLATE utf8mb4_bin NOT NULL,
  `pedid_codig` int(11) NOT NULL,
  `ptipo_codig` int(11) NOT NULL,
  `epago_codig` int(11) NOT NULL,
  `pagos_refer` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `pagos_monto` double NOT NULL,
  `pagos_rimag` text COLLATE utf8mb4_bin NOT NULL,
  `pagos_obser` text COLLATE utf8mb4_bin NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `pagos_fcrea` date NOT NULL,
  `pagos_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Volcado de datos para la tabla `pedido_pagos`
--

INSERT INTO `pedido_pagos` (`pagos_codig`, `pagos_numer`, `pedid_codig`, `ptipo_codig`, `epago_codig`, `pagos_refer`, `pagos_monto`, `pagos_rimag`, `pagos_obser`, `usuar_codig`, `pagos_fcrea`, `pagos_hcrea`) VALUES
(1, 'P201902-00001', 39, 5, 2, 'ABONO INCIAL', 466000, 'files/detalle/pedido/39/pagos/20190222_095400_P201902-00001.jpeg', '', 25, '2019-02-22', '09:54:00'),
(2, 'P201902-00002', 47, 4, 2, 'ABONO INCIAL', 913500, '', '', 25, '2019-02-22', '10:03:50'),
(3, 'P201902-00003', 47, 5, 2, 'ABONO  PARCIAL A SALDO', 850000, 'files/detalle/pedido/47/pagos/20190222_100712_P201902-00003.png', '', 25, '2019-02-22', '10:07:12'),
(4, 'P201902-00004', 47, 1, 2, 'SALDO FINAL', 64500, '', '', 25, '2019-02-22', '10:08:13'),
(5, 'P201902-00005', 48, 1, 2, 'ABONO EN COLEGIO + EN LOCAL', 146250, '', 'YA SE DIO BOLETA POR  135.000', 25, '2019-02-22', '11:44:12'),
(9, 'P201902-00006', 73, 5, 2, '', 490500, 'files/detalle/pedido/73/pagos/20190226_101628_P201902-00006.jpeg', '', 33, '2019-02-26', '10:16:28'),
(10, 'P201903-00010', 76, 5, 2, 'ABONO 50%', 451750, 'files/detalle/pedido/76/pagos/20190317_133125_P201903-00010.png', '', 40, '2019-03-17', '13:31:25'),
(11, 'P201903-00011', 89, 6, 2, '', 245000, 'files/detalle/pedido/89/pagos/20190331_235204_P201903-00011.png', '', 25, '2019-03-31', '23:52:04'),
(13, 'P201904-00012', 72, 5, 2, '', 561000, 'files/detalle/pedido/72/pagos/20190401_003231_P201904-00012.png', '', 25, '2019-04-01', '00:32:31'),
(14, 'P201904-00014', 88, 1, 2, 'ABONADO', 227000, 'files/detalle/pedido/88/pagos/20190401_080838_P201904-00014.jpg', 'HOLA, MIRA EN LA ESPECIFICACIÓN QUE ME MANDASTE DEL PEDIDO POR TELÉFONO, EN LA PARTE DE LA ESPALDA EL DISEÑO DE IV SOLO LO QUEREMOS CON LOS BORDES BLANCOS, NO PINTADO COMO APARECE EN LA FOTO QUE TE ENVIAMOS. AQUÍ VERIFICAMOS EL PEDIDO Y EL MONTO ABONADO, GRACIAS.', 50, '2019-04-01', '08:08:38'),
(15, 'P201904-00015', 91, 6, 2, '', 130000, 'files/detalle/pedido/91/pagos/20190402_134935_P201904-00015.jpeg', '', 25, '2019-04-02', '13:49:35'),
(16, 'P201904-00016', 96, 5, 2, '', 262500, 'files/detalle/pedido/96/pagos/20190402_204220_P201904-00016.jpeg', 'COMPRA SE COMPONE DE PEDIDO N° 85 Y 95 AMBOS SUMAN 525.000  , SE ABONA  262.500.  ', 25, '2019-04-02', '20:42:20'),
(17, 'P201904-00017', 87, 5, 2, '', 0, '', 'COMPRA SE COMPONE DE PEDIDO N° 85 Y 95 AMBOS SUMAN 525.000 , SE ABONA 262.500, REGISTRADO TODO EN EL PEDIDO 95.', 25, '2019-04-02', '20:44:23'),
(18, 'P201904-00018', 75, 1, 2, 'EN LA VISITA', 60000, '', '', 25, '2019-04-02', '21:52:51'),
(19, 'P201904-00019', 75, 6, 2, '5656', 308, '', '', 3, '2019-04-03', '13:12:42'),
(20, 'P201904-00020', 75, 6, 2, '5656', 308000, '', '', 3, '2019-04-03', '13:13:00'),
(21, 'P201904-00021', 75, 6, 1, '5656', 12345678, '', '', 3, '2019-04-03', '13:13:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_pedidos`
--

CREATE TABLE `pedido_pedidos` (
  `pedid_codig` int(11) NOT NULL,
  `pedid_numer` varchar(50) NOT NULL DEFAULT '0',
  `pedid_total` int(11) NOT NULL DEFAULT '0',
  `pedid_monto` double NOT NULL,
  `pedid_cmode` int(11) NOT NULL DEFAULT '0',
  `pedid_ccuer` int(11) NOT NULL,
  `pedid_cbols` int(11) NOT NULL,
  `pedid_cmang` int(11) NOT NULL,
  `pedid_cegor` int(11) NOT NULL,
  `pedid_cigor` int(11) NOT NULL,
  `pedid_cppre` int(11) NOT NULL,
  `pedid_cpprl` int(11) NOT NULL,
  `pedid_cierr` int(11) NOT NULL,
  `pedid_ccier` int(11) NOT NULL,
  `pedid_broch` int(11) NOT NULL,
  `pedid_ccurs` int(11) NOT NULL,
  `pedid_cnper` int(11) NOT NULL,
  `pedid_caesp` int(11) NOT NULL,
  `pedid_celec` int(11) NOT NULL,
  `pedid_cfras` int(11) NOT NULL,
  `pedid_cvivo` int(11) NOT NULL,
  `pedid_otros` text NOT NULL,
  `pedid_pposi` int(11) NOT NULL,
  `pedid_pibde` int(11) NOT NULL,
  `pedid_ptbde` text NOT NULL,
  `pedid_ptide` int(11) NOT NULL,
  `pedid_pibiz` int(11) NOT NULL,
  `pedid_ptbiz` text NOT NULL,
  `pedid_ptiiz` int(11) NOT NULL,
  `pedid_ptbor` text NOT NULL,
  `pedid_pfuen` int(11) NOT NULL,
  `pedid_pcolo` int(11) NOT NULL,
  `pedid_plima` int(11) NOT NULL,
  `pedid_prima` text NOT NULL,
  `pedid_pobse` text NOT NULL,
  `pedid_gposi` int(11) NOT NULL,
  `pedid_gibde` int(11) NOT NULL,
  `pedid_gtbde` text NOT NULL,
  `pedid_gtide` int(11) NOT NULL,
  `pedid_gibiz` int(11) NOT NULL,
  `pedid_gtbiz` text NOT NULL,
  `pedid_gtiiz` int(11) NOT NULL,
  `pedid_gorie` int(11) NOT NULL,
  `pedid_gtbor` text NOT NULL,
  `pedid_gfuen` int(11) NOT NULL,
  `pedid_gcolo` int(11) NOT NULL,
  `pedid_glima` int(11) NOT NULL,
  `pedid_grima` text NOT NULL,
  `pedid_gobse` text NOT NULL,
  `pedid_mposi` int(11) NOT NULL,
  `pedid_mfuen` int(11) NOT NULL,
  `pedid_mcolo` int(11) NOT NULL,
  `pedid_limag` int(11) NOT NULL,
  `pedid_mibde` int(11) NOT NULL,
  `pedid_mtbde` text NOT NULL,
  `pedid_mtide` int(11) NOT NULL,
  `pedid_mibiz` int(11) NOT NULL,
  `pedid_mtbiz` text NOT NULL,
  `pedid_mtiiz` int(11) NOT NULL,
  `pedid_mtbor` text NOT NULL,
  `pedid_mobse` text NOT NULL,
  `pedid_eatbo` text NOT NULL,
  `pedid_eafue` int(11) NOT NULL,
  `pedid_eacol` int(11) NOT NULL,
  `pedid_ebtbo` text NOT NULL,
  `pedid_ebfue` int(11) NOT NULL,
  `pedid_ebcol` int(11) NOT NULL,
  `pedid_aefue` int(11) NOT NULL,
  `pedid_aecol` int(11) NOT NULL,
  `pedid_eobse` text NOT NULL,
  `pedid_mrima` text NOT NULL,
  `pedid_iprut` text NOT NULL,
  `pedid_rifro` text NOT NULL,
  `pedid_riesp` text NOT NULL,
  `pedid_obser` text NOT NULL,
  `pedid_pasos` int(11) NOT NULL,
  `epedi_codig` int(11) DEFAULT '0',
  `pedid_fentr` date NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `pedid_fcrea` date NOT NULL,
  `pedid_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='articulos de ropa';

--
-- Volcado de datos para la tabla `pedido_pedidos`
--

INSERT INTO `pedido_pedidos` (`pedid_codig`, `pedid_numer`, `pedid_total`, `pedid_monto`, `pedid_cmode`, `pedid_ccuer`, `pedid_cbols`, `pedid_cmang`, `pedid_cegor`, `pedid_cigor`, `pedid_cppre`, `pedid_cpprl`, `pedid_cierr`, `pedid_ccier`, `pedid_broch`, `pedid_ccurs`, `pedid_cnper`, `pedid_caesp`, `pedid_celec`, `pedid_cfras`, `pedid_cvivo`, `pedid_otros`, `pedid_pposi`, `pedid_pibde`, `pedid_ptbde`, `pedid_ptide`, `pedid_pibiz`, `pedid_ptbiz`, `pedid_ptiiz`, `pedid_ptbor`, `pedid_pfuen`, `pedid_pcolo`, `pedid_plima`, `pedid_prima`, `pedid_pobse`, `pedid_gposi`, `pedid_gibde`, `pedid_gtbde`, `pedid_gtide`, `pedid_gibiz`, `pedid_gtbiz`, `pedid_gtiiz`, `pedid_gorie`, `pedid_gtbor`, `pedid_gfuen`, `pedid_gcolo`, `pedid_glima`, `pedid_grima`, `pedid_gobse`, `pedid_mposi`, `pedid_mfuen`, `pedid_mcolo`, `pedid_limag`, `pedid_mibde`, `pedid_mtbde`, `pedid_mtide`, `pedid_mibiz`, `pedid_mtbiz`, `pedid_mtiiz`, `pedid_mtbor`, `pedid_mobse`, `pedid_eatbo`, `pedid_eafue`, `pedid_eacol`, `pedid_ebtbo`, `pedid_ebfue`, `pedid_ebcol`, `pedid_aefue`, `pedid_aecol`, `pedid_eobse`, `pedid_mrima`, `pedid_iprut`, `pedid_rifro`, `pedid_riesp`, `pedid_obser`, `pedid_pasos`, `epedi_codig`, `pedid_fentr`, `usuar_codig`, `pedid_fcrea`, `pedid_hcrea`) VALUES
(39, '201901-00039', 32, 940000, 2, 9, 32, 42, 9, 32, 9, 59, 1, 32, 0, 0, 0, 0, 0, 0, 0, '', 2, 0, '', 0, 3, 'apodo personalizado', 1, '', 3, 25, 0, '', 'CASI TODOS LLEVAN EMOJI, Y UNA PEROSONA LLEVA ADEMAS SIMBOLO SIMPLE', 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, '', '', 1, 1, 32, 0, 1, '', 5, 0, '', 0, '', 'MISMO COLOR QUE LA FOTO', 'Generación 2019', 4, 25, 'Sentencia Cumplida, 12 años y 1 día. ', 4, 25, 3, 25, 'COPIAR EL COLEGIO MAE EN FUENTES Y COLORES DE TELA Y MODELO', '', 'files/detalle/pedido//39/20190205_102431_iprut.png', 'files/detalle/pedido//39/20190205_102431_rifro.jpg', 'files/detalle/pedido//39/20190205_102431_riesp.png', '', 3, 7, '0000-00-00', 23, '2019-01-28', '15:21:13'),
(47, '201902-00047', 50, 1828000, 1, 35, 35, 35, 35, 35, 35, 41, 1, 41, 0, 0, 0, 0, 0, 0, 0, '', 2, 0, '', 0, 3, 'Apodo Personalizado', 1, '', 17, 25, 0, '', 'EN EL LADO IZQUIERDO LLEVA APODO CON EMOJI QUE ES PERSONALIZADO ', 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, '', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, '', 'LLEVAN LINEAS SOLO EN LA PRETINA Y NO EN LOS PUÑOS, ADEMAS LLEVAN 2 FRANJAS BLANCAS A LO LARGO DE LA MAGA ', '4to Bizarro', 18, 25, '“En cuarto no hay nada que perder por eso viajamos sin ver”', 20, 25, 19, 25, '', '', 'files/detalle/pedido//47/20190221_130239_iprut.png', 'files/detalle/pedido//47/20190221_130239_rifro.png', 'files/detalle/pedido//47/20190221_130239_riesp.png', '', 3, 5, '2019-02-15', 38, '2019-02-05', '10:55:46'),
(48, '201902-00048', 13, 292500, 1, 9, 9, 9, 9, 28, 9, 58, 1, 0, 0, 0, 0, 0, 0, 0, 0, '', 3, 2, 'nombre personalizado', 0, 2, '4 con corona y química industrial', 0, '', 9, 25, 0, '', '', 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, '', '', 1, 9, 25, 0, 2, 'Generación 2019', 0, 0, '', 0, '', '', '', 0, 0, '\"Quisieron enterrarnos, pero no sabían que eramos semillas\"', 13, 25, 9, 25, '', '', 'files/detalle/pedido//48/20190222_113811_iprut.png', 'files/detalle/pedido//48/20190222_113811_rifro.jpg', 'files/detalle/pedido//48/20190222_113811_riesp.jpg', '', 3, 7, '0000-00-00', 37, '2019-02-05', '20:00:45'),
(56, '201902-00056', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, '', 0, 0, '', 0, '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, '', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, '', '', '', 0, 0, '', 0, 0, 0, 0, '', '', '', '', '', '', 0, 1, '0000-00-00', 28, '2019-02-08', '16:35:01'),
(58, '201902-00058', 31, 858500, 3, 20, 20, 20, 60, 60, 20, 60, 1, 20, 4, 0, 0, 0, 0, 0, 0, '', 2, 0, '', 0, 2, 'apodo ', 0, '', 8, 60, 0, '', '', 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, '', '', 1, 8, 60, 0, 2, 'IVªA', 0, 0, '', 0, '', '', 'Generación 2019', 8, 60, 'After si, casa no #206', 8, 60, 8, 60, '', '', 'files/detalle/pedido//58/20190208_201230_iprut.jpeg', 'files/detalle/pedido//58/20190208_201230_rifro.jpeg', 'files/detalle/pedido//58/20190208_201230_riesp.jpeg', '', 3, 2, '0000-00-00', 27, '2019-02-08', '19:06:15'),
(61, '201902-00061', 14, 0, 1, 20, 20, 20, 20, 60, 20, 60, 1, 20, 4, 0, 0, 0, 0, 0, 0, '', 2, 0, '', 0, 2, 'apodo', 0, '', 8, 60, 0, '', '', 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, '', '', 1, 8, 60, 0, 2, 'IVªA', 0, 0, '', 0, '', '', 'Generación 2019', 8, 60, 'After si, casa no #206', 8, 60, 8, 60, '', '', 'files/detalle/pedido//61/20190211_033329_iprut.jpeg', 'files/detalle/pedido//61/20190211_033329_rifro.jpeg', 'files/detalle/pedido//61/20190211_033329_riesp.jpeg', '', 3, 2, '0000-00-00', 27, '2019-02-11', '03:01:26'),
(62, '201902-00062', 3, 0, 1, 20, 20, 20, 20, 60, 20, 60, 1, 20, 0, 0, 0, 0, 0, 0, 0, '', 2, 0, '', 0, 2, 'Apodo', 0, '', 8, 60, 0, '', '', 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, '', '', 1, 8, 60, 0, 2, 'IVªA', 0, 0, '', 0, '', '', 'Generación 2019', 8, 60, 'After si , casa no #206', 8, 60, 8, 60, '', '', 'files/detalle/pedido//62/20190211_034843_iprut.jpeg', 'files/detalle/pedido//62/20190211_034843_rifro.jpeg', 'files/detalle/pedido//62/20190211_034843_riesp.jpeg', '', 3, 2, '0000-00-00', 27, '2019-02-11', '03:38:36'),
(72, '201902-00072', 33, 1015100, 1, 50, 50, 50, 50, 52, 50, 59, 1, 50, 0, 0, 0, 0, 0, 0, 0, '', 2, 0, '', 0, 3, 'Nombres', 5, '', 14, 18, 0, '', 'NUESTROS NOMBRES LO QUEREMOS EN COLOR PLATEADO - Y LA IMAGEN PERSONALIZADA EN COLOR BLANCO.', 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, '', '', 1, 0, 25, 0, 1, '', 5, 0, '', 0, '', '', 'GENERACIÓN 2019', 12, 17, 'NOMBRE DEL PROFESOR CENTRADO EN LA PARTE INFERIOR ', 14, 17, 14, 17, 'BORDADO COLOR BLANCO. NOMBRE DEL PROFESOR CENTRADO EN LA PARTE INFERIOR ', '', 'files/detalle/pedido//72/20190401_130839_iprut.jpg', 'files/detalle/pedido//72/20190401_130839_rifro.jpg', 'files/detalle/pedido//72/20190401_130839_riesp.jpg', '', 3, 7, '2019-04-15', 29, '2019-02-14', '16:15:12'),
(73, '201902-00073', 41, 981000, 2, 20, 20, 20, 20, 17, 20, 20, 1, 20, 0, 0, 0, 0, 0, 0, 0, '', 2, 0, '', 0, 2, 'IV B  y cada nombre respectivo', 0, '', 15, 25, 0, '', '', 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, '', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, '', '', 'Generación ´19', 15, 25, 'qué mejor previa que 4 de media', 15, 25, 15, 25, 'LA T DE TULA EN LA BOTELLA DE BENDER', '', 'files/detalle/pedido//73/20190219_203500_iprut.jpg', 'files/detalle/pedido//73/20190219_203500_rifro.jpeg', 'files/detalle/pedido//73/20190219_203500_riesp.jpeg', '', 3, 1, '0000-00-00', 33, '2019-02-19', '14:51:26'),
(75, '201903-00074', 12, 367000, 2, 20, 26, 20, 20, 26, 26, 26, 1, 26, 0, 0, 0, 0, 0, 0, 0, '', 2, 0, '', 0, 3, 'curso con apodo personalizado y emoji', 1, '', 2, 26, 0, '', 'CURSO ARRIBA DEL APODO', 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, '', '', 3, 2, 26, 0, 2, 'dibujo tecnico', 0, 1, '', 5, '', 'TAL COMO EL OTRO SAN MATEO (B) EN EL FONDO AZUL', 'Generación 2019', 11, 26, '\"La vida es el arte de dibujar sin borrar\"', 11, 26, 11, 26, 'DENTRO DE LA A , ARRIBA DEBE IR EL PROFESOR :   RODRIGO GALGANY,   (LA FUENTE DEL LAS LETRAS COMO EL COLEGIO TOMAS MORO A)', '', 'files/detalle/pedido//75/20190308_101545_iprut.jpeg', 'files/detalle/pedido//75/20190308_101545_rifro.jpeg', 'files/detalle/pedido//75/20190308_101545_riesp.jpeg', '', 3, 3, '2019-05-05', 39, '2019-03-04', '14:35:16'),
(76, '201903-00076', 34, 882000, 2, 20, 20, 18, 20, 18, 18, 58, 1, 20, 1, 0, 0, 0, 0, 0, 0, '', 1, 2, 'IV°HUMANISTA', 0, 0, '', 0, '', 6, 18, 0, '', 'IV°  Y DEBAJO EL HUMANISTA', 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, '', '', 1, 6, 9, 0, 1, '', 5, 0, '', 0, '', 'ES LA INSIGNIA DEL COLEGIO QUE VA EN LA MANGA DERECHA, EN LA IZQUIERDA VA EL APODO DE CADA UNO', '4° ACUENTA', 6, 25, 'No hay nada permanente excepto el cambio', 0, 0, 6, 18, 'SOLO EL DIBUJO', '', 'files/detalle/pedido//76/20190314_180512_iprut.jpeg', 'files/detalle/pedido//76/20190314_180512_rifro.jpg', 'files/detalle/pedido//76/20190314_180512_riesp.jpg', '', 3, 7, '0000-00-00', 40, '2019-03-12', '18:53:26'),
(82, '201903-00077', 30, 858500, 10, 9, 14, 14, 9, 14, 9, 14, 1, 9, 3, 0, 0, 0, 0, 0, 0, '', 3, 2, 'IV°', 0, 2, 'Nombre personalizado', 0, '', 19, 25, 0, '', '', 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, '', '', 2, 0, 0, 0, 0, '', 0, 1, '', 6, '', '', '', 19, 25, 'Tomamos de todo menos atención', 0, 0, 19, 11, '', '', 'files/detalle/pedido//82/20190319_144510_iprut.jpg', '', '', '', 3, 1, '0000-00-00', 47, '2019-03-15', '11:50:55'),
(83, '201903-00083', 0, 0, 0, 20, 20, 20, 62, 20, 20, 58, 1, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, '', 0, 0, '', 0, '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, '', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, '', '', '', 0, 0, '', 0, 0, 0, 0, '', '', '', '', '', '', 2, 1, '0000-00-00', 48, '2019-03-18', '16:11:45'),
(87, '201903-00085', 9, 189000, 1, 21, 21, 21, 21, 20, 21, 21, 1, 0, 0, 0, 0, 0, 0, 0, 0, '', 2, 0, '', 0, 3, 'generacion 2019', 5, '', 2, 20, 0, '', '', 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, '', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, '', '', '', 0, 0, '', 0, 0, 2, 20, 'ESPALDA PERSONALIZADA SOLO EL APODO ESPALDA', '', '', 'files/detalle/pedido//87/20190322_151010_rifro.jpg', 'files/detalle/pedido//87/20190402_182921_riesp.png', '', 3, 3, '2019-05-24', 49, '2019-03-22', '14:15:11'),
(88, '201903-00088', 18, 483000, 2, 20, 20, 62, 20, 62, 20, 58, 1, 20, 0, 0, 0, 0, 0, 0, 12, '', 3, 2, 'Apodo personalizado', 0, 2, 'A mayuscula del curso', 0, '', 14, 25, 0, '', 'MISMA A QUE LA MUESTRA MILITAR FUCSIA (SOBRE EL COLOR DEL RESPECTIVO MILITAR YA SEA FUCSIA O AZULINO)', 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, '', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, '', '', 'Generación 2019', 14, 25, '\"No pensamos ni en clases y vamos a pensar en una frase\"', 14, 25, 14, 25, 'EN LA IV SOLO EL CONTORNO BLANCO,  SIN PINTAR', '', 'files/detalle/pedido//88/20190328_103752_iprut.jpg', 'files/detalle/pedido//88/20190328_103752_rifro.PNG', 'files/detalle/pedido//88/20190328_103752_riesp.jpg', '', 3, 3, '2019-05-28', 50, '2019-03-27', '16:38:45'),
(89, '201903-00089', 16, 572500, 1, 50, 50, 50, 50, 56, 50, 59, 1, 50, 0, 0, 0, 0, 0, 0, 0, '', 3, 2, 'Nombre Personalizado + emoji', 0, 1, '', 0, '', 8, 25, 0, '', '', 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, '', '', 3, 8, 25, 0, 1, '', 5, 1, '', 5, '', 'LA INSIGNIA EN COLOR BLANCO   LINEAS EN MANGA AMARILLA ARRIBA Y VERDE ABAJO', '', 0, 0, 'La mejor forma de predecir el futuro es creándolo', 15, 25, 15, 25, '', '', 'files/detalle/pedido//89/20190331_232026_iprut.png', 'files/detalle/pedido//89/20190331_232026_rifro.png', 'files/detalle/pedido//89/20190331_232026_riesp.png', '', 3, 7, '0000-00-00', 52, '2019-03-31', '21:02:04'),
(90, '201904-00090', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, '', 0, 0, '', 0, '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, '', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, '', '', '', 0, 0, '', 0, 0, 0, 0, '', '', '', '', '', '', 0, 1, '0000-00-00', 53, '2019-04-01', '17:23:01'),
(91, '201904-00091', 17, 509500, 1, 50, 42, 42, 50, 42, 50, 59, 1, 50, 0, 0, 0, 0, 0, 0, 0, '', 2, 0, '', 0, 2, 'personalizado con emoji', 0, '', 11, 25, 0, '', '', 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, '', '', 1, 1, 25, 0, 2, '4°F', 0, 0, '', 0, '', '', 'Administración 2019', 10, 25, '\"No hay mejor previa, que cuatro años de media”', 10, 25, 14, 25, 'LA IMAGEN DE LA ESPALDA ES UNA F RELLENA CON LOS APODOS Y POR LOS LADOS RAMAS DE OLIVO', '', 'files/detalle/pedido//91/20190401_224139_iprut.png', 'files/detalle/pedido//91/20190401_224139_rifro.png', 'files/detalle/pedido//91/20190401_224139_riesp.png', '', 3, 3, '2019-04-30', 57, '2019-04-01', '20:18:30'),
(93, '201904-00093', 36, 966000, 6, 30, 30, 30, 30, 33, 33, 30, 1, 30, 2, 0, 0, 0, 0, 0, 0, '', 2, 0, '', 0, 2, 'Nombres de los alumnos.', 0, '', 5, 33, 0, '', '', 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, '', '', 3, 0, 0, 0, 1, '', 5, 1, '', 6, '', 'LA IMAGEN DEL CURSO QUE VA EN LA MANGA , LO COLOR CORAL DEBE SER NEGRO    (LAS MANGAS DE LOS CANGUROS Y POLO  LLEVAN FRANJAS EN LAS DOS MANGAS, LO AMERICANOS NO LLEVAN, SOLO AMERICANOS CON PUÑOS Y PRETINAS CON LINEAS)', 'IV°Babilónico', 14, 33, '“Con estilo y elegancia,\r\negresa la vagancia.\"', 4, 33, 5, 33, '*EL NOMBRE DEL CURSO CON EL BORDE NEGRO.\r\n*LA IMAGEN PRINCIPAL TENDRÁ LAS FLORES LILA, VERDE AGUA Y AMARILLO PASTEL.', '', 'files/detalle/pedido//93/20190402_114640_iprut.jpeg', 'files/detalle/pedido//93/20190402_225454_rifro.jpeg', 'files/detalle/pedido//93/20190402_230038_riesp.png', '', 3, 1, '0000-00-00', 55, '2019-04-01', '21:46:43'),
(94, '201904-00094', 0, 0, 0, 9, 9, 33, 9, 33, 33, 9, 1, 9, 3, 0, 0, 0, 0, 0, 0, '', 2, 0, '', 0, 2, 'IV°C', 0, '', 6, 25, 0, '', 'NOMBRE ABAJO ', 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, '', '', 1, 0, 0, 0, 1, '', 6, 0, '', 0, '', '', 'IV° CAMPEÓN', 18, 25, '¨Ni una frase representa estos 4 años juntos¨', 6, 0, 6, 25, '', '', 'files/detalle/pedido//94/20190401_230709_iprut.jpg', '', '', '', 3, 1, '0000-00-00', 54, '2019-04-01', '21:57:03'),
(96, '201904-00095', 16, 336000, 1, 14, 14, 14, 14, 20, 14, 14, 1, 0, 0, 0, 0, 0, 0, 0, 0, '', 2, 0, '', 0, 3, 'Generacion 2019', 5, '', 2, 20, 0, '', 'LA LETRA A CON LA CORONA Y LA GENERACION, TAL COMO EL OTRO PEDIDO ROSADO', 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, '', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, '', '', '', 2, 20, '', 0, 0, 2, 20, '', '', 'files/detalle/pedido//96/20190402_202309_iprut.png', '', 'files/detalle/pedido//96/20190402_202309_riesp.png', '', 3, 3, '2019-05-24', 49, '2019-04-01', '22:28:34'),
(98, '201904-00097', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, '', 0, 0, '', 0, '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, '', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, '', '', '', 0, 0, '', 0, 0, 0, 0, '', '', '', '', '', '', 0, 1, '0000-00-00', 56, '2019-04-02', '23:30:35'),
(99, '201904-00099', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, '', 0, 0, '', 0, '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, '', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, '', '', '', 0, 0, '', 0, 0, 0, 0, '', '', '', '', '', '', 0, 1, '0000-00-00', 3, '2019-04-20', '17:55:13'),
(100, '201906-00100', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, '', 0, 0, '', 0, '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, '', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, '', '', '', 0, 0, '', 0, 0, 0, 0, '', '', '', '', '', '', 0, 1, '0000-00-00', 3, '2019-06-12', '14:15:31'),
(101, '201906-00101', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, '', 0, 0, '', 0, '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, '', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, '', '', '', 0, 0, '', 0, 0, 0, 0, '', '', '', '', '', '', 0, 1, '0000-00-00', 3, '2019-06-21', '17:13:36'),
(102, '201906-00102', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, '', 0, 0, '', 0, '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', 0, 0, '', 0, 0, 0, '', '', 0, 0, 0, 0, 0, '', 0, 0, '', 0, '', '', '', 0, 0, '', 0, 0, 0, 0, '', '', '', '', '', '', 0, 1, '0000-00-00', 3, '2019-06-24', '20:35:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_pedidos_adicional`
--

CREATE TABLE `pedido_pedidos_adicional` (
  `adici_codig` int(11) NOT NULL,
  `pedid_codig` int(11) NOT NULL,
  `costo_codig` int(11) NOT NULL,
  `adici_monto` double NOT NULL,
  `adici_obser` text NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `adici_fcrea` date NOT NULL,
  `adici_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pedido_pedidos_adicional`
--

INSERT INTO `pedido_pedidos_adicional` (`adici_codig`, `pedid_codig`, `costo_codig`, `adici_monto`, `adici_obser`, `usuar_codig`, `adici_fcrea`, `adici_hcrea`) VALUES
(6, 47, 4, 175000, 'LLEVAN 2 FRANJAS BLANCAS A LO LARGO DE LAS MANGAS', 25, '2019-02-21', '13:04:46'),
(7, 48, 7, 13000, '', 25, '2019-02-22', '11:41:51'),
(11, 75, 9, 22000, '2000 C/U DE LOS POLERONES ', 22, '2019-03-11', '13:07:20'),
(12, 75, 8, 11000, '1000 C/U DE LOS POLERONES ', 22, '2019-03-11', '13:08:24'),
(13, 76, 8, 34000, 'INSIGNIA COLEGIO CON COSTO ADICIONAL DE $1.000 POR POLERON', 25, '2019-03-14', '21:53:07'),
(16, 89, 4, 40000, 'PRECIO ADICIONAL POR PERSONA +2500', 25, '2019-03-31', '23:47:14'),
(17, 89, 8, 24000, 'PRECIO ADICIONAL POR PERSONA +1500', 25, '2019-03-31', '23:47:45'),
(18, 72, 8, 66000, 'ADICIONAL OVNI PECHO 2000 POR PERSONA', 25, '2019-04-01', '00:07:06'),
(20, 72, 10, 5100, 'COSTO DE ENVÍO POR REGRESO DE TALLAS DE PRUEBA A STGO CORREOS CHILE', 25, '2019-04-01', '00:36:04'),
(21, 75, 7, 24000, 'EXTRA POR POLERON 2000', 25, '2019-04-02', '17:13:02'),
(22, 72, 10, 10000, 'CAMBIO DE IMAGEN PRINCIPAL ESPALDA', 25, '2019-04-02', '22:11:33'),
(23, 93, 4, 54000, '2.000 FRANJAS MANGAS PARA POLO Y CANGURO X 27', 25, '2019-04-02', '23:07:07'),
(24, 93, 10, 3000, 'VIVO BOLSILLO $500 X6.\r\n CATALINA BRAVO \r\nAMANDA GODOY\r\nJONÁS SANTANDER\r\nDAMIÁN SOTO\r\nFRANCISCA VALDIVIA \r\nCATALINA YÁÑEZ', 25, '2019-04-02', '23:43:41');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_pedidos_deduccion`
--

CREATE TABLE `pedido_pedidos_deduccion` (
  `pdedu_codig` int(11) NOT NULL,
  `pedid_codig` int(11) NOT NULL,
  `deduc_codig` int(11) NOT NULL,
  `pdedu_monto` double NOT NULL,
  `pdedu_obser` text COLLATE utf8mb4_bin NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `pdedu_fcrea` date NOT NULL,
  `pdedu_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Volcado de datos para la tabla `pedido_pedidos_deduccion`
--

INSERT INTO `pedido_pedidos_deduccion` (`pdedu_codig`, `pedid_codig`, `deduc_codig`, `pdedu_monto`, `pdedu_obser`, `usuar_codig`, `pdedu_fcrea`, `pdedu_hcrea`) VALUES
(5, 47, 1, 25000, 'DESCUENTO PORQUE NO LLEVAN LINEAS LOS PUÑOS', 25, '2019-02-21', '14:32:48');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_pedidos_detalle`
--

CREATE TABLE `pedido_pedidos_detalle` (
  `pdeta_codig` int(11) NOT NULL,
  `pedid_codig` int(11) NOT NULL,
  `tmode_codig` int(11) DEFAULT NULL,
  `model_codig` int(11) NOT NULL,
  `mcate_codig` int(11) DEFAULT NULL,
  `mvari_codig` int(11) DEFAULT NULL,
  `mcolo_codig` int(11) DEFAULT NULL,
  `pdeta_canti` int(11) DEFAULT NULL,
  `pdeta_opci` int(11) DEFAULT NULL,
  `mopci_codig` int(11) DEFAULT NULL,
  `mopci_codi2` int(11) NOT NULL,
  `pdeta_dopci` int(11) DEFAULT NULL,
  `pdeta_copci` int(11) DEFAULT NULL,
  `pdeta_mensa` text,
  `pdeta_monto` int(11) DEFAULT NULL,
  `pdeta_npfue` int(11) NOT NULL,
  `pdeta_npcol` int(11) NOT NULL,
  `pdeta_nppos` int(11) NOT NULL,
  `pdeta_aefue` int(11) NOT NULL,
  `pdeta_aecol` int(11) NOT NULL,
  `pdeta_aepos` int(11) NOT NULL,
  `pdeta_pposi` int(11) NOT NULL,
  `pdeta_ptder` text NOT NULL,
  `pdeta_ptizq` varchar(50) NOT NULL,
  `pdeta_pfuen` int(11) NOT NULL,
  `pdeta_pcolo` int(11) NOT NULL,
  `pdeta_pobse` text NOT NULL,
  `pdeta_gposi` int(11) NOT NULL,
  `pdeta_gfuen` int(11) NOT NULL,
  `pdeta_gcolo` int(11) NOT NULL,
  `pdeta_gobse` text NOT NULL,
  `pdeta_mposi` int(11) NOT NULL,
  `pdeta_mtder` varchar(50) NOT NULL,
  `pdeta_mtizq` varchar(50) NOT NULL,
  `pdeta_mfuen` int(11) NOT NULL,
  `pdeta_mcolo` int(11) NOT NULL,
  `pdeta_limag` int(11) NOT NULL,
  `pdeta_mobse` text NOT NULL,
  `pdeta_eatbo` varchar(50) NOT NULL,
  `pdeta_eafue` int(11) NOT NULL,
  `pdeta_eacol` int(11) NOT NULL,
  `pdeta_ebtbo` varchar(50) NOT NULL,
  `pdeta_ebfue` int(11) NOT NULL,
  `pdeta_ebcol` int(11) NOT NULL,
  `pdeta_aetbo` varchar(50) NOT NULL,
  `pdeta_eobse` text NOT NULL,
  `pdeta_mrima` text NOT NULL,
  `pdeta_iprut` text NOT NULL,
  `pdeta_rifro` text NOT NULL,
  `pdeta_riesp` text NOT NULL,
  `pdeta_pasos` int(11) DEFAULT NULL,
  `usuar_codig` int(11) DEFAULT NULL,
  `pdeta_fcrea` date DEFAULT NULL,
  `pdeta_hcrea` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pedido_pedidos_detalle`
--

INSERT INTO `pedido_pedidos_detalle` (`pdeta_codig`, `pedid_codig`, `tmode_codig`, `model_codig`, `mcate_codig`, `mvari_codig`, `mcolo_codig`, `pdeta_canti`, `pdeta_opci`, `mopci_codig`, `mopci_codi2`, `pdeta_dopci`, `pdeta_copci`, `pdeta_mensa`, `pdeta_monto`, `pdeta_npfue`, `pdeta_npcol`, `pdeta_nppos`, `pdeta_aefue`, `pdeta_aecol`, `pdeta_aepos`, `pdeta_pposi`, `pdeta_ptder`, `pdeta_ptizq`, `pdeta_pfuen`, `pdeta_pcolo`, `pdeta_pobse`, `pdeta_gposi`, `pdeta_gfuen`, `pdeta_gcolo`, `pdeta_gobse`, `pdeta_mposi`, `pdeta_mtder`, `pdeta_mtizq`, `pdeta_mfuen`, `pdeta_mcolo`, `pdeta_limag`, `pdeta_mobse`, `pdeta_eatbo`, `pdeta_eafue`, `pdeta_eacol`, `pdeta_ebtbo`, `pdeta_ebfue`, `pdeta_ebcol`, `pdeta_aetbo`, `pdeta_eobse`, `pdeta_mrima`, `pdeta_iprut`, `pdeta_rifro`, `pdeta_riesp`, `pdeta_pasos`, `usuar_codig`, `pdeta_fcrea`, `pdeta_hcrea`) VALUES
(45, 39, 1, 3, 9, 1, 0, 9, 1, 6, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 23, '2019-01-28', '15:30:46'),
(46, 39, 2, 3, 9, 1, 0, 23, 1, 6, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 23, '2019-01-28', '16:33:26'),
(48, 47, 2, 3, 9, 1, 0, 50, 1, 1, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 22, '2019-02-05', '10:58:50'),
(49, 48, 1, 3, 10, 1, 0, 13, 1, 3, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 25, '2019-02-05', '20:06:40'),
(54, 56, 1, 5, 5, 1, 0, 4, 1, 5, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 28, '2019-02-08', '16:37:57'),
(56, 58, 1, 5, 5, 1, 0, 14, 1, 5, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 27, '2019-02-08', '19:18:58'),
(59, 61, 1, 5, 5, 1, 0, 14, 1, 2, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 27, '2019-02-11', '03:02:42'),
(60, 62, 1, 3, 9, 1, 0, 3, 1, 6, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 27, '2019-02-11', '03:40:11'),
(61, 56, 1, 5, 5, 1, 0, 1, 1, 4, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 28, '2019-02-14', '11:37:11'),
(62, 56, 1, 5, 6, 1, 0, 1, 1, 4, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 28, '2019-02-14', '11:39:40'),
(63, 56, 1, 5, 5, 1, 0, 10, 1, 2, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 28, '2019-02-14', '11:42:27'),
(64, 72, 2, 3, 9, 1, 0, 33, 1, 6, 0, 0, 0, 'CORTAVIENTO COMPLETAMENTE NEGRO, TANTO POR DENTRO COMO POR FUERA. \r\nIV EN EL BRAZO COLOR BLANCO\r\nIMAGEN DE REFERANCIA ATRÁS, ASTRONAUTA BLANCO. Y NUESTROS NOMBRES PLATEADOS.\r\nESTAMPADO EXTRA ADELANTE, IMAGEN DE REFERENCIA, NAVE ESPACIAL BLANCA QUE HACE ILUSIÓN DE QUE ESTÁ ABDUCIENDO NUESTRO NOMBRE. NOMBRE ADELANTE (BAJO LA NAVE) COLOR PLATEADO \r\nTEXTO: GENERACIÓN 2019 EN LA ESPALDA, SOBRE EL DIBUJO Y NUESTROS NOMBRES.', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 29, '2019-02-14', '16:19:49'),
(65, 56, 1, 3, 10, 1, 0, 1, 1, 1, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 28, '2019-02-15', '11:58:57'),
(66, 56, 1, 5, 6, 1, 0, 1, 1, 2, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 28, '2019-02-15', '12:02:56'),
(67, 56, 2, 3, 9, 1, 0, 2, 1, 1, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 28, '2019-02-15', '12:11:13'),
(74, 56, 2, 5, 5, 1, 0, 2, 1, 5, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 28, '2019-02-15', '12:23:36'),
(75, 56, 2, 5, 5, 1, 0, 2, 1, 2, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 28, '2019-02-15', '12:29:09'),
(76, 73, 1, 3, 11, 1, 0, 30, 1, 6, 0, 0, 0, 'COLOR NEGRO E INTERIOR GORRO A2', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 33, '2019-02-19', '14:55:17'),
(77, 73, 1, 3, 10, 1, 0, 11, 1, 6, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 33, '2019-02-19', '19:02:39'),
(78, 58, 1, 5, 5, 1, 0, 14, 1, 2, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 22, '2019-02-21', '08:48:01'),
(79, 58, 1, 3, 9, 1, 0, 3, 1, 6, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 22, '2019-02-21', '09:06:33'),
(80, 75, 2, 3, 9, 1, 0, 8, 1, 6, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 25, '2019-03-04', '14:37:35'),
(81, 75, 1, 3, 9, 1, 0, 4, 1, 6, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 25, '2019-03-04', '15:10:30'),
(82, 76, 1, 3, 9, 1, 0, 21, 1, 6, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 40, '2019-03-12', '18:56:27'),
(84, 76, 1, 5, 5, 1, 0, 13, 1, 4, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 40, '2019-03-13', '18:30:24'),
(87, 82, 1, 5, 5, 1, 0, 3, 1, 2, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 47, '2019-03-15', '12:07:18'),
(88, 82, 1, 5, 5, 1, 0, 9, 1, 5, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 47, '2019-03-18', '11:07:58'),
(89, 82, 2, 5, 5, 1, 0, 3, 1, 2, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 47, '2019-03-18', '11:49:12'),
(90, 82, 2, 5, 5, 1, 0, 3, 1, 5, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 47, '2019-03-18', '12:20:14'),
(91, 82, 2, 3, 9, 1, 0, 3, 1, 6, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 47, '2019-03-18', '12:29:37'),
(92, 82, 2, 5, 5, 1, 0, 2, 1, 4, 0, 0, 0, 'SIN CIERRE', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 47, '2019-03-18', '12:46:15'),
(100, 83, 1, 3, 10, 1, 0, 16, 1, 8, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 48, '2019-03-18', '16:19:27'),
(101, 82, 1, 3, 9, 1, 0, 4, 1, 6, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 47, '2019-03-19', '09:46:30'),
(102, 82, 1, 3, 10, 1, 0, 1, 1, 6, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 47, '2019-03-19', '09:50:53'),
(105, 82, 2, 5, 6, 1, 0, 1, 1, 2, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 47, '2019-03-19', '09:54:19'),
(116, 82, 1, 1, 1, 1, 0, 1, 1, 6, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 47, '2019-03-19', '10:07:43'),
(120, 87, 1, 3, 10, 1, 0, 9, 1, 6, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 49, '2019-03-22', '14:21:09'),
(171, 88, 1, 3, 9, 1, 0, 14, 1, 8, 3, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 50, '2019-03-27', '17:06:58'),
(172, 88, 1, 3, 10, 1, 0, 4, 1, 8, 3, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 50, '2019-03-27', '19:40:53'),
(173, 89, 2, 3, 9, 1, 0, 20, 1, 6, 6, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 25, '2019-03-31', '21:06:22'),
(174, 90, 1, 3, 10, 1, 0, 23, 1, 6, 6, 0, 0, 'FORRO GORRO  Y CORDON COLOR BURDEO', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 53, '2019-04-01', '17:49:37'),
(175, 91, 2, 3, 9, 1, 0, 17, 1, 6, 6, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 25, '2019-04-01', '20:25:30'),
(176, 93, 1, 3, 10, 1, 0, 16, 1, 6, 6, 0, 0, 'TODOS CON LÍNEAS EN LOS BÍCEPS', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 55, '2019-04-01', '21:52:56'),
(177, 94, 1, 5, 5, 1, 0, 12, 1, 2, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 54, '2019-04-01', '21:59:18'),
(178, 96, 1, 3, 10, 1, 0, 16, 1, 6, 6, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 49, '2019-04-01', '22:31:29'),
(179, 94, 1, 5, 6, 1, 0, 2, 1, 2, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 54, '2019-04-01', '23:12:14'),
(180, 94, 1, 5, 5, 1, 0, 2, 1, 5, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 54, '2019-04-01', '23:21:48'),
(181, 94, 1, 5, 5, 1, 0, 1, 1, 4, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 54, '2019-04-01', '23:25:19'),
(182, 94, 2, 5, 5, 1, 0, 10, 1, 5, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 54, '2019-04-01', '23:27:58'),
(185, 94, 2, 5, 6, 1, 0, 4, 1, 5, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 54, '2019-04-01', '23:44:25'),
(189, 94, 2, 5, 5, 1, 0, 3, 1, 2, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 54, '2019-04-01', '23:52:58'),
(190, 94, 2, 5, 6, 1, 0, 2, 1, 2, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 54, '2019-04-01', '23:55:10'),
(191, 93, 1, 1, 1, 1, 0, 2, 1, 6, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 55, '2019-04-02', '08:48:59'),
(192, 93, 1, 3, 9, 1, 0, 9, 1, 6, 6, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 55, '2019-04-02', '08:56:50'),
(193, 93, 1, 5, 6, 1, 0, 6, 1, 4, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 55, '2019-04-02', '10:41:25'),
(194, 93, 1, 5, 5, 1, 0, 1, 1, 2, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 55, '2019-04-02', '11:01:53'),
(195, 93, 1, 5, 6, 1, 0, 2, 1, 2, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 55, '2019-04-02', '11:03:57'),
(196, 98, 1, 5, 5, 1, 0, 14, 1, 2, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 56, '2019-04-02', '23:31:25'),
(197, 98, 2, 5, 5, 1, 0, 14, 1, 2, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, '', 0, 0, 0, '', 0, '', '', 0, 0, 0, '', '', 0, 0, '', 0, 0, '', '', '', '', '', '', 2, 56, '2019-04-02', '23:38:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_pedidos_detalle_listado`
--

CREATE TABLE `pedido_pedidos_detalle_listado` (
  `pdlis_codig` int(11) NOT NULL,
  `pedid_codig` int(11) NOT NULL,
  `pdeta_codig` int(11) NOT NULL,
  `pdlis_ndocu` int(11) NOT NULL,
  `pdlis_nombr` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `talla_codig` int(11) NOT NULL,
  `pdlis_tajus` text COLLATE utf8mb4_bin NOT NULL,
  `pdlis_npers` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `pdlis_aespa` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `pdlis_elect` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `pdlis_timag` int(11) NOT NULL,
  `emoji_codig` int(11) NOT NULL,
  `pdlis_rimag` text COLLATE utf8mb4_bin NOT NULL,
  `pdlis_obser` text COLLATE utf8mb4_bin NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `pdlis_fcrea` date NOT NULL,
  `pdlis_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Volcado de datos para la tabla `pedido_pedidos_detalle_listado`
--

INSERT INTO `pedido_pedidos_detalle_listado` (`pdlis_codig`, `pedid_codig`, `pdeta_codig`, `pdlis_ndocu`, `pdlis_nombr`, `talla_codig`, `pdlis_tajus`, `pdlis_npers`, `pdlis_aespa`, `pdlis_elect`, `pdlis_timag`, `emoji_codig`, `pdlis_rimag`, `pdlis_obser`, `usuar_codig`, `pdlis_fcrea`, `pdlis_hcrea`) VALUES
(65, 39, 45, 20812116, 'TANIA FERNANDA BADILLA SANTANDER', 3, 'null', 'Tanyaa', 'Tania.B', '', 0, 0, '', '', 23, '2019-01-28', '15:35:59'),
(66, 39, 45, 21003178, 'LUIIS MATIAS CÉSPEDES', 4, 'null', 'Lucho', 'Luis Matías.C', '', 0, 0, '', '', 23, '2019-01-28', '15:40:35'),
(67, 39, 45, 20670141, 'VALENTINA BETSABET CURIN SALAZAR', 1, 'null', ' Valilu', 'Valentina.C', '', 0, 0, '', '', 23, '2019-01-28', '15:42:31'),
(68, 39, 45, 20447987, 'FABIAN FRANCISCO IRRIBARRA CAYUL', 4, 'null', 'Fabián', 'Fabián.I', '', 0, 0, '', '', 23, '2019-01-28', '15:44:39'),
(69, 39, 45, 20811881, 'OSCAR JAVIER LAGOS TILLERIA', 9, 'null', 'Oscar', 'Oscar.L', '', 0, 0, '', '', 23, '2019-01-28', '15:52:40'),
(70, 39, 45, 20789487, 'MILLARAY LASCANO LARA', 3, 'null', 'Milla', 'Millaray.L', '', 0, 0, '', '', 23, '2019-01-28', '15:55:17'),
(71, 39, 45, 20746321, 'BENJAMÍN IGNACIO MALDONADO MORALES', 4, 'null', 'Maldonado', 'Benjamín.M', '', 0, 0, '', '', 23, '2019-01-28', '15:57:09'),
(72, 39, 45, 1234567, 'ANDRES TRINCADO', 9, 'null', 'Profe Andrés', 'Andrés.T', '', 0, 0, '', 'PROFESOR', 23, '2019-01-28', '16:00:30'),
(73, 39, 45, 0, 'JAVIERA ESPINOZA', 3, '[\"3\",\"10\"]', 'Pecosa', 'Javiera.E', '', 0, 0, '', 'SIN GORRO', 23, '2019-01-28', '16:04:24'),
(74, 39, 46, 20961524, 'MATIAS IGNACIO ARAYA VILLAROEL', 4, 'null', 'Aizaya', 'Matías.A', '', 0, 0, '', '', 23, '2019-01-28', '16:35:31'),
(75, 39, 46, 21040356, 'NICOLAS ALEJANDRO BARRIOS JORQUERA', 4, 'null', 'La Promesa', 'Nicolas.B', '', 0, 0, '', '', 23, '2019-01-28', '16:38:45'),
(76, 39, 46, 20450776, 'CAROLINA DE LOS ANGELES CAMPOS RAMIREZ', 3, '[\"10\"]', 'Caritolina', 'Carolina.C', '', 0, 0, '', '', 23, '2019-01-28', '16:42:02'),
(77, 39, 46, 20931339, 'ANGELO JESUS CHAMORRO ZUÑIGA', 4, 'null', 'Ja.ja', 'Angelo.CH', '', 0, 0, '', '', 23, '2019-01-28', '16:46:10'),
(78, 39, 46, 20785021, 'ALEJANDRA KARINA HENRIQUEZ TOBAR', 5, 'null', 'Ale', 'Alejandra.H', '', 0, 0, '', '', 23, '2019-01-28', '16:47:51'),
(79, 39, 46, 20789899, 'VALENTINA PATRICIA HERRERA MESA', 2, 'null', 'VALE', 'Valentina.H', '', 0, 0, '', '', 23, '2019-01-28', '16:49:38'),
(80, 39, 46, 20447411, 'DENISSE ALEJANDRA HUINCA ANCAMIL', 6, 'null', 'D_nisse', 'Denisse.H', '', 0, 0, '', '+BUSTO', 23, '2019-01-28', '16:52:00'),
(81, 39, 46, 20813273, 'JESÚS ISAI LARA CARPIO', 4, 'null', 'LGsus', 'Jesús.L', '', 0, 0, '', '', 23, '2019-01-28', '16:54:18'),
(82, 39, 46, 20813346, 'CONSTANZA MILLARAY LARA HERNANDEZ', 2, '[\"1\"]', 'Kony', 'Constanza.L', '', 0, 0, '', '', 23, '2019-01-28', '16:56:42'),
(83, 39, 46, 20, 'SHIRLEY YESSMIN LIZANA SALINAS', 2, 'null', 'Shirley', 'Shirley.L', '', 0, 0, '', '', 23, '2019-01-28', '16:58:35'),
(84, 39, 46, 20791566, 'CONSTANZA EUGENIA MELITA HENRIQUEZ', 5, 'null', 'La Melita', 'Constanza M.', '', 0, 0, '', '', 23, '2019-01-28', '17:00:59'),
(85, 39, 46, 20801851, 'GERALDINE CONSTANZA NAVARRO GOMEZ', 2, 'null', 'Geral', 'Geraldine.N', '', 0, 0, '', '', 23, '2019-01-28', '17:02:13'),
(86, 39, 46, 20448566, 'CATALINA ARELY NILO BUSTOS', 3, 'null', 'Chini', 'Catalina.N', '', 0, 0, '', '', 23, '2019-01-28', '17:05:17'),
(87, 39, 46, 20448383, 'ANGELO ANDRES OYARCE ORELLANA', 4, 'null', 'Pollito', 'Angelo.O', '', 0, 0, '', 'NO SE PROBO', 23, '2019-01-28', '17:07:04'),
(88, 39, 46, 20746387, 'CAMILA JAVIERA PEREZ CUEVAS', 3, '[\"3\",\"10\"]', 'Camiloca', 'Camila.P', '', 0, 0, '', '', 23, '2019-01-28', '17:08:40'),
(89, 39, 46, 20791564, 'MARIA IGNACIA PEREZ DURAN', 3, '[\"3\",\"9\"]', 'Mary', 'Maria Ignacia.P', '', 0, 0, '', 'AJUSTADO MANGA', 23, '2019-01-28', '17:11:05'),
(90, 39, 46, 20813073, 'DARILSON GERARDO PONCE LAVIN', 6, 'null', 'Ponce', 'Darilson.P', '', 0, 0, '', '', 23, '2019-01-28', '17:12:38'),
(91, 39, 46, 20669673, 'ALVARO ALEXANDER RIFFO ORTIZ', 2, '[\"1\",\"5\"]', 'Alvarito', 'Alvaro.R', '', 0, 0, '', '', 23, '2019-01-28', '17:14:08'),
(92, 39, 46, 20811610, 'GUILLERMO PATRICK SALDAÑA HILARIO', 4, 'null', 'Guilli', 'Guillermo.S', '', 0, 0, '', '', 23, '2019-01-28', '17:16:14'),
(93, 39, 46, 20813155, 'TOMAS CRISTOBAL SEPULVEDA COLIMA', 4, '[\"1\"]', 'Tomayi', 'Tomas.S', '', 0, 0, '', '', 23, '2019-01-28', '17:18:53'),
(94, 39, 46, 20790095, 'CONTANZA SCARLETE TORRES ESPINOZA', 3, 'null', 'Conita', 'C.Torres', '', 0, 0, '', '', 23, '2019-01-28', '17:20:14'),
(95, 39, 46, 20791803, 'MATIAS BYRON TRONCOSO PAILAMILLA', 4, 'null', 'MatiasAbc', 'Matías.T', '', 0, 0, '', '', 23, '2019-01-28', '17:22:04'),
(96, 39, 46, 20790779, 'CONSTANZA FABIOLA VEGA AILLAPE', 2, 'null', 'CONITA', 'CONSTANZA.V', 'NADA', 0, 0, '', '', 23, '2019-01-28', '17:23:39'),
(99, 47, 48, 1, 'Larezka Almarza', 7, 'null', 'sta.lare', 'lare', '', 0, 0, '', '', 22, '2019-02-05', '11:03:54'),
(100, 47, 48, 2, 'Benjamín Álvarez ', 6, '[\"5\"]', 'Benjalemo', 'Benjalemo', '', 0, 0, '', '', 22, '2019-02-05', '11:05:46'),
(101, 47, 48, 3, 'Alexis Arancibia ', 4, 'null', 'Chito', 'Chito', '', 0, 0, '', '', 22, '2019-02-05', '11:07:27'),
(102, 47, 48, 4, 'Antonella Araya', 2, '[\"1\"]', 'Antooo', 'Antooo', '', 0, 0, '', '', 22, '2019-02-05', '11:13:05'),
(103, 47, 48, 5, 'Gerardo Arriola', 3, '[\"1\"]', 'xGGeRa', 'Jerryx', '', 0, 0, '', '', 22, '2019-02-05', '11:26:30'),
(104, 47, 48, 6, 'Loreto Armijo', 2, 'null', 'Lore.aa', 'Lore.aa', '', 0, 0, '', '', 22, '2019-02-05', '13:38:41'),
(105, 47, 48, 7, 'Catalina burgos', 2, '[\"1\"]', 'CataGB', 'Gbcita', '', 0, 0, '', '', 22, '2019-02-05', '13:53:00'),
(106, 47, 48, 8, 'Christina cabañas ', 2, '[\"1\"]', 'Conitô', 'Christi', '', 0, 0, '', '', 22, '2019-02-05', '13:57:36'),
(107, 47, 48, 9, 'José miguel castro', 6, '[\"5\"]', 'joc', 'Joc', '', 0, 0, '', '', 22, '2019-02-05', '14:01:16'),
(108, 47, 48, 10, 'Fernanda catrilao', 2, '[\"1\"]', 'Toro', 'Fernanda', '', 0, 0, '', '', 22, '2019-02-05', '14:03:45'),
(109, 47, 48, 11, 'Joaquín Cerna', 7, 'null', 'KakoMC', 'Kako', '', 0, 0, '', '', 22, '2019-02-05', '14:06:01'),
(110, 47, 48, 12, 'Milenko Denzer', 7, 'null', 'Milenkod', 'Milenkod', '', 0, 0, '', '', 22, '2019-02-05', '14:07:29'),
(111, 47, 48, 13, 'Lucas Faundez', 7, 'null', 'Kuka', 'Kuk-ass', '', 0, 0, '', '', 22, '2019-02-05', '14:10:15'),
(112, 47, 48, 14, 'Nicolás fuentes', 4, '[\"5\"]', 'Nicomvtivss', 'Nicomvtivss', '', 0, 0, '', '', 22, '2019-02-05', '14:19:12'),
(113, 47, 48, 15, 'Javiera González', 2, '[\"1\"]', 'Necro o o', 'Necro o o', '', 0, 0, '', '', 22, '2019-02-05', '14:21:14'),
(114, 47, 48, 16, 'María Fernanda González', 2, '[\"1\"]', 'Toñita', 'Toñita', '', 0, 0, '', '', 22, '2019-02-05', '14:25:52'),
(115, 47, 48, 17, 'Alejandra González ', 3, '[\"3\"]', 'Alechan', 'Aletunn', '', 0, 0, '', '', 22, '2019-02-05', '14:28:52'),
(116, 47, 48, 18, 'Pilar González', 2, 'null', 'Pilili', 'Pilili', '', 0, 0, '', '', 22, '2019-02-05', '14:30:00'),
(117, 47, 48, 19, 'Valentina González ', 2, '[\"1\"]', 'Klein', 'Kleincita', '', 0, 0, '', '', 22, '2019-02-05', '14:31:36'),
(118, 47, 48, 20, 'Aylin Gutiérrez', 2, 'null', 'Aylingg', 'Ggcita', '', 0, 0, '', '', 22, '2019-02-05', '14:44:57'),
(119, 47, 48, 21, 'Ayelen Hermosilla ', 2, 'null', 'Yeye', 'Yeye', '', 0, 0, '', '', 22, '2019-02-05', '14:46:41'),
(120, 47, 48, 22, 'Almendra Meneses', 3, 'null', 'Almendrix', 'Almendrix', '', 0, 0, '', '', 22, '2019-02-05', '14:50:49'),
(121, 47, 48, 23, 'Sofía mira ', 3, '[\"9\"]', 'Sofiii', 'Sofiii', '', 0, 0, '', '', 22, '2019-02-05', '14:52:09'),
(122, 47, 48, 24, 'Fernanda Mérida', 2, '[\"1\"]', 'Feñaa', 'Fedña', '', 0, 0, '', '', 22, '2019-02-05', '14:54:24'),
(123, 47, 48, 25, 'Camila morales', 2, '[\"1\"]', 'Cami', 'Cami', '', 0, 0, '', '', 22, '2019-02-05', '14:56:12'),
(124, 47, 48, 26, 'Matías mella', 6, '[\"1\"]', 'Mati', 'Mati', '', 0, 0, '', '', 22, '2019-02-05', '14:57:10'),
(125, 47, 48, 27, 'Diego Ojeda', 9, 'null', 'Pakonuda', 'Checho ', '', 0, 0, '', '', 22, '2019-02-05', '14:58:39'),
(126, 47, 48, 28, 'Luciano paredes', 7, 'null', 'Chupete_san', 'Chupete_san', '', 0, 0, '', '', 22, '2019-02-05', '14:59:45'),
(127, 47, 48, 29, 'Pablo Pérez', 14, 'null', 'Pvblofreestyle', 'Pvblofreestyle', '', 0, 0, '', '', 22, '2019-02-05', '15:21:49'),
(128, 47, 48, 30, 'Felipe Ormeño ', 7, 'null', 'Felipessj', 'Felipessj', '', 0, 0, '', '', 22, '2019-02-05', '17:06:48'),
(129, 47, 48, 31, 'Millaray pueblas', 2, '[\"1\"]', 'Milla_aaaa', 'Millaaaa', '', 0, 0, '', '', 22, '2019-02-05', '17:08:26'),
(130, 47, 48, 32, 'Alonso romo', 6, 'null', 'Wiiz', 'Wizz ', '', 0, 0, '', '', 22, '2019-02-05', '17:09:54'),
(131, 47, 48, 33, 'Gonzalo Rodríguez ', 4, 'null', 'Gonzvlo ', 'Gonzvlo ', '', 0, 0, '', '', 22, '2019-02-05', '17:11:20'),
(132, 47, 48, 34, 'Tania Rodríguez ', 2, '[\"1\"]', 'Taniaa_r', 'Taniaa_r', '', 0, 0, '', '', 22, '2019-02-05', '17:13:10'),
(133, 47, 48, 35, 'Jimena rosales', 2, 'null', 'Jime', 'Jime', '', 0, 0, '', '', 22, '2019-02-05', '17:16:00'),
(134, 48, 49, 0, 'Jeimy Henríquez', 2, '[\"1\"]', 'Jeimy', 'Jeimy', '', 0, 0, '', '', 25, '2019-02-05', '20:08:52'),
(135, 47, 48, 36, 'Valentina tapia', 3, '[\"9\"]', 'Ruli', 'Rulitos', '', 0, 0, '', '', 22, '2019-02-05', '20:12:19'),
(136, 47, 48, 37, 'Valentina tell', 2, '[\"1\"]', 'Negraa', 'Negraa', '', 0, 0, '', '', 22, '2019-02-05', '20:15:37'),
(137, 47, 48, 38, 'Madelaine torrejón', 2, 'null', 'Madeliciosa', 'Madde', '', 0, 0, '', '', 22, '2019-02-05', '20:29:47'),
(138, 47, 48, 39, 'Valentina Valdivia', 2, '[\"1\"]', 'Vaaale', 'Valecita', '', 0, 0, '', '', 22, '2019-02-05', '20:32:52'),
(139, 47, 48, 40, 'Vicente Vargas', 14, '[\"5\"]', 'Vichosaurio', 'Vichosaurio', '', 0, 0, '', '', 22, '2019-02-05', '20:35:11'),
(140, 47, 48, 41, 'Mariana Vázquez', 1, '[\"1\"]', 'Marciciana', 'Marciciana', '', 0, 0, '', '', 22, '2019-02-05', '20:54:19'),
(141, 47, 48, 42, 'Francisca Villavicencio', 3, '[\"9\"]', 'Flan', 'Flan', '', 0, 0, '', '', 22, '2019-02-05', '20:57:39'),
(142, 47, 48, 43, 'Marcelo salvo', 6, 'null', 'Cheelo', 'Cheelo', '', 0, 0, '', '', 22, '2019-02-05', '20:58:53'),
(143, 47, 48, 44, 'Benjamín peña ', 4, 'null', 'Peñaranda', 'Kodigo', '', 0, 0, '', '', 22, '2019-02-05', '21:00:36'),
(144, 47, 48, 45, 'Anaí Zúñiga', 2, '[\"1\"]', 'Ana.i', 'Ana.i', '', 0, 0, '', '', 22, '2019-02-05', '21:03:51'),
(145, 47, 48, 46, 'Marisol urra', 2, '[\"1\"]', 'Profe Marisol ', 'Profe Marisol ', '', 0, 0, '', '', 22, '2019-02-05', '21:06:14'),
(146, 47, 48, 47, 'Elizabeth Muñoz ', 5, 'null', 'Profe Ely ', '', '', 0, 0, '', '', 22, '2019-02-05', '21:09:22'),
(147, 47, 48, 48, 'Fernando Ureta', 14, 'null', 'Profe Ureta', '', '', 0, 0, '', '', 22, '2019-02-05', '21:10:50'),
(148, 47, 48, 49, 'Marianela garay', 5, 'null', 'Profe Nela', '', '', 0, 0, '', '', 22, '2019-02-05', '21:11:56'),
(149, 47, 48, 50, 'Francisco Corvalán ', 14, 'null', 'Profe Pancho', '', '', 0, 0, '', '', 22, '2019-02-05', '21:13:00'),
(150, 48, 49, 1, 'marla mondaca', 5, 'null', 'Marla', 'Marla', '', 0, 0, '', '', 25, '2019-02-06', '17:17:26'),
(151, 48, 49, 3, 'Yanara Mondaca', 5, 'null', 'Yanara', 'Yanara', '', 0, 0, '', '', 25, '2019-02-06', '17:18:29'),
(152, 48, 49, 4, 'Fernanda Delgado', 2, 'null', 'Fernanda', 'Feña', '', 0, 0, '', '', 25, '2019-02-06', '17:19:08'),
(153, 48, 49, 5, 'Camilo Maldonado', 7, 'null', 'Camilo', 'Camilo', '', 0, 0, '', 'INTERIOR DEL GORRO  GRAFITO', 25, '2019-02-06', '17:19:30'),
(154, 48, 49, 6, 'Ignacio Tiznado', 6, 'null', 'Ignacio', 'Ignacio', '', 0, 0, '', 'INTERIOR DEL GORRO GRAFITO', 25, '2019-02-06', '17:20:45'),
(155, 48, 49, 7, 'Constanza Maulin', 2, 'null', 'Constanza', 'Constanza', '', 0, 0, '', '', 25, '2019-02-06', '17:22:24'),
(156, 48, 49, 8, 'Patricia Sepulveda', 1, 'null', 'Patricia', 'Patricia', '', 0, 0, '', '', 25, '2019-02-06', '17:22:57'),
(157, 48, 49, 9, 'Brenda Saavedra', 3, 'null', 'Brenda', 'Brenda', '', 0, 0, '', '', 25, '2019-02-06', '17:23:35'),
(158, 48, 49, 10, 'Francisca Cabrera', 7, 'null', 'Francisca', 'Francisca', '', 0, 0, '', '', 25, '2019-02-06', '17:24:14'),
(159, 48, 49, 11, 'Devora Molina', 3, 'null', 'Devora', 'Flaca', '', 0, 0, '', '', 25, '2019-02-06', '17:24:45'),
(160, 48, 49, 12, 'Betsy Acevedo', 5, 'null', 'Betsy', 'Betsy', '', 0, 0, '', '', 25, '2019-02-06', '17:25:19'),
(161, 48, 49, 13, 'Catalina Marchant', 7, 'null', 'Catalina', 'Maru', '', 0, 0, '', '', 25, '2019-02-06', '17:27:31'),
(167, 56, 54, 15, 'Verniel', 3, 'null', 'Verniel', 'Verniel', '', 0, 0, '', 'REVISAR QUE TIPO DE IMAGEN ES', 28, '2019-02-08', '16:59:11'),
(168, 56, 54, 16, 'Carolina Zuñiga', 2, 'null', 'Krito', 'Carolina', '', 0, 0, '', '', 28, '2019-02-08', '17:00:49'),
(169, 56, 54, 17, 'juan Pablo Rojas', 9, 'null', 'Guaporacer', 'Juan Pablo', '', 0, 0, '', '', 28, '2019-02-08', '17:01:57'),
(170, 56, 54, 18, 'Denissa Soto', 2, 'null', 'Negra ', 'Denissa', '', 0, 0, '', '', 28, '2019-02-08', '17:03:11'),
(172, 58, 56, 55, 'araya ', 6, 'null', 'Nicoarayal', 'Nicoarayal', '', 0, 0, '', '', 27, '2019-02-08', '19:21:20'),
(173, 58, 56, 56, 'Carvajal', 6, '[\"1\"]', 'Mrtneu;', 'Mrtneu;', '', 0, 0, '', '', 27, '2019-02-08', '19:24:57'),
(174, 58, 56, 57, 'Del pino ', 6, 'null', 'Weedson', 'Weedson', '', 0, 0, '', '', 27, '2019-02-08', '19:26:55'),
(175, 58, 56, 58, 'Morales ', 1, '[\"3\",\"9\"]', 'Nico', 'Nico', '', 0, 0, '', '', 27, '2019-02-08', '19:34:06'),
(176, 58, 56, 59, 'Lopez ', 4, 'null', 'Lápiz ', 'Lápiz ', '', 0, 0, '', '', 27, '2019-02-08', '19:35:43'),
(177, 58, 56, 60, 'Hidalgo ', 2, 'null', 'Cooni', 'Cooni ', '', 0, 0, '', '', 27, '2019-02-08', '19:38:46'),
(178, 58, 56, 61, 'Cid ', 2, '[\"1\"]', 'Mexi', 'Mexi ', '', 0, 0, '', '', 27, '2019-02-08', '19:40:19'),
(179, 58, 56, 62, 'Catalan ', 1, '[\"1\"]', 'Cotyco LGM', 'Cotyco LGM', '', 0, 0, '', '', 27, '2019-02-08', '19:42:19'),
(180, 58, 56, 63, 'Pineda ', 6, 'null', 'Peneda ', 'Peneda ', '', 0, 0, '', '', 27, '2019-02-08', '19:44:26'),
(181, 58, 56, 64, 'Solari', 2, '[\"1\"]', 'Barbi', 'Barbi ', '', 0, 0, '', '', 27, '2019-02-08', '19:45:47'),
(182, 58, 56, 65, 'Cristobal Rojas ', 5, 'null', 'Crissyz', 'Crissyz', '', 0, 0, '', '', 27, '2019-02-08', '19:47:30'),
(183, 58, 56, 66, 'Zavala ', 2, 'null', 'Huasita ', 'Huasita ', '', 0, 0, '', '', 27, '2019-02-08', '19:48:44'),
(184, 58, 56, 67, 'Inti Rojas ', 1, '[\"3\"]', 'Pili LGM', 'Pili LGM', '', 0, 0, '', '', 27, '2019-02-08', '19:55:10'),
(185, 58, 56, 68, 'Moreno ', 4, 'null', 'Rubencte', 'Rubencte ', '', 0, 0, '', '', 27, '2019-02-08', '19:56:38'),
(193, 61, 59, 80, 'Escobar ', 3, 'null', 'Pao', 'Pao', '', 0, 0, '', '', 27, '2019-02-11', '03:06:03'),
(194, 61, 59, 81, 'Arriagada ', 1, '[\"1\"]', 'Aruru LGM ', 'Aruru LGM', '', 0, 0, '', '', 27, '2019-02-11', '03:07:49'),
(195, 61, 59, 82, 'Fredes ', 1, 'null', 'Feña ', 'Feña', '', 0, 0, '', '', 27, '2019-02-11', '03:09:39'),
(196, 61, 59, 83, 'Mora ', 4, 'null', 'Mora ', 'Mora ', '', 0, 0, '', '', 27, '2019-02-11', '03:10:52'),
(197, 61, 59, 84, 'Medina ', 6, '[\"1\"]', 'Diego', 'Diego', '', 0, 0, '', '', 27, '2019-02-11', '03:12:49'),
(198, 61, 59, 85, 'Tapia', 2, 'null', 'Pepaa', 'Pepaa', '', 0, 0, '', '', 27, '2019-02-11', '03:14:10'),
(199, 61, 59, 86, 'Olmos', 2, 'null', 'Palomacs', 'Palomacs', '', 0, 0, '', '', 27, '2019-02-11', '03:15:28'),
(200, 61, 59, 87, 'Quitral', 4, 'null', 'Alex', 'Alex', '', 0, 0, '', '', 27, '2019-02-11', '03:16:26'),
(201, 61, 59, 88, 'Antonia Reyes', 3, 'null', 'Anto', 'Anto', '', 0, 0, '', '', 27, '2019-02-11', '03:17:53'),
(202, 61, 59, 89, 'Max Reyes', 6, '[\"1\"]', 'Macs', 'Macs', '', 0, 0, '', '', 27, '2019-02-11', '03:19:16'),
(203, 61, 59, 90, 'Catalina Urra', 3, 'null', 'Cata', 'Cata', '', 0, 0, '', '', 27, '2019-02-11', '03:20:20'),
(204, 61, 59, 91, 'Alexandre ', 1, '[\"1\"]', 'Antonella', 'Antonella', '', 0, 0, '', '', 27, '2019-02-11', '03:23:43'),
(205, 61, 59, 92, 'Palomino', 3, 'null', 'Palmi LGM', 'Palmi LGM', '', 0, 0, '', '', 27, '2019-02-11', '03:25:10'),
(206, 61, 59, 93, 'Martinez', 2, '[\"1\"]', 'Fer', 'Fer', '', 0, 0, '', '', 27, '2019-02-11', '03:26:20'),
(207, 62, 60, 94, 'Iribarren', 4, 'null', 'Mauro', 'Mauro', '', 0, 0, '', '', 27, '2019-02-11', '03:41:50'),
(208, 62, 60, 95, 'Profe Luis', 6, 'null', 'Profe Luis', 'Profe Luis', '', 0, 0, '', '', 27, '2019-02-11', '03:43:02'),
(209, 62, 60, 96, 'Olivares', 1, 'null', 'Javi', 'Javi', '', 0, 0, '', '', 27, '2019-02-11', '03:44:01'),
(210, 56, 61, 14, 'Dilan Vera', 9, 'null', 'Dilan', 'Dilan', '', 0, 0, '', '', 28, '2019-02-14', '11:39:08'),
(211, 56, 62, 13, 'Marcelo', 4, 'null', 'Chelo', 'Marcelo', '', 0, 0, '', '', 28, '2019-02-14', '11:40:37'),
(212, 56, 63, 3, 'Agustín Moreira', 4, '[\"5\"]', 'Agustinho', 'Agustín', '', 0, 0, '', '', 28, '2019-02-14', '11:47:22'),
(213, 56, 63, 4, 'Ignacio Barrientos', 4, 'null', 'Jordi', 'Ignacio', '', 0, 0, '', '', 28, '2019-02-14', '11:56:30'),
(214, 56, 63, 5, 'Bastián Navarrete', 4, 'null', 'Basti_nj', 'Bastián', '', 0, 0, '', '', 28, '2019-02-14', '11:58:21'),
(215, 56, 63, 6, 'Angela Espinoza', 2, 'null', 'Milla', 'Angela', '', 0, 0, '', '', 28, '2019-02-14', '12:01:42'),
(216, 56, 63, 7, 'Ítalo Soto', 9, 'null', 'Talo_I', 'Ítalo', '', 0, 0, '', 'REVISAR IMAGEN', 28, '2019-02-14', '12:06:24'),
(217, 56, 63, 8, 'Víctor Corvalán', 4, '[\"9\"]', 'Víctor', 'Víctor', '', 0, 0, '', '', 28, '2019-02-14', '12:10:31'),
(218, 56, 63, 9, 'Rodrigo', 6, '[\"5\"]', 'Roro', 'Rodrigo', '', 0, 0, '', '', 28, '2019-02-14', '12:22:55'),
(219, 56, 63, 10, 'Rayén', 5, '[\"7\",\"9\"]', 'Rayén', 'Rayén', '', 0, 0, '', '', 28, '2019-02-14', '12:24:56'),
(220, 56, 63, 11, 'José Salas', 4, '[\"5\"]', 'Nachito', 'José S.', '', 0, 0, '', '', 28, '2019-02-14', '13:18:16'),
(221, 56, 63, 12, 'Nicolás', 4, '[\"9\"]', 'Tufavo', 'Nicolás', '', 0, 0, '', '', 28, '2019-02-14', '13:19:26'),
(222, 72, 64, 209877716, 'María Paz Benavides', 3, '[\"7\",\"9\",\"10\"]', 'Mari', 'María Paz ', '', 0, 0, '', 'EL EMOJI DEL RAYO JUNTO CON EL NOMBRE', 29, '2019-02-14', '16:25:45'),
(223, 72, 64, 253579838, 'Nayadeth Miranda', 2, '[\"10\"]', 'Naya.Miranda.C', 'Naya.M.C', '', 0, 0, '', '', 29, '2019-02-14', '16:28:55'),
(224, 72, 64, 208266632, 'Estrella Yañez', 2, '[\"7\",\"9\",\"10\"]', 'Estrella Belén', 'Estrella', '', 0, 0, '', '', 29, '2019-02-14', '16:32:09'),
(225, 72, 64, 209472430, 'Camila Barrachina', 2, '[\"7\",\"9\",\"10\"]', 'Camila Ignacia', 'Camila', '', 0, 0, '', '', 29, '2019-02-14', '16:36:21'),
(226, 72, 64, 179088691, 'Daniela Catalán', 3, '[\"7\",\"9\",\"10\"]', 'Kitty', 'Kitty ', '', 0, 0, '', '', 29, '2019-02-14', '16:39:37'),
(227, 72, 64, 205691138, 'Ornella Orellana', 2, '[\"7\",\"9\",\"10\"]', 'Orne ', 'Orne', '', 0, 0, '', '', 29, '2019-02-14', '16:42:03'),
(228, 72, 64, 205695230, 'Angel Fuentes', 6, 'null', 'Angel', 'Angel_Kingz', '', 0, 0, '', '', 29, '2019-02-14', '16:43:40'),
(229, 72, 64, 209977397, 'Amparo Abrigo', 9, '[\"7\"]', 'Hitommi', 'Hitommi Abrigo H.', '', 0, 0, '', '', 29, '2019-02-14', '16:47:52'),
(230, 72, 64, 210205322, 'Arleth Narváez', 3, '[\"9\",\"10\"]', 'Arleth', 'Arleth Narváez', '', 0, 0, '', '', 29, '2019-02-14', '16:50:04'),
(231, 72, 64, 209431955, 'Esteban Peña', 6, 'null', 'Peñita', 'Peña', '', 0, 0, '', '', 29, '2019-02-14', '16:53:29'),
(232, 72, 64, 210211934, 'Fabián Arancibia', 3, 'null', 'Fabián AA', 'Fabian2blea', '', 0, 0, '', 'EMOJI DE CARTA DE JOKER Y BOMBA JUNTO CON EL NOMBRE', 29, '2019-02-14', '16:59:49'),
(233, 72, 64, 207288659, 'Carlos Catalán', 7, 'null', 'C.H.R', 'C.H.R', '', 0, 0, '', '', 29, '2019-02-14', '17:16:31'),
(234, 72, 64, 20833378, 'Yessica Medina', 3, '[\"7\",\"9\",\"10\"]', 'Melly', 'Melly', '', 0, 0, '', '', 29, '2019-02-14', '17:39:24'),
(235, 56, 65, 1, 'Isabel', 5, '[\"3\",\"7\"]', 'Isabel', '', '', 0, 0, '', 'REVISAR APODO ESPALDA, PLANILLA EN BLANCO', 28, '2019-02-15', '12:02:09'),
(236, 56, 66, 2, 'José fuentes', 6, '[\"5\"]', 'Rombo', 'José F.', '', 0, 0, '', '', 28, '2019-02-15', '12:07:15'),
(237, 56, 67, 19, 'Claudia Cisternas', 2, 'null', 'Clau', 'Claudia', '', 0, 0, '', '', 28, '2019-02-15', '12:14:26'),
(238, 56, 67, 20, 'Bárbara', 2, '[\"1\"]', 'Barby', 'Bárbara', '', 0, 0, '', '', 28, '2019-02-15', '12:17:54'),
(239, 56, 74, 22, 'Francisca Llano', 1, 'null', 'Soprole', 'Francisca', '', 0, 0, '', 'REVISAR IMAGEN (IMAGEN REFERENCIAL)', 28, '2019-02-15', '12:26:10'),
(240, 56, 74, 23, 'Camila Bascuñán', 3, 'null', 'Bascu', 'Camila', '', 0, 0, '', 'REVISAR IMAGEN (IMAGEN REFERENCIAL)', 28, '2019-02-15', '12:28:04'),
(241, 56, 75, 24, 'Catalina Reyes', 1, 'null', 'Catita', 'Catalina', '', 0, 0, '', '', 28, '2019-02-15', '12:30:17'),
(242, 56, 75, 25, 'Gabriel Silva ', 5, '[\"9\"]', 'Polako', 'Gabriel', '', 0, 0, '', '', 28, '2019-02-15', '12:31:29'),
(243, 72, 64, 20893255, 'Iván Calquín', 5, 'null', 'Iván Presley', 'Iván Presley', '', 0, 0, '', '', 29, '2019-02-16', '18:24:15'),
(244, 72, 64, 20965149, 'Benjamín Torrealba', 6, 'null', '~Benjamín', '~Benjamín', '', 0, 0, '', '', 29, '2019-02-16', '18:29:45'),
(245, 72, 64, 20985283, 'Cristobal Morales', 6, '[\"1\"]', 'Cristobal', 'Morales', '', 0, 0, '', '', 29, '2019-02-16', '18:37:51'),
(246, 72, 64, 20884773, 'Constanza Rojas', 2, '[\"10\"]', 'Conni', 'Constanza', '', 0, 0, '', '', 29, '2019-02-16', '18:47:11'),
(247, 72, 64, 21037566, 'Romina Berrios', 3, '[\"10\"]', 'Romy', 'Romina Andrea', '', 0, 0, '', '', 29, '2019-02-16', '19:01:56'),
(248, 72, 64, 21020715, 'Lian Montalba', 7, 'null', 'Lian', 'Lian', '', 0, 0, '', '', 29, '2019-02-16', '19:08:15'),
(249, 72, 64, 20948447, 'Mikaela Juan', 2, '[\"2\"]', 'Mika Fifi', 'Mika Juan ', '', 0, 0, '', '', 29, '2019-02-16', '19:30:41'),
(250, 72, 64, 20576370, 'Dylan Llanos', 3, 'null', 'Arki-nos', 'Dylân', '', 0, 0, '', '', 29, '2019-02-16', '20:08:23'),
(251, 72, 64, 20568906, 'Ricardo Álvarez', 5, 'null', 'Ricardo', 'Ricardo', '', 0, 0, '', '', 29, '2019-02-16', '20:18:48'),
(252, 72, 64, 20731014, 'Constanza Ríos', 5, 'null', 'Conito uwu', 'Conito uwu', '', 0, 0, '', '', 29, '2019-02-16', '20:42:02'),
(253, 72, 64, 24440873, 'Alejandro Zamudio', 4, 'null', 'Zamudio', 'Zamudio', '', 0, 0, '', '', 29, '2019-02-16', '21:15:56'),
(254, 72, 64, 20445306, 'Alvaro Salinas', 4, 'null', 'Black', 'Alvaro.S.V', '', 0, 0, '', '', 29, '2019-02-16', '21:18:10'),
(255, 72, 64, 21011686, 'Tamara Moyano', 2, 'null', 'Tamara Catalina', 'Tamara Catalina', '', 0, 0, '', '', 29, '2019-02-16', '21:42:44'),
(256, 72, 64, 18761302, 'Damián Cueto', 4, 'null', 'Profe Damián', 'Profe Damián', '', 0, 0, '', '', 29, '2019-02-16', '22:03:35'),
(257, 73, 76, 208076426, 'atencia', 1, 'null', 'lady', 'españolísima', '', 0, 0, '', '', 33, '2019-02-19', '16:37:29'),
(258, 73, 76, 123456789, 'igor', 8, 'null', 'igørcin', 'australia', '', 0, 0, '', '', 33, '2019-02-19', '16:43:29'),
(259, 73, 76, 123456788, 'emilia', 3, 'null', 'emimi', 'tomatito', '', 0, 0, '', '', 33, '2019-02-19', '16:44:56'),
(260, 73, 76, 123456787, 'catrileo', 3, 'null', 'flo', 'system 1', '', 0, 0, '', '', 33, '2019-02-19', '16:50:41'),
(261, 73, 76, 123456799, 'chirino', 2, 'null', 'anto', 'antonia', '', 0, 0, '', '', 33, '2019-02-19', '16:51:24'),
(262, 73, 76, 123456678, 'contreras', 2, 'null', 'cao', 'estilosa', '', 0, 0, '', '', 33, '2019-02-19', '16:52:45'),
(263, 73, 76, 123345567, 'dupuis', 2, 'null', 'mooni', 'la flexible', '', 0, 0, '', '', 33, '2019-02-19', '16:54:52'),
(264, 73, 76, 12, 'freire', 3, 'null', 'funky danky', 'itsdimondo', '', 0, 0, '', '', 33, '2019-02-19', '16:55:48'),
(265, 73, 76, 123, 'leon', 2, 'null', 'morocha', 'raawr', '', 0, 0, '', '', 33, '2019-02-19', '16:56:48'),
(266, 73, 76, 1234, 'loyola', 1, '[\"1\"]', 'soleste', 'soledaddy', '', 0, 0, '', '', 33, '2019-02-19', '16:59:57'),
(267, 73, 76, 12342, 'montes', 3, 'null', 'mc lovin', 'montesco', '', 0, 0, '', '', 33, '2019-02-19', '17:00:41'),
(268, 73, 76, 13092001, 'viky', 5, 'null', 'negritss', 'vi.kyyyyy', '', 0, 0, '', '', 33, '2019-02-19', '17:12:14'),
(269, 73, 76, 1222, 'muñoz', 7, '[\"5\"]', 'Luchø ', 'chølu', '', 0, 0, '', '', 33, '2019-02-19', '17:20:04'),
(270, 73, 76, 98765, 'ponce a', 1, 'null', 'almond', 'lengüita', '', 0, 0, '', '', 33, '2019-02-19', '17:21:22'),
(271, 73, 76, 2, 'ponce c', 4, 'null', 'mc marchas', ' wolfy', '', 0, 0, '', '', 33, '2019-02-19', '17:22:28'),
(272, 73, 76, 0, 'ramirez', 3, 'null', 'falling.asleep', 'falling.asleep', '', 0, 0, '', '', 33, '2019-02-19', '17:24:02'),
(273, 73, 76, 93838, 'ramos c', 2, 'null', 'cami', 'cami', '', 0, 0, '', '', 33, '2019-02-19', '17:30:25'),
(274, 73, 76, 999, 'ren', 2, 'null', 'ren', 'JiayIR', '', 0, 0, '', '', 33, '2019-02-19', '17:31:33'),
(275, 73, 76, 22233, 'Ramos', 1, 'null', 'grinch', 'daisy', '', 0, 0, '', '', 33, '2019-02-19', '17:33:04'),
(276, 73, 76, 77899, 'salas', 2, 'null', 'britney', '√2gh', '', 0, 0, '', '', 33, '2019-02-19', '18:05:50'),
(277, 73, 76, 101, 'sanhueza', 3, 'null', 'brujita', 'minnie', '', 0, 0, '', '', 33, '2019-02-19', '18:26:26'),
(278, 73, 76, 303, 'xu', 3, 'null', 'eli', 'china bélica', '', 0, 0, '', '', 33, '2019-02-19', '18:30:58'),
(279, 73, 76, 87987, 'zhang', 2, 'null', 'wen', 'kun fu panda', '', 0, 0, '', '', 33, '2019-02-19', '18:34:10'),
(280, 73, 76, 234432, 'juanpi', 8, 'null', 'juanpi', 'negro', '', 0, 0, '', '', 33, '2019-02-19', '18:35:18'),
(281, 73, 76, 324, 'ximenA', 3, 'null', 'miss xime', 'miss ximena', '', 0, 0, '', '', 33, '2019-02-19', '18:40:26'),
(282, 73, 76, 3542, 'cristian', 7, 'null', 'profe cristian', '', '', 0, 0, '', '', 33, '2019-02-19', '18:41:11'),
(283, 73, 76, 98, 'gabo', 9, 'null', 'profe gabo', '', '', 0, 0, '', '', 33, '2019-02-19', '18:41:42'),
(284, 73, 76, 889, 'mauri', 7, 'null', 'shakiro', 'dr. nipple', '', 0, 0, '', '', 33, '2019-02-19', '18:42:20'),
(285, 73, 76, 233324, 'monica', 3, 'null', 'miss mónica', 'miss mónica', '', 0, 0, '', '', 33, '2019-02-19', '18:45:19'),
(286, 73, 76, 76, 'joao', 6, 'null', 'profe joao', '', '', 0, 0, '', '', 33, '2019-02-19', '19:01:45'),
(287, 73, 77, 879000, 'aguilera', 5, 'null', 'xelo', 'marceleiin', '', 0, 0, '', '', 33, '2019-02-19', '19:03:16'),
(288, 73, 77, 9876, 'ayala', 6, 'null', 'zabio', 'zabiosaurio', '', 0, 0, '', '', 33, '2019-02-19', '19:03:57'),
(289, 73, 77, 896965, 'bastias', 5, 'null', 'machine', 'mákina', '', 0, 0, '', '', 33, '2019-02-19', '19:04:35'),
(290, 73, 77, 234567, 'bonacic', 5, 'null', 'bonacic', 'bonacic', '', 0, 0, '', '', 33, '2019-02-19', '19:05:29'),
(291, 73, 77, 545444, 'capitani', 7, 'null', 'tito', 'tito', '', 0, 0, '', '', 33, '2019-02-19', '19:06:30'),
(292, 73, 77, 676, 'fuenteseca', 3, 'null', 'chiki', 'enaneitor', '', 0, 0, '', '', 33, '2019-02-19', '19:07:16'),
(293, 73, 77, 3456, 'lópez', 5, 'null', 'narcopun', 'capuleto', '', 0, 0, '', '', 33, '2019-02-19', '19:08:21'),
(294, 73, 77, 666, 'Josefa Rodriguez', 3, 'null', 'jose', 'jose', '', 0, 0, '', '', 33, '2019-02-19', '19:09:22'),
(295, 73, 77, 345687, 'silva', 5, 'null', 'wisle', 'byron', '', 0, 0, '', '', 33, '2019-02-19', '19:14:41'),
(296, 73, 77, 4567, 'yarad', 9, 'null', 'kito', 'bin laden', '', 0, 0, '', '', 33, '2019-02-19', '19:15:14'),
(297, 73, 77, 324899, 'Nico', 6, '[\"1\"]', 'Nico', 'Montero', '', 0, 0, '', '', 33, '2019-02-19', '19:18:46'),
(298, 72, 64, 20567962, 'Diego Vera', 4, 'null', 'D.a.v_bvrber', 'Dav barber', '', 0, 0, '', 'AGREGAR EL EMOJI DEL EXTRATERRESTRE Y POSTE DE BARBERO JUNTO EL NOMBRE ', 29, '2019-02-19', '21:13:32'),
(299, 72, 64, 20974362, 'Rocío Torrealba', 3, 'null', 'Rocío', 'Rossev', '', 0, 0, '', '', 29, '2019-02-20', '20:55:11'),
(300, 72, 64, 20893189, 'Nicolás Cortés', 7, 'null', 'Xero', 'Xero', '', 0, 0, '', '', 29, '2019-02-20', '20:57:48'),
(301, 72, 64, 20626280, 'Janis Domínguez', 3, 'null', 'Janiss', 'Janiss', '', 0, 0, '', '', 29, '2019-02-20', '21:00:57'),
(302, 72, 64, 20615274, 'Carlos Fuentes', 5, 'null', 'Carlos F. P.', 'Walala', '', 0, 0, '', '', 29, '2019-02-20', '21:13:12'),
(303, 72, 64, 20892963, 'Dante Tapia', 5, 'null', 'Danteskof', 'Dante', '', 0, 0, '', '', 29, '2019-02-20', '21:34:11'),
(304, 58, 78, 1, 'Escobar Pao', 3, 'null', 'Pao', 'Pao', '', 0, 0, '', '', 22, '2019-02-21', '08:50:27'),
(305, 58, 78, 2, 'Arriagada', 1, '[\"1\"]', 'Aruru LGM', 'Aruru LGM', '', 0, 0, '', '', 22, '2019-02-21', '08:51:35'),
(306, 58, 78, 3, 'Fredes', 1, 'null', 'Feña ', 'Feña ', '', 0, 0, '', '', 22, '2019-02-21', '08:52:46'),
(307, 58, 78, 4, 'Mora ', 4, 'null', 'Mora ', 'Mora ', '', 0, 0, '', '', 22, '2019-02-21', '08:53:31'),
(308, 58, 78, 5, 'Medina', 6, '[\"1\"]', 'Diego', 'Diego', '', 0, 0, '', '', 22, '2019-02-21', '08:54:35'),
(309, 58, 78, 6, 'Tapia ', 2, 'null', 'Pepaa ', 'Pepaa ', '', 0, 0, '', '', 22, '2019-02-21', '08:55:26'),
(310, 58, 78, 7, 'Olmos', 2, 'null', 'Palomacs ', 'Palomacs ', '', 0, 0, '', '', 22, '2019-02-21', '08:56:05'),
(311, 58, 78, 8, 'Quitral ', 4, 'null', 'Alex', 'Alex', '', 0, 0, '', '', 22, '2019-02-21', '08:57:20'),
(312, 58, 78, 9, 'Antonia Reyes', 3, 'null', 'Anto', 'Anto', '', 0, 0, '', '', 22, '2019-02-21', '08:57:59'),
(313, 58, 78, 10, 'Max Reyes ', 6, '[\"1\"]', 'Macs', 'Macs', '', 0, 0, '', '', 22, '2019-02-21', '08:58:46'),
(314, 58, 78, 11, 'Catalina Urra ', 3, 'null', 'Cata ', 'Cata ', '', 0, 0, '', '', 22, '2019-02-21', '08:59:31'),
(315, 58, 78, 12, 'Alexandre', 1, '[\"1\"]', 'Antonella', 'Antonella', '', 0, 0, '', '', 22, '2019-02-21', '09:00:17'),
(316, 58, 78, 13, 'Palomino', 3, 'null', 'Palmi LGM ', 'Palmi LGM ', '', 0, 0, '', '', 22, '2019-02-21', '09:01:00'),
(317, 58, 78, 14, 'Martinez', 2, '[\"1\"]', 'Fer ', 'Fer ', '', 0, 0, '', '', 22, '2019-02-21', '09:02:52'),
(318, 58, 79, 15, 'Iribarren', 4, 'null', 'Mauro ', 'Mauro ', '', 0, 0, '', '', 22, '2019-02-21', '09:10:28'),
(319, 58, 79, 16, 'Profe Luis ', 6, 'null', 'Profe Luis ', 'Profe Luis ', '', 0, 0, '', '', 22, '2019-02-21', '09:11:24'),
(320, 58, 79, 17, 'Olivares', 1, 'null', 'Javi', 'Javi', '', 0, 0, '', '', 22, '2019-02-21', '09:12:21'),
(321, 75, 80, 1, 'Daniela ', 4, '[\"4\"]', 'Daniela ', 'Daniela', '', 0, 0, '', '', 25, '2019-03-04', '14:42:07'),
(322, 75, 80, 2, 'pilar', 3, 'null', 'Alejandra', 'Alejandra', '', 0, 0, '', '', 25, '2019-03-04', '14:45:42'),
(323, 75, 80, 3, 'jeraldin', 2, 'null', 'jera', 'jera', '', 0, 0, '', '', 25, '2019-03-04', '14:51:45'),
(324, 75, 80, 4, 'constanza', 2, '[\"2\",\"5\"]', 'consty', 'consty', '', 0, 0, '', '', 25, '2019-03-04', '14:59:35'),
(325, 75, 80, 5, 'cony gamboa', 2, 'null', 'negrita', 'negrita', '', 0, 0, '', '', 25, '2019-03-04', '15:01:54'),
(326, 75, 80, 6, 'alan', 4, 'null', 'alan.....mua', 'alan.....mua', '', 0, 0, '', '', 25, '2019-03-04', '15:05:54'),
(327, 75, 80, 7, 'maria', 3, 'null', 'Maria...ssi', 'Maria...ssi', '', 0, 0, '', '', 25, '2019-03-04', '15:09:08'),
(328, 75, 81, 0, 'vlentina', 5, '[\"10\"]', 'Valuchy', 'Valuchy', '', 0, 0, '', '', 25, '2019-03-04', '15:12:12'),
(329, 75, 81, 99, 'natalia', 7, 'null', 'Gasparina', 'Gasparina', '', 0, 0, '', '', 25, '2019-03-04', '15:17:32'),
(330, 75, 81, 555, 'isack', 6, 'null', 'yitzhak', 'yitzhak', '', 0, 0, '', '', 25, '2019-03-04', '15:23:00'),
(331, 75, 81, 66, 'belen', 3, 'null', 'pandix', 'pandix', '', 0, 0, '', '', 25, '2019-03-04', '15:24:27'),
(332, 76, 82, 208192981, 'Constanza Soto', 2, '[\"1\"]', 'Conti', '@cxnstza', 'Humanista', 0, 0, '', '', 40, '2019-03-12', '19:03:05'),
(333, 76, 82, 21000, 'Constanza', 2, '[\"1\"]', 'Kony', '@cnymrtnz', 'Humanista', 0, 0, '', '', 40, '2019-03-13', '17:20:15'),
(334, 76, 82, 20680, 'Mariana Lizama', 2, 'null', 'Mariana', '@_mrn.slng_', 'Humanista', 0, 0, '', '', 40, '2019-03-13', '17:22:25'),
(335, 76, 82, 20911, 'Yanella Olguin', 1, 'null', 'Yaneloca', '@_.mlnso._', 'Humanista', 0, 0, '', '', 40, '2019-03-13', '17:28:09'),
(336, 76, 82, 21007, 'Gabriel Mendez', 7, 'null', 'Wills', 'Gabo', 'Humanista', 0, 0, '', '', 40, '2019-03-13', '17:29:55'),
(337, 76, 82, 21005, 'Diego Soto', 7, 'null', 'Diego', 'Otani', 'Humanista', 0, 0, '', '', 40, '2019-03-13', '17:31:12'),
(338, 76, 82, 20785, 'Mattias Bacilio', 3, 'null', 'Bacilio', 'Bacilio', 'Humanista', 0, 0, '', '', 40, '2019-03-13', '17:33:37'),
(339, 76, 82, 20451639, 'Fernanda Pozo', 1, 'null', 'Feerjavi', 'Feerjavi', 'Humanista', 0, 0, '', '', 40, '2019-03-13', '17:36:05'),
(340, 76, 82, 20336556, 'Martin Ugarte', 7, 'null', 'Martinoh', 'Martinoh', 'Humanista', 0, 0, '', '', 40, '2019-03-13', '17:38:06'),
(341, 76, 82, 21065, 'Simoné Ceron', 5, 'null', 'Simo', 'Simo', 'Humanista', 0, 0, '', '', 40, '2019-03-13', '17:39:39'),
(342, 76, 82, 21014622, 'Javier Perez', 9, 'null', 'Xavier', 'off____record', 'Humanista', 0, 0, '', '', 40, '2019-03-13', '17:42:06'),
(343, 76, 82, 20835, 'Javiera Rodriguez', 3, 'null', '@Javieera__', '@Javieera__', 'Humanista', 0, 0, '', '', 40, '2019-03-13', '17:43:54'),
(344, 76, 82, 20707, 'Sebastian Figueroa', 7, 'null', 'Tata Figue', '@_.des.orden_', 'Humanista', 0, 0, '', '', 40, '2019-03-13', '17:45:46'),
(345, 76, 82, 20911, 'Alejandra Toledo', 9, 'null', '@4.4leeh', 'Alejandra', 'Humanista', 0, 0, '', '', 40, '2019-03-13', '17:48:05'),
(347, 76, 82, 20908, 'Cinthya Reyes', 3, 'null', 'cici', '@cicireyesx', 'Humanista', 0, 0, '', '', 40, '2019-03-13', '17:51:03'),
(348, 76, 82, 20846768, 'Martín Vasconcelos', 7, 'null', 'Vasconcelos', 'Red Star', 'Humanista', 0, 0, '', '', 40, '2019-03-13', '17:54:01'),
(349, 76, 82, 20834, 'Juan Salas', 9, 'null', 'GHOST', 'GHOST', 'Humanista', 0, 0, '', '', 40, '2019-03-13', '17:55:43'),
(350, 76, 82, 20467, 'Felipe Villalon', 6, 'null', 'Felipe', '6lin_lin9', 'Humanista', 0, 0, '', '', 40, '2019-03-13', '17:58:27'),
(351, 76, 82, 22182121, 'Pamela Falcon', 3, 'null', 'Pame', 'P4mel4._', 'Humanista', 0, 0, '', '', 40, '2019-03-13', '18:00:57'),
(352, 76, 82, 20390, 'Yerko Torres', 9, '[\"5\"]', 'DYZVZTER', 'dyz_ohm', 'Humanista', 0, 0, '', 'LA TALLA ES XXLB', 40, '2019-03-13', '18:04:18'),
(353, 76, 84, 20534, 'Jose San Martin', 6, 'null', '@Nxch0_', 'Jose', 'Humanista', 0, 0, '', '', 40, '2019-03-13', '18:32:09'),
(354, 76, 84, 20677, 'Luis Vasquez', 7, 'null', 'Shisho', 'Shisho', 'Humanista', 0, 0, '', '', 40, '2019-03-13', '18:36:01'),
(355, 76, 84, 207314196, 'sarai', 5, '[\"1\"]', '@_.ss.gm._', 'zorrai', 'humanista', 0, 0, '', '', 40, '2019-03-14', '10:54:49'),
(356, 76, 84, 210627383, 'Benjamin', 5, 'null', 'Benjinha', 'Benjinha', 'Humanista', 0, 0, '', '', 40, '2019-03-14', '10:57:43'),
(358, 76, 84, 2147483647, 'Matias Vasquez', 3, 'null', 'Maty', 'El Tata', 'Humanista', 0, 0, '', '', 40, '2019-03-14', '12:37:13'),
(359, 76, 84, 20468, 'Paolo ', 7, 'null', 'Pailo', '@que_wia', 'Humanista', 0, 0, '', '', 40, '2019-03-14', '12:41:44'),
(360, 76, 84, 20907, 'Vania ', 9, 'null', 'Mapache', 'Mapache', '', 0, 0, '', '', 40, '2019-03-14', '12:44:05'),
(361, 76, 84, 20783, 'Diego Hidalgo', 4, 'null', 'Diego Ignacio', 'Diego Mcqueen', 'Humanista', 0, 0, '', '', 40, '2019-03-14', '12:46:21'),
(362, 76, 84, 20814995, 'Sofia', 9, 'null', 'Mellon Collie', 'Kabuki Girl', 'Humanista', 0, 0, '', '', 40, '2019-03-14', '12:48:44'),
(363, 76, 84, 163606364, 'Alexis', 6, 'null', 'Alex', 'Profe Alexis', 'Humanista', 0, 0, '', 'ES EL PROFESOR', 40, '2019-03-14', '12:57:31'),
(364, 76, 84, 21023, 'John Vasquez', 6, 'null', 'Towe', 'Towe', 'Humanista', 0, 0, '', '', 40, '2019-03-14', '13:15:46'),
(365, 76, 82, 139047427, 'Patricio Osorio', 4, 'null', 'Teacher PaTTo', 'Teacher PaTTo', '', 0, 0, '', '', 40, '2019-03-14', '16:26:54'),
(366, 76, 84, 210232958, 'Ignacio Mardones', 7, 'null', 'Nachito', 'Nachito', 'Humanista', 0, 0, '', '', 40, '2019-03-14', '16:39:02'),
(367, 76, 84, 205535411, 'Matias Rojas', 9, 'null', 'Chepa', 'Chepa', 'Humanista', 0, 0, '', '', 40, '2019-03-14', '17:03:41'),
(369, 82, 87, 20976, 'Miguel', 4, '[\"3\"]', 'Miguelon ????', 'Miguel', 'Humanista', 0, 0, '', 'MANGA AJUSTADA', 47, '2019-03-18', '10:50:59'),
(370, 82, 87, 20965, 'Karina', 3, '[\"10\"]', 'Primera Dama ', 'Karina', 'Ciéntifico', 0, 0, '', '', 47, '2019-03-18', '11:04:28'),
(371, 82, 87, 21061, 'María José', 2, '[\"10\"]', 'Cotee???? ', 'Jesús????', 'Científica ', 0, 0, '', '', 47, '2019-03-18', '11:06:36'),
(372, 82, 88, 20426, 'Monttserrat', 2, '[\"10\"]', 'Monsita ????', 'Monttserrat', 'Humanista', 0, 0, '', 'MANGA AJUSTADA', 47, '2019-03-18', '11:10:14'),
(373, 82, 88, 20670, 'Matías|', 4, '[\"5\"]', 'energetine masnter', 'Matías', 'Humanista', 0, 0, '', '', 47, '2019-03-18', '11:14:18'),
(374, 82, 88, 20435, 'Scarlett', 4, 'null', 'Carly', 'Scarlett', 'Humanista', 0, 0, '', '', 47, '2019-03-18', '11:17:16'),
(375, 82, 88, 20784, 'Catalina', 4, '[\"7\",\"10\"]', 'Catita', 'Catalina', 'Científico', 0, 0, '', '', 47, '2019-03-18', '11:21:12'),
(376, 82, 88, 20937, 'Franco', 6, 'null', 'Rossy ????', 'Franco', 'Humanista', 0, 0, '', '', 47, '2019-03-18', '11:40:28'),
(377, 82, 88, 20773, 'Zorhaya', 6, 'null', 'Zory ????', 'Zorhaya', 'Científico', 0, 0, '', '', 47, '2019-03-18', '11:44:54'),
(378, 82, 88, 25776, 'Juan Pablo', 6, 'null', 'JP', 'Juan Pablo', 'Científico', 0, 0, '', '', 47, '2019-03-18', '11:46:05'),
(379, 82, 88, 183410121, 'Ricardo', 4, 'null', 'Ricardo', 'Profesor Ricardo', '', 0, 0, '', '', 47, '2019-03-18', '11:47:54'),
(380, 82, 89, 20763, 'Camilo', 7, '[\"9\"]', 'Tsm-king', 'Camilo', 'Humanista', 0, 0, '', '', 47, '2019-03-18', '11:50:34'),
(381, 82, 89, 20761, 'Bárbara', 3, '[\"7\",\"10\"]', 'Barbi ????', 'Bárbara', 'Científica', 0, 0, '', '', 47, '2019-03-18', '12:12:03'),
(382, 82, 89, 20223, 'Danitza', 2, 'null', 'Cabra chica ????', 'Danitza', '', 0, 0, '', 'MÁS ANCHO DE CUERPO', 47, '2019-03-18', '12:14:55'),
(383, 82, 90, 20535, 'Tomás ', 6, 'null', 'Tomás._.aa', 'Tomás A', 'Humanista', 0, 0, '', '', 47, '2019-03-18', '12:22:05'),
(384, 82, 90, 20995, 'Benjamín', 6, 'null', 'Benja ????', 'Benjamín', 'Humanista', 0, 0, '', '', 47, '2019-03-18', '12:25:43'),
(385, 82, 90, 100318, 'Brisa', 3, '[\"9\",\"10\"]', 'Briisa ❣', 'Brisa', 'Científico', 0, 0, '', '', 47, '2019-03-18', '12:27:30'),
(386, 82, 91, 20468, 'Francisco', 5, 'null', 'Pancho (auto de formula)????', 'Francisco', 'Científico', 0, 0, '', '', 47, '2019-03-18', '12:32:44'),
(387, 82, 91, 20882, 'DIego', 6, '[\"1\"]', 'Dicta blanda', 'Diego', 'Científico', 0, 0, '', '', 47, '2019-03-18', '12:36:46'),
(388, 82, 92, 20732, 'Cristian', 4, '[\"3\"]', 'Reus ????', 'Cristian', 'Humanista', 0, 0, '', '', 47, '2019-03-18', '12:47:46'),
(389, 82, 92, 21043, 'Jherico', 6, 'null', 'Durgzza', 'Jherico', 'Humanista', 0, 0, '', '', 47, '2019-03-18', '12:49:47'),
(390, 82, 101, 20219, 'Ariel', 7, 'null', 'Jacinto', 'Ariel', 'Científico', 0, 0, '', '', 47, '2019-03-19', '09:47:48'),
(391, 82, 101, 20971, 'Tomás', 7, 'null', 'Tomate', 'Tomás N', 'Humanista', 0, 0, '', '', 47, '2019-03-19', '09:49:40'),
(392, 82, 102, 20762, 'Sebastián', 5, 'null', 'sb.panther', 'Sebastián', 'Científico', 0, 0, '', '', 47, '2019-03-19', '09:52:23'),
(393, 82, 105, 20819, 'VIcente', 4, 'null', 'Visho (presi) (medalla)', 'Vicente', 'Científico', 0, 0, '', '', 47, '2019-03-19', '10:01:29'),
(394, 82, 116, 20446, 'Lucas', 4, '[\"3\",\"9\"]', 'Alqaeda', 'Lucas', 'Humanista', 0, 0, '', '', 47, '2019-03-19', '10:08:49'),
(395, 82, 101, 20533, 'Byron', 6, 'null', 'Huaso', 'Byron', 'Humanista', 0, 0, '', '', 47, '2019-03-19', '10:17:42'),
(396, 82, 101, 13363, 'Rodrigo', 9, 'null', 'Rodrigo', 'Profesor Rodrigo', 'Científico', 0, 0, '', '', 47, '2019-03-19', '10:48:14'),
(397, 82, 88, 20534, 'Rosario', 6, 'null', 'Shallo', 'Rosario', 'Humanista', 0, 0, '', '', 47, '2019-03-19', '12:30:27'),
(398, 82, 91, 20782, 'Luna', 2, '[\"10\"]', 'Luna ????', 'Luna', 'Humanista', 0, 0, '', '', 47, '2019-03-19', '12:41:15'),
(400, 87, 120, 205106251, 'Hilda', 5, 'null', 'Hilda', 'Hildi', 'Humanisto', 0, 0, '', '', 49, '2019-03-22', '14:39:39'),
(401, 87, 120, 205106254, 'Felipe', 5, 'null', 'Felipe', 'Moris27', 'Humanista', 0, 0, '', '', 49, '2019-03-22', '14:41:45'),
(402, 87, 120, 205106250, 'Diego', 6, 'null', 'Diego', 'Diego Go!', 'Humanista', 0, 0, '', '', 49, '2019-03-22', '14:43:18'),
(403, 87, 120, 205106252, 'Fernanda', 5, 'null', 'Fernanda', 'Shika A.', 'Humanista', 0, 0, '', '', 49, '2019-03-22', '14:49:04'),
(404, 87, 120, 205106253, 'Javiera', 2, 'null', 'Javiera', 'Blanquita Perla', 'Humanista', 0, 0, '', '', 49, '2019-03-22', '14:50:40'),
(405, 87, 120, 205106221, 'Tamara', 2, 'null', 'Tamara', 'Chica Tamy', 'cientifico', 0, 0, '', '', 49, '2019-03-22', '14:51:48'),
(406, 87, 120, 205106222, 'Alison', 2, 'null', 'Alison', 'Princess', 'Cientifico', 0, 0, '', '', 49, '2019-03-22', '14:52:43'),
(407, 87, 120, 205106235, 'Matias', 6, 'null', 'Matias', 'T-fue', 'Humanista', 0, 0, '', '', 49, '2019-03-22', '14:53:38'),
(408, 87, 120, 205106233, 'Kesyi', 2, 'null', 'Javiera', 'H0rmiga', 'Humanista', 0, 0, '', '', 49, '2019-03-22', '14:54:43'),
(409, 88, 171, 20983331, 'valentina farias', 2, 'null', 'Tina', 'Vale', '', 0, 0, '', 'LA I DEL APODO TINA QUE SEA CON UN PEQUEÑO CORAZÓN ARRIBA, EL PUNTITO DE LA I QUE SE REEMPLACE POR UN PEQUEÑO CORAZÓN. LAS MANGAS DEL POLERÓN MILITAR SERAS FUCSIAS.', 50, '2019-03-27', '18:06:08'),
(410, 88, 171, 20372450, 'Bastián Álvarez ', 7, '[\"1\"]', 'Astian', 'Bastián', '', 0, 0, '', 'LAS MANGAS DE POLERÓN MILITAR SERÁN FUCSIAS', 50, '2019-03-27', '18:14:40'),
(411, 88, 171, 20995402, 'Francisca Díaz', 2, 'null', 'Chinita', 'Hopps', '', 0, 0, '', 'LAS MANGAS DEL POLERÓN SERÁN AZULINO', 50, '2019-03-27', '18:21:58'),
(412, 88, 171, 20680032, 'Daniel Carvajal', 7, '[\"3\"]', 'Danny Yankee ', 'Blanquito', '', 0, 0, '', 'LAS MANGAS DEL POLERÓN MILITAR SERÁN FUCSIA', 50, '2019-03-27', '18:28:52'),
(413, 88, 171, 20519685, 'vicente parra', 7, 'null', 'Viceee', 'Viceee', '', 0, 0, '', 'LAS MANGAS DEL POLERÓN SERÁN AZULINO', 50, '2019-03-27', '18:34:04'),
(414, 88, 171, 20679737, 'sara muñoz', 1, 'null', 'Saritax', 'Saritax', '', 0, 0, '', 'LAS MANGAS DEL POLERÓN SERÁN FUCSIA', 50, '2019-03-27', '18:38:05'),
(415, 88, 171, 20637863, 'gabriela jofre', 2, 'null', 'Chica', 'Gabii', '', 0, 0, '', 'LAS MANGAS DEL POLERÓN SERÁN FUCSIAS, EL SÍMBOLO SIMPLE DEL APODO PERSONALIZADO ES SOLO EL CONTORNO DEL CORAZÓN, NO PINTADO, SOLO CONTORNO', 50, '2019-03-27', '18:42:19'),
(416, 88, 171, 9958835, 'lilian diaz', 2, 'null', 'Profe Lilian', 'Mami Lilly', '', 0, 0, '', 'LAS MANGAS DEL POLERÓN SERÁN FUCSIAS', 50, '2019-03-27', '19:03:38'),
(417, 88, 171, 20974307, 'alejandra farias', 3, 'null', 'Aleausente', 'Aleausente', '', 0, 0, '', 'LAS MANGAS DEL POLERÓN MILITAR SERÁN AZULINO', 50, '2019-03-27', '19:06:43'),
(418, 88, 171, 20986731, 'sava zencovich', 1, 'null', 'Sava', 'Zencoperra', '', 0, 0, '', 'LAS MANGAS DEL POLERÓN MILITAR SERÁN FUCSIAS\r\n', 50, '2019-03-27', '19:24:43'),
(419, 88, 171, 20648002, 'Josue miranda', 9, 'null', 'GasperBlaster', 'ArtoriasXD ', '', 0, 0, '', 'LAS MANGAS DEL POLERÓN MILITAR SERÁN AZULINO', 50, '2019-03-27', '19:29:28'),
(420, 88, 171, 20669770, 'Karym Vilches', 1, 'null', 'Karii', 'Karym', '', 0, 0, '', 'LAS MANGAS DEL POLERÓN MILITAR SERÁN FUCSIAS', 50, '2019-03-27', '19:31:21'),
(421, 88, 171, 20398714, 'Esteban Reiman', 7, 'null', '£¤cθ-R3¡MaN', '£¤cθ-R3¡MaN', '', 0, 0, '', 'LAS MANGAS DEL POLERÓN MILITAR SERÁN FUCSIAS', 50, '2019-03-27', '19:33:39'),
(422, 88, 171, 20382400, 'vicento soto', 7, 'null', 'Young Gangster YG', 'Vicho', '', 0, 0, '', 'LAS MANGAS DEL POLERÓN SERÁN AZULINO', 50, '2019-03-27', '19:36:03'),
(423, 88, 172, 20246309, 'Yovanka Arevalo', 2, 'null', 'Yova', 'Arévalo', '', 0, 0, '', 'LAS MANGAS DEL POLERÓN MILITAR SERÁN FUCSIAS, EL SIMBOLO SIMPLE DE TREBOL QUE SEA PINTADO POR DENTRO', 50, '2019-03-27', '19:42:52'),
(424, 88, 172, 20006234, 'sebastian echeverria', 9, 'null', '[sXs] vL Fox ', 'Tatita', '', 0, 0, '', 'LAS MANGAS DEL POLERÓN MILITAR SERÁN FUCSIAS', 50, '2019-03-27', '20:49:22'),
(425, 88, 172, 25077851, 'Fabiana sotillo', 5, 'null', 'munbia', 'munbia', '', 0, 0, '', 'LAS MANGAS DEL POLERÓN MILITAR SERÁN AZULINO', 50, '2019-03-27', '20:51:20'),
(426, 88, 172, 20647504, 'alvaro perez', 7, 'null', 'AlvarIIto', 'Alvaro:P', '', 0, 0, '', 'LAS MANGAS DEL POLERÓN MILITAR SERÁN AZULINO', 50, '2019-03-27', '20:54:08'),
(427, 89, 173, 1, 'Misol Arenas', 2, 'null', 'Misol', 'Pellü', '', 0, 0, '', '', 25, '2019-03-31', '22:05:17'),
(428, 89, 173, 2, 'Carlos Beltrán', 6, 'null', 'Carlos', 'Cacarlitos', '', 0, 0, '', '', 25, '2019-03-31', '22:08:38'),
(429, 89, 173, 3, 'Bianca ErazoBianca Erazo', 6, '[\"10\"]', 'Bianca', 'Binnie', '', 0, 0, '', '', 25, '2019-03-31', '22:09:18'),
(430, 89, 173, 4, 'Geraldyn González', 3, '[\"7\"]', 'Geraldyn ', 'Gerel', '', 0, 0, '', '', 25, '2019-03-31', '22:11:01'),
(431, 89, 173, 5, 'Scarlette Leiva', 3, '[\"6\",\"10\"]', 'Scarlette', 'Ecale', '', 0, 0, '', '', 25, '2019-03-31', '22:15:07'),
(432, 89, 173, 6, 'Dowson Lillo', 4, '[\"10\"]', 'Dowson', 'Perroson', '', 0, 0, '', 'SIN GORRO', 25, '2019-03-31', '22:17:58'),
(433, 89, 173, 7, 'Jeniffer Marimán', 3, 'null', 'Jeniffer', 'Chama', '', 0, 0, '', '', 25, '2019-03-31', '22:26:13'),
(434, 89, 173, 8, 'Ghislane Meza', 5, 'null', 'Ghislane', 'Ichigo', '', 0, 0, '', '', 25, '2019-03-31', '22:38:14'),
(435, 89, 173, 9, 'Dayana Navarro', 3, 'null', 'Dayana', 'Daya', '', 0, 0, '', '', 25, '2019-03-31', '22:42:29'),
(436, 89, 173, 10, 'Kamila Peñaloza', 2, '[\"10\"]', 'Kamila', 'Koneha', '', 0, 0, '', '', 25, '2019-03-31', '22:43:46'),
(437, 89, 173, 11, 'Nicole  Soto', 3, '[\"7\",\"9\"]', 'Nicole', 'Pity', '', 0, 0, '', '', 25, '2019-03-31', '22:46:52'),
(438, 89, 173, 12, 'Ivan Vivanco', 6, 'null', 'Ivan', 'Vivaldi', '', 0, 0, '', 'NO SE PROBO', 25, '2019-03-31', '22:56:18'),
(439, 89, 173, 13, 'Selomit Zuñiga', 2, 'null', 'Selomit', 'Fenia', '', 0, 0, '', '', 25, '2019-03-31', '23:01:14'),
(440, 89, 173, 14, 'Fabiola Ramos', 7, 'null', 'Profesora Fabiola ', 'Faraona', '', 0, 0, '', 'PECHO SIN CURSO REEMPLAZAR POR: GRÁFICA , Y ESPALDA SIN NOMBRES DE ATRÁS', 25, '2019-03-31', '23:05:00'),
(441, 89, 173, 15, 'Melanie Lillo', 7, '[\"4\"]', 'Melanie', '', '', 0, 0, '', '', 25, '2019-03-31', '23:08:18'),
(442, 89, 173, 16, 'Cristian Flores', 6, 'null', 'Cristian', 'Flowers', '', 0, 0, '', '', 25, '2019-03-31', '23:25:54'),
(443, 90, 174, 20679048, 'maria fernanada gascon ', 3, 'null', 'Mª Fernanda', 'Mafer', 'Matematico', 0, 0, '', '', 53, '2019-04-01', '18:01:44'),
(444, 90, 174, 20679047, 'Maria Jesus Gascon ', 5, '[\"3\"]', 'MªJesus ', 'TUTU', 'Biologo', 0, 0, '', '', 53, '2019-04-01', '18:03:48'),
(445, 90, 174, 20829470, 'Millaray', 5, '[\"7\"]', 'Millary', 'Miluu', 'Humanista', 0, 0, '', '', 53, '2019-04-01', '18:08:31'),
(446, 91, 175, 1, 'Jordy Araya', 4, 'null', 'Gio Slim', '@Gio_slim.ch', '', 0, 0, '', '', 25, '2019-04-01', '20:28:02'),
(447, 91, 175, 2, 'Jordán Campos', 8, 'null', 'Jordán', 'El23', '', 0, 0, '', '', 25, '2019-04-01', '21:35:34'),
(448, 91, 175, 3, 'Pía Chávez', 1, 'null', 'Pia_catvlinna', 'Pia_cvtalinna ', '', 0, 0, '', '', 25, '2019-04-01', '21:36:19'),
(449, 91, 175, 4, 'Daeyssy Concha', 4, 'null', 'MonaChica', 'DaeNieves♫', '', 0, 0, '', '', 25, '2019-04-01', '21:37:29'),
(450, 91, 175, 5, 'Fabricio Cruz', 4, 'null', 'Fabricio', 'YiyoM=7', '', 0, 0, '', '', 25, '2019-04-01', '21:39:53'),
(451, 91, 175, 6, 'Karina Crisóstomo', 4, 'null', 'Karina', 'Kari', '', 0, 0, '', '', 25, '2019-04-01', '21:40:31'),
(452, 91, 175, 7, 'Martina Gonzalez', 9, '[\"7\"]', 'Marty', 'MartyAranxa', '', 0, 0, '', '', 25, '2019-04-01', '21:41:26'),
(453, 91, 175, 8, 'Yarita Rojas', 4, 'null', 'Yari', 'Yarita', '', 0, 0, '', '', 25, '2019-04-01', '21:43:50'),
(454, 91, 175, 9, 'Vivi', 3, 'null', 'Duxsick', 'Vivi', '', 0, 0, '', '', 25, '2019-04-01', '21:49:08'),
(455, 91, 175, 10, 'Cristopher Soto', 4, 'null', 'Criss', 'Potito', '', 0, 0, '', '', 25, '2019-04-01', '21:50:05'),
(456, 91, 175, 11, 'Nashky Mancilla', 4, 'null', 'Nash', 'Nashky', '', 0, 0, '', '', 25, '2019-04-01', '21:50:54'),
(457, 91, 175, 12, 'María Muñoz', 2, '[\"5\"]', 'María', 'Mari', '', 0, 0, '', '', 25, '2019-04-01', '21:52:01'),
(458, 91, 175, 13, 'Natalia Silva', 5, 'null', 'Profe Naty', 'Profe Naty', '', 0, 0, '', 'MAS ANCHO DE CADERA', 25, '2019-04-01', '21:53:56'),
(459, 91, 175, 14, 'Javiera Araos', 3, 'null', 'J.avieraa ', 'J.avieraa ', '', 0, 0, '', 'EMOJI CABELLO CAFE', 25, '2019-04-01', '21:55:25'),
(460, 91, 175, 15, 'Elizabeth González', 1, 'null', 'Elizabeth', 'Enanaaa', '', 0, 0, '', '', 25, '2019-04-01', '21:59:25'),
(461, 91, 175, 16, 'Gustavo Erredondo', 6, 'null', 'Gus', 'Gustavo', '', 0, 0, '', '', 25, '2019-04-01', '22:01:01'),
(462, 91, 175, 17, 'Vicente Sandoval', 4, 'null', 'Vicente', 'Vicho', '', 0, 0, '', '', 25, '2019-04-01', '22:02:03'),
(463, 94, 177, 20788308, 'jordan', 6, '[\"9\"]', 'Jordan', 'Ziggy', '', 0, 0, '', '', 54, '2019-04-01', '22:04:58'),
(464, 94, 177, 20969120, 'Francisco', 6, 'null', 'Panchito', 'Panxiio', '', 0, 0, '', '', 54, '2019-04-01', '22:09:19'),
(465, 94, 177, 208381504, 'matias', 4, 'null', 'Mathyas Hidalgo', 'Goofy', '', 0, 0, '', '', 54, '2019-04-01', '22:11:19'),
(466, 94, 177, 21007168, 'daniel', 6, 'null', 'Daniel.C', 'Xoro Dani', '', 0, 0, '', '', 54, '2019-04-01', '22:12:35'),
(467, 94, 177, 20513533, 'jonatan', 6, '[\"9\"]', 'Jona', 'Stifler', '', 0, 0, '', '', 54, '2019-04-01', '22:13:56'),
(468, 94, 177, 21063564, 'kevin', 4, '[\"3\"]', 'Kevin', 'Kelvin', '', 0, 0, '', '', 54, '2019-04-01', '22:15:32'),
(469, 94, 177, 20975345, 'Matias Lizama', 9, 'null', 'M. Lizama', 'Don King', '', 0, 0, '', 'ANCHO DE XXXLB CON EL LARGO DE XXL', 54, '2019-04-01', '22:19:11'),
(470, 94, 177, 21013326, 'Eduardo ', 4, 'null', 'Morita', 'Morita', '', 0, 0, '', '', 54, '2019-04-01', '22:20:20'),
(471, 94, 177, 20824603, 'Camilo', 4, '[\"9\"]', 'Camilo', 'Mandrake', '', 0, 0, '', '', 54, '2019-04-01', '22:21:53'),
(472, 94, 177, 2001, 'Karina', 3, 'null', 'Profe Karina', 'Profe Karina', '', 0, 0, '', '', 54, '2019-04-01', '22:23:21'),
(473, 94, 177, 2002, 'Jennifer', 7, 'null', 'Profe Jenny', 'Profe Jenny', '', 0, 0, '', '', 54, '2019-04-01', '22:24:58'),
(474, 94, 177, 2003, 'Jose', 6, 'null', 'Profe Leo', 'Profe Leo', '', 0, 0, '', '', 54, '2019-04-01', '22:26:09'),
(475, 96, 178, 205106252, 'Danilo', 3, 'null', 'Danilo', 'Danilosky', 'Humanista', 0, 0, '', '', 49, '2019-04-01', '22:34:37'),
(476, 96, 178, 205106623, 'Williams', 6, 'null', 'Williams', 'Willy', 'Humanista', 0, 0, '', '', 49, '2019-04-01', '22:35:38'),
(477, 96, 178, 205156151, 'Javier', 6, 'null', 'Javier', 'The cabezoun', 'Cientifico', 0, 0, '', '', 49, '2019-04-01', '22:36:28'),
(478, 96, 178, 205663452, 'Constanza', 5, 'null', 'Coni', 'Cono', 'Humanista', 0, 0, '', '', 49, '2019-04-01', '22:37:19'),
(479, 96, 178, 205561552, 'Lucia', 3, 'null', 'Lucia', 'Mabel', 'Humanista', 0, 0, '', '', 49, '2019-04-01', '22:38:08'),
(480, 93, 176, 207837687, 'Catalina Bravo', 3, 'null', '+ﾟ★+｡ catalun a｡+ﾟ★ +', 'pisaturno', 'Matemática', 0, 0, '', 'VIVO BOLSILLO', 55, '2019-04-01', '22:38:20');
INSERT INTO `pedido_pedidos_detalle_listado` (`pdlis_codig`, `pedid_codig`, `pdeta_codig`, `pdlis_ndocu`, `pdlis_nombr`, `talla_codig`, `pdlis_tajus`, `pdlis_npers`, `pdlis_aespa`, `pdlis_elect`, `pdlis_timag`, `emoji_codig`, `pdlis_rimag`, `pdlis_obser`, `usuar_codig`, `pdlis_fcrea`, `pdlis_hcrea`) VALUES
(481, 96, 178, 205106254, 'Paula', 5, 'null', 'Paula', 'Delfi', 'Humanista', 0, 0, '', '', 49, '2019-04-01', '22:38:59'),
(482, 96, 178, 205626251, 'Simon', 6, 'null', 'Simon', 'IZAK', 'Cientifico', 0, 0, '', '', 49, '2019-04-01', '22:39:47'),
(483, 96, 178, 205106451, 'Victoria', 2, 'null', 'Victoria', 'Vicky G', 'Humanista', 0, 0, '', '', 49, '2019-04-01', '22:40:47'),
(484, 96, 178, 215106251, 'Cesar', 6, 'null', 'César', 'César', 'Humanista', 0, 0, '', '', 49, '2019-04-01', '22:41:41'),
(485, 96, 178, 230727173, 'Camilo', 3, 'null', 'Camilo', 'Camlin', 'Cientifico', 0, 0, '', '', 49, '2019-04-01', '22:42:32'),
(486, 96, 178, 240985106, 'David', 6, 'null', 'David', 'Rusio', 'Cientifico', 0, 0, '', '', 49, '2019-04-01', '22:43:21'),
(487, 96, 178, 205156221, 'Lisett', 2, 'null', 'Lisett', 'Liz Moris', 'Humanista', 0, 0, '', '', 49, '2019-04-01', '22:44:35'),
(488, 96, 178, 20561234, 'Christian', 6, 'null', 'Christian', 'Kikhan Mendez', 'Cientifico', 0, 0, '', '', 49, '2019-04-01', '22:46:05'),
(489, 93, 176, 209421070, 'Tamara Zunino', 2, 'null', 'Tamara', 'Turrón', 'Humanista', 0, 0, '', '', 55, '2019-04-01', '22:46:47'),
(490, 96, 178, 205106341, 'Esmeralda', 2, 'null', 'Esmeralda', 'Meme', 'Humanista', 0, 0, '', '', 49, '2019-04-01', '22:47:04'),
(491, 96, 178, 20561861, 'Jesús', 6, 'null', 'Jesús', 'Yizus', 'Cientifico', 0, 0, '', '', 49, '2019-04-01', '22:47:46'),
(492, 96, 178, 205168961, 'Martina', 5, 'null', 'Martina', 'Martina', 'Humanista', 0, 0, '', '', 49, '2019-04-01', '22:49:04'),
(493, 93, 176, 208094068, 'Daniel Ureta', 7, 'null', 'Uretita', 'Uretra', 'Humanista', 0, 0, '', '', 55, '2019-04-01', '22:52:05'),
(494, 93, 176, 209372096, 'Martina Silva', 5, 'null', 'mvrtirs', 'Martini', 'Humanista', 0, 0, '', '', 55, '2019-04-01', '22:58:58'),
(495, 93, 176, 20680, 'Amaralina Olguín', 2, '[\"1\"]', '₳mimipi®️', 'Wafflesita', 'Cientìfica', 0, 0, '', '*EMOJI DE ARCOÍRIS ANTES DE SU NOMBRE Y EMOJI DE ESTRELLAS 3 DESPUÉS DE SU NOMBRE.', 55, '2019-04-01', '23:07:11'),
(496, 93, 176, 207841250, 'Millaray Toro', 5, 'null', 'Mily', 'Mallai', 'Científica', 0, 0, '', '', 55, '2019-04-01', '23:09:50'),
(497, 94, 179, 20677920, 'marcelo', 9, 'null', 'Misa ', 'Loco Sami', '', 0, 0, '', '', 54, '2019-04-01', '23:13:56'),
(498, 93, 176, 20886100, 'Felipe Troncoso', 3, 'null', 'Negro', 'Canuto Masisi', 'Humanista', 0, 0, '', '', 55, '2019-04-01', '23:14:28'),
(499, 93, 176, 210734341, 'Lucas Bueno', 6, 'null', 'cv3z0n', 'Caeza', 'Matemático', 0, 0, '', '', 55, '2019-04-01', '23:19:47'),
(500, 94, 179, 20426828, 'francisco', 9, '[\"9\"]', 'Francisco', 'Pancho', '', 0, 0, '', '', 54, '2019-04-01', '23:20:21'),
(501, 93, 176, 210035184, 'Manuel Droguette', 4, 'null', 'ElManuletos Sánchez', 'Manu Manito', 'Humanista', 0, 0, '', '', 55, '2019-04-01', '23:22:08'),
(502, 94, 180, 200829189, 'matias arias', 4, 'null', 'Matti', 'Dj Mati', '', 0, 0, '', '', 54, '2019-04-01', '23:23:50'),
(503, 94, 180, 20945194, 'francisco', 6, '[\"5\"]', 'El Neculpan', 'Neutron', '', 0, 0, '', '', 54, '2019-04-01', '23:24:48'),
(504, 94, 181, 20849490, 'Matias', 6, '[\"9\"]', 'Matias', 'Ay Veraa', '', 0, 0, '', '', 54, '2019-04-01', '23:26:47'),
(505, 93, 176, 210064133, 'Danae Osorio', 2, '[\"10\"]', 'Danae', 'Danae Peralta', 'Humanista', 0, 0, '', '', 55, '2019-04-01', '23:31:03'),
(506, 94, 182, 21042817, 'lucas', 4, '[\"1\"]', 'Luckas', 'Fome', '', 0, 0, '', '', 54, '2019-04-01', '23:33:15'),
(507, 94, 182, 20819136, 'Paz', 3, '[\"1\",\"7\"]', 'Pazita', 'Churry', '', 0, 0, '', '', 54, '2019-04-01', '23:34:13'),
(508, 93, 176, 20922142, 'Francisco Segovia', 7, 'null', 'Sego', 'Expreso de Cueto', 'Científico', 0, 0, '', '', 55, '2019-04-01', '23:35:21'),
(509, 94, 182, 20991132, 'Sergio', 4, '[\"9\"]', 'S. Gómez', 'Serxio', '', 0, 0, '', '', 54, '2019-04-01', '23:35:37'),
(510, 94, 182, 14, 'Tomas ', 6, '[\"5\"]', 'Tomás', 'Toma-más', '', 0, 0, '', '', 54, '2019-04-01', '23:37:08'),
(511, 93, 176, 207832871, 'Andrea Suárez', 3, 'null', 'Andrea', 'Power Andrea', 'Humanista', 0, 0, '', '', 55, '2019-04-01', '23:37:46'),
(512, 94, 182, 21020871, 'felipe', 6, '[\"10\"]', 'Felipe', 'Gringo', '', 0, 0, '', '', 54, '2019-04-01', '23:38:44'),
(513, 94, 182, 23165648, 'abraham', 4, 'null', 'Abraham', 'Trauco', '', 0, 0, '', '', 54, '2019-04-01', '23:39:42'),
(514, 94, 182, 27, 'german', 6, 'null', 'German', 'Tatita', '', 0, 0, '', '', 54, '2019-04-01', '23:41:03'),
(515, 94, 182, 29, 'Rodrigo', 4, '[\"10\"]', 'Negro0t3', 'Negrote', '', 0, 0, '', '', 54, '2019-04-01', '23:42:24'),
(516, 94, 182, 30, 'Cristobal', 4, 'null', 'Cris Royce', 'Bartolo', '', 0, 0, '', '', 54, '2019-04-01', '23:43:08'),
(517, 94, 182, 31, 'matias', 4, '[\"9\"]', 'Matias', 'Chiky', '', 0, 0, '', '', 54, '2019-04-01', '23:44:02'),
(518, 94, 185, 8, 'alexis', 4, 'null', 'Alexis', 'Cannabis', '', 0, 0, '', '', 54, '2019-04-01', '23:45:56'),
(519, 94, 185, 9, 'sebastian', 4, 'null', 'Seba :)', 'Cba', '', 0, 0, '', '', 54, '2019-04-01', '23:47:04'),
(521, 94, 185, 32, 'edwin', 4, '[\"9\"]', 'Elver Galarga', 'Mono', '', 0, 0, '', '', 54, '2019-04-01', '23:52:32'),
(522, 94, 189, 1, 'Matias', 6, 'null', 'Mati', 'Chino Negro', '', 0, 0, '', '', 54, '2019-04-01', '23:53:31'),
(523, 94, 189, 22, 'Jordan', 9, 'null', 'Jordan', 'Huaso', '', 0, 0, '', '', 54, '2019-04-01', '23:54:16'),
(524, 94, 189, 28, 'DIego', 4, 'null', 'Dieguito', 'Dieguin Bombin', '', 0, 0, '', '', 54, '2019-04-01', '23:54:57'),
(525, 94, 190, 19, 'leandro', 4, '[\"9\"]', 'Leito', 'Mcfly', '', 0, 0, '', '', 54, '2019-04-01', '23:56:41'),
(526, 94, 190, 18, 'elena', 3, '[\"1\",\"7\"]', 'Gelena', 'Jelen', '', 0, 0, '', '', 54, '2019-04-01', '23:57:25'),
(527, 93, 176, 209665190, 'Catalina Yáñez', 2, 'null', 'Catita', 'Topo', 'Científico ', 0, 0, '', 'VIVO BOLSILLO', 55, '2019-04-02', '08:37:38'),
(528, 93, 176, 208266837, 'Francisca Valdivia', 2, 'null', 'Polillita', 'Valdiviana', 'Humanista', 0, 0, '', 'VIVO BOLSILLO', 55, '2019-04-02', '08:41:31'),
(529, 93, 176, 208192604, 'Damián Soto', 6, '[\"1\"]', 'El_Daminih', 'Cagón', 'Matemático ', 0, 0, '', 'VIVO EN BOLSILLO', 55, '2019-04-02', '08:43:45'),
(530, 93, 176, 12, 'Ignacio García', 6, 'null', 'Pnacho', 'Cockys', 'Matemático ', 0, 0, '', '', 55, '2019-04-02', '08:47:49'),
(531, 93, 191, 209998750, 'Vicente Venegas', 6, 'null', 'Vicente Ariel', 'Super-Man', 'Matemático ', 0, 0, '', '', 55, '2019-04-02', '08:51:14'),
(532, 93, 191, 20787, 'Cristóbal García ', 6, 'null', 'Baby', 'Jack-Jack', 'Matemático ', 0, 0, '', '', 55, '2019-04-02', '08:55:27'),
(533, 93, 192, 207225568, 'Lourdes Barría', 5, 'null', 'Loulita', 'Loulinda', 'Humanista ', 0, 0, '', '', 55, '2019-04-02', '08:58:43'),
(534, 93, 192, 8, 'Yénifer Fernández', 3, 'null', 'yeni', 'Marley', 'Humanista', 0, 0, '', '', 55, '2019-04-02', '09:01:46'),
(535, 93, 192, 9, 'Fernando Flores', 7, 'null', 'F', 'Faña', 'Matemático ', 0, 0, '', '', 55, '2019-04-02', '09:03:14'),
(536, 93, 192, 210255591, 'Paula González ', 1, 'null', 'Pauli', 'Pauleee', 'Científico ', 0, 0, '', '', 55, '2019-04-02', '10:12:48'),
(537, 93, 192, 203916770, 'Amanda Godoy', 3, 'null', 'Amanda', 'Cállate Amanda', 'Humanista', 0, 0, '', 'VIVO BOLSILLO', 55, '2019-04-02', '10:14:45'),
(538, 93, 192, 16, 'Vicente Lecaros', 6, '[\"2\"]', 'Èl Lécàróh', 'do Macaco', 'Humanista', 0, 0, '', '', 55, '2019-04-02', '10:17:07'),
(539, 93, 192, 22, 'Jonás Santander', 5, 'null', 'pelao', 'Jonanto', 'Matemático ', 0, 0, '', 'VIVO BOLSILLO', 55, '2019-04-02', '10:18:21'),
(540, 93, 192, 208339060, 'Denisse Carvalho', 2, 'null', 'Niss', 'Fresita', 'Científico ', 0, 0, '', '', 55, '2019-04-02', '10:29:29'),
(541, 93, 192, 39, 'Profesora Ana María Saavedra', 5, 'null', 'Anita Mami', 'Chusma', 'Científico ', 0, 0, '', '*ESTE POLERÓN TIENE 50% DCTO. Y EL NOMBRE DE LA ESPALDA VA ARRIBA DE LA FRASE DEL POLERÓN ', 55, '2019-04-02', '10:37:28'),
(542, 93, 193, 209888467, 'Mauricio Balmaceda ', 4, 'null', 'Mauri', 'Balmcgregor', 'Matemático ', 0, 0, '', '', 55, '2019-04-02', '10:42:54'),
(543, 93, 193, 209409275, 'Josep Miranda', 6, 'null', 'xm_josep_xg', 'Las Primas', 'Científico ', 0, 0, '', '*UN EMOJI VA AL PRINCIPIO DE SU NOMBRE Y EL OTRO AL FINAL.', 55, '2019-04-02', '10:51:58'),
(544, 93, 193, 206802871, 'Benjamín Rubio', 6, 'null', 'rubi0ski', 'mr_pidboy', 'Científico ', 0, 0, '', '', 55, '2019-04-02', '10:55:52'),
(545, 93, 193, 209928809, 'Anaís Sarmiento', 2, 'null', 'Ani', 'Maní', 'Humanista ', 0, 0, '', '', 55, '2019-04-02', '10:57:14'),
(546, 93, 193, 204514895, 'Javiera Valenzuela', 2, '[\"1\"]', 'xinita', 'Tchonona bb', 'Científico ', 0, 0, '', '', 55, '2019-04-02', '10:59:16'),
(547, 93, 193, 209510111, 'Nicolás Cortés', 6, 'null', 'blau.beetle', 'gepegepegepe', 'Científico ', 0, 0, '', '', 55, '2019-04-02', '11:00:34'),
(548, 93, 194, 208808265, 'Felipe Valencia', 9, 'null', 'Felix', 'Pipito', 'Científico ', 0, 0, '', '', 55, '2019-04-02', '11:03:25'),
(549, 93, 195, 205815147, 'Bayron González', 7, 'null', 'simplemente bayron', 'Baalt15', 'Científico ', 0, 0, '', '*EL PRIMER EMOJI VA AL INICIO DEL NOMBRE Y EL SEGUNDO AL FINAL.', 55, '2019-04-02', '11:07:00'),
(550, 93, 195, 203371012, 'Constanza Sanhueza', 1, 'null', 'Conny', 'honey girl', 'Científico ', 0, 0, '', '', 55, '2019-04-02', '11:13:15'),
(551, 75, 80, 11, 'giobany', 6, 'null', 'Hobbit', 'Hobbit', '', 0, 0, '', 'SIMBOLO SIMPLE PINGUINO', 25, '2019-04-02', '17:37:14'),
(552, 98, 196, 250784171, 'Carlos Agreda', 6, 'null', 'Pishii', 'Carlitos', 'Científico', 0, 0, '', '', 56, '2019-04-03', '09:45:12'),
(553, 98, 196, 20535333, 'Javiera Alcapia', 3, 'null', 'Negraa', 'Javiera', 'Científico', 0, 0, '', '', 56, '2019-04-03', '09:47:19'),
(554, 98, 196, 207861499, 'Sergio Carrasco', 9, 'null', 'Shergiok', 'Sergio', 'Científico', 0, 0, '', '', 56, '2019-04-03', '09:48:32'),
(555, 98, 196, 253485124, 'Lucero Chavi', 3, '[\"\"]', 'Lucerito', 'Lucero', 'Científico', 0, 0, '', 'EMOJI AL LADO DEL APODO', 56, '2019-04-03', '09:51:37'),
(556, 98, 196, 25082478, 'Luddy Chorez', 4, 'null', 'Negrito', 'Gustavo', 'Científico', 0, 0, '', 'MANGA AJUSTADA', 56, '2019-04-03', '09:54:53'),
(557, 98, 196, 206312874, 'Julián Irrazabal', 7, '[\"9\"]', 'Julián', 'Julián', 'Científico', 0, 0, '', '', 56, '2019-04-03', '09:56:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_pedidos_imagen`
--

CREATE TABLE `pedido_pedidos_imagen` (
  `pimag_codig` int(11) NOT NULL,
  `pedid_codig` int(11) NOT NULL,
  `pimag_orden` int(11) NOT NULL,
  `pimag_descr` varchar(50) NOT NULL,
  `pimag_tbord` text NOT NULL,
  `ptima_codig` int(11) NOT NULL,
  `pimag_cimag` int(11) NOT NULL,
  `pimag_ruta` text NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `pimag_fcrea` date NOT NULL,
  `pimag_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pedido_pedidos_imagen`
--

INSERT INTO `pedido_pedidos_imagen` (`pimag_codig`, `pedid_codig`, `pimag_orden`, `pimag_descr`, `pimag_tbord`, `ptima_codig`, `pimag_cimag`, `pimag_ruta`, `usuar_codig`, `pimag_fcrea`, `pimag_hcrea`) VALUES
(48, 39, 5, 'MANGA DERECHA', '', 5, 0, 'files/detalle/pedido//39/20190129_213806_mride.jpeg', 23, '2019-01-29', '21:38:06'),
(49, 39, 7, 'IMAGEN PRINCIPAL', '', 5, 0, 'files/detalle/pedido//39/20190205_102431_iprut.png', 23, '2019-01-29', '21:56:40'),
(50, 39, 8, 'IMAGEN FRONTAL', '', 5, 0, 'files/detalle/pedido//39/20190205_102431_rifro.jpg', 23, '2019-01-29', '21:56:40'),
(51, 39, 9, 'IMAGEN ESPALDA', '', 5, 0, 'files/detalle/pedido//39/20190205_102431_riesp.png', 23, '2019-01-29', '21:56:40'),
(52, 47, 7, 'IMAGEN PRINCIPAL', '', 5, 0, 'files/detalle/pedido//47/20190221_130239_iprut.png', 22, '2019-02-06', '10:50:28'),
(53, 47, 8, 'IMAGEN FRONTAL', '', 5, 0, 'files/detalle/pedido//47/20190221_130239_rifro.png', 22, '2019-02-06', '10:50:28'),
(54, 47, 9, 'IMAGEN ESPALDA', '', 5, 0, 'files/detalle/pedido//47/20190221_130239_riesp.png', 22, '2019-02-06', '10:50:28'),
(55, 48, 7, 'IMAGEN PRINCIPAL', '', 5, 0, 'files/detalle/pedido//48/20190222_113811_iprut.png', 25, '2019-02-06', '17:13:44'),
(56, 48, 8, 'IMAGEN FRONTAL', '', 5, 0, 'files/detalle/pedido//48/20190222_113811_rifro.jpg', 25, '2019-02-06', '17:13:44'),
(57, 48, 9, 'IMAGEN ESPALDA', '', 5, 0, 'files/detalle/pedido//48/20190222_113811_riesp.jpg', 25, '2019-02-06', '17:13:44'),
(67, 58, 7, 'IMAGEN PRINCIPAL', '', 5, 0, 'files/detalle/pedido//58/20190208_201230_iprut.jpeg', 27, '2019-02-08', '20:10:40'),
(68, 58, 8, 'IMAGEN FRONTAL', '', 5, 0, 'files/detalle/pedido//58/20190208_201230_rifro.jpeg', 27, '2019-02-08', '20:10:40'),
(69, 58, 9, 'IMAGEN ESPALDA', '', 5, 0, 'files/detalle/pedido//58/20190208_201230_riesp.jpeg', 27, '2019-02-08', '20:10:40'),
(70, 61, 7, 'IMAGEN PRINCIPAL', '', 5, 0, 'files/detalle/pedido//61/20190211_033329_iprut.jpeg', 27, '2019-02-11', '03:33:29'),
(71, 61, 8, 'IMAGEN FRONTAL', '', 5, 0, 'files/detalle/pedido//61/20190211_033329_rifro.jpeg', 27, '2019-02-11', '03:33:29'),
(72, 61, 9, 'IMAGEN ESPALDA', '', 5, 0, 'files/detalle/pedido//61/20190211_033329_riesp.jpeg', 27, '2019-02-11', '03:33:29'),
(73, 62, 7, 'IMAGEN PRINCIPAL', '', 5, 0, 'files/detalle/pedido//62/20190211_034843_iprut.jpeg', 27, '2019-02-11', '03:48:43'),
(74, 62, 8, 'IMAGEN FRONTAL', '', 5, 0, 'files/detalle/pedido//62/20190211_034843_rifro.jpeg', 27, '2019-02-11', '03:48:43'),
(75, 62, 9, 'IMAGEN ESPALDA', '', 5, 0, 'files/detalle/pedido//62/20190211_034843_riesp.jpeg', 27, '2019-02-11', '03:48:43'),
(76, 72, 7, 'IMAGEN PRINCIPAL', '', 5, 0, 'files/detalle/pedido//72/20190401_130839_iprut.jpg', 29, '2019-02-14', '17:36:30'),
(77, 72, 8, 'IMAGEN FRONTAL', '', 5, 0, 'files/detalle/pedido//72/20190401_130839_rifro.jpg', 29, '2019-02-14', '17:36:30'),
(78, 72, 9, 'IMAGEN ESPALDA', '', 5, 0, 'files/detalle/pedido//72/20190401_130839_riesp.jpg', 29, '2019-02-14', '17:36:30'),
(79, 73, 7, 'IMAGEN PRINCIPAL', '', 5, 0, 'files/detalle/pedido//73/20190219_203500_iprut.jpg', 33, '2019-02-19', '20:35:00'),
(80, 73, 8, 'IMAGEN FRONTAL', '', 5, 0, 'files/detalle/pedido//73/20190219_203500_rifro.jpeg', 33, '2019-02-19', '20:35:00'),
(81, 73, 9, 'IMAGEN ESPALDA', '', 5, 0, 'files/detalle/pedido//73/20190219_203500_riesp.jpeg', 33, '2019-02-19', '20:35:00'),
(106, 75, 6, 'MANGA IZQUIERDA', '', 5, 0, 'files/detalle/pedido//75/20190305_122104_mriiz.jpg', 3, '2019-03-05', '12:11:44'),
(107, 75, 7, 'IMAGEN PRINCIPAL', '', 5, 0, 'files/detalle/pedido//75/20190308_101545_iprut.jpeg', 25, '2019-03-07', '19:19:46'),
(108, 75, 8, 'IMAGEN FRONTAL', '', 5, 0, 'files/detalle/pedido//75/20190308_101545_rifro.jpeg', 25, '2019-03-07', '19:19:46'),
(109, 75, 9, 'IMAGEN ESPALDA', '', 5, 0, 'files/detalle/pedido//75/20190308_101545_riesp.jpeg', 25, '2019-03-07', '19:19:46'),
(111, 76, 5, 'MANGA DERECHA', '', 5, 0, 'files/detalle/pedido//76/20190314_172834_mride.jpg', 40, '2019-03-14', '17:28:34'),
(112, 76, 7, 'IMAGEN PRINCIPAL', '', 5, 0, 'files/detalle/pedido//76/20190314_180512_iprut.jpeg', 40, '2019-03-14', '18:05:12'),
(113, 76, 8, 'IMAGEN FRONTAL', '', 5, 0, 'files/detalle/pedido//76/20190314_180512_rifro.jpg', 40, '2019-03-14', '18:05:12'),
(114, 76, 9, 'IMAGEN ESPALDA', '', 5, 0, 'files/detalle/pedido//76/20190314_180512_riesp.jpg', 40, '2019-03-14', '18:05:12'),
(120, 82, 7, 'CIENTIFICO', '(EN FORMA CIRCULAR)', 5, 0, 'files/detalle/listado/20190403_135201_0.png', 47, '2019-03-19', '14:45:10'),
(121, 87, 2, 'PECHO IZQUIERDA', '', 5, 0, 'files/detalle/pedido//87/20190322_145841_priiz.jpg', 49, '2019-03-22', '14:58:41'),
(122, 87, 8, 'IMAGEN FRONTAL', '', 5, 0, 'files/detalle/pedido//87/20190322_151010_rifro.jpg', 49, '2019-03-22', '15:10:10'),
(123, 87, 9, 'IMAGEN ESPALDA', '', 5, 0, 'files/detalle/pedido//87/20190402_182921_riesp.png', 49, '2019-03-22', '15:10:10'),
(124, 88, 7, 'IMAGEN PRINCIPAL', '', 5, 0, 'files/detalle/pedido//88/20190328_103752_iprut.jpg', 50, '2019-03-28', '10:37:52'),
(125, 88, 8, 'IMAGEN FRONTAL', '', 5, 0, 'files/detalle/pedido//88/20190328_103752_rifro.PNG', 50, '2019-03-28', '10:37:52'),
(126, 88, 9, 'IMAGEN ESPALDA', '', 5, 0, 'files/detalle/pedido//88/20190328_103752_riesp.jpg', 50, '2019-03-28', '10:37:52'),
(127, 89, 2, 'PECHO IZQUIERDA', '', 5, 0, 'files/detalle/pedido//89/20190331_231735_priiz.png', 25, '2019-03-31', '23:17:35'),
(128, 89, 5, 'MANGA DERECHA', '', 5, 0, 'files/detalle/pedido//89/20190331_231735_mride.png', 25, '2019-03-31', '23:17:35'),
(129, 89, 6, 'MANGA IZQUIERDA', '', 5, 0, 'files/detalle/pedido//89/20190331_231735_mriiz.png', 25, '2019-03-31', '23:17:35'),
(130, 89, 7, 'IMAGEN PRINCIPAL', '', 5, 0, 'files/detalle/pedido//89/20190331_232026_iprut.png', 25, '2019-03-31', '23:20:26'),
(131, 89, 8, 'IMAGEN FRONTAL', '', 5, 0, 'files/detalle/pedido//89/20190331_232026_rifro.png', 25, '2019-03-31', '23:20:26'),
(132, 89, 9, 'IMAGEN ESPALDA', '', 5, 0, 'files/detalle/pedido//89/20190331_232026_riesp.png', 25, '2019-03-31', '23:20:26'),
(133, 72, 2, 'PECHO IZQUIERDA', '', 5, 0, 'files/detalle/pedido//72/20190401_132702_priiz.jpg', 25, '2019-04-01', '00:02:12'),
(134, 72, 5, 'MANGA DERECHA', '', 5, 0, 'files/detalle/pedido//72/20190402_222544_mride.jpeg', 25, '2019-04-01', '00:02:12'),
(136, 91, 7, 'IMAGEN PRINCIPAL', '', 5, 0, 'files/detalle/pedido//91/20190401_224139_iprut.png', 25, '2019-04-01', '22:41:39'),
(137, 91, 8, 'IMAGEN FRONTAL', '', 5, 0, 'files/detalle/pedido//91/20190401_224139_rifro.png', 25, '2019-04-01', '22:41:39'),
(138, 91, 9, 'IMAGEN ESPALDA', '', 5, 0, 'files/detalle/pedido//91/20190401_224139_riesp.png', 25, '2019-04-01', '22:41:39'),
(139, 96, 2, 'PECHO IZQUIERDA', '', 5, 0, 'files/detalle/pedido//96/20190401_225438_priiz.jpg', 49, '2019-04-01', '22:54:38'),
(140, 94, 7, 'IMAGEN PRINCIPAL', '', 5, 0, 'files/detalle/pedido//94/20190401_230709_iprut.jpg', 54, '2019-04-01', '23:07:09'),
(141, 93, 7, 'IMAGEN PRINCIPAL', '', 5, 0, 'files/detalle/pedido//93/20190402_114640_iprut.jpeg', 55, '2019-04-02', '11:46:40'),
(142, 96, 7, 'IMAGEN PRINCIPAL', '', 5, 0, 'files/detalle/pedido//96/20190402_202309_iprut.png', 25, '2019-04-02', '20:23:09'),
(143, 96, 9, 'IMAGEN ESPALDA', '', 5, 0, 'files/detalle/pedido//96/20190402_202309_riesp.png', 25, '2019-04-02', '20:23:09'),
(144, 93, 5, 'MANGA DERECHA', '', 5, 0, 'files/detalle/pedido//93/20190402_224920_mride.jpeg', 25, '2019-04-02', '22:49:20'),
(145, 93, 8, 'IMAGEN FRONTAL', '', 5, 0, 'files/detalle/pedido//93/20190402_225454_rifro.jpeg', 25, '2019-04-02', '22:54:54'),
(146, 93, 9, 'IMAGEN ESPALDA', '', 5, 0, 'files/detalle/pedido//93/20190402_230038_riesp.png', 25, '2019-04-02', '23:00:38'),
(171, 82, 11, 'HUMANISTA', '(EN FORMA CIRCULAR)', 5, 12, 'files/detalle/listado/20190403_143549_12.png', 3, '2019-04-03', '13:56:48'),
(172, 93, 10, 'CIENTIFICO', '', 5, 10, '', 3, '2019-04-04', '12:05:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_pedidos_imagen_tipo`
--

CREATE TABLE `pedido_pedidos_imagen_tipo` (
  `ptima_codig` int(11) NOT NULL,
  `ptima_descr` varchar(50) NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `ptima_fcrea` date NOT NULL,
  `ptima_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pedido_pedidos_imagen_tipo`
--

INSERT INTO `pedido_pedidos_imagen_tipo` (`ptima_codig`, `ptima_descr`, `usuar_codig`, `ptima_fcrea`, `ptima_hcrea`) VALUES
(1, 'EMOJI', 3, '2019-01-09', '22:43:26'),
(2, 'IMAGEN PERSONALIZADA', 3, '2019-01-09', '22:45:21'),
(3, 'SIMBOLO SIMPLE', 3, '2019-01-09', '22:45:38'),
(4, 'SIN IMAGEN', 3, '2019-01-09', '22:46:04'),
(5, 'OTRA', 3, '2019-01-09', '22:46:25'),
(6, 'ELECTIVO', 3, '2019-01-16', '15:04:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_precio`
--

CREATE TABLE `pedido_precio` (
  `pprec_codig` int(11) NOT NULL,
  `pprec_descr` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `pprec_preci` double NOT NULL,
  `pprec_adici` double NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `pprec_fcrea` date NOT NULL,
  `pprec_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Volcado de datos para la tabla `pedido_precio`
--

INSERT INTO `pedido_precio` (`pprec_codig`, `pprec_descr`, `pprec_preci`, `pprec_adici`, `usuar_codig`, `pprec_fcrea`, `pprec_hcrea`) VALUES
(1, 'EMOJI', 3500, 1000, 3, '2018-11-20', '11:20:37'),
(2, 'IMAGEN LIBRE PERSONALIZADA', 6000, 6000, 3, '2018-11-20', '11:21:34'),
(3, 'SIMBOLO SIMPLE', 0, 0, 3, '2018-11-27', '18:29:26'),
(4, 'SIN IMAGEN', 0, 0, 3, '2018-12-03', '16:12:23'),
(5, 'PALABRA O FRASE', 2000, 2000, 3, '2019-01-16', '21:26:28'),
(7, 'INSIGNIA', 1500, 0, 22, '2019-03-14', '10:34:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_precio_electivo`
--

CREATE TABLE `pedido_precio_electivo` (
  `pelec_codig` int(11) NOT NULL,
  `pelec_descr` varchar(50) NOT NULL,
  `pelec_preci` double NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `pelec_fcrea` date NOT NULL,
  `pelec_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pedido_precio_electivo`
--

INSERT INTO `pedido_precio_electivo` (`pelec_codig`, `pelec_descr`, `pelec_preci`, `usuar_codig`, `pelec_fcrea`, `pelec_hcrea`) VALUES
(1, 'ELECTIVO', 2500, 3, '2019-01-17', '13:37:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_simbolo_simple`
--

CREATE TABLE `pedido_simbolo_simple` (
  `ssimp_codig` int(11) NOT NULL,
  `ssimp_descr` varchar(50) NOT NULL,
  `ssimp_ruta` text NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `ssimp_fcrea` date NOT NULL,
  `ssimp_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pedido_simbolo_simple`
--

INSERT INTO `pedido_simbolo_simple` (`ssimp_codig`, `ssimp_descr`, `ssimp_ruta`, `usuar_codig`, `ssimp_fcrea`, `ssimp_hcrea`) VALUES
(2, 'BASQUET', 'files/simple/20190128_231220.jpg', 22, '2019-01-28', '23:12:20'),
(3, 'BOCA', 'files/simple/20190128_231238.jpg', 22, '2019-01-28', '23:12:38'),
(4, 'CALAVERA', 'files/simple/20190128_231303.jpg', 22, '2019-01-28', '23:13:03'),
(5, 'CORAZON', 'files/simple/20190128_231324.jpg', 22, '2019-01-28', '23:13:24'),
(6, 'CORONA', 'files/simple/20190128_231349.jpg', 22, '2019-01-28', '23:13:49'),
(7, 'DIAMANTE', 'files/simple/20190128_231410.jpg', 22, '2019-01-28', '23:14:10'),
(8, 'ESTRELLA', 'files/simple/20190128_231430.jpg', 22, '2019-01-28', '23:14:30'),
(9, 'ESTRELLAS', 'files/simple/20190128_231453.jpg', 22, '2019-01-28', '23:14:53'),
(10, 'FLOR', 'files/simple/20190128_231513.jpg', 22, '2019-01-28', '23:15:13'),
(11, 'FUTBOL', 'files/simple/20190128_231536.jpg', 22, '2019-01-28', '23:15:36'),
(12, 'MANO', 'files/simple/20190128_231557.jpg', 22, '2019-01-28', '23:15:57'),
(13, 'MANO1', 'files/simple/20190128_231618.jpg', 22, '2019-01-28', '23:16:18'),
(14, 'MONO', 'files/simple/20190128_231638.jpg', 22, '2019-01-28', '23:16:38'),
(15, 'MUSCULO', 'files/simple/20190128_231658.jpg', 22, '2019-01-28', '23:16:58'),
(16, 'NOTAS', 'files/simple/20190128_231717.jpg', 22, '2019-01-28', '23:17:17'),
(17, 'PATITAS', 'files/simple/20190128_231739.jpg', 22, '2019-01-28', '23:17:39'),
(18, 'POLLO', 'files/simple/20190128_231801.jpg', 22, '2019-01-28', '23:18:01'),
(19, 'RATON', 'files/simple/20190128_231826.jpg', 22, '2019-01-28', '23:18:26'),
(20, 'RAYO', 'files/simple/20190128_231848.jpg', 22, '2019-01-28', '23:18:48'),
(21, 'SHOP', 'files/simple/20190128_231908.jpg', 22, '2019-01-28', '23:19:08'),
(22, 'TREBOL', 'files/simple/20190128_231932.jpg', 22, '2019-01-28', '23:19:32'),
(23, 'YINGYANG', 'files/simple/20190128_231952.jpg', 22, '2019-01-28', '23:19:52'),
(24, 'CARA LENTES SOL', 'files/simple/20190304_145012.png', 25, '2019-03-04', '14:50:12'),
(25, 'FANTASMA', 'files/simple/20190304_151612.png', 25, '2019-03-04', '15:16:12'),
(26, 'RISA NERVIOSA', 'files/simple/20190304_152120.jpg', 25, '2019-03-04', '15:21:20'),
(27, 'LUNA', 'files/simple/20190401_225948.png', 25, '2019-04-01', '22:59:48'),
(28, 'FLOR DE LIZ', 'files/simple/20190402_121737.png', 25, '2019-04-02', '12:17:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_talla`
--

CREATE TABLE `pedido_talla` (
  `talla_codig` int(11) NOT NULL,
  `talla_descr` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `talla_fcrea` date NOT NULL,
  `talla_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Volcado de datos para la tabla `pedido_talla`
--

INSERT INTO `pedido_talla` (`talla_codig`, `talla_descr`, `usuar_codig`, `talla_fcrea`, `talla_hcrea`) VALUES
(1, 'XS', 3, '2018-12-03', '09:26:01'),
(2, 'S', 3, '2018-12-03', '09:27:57'),
(3, 'M', 3, '2018-12-03', '09:27:57'),
(4, 'MB', 3, '2018-12-03', '09:27:57'),
(5, 'L', 3, '2018-12-03', '09:27:57'),
(6, 'LB', 3, '2018-12-03', '09:27:57'),
(7, 'XL', 3, '2018-12-03', '09:27:57'),
(8, 'XLB', 3, '2018-12-03', '09:27:57'),
(9, 'XXL', 3, '2018-12-03', '09:27:57'),
(14, 'LD', 3, '2019-02-05', '15:14:24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido_talla_ajuste`
--

CREATE TABLE `pedido_talla_ajuste` (
  `tajus_codig` int(11) NOT NULL,
  `tajus_descr` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `tajus_padre` int(11) NOT NULL,
  `tajus_super` int(11) NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `tajus_fcrea` date NOT NULL,
  `tajus_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Volcado de datos para la tabla `pedido_talla_ajuste`
--

INSERT INTO `pedido_talla_ajuste` (`tajus_codig`, `tajus_descr`, `tajus_padre`, `tajus_super`, `usuar_codig`, `tajus_fcrea`, `tajus_hcrea`) VALUES
(1, '+1 PUÑO', 11, 0, 0, '0000-00-00', '00:00:00'),
(2, '+2 PUÑO', 11, 0, 0, '0000-00-00', '00:00:00'),
(3, '-1 PUÑO', 11, 0, 0, '0000-00-00', '00:00:00'),
(4, '-2 PUÑO', 11, 0, 0, '0000-00-00', '00:00:00'),
(5, '+1 PRETINA', 12, 0, 0, '0000-00-00', '00:00:00'),
(6, '+2 PRETINA', 12, 0, 0, '0000-00-00', '00:00:00'),
(7, '-1 PRETINA', 12, 0, 0, '0000-00-00', '00:00:00'),
(8, '-2 PRETINA', 12, 0, 0, '0000-00-00', '00:00:00'),
(9, 'AJUSTADO', 13, 0, 0, '0000-00-00', '00:00:00'),
(10, 'ACINTURADO', 13, 0, 0, '0000-00-00', '00:00:00'),
(11, 'LARGO DE MANGAS', 0, 1, 0, '0000-00-00', '00:00:00'),
(12, 'LARGO DE CUERPO', 0, 1, 0, '0000-00-00', '00:00:00'),
(13, 'CUERPO', 0, 1, 0, '0000-00-00', '00:00:00'),
(14, 'SIN AJUSTE DE TALLA', 0, 0, 0, '0000-00-00', '00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE `permisos` (
  `permi_codig` bigint(20) UNSIGNED NOT NULL,
  `modul_codig` int(10) NOT NULL,
  `urole_codig` int(11) NOT NULL,
  `permi_bperm` tinyint(1) NOT NULL,
  `permi_bborr` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`permi_codig`, `modul_codig`, `urole_codig`, `permi_bperm`, `permi_bborr`) VALUES
(8, 1, 1, 1, 0),
(9, 100, 1, 1, 0),
(10, 101, 1, 1, 0),
(11, 102, 1, 1, 0),
(12, 103, 1, 1, 0),
(13, 104, 1, 1, 1),
(14, 105, 1, 1, 0),
(16, 107, 1, 1, 0),
(19, 110, 1, 1, 0),
(21, 900, 1, 1, 0),
(22, 112, 1, 1, 0),
(23, 200, 1, 0, 1),
(24, 300, 1, 1, 0),
(25, 400, 1, 0, 1),
(26, 600, 1, 0, 1),
(27, 700, 1, 1, 0),
(28, 701, 1, 1, 0),
(29, 800, 1, 0, 1),
(30, 801, 1, 0, 1),
(31, 702, 1, 1, 0),
(32, 802, 1, 0, 1),
(33, 703, 1, 0, 1),
(34, 201, 1, 1, 0),
(35, 202, 1, 1, 0),
(36, 203, 1, 1, 0),
(37, 500, 1, 0, 1),
(38, 113, 1, 1, 0),
(39, 2, 1, 1, 0),
(40, 114, 1, 1, 0),
(41, 115, 1, 1, 0),
(42, 116, 1, 0, 1),
(43, 1, 2, 1, 0),
(44, 1, 3, 1, 0),
(47, 1000, 1, 1, 0),
(48, 1001, 1, 1, 0),
(49, 1002, 1, 1, 0),
(50, 1003, 1, 1, 0),
(51, 1004, 1, 0, 1),
(52, 1005, 1, 1, 0),
(53, 1006, 1, 1, 0),
(54, 1007, 1, 1, 0),
(55, 3, 1, 1, 0),
(56, 4, 1, 1, 0),
(57, 1008, 1, 1, 0),
(58, 1009, 1, 1, 0),
(59, 2, 3, 1, 0),
(60, 4, 3, 1, 0),
(62, 2, 2, 1, 0),
(63, 4, 2, 1, 0),
(64, 900, 2, 1, 0),
(65, 900, 3, 1, 0),
(66, 1100, 1, 1, 0),
(67, 1200, 1, 1, 0),
(68, 117, 1, 1, 0),
(69, 118, 1, 1, 0),
(70, 902, 1, 1, 0),
(71, 903, 1, 1, 0),
(72, 119, 1, 1, 0),
(73, 902, 3, 1, 0),
(74, 903, 3, 1, 0),
(75, 120, 1, 1, 0),
(76, 905, 1, 1, 0),
(77, 121, 1, 1, 0),
(78, 1200, 3, 1, 0),
(79, 1010, 1, 1, 0),
(80, 1011, 1, 1, 0),
(81, 1012, 1, 1, 0),
(82, 1013, 1, 1, 0),
(83, 704, 1, 1, 0),
(84, 122, 1, 1, 0),
(85, 123, 1, 1, 0),
(86, 124, 1, 1, 0),
(87, 9000, 1, 1, 0),
(88, 125, 1, 1, 0),
(89, 126, 1, 1, 0),
(90, 127, 1, 1, 0),
(91, 128, 1, 1, 0),
(92, 8000, 1, 1, 0),
(93, 8002, 1, 1, 0),
(94, 8003, 1, 1, 0),
(95, 8004, 1, 1, 0),
(96, 8001, 1, 1, 0),
(97, 8005, 1, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos_estatus`
--

CREATE TABLE `permisos_estatus` (
  `eperm_codig` int(11) NOT NULL,
  `eperm_descr` varchar(50) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Volcado de datos para la tabla `permisos_estatus`
--

INSERT INTO `permisos_estatus` (`eperm_codig`, `eperm_descr`) VALUES
(1, 'ACTIVO'),
(0, 'INACTIVO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `perso_codig` int(11) NOT NULL,
  `nacio_value` varchar(1) NOT NULL,
  `perso_cedul` bigint(50) NOT NULL,
  `perso_pnomb` varchar(20) NOT NULL,
  `perso_snomb` varchar(20) DEFAULT NULL,
  `perso_papel` varchar(20) NOT NULL,
  `perso_sapel` varchar(20) DEFAULT NULL,
  `perso_fnaci` date NOT NULL,
  `gener_value` varchar(1) NOT NULL,
  `ecivi_value` varchar(45) NOT NULL,
  `perso_obser` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`perso_codig`, `nacio_value`, `perso_cedul`, `perso_pnomb`, `perso_snomb`, `perso_papel`, `perso_sapel`, `perso_fnaci`, `gener_value`, `ecivi_value`, `perso_obser`) VALUES
(1, 'V', 24900845, 'KEVIN', 'ALEJANDRO', 'FIGUERA', 'RODRIGUEZ', '1994-10-03', 'M', 'S', 'KEVIN'),
(19, 'V', 13882284, 'IVEY', 'EMILIA', 'MONTOYA', 'DE MARQUEZ', '1977-05-01', 'F', 'C', 'NINGUNA'),
(35, '', 24900846, 'LUZ', 'VERONICA', 'HERNANDEZ', 'HERNANDEZ', '1981-03-22', '2', '', ''),
(36, '', 24900847, 'CONTANZA', 'MILLARAY', 'LARA', 'HERNANDEZ', '2001-10-30', '2', '', ''),
(37, '', 24900848, 'KEVIN', 'KEVIN', 'FIGUERA', 'FIGUERA', '0000-00-00', '', '', 'A'),
(38, 'V', 1, 'MATIAS', '', 'CASSANO', '', '2019-01-30', 'M', 'S', ''),
(39, 'E', 243688841, 'CARLOS', 'MARIO', 'URRUTIA', '', '1985-06-29', 'M', 'S', ''),
(40, '', 243688842, 'FERNANDA', 'JAVIERA ', 'MARTINEZ ', 'MORENO ', '2001-08-14', '2', '', ''),
(41, '', 243688843, 'YOVANA ', 'ANDREA ', 'MORENO ', 'GONZALEZ ', '1974-10-31', '2', '', ''),
(42, '', 243688844, 'ISIDORA', '', 'RETAMAL', '', '2019-02-13', '2', '', ''),
(43, '', 243688845, 'CAROLINA', '', 'ZUÑIGA', '', '2019-02-03', '2', '', ''),
(44, '', 243688846, 'MARÍA PAZ', '', 'BENAVIDES', 'MARTÍNEZ', '2002-03-11', '2', '', ''),
(45, '', 243688847, 'CONSTANZA', 'BELÉN', 'RÍOS', 'VIVAR', '2001-04-09', '2', '', ''),
(49, '', 243688848, 'VIKY', '', 'MUNIZAGA', '', '2001-09-13', '2', '', ''),
(52, '', 243688849, 'CARLOS', 'IGNACIO', 'ESPINA', 'GALVEZ', '2190-02-28', '1', '', ''),
(53, '', 243688850, '4RTGHN', '3RFB', '4RTG', '', '2019-03-06', '1', '', ''),
(54, '', 243688851, 'ISIDORA', '', 'RAMÍREZ', '', '0001-09-19', '2', '', ''),
(57, '', 243688852, 'JEIMY', '', 'HENRÍQUEZ', '', '2019-02-27', '2', '', ''),
(58, '', 243688853, 'MARÍA', 'FERNANDA', 'GONZÁLEZ ', 'DURÁN', '2019-02-27', '2', '', ''),
(59, '', 243688854, 'MARLA ', '', 'MONDACA', '', '2019-02-27', '2', '', ''),
(60, '', 243688855, 'VALENTINA', 'MILLARAY', 'VALDIVIA ', 'MÉNDEZ ', '2019-02-27', '2', '', ''),
(61, '', 243688856, 'DANIELA', 'ESCARLETT', 'ERICES', 'VASQUEZ', '2019-01-01', '2', '', ''),
(62, '', 243688857, 'CONSTANZA', 'ALEJANDRA', 'SOTO', 'HEREDIA', '2002-01-05', '2', '', ''),
(63, '', 243688858, 'CONSTANZA ', 'DANAE ', 'GUTIÉRREZ ', 'MARTÍNEZ ', '0000-00-00', '', '', 'COLEGIO ALTO PALENA '),
(64, '', 243688859, 'PABLO', 'NICOLAS', 'FLORES', 'ARCE ', '0000-00-00', '', '', 'CONFEDERACIÓN SUIZA '),
(65, '', 243688860, 'CONSTANZA', '', 'GUTIERREZ', '', '2002-04-05', '2', '', ''),
(66, '', 243688861, 'VALENTINA', 'ESTEFANIA', 'CARRASCO', 'SOTO', '0000-00-00', '', '', 'LICEO NACIONAL DE LLOLLEO'),
(67, '', 243688862, 'PRUEBA', '', 'PRUEBA', '', '0000-00-00', '', '', 'PRUEBA'),
(68, '', 243688863, 'TRABAJADOR', 'KEVIN', 'FIGUERA', 'A', '0000-00-00', '', '', 'PRUEBA'),
(69, '', 243688864, 'TRABAJADOR', 'KEVIN', 'FIGUERA', '', '0000-00-00', '', '', 'AAA'),
(70, '', 243688865, 'RICARDO', '', 'BRIONES ', 'REIG', '1993-02-02', '1', '', ''),
(71, '', 243688866, 'SORAYA', 'DAYHANA', 'ROA', 'FIGUEROA', '1981-03-14', '2', '', ''),
(74, '', 243688867, 'DIEGO', 'HERNAN', 'FIGUEROA', 'GALLARDO', '2001-04-25', '1', '', ''),
(75, '', 243688868, 'BARBARA', 'AYLIN', 'DURÁN', '', '2019-03-16', '2', '', ''),
(76, '', 243688869, 'CATALINA', 'IGNACIA', 'YAÑEZ', 'MONTIEL', '2001-07-18', '2', '', ''),
(77, '', 243688870, 'HILDA', '', 'VALENZUELA', '', '2002-01-03', '2', '', ''),
(78, '', 243688871, 'VALENTINA', 'PAZ', 'FARIAS', 'MUÑOZ', '2002-03-07', '2', '', ''),
(80, '', 243688872, 'NORA', 'ISABEL', 'MUÑOZ', 'MUÑOZ', '1977-11-06', '2', '', ''),
(81, '', 243688873, 'KARLA', 'DATH', 'SILVA', 'MENDEZ', '0000-00-00', '', '', 'COLEGIO PIAMARTA '),
(83, '', 243688874, 'SCARLETTE ', '', 'LEIVA ', '', '2019-04-09', '2', '', ''),
(84, '', 243688875, 'GERALDYN ', '', 'GONZÁLEZ', '', '2017-04-11', '2', '', ''),
(85, '', 243688876, 'MARIA', 'PAMELA', 'MADRID', 'HIDALGO', '1974-01-04', '2', '', ''),
(87, '', 243688877, 'CAROLINA', 'VANIA', 'BELLO', 'PEREZ', '1974-11-27', '2', '', ''),
(88, '', 243688878, 'ELENA', 'IGNACIA', 'HUENCHULEO', 'CASTRO', '2002-05-17', '2', '', ''),
(89, '', 243688879, 'CATALINA', 'ANTONIA', 'BRAVO', ']PIUTRÍN', '2001-07-01', '2', '', ''),
(91, '', 243688880, 'PAZ', 'CAROLINA', 'GATICA', 'LOPEZ', '2001-12-07', '2', '', ''),
(92, '', 243688881, 'ANDREA', 'ELIZABETH', 'PIUTRÍN', 'PIZARRO', '1982-09-10', '2', '', ''),
(93, '', 243688882, 'CLAUDIO', 'ANDRÉS', 'SALGADO', 'RICE', '1992-03-05', '1', '', ''),
(94, '', 243688883, 'DAEYSSY ', '', 'CONCHA', '', '2019-04-02', '2', '', ''),
(95, '', 243688884, 'MARTINA', '', 'GONZÁLEZ', '', '2019-04-02', '2', '', ''),
(97, '', 243688885, 'SEGUNDO ', '', 'CONTACTO', '', '2019-01-16', '2', '', ''),
(98, '', 243688886, 'TIRZO', 'ALEXANDRO', 'BRUNETTI', 'LABRÍN', '1964-07-14', '1', '', ''),
(100, 'V', 123456789, 'AAAA', 'AAAA', 'AAA', 'AAAA', '2019-06-26', 'M', 'S', 'A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto_tipo`
--

CREATE TABLE `producto_tipo` (
  `tprod_codig` int(11) NOT NULL,
  `tprod_descr` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `producto_tipo`
--

INSERT INTO `producto_tipo` (`tprod_codig`, `tprod_descr`) VALUES
(1, 'PRODUCTOS'),
(2, 'SERVICIOS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `prove_codig` int(11) NOT NULL,
  `tclie_codig` int(11) NOT NULL,
  `tdocu_codig` int(11) NOT NULL,
  `prove_ndocu` varchar(20) NOT NULL,
  `prove_denom` varchar(200) NOT NULL,
  `prove_ccont` varchar(20) NOT NULL,
  `prove_corre` varchar(50) NOT NULL,
  `prove_telef` varchar(15) NOT NULL,
  `prove_direc` text NOT NULL,
  `prove_obser` text NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `prove_fcrea` date NOT NULL,
  `prove_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`prove_codig`, `tclie_codig`, `tdocu_codig`, `prove_ndocu`, `prove_denom`, `prove_ccont`, `prove_corre`, `prove_telef`, `prove_direc`, `prove_obser`, `usuar_codig`, `prove_fcrea`, `prove_hcrea`) VALUES
(3, 1, 1, '123456', 'KEVIN FIGUERA', '12345', 'A@A.A', '12345', '1234', '', 3, '2018-01-22', '08:13:57'),
(4, 1, 1, '24900845', 'KEVIN FIGUERA', '', 'KEVINALEJANDRO3@GMAIL.COM', '4241941881', 'MONTALBAN', '', 3, '2018-08-01', '19:36:54'),
(5, 1, 1, '13882284', 'IVEY MONTOYA', '', 'IVEYMONTOYA@GMAIL.COM', '5555555555', 'CALLE NEGRIN CON GARCIA LOS ALAMOS ', 'OTRO BANCO', 3, '2018-08-07', '22:13:40'),
(6, 1, 1, '24321991', 'KARLA NADALES', '', 'KARLANADALES12@GMAIL.COM', '04141111111', 'BARINAS', '2', 3, '2018-08-08', '05:24:10'),
(9, 1, 1, '18390585', 'DANNY MARIN', '', 'DMARIN@GMAIL.COM', '02121112222', 'A', '1', 3, '2018-08-08', '05:27:45'),
(19, 2, 4, '309386310', 'JVKA REDES Y SISTEMAS CA', '', 'JVKACA@GMAIL.COM', '0421941881', 'CARACAS VENEZUELA', 'PRUEBA 2', 3, '2018-08-14', '19:41:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor_tipo`
--

CREATE TABLE `proveedor_tipo` (
  `tprov_codig` int(11) NOT NULL,
  `tprov_descr` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `proveedor_tipo`
--

INSERT INTO `proveedor_tipo` (`tprov_codig`, `tprov_descr`) VALUES
(1, 'NATURAL'),
(2, 'JURIDICO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rango_categoria`
--

CREATE TABLE `rango_categoria` (
  `ranca_codig` int(11) NOT NULL,
  `categ_codig` int(11) NOT NULL,
  `categ_desde` varchar(10) NOT NULL,
  `categ_hasta` varchar(10) NOT NULL,
  `parti_inici` int(11) NOT NULL,
  `parti_final` int(11) NOT NULL,
  `parti_actua` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `rango_categoria`
--

INSERT INTO `rango_categoria` (`ranca_codig`, `categ_codig`, `categ_desde`, `categ_hasta`, `parti_inici`, `parti_final`, `parti_actua`) VALUES
(1, 1, '20', '23', 201, 250, 209),
(2, 2, '15', '17', 101, 200, 149),
(3, 3, '8', '11', 1, 100, 73),
(4, 3, '12', '14', 1, 100, 73),
(7, 4, '30', '34', 251, 99999, 272),
(8, 4, '35', '39', 251, 99999, 272),
(9, 4, '40', '44', 251, 99999, 272),
(10, 4, '45', '49', 251, 99999, 272),
(11, 4, '50', '54', 251, 99999, 272),
(12, 4, '55', '59', 251, 99999, 272),
(13, 4, '60', '999', 251, 99999, 272),
(14, 5, '24', '999', 201, 250, 209),
(15, 6, '18', '19', 201, 250, 209);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rd_preregistro`
--

CREATE TABLE `rd_preregistro` (
  `prere_codig` int(11) NOT NULL,
  `prere_numer` varchar(50) NOT NULL,
  `prere_tdcom` int(11) NOT NULL,
  `prere_rifco` varchar(50) NOT NULL,
  `prere_rsoci` varchar(200) NOT NULL,
  `prere_nfant` varchar(200) NOT NULL,
  `acome_codig` int(11) NOT NULL,
  `prere_tmovi` varchar(50) NOT NULL,
  `prere_tfijo` varchar(50) NOT NULL,
  `estad_codig` int(11) NOT NULL,
  `munic_codig` int(11) NOT NULL,
  `parro_codig` int(11) NOT NULL,
  `ciuda_codig` int(11) NOT NULL,
  `prere_celec` varchar(50) NOT NULL,
  `prere_dfisc` text NOT NULL,
  `banco_codig` int(11) NOT NULL,
  `prere_cafil` varchar(50) NOT NULL,
  `prere_ncuen` varchar(100) NOT NULL,
  `prere_fsoli` date NOT NULL,
  `orige_codig` int(11) NOT NULL,
  `prere_clvip` int(11) NOT NULL,
  `prere_obser` text NOT NULL,
  `estat_codig` int(11) NOT NULL,
  `prere_pasos` int(11) NOT NULL,
  `prere_accio` int(11) NOT NULL,
  `prere_rmoti` int(11) NOT NULL,
  `prere_robse` text NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `prere_fcrea` date NOT NULL,
  `prere_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `rd_preregistro`
--

INSERT INTO `rd_preregistro` (`prere_codig`, `prere_numer`, `prere_tdcom`, `prere_rifco`, `prere_rsoci`, `prere_nfant`, `acome_codig`, `prere_tmovi`, `prere_tfijo`, `estad_codig`, `munic_codig`, `parro_codig`, `ciuda_codig`, `prere_celec`, `prere_dfisc`, `banco_codig`, `prere_cafil`, `prere_ncuen`, `prere_fsoli`, `orige_codig`, `prere_clvip`, `prere_obser`, `estat_codig`, `prere_pasos`, `prere_accio`, `prere_rmoti`, `prere_robse`, `usuar_codig`, `prere_fcrea`, `prere_hcrea`) VALUES
(1, '201906-0000000001', 0, 'J-38630131-0', 'JVKA REDES Y SISTEMAS', 'SMARTWEBTOOLS', 1, '0424-194.18.81', '4241-941.88.1', 24, 462, 1117, 149, 'KEVINALEJANDRO3@GMAIL.COM', 'MONTABAN', 1, '00001', '01340000000000000000', '2019-06-25', 2, 2, 'NINGUNA', 99, 5, 2, 1, 'NINGUNA', 3, '2019-06-24', '22:42:57'),
(2, '201906-0000000002', 0, '', '', '', 0, '', '', 0, 0, 0, 0, '', '', 0, '', '', '0000-00-00', 0, 0, '', 1, 1, 0, 0, '', 3, '2019-06-25', '17:05:54');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rd_preregistro_configuracion_equipo`
--

CREATE TABLE `rd_preregistro_configuracion_equipo` (
  `cequi_codig` int(11) NOT NULL,
  `prere_codig` int(11) NOT NULL,
  `otele_codig` int(11) NOT NULL,
  `cequi_canti` int(11) NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `cequi_fcrea` date NOT NULL,
  `cequi_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `rd_preregistro_configuracion_equipo`
--

INSERT INTO `rd_preregistro_configuracion_equipo` (`cequi_codig`, `prere_codig`, `otele_codig`, `cequi_canti`, `usuar_codig`, `cequi_fcrea`, `cequi_hcrea`) VALUES
(2, 1, 1, 5, 3, '2019-06-25', '00:43:13'),
(3, 1, 2, 10, 3, '2019-06-25', '00:45:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rd_preregistro_documento_digital`
--

CREATE TABLE `rd_preregistro_documento_digital` (
  `docum_codig` int(11) NOT NULL,
  `prere_codig` int(11) NOT NULL,
  `dtipo_codig` int(11) NOT NULL,
  `docum_ruta` text NOT NULL,
  `docum_orden` int(11) NOT NULL,
  `usuar_codig` date NOT NULL,
  `docum_fcrea` date NOT NULL,
  `docum_hcrea` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `rd_preregistro_documento_digital`
--

INSERT INTO `rd_preregistro_documento_digital` (`docum_codig`, `prere_codig`, `dtipo_codig`, `docum_ruta`, `docum_orden`, `usuar_codig`, `docum_fcrea`, `docum_hcrea`) VALUES
(2, 1, 1, 'files/solicitudes/201906-0000000001/20190626_000354_0.pdf', 0, '0000-00-00', '2019-06-26', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rd_preregistro_documento_digital_tipo`
--

CREATE TABLE `rd_preregistro_documento_digital_tipo` (
  `dtipo_codig` int(11) NOT NULL,
  `dtipo_descr` varchar(100) NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `dtipo_fcrea` date NOT NULL,
  `dtipo_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `rd_preregistro_documento_digital_tipo`
--

INSERT INTO `rd_preregistro_documento_digital_tipo` (`dtipo_codig`, `dtipo_descr`, `usuar_codig`, `dtipo_fcrea`, `dtipo_hcrea`) VALUES
(1, 'Planilla de Solicitud ', 3, '2019-06-25', '18:06:52'),
(2, 'Documento Constitutivo', 3, '2019-06-25', '18:06:52'),
(3, 'Estado de Cuenta', 3, '2019-06-25', '18:07:32'),
(4, 'RIF del Comercio', 3, '2019-06-25', '18:07:32'),
(5, 'Cédula/Pasaporte', 3, '2019-06-25', '18:08:05'),
(6, 'RIF de los Representantes Legales', 3, '2019-06-25', '18:08:05'),
(7, 'Contrato', 3, '2019-06-25', '18:08:17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rd_preregistro_representante_legal`
--

CREATE TABLE `rd_preregistro_representante_legal` (
  `rlega_codig` int(11) NOT NULL,
  `prere_codig` int(11) NOT NULL,
  `rlega_tdrif` varchar(50) NOT NULL,
  `rlega_rifrl` varchar(50) NOT NULL,
  `rlega_nombr` varchar(50) NOT NULL,
  `rlega_apell` varchar(50) NOT NULL,
  `tdocu_codig` int(11) NOT NULL,
  `rlega_ndocu` varchar(50) NOT NULL,
  `rlega_cargo` varchar(100) NOT NULL,
  `rlega_tmovi` varchar(50) NOT NULL,
  `rlega_tfijo` varchar(50) NOT NULL,
  `rlega_corre` varchar(100) NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `rlega_fcrea` date NOT NULL,
  `rlega_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `rd_preregistro_representante_legal`
--

INSERT INTO `rd_preregistro_representante_legal` (`rlega_codig`, `prere_codig`, `rlega_tdrif`, `rlega_rifrl`, `rlega_nombr`, `rlega_apell`, `tdocu_codig`, `rlega_ndocu`, `rlega_cargo`, `rlega_tmovi`, `rlega_tfijo`, `rlega_corre`, `usuar_codig`, `rlega_fcrea`, `rlega_hcrea`) VALUES
(10, 1, '', 'v-24900845-8', 'Kevin', 'Figuera', 1, '24900845', 'presidente', '0424-194.18.81', '0424-194.18.81', 'kevinalejandro3@gmail.com', 3, '2019-06-25', '18:05:31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registro`
--

CREATE TABLE `registro` (
  `regis_codig` int(11) NOT NULL,
  `coleg_codig` int(11) NOT NULL,
  `regis_direc` text COLLATE utf8mb4_bin NOT NULL,
  `regis_telef` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `regis_pgweb` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `regis_rede1` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `regis_rede2` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `regis_rede3` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `cont1_ndocu` int(11) NOT NULL,
  `cont1_pnomb` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `cont1_snomb` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `cont1_papel` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `cont1_sapel` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `cont1_corre` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `cont1_tofic` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `cont1_tcelu` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `cont1_fnaci` date DEFAULT NULL,
  `cont1_gener` int(11) NOT NULL,
  `cont1_curso` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `cont2_ndocu` int(11) NOT NULL,
  `cont2_pnomb` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `cont2_snomb` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `cont2_papel` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `cont2_sapel` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `cont2_corre` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `cont2_tofic` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `cont2_tcelu` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `cont2_fnaci` date DEFAULT NULL,
  `cont2_gener` int(11) NOT NULL,
  `cont2_curso` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `eregi_codig` int(11) NOT NULL,
  `regis_obser` text COLLATE utf8mb4_bin NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `regis_fcrea` date NOT NULL,
  `regis_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Volcado de datos para la tabla `registro`
--

INSERT INTO `registro` (`regis_codig`, `coleg_codig`, `regis_direc`, `regis_telef`, `regis_pgweb`, `regis_rede1`, `regis_rede2`, `regis_rede3`, `cont1_ndocu`, `cont1_pnomb`, `cont1_snomb`, `cont1_papel`, `cont1_sapel`, `cont1_corre`, `cont1_tofic`, `cont1_tcelu`, `cont1_fnaci`, `cont1_gener`, `cont1_curso`, `cont2_ndocu`, `cont2_pnomb`, `cont2_snomb`, `cont2_papel`, `cont2_sapel`, `cont2_corre`, `cont2_tofic`, `cont2_tcelu`, `cont2_fnaci`, `cont2_gener`, `cont2_curso`, `eregi_codig`, `regis_obser`, `usuar_codig`, `regis_fcrea`, `regis_hcrea`) VALUES
(1, 287, 'LA PINTANA, SANTA ROSA', ' +56 2 2542 2102', 'WWW.COLEGIOIBEROAMERICANO.CL/', '', '', '', 24900846, 'LUZ', 'VERONICA', 'HERNANDEZ', 'HERNANDEZ', 'matiascassano@gmail.com', '', '+56983702264', '1981-03-22', 2, 'MAMA', 0, 'CONTANZA', 'MILLARAY', 'LARA', 'HERNANDEZ', 'CPNTANZAMILLARAYLARAHERNANDEZ@GMAIL.COM', '', '+56976152650', '2001-10-30', 2, '4B', 3, 'IBEROAMERICANO 4B', 23, '2019-01-28', '15:01:18'),
(2, 0, '', '', '', '', '', '', 24900848, 'KEVIN', 'KEVIN', 'FIGUERA', 'FIGUERA', 'ke.vinalejandro3@gmail.com', '', '+5604241941881', NULL, 0, '', 0, '', '', '', '', '', '', '', NULL, 0, '', 1, 'A', 24, '2019-01-30', '14:02:28'),
(3, 291, 'WALKER MARTÍNEZ 1106, LA FLORIDA, REGIÓN METROPOLITANA, CHILE', '+56 2 2255 1657', 'WWW.ALCANTARA-ALICANTE.CL', '', '', '', 243688842, 'FERNANDA', 'JAVIERA ', 'MARTINEZ ', 'MORENO ', 'fer1408@icloud.com', '', '+56956669021', '2001-08-14', 2, '4MEDIOA', 0, 'YOVANA ', 'ANDREA ', 'MORENO ', 'GONZALEZ ', 'YOVIMO.FE@GMAIL.COM', '', '+56973825005', '1974-10-31', 2, '4MEDIOA', 3, 'ALCANTARA DE LA CORDILLERA ', 27, '2019-02-07', '15:52:44'),
(4, 288, ' PUENTE ALTO, REGIÓN METROPOLITANA, CHILE', '+56 2 2268 4132', 'WWW.COLEGIOSANCARLOS.CL', '', '', '', 243688844, 'ISIDORA', '', 'RETAMAL', '', 'camilocassano@gmail.com', '', '+56990074272', '2019-02-13', 2, 'IV D', 0, 'CAROLINA', '', 'ZUÑIGA', '', 'MATIASCASSANO@GMAIL.COM', '', '+56978925590', '2019-02-03', 2, 'IV D', 3, '', 28, '2019-02-08', '16:14:53'),
(5, 290, 'EL MOLLE Nº1390', '35-2481057', '', '', '', '', 243688846, 'MARÍA PAZ', '', 'BENAVIDES', 'MARTÍNEZ', 'mpbenavides31@gmail.com', '', '+56990074255', '2002-03-11', 2, '4 MEDIO', 0, 'CONSTANZA', 'BELÉN', 'RÍOS', 'VIVAR', 'CONSTANZA.RIOS.V@GMAIL.COM', '', '+56983436141', '2001-04-09', 2, '4 MEDIO', 3, 'COLEGIO TERESA DE LOS ANDES, ALGARROBO', 29, '2019-02-09', '14:54:43'),
(9, 294, 'PEDRO DE VALDIVIA 1939', '974598338', 'WWW.CPDV.CL', '', '', '', 243688848, 'VIKY', '', 'MUNIZAGA', '', 'vikymunizaga@gmail.com', '', '+56974598338', '2001-09-13', 2, 'IV', 0, 'ISIDORA', '', 'RAMÍREZ', '', 'ISIMAUNA@GMAIL.COM', '', '+56994995503', '0001-09-19', 2, 'IV', 3, 'COLEGIO PEDRO DE VALDIVIA PROVIDENCIA', 33, '2019-02-18', '16:02:43'),
(10, 2, '12345', '2345', '12345', '12345', '1234', '12344', 243688849, 'CARLOS', 'IGNACIO', 'ESPINA', 'GALVEZ', 'maticassano@outlook.com', '', '+567895452', '2190-02-28', 1, 'HGFR', 0, '4RTGHN', '3RFB', '4RTG', '', '3ERFGB@G.COM', '', '+563', '2019-03-06', 1, '456', 3, 'M', 34, '2019-02-19', '14:19:49'),
(13, 296, 'SANTIAGO, LA CISTERNA, REGIÓN METROPOLITANA, CHILE', '+56 2 2596 7809', 'HTTP://POLITECNICOSANRAMON.CL/', '', 'HTTPS://WWW.FACEBOOK.COM/CENTRO.RAMON', '', 243688852, 'JEIMY', '', 'HENRÍQUEZ', '', 'jeimyhenriquezolivares1@gmail.com', '', '+56945820617', '2019-02-27', 2, 'CURSO	4 QUÍMICA INDUSTRIAL ', 0, 'MARLA ', '', 'MONDACA', '', 'JEIMYHENRIQUEZOLIVARES1@GMAIL.COM', '', '+56986516630', '2019-02-27', 2, 'CURSO	4 QUÍMICA INDUSTRIAL ', 3, 'CENTRO POLITÉCNICO PARTICULAR SAN RAMÓN', 37, '2019-02-27', '12:14:11'),
(14, 297, 'GABRIELA PTE 662, PUENTE ALTO, REGIÓN METROPOLITANA, CHILE', '+56 2 2369 9238', 'HTTP://WWW.CSMC.CL/', '', '', '', 243688853, 'MARÍA', 'FERNANDA', 'GONZÁLEZ ', 'DURÁN', 'mgonzalezduran59@gmail.com', '', '+56950789821', '2019-02-27', 2, ' 3ERO MEDIO B ', 0, 'VALENTINA', 'MILLARAY', 'VALDIVIA ', 'MÉNDEZ ', 'MGONZALEZDURAN59@GMAIL.COM', '', '+56950166580', '2019-02-27', 2, ' 3ERO MEDIO B ', 3, ' SANTA MARÍA DE LA CORDILLERA', 38, '2019-02-27', '12:15:41'),
(15, 298, 'EL MEDIANERO 02081, PUENTE ALTO, REGIÓN METROPOLITANA', '228744791', 'HTTP://WWW.CORPORACIONEDUCSANMATEO.COM/', '', '', '', 243688856, 'DANIELA', 'ESCARLETT', 'ERICES', 'VASQUEZ', 'de9757641@gmail.com', '', '+56954725434', '2019-01-01', 2, '4TO A', 0, 'SEGUNDO ', '', 'CONTACTO', '', 'DE9757641@GMAIL.COM', '', '+56954725434', '2019-01-16', 2, '4TO A', 3, 'SAN MATEO', 39, '2019-03-04', '14:24:14'),
(16, 57, 'MAIPÚ #85', '2 2682 0443', '', '', '', '', 243688857, 'CONSTANZA', 'ALEJANDRA', 'SOTO', 'HEREDIA', 'constanzaalejandra.sh@gmail.com', '', '+56989803693', '2002-01-05', 2, '4A', 0, 'CONSTANZA', '', 'GUTIERREZ', '', 'M.EVELYNHEREDIA@GMAIL.COM', '', '+569', '2002-04-05', 2, '4A', 3, 'COLEGIO ALTO PALENA', 40, '2019-03-11', '13:43:05'),
(17, 0, '', '', '', '', '', '', 243688858, 'CONSTANZA ', 'DANAE ', 'GUTIÉRREZ ', 'MARTÍNEZ ', 'konygutierrezmartinez54@gmail.com', '', '+56979106005', NULL, 0, '', 0, '', '', '', '', '', '', '', NULL, 0, '', 1, 'COLEGIO ALTO PALENA ', 41, '2019-03-11', '15:24:08'),
(23, 303, 'COLEGIO SAN JOSÉ AV. DON ORIONE 7357', '962250264', 'WWW.COLEGIOSANJOSECERRILLOS.CL', '', '', '', 243688865, 'RICARDO', '', 'BRIONES ', 'REIG', 'ricardo.breig@gmail.com', '', '+56962250264', '1993-02-02', 1, '4 ', 0, 'BARBARA', 'AYLIN', 'DURÁN', '', 'RICARDO.BREIG@GMAIL.COM', '', '+56956600662', '2019-03-16', 2, '4', 3, 'RICARDO', 47, '2019-03-14', '14:57:45'),
(24, 300, 'PUERTO VARAS', '(65) 2 515054', 'HTTP://WWW.COLEGIOALTASCUMBRESPV.CL/', '', '', '', 243688866, 'SORAYA', 'DAYHANA', 'ROA', 'FIGUEROA', 'sorayaroa@hotmail.com', '', '+56995559987', '1981-03-14', 2, '4° MEDIO', 0, 'CATALINA', 'IGNACIA', 'YAÑEZ', 'MONTIEL', 'CAATAALINA23@GMAIL.COM', '', '+56941793578', '2001-07-18', 2, '4° MEDIO', 3, 'COLEGIO ALTAS CUMBRES PUERTO VARAS', 48, '2019-03-14', '15:48:32'),
(25, 304, 'MAULE CL, CALLE BOMBERO GARRIDO 1173-B, CURICÓ, REGIÓN DEL MAULE', '75231 5321', '', '', '', '', 243688867, 'DIEGO', 'HERNAN', 'FIGUEROA', 'GALLARDO', 'dfigueroagallardo@gmail.com', '', '+560977436745', '2001-04-25', 1, '4 MEDIO', 0, 'HILDA', '', 'VALENZUELA', '', 'DIEGOHERNANFIGUEROAGALLARSO@GMAIL.COM', '', '+56976981525', '2002-01-03', 2, '4 MEDIO', 3, 'CENTRO EDUCATIVO PEUMAYEN', 49, '2019-03-15', '09:09:36'),
(26, 306, 'AV. ECHEÑIQUE 8625, LA REINA, REGIÓN METROPOLITANA', '+562291243 00', 'WWW.CORP-LAREINA.CL/LICEO-EUGENIO-MARIA-DE-HOSTOS/', '', '', '', 243688871, 'VALENTINA', 'PAZ', 'FARIAS', 'MUÑOZ', 'valentinapazfariasmunoz@gmail.com', '', '+56965856960', '2002-03-07', 2, 'IV°A', 0, 'NORA', 'ISABEL', 'MUÑOZ', 'MUÑOZ', 'ISABELITA.MORELLIA@GMAIL.COM', '', '+56965817960', '1977-11-06', 2, 'IV°A', 3, 'LICEO EUGENIO MARIA DE HOSTOS', 50, '2019-03-27', '15:44:02'),
(27, 0, '', '', '', '', '', '', 243688873, 'KARLA', 'DATH', 'SILVA', 'MENDEZ', 'karladasilva01@gmail.com', '', '+56973314076', NULL, 0, '', 0, '', '', '', '', '', '', '', NULL, 0, '', 1, 'COLEGIO PIAMARTA ', 51, '2019-04-01', '10:38:18'),
(28, 308, 'AV. STA. ROSA 12910, LA PINTANA, REGIÓN METROPOLITANA', '22542 1055', '', '', '', '', 243688874, 'SCARLETTE ', '', 'LEIVA ', '', 'tercergraficasantarosa@gmail.com', '', '+56984724517', '2019-04-09', 2, '4TO MEDIO C 2019', 0, 'GERALDYN ', '', 'GONZÁLEZ', '', 'TERCERGRAFICASANTAROSA@GMAIL.COM', '', '+56988881896', '2017-04-11', 2, '4TO MEDIO C 2019', 3, '', 52, '2019-04-01', '11:22:00'),
(29, 309, 'EDUARDO CASTILLO VELASCO 2525, ÑUÑOA, REGIÓN METROPOLITANA', '22343 7688', 'WWW.COLEGIOPRECIOSASANGRE.CL', '', '', '', 243688876, 'MARIA', 'PAMELA', 'MADRID', 'HIDALGO', 'mpamemh@gmail.com', '', '+56962062756', '1974-01-04', 2, '4 MEDIO', 0, 'CAROLINA', 'VANIA', 'BELLO', 'PEREZ', 'CAROLINAVAFE@GMAIL.COM', '', '+56966056963', '1974-11-27', 2, '4 MEDIO', 3, 'COLEGIO PRECIOSA SANGRE ÑUÑOA', 53, '2019-04-01', '16:38:29'),
(30, 305, 'CALLE DR JOHOW 357, SANTIAGO, REGIÓN METROPOLITANA', '22752 3913', 'WWW.LICHAN.CL', '', '', '', 243688878, 'ELENA', 'IGNACIA', 'HUENCHULEO', 'CASTRO', 'elena.huenchuleo@gmail.com', '', '+56942688794', '2002-05-17', 2, '4° MEDIO', 0, 'PAZ', 'CAROLINA', 'GATICA', 'LOPEZ', 'PAZITAAGL@HOTMAIL.COM', '', '+56993107070', '2001-12-07', 2, '4°MEDIO', 3, 'LICEO INDUSTRIAL CHILENO ALEMAN ', 54, '2019-04-01', '18:00:15'),
(31, 62, 'MAPOCHO #2341, SANTIAGO CENTRO', '+56965475529', '', '', '', '4TO_BABILONICO', 243688879, 'CATALINA', 'ANTONIA', 'BRAVO', ']PIUTRÍN', 'cata.bravo.piutrin@gmail.com', '', '+56965475529', '2001-07-01', 2, 'IV°B', 0, 'ANDREA', 'ELIZABETH', 'PIUTRÍN', 'PIZARRO', 'APIUTRINPIZARRO@GMAIL.COM', '', '+56987517876', '1982-09-10', 2, 'IV°B', 3, 'COLEGIO NUESTRA SEÑORA DE ANDACOLLO', 55, '2019-04-01', '18:11:53'),
(32, 307, 'LA CONCEPCION 568, PAINE, SANTIAGO', '225499269', 'HTTP://WWW.PAULAJARAQUEMADAPAINE.CL/', 'COLEGIOPJQAPAINE', '', 'COLEGIOPJQAPAINE', 243688882, 'CLAUDIO', 'ANDRÉS', 'SALGADO', 'RICE', 'orientacion@paulajaraquemadapaine.cl', '', '+56932426108', '1992-03-05', 1, 'IV MEDIO A', 0, 'TIRZO', 'ALEXANDRO', 'BRUNETTI', 'LABRÍN', 'TIRZO.BRUNETTI@PAULAJARAQUEMADAPAINE.CL', '', '+56999108314', '1964-07-14', 1, 'IV MEDIO A', 3, 'COLEGIO PAULA JARAQUEMADA ALQUÍZAR', 56, '2019-04-02', '13:26:40'),
(33, 311, 'EGAÑA 1350, PEÑALOLEN, STGO', '22715061', 'HTTP://COLEGIOYORK.CL/', '', '', '', 243688883, 'DAEYSSY ', '', 'CONCHA', '', 'dae.nieves@gmail.com', '', '+56993935602', '2019-04-02', 2, 'CUARTO MEDIO F', 0, 'MARTINA', '', 'GONZÁLEZ', '', 'DAE.NIEVES@GMAIL.COM', '', '+56956720075', '2019-04-02', 2, 'CUARTO MEDIO F', 3, 'COLEGIO POLIVALENTE YORK', 57, '2019-04-02', '14:07:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registro_estatu`
--

CREATE TABLE `registro_estatu` (
  `eregi_codig` int(11) NOT NULL,
  `eregi_descr` varchar(50) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Volcado de datos para la tabla `registro_estatu`
--

INSERT INTO `registro_estatu` (`eregi_codig`, `eregi_descr`) VALUES
(3, 'APROBADO'),
(2, 'ENVIADO'),
(1, 'PENDIENTE'),
(9, 'RECHAZADO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registro_movimiento`
--

CREATE TABLE `registro_movimiento` (
  `mregi_codig` int(11) NOT NULL,
  `regis_codig` int(11) NOT NULL,
  `eregi_codig` int(11) NOT NULL,
  `mregi_motiv` text COLLATE utf8mb4_bin NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `mregi_fcrea` date NOT NULL,
  `mregi_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Volcado de datos para la tabla `registro_movimiento`
--

INSERT INTO `registro_movimiento` (`mregi_codig`, `regis_codig`, `eregi_codig`, `mregi_motiv`, `usuar_codig`, `mregi_fcrea`, `mregi_hcrea`) VALUES
(1, 1, 3, 'APROBADO', 23, '2019-01-28', '15:20:48'),
(2, 3, 3, 'APROBADO', 27, '2019-02-07', '16:43:32'),
(3, 4, 3, 'APROBADO', 28, '2019-02-08', '16:34:40'),
(4, 5, 3, 'APROBADO', 29, '2019-02-10', '20:04:15'),
(7, 10, 3, 'APROBADO', 34, '2019-02-19', '14:24:35'),
(8, 9, 3, 'FALTA NOMBRE DE COLEGIO', 25, '2019-02-19', '14:26:41'),
(9, 9, 3, 'APROBADO', 33, '2019-02-19', '14:44:47'),
(10, 13, 3, 'APROBADO', 37, '2019-02-27', '12:23:52'),
(11, 14, 3, 'APROBADO', 38, '2019-02-27', '12:26:46'),
(12, 16, 3, 'APROBADO', 40, '2019-03-12', '18:49:38'),
(15, 23, 3, 'FALTA NOMBRE DE COLEGIO', 3, '2019-03-15', '09:58:04'),
(16, 23, 3, 'APROBADO', 3, '2019-03-15', '10:01:58'),
(17, 24, 3, 'APROBADO', 48, '2019-03-18', '16:11:07'),
(18, 25, 3, 'APROBADO', 49, '2019-03-21', '12:51:48'),
(19, 26, 3, 'APROBADO', 50, '2019-03-27', '16:38:12'),
(20, 28, 3, 'APROBADO', 52, '2019-04-01', '11:37:57'),
(21, 29, 3, 'APROBADO', 53, '2019-04-01', '17:22:31'),
(22, 30, 3, 'APROBADO', 54, '2019-04-01', '20:36:14'),
(23, 31, 3, 'APROBADO', 55, '2019-04-01', '21:45:37'),
(24, 33, 3, 'APROBADO', 57, '2019-04-02', '16:09:15'),
(25, 15, 3, 'APROBADO', 39, '2019-04-02', '21:40:03'),
(26, 32, 3, 'APROBADO', 56, '2019-04-02', '23:28:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seguridad_permisos_acceso`
--

CREATE TABLE `seguridad_permisos_acceso` (
  `spacc_codig` int(11) NOT NULL,
  `spacc_descr` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `seguridad_permisos_acceso`
--

INSERT INTO `seguridad_permisos_acceso` (`spacc_codig`, `spacc_descr`) VALUES
(1, 'ACTIVO'),
(2, 'INACTIVO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seguridad_permisos_nivel`
--

CREATE TABLE `seguridad_permisos_nivel` (
  `spniv_codig` int(11) NOT NULL,
  `spniv_descr` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `seguridad_permisos_nivel`
--

INSERT INTO `seguridad_permisos_nivel` (`spniv_codig`, `spniv_descr`) VALUES
(2, 'GRUPAL'),
(1, 'INDIVIDUAL');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seguridad_permisos_roles`
--

CREATE TABLE `seguridad_permisos_roles` (
  `sprol_codig` int(11) NOT NULL,
  `srole_codig` int(11) NOT NULL,
  `modul_codig` int(11) NOT NULL,
  `spacc_codig` int(11) NOT NULL,
  `sprol_consu` tinyint(1) NOT NULL,
  `sprol_ingre` tinyint(1) NOT NULL,
  `sprol_modif` tinyint(1) NOT NULL,
  `sprol_elimi` tinyint(1) NOT NULL,
  `spniv_codig` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `seguridad_permisos_roles`
--

INSERT INTO `seguridad_permisos_roles` (`sprol_codig`, `srole_codig`, `modul_codig`, `spacc_codig`, `sprol_consu`, `sprol_ingre`, `sprol_modif`, `sprol_elimi`, `spniv_codig`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 2),
(2, 1, 10, 1, 1, 1, 1, 1, 2),
(3, 1, 21, 1, 1, 1, 1, 1, 2),
(4, 1, 200, 1, 1, 1, 1, 1, 2),
(5, 1, 203, 1, 1, 1, 1, 1, 2),
(6, 1, 201, 1, 1, 1, 1, 1, 2),
(7, 1, 202, 1, 1, 1, 1, 1, 2),
(8, 1, 8000, 1, 1, 1, 1, 1, 2),
(9, 1, 114, 1, 1, 1, 1, 1, 2),
(10, 1, 100, 1, 1, 1, 1, 1, 2),
(11, 1, 8005, 1, 1, 1, 1, 1, 2),
(12, 1, 123, 1, 1, 1, 1, 1, 2),
(13, 1, 124, 1, 1, 1, 1, 1, 2),
(14, 1, 125, 1, 1, 1, 1, 1, 2),
(15, 1, 126, 1, 1, 1, 1, 1, 2),
(16, 1, 30, 1, 1, 1, 1, 1, 2),
(17, 1, 31, 1, 1, 1, 1, 1, 2),
(18, 1, 32, 1, 1, 1, 1, 1, 2),
(19, 1, 1200, 1, 1, 1, 1, 1, 2),
(20, 1, 33, 1, 1, 1, 1, 1, 2),
(21, 1, 34, 1, 1, 1, 1, 1, 2),
(22, 1, 50, 1, 1, 1, 1, 1, 2),
(23, 1, 51, 1, 1, 1, 1, 1, 2),
(24, 1, 52, 1, 1, 1, 1, 1, 2),
(25, 1, 53, 1, 1, 1, 1, 1, 2),
(26, 1, 54, 1, 1, 1, 1, 1, 2),
(27, 1, 1100, 1, 1, 1, 1, 1, 2),
(32, 1, 102, 1, 1, 1, 1, 1, 2),
(33, 1, 108, 1, 1, 1, 1, 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seguridad_permisos_usuarios`
--

CREATE TABLE `seguridad_permisos_usuarios` (
  `spusu_codig` int(11) NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `modul_codig` int(11) NOT NULL,
  `spacc_codig` int(11) NOT NULL,
  `spusu_consu` tinyint(1) NOT NULL,
  `spusu_ingre` tinyint(1) NOT NULL,
  `spusu_modif` tinyint(1) NOT NULL,
  `spusu_elimi` tinyint(1) NOT NULL,
  `spniv_codig` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `seguridad_permisos_usuarios`
--

INSERT INTO `seguridad_permisos_usuarios` (`spusu_codig`, `usuar_codig`, `modul_codig`, `spacc_codig`, `spusu_consu`, `spusu_ingre`, `spusu_modif`, `spusu_elimi`, `spniv_codig`) VALUES
(2, 3, 900, 1, 1, 1, 1, 1, 1),
(3, 3, 1, 1, 1, 1, 1, 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seguridad_roles`
--

CREATE TABLE `seguridad_roles` (
  `srole_codig` int(11) NOT NULL,
  `srole_descr` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `seguridad_roles`
--

INSERT INTO `seguridad_roles` (`srole_codig`, `srole_descr`) VALUES
(1, 'SUPER ADMINISTRADOR');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sequence_data`
--

CREATE TABLE `sequence_data` (
  `sequence_name` varchar(100) NOT NULL,
  `sequence_increment` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `sequence_min_value` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `sequence_max_value` bigint(20) UNSIGNED NOT NULL DEFAULT '18446744073709551615',
  `sequence_cur_value` bigint(20) UNSIGNED DEFAULT '1',
  `sequence_cycle` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios`
--

CREATE TABLE `servicios` (
  `servi_codig` int(11) NOT NULL,
  `servi_descr` varchar(50) COLLATE utf8_bin NOT NULL,
  `servi_preci` double NOT NULL,
  `moned_codig` int(11) NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `servi_fcrea` date NOT NULL,
  `servi_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `servicios`
--

INSERT INTO `servicios` (`servi_codig`, `servi_descr`, `servi_preci`, `moned_codig`, `usuar_codig`, `servi_fcrea`, `servi_hcrea`) VALUES
(1, 'CORTE DE CABELLO', 10000, 1, 3, '2018-08-20', '18:57:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitud_origen`
--

CREATE TABLE `solicitud_origen` (
  `orige_codig` int(11) NOT NULL,
  `orige_descr` varchar(100) NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `orige_fcrea` date NOT NULL,
  `orige_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `solicitud_origen`
--

INSERT INTO `solicitud_origen` (`orige_codig`, `orige_descr`, `usuar_codig`, `orige_fcrea`, `orige_hcrea`) VALUES
(1, 'CORREO ELECTRÓNICO', 3, '2019-06-25', '17:11:02'),
(2, 'ALIADO', 3, '2019-06-25', '17:11:02'),
(3, 'CALL CENTER', 3, '2019-06-25', '17:11:17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_cliente_documento`
--

CREATE TABLE `tipo_cliente_documento` (
  `tcdoc_codig` int(11) NOT NULL,
  `tclie_codig` int(11) NOT NULL,
  `tdocu_codig` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `tipo_cliente_documento`
--

INSERT INTO `tipo_cliente_documento` (`tcdoc_codig`, `tclie_codig`, `tdocu_codig`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 2, 4),
(5, 2, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajador`
--

CREATE TABLE `trabajador` (
  `traba_codig` int(11) NOT NULL,
  `perso_codig` int(11) NOT NULL,
  `ttipo_codig` int(11) NOT NULL,
  `etrab_codig` int(11) NOT NULL,
  `traba_corre` varchar(50) NOT NULL,
  `traba_telef` varchar(50) NOT NULL,
  `traba_direc` text NOT NULL,
  `traba_salar` double NOT NULL,
  `traba_comis` double NOT NULL DEFAULT '0',
  `banco_value` varchar(4) NOT NULL,
  `traba_ncuen` varchar(20) NOT NULL,
  `tcuen_codig` int(11) NOT NULL,
  `traba_ctrab` varchar(45) NOT NULL,
  `traba_obser` text NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `traba_fcrea` date NOT NULL,
  `traba_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajador_estatus`
--

CREATE TABLE `trabajador_estatus` (
  `etrab_codig` int(11) NOT NULL,
  `etrab_descr` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `trabajador_estatus`
--

INSERT INTO `trabajador_estatus` (`etrab_codig`, `etrab_descr`) VALUES
(1, 'ACTIVO'),
(3, 'EGRESADO'),
(2, 'INACTIVO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajador_tipo`
--

CREATE TABLE `trabajador_tipo` (
  `ttipo_codig` int(11) NOT NULL,
  `ttipo_descr` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `trabajador_tipo`
--

INSERT INTO `trabajador_tipo` (`ttipo_codig`, `ttipo_descr`) VALUES
(4, 'GERENTE'),
(1, 'VENDEDOR');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidad_tipo`
--

CREATE TABLE `unidad_tipo` (
  `tunid_codig` int(11) NOT NULL,
  `tunid_descr` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `unidad_tipo`
--

INSERT INTO `unidad_tipo` (`tunid_codig`, `tunid_descr`) VALUES
(1, 'CAJA'),
(3, 'DESCRIPCION'),
(2, 'LITRO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `email`, `password`) VALUES
(0, 'lunafive@hotmail.com', '123456789');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `usuar_codig` int(11) NOT NULL,
  `perso_codig` int(11) NOT NULL,
  `coleg_codig` int(11) NOT NULL,
  `usuar_login` varchar(50) NOT NULL,
  `usuar_passw` varchar(50) NOT NULL,
  `urole_codig` int(11) NOT NULL,
  `uesta_codig` int(11) NOT NULL,
  `usuar_obser` text NOT NULL,
  `usuar_fcrea` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`usuar_codig`, `perso_codig`, `coleg_codig`, `usuar_login`, `usuar_passw`, `urole_codig`, `uesta_codig`, `usuar_obser`, `usuar_fcrea`) VALUES
(3, 1, 0, 'kevinalejandro3@gmail.com', '951c0c4f3324e61135c69270ee66dc04', 1, 1, 'NINGUNA', '0000-00-00'),
(22, 19, 0, 'iveymontoya@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 1, 1, '', '2018-11-22'),
(23, 35, 287, 'matiascassano@gmail.com', '22c038280f7f020b76cae24653561f7e', 3, 1, 'IBEROAMERICANO 4B', '2019-01-28'),
(24, 37, 0, 'ke.vinalejandro3@gmail.com', 'ec41480e3861cc2623285a95a538e057', 2, 1, 'A', '2019-01-30'),
(25, 38, 0, 'ventas@poleronestiempo.cl', '7d1a7cc4fdba72a0781a6f43acc3269a', 1, 1, '', '2019-01-30'),
(26, 39, 0, 'carlosmariourrutia@hotmail.com', '7d2efa67f4ff694f4acbcebc91b6e7ee', 5, 1, '', '2019-01-31'),
(27, 40, 291, 'fer1408@icloud.com', 'ac8d3b9fd8a956ca27f93c8c3feb305d', 3, 1, 'ALCANTARA DE LA CORDILLERA ', '2019-02-07'),
(28, 42, 288, 'camilocassano@gmail.com', '1d4dbeab22d5ba5efde2bd6e21fdf710', 3, 1, 'TIEMPO', '2019-02-08'),
(29, 44, 290, 'mpbenavides31@gmail.com', '4abf87a3ff56040c415e535d35fcf64d', 3, 1, 'COLEGIO TERESA DE LOS ANDES, ALGARROBO', '2019-02-09'),
(33, 49, 294, 'vikymunizaga@gmail.com', '13a267683adab7fb184526966717ae34', 3, 1, 'COLEGIO PEDRO DE VALDIVIA PROVIDENCIA', '2019-02-18'),
(34, 52, 2, 'maticassano@outlook.com', 'a46d6c8f64ed2ac38242ccbb12e8152a', 3, 1, 'M', '2019-02-19'),
(37, 57, 296, 'jeimyhenriquezolivares1@gmail.com', '0fb7600fcd0b68664aca78f23c4a2305', 3, 1, 'CENTRO POLITÉCNICO PARTICULAR SAN RAMÓN', '2019-02-27'),
(38, 58, 297, 'mgonzalezduran59@gmail.com', '7ad90445db34e3dabe46f2bac6b1e4c5', 3, 1, ' SANTA MARÍA DE LA CORDILLERA', '2019-02-27'),
(39, 61, 298, 'de9757641@gmail.com', 'c53d01da818c17294a3d6bc3547694ba', 3, 1, 'SAN MATEO', '2019-03-04'),
(40, 62, 57, 'constanzaalejandra.sh@gmail.com', 'f7a0cd3b474e43cb1af4c78e7ba4cf26', 3, 1, 'COLEGIO ALTO PALENA', '2019-03-11'),
(41, 63, 0, 'konygutierrezmartinez54@gmail.com', 'fb301b2fc98f2e557823b1a5942e3f04', 2, 1, 'COLEGIO ALTO PALENA ', '2019-03-11'),
(47, 70, 303, 'ricardo.breig@gmail.com', 'ff2d664bc99fd312dff31c29686f882e', 3, 1, 'RICARDO', '2019-03-14'),
(48, 71, 300, 'sorayaroa@hotmail.com', '79b79ccbdc14357dbeb188250bee9fa4', 3, 1, 'COLEGIO ALTAS CUMBRES PUERTO VARAS', '2019-03-14'),
(49, 74, 304, 'dfigueroagallardo@gmail.com', '92ddcc2466f4188bba769284e99a4c58', 3, 1, 'CENTRO EDUCATIVO PEUMAYEN', '2019-03-15'),
(50, 78, 306, 'valentinapazfariasmunoz@gmail.com', 'c32557b8031664ca216aa898bbe33fbb', 3, 1, 'LICEO EUGENIO MARIA DE HOSTOS', '2019-03-27'),
(51, 81, 0, 'karladasilva01@gmail.com', '2eed6ae477433b58a6e623726ba65fdd', 2, 1, 'COLEGIO PIAMARTA ', '2019-04-01'),
(52, 83, 308, 'tercergraficasantarosa@gmail.com', '1da341d42b8eb8853a8fe0e39fb14373', 3, 1, '', '2019-04-01'),
(53, 85, 309, 'mpamemh@gmail.com', '41b2a6d89ca29fea861794770986a32b', 3, 1, 'COLEGIO PRECIOSA SANGRE ÑUÑOA', '2019-04-01'),
(54, 88, 305, 'elena.huenchuleo@gmail.com', 'f00782c653426f94ef08d2a009576157', 3, 1, 'LICEO INDUSTRIAL CHILENO ALEMAN ', '2019-04-01'),
(55, 89, 62, 'cata.bravo.piutrin@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 3, 1, 'COLEGIO NUESTRA SEÑORA DE ANDACOLLO', '2019-04-01'),
(56, 93, 307, 'orientacion@paulajaraquemadapaine.cl', '990bbab2ec968c5ff29c6192a7c861e6', 3, 1, 'COLEGIO PAULA JARAQUEMADA ALQUÍZAR', '2019-04-02'),
(57, 94, 311, 'dae.nieves@gmail.com', '5a711ddd367fe739252eff69b74392a6', 3, 1, 'COLEGIO POLIVALENTE YORK', '2019-04-02'),
(58, 100, 0, 'kevinalejandro2@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 1, 1, 'A', '2019-06-26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_estatus`
--

CREATE TABLE `usuarios_estatus` (
  `uesta_codig` int(11) NOT NULL,
  `uesta_descr` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios_estatus`
--

INSERT INTO `usuarios_estatus` (`uesta_codig`, `uesta_descr`) VALUES
(1, 'ACTIVO'),
(9, 'INACTIVO'),
(2, 'VERIFICACION');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_roles`
--

CREATE TABLE `usuarios_roles` (
  `urole_codig` int(11) NOT NULL,
  `urole_descr` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios_roles`
--

INSERT INTO `usuarios_roles` (`urole_codig`, `urole_descr`) VALUES
(1, 'ADMINISTRADOR'),
(5, 'BORDADOR'),
(2, 'CLIENTE EN REGISTRO'),
(3, 'CLIENTE REGISTRADO'),
(4, 'DISEÑADOR');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta_tipo`
--

CREATE TABLE `venta_tipo` (
  `tvent_codig` int(11) NOT NULL,
  `tvent_descr` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `venta_tipo`
--

INSERT INTO `venta_tipo` (`tvent_codig`, `tvent_descr`) VALUES
(1, 'CONTADO'),
(2, 'CREDITO');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `actividad_comercial`
--
ALTER TABLE `actividad_comercial`
  ADD PRIMARY KEY (`acome_codig`),
  ADD UNIQUE KEY `acome_descr` (`acome_descr`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `banco`
--
ALTER TABLE `banco`
  ADD PRIMARY KEY (`banco_codig`);
--
-- Indices de la tabla `banco_tipo_cuenta`
--
ALTER TABLE `banco_tipo_cuenta`
  ADD PRIMARY KEY (`tcuen_codig`);

--
-- Indices de la tabla `ciudades`
--
ALTER TABLE `ciudades`
  ADD PRIMARY KEY (`ciuda_codig`),
  ADD KEY `estad_codig` (`estad_codig`),
  ADD KEY `paise_codig` (`paise_codig`) USING BTREE;

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`clien_codig`),
  ADD KEY `tclie_codig` (`tclie_codig`),
  ADD KEY `tdocu_codig` (`tdocu_codig`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `cliente_tipo`
--
ALTER TABLE `cliente_tipo`
  ADD PRIMARY KEY (`tclie_codig`),
  ADD UNIQUE KEY `tclie_descr_UNIQUE` (`tclie_descr`);

--
-- Indices de la tabla `cobranza_tipo`
--
ALTER TABLE `cobranza_tipo`
  ADD PRIMARY KEY (`ctipo_codig`),
  ADD UNIQUE KEY `ctipo_descr_UNIQUE` (`ctipo_descr`);

--
-- Indices de la tabla `colegio`
--
ALTER TABLE `colegio`
  ADD PRIMARY KEY (`coleg_codig`),
  ADD UNIQUE KEY `coleg_nombr` (`coleg_nombr`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `colegio_contacto`
--
ALTER TABLE `colegio_contacto`
  ADD PRIMARY KEY (`ccole_codig`),
  ADD UNIQUE KEY `coleg_codig_2` (`coleg_codig`,`perso_codig`),
  ADD KEY `coleg_codig` (`coleg_codig`),
  ADD KEY `perso_codig` (`perso_codig`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `depositos`
--
ALTER TABLE `depositos`
  ADD PRIMARY KEY (`depos_codig`),
  ADD UNIQUE KEY `depos_codig` (`depos_codig`);

--
-- Indices de la tabla `depositos_validar`
--
ALTER TABLE `depositos_validar`
  ADD PRIMARY KEY (`valid_codig`),
  ADD UNIQUE KEY `valid_codig` (`valid_codig`);

--
-- Indices de la tabla `documento_tipo`
--
ALTER TABLE `documento_tipo`
  ADD PRIMARY KEY (`tdocu_codig`),
  ADD UNIQUE KEY `tdocu_descr` (`tdocu_descr`);

--
-- Indices de la tabla `egresos`
--
ALTER TABLE `egresos`
  ADD PRIMARY KEY (`egres_codig`),
  ADD KEY `banco_codig` (`banco_codig`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `empresa_tipo`
--
ALTER TABLE `empresa_tipo`
  ADD PRIMARY KEY (`etipo_codig`),
  ADD UNIQUE KEY `etipo_value_UNIQUE` (`etipo_descr`);

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`estad_codig`),
  ADD UNIQUE KEY `estad_codig` (`estad_codig`,`paise_codig`),
  ADD KEY `paise_codig` (`paise_codig`);

--
-- Indices de la tabla `estado_civil`
--
ALTER TABLE `estado_civil`
  ADD PRIMARY KEY (`ecivi_codig`),
  ADD UNIQUE KEY `ecivi_descr_UNIQUE` (`ecivi_descr`),
  ADD UNIQUE KEY `ecivi_value_UNIQUE` (`ecivi_value`);

--
-- Indices de la tabla `estatus`
--
ALTER TABLE `estatus`
  ADD PRIMARY KEY (`estat_codig`),
  ADD UNIQUE KEY `estat_descr` (`estat_descr`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `estatus_validar`
--
ALTER TABLE `estatus_validar`
  ADD PRIMARY KEY (`estval_codig`),
  ADD UNIQUE KEY `estval_codig` (`estval_codig`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`factu_codig`),
  ADD UNIQUE KEY `factu_nfact` (`factu_nfact`),
  ADD KEY `traba_codig` (`traba_codig`),
  ADD KEY `tvent_codig` (`tvent_codig`),
  ADD KEY `ptipo_codig` (`ptipo_codig`),
  ADD KEY `usuar_codig` (`usuar_codig`),
  ADD KEY `clien_codig` (`clien_codig`),
  ADD KEY `efact_codig` (`efact_codig`),
  ADD KEY `banco_codig` (`banco_codig`),
  ADD KEY `iva_codig` (`iva_codig`) USING BTREE;

--
-- Indices de la tabla `factura_estatu`
--
ALTER TABLE `factura_estatu`
  ADD PRIMARY KEY (`efact_codig`),
  ADD UNIQUE KEY `efact_descr` (`efact_descr`);

--
-- Indices de la tabla `factura_estatu_pago`
--
ALTER TABLE `factura_estatu_pago`
  ADD PRIMARY KEY (`efpag_codig`);

--
-- Indices de la tabla `factura_producto`
--
ALTER TABLE `factura_producto`
  ADD PRIMARY KEY (`fprod_codig`),
  ADD KEY `usuar_codig` (`usuar_codig`),
  ADD KEY `inven_codig` (`inven_codig`),
  ADD KEY `factu_codig` (`factu_codig`),
  ADD KEY `tprod_codig` (`tprod_codig`);

--
-- Indices de la tabla `genero`
--
ALTER TABLE `genero`
  ADD PRIMARY KEY (`gener_codig`),
  ADD UNIQUE KEY `gener_value` (`gener_value`),
  ADD UNIQUE KEY `gener_descr` (`gener_descr`);

--
-- Indices de la tabla `impuesto_iva`
--
ALTER TABLE `impuesto_iva`
  ADD PRIMARY KEY (`iva_codig`),
  ADD UNIQUE KEY `iva_descr` (`iva_descr`);

--
-- Indices de la tabla `inventario`
--
ALTER TABLE `inventario`
  ADD PRIMARY KEY (`inven_codig`),
  ADD KEY `tunid_codig` (`tunid_codig`),
  ADD KEY `usuar_codig` (`usuar_codig`) USING BTREE,
  ADD KEY `ivent_cprod_UNIQUE` (`inven_cprod`) USING BTREE,
  ADD KEY `moned_codig` (`moned_codig`);

--
-- Indices de la tabla `inventario_entrada`
--
ALTER TABLE `inventario_entrada`
  ADD PRIMARY KEY (`ientr_codig`),
  ADD KEY `prove_codig` (`prove_codig`),
  ADD KEY `ieest_codig` (`ieest_codig`);

--
-- Indices de la tabla `inventario_entrada_estatus`
--
ALTER TABLE `inventario_entrada_estatus`
  ADD PRIMARY KEY (`ieest_codig`);

--
-- Indices de la tabla `inventario_entrada_producto`
--
ALTER TABLE `inventario_entrada_producto`
  ADD PRIMARY KEY (`iepro_codig`),
  ADD KEY `usuar_codig` (`usuar_codig`),
  ADD KEY `inven_codig` (`inven_codig`),
  ADD KEY `ientr_codig` (`ientr_codig`);

--
-- Indices de la tabla `inventario_salida`
--
ALTER TABLE `inventario_salida`
  ADD PRIMARY KEY (`isali_codig`),
  ADD KEY `prove_codig` (`traba_codig`),
  ADD KEY `isest_codig` (`isest_codig`);

--
-- Indices de la tabla `inventario_salida_estatus`
--
ALTER TABLE `inventario_salida_estatus`
  ADD PRIMARY KEY (`isest_codig`);

--
-- Indices de la tabla `inventario_salida_producto`
--
ALTER TABLE `inventario_salida_producto`
  ADD PRIMARY KEY (`ispro_codig`),
  ADD KEY `usuar_codig` (`usuar_codig`),
  ADD KEY `inven_codig` (`inven_codig`),
  ADD KEY `isali_codig` (`isali_codig`);

--
-- Indices de la tabla `meses`
--
ALTER TABLE `meses`
  ADD PRIMARY KEY (`meses_codig`),
  ADD UNIQUE KEY `meses_descr` (`meses_descr`);

--
-- Indices de la tabla `modulos`
--
ALTER TABLE `modulos`
  ADD PRIMARY KEY (`modul_codig`);

--
-- Indices de la tabla `modulos_estatus`
--
ALTER TABLE `modulos_estatus`
  ADD PRIMARY KEY (`emodu_codig`),
  ADD UNIQUE KEY `emodu_descr` (`emodu_descr`);

--
-- Indices de la tabla `moneda`
--
ALTER TABLE `moneda`
  ADD PRIMARY KEY (`moned_codig`),
  ADD UNIQUE KEY `moned_descr` (`moned_descr`),
  ADD UNIQUE KEY `moned_value` (`moned_value`);

--
-- Indices de la tabla `municipio`
--
ALTER TABLE `municipio`
  ADD PRIMARY KEY (`munic_codig`),
  ADD UNIQUE KEY `munic_codig` (`munic_codig`,`estad_codig`,`paise_codig`),
  ADD KEY `estad_codig` (`estad_codig`),
  ADD KEY `paise_codig` (`paise_codig`);

--
-- Indices de la tabla `nacionalidad`
--
ALTER TABLE `nacionalidad`
  ADD PRIMARY KEY (`nacio_codig`),
  ADD UNIQUE KEY `nacio_descr` (`nacio_descr`),
  ADD UNIQUE KEY `nacio_value` (`nacio_value`);

--
-- Indices de la tabla `nomina`
--
ALTER TABLE `nomina`
  ADD PRIMARY KEY (`nomin_codig`),
  ADD UNIQUE KEY `nomin_descr` (`nomin_descr`),
  ADD KEY `enomi_codig` (`enomi_codig`),
  ADD KEY `tnomi_codig` (`tnomi_codig`),
  ADD KEY `pnomi_codig` (`pnomi_codig`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `nomina_asignacion`
--
ALTER TABLE `nomina_asignacion`
  ADD PRIMARY KEY (`asign_codig`),
  ADD UNIQUE KEY `nomin_codig_2` (`nomin_codig`,`perio_codig`,`traba_codig`),
  ADD KEY `nomin_codig` (`nomin_codig`),
  ADD KEY `perio_codig` (`perio_codig`),
  ADD KEY `traba_codig` (`traba_codig`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `nomina_concepto`
--
ALTER TABLE `nomina_concepto`
  ADD PRIMARY KEY (`conce_codig`),
  ADD KEY `nomin_codig` (`nomin_codig`),
  ADD KEY `tconc_codig` (`tconc_codig`),
  ADD KEY `usuar_cofig` (`usuar_codig`);

--
-- Indices de la tabla `nomina_concepto_tipo`
--
ALTER TABLE `nomina_concepto_tipo`
  ADD PRIMARY KEY (`tconc_codig`),
  ADD UNIQUE KEY `tconc_descr` (`tconc_descr`);

--
-- Indices de la tabla `nomina_estatus`
--
ALTER TABLE `nomina_estatus`
  ADD PRIMARY KEY (`enomi_codig`),
  ADD UNIQUE KEY `enomi_descr` (`enomi_descr`);

--
-- Indices de la tabla `nomina_periodo`
--
ALTER TABLE `nomina_periodo`
  ADD PRIMARY KEY (`perio_codig`),
  ADD KEY `usuar_codig` (`usuar_codig`),
  ADD KEY `nomin_codig` (`nomin_codig`),
  ADD KEY `perio_seria` (`perio_ano`),
  ADD KEY `meses_codig` (`meses_codig`),
  ADD KEY `eperi_codig` (`eperi_codig`);

--
-- Indices de la tabla `nomina_periodo_estatus`
--
ALTER TABLE `nomina_periodo_estatus`
  ADD PRIMARY KEY (`eperi_codig`),
  ADD UNIQUE KEY `eperi_descr` (`eperi_descr`);

--
-- Indices de la tabla `nomina_resumen`
--
ALTER TABLE `nomina_resumen`
  ADD PRIMARY KEY (`resum_codig`),
  ADD UNIQUE KEY `nomin_codig_2` (`nomin_codig`,`perio_codig`,`traba_codig`,`conce_codig`),
  ADD KEY `nomin_codig` (`nomin_codig`),
  ADD KEY `perio_codig` (`perio_codig`),
  ADD KEY `traba_codig` (`traba_codig`),
  ADD KEY `conce_codig` (`conce_codig`),
  ADD KEY `tconc_codig` (`tconc_codig`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `nomina_tipo`
--
ALTER TABLE `nomina_tipo`
  ADD PRIMARY KEY (`tnomi_codig`),
  ADD UNIQUE KEY `tnomi_descr` (`tnomi_descr`);

--
-- Indices de la tabla `nomina_tipo_pago`
--
ALTER TABLE `nomina_tipo_pago`
  ADD PRIMARY KEY (`pnomi_codig`),
  ADD UNIQUE KEY `pnomi_descr` (`pnomi_descr`);

--
-- Indices de la tabla `operador_telefonico`
--
ALTER TABLE `operador_telefonico`
  ADD PRIMARY KEY (`otele_codig`),
  ADD UNIQUE KEY `otele_descr` (`otele_descr`),
  ADD KEY `usuar_` (`usuar_codig`);

--
-- Indices de la tabla `organigrama`
--
ALTER TABLE `organigrama`
  ADD PRIMARY KEY (`organ_codig`),
  ADD UNIQUE KEY `organ_descr` (`organ_descr`),
  ADD KEY `onive_codig` (`onive_codig`);

--
-- Indices de la tabla `organigrama_nivel`
--
ALTER TABLE `organigrama_nivel`
  ADD PRIMARY KEY (`onive_codig`),
  ADD UNIQUE KEY `onive_descr` (`onive_descr`);

--
-- Indices de la tabla `pago_tipo`
--
ALTER TABLE `pago_tipo`
  ADD PRIMARY KEY (`ptipo_codig`),
  ADD UNIQUE KEY `ptipo_descr_UNIQUE` (`ptipo_descr`);

--
-- Indices de la tabla `paises`
--
ALTER TABLE `paises`
  ADD PRIMARY KEY (`paise_codig`);

--
-- Indices de la tabla `parroquia`
--
ALTER TABLE `parroquia`
  ADD PRIMARY KEY (`parro_codig`),
  ADD UNIQUE KEY `parro_codig` (`parro_codig`,`munic_codig`,`estad_codig`,`paise_codig`),
  ADD KEY `munic_codig` (`munic_codig`),
  ADD KEY `estad_codig` (`estad_codig`) USING BTREE,
  ADD KEY `paise_codig` (`paise_codig`) USING BTREE;

--
-- Indices de la tabla `pedido_corte`
--
ALTER TABLE `pedido_corte`
  ADD PRIMARY KEY (`corte_codig`),
  ADD KEY `pdlis_codig` (`pdlis_codig`),
  ADD KEY `traba_codig` (`traba_codig`),
  ADD KEY `ecort_codig` (`ecort_codig`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `pedido_corte_estatu`
--
ALTER TABLE `pedido_corte_estatu`
  ADD PRIMARY KEY (`ecort_codig`),
  ADD UNIQUE KEY `ecort_descr` (`ecort_descr`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `pedido_costo`
--
ALTER TABLE `pedido_costo`
  ADD PRIMARY KEY (`costo_codig`),
  ADD UNIQUE KEY `costo_descr` (`costo_descr`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `pedido_deduccion`
--
ALTER TABLE `pedido_deduccion`
  ADD PRIMARY KEY (`deduc_codig`),
  ADD UNIQUE KEY `deduc_descr` (`deduc_descr`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `pedido_detalle_listado_imagen`
--
ALTER TABLE `pedido_detalle_listado_imagen`
  ADD PRIMARY KEY (`pdlim_codig`),
  ADD KEY `pedid_codig` (`pedid_codig`),
  ADD KEY `pdeta_codig` (`pdeta_codig`),
  ADD KEY `pdlis_codig` (`pdlis_codig`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `pedido_diseno`
--
ALTER TABLE `pedido_diseno`
  ADD PRIMARY KEY (`disen_codig`),
  ADD KEY `usuar_codig` (`usuar_codig`),
  ADD KEY `ecort_codig` (`ecort_codig`),
  ADD KEY `traba_codig` (`traba_codig`),
  ADD KEY `pedid_codig` (`pedid_codig`);

--
-- Indices de la tabla `pedido_emoji`
--
ALTER TABLE `pedido_emoji`
  ADD PRIMARY KEY (`emoji_codig`);

--
-- Indices de la tabla `pedido_estatu`
--
ALTER TABLE `pedido_estatu`
  ADD PRIMARY KEY (`epedi_codig`),
  ADD UNIQUE KEY `epedi_descr` (`epedi_descr`);

--
-- Indices de la tabla `pedido_estatu_pago`
--
ALTER TABLE `pedido_estatu_pago`
  ADD PRIMARY KEY (`epago_codig`),
  ADD UNIQUE KEY `epago_descr` (`epago_descr`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `pedido_fuente`
--
ALTER TABLE `pedido_fuente`
  ADD PRIMARY KEY (`fuent_codig`),
  ADD UNIQUE KEY `fuent_descr` (`fuent_descr`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `pedido_incluye`
--
ALTER TABLE `pedido_incluye`
  ADD PRIMARY KEY (`inclu_codig`),
  ADD UNIQUE KEY `inclu_descr` (`inclu_descr`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `pedido_lista`
--
ALTER TABLE `pedido_lista`
  ADD PRIMARY KEY (`lista_codig`);

--
-- Indices de la tabla `pedido_modelo`
--
ALTER TABLE `pedido_modelo`
  ADD PRIMARY KEY (`model_codig`),
  ADD UNIQUE KEY `model_descr` (`model_descr`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `pedido_modelo_categoria`
--
ALTER TABLE `pedido_modelo_categoria`
  ADD PRIMARY KEY (`mcate_codig`),
  ADD UNIQUE KEY `mcate_descr` (`mcate_descr`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `pedido_modelo_color`
--
ALTER TABLE `pedido_modelo_color`
  ADD PRIMARY KEY (`mcolo_codig`),
  ADD UNIQUE KEY `mcolo_numer` (`mcolo_numer`,`mcolo_descr`,`tmode_codig`) USING BTREE,
  ADD KEY `usuar_codig` (`usuar_codig`),
  ADD KEY `model_codig` (`tmode_codig`);

--
-- Indices de la tabla `pedido_modelo_conjunto`
--
ALTER TABLE `pedido_modelo_conjunto`
  ADD PRIMARY KEY (`mconj_codig`),
  ADD UNIQUE KEY `tmode_codig_2` (`tmode_codig`,`model_codig`,`mcate_codig`,`mvari_codig`) USING BTREE,
  ADD KEY `tmode_codig` (`tmode_codig`),
  ADD KEY `model_codig` (`model_codig`),
  ADD KEY `mcate_codig` (`mcate_codig`),
  ADD KEY `mvari_codig` (`mvari_codig`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `pedido_modelo_opcional`
--
ALTER TABLE `pedido_modelo_opcional`
  ADD PRIMARY KEY (`mopci_codig`),
  ADD UNIQUE KEY `mopci_descr` (`mopci_descr`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `pedido_modelo_tipo`
--
ALTER TABLE `pedido_modelo_tipo`
  ADD PRIMARY KEY (`tmode_codig`),
  ADD UNIQUE KEY `model_descr` (`tmode_descr`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `pedido_modelo_variante`
--
ALTER TABLE `pedido_modelo_variante`
  ADD PRIMARY KEY (`mvari_codig`),
  ADD UNIQUE KEY `mvari_descr` (`mvari_descr`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `pedido_pagos`
--
ALTER TABLE `pedido_pagos`
  ADD PRIMARY KEY (`pagos_codig`),
  ADD UNIQUE KEY `pagos_numer` (`pagos_numer`),
  ADD KEY `pedid_codig` (`pedid_codig`),
  ADD KEY `ptipo_codig` (`ptipo_codig`),
  ADD KEY `usuar_codig` (`usuar_codig`),
  ADD KEY `epago_codig` (`epago_codig`);

--
-- Indices de la tabla `pedido_pedidos`
--
ALTER TABLE `pedido_pedidos`
  ADD PRIMARY KEY (`pedid_codig`),
  ADD UNIQUE KEY `pedid_numer` (`pedid_numer`),
  ADD KEY `usuar_codig` (`usuar_codig`),
  ADD KEY `epedi_codig` (`epedi_codig`) USING BTREE;

--
-- Indices de la tabla `pedido_pedidos_adicional`
--
ALTER TABLE `pedido_pedidos_adicional`
  ADD PRIMARY KEY (`adici_codig`),
  ADD KEY `pedid_codig` (`pedid_codig`),
  ADD KEY `costo_codig` (`costo_codig`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `pedido_pedidos_deduccion`
--
ALTER TABLE `pedido_pedidos_deduccion`
  ADD PRIMARY KEY (`pdedu_codig`),
  ADD UNIQUE KEY `pedid_codig` (`pedid_codig`),
  ADD KEY `deduc_codig` (`deduc_codig`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `pedido_pedidos_detalle`
--
ALTER TABLE `pedido_pedidos_detalle`
  ADD PRIMARY KEY (`pdeta_codig`),
  ADD UNIQUE KEY `pedid_codig` (`pedid_codig`,`tmode_codig`,`model_codig`,`mcate_codig`,`mvari_codig`,`mopci_codig`);

--
-- Indices de la tabla `pedido_pedidos_detalle_listado`
--
ALTER TABLE `pedido_pedidos_detalle_listado`
  ADD PRIMARY KEY (`pdlis_codig`),
  ADD KEY `pedid_codig` (`pedid_codig`),
  ADD KEY `pdeta_codig` (`pdeta_codig`),
  ADD KEY `usuar_codig` (`usuar_codig`),
  ADD KEY `talla_codig` (`talla_codig`);

--
-- Indices de la tabla `pedido_pedidos_imagen`
--
ALTER TABLE `pedido_pedidos_imagen`
  ADD PRIMARY KEY (`pimag_codig`),
  ADD KEY `pedid_codig` (`pedid_codig`),
  ADD KEY `ptima_codig` (`ptima_codig`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `pedido_pedidos_imagen_tipo`
--
ALTER TABLE `pedido_pedidos_imagen_tipo`
  ADD PRIMARY KEY (`ptima_codig`),
  ADD UNIQUE KEY `ptima_descr` (`ptima_descr`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `pedido_precio`
--
ALTER TABLE `pedido_precio`
  ADD PRIMARY KEY (`pprec_codig`),
  ADD UNIQUE KEY `pprec_descr` (`pprec_descr`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `pedido_precio_electivo`
--
ALTER TABLE `pedido_precio_electivo`
  ADD PRIMARY KEY (`pelec_codig`) USING BTREE,
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `pedido_simbolo_simple`
--
ALTER TABLE `pedido_simbolo_simple`
  ADD PRIMARY KEY (`ssimp_codig`),
  ADD UNIQUE KEY `ssimp_descr` (`ssimp_descr`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `pedido_talla`
--
ALTER TABLE `pedido_talla`
  ADD PRIMARY KEY (`talla_codig`),
  ADD UNIQUE KEY `talla_descr` (`talla_descr`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `pedido_talla_ajuste`
--
ALTER TABLE `pedido_talla_ajuste`
  ADD PRIMARY KEY (`tajus_codig`),
  ADD UNIQUE KEY `tajus_descr` (`tajus_descr`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `permisos`
--
ALTER TABLE `permisos`
  ADD PRIMARY KEY (`permi_codig`),
  ADD UNIQUE KEY `cvlpermisos` (`permi_codig`),
  ADD KEY `clvrol` (`urole_codig`),
  ADD KEY `modul_codig` (`modul_codig`);

--
-- Indices de la tabla `permisos_estatus`
--
ALTER TABLE `permisos_estatus`
  ADD PRIMARY KEY (`eperm_codig`),
  ADD UNIQUE KEY `eperm_descr` (`eperm_descr`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`perso_codig`),
  ADD KEY `gener_value` (`gener_value`),
  ADD KEY `ecivi_value` (`ecivi_value`);

--
-- Indices de la tabla `producto_tipo`
--
ALTER TABLE `producto_tipo`
  ADD PRIMARY KEY (`tprod_codig`),
  ADD UNIQUE KEY `tprod_descr` (`tprod_descr`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`prove_codig`),
  ADD KEY `tclie_codig` (`tclie_codig`),
  ADD KEY `tdocu_codig` (`tdocu_codig`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `proveedor_tipo`
--
ALTER TABLE `proveedor_tipo`
  ADD PRIMARY KEY (`tprov_codig`);

--
-- Indices de la tabla `rango_categoria`
--
ALTER TABLE `rango_categoria`
  ADD PRIMARY KEY (`ranca_codig`);

--
-- Indices de la tabla `rd_preregistro`
--
ALTER TABLE `rd_preregistro`
  ADD PRIMARY KEY (`prere_codig`),
  ADD UNIQUE KEY `prere_numer` (`prere_numer`),
  ADD KEY `usuar_codig` (`usuar_codig`),
  ADD KEY `orige_codig` (`orige_codig`),
  ADD KEY `banco_codig` (`banco_codig`),
  ADD KEY `ciuda_codig` (`ciuda_codig`),
  ADD KEY `parro_codig` (`parro_codig`),
  ADD KEY `munic_codig` (`munic_codig`),
  ADD KEY `estad_codig` (`estad_codig`),
  ADD KEY `acome_codig` (`acome_codig`),
  ADD KEY `estat_codig` (`estat_codig`);

--
-- Indices de la tabla `rd_preregistro_configuracion_equipo`
--
ALTER TABLE `rd_preregistro_configuracion_equipo`
  ADD PRIMARY KEY (`cequi_codig`),
  ADD KEY `prere_codig` (`prere_codig`),
  ADD KEY `opera_codig` (`otele_codig`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `rd_preregistro_documento_digital`
--
ALTER TABLE `rd_preregistro_documento_digital`
  ADD PRIMARY KEY (`docum_codig`),
  ADD KEY `prere_codig` (`prere_codig`),
  ADD KEY `dtipo_codig` (`dtipo_codig`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `rd_preregistro_documento_digital_tipo`
--
ALTER TABLE `rd_preregistro_documento_digital_tipo`
  ADD PRIMARY KEY (`dtipo_codig`),
  ADD UNIQUE KEY `dtipo_descr` (`dtipo_descr`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `rd_preregistro_representante_legal`
--
ALTER TABLE `rd_preregistro_representante_legal`
  ADD PRIMARY KEY (`rlega_codig`),
  ADD KEY `usuar_codig` (`usuar_codig`),
  ADD KEY `tdocu_codig` (`tdocu_codig`),
  ADD KEY `prere_codig` (`prere_codig`);

--
-- Indices de la tabla `registro`
--
ALTER TABLE `registro`
  ADD PRIMARY KEY (`regis_codig`),
  ADD KEY `usuar_codig` (`usuar_codig`),
  ADD KEY `cont2_gener` (`cont2_gener`),
  ADD KEY `cont1_gener` (`cont1_gener`),
  ADD KEY `eregi_codig` (`eregi_codig`) USING BTREE;

--
-- Indices de la tabla `registro_estatu`
--
ALTER TABLE `registro_estatu`
  ADD PRIMARY KEY (`eregi_codig`),
  ADD UNIQUE KEY `eregi_descr` (`eregi_descr`);

--
-- Indices de la tabla `registro_movimiento`
--
ALTER TABLE `registro_movimiento`
  ADD PRIMARY KEY (`mregi_codig`),
  ADD KEY `regis_codig` (`regis_codig`),
  ADD KEY `eregi_codig` (`eregi_codig`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `seguridad_permisos_acceso`
--
ALTER TABLE `seguridad_permisos_acceso`
  ADD PRIMARY KEY (`spacc_codig`),
  ADD UNIQUE KEY `spacc_descr` (`spacc_descr`);

--
-- Indices de la tabla `seguridad_permisos_nivel`
--
ALTER TABLE `seguridad_permisos_nivel`
  ADD PRIMARY KEY (`spniv_codig`),
  ADD UNIQUE KEY `spniv_descr` (`spniv_descr`);

--
-- Indices de la tabla `seguridad_permisos_roles`
--
ALTER TABLE `seguridad_permisos_roles`
  ADD PRIMARY KEY (`sprol_codig`),
  ADD KEY `usuar_codig` (`srole_codig`),
  ADD KEY `modul_codig` (`modul_codig`),
  ADD KEY `spacc_codig` (`spacc_codig`),
  ADD KEY `spniv_codig` (`spniv_codig`);

--
-- Indices de la tabla `seguridad_permisos_usuarios`
--
ALTER TABLE `seguridad_permisos_usuarios`
  ADD PRIMARY KEY (`spusu_codig`),
  ADD KEY `usuar_codig` (`usuar_codig`),
  ADD KEY `modul_codig` (`modul_codig`),
  ADD KEY `spacc_codig` (`spacc_codig`),
  ADD KEY `spniv_codig` (`spniv_codig`);

--
-- Indices de la tabla `seguridad_roles`
--
ALTER TABLE `seguridad_roles`
  ADD PRIMARY KEY (`srole_codig`),
  ADD UNIQUE KEY `srole_descr` (`srole_descr`);

--
-- Indices de la tabla `sequence_data`
--
ALTER TABLE `sequence_data`
  ADD PRIMARY KEY (`sequence_name`);

--
-- Indices de la tabla `servicios`
--
ALTER TABLE `servicios`
  ADD PRIMARY KEY (`servi_codig`),
  ADD UNIQUE KEY `servi_descr` (`servi_descr`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `solicitud_origen`
--
ALTER TABLE `solicitud_origen`
  ADD PRIMARY KEY (`orige_codig`),
  ADD UNIQUE KEY `orige_descr` (`orige_descr`),
  ADD KEY `usuar_codig` (`usuar_codig`);

--
-- Indices de la tabla `tipo_cliente_documento`
--
ALTER TABLE `tipo_cliente_documento`
  ADD PRIMARY KEY (`tcdoc_codig`),
  ADD UNIQUE KEY `tclie_codig_2` (`tclie_codig`,`tdocu_codig`),
  ADD KEY `tclie_codig` (`tclie_codig`),
  ADD KEY `tdocu_codig` (`tdocu_codig`);

--
-- Indices de la tabla `trabajador`
--
ALTER TABLE `trabajador`
  ADD PRIMARY KEY (`traba_codig`),
  ADD KEY `perso_codig` (`perso_codig`),
  ADD KEY `ttipo_codig` (`ttipo_codig`),
  ADD KEY `banco_value` (`banco_value`),
  ADD KEY `usuar_codig` (`usuar_codig`),
  ADD KEY `tcuen_tipo` (`tcuen_codig`),
  ADD KEY `etrab_codig` (`etrab_codig`);

--
-- Indices de la tabla `trabajador_estatus`
--
ALTER TABLE `trabajador_estatus`
  ADD PRIMARY KEY (`etrab_codig`),
  ADD UNIQUE KEY `etrab_descr` (`etrab_descr`);

--
-- Indices de la tabla `trabajador_tipo`
--
ALTER TABLE `trabajador_tipo`
  ADD PRIMARY KEY (`ttipo_codig`),
  ADD UNIQUE KEY `ttipo_descr_UNIQUE` (`ttipo_descr`);

--
-- Indices de la tabla `unidad_tipo`
--
ALTER TABLE `unidad_tipo`
  ADD PRIMARY KEY (`tunid_codig`),
  ADD UNIQUE KEY `tunid_descr_UNIQUE` (`tunid_descr`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`usuar_codig`),
  ADD UNIQUE KEY `perso_codig` (`perso_codig`),
  ADD KEY `uesta_codig` (`uesta_codig`),
  ADD KEY `urole_codig` (`urole_codig`);

--
-- Indices de la tabla `usuarios_estatus`
--
ALTER TABLE `usuarios_estatus`
  ADD PRIMARY KEY (`uesta_codig`),
  ADD UNIQUE KEY `uesta_descr` (`uesta_descr`);

--
-- Indices de la tabla `usuarios_roles`
--
ALTER TABLE `usuarios_roles`
  ADD PRIMARY KEY (`urole_codig`),
  ADD UNIQUE KEY `roles_descr` (`urole_descr`);

--
-- Indices de la tabla `venta_tipo`
--
ALTER TABLE `venta_tipo`
  ADD PRIMARY KEY (`tvent_codig`),
  ADD UNIQUE KEY `tvent_descr` (`tvent_descr`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `actividad_comercial`
--
ALTER TABLE `actividad_comercial`
  MODIFY `acome_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `banco`
--
ALTER TABLE `banco`
  MODIFY `banco_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT de la tabla `banco_tipo_cuenta`
--
ALTER TABLE `banco_tipo_cuenta`
  MODIFY `tcuen_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `ciudades`
--
ALTER TABLE `ciudades`
  MODIFY `ciuda_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=523;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `clien_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `cliente_tipo`
--
ALTER TABLE `cliente_tipo`
  MODIFY `tclie_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `cobranza_tipo`
--
ALTER TABLE `cobranza_tipo`
  MODIFY `ctipo_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `colegio`
--
ALTER TABLE `colegio`
  MODIFY `coleg_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=312;

--
-- AUTO_INCREMENT de la tabla `colegio_contacto`
--
ALTER TABLE `colegio_contacto`
  MODIFY `ccole_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT de la tabla `depositos`
--
ALTER TABLE `depositos`
  MODIFY `depos_codig` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=208;

--
-- AUTO_INCREMENT de la tabla `depositos_validar`
--
ALTER TABLE `depositos_validar`
  MODIFY `valid_codig` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `documento_tipo`
--
ALTER TABLE `documento_tipo`
  MODIFY `tdocu_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `egresos`
--
ALTER TABLE `egresos`
  MODIFY `egres_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `empresa_tipo`
--
ALTER TABLE `empresa_tipo`
  MODIFY `etipo_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `estado_civil`
--
ALTER TABLE `estado_civil`
  MODIFY `ecivi_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `estatus`
--
ALTER TABLE `estatus`
  MODIFY `estat_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT de la tabla `estatus_validar`
--
ALTER TABLE `estatus_validar`
  MODIFY `estval_codig` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `factu_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `factura_estatu`
--
ALTER TABLE `factura_estatu`
  MODIFY `efact_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `factura_estatu_pago`
--
ALTER TABLE `factura_estatu_pago`
  MODIFY `efpag_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `factura_producto`
--
ALTER TABLE `factura_producto`
  MODIFY `fprod_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT de la tabla `genero`
--
ALTER TABLE `genero`
  MODIFY `gener_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `impuesto_iva`
--
ALTER TABLE `impuesto_iva`
  MODIFY `iva_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `inventario`
--
ALTER TABLE `inventario`
  MODIFY `inven_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `inventario_entrada`
--
ALTER TABLE `inventario_entrada`
  MODIFY `ientr_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `inventario_entrada_estatus`
--
ALTER TABLE `inventario_entrada_estatus`
  MODIFY `ieest_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `inventario_entrada_producto`
--
ALTER TABLE `inventario_entrada_producto`
  MODIFY `iepro_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT de la tabla `inventario_salida`
--
ALTER TABLE `inventario_salida`
  MODIFY `isali_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `inventario_salida_estatus`
--
ALTER TABLE `inventario_salida_estatus`
  MODIFY `isest_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `inventario_salida_producto`
--
ALTER TABLE `inventario_salida_producto`
  MODIFY `ispro_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT de la tabla `meses`
--
ALTER TABLE `meses`
  MODIFY `meses_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `modulos`
--
ALTER TABLE `modulos`
  MODIFY `modul_codig` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9001;

--
-- AUTO_INCREMENT de la tabla `modulos_estatus`
--
ALTER TABLE `modulos_estatus`
  MODIFY `emodu_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `moneda`
--
ALTER TABLE `moneda`
  MODIFY `moned_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `municipio`
--
ALTER TABLE `municipio`
  MODIFY `munic_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=463;

--
-- AUTO_INCREMENT de la tabla `nacionalidad`
--
ALTER TABLE `nacionalidad`
  MODIFY `nacio_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `nomina`
--
ALTER TABLE `nomina`
  MODIFY `nomin_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `nomina_asignacion`
--
ALTER TABLE `nomina_asignacion`
  MODIFY `asign_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `nomina_concepto`
--
ALTER TABLE `nomina_concepto`
  MODIFY `conce_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `nomina_concepto_tipo`
--
ALTER TABLE `nomina_concepto_tipo`
  MODIFY `tconc_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `nomina_estatus`
--
ALTER TABLE `nomina_estatus`
  MODIFY `enomi_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `nomina_periodo`
--
ALTER TABLE `nomina_periodo`
  MODIFY `perio_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `nomina_periodo_estatus`
--
ALTER TABLE `nomina_periodo_estatus`
  MODIFY `eperi_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `nomina_resumen`
--
ALTER TABLE `nomina_resumen`
  MODIFY `resum_codig` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `nomina_tipo`
--
ALTER TABLE `nomina_tipo`
  MODIFY `tnomi_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `nomina_tipo_pago`
--
ALTER TABLE `nomina_tipo_pago`
  MODIFY `pnomi_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `operador_telefonico`
--
ALTER TABLE `operador_telefonico`
  MODIFY `otele_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `organigrama`
--
ALTER TABLE `organigrama`
  MODIFY `organ_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `organigrama_nivel`
--
ALTER TABLE `organigrama_nivel`
  MODIFY `onive_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `pago_tipo`
--
ALTER TABLE `pago_tipo`
  MODIFY `ptipo_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `paises`
--
ALTER TABLE `paises`
  MODIFY `paise_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=241;

--
-- AUTO_INCREMENT de la tabla `parroquia`
--
ALTER TABLE `parroquia`
  MODIFY `parro_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1139;

--
-- AUTO_INCREMENT de la tabla `pedido_corte`
--
ALTER TABLE `pedido_corte`
  MODIFY `corte_codig` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pedido_corte_estatu`
--
ALTER TABLE `pedido_corte_estatu`
  MODIFY `ecort_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `pedido_costo`
--
ALTER TABLE `pedido_costo`
  MODIFY `costo_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `pedido_deduccion`
--
ALTER TABLE `pedido_deduccion`
  MODIFY `deduc_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `pedido_detalle_listado_imagen`
--
ALTER TABLE `pedido_detalle_listado_imagen`
  MODIFY `pdlim_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=714;

--
-- AUTO_INCREMENT de la tabla `pedido_diseno`
--
ALTER TABLE `pedido_diseno`
  MODIFY `disen_codig` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pedido_emoji`
--
ALTER TABLE `pedido_emoji`
  MODIFY `emoji_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT de la tabla `pedido_estatu`
--
ALTER TABLE `pedido_estatu`
  MODIFY `epedi_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `pedido_estatu_pago`
--
ALTER TABLE `pedido_estatu_pago`
  MODIFY `epago_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `pedido_fuente`
--
ALTER TABLE `pedido_fuente`
  MODIFY `fuent_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `pedido_incluye`
--
ALTER TABLE `pedido_incluye`
  MODIFY `inclu_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `pedido_lista`
--
ALTER TABLE `pedido_lista`
  MODIFY `lista_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `pedido_modelo`
--
ALTER TABLE `pedido_modelo`
  MODIFY `model_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `pedido_modelo_categoria`
--
ALTER TABLE `pedido_modelo_categoria`
  MODIFY `mcate_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `pedido_modelo_color`
--
ALTER TABLE `pedido_modelo_color`
  MODIFY `mcolo_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT de la tabla `pedido_modelo_conjunto`
--
ALTER TABLE `pedido_modelo_conjunto`
  MODIFY `mconj_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `pedido_modelo_opcional`
--
ALTER TABLE `pedido_modelo_opcional`
  MODIFY `mopci_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `pedido_modelo_tipo`
--
ALTER TABLE `pedido_modelo_tipo`
  MODIFY `tmode_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `pedido_modelo_variante`
--
ALTER TABLE `pedido_modelo_variante`
  MODIFY `mvari_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `pedido_pagos`
--
ALTER TABLE `pedido_pagos`
  MODIFY `pagos_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `pedido_pedidos`
--
ALTER TABLE `pedido_pedidos`
  MODIFY `pedid_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT de la tabla `pedido_pedidos_adicional`
--
ALTER TABLE `pedido_pedidos_adicional`
  MODIFY `adici_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de la tabla `pedido_pedidos_deduccion`
--
ALTER TABLE `pedido_pedidos_deduccion`
  MODIFY `pdedu_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `pedido_pedidos_detalle`
--
ALTER TABLE `pedido_pedidos_detalle`
  MODIFY `pdeta_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=198;

--
-- AUTO_INCREMENT de la tabla `pedido_pedidos_detalle_listado`
--
ALTER TABLE `pedido_pedidos_detalle_listado`
  MODIFY `pdlis_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=558;

--
-- AUTO_INCREMENT de la tabla `pedido_pedidos_imagen`
--
ALTER TABLE `pedido_pedidos_imagen`
  MODIFY `pimag_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=173;

--
-- AUTO_INCREMENT de la tabla `pedido_pedidos_imagen_tipo`
--
ALTER TABLE `pedido_pedidos_imagen_tipo`
  MODIFY `ptima_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `pedido_precio`
--
ALTER TABLE `pedido_precio`
  MODIFY `pprec_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `pedido_precio_electivo`
--
ALTER TABLE `pedido_precio_electivo`
  MODIFY `pelec_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `pedido_simbolo_simple`
--
ALTER TABLE `pedido_simbolo_simple`
  MODIFY `ssimp_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de la tabla `pedido_talla`
--
ALTER TABLE `pedido_talla`
  MODIFY `talla_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `pedido_talla_ajuste`
--
ALTER TABLE `pedido_talla_ajuste`
  MODIFY `tajus_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `permisos`
--
ALTER TABLE `permisos`
  MODIFY `permi_codig` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT de la tabla `permisos_estatus`
--
ALTER TABLE `permisos_estatus`
  MODIFY `eperm_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `perso_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT de la tabla `producto_tipo`
--
ALTER TABLE `producto_tipo`
  MODIFY `tprod_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `prove_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `proveedor_tipo`
--
ALTER TABLE `proveedor_tipo`
  MODIFY `tprov_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `rango_categoria`
--
ALTER TABLE `rango_categoria`
  MODIFY `ranca_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `rd_preregistro`
--
ALTER TABLE `rd_preregistro`
  MODIFY `prere_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `rd_preregistro_configuracion_equipo`
--
ALTER TABLE `rd_preregistro_configuracion_equipo`
  MODIFY `cequi_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `rd_preregistro_documento_digital`
--
ALTER TABLE `rd_preregistro_documento_digital`
  MODIFY `docum_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `rd_preregistro_documento_digital_tipo`
--
ALTER TABLE `rd_preregistro_documento_digital_tipo`
  MODIFY `dtipo_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `rd_preregistro_representante_legal`
--
ALTER TABLE `rd_preregistro_representante_legal`
  MODIFY `rlega_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `registro`
--
ALTER TABLE `registro`
  MODIFY `regis_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `registro_estatu`
--
ALTER TABLE `registro_estatu`
  MODIFY `eregi_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `registro_movimiento`
--
ALTER TABLE `registro_movimiento`
  MODIFY `mregi_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `seguridad_permisos_acceso`
--
ALTER TABLE `seguridad_permisos_acceso`
  MODIFY `spacc_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `seguridad_permisos_nivel`
--
ALTER TABLE `seguridad_permisos_nivel`
  MODIFY `spniv_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `seguridad_permisos_roles`
--
ALTER TABLE `seguridad_permisos_roles`
  MODIFY `sprol_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `seguridad_permisos_usuarios`
--
ALTER TABLE `seguridad_permisos_usuarios`
  MODIFY `spusu_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `seguridad_roles`
--
ALTER TABLE `seguridad_roles`
  MODIFY `srole_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `servicios`
--
ALTER TABLE `servicios`
  MODIFY `servi_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `solicitud_origen`
--
ALTER TABLE `solicitud_origen`
  MODIFY `orige_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipo_cliente_documento`
--
ALTER TABLE `tipo_cliente_documento`
  MODIFY `tcdoc_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `trabajador`
--
ALTER TABLE `trabajador`
  MODIFY `traba_codig` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `trabajador_estatus`
--
ALTER TABLE `trabajador_estatus`
  MODIFY `etrab_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `trabajador_tipo`
--
ALTER TABLE `trabajador_tipo`
  MODIFY `ttipo_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `unidad_tipo`
--
ALTER TABLE `unidad_tipo`
  MODIFY `tunid_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `usuar_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT de la tabla `usuarios_estatus`
--
ALTER TABLE `usuarios_estatus`
  MODIFY `uesta_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `usuarios_roles`
--
ALTER TABLE `usuarios_roles`
  MODIFY `urole_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `venta_tipo`
--
ALTER TABLE `venta_tipo`
  MODIFY `tvent_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `actividad_comercial`
--
ALTER TABLE `actividad_comercial`
  ADD CONSTRAINT `actividad_comercial_ibfk_1` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`);

--
-- Filtros para la tabla `ciudades`
--
ALTER TABLE `ciudades`
  ADD CONSTRAINT `ciudades_ibfk_1` FOREIGN KEY (`estad_codig`) REFERENCES `estado` (`estad_codig`),
  ADD CONSTRAINT `ciudades_ibfk_2` FOREIGN KEY (`paise_codig`) REFERENCES `paises` (`paise_codig`);

--
-- Filtros para la tabla `colegio_contacto`
--
ALTER TABLE `colegio_contacto`
  ADD CONSTRAINT `colegio_contacto_ibfk_1` FOREIGN KEY (`perso_codig`) REFERENCES `persona` (`perso_codig`),
  ADD CONSTRAINT `colegio_contacto_ibfk_2` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`),
  ADD CONSTRAINT `colegio_contacto_ibfk_3` FOREIGN KEY (`coleg_codig`) REFERENCES `colegio` (`coleg_codig`);

--
-- Filtros para la tabla `inventario_entrada`
--
ALTER TABLE `inventario_entrada`
  ADD CONSTRAINT `inventario_entrada_ibfk_1` FOREIGN KEY (`ieest_codig`) REFERENCES `inventario_entrada_estatus` (`ieest_codig`);

--
-- Filtros para la tabla `inventario_salida`
--
ALTER TABLE `inventario_salida`
  ADD CONSTRAINT `inventario_salida_ibfk_1` FOREIGN KEY (`isest_codig`) REFERENCES `inventario_salida_estatus` (`isest_codig`);

--
-- Filtros para la tabla `municipio`
--
ALTER TABLE `municipio`
  ADD CONSTRAINT `municipio_ibfk_1` FOREIGN KEY (`estad_codig`) REFERENCES `estado` (`estad_codig`),
  ADD CONSTRAINT `municipio_ibfk_2` FOREIGN KEY (`paise_codig`) REFERENCES `paises` (`paise_codig`);

--
-- Filtros para la tabla `nomina_concepto`
--
ALTER TABLE `nomina_concepto`
  ADD CONSTRAINT `nomina_concepto_ibfk_1` FOREIGN KEY (`nomin_codig`) REFERENCES `nomina` (`nomin_codig`),
  ADD CONSTRAINT `nomina_concepto_ibfk_2` FOREIGN KEY (`tconc_codig`) REFERENCES `nomina_concepto_tipo` (`tconc_codig`),
  ADD CONSTRAINT `nomina_concepto_ibfk_3` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`);

--
-- Filtros para la tabla `nomina_periodo`
--
ALTER TABLE `nomina_periodo`
  ADD CONSTRAINT `nomina_periodo_ibfk_1` FOREIGN KEY (`eperi_codig`) REFERENCES `nomina_periodo_estatus` (`eperi_codig`),
  ADD CONSTRAINT `nomina_periodo_ibfk_2` FOREIGN KEY (`meses_codig`) REFERENCES `meses` (`meses_codig`),
  ADD CONSTRAINT `nomina_periodo_ibfk_3` FOREIGN KEY (`nomin_codig`) REFERENCES `nomina` (`nomin_codig`),
  ADD CONSTRAINT `nomina_periodo_ibfk_4` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`);

--
-- Filtros para la tabla `nomina_resumen`
--
ALTER TABLE `nomina_resumen`
  ADD CONSTRAINT `nomina_resumen_ibfk_1` FOREIGN KEY (`nomin_codig`) REFERENCES `nomina` (`nomin_codig`),
  ADD CONSTRAINT `nomina_resumen_ibfk_2` FOREIGN KEY (`conce_codig`) REFERENCES `nomina_concepto` (`conce_codig`),
  ADD CONSTRAINT `nomina_resumen_ibfk_3` FOREIGN KEY (`perio_codig`) REFERENCES `nomina_periodo` (`perio_codig`),
  ADD CONSTRAINT `nomina_resumen_ibfk_4` FOREIGN KEY (`tconc_codig`) REFERENCES `nomina_concepto_tipo` (`tconc_codig`),
  ADD CONSTRAINT `nomina_resumen_ibfk_5` FOREIGN KEY (`traba_codig`) REFERENCES `trabajador` (`traba_codig`),
  ADD CONSTRAINT `nomina_resumen_ibfk_6` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`);

--
-- Filtros para la tabla `operador_telefonico`
--
ALTER TABLE `operador_telefonico`
  ADD CONSTRAINT `operador_telefonico_ibfk_1` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`);

--
-- Filtros para la tabla `organigrama`
--
ALTER TABLE `organigrama`
  ADD CONSTRAINT `organigrama_ibfk_1` FOREIGN KEY (`onive_codig`) REFERENCES `organigrama_nivel` (`onive_codig`);

--
-- Filtros para la tabla `parroquia`
--
ALTER TABLE `parroquia`
  ADD CONSTRAINT `parroquia_ibfk_1` FOREIGN KEY (`estad_codig`) REFERENCES `estado` (`estad_codig`),
  ADD CONSTRAINT `parroquia_ibfk_2` FOREIGN KEY (`munic_codig`) REFERENCES `municipio` (`munic_codig`),
  ADD CONSTRAINT `parroquia_ibfk_3` FOREIGN KEY (`paise_codig`) REFERENCES `paises` (`paise_codig`);

--
-- Filtros para la tabla `pedido_corte`
--
ALTER TABLE `pedido_corte`
  ADD CONSTRAINT `pedido_corte_ibfk_1` FOREIGN KEY (`ecort_codig`) REFERENCES `pedido_corte_estatu` (`ecort_codig`),
  ADD CONSTRAINT `pedido_corte_ibfk_2` FOREIGN KEY (`pdlis_codig`) REFERENCES `pedido_pedidos_detalle_listado` (`pdlis_codig`),
  ADD CONSTRAINT `pedido_corte_ibfk_3` FOREIGN KEY (`traba_codig`) REFERENCES `trabajador` (`traba_codig`),
  ADD CONSTRAINT `pedido_corte_ibfk_4` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`);

--
-- Filtros para la tabla `pedido_corte_estatu`
--
ALTER TABLE `pedido_corte_estatu`
  ADD CONSTRAINT `pedido_corte_estatu_ibfk_1` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`);

--
-- Filtros para la tabla `pedido_costo`
--
ALTER TABLE `pedido_costo`
  ADD CONSTRAINT `pedido_costo_ibfk_1` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`);

--
-- Filtros para la tabla `pedido_detalle_listado_imagen`
--
ALTER TABLE `pedido_detalle_listado_imagen`
  ADD CONSTRAINT `pedido_detalle_listado_imagen_ibfk_1` FOREIGN KEY (`pedid_codig`) REFERENCES `pedido_pedidos` (`pedid_codig`),
  ADD CONSTRAINT `pedido_detalle_listado_imagen_ibfk_2` FOREIGN KEY (`pdeta_codig`) REFERENCES `pedido_pedidos_detalle` (`pdeta_codig`),
  ADD CONSTRAINT `pedido_detalle_listado_imagen_ibfk_3` FOREIGN KEY (`pdlis_codig`) REFERENCES `pedido_pedidos_detalle_listado` (`pdlis_codig`),
  ADD CONSTRAINT `pedido_detalle_listado_imagen_ibfk_4` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`);

--
-- Filtros para la tabla `pedido_diseno`
--
ALTER TABLE `pedido_diseno`
  ADD CONSTRAINT `pedido_diseno_ibfk_1` FOREIGN KEY (`ecort_codig`) REFERENCES `pedido_corte_estatu` (`ecort_codig`),
  ADD CONSTRAINT `pedido_diseno_ibfk_2` FOREIGN KEY (`pedid_codig`) REFERENCES `pedido_pedidos` (`pedid_codig`),
  ADD CONSTRAINT `pedido_diseno_ibfk_3` FOREIGN KEY (`traba_codig`) REFERENCES `trabajador` (`traba_codig`),
  ADD CONSTRAINT `pedido_diseno_ibfk_4` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`);

--
-- Filtros para la tabla `pedido_estatu_pago`
--
ALTER TABLE `pedido_estatu_pago`
  ADD CONSTRAINT `pedido_estatu_pago_ibfk_1` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`);

--
-- Filtros para la tabla `pedido_fuente`
--
ALTER TABLE `pedido_fuente`
  ADD CONSTRAINT `pedido_fuente_ibfk_1` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`);

--
-- Filtros para la tabla `pedido_modelo`
--
ALTER TABLE `pedido_modelo`
  ADD CONSTRAINT `pedido_modelo_ibfk_1` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`);

--
-- Filtros para la tabla `pedido_modelo_categoria`
--
ALTER TABLE `pedido_modelo_categoria`
  ADD CONSTRAINT `pedido_modelo_categoria_ibfk_1` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`);

--
-- Filtros para la tabla `pedido_modelo_color`
--
ALTER TABLE `pedido_modelo_color`
  ADD CONSTRAINT `pedido_modelo_color_ibfk_1` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`),
  ADD CONSTRAINT `pedido_modelo_color_ibfk_2` FOREIGN KEY (`tmode_codig`) REFERENCES `pedido_modelo_tipo` (`tmode_codig`);

--
-- Filtros para la tabla `pedido_modelo_conjunto`
--
ALTER TABLE `pedido_modelo_conjunto`
  ADD CONSTRAINT `pedido_modelo_conjunto_ibfk_1` FOREIGN KEY (`mcate_codig`) REFERENCES `pedido_modelo_categoria` (`mcate_codig`),
  ADD CONSTRAINT `pedido_modelo_conjunto_ibfk_2` FOREIGN KEY (`model_codig`) REFERENCES `pedido_modelo` (`model_codig`),
  ADD CONSTRAINT `pedido_modelo_conjunto_ibfk_3` FOREIGN KEY (`mvari_codig`) REFERENCES `pedido_modelo_variante` (`mvari_codig`),
  ADD CONSTRAINT `pedido_modelo_conjunto_ibfk_4` FOREIGN KEY (`tmode_codig`) REFERENCES `pedido_modelo_tipo` (`tmode_codig`),
  ADD CONSTRAINT `pedido_modelo_conjunto_ibfk_5` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`);

--
-- Filtros para la tabla `pedido_modelo_opcional`
--
ALTER TABLE `pedido_modelo_opcional`
  ADD CONSTRAINT `pedido_modelo_opcional_ibfk_1` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`);

--
-- Filtros para la tabla `pedido_modelo_tipo`
--
ALTER TABLE `pedido_modelo_tipo`
  ADD CONSTRAINT `pedido_modelo_tipo_ibfk_1` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`);

--
-- Filtros para la tabla `pedido_modelo_variante`
--
ALTER TABLE `pedido_modelo_variante`
  ADD CONSTRAINT `pedido_modelo_variante_ibfk_1` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`);

--
-- Filtros para la tabla `pedido_pagos`
--
ALTER TABLE `pedido_pagos`
  ADD CONSTRAINT `pedido_pagos_ibfk_1` FOREIGN KEY (`epago_codig`) REFERENCES `pedido_estatu_pago` (`epago_codig`),
  ADD CONSTRAINT `pedido_pagos_ibfk_2` FOREIGN KEY (`pedid_codig`) REFERENCES `pedido_pedidos` (`pedid_codig`),
  ADD CONSTRAINT `pedido_pagos_ibfk_3` FOREIGN KEY (`ptipo_codig`) REFERENCES `pago_tipo` (`ptipo_codig`),
  ADD CONSTRAINT `pedido_pagos_ibfk_4` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`);

--
-- Filtros para la tabla `pedido_pedidos`
--
ALTER TABLE `pedido_pedidos`
  ADD CONSTRAINT `pedido_pedidos_ibfk_1` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`),
  ADD CONSTRAINT `pedido_pedidos_ibfk_2` FOREIGN KEY (`epedi_codig`) REFERENCES `pedido_estatu` (`epedi_codig`);

--
-- Filtros para la tabla `pedido_pedidos_adicional`
--
ALTER TABLE `pedido_pedidos_adicional`
  ADD CONSTRAINT `pedido_pedidos_adicional_ibfk_1` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`),
  ADD CONSTRAINT `pedido_pedidos_adicional_ibfk_2` FOREIGN KEY (`costo_codig`) REFERENCES `pedido_costo` (`costo_codig`),
  ADD CONSTRAINT `pedido_pedidos_adicional_ibfk_3` FOREIGN KEY (`pedid_codig`) REFERENCES `pedido_pedidos` (`pedid_codig`);

--
-- Filtros para la tabla `pedido_pedidos_deduccion`
--
ALTER TABLE `pedido_pedidos_deduccion`
  ADD CONSTRAINT `pedido_pedidos_deduccion_ibfk_1` FOREIGN KEY (`deduc_codig`) REFERENCES `pedido_deduccion` (`deduc_codig`),
  ADD CONSTRAINT `pedido_pedidos_deduccion_ibfk_2` FOREIGN KEY (`pedid_codig`) REFERENCES `pedido_pedidos` (`pedid_codig`),
  ADD CONSTRAINT `pedido_pedidos_deduccion_ibfk_3` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`);

--
-- Filtros para la tabla `pedido_pedidos_detalle_listado`
--
ALTER TABLE `pedido_pedidos_detalle_listado`
  ADD CONSTRAINT `pedido_pedidos_detalle_listado_ibfk_1` FOREIGN KEY (`talla_codig`) REFERENCES `pedido_talla` (`talla_codig`),
  ADD CONSTRAINT `pedido_pedidos_detalle_listado_ibfk_2` FOREIGN KEY (`pedid_codig`) REFERENCES `pedido_pedidos` (`pedid_codig`),
  ADD CONSTRAINT `pedido_pedidos_detalle_listado_ibfk_3` FOREIGN KEY (`pdeta_codig`) REFERENCES `pedido_pedidos_detalle` (`pdeta_codig`);

--
-- Filtros para la tabla `pedido_pedidos_imagen`
--
ALTER TABLE `pedido_pedidos_imagen`
  ADD CONSTRAINT `pedido_pedidos_imagen_ibfk_1` FOREIGN KEY (`pedid_codig`) REFERENCES `pedido_pedidos` (`pedid_codig`),
  ADD CONSTRAINT `pedido_pedidos_imagen_ibfk_2` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`),
  ADD CONSTRAINT `pedido_pedidos_imagen_ibfk_3` FOREIGN KEY (`ptima_codig`) REFERENCES `pedido_pedidos_imagen_tipo` (`ptima_codig`);

--
-- Filtros para la tabla `pedido_pedidos_imagen_tipo`
--
ALTER TABLE `pedido_pedidos_imagen_tipo`
  ADD CONSTRAINT `pedido_pedidos_imagen_tipo_ibfk_1` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`);

--
-- Filtros para la tabla `pedido_precio`
--
ALTER TABLE `pedido_precio`
  ADD CONSTRAINT `pedido_precio_ibfk_1` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`);

--
-- Filtros para la tabla `pedido_precio_electivo`
--
ALTER TABLE `pedido_precio_electivo`
  ADD CONSTRAINT `pedido_precio_electivo_ibfk_1` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`);

--
-- Filtros para la tabla `pedido_talla`
--
ALTER TABLE `pedido_talla`
  ADD CONSTRAINT `pedido_talla_ibfk_1` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`);

--
-- Filtros para la tabla `permisos`
--
ALTER TABLE `permisos`
  ADD CONSTRAINT `permisos_ibfk_1` FOREIGN KEY (`urole_codig`) REFERENCES `usuarios_roles` (`urole_codig`),
  ADD CONSTRAINT `permisos_ibfk_2` FOREIGN KEY (`modul_codig`) REFERENCES `modulos` (`modul_codig`);

--
-- Filtros para la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD CONSTRAINT `proveedor_ibfk_1` FOREIGN KEY (`tclie_codig`) REFERENCES `proveedor_tipo` (`tprov_codig`);

--
-- Filtros para la tabla `rd_preregistro_documento_digital_tipo`
--
ALTER TABLE `rd_preregistro_documento_digital_tipo`
  ADD CONSTRAINT `rd_preregistro_documento_digital_tipo_ibfk_1` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`);

--
-- Filtros para la tabla `registro`
--
ALTER TABLE `registro`
  ADD CONSTRAINT `registro_ibfk_1` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`),
  ADD CONSTRAINT `registro_ibfk_2` FOREIGN KEY (`eregi_codig`) REFERENCES `registro_estatu` (`eregi_codig`);

--
-- Filtros para la tabla `seguridad_permisos_roles`
--
ALTER TABLE `seguridad_permisos_roles`
  ADD CONSTRAINT `seguridad_permisos_roles_ibfk_1` FOREIGN KEY (`modul_codig`) REFERENCES `modulos` (`modul_codig`),
  ADD CONSTRAINT `seguridad_permisos_roles_ibfk_2` FOREIGN KEY (`spacc_codig`) REFERENCES `seguridad_permisos_acceso` (`spacc_codig`),
  ADD CONSTRAINT `seguridad_permisos_roles_ibfk_3` FOREIGN KEY (`spniv_codig`) REFERENCES `seguridad_permisos_nivel` (`spniv_codig`),
  ADD CONSTRAINT `seguridad_permisos_roles_ibfk_4` FOREIGN KEY (`srole_codig`) REFERENCES `seguridad_roles` (`srole_codig`);

--
-- Filtros para la tabla `seguridad_permisos_usuarios`
--
ALTER TABLE `seguridad_permisos_usuarios`
  ADD CONSTRAINT `seguridad_permisos_usuarios_ibfk_1` FOREIGN KEY (`modul_codig`) REFERENCES `modulos` (`modul_codig`),
  ADD CONSTRAINT `seguridad_permisos_usuarios_ibfk_2` FOREIGN KEY (`spacc_codig`) REFERENCES `seguridad_permisos_acceso` (`spacc_codig`),
  ADD CONSTRAINT `seguridad_permisos_usuarios_ibfk_3` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`),
  ADD CONSTRAINT `seguridad_permisos_usuarios_ibfk_4` FOREIGN KEY (`spniv_codig`) REFERENCES `seguridad_permisos_nivel` (`spniv_codig`);

--
-- Filtros para la tabla `solicitud_origen`
--
ALTER TABLE `solicitud_origen`
  ADD CONSTRAINT `solicitud_origen_ibfk_1` FOREIGN KEY (`usuar_codig`) REFERENCES `usuarios` (`usuar_codig`);

--
-- Filtros para la tabla `tipo_cliente_documento`
--
ALTER TABLE `tipo_cliente_documento`
  ADD CONSTRAINT `tipo_cliente_documento_ibfk_1` FOREIGN KEY (`tclie_codig`) REFERENCES `cliente_tipo` (`tclie_codig`),
  ADD CONSTRAINT `tipo_cliente_documento_ibfk_2` FOREIGN KEY (`tdocu_codig`) REFERENCES `documento_tipo` (`tdocu_codig`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
