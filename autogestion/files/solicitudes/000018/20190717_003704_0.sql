-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 16-07-2019 a las 18:51:57
-- Versión del servidor: 10.1.40-MariaDB-0ubuntu0.18.04.1
-- Versión de PHP: 7.2.19-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `rapidpago`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitud_terminal`
--

CREATE TABLE `solicitud_terminal` (
  `termi_codig` int(11) NOT NULL,
  `solic_codig` int(11) NOT NULL,
  `termi_numer` int(11) NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `termi_fcrea` date NOT NULL,
  `termi_hcrea` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `solicitud_terminal`
--
ALTER TABLE `solicitud_terminal`
  ADD PRIMARY KEY (`termi_codig`),
  ADD KEY `usuar_codig` (`usuar_codig`),
  ADD KEY `solicitud_terminal_ibfk_1` (`solic_codig`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `solicitud_terminal`
--
ALTER TABLE `solicitud_terminal`
  MODIFY `termi_codig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `solicitud_terminal`
--
ALTER TABLE `solicitud_terminal`
  ADD CONSTRAINT `solicitud_terminal_ibfk_1` FOREIGN KEY (`solic_codig`) REFERENCES `solicitud` (`solic_codig`),
  ADD CONSTRAINT `solicitud_terminal_ibfk_2` FOREIGN KEY (`usuar_codig`) REFERENCES `seguridad_usuarios` (`usuar_codig`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
