<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Ayuda</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Ayuda</a></li>
            <li><a href="#">Banco</a></li>
            <li><a href="#">Solicitudes Certificadas por el Banco</a></li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>


 <div class="panel panel-default">
                     <div class="panel-heading">Ayuda</div>

                  <div class="panel-wrapper collapse in">
                     <div class="panel-body">
                        <h3 class="box-title">Solicitudes Certificadas por el Banco</h3>
                        <p>
                           Opción del Sistema que permite impimir reporte en el cual muestra listado de solicitudes que fueron certificadas satisfactoriamente por el banco, tal cmo se muestra enla siguiente imagén:</p>
                        <img src='/files/ayuda/banco/r_certificacion.png' class='img-responsive center-block'>
                        <p>
                            Se debera tildar el boron "Imprimir", automaticmente se genera el reporte con todas las solicitudes, como se muestra en la siguiente imagen:
                        </p>
                        <img src='/files/ayuda/banco/pdf_certificacion.png' class='img-responsive center-block'>
                       
                     </div>
                  </div>
                  <div class="panel-footer">
                    <div class="row">
                      <div class="col-xs-6"><a href="cargaParametros" class="btn btn-info">Cargar Parámetros</a></div>
                      <div class="col-xs-6"><a href="reporteConfirmacion" class="btn btn-success" style="float:right;">Reporte/Parametros Confirmadas por el Banco</a></div>
                    </div>
                 </div>
