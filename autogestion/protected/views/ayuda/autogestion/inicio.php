<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Ayuda</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Ayuda</a></li>
            <li><a href="#">Autogestión</a></li>
            <li><a href="#">Inicio</a></li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

 <div class="panel panel-default">
                     <div class="panel-heading">Ayuda</div>

                  <div class="panel-wrapper collapse in">
                     <div class="panel-body">
                        <h3 class="box-title">Inicio</h3>
                    <p>
                     Una ves haya iniciado sesión en el sistema, podrá ver el escritorio de trabajo el cual muestra estadísticas donde se representan los avances o estatus de las solicitudes, tal como se ve en la siguiente imagen:
                  </p>
                  <img src='/files/ayuda/autogestion/bandeja.png' class='img-responsive center-block'> 
                     </div>
                  </div>
                  <div class="panel-footer">
                    <div class="row">
                      <div class="col-xs-6"><a href="login" class="btn btn-success">Login</a></div>
                      <div class="col-xs-6"><a href="registroDocumentacion" class="btn btn-success" style="float:right;">Registro y Documentación</a></div>
                    </div>
                 </div>