<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Ayuda</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Ayuda</a></li>
            <li><a href="#">Autogestión</a></li>
            <li><a href="#">Datos de Afiliación y Dispositivos</a></li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>


 <div class="panel panel-default">
                     <div class="panel-heading">Ayuda</div>

                  <div class="panel-wrapper collapse in">
                     <div class="panel-body">
                        <h3 class="box-title">Datos de Afiliación y Dispositivos</h3>
                        <p>
                           Esta opción permite registrar los datos del operador de telefonia a configurar en el equipo y la cantidad requerida. se pueden asignar varios operadores en la misma pantalla.
                         </p>
                        <img src='/files/ayuda/autogestion/datos_afiliacion.png' class='img-responsive center-block'>
                        <p>
                            Los Campos que se deben llenar del formulario son los siguientes:
                        </p>
                        <ul>
                          <li><strong>Operador Telefónico *:</strong> indica el proveedor inalámbrico o compañía de teléfono que proporciona servicios para los usuarios de teléfonos móviles.</li>
                           <li><strong>Cantidad *: </strong> indica la cantidad de equipos solicitados por operador móvil.</li>
                           <li><strong>Más *: </strong> Botón que permite agregar más items a la solicitud.</li>
                          </ul>
                        <p>
                           Una vez se hayan completados los campos se tilda el botón siguiente, inmediatamente el sistema te envía al siguiente paso para continuar el registro.
                        </p>
                       
                     </div>
                  </div>
                  <div class="panel-footer">
                    <div class="row">
                      <div class="col-xs-6"><a href="preRegistro" class="btn btn-info">Pre Registro</a></div>
                      <div class="col-xs-6"><a href="representanteLegal" class="btn btn-success" style="float:right;">Representantes Legales</a></div>
                    </div>
                 </div>
