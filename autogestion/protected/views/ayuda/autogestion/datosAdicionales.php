<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Ayuda</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Ayuda</a></li>
            <li><a href="#">Autogestión</a></li>
            <li><a href="#">Datos Adicionales</a></li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>


 <div class="panel panel-default">
                     <div class="panel-heading">Ayuda</div>

                  <div class="panel-wrapper collapse in">
                     <div class="panel-body">
                        <h3 class="box-title">Datos Adicionales</h3>
                        <p>
                           Opción que permite Registrar Datos Adicionales que serviran de referencias para el personal de RapidPago.
                         </p>
                        <img src='/files/ayuda/autogestion/datos_adicionales.png' class='img-responsive center-block'>
                           <p>
                          Los datos que se deben llenar del formulario son los siguientes:
                          </p>
                           <ul>
                              <li><strong>Fecha de solicitud *: </strong> indica la fecha del día en el que se está registrando la solicitud.</li>
                              <li><strong>Origen *: </strong> por defecto el origen es Autogestión.</li>
                              <li><strong>Observaciones:</strong> permite registrar cualquier comentario u observación adicional de la solicitud. </li>
                           </ul>
                     </div>
                  </div>
                  <div class="panel-footer">
                    <div class="row">
                      <div class="col-xs-6"><a href="representanteLegal" class="btn btn-info">Representantes Legales</a></div>
                      <div class="col-xs-6"><a href="documentosDigitales" class="btn btn-success" style="float:right;">Ducumentos Digitales</a></div>
                    </div>
                 </div>
