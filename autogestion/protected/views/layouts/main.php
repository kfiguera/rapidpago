<!DOCTYPE html>  
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/img/favicon.png">
<title><?php echo CHtml::encode($this->pageTitle); ?></title>

<!-- Bootstrap Core CSS -->
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
<!-- toast CSS -->
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
<!-- morris CSS -->
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/morrisjs/morris.css" rel="stylesheet">

<!-- Form Validation CSS -->
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/form-validation/formValidation.min.css" id="theme"  rel="stylesheet">
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/form-validation/demo.css" id="theme"  rel="stylesheet">
<!-- DataTables CSS -->
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/datatables/dataTables.bootstrap.min.css" rel="stylesheet" >

<!--alerts CSS -->
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
<!-- Page plugins css -->
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
<!-- Color picker plugins css -->
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/jquery-asColorPicker-master/css/asColorPicker.css" rel="stylesheet">
<!-- Date picker plugins css -->
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Daterange picker plugins css -->
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/custom-select/custom-select.css" rel="stylesheet">
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet">

<!-- icheck CSS -->
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/icheck/skins/all.css" rel="stylesheet">
<!-- Wizard CSS -->
    <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/jquery-wizard-master/css/wizard.css" rel="stylesheet">
    
<!-- animation CSS -->
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/css/animate.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/css/colors/blue.css" id="theme"  rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<style type="text/css">
  .logo{
     display: table-cell;
    vertical-align: middle;
  }
  .logo-img{
    max-height: 50px;
  }
  .top-left-part{
    display: table;
    height: 60px;
  }
</style>
<style type="text/css">
    .badge {
      word-wrap: break-word;  
    }
    table {
        word-wrap: break-word;
    }
</style>
<style>
    .image-preview-input {
        position: relative;
        overflow: hidden;
        margin: 0px;    
        color: #333;
        background-color: #fff;
        border-color: #ccc;    
    }
    .image-preview-input input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }
    .image-preview-input-title {
        margin-left:2px;
    }
</style> 

<style type="text/css">
    .form-control {
        padding-right: 12px !important;
    }
</style>
<!-- jQuery -->
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>

<!--Mask -->
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/jquery.mask.js"></script>
<!-- Date Picker Plugin JavaScript -->
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<!-- icheck -->
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/icheck/icheck.min.js"></script>
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/icheck/icheck.init.js"></script>
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/blockUI/jquery.blockUI.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-122642309-7"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-122642309-7');
</script>

</head>
<body style='opacity: 1' class="fix-header fix-sidebar">
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Navigation -->
  <?php
    if (!Yii::app()->user->isGuest) {
  ?>
  <nav class="navbar navbar-default navbar-static-top m-b-0">
    <div class="navbar-header"> 
      <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
      <div class="top-left-part">
        <a class="logo" href="<?php echo Yii::app()->request->getBaseUrl(true)?>/site/inicio">
          
            <img src="https://autogestion.rapidpago.com/assets/img/logo.png" alt="home" class="img-responsive center-block logo-img" />

          
            
        </a>
      </div>
      <ul class="nav navbar-top-links navbar-left hidden-xs">
        <li>
          <a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light">
            <i class="fa fa-chevron-left ti-menu"></i>
          </a>
        </li>
        
      </ul>
      <ul class="nav navbar-top-links navbar-right pull-right">
        <li>
          <a href="<?php echo Yii::app()->request->getBaseUrl(true)?>/site/logout" class="">
            <i class="fa fa-power-off"></i> Salir
          </a>
        </li>
        <li class="dropdown"> 
          <a class="dropdown-toggle waves-effect waves-light profile-pic" data-toggle="dropdown" href="#" aria-expanded="true"> 
              <img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/img/user.png" alt="user-img" width="36" class="img-circle"><b class="hidden-xs"><?php echo Yii::app()->user->id["usuario"]["nombre"]?></b> </a>
          <ul class="dropdown-menu dropdown-user scale-up animated bounceInDown">
            <li><a href="<?php echo Yii::app()->request->getBaseUrl(true)?>/parametros/usuario/cambiarclave"><i class="ti-lock"></i> Cambiar Contraseña</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?php echo Yii::app()->request->getBaseUrl(true)?>/site/logout"><i class="fa fa-power-off"></i> Salir</a></li>          </ul>
          <!-- /.dropdown-user -->
        </li>

        
      </ul>
    </div>
    <!-- /.navbar-header -->
    <!-- /.navbar-top-links -->
    <!-- /.navbar-static-side -->
  </nav>
  <!-- Left navbar-header -->
  
  <div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar" style="overflow: visible;">
      
      <ul class="nav" id="side-menu">
        <!--li class="user-pro"> <a href="#" class="waves-effect"><img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/images/users/varun.jpg" alt="user-img" class="img-circle"> <span class="hide-menu"> <?php echo Yii::app()->user->id["usuario"]["nombre"]?><span class="fa arrow"></span></span></a>
          <ul class="nav nav-second-level" aria-expanded="false" style="height: 0px;">
            <li><a href="javascript:void(0)"><i class="ti-user"></i> My Profile</a></li>
            <li><a href="javascript:void(0)"><i class="ti-wallet"></i> My Balance</a></li>
            <li><a href="javascript:void(0)"><i class="ti-email"></i> Inbox</a></li>
            <li><a href="javascript:void(0)"><i class="ti-settings"></i> Account Setting</a></li>
            <li><a href="<?php echo Yii::app()->request->getBaseUrl(true)?>/site/logout"><i class="fa fa-power-off"></i> Salir</a></li>
          </ul>
        </li>
        <li class="nav-small-cap m-t-10">--- Main Menu</li-->
        <?php

          $conexion = Yii::app()->db;
          $sql="SELECT urole_codig FROM seguridad_usuarios where usuar_codig='".Yii::app()->user->id['usuario']['codigo']."'";
          $datos = $conexion->createCommand($sql)->queryRow();
          
          $permiso=$datos['urole_codig'];
          
          $sql="SELECT a.modul_codig, a.modul_padre, a.modul_descr, a.modul_ruta, a.modul_orden, 
                  a.modul_bpadr, b.permi_bborr, b.permi_bperm, a.modul_icono 
                FROM seguridad_modulos a
                JOIN p_permisos b ON (a.modul_codig = b.modul_codig)
                WHERE a.modul_padre='0' 
                  AND b.urole_codig='".$permiso."'
                GROUP BY a.modul_codig, a.modul_padre, a.modul_descr, a.modul_ruta, a.modul_orden, a.modul_bpadr, b.permi_bborr, b.permi_bperm, a.modul_icono 
                ORDER by modul_orden asc";

          $menu = $conexion->createCommand($sql)->queryAll();
          foreach ($menu as $key => $value) {
            if($value['permi_bborr']!=true and $value['permi_bperm']==true ){
              $icono='fa fa-gear';
              if($value['modul_icono']!=null){
                $icono=$value['modul_icono'];
              }
              
              if($value['modul_bpadr']){
                $html.= '<li>
                  <a href="#" class="waves-effect">
                    <i class="'.$icono.'"> </i> 
                    <span class="hide-menu">
                      '.$value['modul_descr'].'
                      <span class="fa arrow"></span>
                    </span>
                  </a>
                  <ul class="nav nav-second-level">';
                  $sql="SELECT a.modul_codig, a.modul_padre, a.modul_descr, a.modul_ruta,   
                          a.modul_orden, a.modul_bpadr, b.permi_bborr, b.permi_bperm, a.modul_icono 
                        FROM seguridad_modulos a
                        JOIN p_permisos b ON (a.modul_codig = b.modul_codig)
                        WHERE a.modul_padre='".$value['modul_codig']."'
                          AND b.urole_codig='".$permiso."'
                        GROUP BY a.modul_codig, a.modul_padre, a.modul_descr, a.modul_ruta, 
                          a.modul_orden, a.modul_bpadr, b.permi_bborr, b.permi_bperm, a.modul_icono 
                        ORDER by modul_orden asc";
                  $submenu = $conexion->createCommand($sql)->queryAll();
                    foreach ($submenu as $key => $valor) {
                      if($valor['modul_icono']!=null){
                        $icono=$valor['modul_icono'];
                      }
                      if($valor['permi_bborr']!=true and $valor['permi_bperm']==true ){  
                        $html.= '<li>
                          <a href="'.Yii::app()->request->getBaseUrl(true).$valor['modul_ruta'].'">
                            <i class="'.$icono.'"> </i>
                            '. $valor['modul_descr'].'
                          </a>
                        </li>';
                      }
                    }
                  $html.= '</ul>
                    </li>';
                }else{
                  $html.= '<li>
                    <a href="'.Yii::app()->request->getBaseUrl(true).$value['modul_ruta'].'" class="waves-effect">
                      <i class="'.$icono.'"> </i> 
                      <span class="hide-menu">'. $value['modul_descr'].'</span>
                    </a>
                  </li>';
                }
              } 
            }
            //echo $html;
          ?>
          <!--li class="nav-small-cap">--- Permisos Nuevos</li-->
          <?php
          unset($html);
          $conexion = Yii::app()->db;
          $sql="SELECT urole_codig 
                FROM seguridad_usuarios 
                where usuar_codig='".Yii::app()->user->id['usuario']['codigo']."'";
          $datos = $conexion->createCommand($sql)->queryRow();
          $permiso=$datos['urole_codig'];
          
          $sql="SELECT a.modul_codig, a.modul_padre, a.modul_descr, a.modul_ruta, a.modul_orden, 
                  a.modul_bpadr, b.spacc_codig, a.modul_icono 
                FROM seguridad_modulos a
                JOIN seguridad_permisos_roles b ON (a.modul_codig = b.modul_codig)
                WHERE a.modul_padre='0' 
                  AND b.srole_codig='".$permiso."'
                GROUP BY a.modul_codig, a.modul_padre, a.modul_descr, a.modul_ruta, a.modul_orden, a.modul_bpadr, b.spacc_codig, a.modul_icono 
                UNION
                SELECT a.modul_codig, a.modul_padre, a.modul_descr, a.modul_ruta, a.modul_orden, 
                  a.modul_bpadr, b.spacc_codig, a.modul_icono 
                FROM seguridad_modulos a
                JOIN seguridad_permisos_usuarios b ON (a.modul_codig = b.modul_codig)
                WHERE a.modul_padre='0' 
                  AND b.usuar_codig='".Yii::app()->user->id['usuario']['codigo']."'
                GROUP BY a.modul_codig, a.modul_padre, a.modul_descr, a.modul_ruta, a.modul_orden, a.modul_bpadr, b.spacc_codig, a.modul_icono 
                ORDER by modul_orden asc";
          
          $menu = $conexion->createCommand($sql)->queryAll();
          
          $defecto=$icono='fa fa-gear';
          foreach ($menu as $key => $value) {
            if($value['spacc_codig']=='1' ){
              $icono='fa fa-gear';
              if($value['modul_icono']!=null){
                $icono=$value['modul_icono'];
              }
              
              if($value['modul_bpadr']){
                $html.= '<li>
                  <a href="#" class="waves-effect">
                    <i class="'.$icono.'"> </i> 
                    <span class="hide-menu">
                      '.$value['modul_descr'].'
                      <span class="fa arrow"></span>
                    </span>
                  </a>
                  <ul class="nav nav-second-level">';
                  $sql="SELECT a.modul_codig, a.modul_padre, a.modul_descr, a.modul_ruta, a.modul_orden, a.modul_bpadr, b.spacc_codig, a.modul_icono 
                        FROM seguridad_modulos a
                        JOIN seguridad_permisos_roles b ON (a.modul_codig = b.modul_codig)
                        WHERE a.modul_padre='".$value['modul_codig']."'
                          AND b.srole_codig='".$permiso."'
                        GROUP BY a.modul_codig, a.modul_padre, a.modul_descr, a.modul_ruta, a.modul_orden, a.modul_bpadr, b.spacc_codig, a.modul_icono 
                        UNION
                        SELECT a.modul_codig, a.modul_padre, a.modul_descr, a.modul_ruta, a.modul_orden, a.modul_bpadr, b.spacc_codig, a.modul_icono 
                        FROM seguridad_modulos a
                        JOIN seguridad_permisos_usuarios b ON (a.modul_codig = b.modul_codig)
                        WHERE a.modul_padre='".$value['modul_codig']."'
                          AND b.usuar_codig='".Yii::app()->user->id['usuario']['codigo']."'
                        GROUP BY a.modul_codig, a.modul_padre, a.modul_descr, a.modul_ruta, a.modul_orden, a.modul_bpadr, b.spacc_codig, a.modul_icono 
                        ORDER by modul_orden asc";
                  $submenu = $conexion->createCommand($sql)->queryAll();
                    foreach ($submenu as $key => $valor) {
                      if($valor['modul_icono']!=null){
                        $icono=$valor['modul_icono'];
                      }
                      if($valor['spacc_codig']=='1' ){  
                        $html.= '<li>
                          <a href="'.Yii::app()->request->getBaseUrl(true).$valor['modul_ruta'].'">
                            <i class="'.$icono.'"> </i>
                            '. $valor['modul_descr'].'
                          </a>
                        </li>';
                      }
                    }
                  $html.= '</ul>
                    </li>';
                }else{
                  $html.= '<li>
                    <a href="'.Yii::app()->request->getBaseUrl(true).$value['modul_ruta'].'" class="waves-effect">
                      <i class="'.$icono.'"> </i> 
                      <span class="hide-menu">'. $value['modul_descr'].'</span>
                    </a>
                  </li>';
                }
              } 
            }
            echo $html;
          ?>
          <li>
            <a href="<?php echo Yii::app()->request->getBaseUrl(true) ?>/files/documentos/Manual_autogestion.pdf" target="_blank" class="waves-effect">
              <i class="fa fa-question-circle"> </i> 
              <span class="hide-menu">Ayuda</span>
            </a>
          </li>
          <li>
            <a href="<?php echo Yii::app()->request->getBaseUrl(true) ?>/site/logout"class="waves-effect">
              <i class="fa fa-power-off"> </i> 
              <span class="hide-menu">Salir</span>
            </a>
          </li>
      </ul>
    </div>
  </div>

<?php }else{

  ?>
  <style type="text/css">
    #page-wrapper{
      margin-left: 0px !important;
      margin-top: 0px !important;
    }
    .content-wrapper .footer{
      left: 0px !important;
    }
  </style>
  <?php
} ?>

  <!-- Left navbar-header end -->
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">

      <?php echo $content ?>
      <div class="panel panel-default hide">
    <div class="panel-body" >
        <div class="row"> 
            <div class="col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6 col-sm-12">
              <div class="row"> 
                <div class="col-sm-3">
                    <button type="button" class="btn btn-info btn-block btn-rounded btn-xs">
                        <i class="fa fa-check-circle"></i> 
                         Iniciado
                    </button>

                </div>
                <div class="col-sm-3">
                    <button type="button" class="btn btn-success btn-block btn-rounded btn-xs">
                        <i class="fa fa-check-circle"></i> 
                        A Tiempo
                    </button>
                </div>
                <div class="col-sm-3">
                    <button type="button" class="btn btn-warning btn-block btn-rounded btn-xs">
                        <i class="fa fa-exclamation-circle"></i> 
                        Demorado
                    </button>
                </div>
                <div class="col-sm-3">
                    <button type="button" class="btn btn-danger btn-block btn-rounded btn-xs">
                        <i class="fa fa-times-circle"></i> 
                        Critico
                    </button>
                </div>

            </div>
            </div>
        </div>
        
    </div>
</div>
    </div>
    <!-- /.container-fluid -->

    <footer class="footer text-center"> <?php echo date('Y') ?> &copy; Hecho por Innova Technology </footer>
  </div>
  <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->





<!-- Bootstrap Core JavaScript -->
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/waves.js"></script>
<!--Counter js -->
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/counterup/jquery.counterup.min.js"></script>


<!--Morris JavaScript -->
<!--script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/raphael/raphael-min.js"></script>
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/morrisjs/morris.js"></script>
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script-->


<!-- Chart JS -->
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/Chart.js/Chart.min.js"></script>


<!-- Custom Theme JavaScript -->
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/custom.js"></script>
<!--script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/dashboard1.js"></script-->
<!-- Sparkline chart JavaScript -->
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>

<!--Style Switcher -->
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>


<!--Form Validation -->
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/form-validation/formValidation.min.js"></script>
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/form-validation/bootstrap.min.js"></script>

<!--DataTables -->
<script type="text/javascript" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/datatables/dataTables.bootstrap.js"></script>

<!--Bootbox -->
<script type="text/javascript" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/bootbox/bootbox.min.js"></script>
<!-- Sweet-Alert  -->
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>

<!-- Clock Plugin JavaScript -->
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>
<!-- Color Picker Plugin JavaScript -->
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asColor.js"></script>
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asGradient.js"></script>
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js"></script>

<!-- Date range Plugin JavaScript -->
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/custom-select/custom-select.min.js"></script>
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.js"></script>
<!-- Form Wizard JavaScript -->
    <script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/jquery-wizard-master/dist/jquery-wizard.min.js"></script>
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/cbpFWTabs.js"></script>
<script type="text/javascript">
      (function() {

                [].slice.call( document.querySelectorAll( '.sttabs' ) ).forEach( function( el ) {
                    new CBPFWTabs( el );
                });

            })();
</script>
<script>
      $(".select2").select2();
</script>
<script type="text/javascript">
    
    $(document).keypress(function(event){
    
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        alert('Acción no permitida');    
    }
    
});

</script>
<!--script type="text/javascript">
  var due_date = new Date('2019-05-30');
  var days_deadline = 60;
  /* stop changing here */
  
  var current_date = new Date();
  var utc1 = Date.UTC(due_date.getFullYear(), due_date.getMonth(), due_date.getDate());
  var utc2 = Date.UTC(current_date.getFullYear(), current_date.getMonth(), current_date.getDate());
  var days = Math.floor((utc2 - utc1) / (1000 * 60 * 60 * 24));
  
  if(days > 0) {
    var days_late = days_deadline-days;
    var opacity = (days_late*100/days_deadline)/100;
      opacity = (opacity < 0) ? 0 : opacity;
      opacity = (opacity > 1) ? 1 : opacity;
    if(opacity >= 0 && opacity <= 1) {
      document.getElementsByTagName("body")[0].style.opacity = opacity;
    }
    
  }
</script-->
</body>
</html>
