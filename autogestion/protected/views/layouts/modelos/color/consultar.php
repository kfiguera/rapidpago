<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Color</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Modelos</a></li>
            <li><a href="#">Color</a></li>
            <li class="active">Consultar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Consultar Color</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row hide">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Código</label>
                            
                                <?php 
                                echo CHtml::textField('codig', $color['mcolo_codig'], array('class' => 'form-control', 'placeholder' => "Código", 'disabled'=>'true')); ?>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Imagen</label>
                            <br>
                            <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class='img-responsive img-thumbnail'>
                        </div>
                    </div>
                    <div class="col-sm-8">    
                        <div class="row">
                            <div class="col-sm-12">        
                                <div class="form-group">
                                    <label>Tipo *</label>
                                    <?php 
                                        $conexion = Yii::app()->db;
                                        $sql="SELECT tmode_codig, tmode_descr
                                              FROM pedido_modelo_tipo 
                                              ORDER BY 1";
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'tmode_codig','tmode_descr');
                                        echo CHtml::dropDownList('tmode', $color['tmode_codig'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...', 'disabled' => 'true')); ?>
                                </div>
                            </div>
                            <div class="col-sm-12">        
                                <div class="form-group">
                                    <label>Código *</label>
                                    <?php 
                                        echo CHtml::textField('numer', $color['mcolo_numer'], array('class' => 'form-control', 'placeholder' => "Código",'prompt'=>'Seleccione...', 'disabled' => 'true')); ?>
                                </div>
                            </div>
                            <div class="col-sm-12">        
                                <div class="form-group">
                                    <label>Descripción *</label>
                                    <?php 
                                        echo CHtml::textField('descr', $color['mcolo_descr'], array('class' => 'form-control', 'placeholder' => "Código",'prompt'=>'Seleccione...', 'disabled' => 'true')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 
                
                
                    <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-12">
                            <a href="listado" type="reset" class="btn-block btn btn-info">Volver </a>
                        </div>
                    </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
