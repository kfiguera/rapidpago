<table id='auditoria' class="table table-bordered table-hover dataTable">
    <thead>
        <tr>
            <th width="2%">
                #
            </th>
            <th>
                Tipo
            </th>
            <th>
                Código
            </th>
            <th>
                Descripción
            </th>
            <th width="5%">
                Consultar
            </th>
            <th width="5%">
                Modificar
            </th>
            <th width="5%">
                Eliminar
            </th>
        </tr>
    </thead>

    <tbody>
        <?php
        $sql = "SELECT * 
                FROM pedido_modelo_color a
                JOIN pedido_modelo_tipo b ON (a.tmode_codig = b.tmode_codig)
                ".$condicion."
                ORDER BY 2,3,4,5,6";
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $p_persona = $command->query();
        $i=0;
        while (($row = $p_persona->read()) !== false) {
            $i++;
        ?>
        <tr>
            <td class="tabla"><?php echo $i ?></td>
            <td class="tabla"><?php echo $row['tmode_descr'] ?></td>
            <td class="tabla"><?php echo $row['mcolo_numer'] ?></td>
            <td class="tabla"><?php echo $row['mcolo_descr'] ?></td>
            <td class="tabla"><a href="consultar?c=<?php echo $row['mcolo_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-search"></i></a></td>
            <td class="tabla"><a href="modificar?c=<?php echo $row['mcolo_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-pencil"></i></a></td>
            <td class="tabla"><a href="eliminar?c=<?php echo $row['mcolo_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-close"></i></a></td>

        </tr>
        <?php
            }   
        ?>
    </tbody>
</table>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable(); 
    });
</script>
