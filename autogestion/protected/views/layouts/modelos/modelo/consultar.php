<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Modelo</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Modelos</a></li>
            <li><a href="#">Modelo</a></li>
            <li class="active">Consultar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Consultar Modelo</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row hide">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Código</label>
                            
                                <?php 
                                echo CHtml::textField('codig', $modelo['model_codig'], array('class' => 'form-control', 'placeholder' => "Código", 'disabled'=>'true')); ?>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Descripción</label>
                                <?php 
                                    echo CHtml::textField('descr', $modelo['model_descr'], array('class' => 'form-control', 'placeholder' => "Descripción", 'disabled'=>'true')); 
                                ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>¿Posee Gorro?</label>
                                <?php 
                                $data=array('1'=>'SI', '2'=>'NO');
                                    echo CHtml::dropDownList('gorro', $modelo['model_gorro'], $data ,array('class' => 'form-control', 'placeholder' => "Descripción", 'prompt'=>'Seleccione', 'disabled'=>'true')); 
                                ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>¿Posee Puño y Pretina?</label>
                                <?php 
                                $data=array('1'=>'SI', '2'=>'NO');
                                    echo CHtml::dropDownList('preti', $modelo['model_preti'], $data ,array('class' => 'form-control', 'placeholder' => "Descripción", 'prompt'=>'Seleccione', 'disabled'=>'true')); 
                                ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Mensaje</label>
                                <?php 
                                    echo CHtml::textArea('mensa', $modelo['model_mensa'], array('class' => 'form-control', 'placeholder' => "Mensaje", 'disabled'=>'true')); 
                                ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observaciones</label>
                                <?php 
                                    echo CHtml::textArea('obser', $modelo['model_obser'], array('class' => 'form-control', 'placeholder' => "Observaciones", 'disabled'=>'true')); 
                                ?>
                        </div>
                    </div>
                </div>
                
                    <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-12">
                            <a href="listado" type="reset" class="btn-block btn btn-info">Volver </a>
                        </div>
                    </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
