<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Emoji</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Parametros</a></li>
            <li><a href="#">Emoji</a></li>
            <li class="active">Consultar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Consultar Emoji</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row hide">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Código</label>
                            
                                <?php 
                                echo CHtml::textField('codig', $emoji['emoji_codig'], array('class' => 'form-control', 'placeholder' => "Código", 'disabled'=>'true')); ?>

                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Imagen</label>
                            <br>
                            <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$emoji['emoji_ruta'] ?>" class='img-responsive img-thumbnail'>
                        </div>
                    </div>
                    <div class="col-sm-8">        
                        <div class="form-group">
                            <label>Descripción</label>
                                <?php 
                                    echo CHtml::textField('descr', $emoji['emoji_descr'], array('class' => 'form-control', 'placeholder' => "Descripción", 'disabled'=>'true')); 
                                ?>
                        </div>
                    </div>
                </div>
                
                
                    <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-12">
                            <a href="listado" type="reset" class="btn-block btn btn-info">Volver </a>
                        </div>
                    </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
