<?php
$this->pageTitle = Yii::app()->name . ' - Login';
$this->breadcrumbs = array(
    'Login',
);
?>
<style type="text/css">
  .login-box{
    margin: 0 auto;

  }
</style>
  <div class="white-box m-l-30 m-r-30">
    <img src='https://autogestion.rapidpago.com/assets/img/logo.png' class='img-responsive center-block'>

  
      
        <div id="login">
      <?php
        $form = $this->beginWidget('CActiveForm', array('id' => 'loginform', 'htmlOptions'=>array(
                          'class'=>'form-horizontal form-material',
                        ),'enableClientValidation' => true, 'clientOptions' => array('validateOnSubmit' => true)));


        
        
        if(strlen($form->error($model,'mensaje'))!='80'){

          echo '<div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              '.$form->error($model,'mensaje').' </div>';
        }
        //Mostrar mensajes del controlador
        foreach (Yii::app()->user->getFlashes() as $key => $message) {
          echo '<div class="alert alert-' . $key . ' alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              ' . $message  .' </div>';          
        }

        
      ?>
        <h3 class="box-title m-b-20">Bienvenido </h3>
        <div class="form-group ">
          <div class="row">
          <div class="col-xs-6">
            <a href="../registro/preregistro/registrar" class="btn btn-lg btn-block btn-info">
              <i class="fa fa-plus"></i><br>
              Nuevo Cliente
            </a>
          </div>
          <div class="col-xs-6">
            <a href="login" class="btn btn-lg btn-block btn-info">
              <i class="fa fa-user"></i> <br>
              Cliente Existente
            </a>
          </div>
          </div>
      <?php $this->endWidget(); ?>
      </div>
    </div>
</div>