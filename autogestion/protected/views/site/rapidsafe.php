<?php
    $this->pageTitle = 'Registro | ' . Yii::app()->name ;
    $this->breadcrumbs = array(
        'Registro',
    );
?>
    <style type="text/css">
  .login-box{
    margin: 7vh auto;
  } 
</style>

<div class="login-box" style="width: 80vw; top:5vw;">
    
    <div class="white-box">

        <div class="m-b-20 ">
            <img src='https://autogestion.rapidpago.com/assets/img/logo.png' class='img-responsive center-block' style="max-width: 200px">
        </div>
        <div class="row">
                <div class="col-sm-12 m-b-20">
                    <h3 class="box-title ">Condiciones RapidSafe</h3>
                </div>
            </div>
        <div class="col-sm-12" id="slimtest1">
            <ol>
                <li>
                    <p>El cliente queda obligado a notificar a RapidPago la ocurrencia del incidente, en el plazo máximo de cinco (5) días continuos de haberlo conocido y comunicar las circunstancias y consecuencias del mismo; el efecto jurídico del incumplimiento de dicha declaración en el plazo fijado exonera de responsabilidad a RapidPago, a menos que se compruebe que la omisión se debió a un hecho ajeno a su voluntad.</p>
                    <p>Asimismo, dentro de los diez (10) días continuos siguientes a la fecha de haberlo conocido, el cliente debe suministrarle a RapidPago:</p>
                    <ul>
                        <li>
                            <p>Un informe escrito con todas las circunstancias relativas al incidente del Punto de Venta afectado.</p>
                        </li>
                        <li>
                            <p>Original de la factura de compra.</p>
                        </li>
                    </ul>
                </li>
                <li>
                    <p>Los defectos de fabricación no están cubiertos por RapidSafe ya que los mismos están cubiertos, por la garantía oficial que ofrece nuestra empresa RapidPago, la cual tiene una vigencia de un año contado desde la fecha de emisión de la factura, y previa verificación de nuestros técnicos de que el fallo del dispositivo es de fabricación, y no por el uso del propietario.</p>
                </li>
                <li>
                    <p>RapidPago se reserva el derecho a reemplazar, reponer o reparar el Punto de Venta que se encuentre fuera de garantía.</p>
                </li>
            </ol>
        </div>

        <form  id="login-form" method="post"  autocomplete="off">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Rif del Comercio</label>
                        <input class="form-control" id="rifco" name="rifco" type="text" required="" placeholder="Rif del Comercio">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group ">
                        <label>Correo Electrónico</label>
                        <input class="form-control" id="corre" name="corre" type="text" required="" placeholder="Correo Electrónico"  oncopy="return false" onpaste="return false" autocomplete="off">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group ">
                        <label>Confirmar Correo Electrónico</label>
                        <input class="form-control" id="ccorr" name="ccorr" type="text" required="" placeholder="Confirmar Correo Electrónico"  oncopy="return false" onpaste="return false" autocomplete="off">
                    </div>
                </div>
                
            </div>
            <div class="row">

                <div class="col-sm-12">
                <div class="form-group">
                    <div class="checkbox checkbox-info text-center">
                      <input id="checkbox1" name="checkbox1" type="checkbox">
                      <label for="checkbox1"> He leído y acepto las Condiciones del Servicio </label>
                    </div>
                  </div> 
                </div> 
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group  m-t-0">
                        <div class="form-group">
                            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" id="guardar"  type="button" disabled="true">Aceptar</button>
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {

        $('#checkbox1').change(function() {
            if(this.checked) {
                $('#guardar').removeAttr("disabled");
            }else{
                $('#guardar').prop("disabled", true);
            }
           
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                rifco:{
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "RIF del Comercio" es obligatorio',
                        },
                        regexp: {
                            regexp: /^[V|E|J|G|C|v|e|j|g|c]\d{8}\d+$/,
                            message: 'Estimado(a) Usuario(a) el campo "RIF" debe poseer el siguiente formato: J000000000'
                        }
                    }
                },
                nacio: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Nacionalidad" es obligatorio',
                        }
                    }
                },

                checkbox1: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) debe aceptar los terminos y condiciones',
                        }
                    }
                },
                ndocu: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Cédula" es obligatorio',
                        },
                        numeric: {
                            message: 'Estimado(a) Usuario(a) el campo "Cédula" es numérico',
                        },stringLength: {
                            min: 0,
                            max: 10,
                            message: 'Estimado(a) Usuario(a) el campo "Cédula" permite hasta 10 caracteres'
                        }
                    }
                },

                pnomb: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Primer Nombre" es obligatorio',
                        },
                        regexp: {
                            regexp: /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/i,
                            message: 'Estimado(a) Usuario(a) el campo "Primer Nombre" solo debe poseer letras'
                        },stringLength: {
                            min: 0,
                            max: 25,
                            message: 'Estimado(a) Usuario(a) el campo "Primer Nombre" permite hasta 25 caracteres'
                        }
                    }
                },
                snomb: {
                    validators: {
                        regexp: {
                            regexp: /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/i,
                            message: 'Estimado(a) Usuario(a) el campo "Segundo Nombre" solo debe poseer letras'
                        },stringLength: {
                            min: 0,
                            max: 25,
                            message: 'Estimado(a) Usuario(a) el campo "Segundo Nombre" permite hasta 25 caracteres'
                        }
                    }
                },
                papel: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Primer Apellido" es obligatorio',
                        },
                        regexp: {
                            regexp: /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/i,
                            message: 'Estimado(a) Usuario(a) el campo "Primer Apellido" solo debe poseer letras'
                        },stringLength: {
                            min: 0,
                            max: 25,
                            message: 'Estimado(a) Usuario(a) el campo "Primer Apellido" permite hasta 25 caracteres'
                        }
                    }
                },
                
                sapel: {
                    validators: {
                        regexp: {
                            regexp: /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/i,
                            message: 'Estimado(a) Usuario(a) el campo "Segundo Apellido" solo debe poseer letras'
                        },stringLength: {
                            min: 0,
                            max: 25,
                            message: 'Estimado(a) Usuario(a) el campo "Segundo Apellido" permite hasta 25 caracteres'
                        }
                    }
                },
                tloca: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Teléfono Local" es obligatorio',
                        },
                        numeric: {
                            message: 'Estimado(a) Usuario(a) el campo "Teléfono Local" debe ser un Número',
                        }
                    }
                },
                corre: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Correo Electrónico" es obligatorio',
                        },emailAddress: {
                            message: 'Estimado(a) Usuario(a) debe ingresar un correo electronico valido: ejemplo@rapidpago.com'
                        }
                    }
                },
                ccorr: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Confirmar Correo Electrónico" es obligatorio',
                        },emailAddress: {
                            message: 'Estimado(a) Usuario(a) debe ingresar un correo electronico valido: ejemplo@rapidpago.com'
                        },
                        identical: {
                            field: 'corre',
                            message: 'Estimado(a) Usuario(a) el campo "Correo Electrónico" y su confirmacion no son iguales'
                        }
                    }
                },
                tcelu: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Teléfono Celular" es obligatorio',
                        },
                        regexp: {
                            regexp: /^[0]?[2|4]\d{2}[\-]{1}\d{3}([\.]{1}\d{2}){2}$/i,
                            message: 'Estimado(a) Usuario(a) el campo "Teléfono Celular" debe ser un número valido'
                        },
                    }
                        
                        
                },
                obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Razón Social" es obligatorio',
                        },
                        regexp: {
                            regexp: /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s0-9\,\.]+$/i,
                            message: 'Estimado(a) Usuario(a) el campo "Razón Social" solo debe poseer letras'
                        },stringLength: {
                            min: 0,
                            max: 70,
                            message: 'Estimado(a) Usuario(a) el campo "Razón Social" permite hasta 70 caracteres'
                        }
                    }
                }
            }
        });
    });
</script>
<script type="text/javascript">
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
        $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'rapidsafe',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                    message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                              '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                });
                },
                success: function (response) {
                $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "¡Registro Exitoso!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('login', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
    $('#tcelu').mask('Z000-000.00.00', {translation:  {'Z': {pattern: /[0]/, optional: true}}});
    $('#rifco').mask('Z000000000',  {
        'translation': {
          'Z': {
            pattern: /[V|E|J|G|C|v|e|j|g|c]/
          }
        }
      });
    });
</script>
<script type="text/javascript">

    $(document).ready(function () {
        $('#slimtest1').slimScroll({
            height: '250px'
        });
     });
</script>