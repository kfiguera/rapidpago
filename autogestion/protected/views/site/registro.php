<?php
    $this->pageTitle = 'Registro | ' . Yii::app()->name ;
    $this->breadcrumbs = array(
        'Registro',
    );
?>
    <style type="text/css">
  .login-box{
    margin: 7vh auto;
  }
</style>

<div class="login-box" style="width: 90vw; top:5vw;">
    <div class="white-box">

        <div class="m-b-20 ">
            <img src='https://autogestion.rapidpago.com/assets/img/logo.png' class='img-responsive center-block' style="max-width: 200px">
        </div>

        <form  id="login-form" method="post"  autocomplete="off">
            <div class="row">
                <div class="col-sm-12 m-b-20">
                    <h3 class="box-title ">Registro de Cliente </h3>
                    <p class="text-info"><b>Ingrese sus datos personales y los de su empresa para registrarse en nuestro sistema de autogestión</b></p>
                </div>
            </div>
            <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group ">
                        <label>Cédula</label>
                        <div class="row">
                            <div class="col-sm-4">
                                <select class="form-control" name="nacio" id="nacio">
                                    
                                    <?php
                                        $sql="SELECT * FROM p_nacionalidad";
                                        $conexion=Yii::app()->db;
                                        $p_nacionalidad=$conexion->createCommand($sql)->query();
                                        while (($nacio = $p_nacionalidad->read()) !== false) {
                                            echo "<option value='".$nacio['nacio_value']."'>".$nacio['nacio_value']."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-8">
                                <input class="form-control" id="ndocu" name="ndocu" type="text" required="" placeholder="Cédula">
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group ">
                        <label>Nombre</label>
                        <input class="form-control" id="pnomb" name="pnomb" type="text" required="" placeholder="Nombre">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group ">
                        <label>Apellido</label>
                        <input class="form-control" id="papel" name="papel" type="text" required="" placeholder="Apellido">
                    </div>
                </div>
                
            </div>
            <div class="row">

                <div class="col-sm-4 hide">
                    <div class="form-group ">
                        <label>Segundo Nombre</label>
                        <input class="form-control" id="snomb" name="snomb" type="text" placeholder="Segundo Nombre">
                    </div>
                </div>
                <div class="col-sm-4 hide">
                    <div class="form-group ">
                        <label>Segundo Apellido</label>
                        <input class="form-control" id="sapel" name="sapel" type="text" placeholder="Segundo Apellido">
                    </div>
                </div>
            </div>
            <div class="row">               
                <div class="col-sm-4 hide">
                    <div class="form-group ">
                        <label>Teléfono Local</label>
                        <input class="form-control" id="tloca" name="tloca" type="text" required="" placeholder="Teléfono Local">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group ">
                        <label>Teléfono Móvil</label>
                        <div class="row">
                            <div class="col-sm-4">
                                <?php
                                    $sql="SELECT * FROM p_codigo_area WHERE carea_tipos = '2'";
                                    $carea=$conexion->createCommand($sql)->queryAll();
                                    $data = CHtml::listData($carea,'carea_value','carea_value');
                                    echo CHtml::dropDownList('tcelu1','',$data,array('class'=>'form-control'));
                                ?>
                        </div>
                        <div class="col-sm-8">
                            <input class="form-control" id="tcelu2" name="tcelu2" type="text" required="" placeholder="Teléfono de Contacto">
                        </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group ">
                        <label>Correo Electrónico</label>
                        <input class="form-control" id="corre" name="corre" type="text" required="" placeholder="Correo Electrónico"  oncopy="return false" onpaste="return false" autocomplete="off">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group ">
                        <label>Confirmar Correo Electrónico</label>
                        <input class="form-control" id="ccorr" name="ccorr" type="text" required="" placeholder="Confirmar Correo Electrónico"  oncopy="return false" onpaste="return false" autocomplete="off">
                    </div>
                </div>
                
            </div>
            <div class="row hide">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Rif del Comercio</label>
                        <div class="row">
                            <div class="col-sm-4">
                                <select class="form-control" name="rifco1" id="rifco1">
                                    <option value=''>Seleccione</option>
                                    <?php
                                        $sql="SELECT * FROM p_documento_tipo";
                                        $conexion=Yii::app()->db;
                                        $p_nacionalidad=$conexion->createCommand($sql)->query();
                                        while (($nacio = $p_nacionalidad->read()) !== false) {
                                            echo "<option value='".$nacio['tdocu_descr']."'>".$nacio['tdocu_descr']."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-8">
                               <input class="form-control" id="rifco2" name="rifco2" type="text" required="" placeholder="Rif del Comercio">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Razón Social</label>
                        <input class="form-control" id="obser" name="obser" type="text" required="" placeholder="Razón Social">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group text-center m-t-0">
                        <p class="text-info text-center"><b>Verifique los datos que ha ingresado, si están correctos, haga clic en registrarse</b></p>
                        <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" id="guardar" type="button">Registrarse</button>
                    </div>
                </div>
            </div>

            <div class="form-group m-b-0">
                <div class="col-sm-12 text-center">
                    <p>¿Ya Tienes Cuenta? <a href="login" class="text-primary m-l-5"><b>Inicia Sesión</b></a></p>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                rifco1:{
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Letra RIF del Comercio" es obligatorio',
                        }
                    }
                },
                rifco2:{
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "RIF del Comercio" es obligatorio',
                        },
                        /*regexp: {
                            regexp: /^[V|E|J|G|C|v|e|j|g|c]\d{8}\d+$/,
                            message: 'Estimado(a) Usuario(a) el campo "RIF" debe poseer el siguiente formato: J000000000'
                        }*/
                        regexp: {
                            regexp: /^\d{8}\d{1}$/,
                            message: 'Estimado(a) Usuario(a) el campo "RIF" debe poseer el siguiente formato: 000000000'
                        }
                    }
                },
                nacio: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Nacionalidad" es obligatorio',
                        }
                    }
                },
                ndocu: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Cédula" es obligatorio',
                        },
                        numeric: {
                            message: 'Estimado(a) Usuario(a) el campo "Cédula" es numérico',
                        },stringLength: {
                            min: 0,
                            max: 10,
                            message: 'Estimado(a) Usuario(a) el campo "Cédula" permite hasta 10 caracteres'
                        }
                    }
                },

                pnomb: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Primer Nombre" es obligatorio',
                        },
                        regexp: {
                            regexp: /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/i,
                            message: 'Estimado(a) Usuario(a) el campo "Primer Nombre" solo debe poseer letras'
                        },stringLength: {
                            min: 0,
                            max: 25,
                            message: 'Estimado(a) Usuario(a) el campo "Primer Nombre" permite hasta 25 caracteres'
                        }
                    }
                },
                snomb: {
                    validators: {
                        regexp: {
                            regexp: /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/i,
                            message: 'Estimado(a) Usuario(a) el campo "Segundo Nombre" solo debe poseer letras'
                        },stringLength: {
                            min: 0,
                            max: 25,
                            message: 'Estimado(a) Usuario(a) el campo "Segundo Nombre" permite hasta 25 caracteres'
                        }
                    }
                },
                papel: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Primer Apellido" es obligatorio',
                        },
                        regexp: {
                            regexp: /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/i,
                            message: 'Estimado(a) Usuario(a) el campo "Primer Apellido" solo debe poseer letras'
                        },stringLength: {
                            min: 0,
                            max: 25,
                            message: 'Estimado(a) Usuario(a) el campo "Primer Apellido" permite hasta 25 caracteres'
                        }
                    }
                },
                
                sapel: {
                    validators: {
                        regexp: {
                            regexp: /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$/i,
                            message: 'Estimado(a) Usuario(a) el campo "Segundo Apellido" solo debe poseer letras'
                        },stringLength: {
                            min: 0,
                            max: 25,
                            message: 'Estimado(a) Usuario(a) el campo "Segundo Apellido" permite hasta 25 caracteres'
                        }
                    }
                },
                tloca: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Teléfono Local" es obligatorio',
                        },
                        numeric: {
                            message: 'Estimado(a) Usuario(a) el campo "Teléfono Local" debe ser un Número',
                        }
                    }
                },
                corre: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Correo Electrónico" es obligatorio',
                        },emailAddress: {
                            message: 'Estimado(a) Usuario(a) debe ingresar un correo electronico valido: ejemplo@rapidpago.com'
                        }
                    }
                },
                ccorr: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Confirmar Correo Electrónico" es obligatorio',
                        },emailAddress: {
                            message: 'Estimado(a) Usuario(a) debe ingresar un correo electronico valido: ejemplo@rapidpago.com'
                        },
                        identical: {
                            field: 'corre',
                            message: 'Estimado(a) Usuario(a) el campo "Correo Electrónico" y su confirmacion no son iguales'
                        }
                    }
                },
                tcelu1: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Teléfono de Contacto" es obligatorio',
                        },
                        regexp: {
                            regexp: /^[0]?[2|4]\d{2}$/i,
                            message: 'Estimado(a) Usuario(a) el campo "Código de Area" debe ser un número valido, Ejemplo:  0212 / 0412'
                        },
                    }
                        
                        
                },
                tcelu2: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Teléfono de Contacto" es obligatorio',
                        },
                        regexp: {
                            regexp: /^\d{3}([\.]{1}\d{2}){2}$/i,
                            message: 'Estimado(a) Usuario(a) el campo "Teléfono de Contacto" debe ser un número valido, Ejemplo: 000.00.00'
                        },
                    }
                        
                        
                },
                obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Razón Social" es obligatorio',
                        },
                        regexp: {
                            regexp: /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s0-9\,\.]+$/i,
                            message: 'Estimado(a) Usuario(a) el campo "Razón Social" solo debe poseer letras'
                        },stringLength: {
                            min: 0,
                            max: 70,
                            message: 'Estimado(a) Usuario(a) el campo "Razón Social" permite hasta 70 caracteres'
                        }
                    }
                }
            }
        });
    });
</script>
<script type="text/javascript">
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
        $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'registro',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                    message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                              '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                });
                },
                success: function (response) {
                $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "¡Registro Exitoso!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('login', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
    $('#tcelu1').mask('0000', {translation:  {'Z': {pattern: /[0]/, optional: true}}});
    $('#tcelu2').mask('000.00.00', {translation:  {'Z': {pattern: /[0]/, optional: true}}});
    $('#rifco').mask('Z000000000',  {
        'translation': {
          'Z': {
            pattern: /[V|E|J|G|C|v|e|j|g|c]/
          }
        }
      });
    });
    $('#rifco2').mask('000000000',  {
        'translation': {
          'Z': {
            pattern: /[V|E|J|G|C|v|e|j|g|c]/
          }
        }
      });
    });
</script>