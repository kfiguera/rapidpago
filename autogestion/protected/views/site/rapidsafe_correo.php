<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>RapidPago | RapidSafe</title>
</head>
<body style="margin:0px; background: #f8f8f8; ">
<div width="100%" style="background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;">
  <div style="max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px">
      <tbody>
        <tr>
          <td style="vertical-align: top; padding-bottom:30px;" align="center">
            <img src="<?php echo Yii::app()->request->getBaseUrl(true);?>/assets/img/logo.png"  width="350px" style="border:none"></td>
        </tr>
      </tbody>
    </table>
    <div style="padding: 40px; background: #fff;">
      <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tbody>
          <tr>
            <td style="border-bottom:1px solid #f6f6f6;"><h1 style="font-size:14px; font-family:arial; margin:0px; font-weight:bold;">
              <p>Estimado cliente, le confirmamos que su solicitud de afiliación a nuestro servicio RapidSafe ha sido exitosa y a partir de este momento usted disfruta de este servicio.</p>

              <p>Queda entendido que usted ha leído y acepta los términos señalados en la página web o portal en Internet de "RapidPago".</p>

              <p>Gracias por su preferencia, ya que nos impulsa a dar más y lo mejor de RapidPago.</p>
                  
              <p>- Muchas gracias.</p> </p>
              <p style="text-align: center;">Para RapidPago, Tu eres el Punto.</p>
            </td>
          </tr>
          <tr>
            <td  style="border-top:1px solid #f6f6f6; padding-top:20px; color:#777">
              
              <p>En caso de asistencia comercial, escriba a ventas@rapidpago.com.</p></td>
          </tr>
        </tbody>
      </table>
    </div>
    
    <div style="text-align: center; font-size: 12px; color: #b2b2b5; margin-top: 20px">
      <p> <a href="https://Innova Technology.net">Elaborado por Innova Technology</a> <p>
    </div>
  </div>
</div>
</body>
</html>
