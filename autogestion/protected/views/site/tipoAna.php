<!-- Bootstrap -->
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/css/bootstrap.min.css" rel="stylesheet">

<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/css/bootstrapValidator.css" rel="stylesheet">
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/css/font-awesome.min.css" rel="stylesheet" >
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/css/dataTables.bootstrap.min.css" rel="stylesheet" >
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script type="text/javascript" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/jquery.mask.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script type="text/javascript" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/bootstrapValidator.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/bootbox.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/dataTables.bootstrap.min.js"></script>
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/css/bootstrap.LineaTiempo.css" rel="stylesheet">
<div class="form-group">
    <label>Tipo de Analista</label>
    <div style="margin-bottom: 25px" class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            <?php echo CHtml::dropDownList('tipo', '', array('3'=>'Analista','4'=>'Coordinador'), array('class' => 'form-control' , 'prompt' => 'Selecciona...')); ?>
    </div>
</div>
<script type="text/javascript">
var tipo = {
            validators: {
                notEmpty: {
                    message: 'Debe seleccionar un Tipo de analiasta'
                },
            }
        };
$('#login-form')
                
                .bootstrapValidator('addField', 'tipo', tipo);
</script>