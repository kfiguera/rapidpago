<style type="text/css">
    .col-xs-15,
    .col-sm-15,
    .col-md-15,
    .col-lg-15 {
        position: relative;
        min-height: 1px;
        padding-right: 10px;
        padding-left: 10px;
    }
    .col-xs-15 {
        width: 20%;
        float: left;
    }

    @media (min-width: 768px) {
    .col-sm-15 {
            width: 20%;
            float: left;
        }
    }

    @media (min-width: 992px) {
        .col-md-15 {
            width: 20%;
            float: left;
        }
    }

    @media (min-width: 1200px) {
        .col-lg-15 {
            width: 20%;
            float: left;
        }
    }
</style>
<?php $conexion=Yii::app()->db;?>


<div class="row bg-title">
    <!-- .page title -->


    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Resumen</h4>
    </div>

    <!-- /.page title -->
    <!-- .breadcrumb -->
    <div class="col-lg-8s col-sm-8 col-md-8 col-xs-12">
          <a href="../registro/preregistro/registrar"  class="btn btn-success pull-right m-l-20 btn-rounded waves-effect waves-light"> <i class="fa fa-plus"></i> Nueva Solicitud</a>
          <a href="../../files/documentos/CONTRATO_RAPIDPAGO.docx"  class="btn btn-info pull-right m-l-20 btn-rounded btn-outline waves-effect waves-light"> <i class="fa fa-download"></i> Descargar Contrato</a>
          
          <ol class="breadcrumb">
            <li class="active">Inicio</li>
          </ol>
        </div>
    <!-- /.breadcrumb -->
</div>
<?php
if(Yii::app()->user->id['usuario']['permiso']==13){
    ?>
<div class="white-box m-t-30 hide">
    <div class="row">
       
        <div class="col-md-4 col-md-offset-4">
            <div class="row">
                <div class="col-md-12 m-b-15">
                        <img src='https://autogestion.rapidpago.com/assets/img/logo.png' class='img-responsive center-block'>
                </div>
            </div>
            
        </div>
    </div>
</div>



        <?php

        $post['usuar']=Yii::app()->user->id['usuario']['usuario'];
        $datos=$this->funciones->RegistroApiSigo('ListarSolicitudes',$post);
        if($datos){
        foreach ($datos as $key => $dato) {
            $ubicacion[0]='start ';
            $ubicacion[4]='finish ';
            $sql="SELECT a.*, b.estat_codig, b.estat_descr 
                  FROM solicitud_modulo a 
                  JOIN rd_preregistro_estatus b ON (a.smodu_codig = b.smodu_codig and b.estat_codig ='".$dato['estat_codig']."')";
            $pasos=$conexion->createCommand($sql)->queryRow();
            $paso=$pasos['smodu_codig'];
            for ($i=0; $i < 5 ; $i++) { 
                $icono[$i]=$i+1;
                if($i<($paso-1)){
                    $ubicacion[$i].='upcoming ';
                    $icono[$i]="<i class='fa fa-check'></i>";
                }
                if ($i==($paso-1)) {
                    $ubicacion[$i].='active ';
                }
            }
            $sql="SELECT * 
                  FROM solicitud_modulo a ";
            $modulos=$conexion->createCommand($sql)->queryAll();
            $i=0;


            ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">

                        
                        <div class="col-md-2">
                            <a href="/registro/preregistro/modificar?c=<?php echo $datos[$key]['prere_codig']; ?>&s=1" class="btn btn-info btn-block btn-rounded btn-outline waves-effect waves-light"><i class="fa fa-edit"></i> Modificar </a>
                        </div>
                        <div class="col-md-6">
                            <h3 class="panel-title"><b>Solicitud:</b><?php echo $datos[$key]['solic_numer']; ?></h3>

                        </div>
                        <div class="col-md-4 text-right">
                            <h3 class="panel-title"><b>Estatus Actual:</b> <?php echo $datos[$key]['estat_descr']; ?></h3>
                        </div>
                    </div>
                    
                </div>
                <div class="panel-body" >
                    <div class="row line-steps">
                        <?php
                        $i=0;
                        foreach ($modulos as $key => $modulo) {                        ?>
                        <div class="col-md-15 column-step <?php echo $ubicacion[$i]; ?>">
                            <div class="step-number"><?php echo $icono[$i]; ?> </div>
                            <div class="step-title"><?php echo $modulo['smodu_descr']; ?></div>
                        </div>
                        <?php
                        $i++;
                    }
                    ?>
                      
                </div>
            </div>
            </div>
            <?php
        }
    }else {
        ?>
        <div class="row ">
          <div class="col-xs-12 text-center m-t-30">
            <h2>¡No Hay Solicitudes todavia!</h2>
            <a href="../registro/preregistro/registrar"  class="btn btn-info btn-lg m-l-20 btn-rounded waves-effect waves-light"> Vamos a iniciar una nueva <i class="fa fa-arrow-right"></i></a>
          </div>
        </div>
        <?php
    }
        ?> 
    
       
<?php } ?>
<?php
if(Yii::app()->user->id['usuario']['permiso']!=13){

$sql="SELECT count(solic_codig) cantidad FROM solicitud WHERE estat_codig NOT IN (0)";
$solicitud = $conexion->createCommand($sql)->queryRow();

$sql="SELECT count(prere_codig) cantidad FROM rd_preregistro WHERE estat_codig NOT IN (0,4)";
$preregistro = $conexion->createCommand($sql)->queryRow();

$total=$solicitud['cantidad']+$preregistro['cantidad'];

$sql="SELECT count(prere_codig) cantidad, c.smodu_codig, c.smodu_descr, c.smodu_icono, c.smodu_color
      FROM rd_preregistro a 
      JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig and a.estat_codig not in (0,4))
      JOIN solicitud_modulo c ON (b.smodu_codig = c.smodu_codig)
      UNION
      SELECT count(solic_codig) cantidad,  c.smodu_codig, c.smodu_descr, c.smodu_icono, c.smodu_color
      FROM solicitud a 
      JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig and a.estat_codig not in (0))
      RIGHT JOIN solicitud_modulo c ON (b.smodu_codig = c.smodu_codig)
      WHERE c.smodu_codig <> 1
      GROUP BY 2,3,4,5
      ORDER BY 2";
$cantidades = $conexion->createCommand($sql)->queryAll();
?>
<div class="row">
    <div class="col-md-12 col-lg-12 col-sm-12">
        <div class="white-box">
            <div class="row row-in">
            <?php
            $i=0;
            foreach ($cantidades as $key => $cantidad) {
                $porcentaje=($cantidad['cantidad']*100)/$total;
                if($i<4){
                    $cabecera='row-in-br';
                }else{
                    $cabecera='b-0';
                }
                ?>
                <div class="col-lg-15 col-sm-6 <?php echo $cabecera; ?>">
                    <div class="col-in row">
                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="<?php echo $cantidad['smodu_icono']; ?>"></i>
                            <h5 class="text-muted vb"><?php echo $cantidad['smodu_descr']; ?></h5>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <h3 class="counter text-right m-t-15 text-<?php echo $cantidad['smodu_color']; ?>"><?php echo $cantidad['cantidad'] ?></h3>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="progress">
                                <div class="progress-bar progress-bar-<?php echo $cantidad['smodu_color']; ?>" role="progressbar" aria-valuenow="<?php echo $porcentaje ?> " aria-valuemin="0" aria-valuemax="<?php echo $total; ?>" style="width: <?php echo $porcentaje ?>%"> <span class="sr-only"><?php echo $porcentaje ?>% Complete (success)</span> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                $i++;
            }

            ?>
            </div>
          </div>
        </div>
      </div>
     
<!-- .row -->
<div class="row">
    <div class="col-md-12 hide">
            
        <div class="row">
            <div class="col-md-15">
                <div class="white-box">
                    <h3 class="box-title">REGISTRO </h3>
                    <ul class="list-inline two-part">
                        <li><i class="fa fa-edit text-info"></i></li>
                        <li class="text-right"><span class="counter">23</span></li>
                    </ul>
                  </div>
            </div>
            <div class="col-md-15">
                <div class="white-box">
                    <h3 class="box-title">TRAMTACIÓN</h3>
                    <ul class="list-inline two-part">
                        <li><i class="fa fa-bank text-success"></i></li>
                        <li class="text-right"><span class="counter">23</span></li>
                    </ul>
                  </div>
            </div>
            <div class="col-md-15">
                <div class="white-box">
                    <h3 class="box-title">ALMACEN</h3>
                    <ul class="list-inline two-part">
                        <li><i class="fa fa-archive text-warning"></i></li>
                        <li class="text-right"><span class="counter">23</span></li>
                    </ul>
                  </div>
            </div>
            <div class="col-md-15">
                <div class="white-box">
                    <h3 class="box-title">CONFIGURACION</h3>
                    <ul class="list-inline two-part">
                        <li><i class="fa fa-sliders text-primary"></i></li>
                        <li class="text-right"><span class="counter">23</span></li>
                    </ul>
                  </div>
            </div>
            <div class="col-md-15">
                <div class="white-box">
                    <h3 class="box-title">DESPACHO</h3>
                    <ul class="list-inline two-part">
                        <li><i class="fa fa-truck text-danger"></i></li>
                        <li class="text-right"><span class="counter">23</span></li>
                    </ul>
                  </div>
            </div>
        </div>
        
    </div>
    <div class="col-md-6">
        <div class="white-box">
            <h3 class="box-title">Solicitudes por Estatus</h3>
            <div>
              <canvas id="chart4" height="239" width="479" style="width: 533px; height: 266px;"> </canvas>
            </div>
          </div>
    </div>
    <div class="col-md-6">
        <div class="white-box">
            <h3 class="box-title">Solicitudes por Estatus</h3>
            <div>
                <table class="table table-bordered -table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Modulo</th>
                            <th>Descrición</th>
                            <th>Cantidad</th>
                        </tr>
                    </thead>
                    <tbody>
                    
                    <?php
                      $sql="SELECT count(a.estat_codig) cantidad,b.estat_codig, b.estat_descr, c.smodu_descr
                          FROM rapidpago.rd_preregistro a
                          JOIN rapidpago.rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                          JOIN rapidpago.solicitud_modulo c ON (b.smodu_codig = c.smodu_codig)
                          WHERE a.estat_codig not in (0,4)

                          GROUP BY estat_codig
                          UNION
                          SELECT count(a.estat_codig) cantidad,b.estat_codig, b.estat_descr, c.smodu_descr
                          FROM rapidpago.solicitud a
                          JOIN rapidpago.rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                          JOIN rapidpago.solicitud_modulo c ON (b.smodu_codig = c.smodu_codig)
                          WHERE a.estat_codig <> 0
                          GROUP BY estat_codig
                          ORDER BY estat_codig;";
                    $datos=$conexion->createCommand($sql)->queryAll();
                    $total=0;
                    $i=1;
                    foreach ($datos as $key => $dato) {
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $dato["smodu_descr"]; ?></td>
                            <td><?php echo $dato["estat_descr"]; ?></td>
                            <td><?php echo $dato["cantidad"]; ?></td>
                        </tr>
                        <?php
                        $total+=$dato["cantidad"];
                        $i++;
                    }

                    ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3">Total de Solicitudes</td>
                            <td><?php echo $total; ?></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
          </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        $(".counter").counterUp({
            delay: 100,
            time: 1200
        });

    });
</script>
<script type="text/javascript">
    $( document ).ready(function() {
    
   
    var ctx4 = document.getElementById("chart4").getContext("2d");
    var data4 = [
        <?php
            $sql="SELECT count(a.estat_codig) cantidad, b.estat_codig, b.estat_descr
                  FROM rapidpago.rd_preregistro a
                  JOIN rapidpago.rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                  WHERE a.estat_codig not in (0,4)

                  GROUP BY estat_codig
                  UNION
                  SELECT count(a.estat_codig) cantidad, b.estat_codig, b.estat_descr
                  FROM rapidpago.solicitud a
                  JOIN rapidpago.rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                  WHERE a.estat_codig <> 0
                  GROUP BY a.estat_codig";
            $datos=$conexion->createCommand($sql)->queryAll();
            
            $i=21;
            foreach ($datos as $key => $dato) {
                
                $color = '#'.substr(md5($i), 0, 6);
                $grafico['value']=$dato['cantidad'];
                $grafico['color']=$color;
                $grafico['highlight']=$color;
                $grafico['label']=$dato['estat_descr'];
                if($i>21){
                    echo ',';    
                }
                echo json_encode($grafico);
                $i++;
            }

        ?>
    ];
    
    var myDoughnutChart = new Chart(ctx4).Doughnut(data4,{
        segmentShowStroke : true,
        segmentStrokeColor : "#fff",
        segmentStrokeWidth : 0,
        animationSteps : 100,
        tooltipCornerRadius: 2,
        animationEasing : "easeOutBounce",
        animateRotate : true,
        animateScale : false,
        legendTemplate : "<ul class=\"<\x25=name.toLowerCase()\x25>-legend\"><\x25 for (var i=0; i<segments.length; i++){\x25><li><span style=\"background-color:<\x25=segments[i].fillColor\x25>\"></span><\x25if(segments[i].label){\x25><\x25=segments[i].label\x25><\x25}\x25></li><\x25}\x25></ul>",
        responsive: true
    });
    
    
});
</script>
<?php } ?>