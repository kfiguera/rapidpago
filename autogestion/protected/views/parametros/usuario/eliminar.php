<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Usuario</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Parametros</a></li>
            <li><a href="#">Usuario</a></li>
            <li class="active">Eliminar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Eliminar Usuario</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row hide">
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Correo</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <?php echo CHtml::hiddenField('codig', $usuario['usuar_codig'], array('class' => 'form-control', 'placeholder' => "Correo")); ?>

                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Correo</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <?php echo CHtml::textField('corre', $usuario['usuar_login'], array('class' => 'form-control', 'placeholder' => "Correo","disabled"=>"true")); ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Estatus</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <select class="form-control" name="estat" id="estat" disabled="true">
                                    <option value=''>Seleccione</option>
                                    <?php
                                        $sql="SELECT * FROM p_usuarios_estatus";
                                        $estatus=$conexion->createCommand($sql)->query();
                                        while (($estat = $estatus->read()) !== false) {
                                            if($usuario['uesta_codig']==$estat['uesta_codig']){
                                                echo "<option value='".$estat['uesta_codig']."' selected>".$estat['uesta_descr']."</option>";
                                            }else{
                                                echo "<option value='".$estat['uesta_codig']."'>".$estat['uesta_descr']."</option>";
                                            }
                                        }
                                    ?>
                                </select>
                                

                            </div>
                        </div>
                    </div>
                    
                </div>
                
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Persona</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <select class="form-control" name="perso" id="perso" disabled="true">
                                    <option value=''>Seleccione</option>
                                    <?php
                                        $sql="SELECT * FROM p_persona";
                                        $p_persona=$conexion->createCommand($sql)->query();
                                        while (($perso = $p_persona->read()) !== false) {
                                            if($usuario['perso_codig']==$perso['perso_codig']){
                                                echo "<option value='".$perso['perso_codig']."' selected>".$perso['perso_pnomb']." ".$perso['perso_papel']."</option>";
                                            }else{
                                                echo "<option value='".$perso['perso_codig']."'>".$perso['perso_pnomb']." ".$perso['perso_papel']."</option>";
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Nivel</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
                                <select class="form-control" name="nivel" id="nivel" disabled="true">
                                    <option value="">Seleccione</option>
                                    <?php
                                        $sql="SELECT * FROM seguridad_roles";
                                        $uroles=$conexion->createCommand($sql)->query();
                                        while (($urole = $uroles->read()) !== false) {
                                            if($usuario['urole_codig']==$urole['srole_codig']){
                                                echo "<option value='".$urole['srole_codig']."' selected>".$urole['srole_descr']."</option>";
                                            }else{
                                                echo "<option value='".$urole['srole_codig']."'>".$urole['srole_descr']."</option>";
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                <div class="row">
                    
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observaciones</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
                                
                                <?php echo CHtml::textArea('obser', $usuario['usuar_obser'], array('class' => 'form-control', 'placeholder' => "Observaciones","disabled"=>"true")); ?>
                                
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                    <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-6 ">
                            <button id="guardar" type="button" class="btn-block btn btn-info">Eliminar  </button>
                        </div>
                        <div class="col-sm-6">
                            <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                        </div>
                    </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
<script>
$('#fnaci').mask('00/00/0000');
$('#desde').mask('00/00/0000');
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                corre: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Correo" es obligatorio',
                        },
                        regexp: {
                            regexp: /^[A-Za-z0-9._%+-]+\@[a-zA-Z0-9.-]+\.[A-Za-z]{2,4}$/,
                            message: 'Estimado(a) Usuario(a) el campo "Correo" debe poseer el siguiente formato: ejemplo@gmail.com'
                        }, identical: {
                            field: 'ccorre',
                            message: 'Estimado(a) Usuario(a) el campo "Correo" y su confirmacion no son iguales'
                        }
                    }
                },ccorre: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Confirmar Correo" es obligatorio',
                        },
                        regexp: {
                            regexp: /^[A-Za-z0-9._%+-]+\@[a-zA-Z0-9.-]+\.[A-Za-z]{2,4}$/,
                            message: 'Estimado(a) Usuario(a) el campo "Confirmar Correo"  debe poseer el siguiente formato: ejemplo@gmail.com'
                        }, identical: {
                            field: 'corre',
                            message: 'Estimado(a) Usuario(a) el campo "Correo" y su confirmacion no son iguales'
                        }
                    }
                },contra: {
                    validators: {
                        /*notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Contraseña" es obligatorio',
                        },*/
                        identical: {
                            field: 'ccontra',
                            message: 'Estimado(a) Usuario(a) el campo "Contraseña" y su confirmacion no son iguales'
                        }
                    }
                },ccontra: {
                    validators: {
                        /*notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Confirmar Contraseña" es obligatorio',
                        },*/
                        identical: {
                            field: 'contra',
                            message: 'Estimado(a) Usuario(a) el campo "Contraseña" y su confirmacion no son iguales'
                        }
                    }
                },perso: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Persona" es obligatorio',
                        }
                    }
                },
                nivel: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Nivel" es obligatorio',
                        }
                    }
                },
                /*obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Observaciones" es obligatorio',
                        }
                    }
                },*/



            }
        });
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'eliminar',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>