<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Usuario</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Parametros</a></li>
            <li><a href="#">Usuario</a></li>
            <li class="active">Consultar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Consultar Usuario</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row">
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Correo</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <?php echo CHtml::textField('corre', $usuario['usuar_login'], array('class' => 'form-control', 'placeholder' => "Correo","disabled"=>"true")); ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Estatus</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <select class="form-control" name="estat" id="estat" disabled="true">
                                    <option value=''>Seleccione</option>
                                    <?php
                                        $sql="SELECT * FROM p_usuarios_estatus";
                                        $estatus=$conexion->createCommand($sql)->query();
                                        while (($estat = $estatus->read()) !== false) {
                                            if($usuario['uesta_codig']==$estat['uesta_codig']){
                                                echo "<option value='".$estat['uesta_codig']."' selected>".$estat['uesta_descr']."</option>";
                                            }else{
                                                echo "<option value='".$estat['uesta_codig']."'>".$estat['uesta_descr']."</option>";
                                            }
                                        }
                                    ?>
                                </select>
                                

                            </div>
                        </div>
                    </div>
                    
                </div>
                
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Persona</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <select class="form-control" name="perso" id="perso" disabled="true">
                                    <option value=''>Seleccione</option>
                                    <?php
                                        $sql="SELECT * FROM p_persona";
                                        $p_persona=$conexion->createCommand($sql)->query();
                                        while (($perso = $p_persona->read()) !== false) {
                                            if($usuario['perso_codig']==$perso['perso_codig']){
                                                echo "<option value='".$perso['perso_codig']."' selected>".$perso['perso_pnomb']." ".$perso['perso_papel']."</option>";
                                            }else{
                                                echo "<option value='".$perso['perso_codig']."'>".$perso['perso_pnomb']." ".$perso['perso_papel']."</option>";
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Nivel</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
                                <select class="form-control" name="nivel" id="nivel" disabled="true">
                                    <option value="">Seleccione</option>
                                    <?php
                                        $sql="SELECT * FROM seguridad_roles";
                                        $uroles=$conexion->createCommand($sql)->query();
                                        while (($urole = $uroles->read()) !== false) {
                                            if($usuario['urole_codig']==$urole['srole_codig']){
                                                echo "<option value='".$urole['srole_codig']."' selected>".$urole['srole_descr']."</option>";
                                            }else{
                                                echo "<option value='".$urole['srole_codig']."'>".$urole['srole_descr']."</option>";
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                <div class="row">
                    
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observaciones</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
                                
                                <?php echo CHtml::textArea('obser', $usuario['usuar_obser'], array('class' => 'form-control', 'placeholder' => "Observaciones","disabled"=>"true")); ?>
                                
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                    <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-12">
                            <a href="listado" type="reset" class="btn-block btn btn-info">Volver </a>
                        </div>
                    </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
