<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Organigrama</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Parametros</a></li>
            <li><a href="#">Organigrama</a></li>
            <li class="active">Consultar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Consultar Organigrama</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Descripción</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php echo CHtml::textField('descr', $roles['organ_descr'], array('class' => 'form-control', 'placeholder' => "Descripción", 'disabled'=>'true')); ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Dependencia *</label>
                            <?php 
                                $conexion = Yii::app()->db;
                                $conexion = Yii::app()->db;
                                $sql="SELECT a.organ_codig, a.organ_descr
                                      FROM p_organigrama a
                                      ORDER by 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'organ_codig','organ_descr');
                                echo CHtml::dropDownList('padre', $roles['organ_padre'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...', 'disabled'=>'true')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Nivel *</label>
                            <?php 
                                $conexion = Yii::app()->db;
                                $conexion = Yii::app()->db;
                                $sql="SELECT a.onive_codig, a.onive_descr
                                      FROM p_organigrama_nivel a
                                      ORDER by 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'onive_codig','onive_descr');
                                echo CHtml::dropDownList('nivel', $roles['onive_codig'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...', 'disabled'=>'true')); ?>
                        </div>
                    </div>
                    
                </div>
                
               
                
                    <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-12">
                            <a href="listado" type="reset" class="btn-block btn btn-info">Volver </a>
                        </div>
                    </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
