<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Permisos</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Parametros</a></li>
            <li><a href="#">Permisos</a></li>
            <li class="active">Consultar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Consultar Permisos</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row hide">
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>codigo</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <?php echo CHtml::hiddenField('codig', $roles['permi_codig'], array('class' => 'form-control', 'placeholder' => "Correo")); ?>

                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Rol de Usuario</label>
                                <?php
                                $sql = "SELECT * FROM seguridad_usuarios_roles ";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'urole_codig','urole_descr');
                                echo CHtml::dropDownlist('urole', $roles['urole_codig'],$data, array('class' => 'form-control', 'placeholder' => "Código",'prompt'=>'Seleccione...','disabled'=>'true')); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Modulo</label>
                            
                                <?php
                                $sql="SELECT b.* 
                                      FROM seguridad_modulos b
                                      WHERE b.modul_codig NOT IN (
                                        SELECT a.modul_codig 
                                        FROM p_permisos a 
                                        WHERE a.urole_codig='".$roles['urole_codig']."'
                                      ) 
                                      UNION
                                      SELECT b.* 
                                      FROM seguridad_modulos b
                                      WHERE b.modul_codig ='".$roles['modul_codig']."'";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'modul_codig','modul_descr');
                                echo CHtml::dropDownlist('modul', $roles['modul_codig'],$data, array('class' => 'form-control', 'placeholder' => "Código",'prompt'=>'Seleccione...','disabled'=>'true')); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Estatus</label>
                                <?php
                                $sql = "SELECT * FROM p_permisos_estatus";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'eperm_codig','eperm_descr');
                                echo CHtml::dropDownlist('estat', $roles['permi_bperm'],$data, array('class' => 'form-control', 'placeholder' => "Código",'prompt'=>'Seleccione...','disabled'=>'true')); ?>

                        </div>
                    </div>
                    
                </div>
                
               
                
                    <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-12">
                            <a href="listado" type="reset" class="btn-block btn btn-info">Volver </a>
                        </div>
                    </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
