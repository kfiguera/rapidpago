<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Persona</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Parametros</a></li>
            <li><a href="#">Persona</a></li>
            <li class="active">Consultar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Consultar Persona</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row hide">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Código</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <?php echo CHtml::hiddenField('codig', $p_persona['perso_codig'], array('class' => 'form-control', 'placeholder' => "Código", 'readonly'=>'true')); ?>

                            </div>
                        </div>
                    </div>    
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Nacionalidad</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-flag"></i></span>
                                <select class="form-control" name="nacio" id="nacio" readonly>
                                    <option value=''>Seleccione</option>
                                    <?php
                                        $sql="SELECT * FROM p_nacionalidad";
                                        $p_nacionalidad=$conexion->createCommand($sql)->query();
                                        while (($nacio = $p_nacionalidad->read()) !== false) {
                                            if($p_persona['nacio_value']==$nacio['nacio_value']){
                                                echo "<option value='".$nacio['nacio_value']."' selected>".$nacio['nacio_descr']."</option>";
                                            }else{
                                                echo "<option value='".$nacio['nacio_value']."'>".$nacio['nacio_descr']."</option>";
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Cédula</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <?php 
                                $cedula=$this->funciones->TransformarMonto_v($p_persona['perso_cedul'],0);
                                echo CHtml::textField('cedula', $cedula, array('class' => 'form-control', 'placeholder' => "Cédula", 'readonly' => "true")); ?>

                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Primer Nombre</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <?php echo CHtml::textField('pnomb', $p_persona['perso_pnomb'], array('class' => 'form-control', 'placeholder' => "Primer Nombre", 'readonly'=>'true')); ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Segundo Nombre</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <?php echo CHtml::textField('snomb', $p_persona['perso_snomb'], array('class' => 'form-control', 'placeholder' => "Segundo Nombre", 'readonly'=>'true')); ?>

                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Primer Apellido</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <?php echo CHtml::textField('papel', $p_persona['perso_papel'], array('class' => 'form-control', 'placeholder' => "Primer Apellido", 'readonly'=>'true')); ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Segundo Apellido</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <?php echo CHtml::textField('sapel', $p_persona['perso_sapel'], array('class' => 'form-control', 'placeholder' => "Segundo Apellido", 'readonly'=>'true')); ?>

                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Fecha de Nacimiento</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                <?php 
                                $p_persona['perso_fnaci'];
                                $perso_fnaci=$this->funciones->TransformarFecha_v($p_persona['perso_fnaci']);
                                echo CHtml::textField('fnaci', $perso_fnaci, array('class' => 'form-control', 'placeholder' => "Fecha de Nacimiento", 'readonly'=>'true')); ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Genero</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <select class="form-control" name="gener" id="gener" readonly>
                                    <option value=''>Seleccione</option>
                                    <?php
                                        $sql="SELECT * FROM p_genero";
                                        $p_genero=$conexion->createCommand($sql)->query();
                                        while (($gener = $p_genero->read()) !== false) {
                                            if($p_persona['gener_value']==$gener['gener_value']){
                                                echo "<option value='".$gener['gener_value']."' selected>".$gener['gener_descr']."</option>";
                                            }else{
                                                echo "<option value='".$gener['gener_value']."'>".$gener['gener_descr']."</option>";
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Estado Civil</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <select class="form-control" name="ecivi" id="ecivi" disabled="">
                                    <option value=''>Seleccione</option>
                                    <?php
                                        $sql="SELECT * FROM p_estado_civil";
                                        $p_estadoscivil=$conexion->createCommand($sql)->query();
                                        while (($ecivi = $p_estadoscivil->read()) !== false) {
                                            if($p_persona['ecivi_value']==$ecivi['ecivi_value']){
                                                echo "<option value='".$ecivi['ecivi_value']."' selected>".$ecivi['ecivi_descr']."</option>";
                                            }else{
                                                echo "<option value='".$ecivi['ecivi_value']."'>".$ecivi['ecivi_descr']."</option>";
                                            }

                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observaciones</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
                                <?php echo CHtml::textArea('obser',$p_persona['perso_obser'], array('class' => 'form-control', 'placeholder' => "Observaciones", 'readonly'=>'true')); ?>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                    <!-- Button -->
                    <div class="row controls">
                        <div class="col-xs-12 ">
                            <a href="listado" id="guardar" type="button" class="btn-block btn btn-info">Volver  </a>

                        </div>
                    </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
<script>
$('#fnaci').mask('00/00/0000');
$('#desde').mask('00/00/0000');
</script>