<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Colegio</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Parametros</a></li>
            <li><a href="#">Colegio</a></li>
            <li class="active">Consultar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Consultar Colegio</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row hide">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Código</label>
                            
                                <?php 
                                echo CHtml::textField('codig', $colegio['coleg_codig'], array('class' => 'form-control', 'placeholder' => "Código", "disabled"=>'true')); ?>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Nombre</label>
                                <?php 
                                    echo CHtml::textField('nombr', $colegio['coleg_nombr'], array('class' => 'form-control', 'placeholder' => "Descripción", "disabled"=>'true')); 
                                ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Teléfono</label>
                                <?php 
                                    echo CHtml::textField('telef', $colegio['colec_telef'], array('class' => 'form-control', 'placeholder' => "Descripción", "disabled"=>'true')); 
                                ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Sitio Web</label>
                                <?php 
                                    echo CHtml::textField('pgweb', $colegio['coleg_pgweb'], array('class' => 'form-control', 'placeholder' => "Descripción", "disabled"=>'true')); 
                                ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Instagram</label>
                                <?php 
                                    echo CHtml::textField('rede1', $colegio['coleg_rede1'], array('class' => 'form-control', 'placeholder' => "Descripción", "disabled"=>'true')); 
                                ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Facebook</label>
                                <?php 
                                    echo CHtml::textField('rede2', $colegio['coleg_rede2'], array('class' => 'form-control', 'placeholder' => "Descripción", "disabled"=>'true')); 
                                ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Twitter</label>
                                <?php 
                                    echo CHtml::textField('rede3', $colegio['coleg_rede3'], array('class' => 'form-control', 'placeholder' => "Descripción", "disabled"=>'true')); 
                                ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Dirección</label>
                                <?php 
                                    echo CHtml::textArea('direc', $colegio['coleg_direc'], array('class' => 'form-control', 'placeholder' => "Dirección", "disabled"=>'true')); 
                                ?>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Observaciones</label>
                                <?php 
                                    echo CHtml::textArea('obser', $colegio['coleg_obser'], array('class' => 'form-control', 'placeholder' => "Observaciones", "disabled"=>'true')); 
                                ?>
                        </div>
                    </div>
                </div>
                
                
                    <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-12">
                            <a href="listado" type="reset" class="btn-block btn btn-info">Volver </a>
                        </div>
                    </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
