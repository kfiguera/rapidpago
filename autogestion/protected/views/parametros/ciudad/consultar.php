<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Ciudades</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Parametros</a></li>
            <li><a href="#">Ciudades</a></li>
            <li class="active">Consultar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Consultar Ciudades</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Paises *</label>
                            <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT a.paise_codig, a.paise_descr
                                      FROM p_paises a
                                      JOIN p_estados b ON (a.paise_codig = b.paise_codig)
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'paise_codig','paise_descr');
                                echo CHtml::dropDownList('paise', $roles['paise_codig'], $data, array('ajax' => array(
                                            'type' => 'POST',
                                            'url' => CController::createUrl('funciones/ComboEstados'), // Controlador que devuelve las p_ciudadeses relacionadas
                                            'update' => '#estad', // id del item que se actualizará
                                        ),'class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Estados *</label>
                            <?php 
                                $sql="SELECT a.estad_codig, a.estad_descr
                                      FROM p_estados a 
                                      WHERE paise_codig='".$roles['paise_codig']."'
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'estad_codig','estad_descr');
                                echo CHtml::dropDownList('estad', $roles['estad_codig'], $data, array('ajax' => array(
                                            'type' => 'POST',
                                            'url' => CController::createUrl('funciones/ComboMunicipios'), // Controlador que devuelve las p_ciudadeses relacionadas
                                            'update' => '#munic', // id del item que se actualizará
                                        ),'class' => 'form-control', 'placeholder' => "Paises",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                        </div>
                    </div>

                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Capital *</label>
                            <?php 
                                $data=array('0' => 'NO', '1'=>'SI');
                                echo CHtml::dropDownList('capit', $roles['ciuda_capit'], $data, array('class' => 'form-control', 'placeholder' => "Paises",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Descripción</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php echo CHtml::textField('descr', $roles['ciuda_descr'], array('class' => 'form-control', 'placeholder' => "Descripción",'disabled'=>'true')); ?>

                            </div>
                        </div>
                    </div>
                    
                </div>
               
                
                    <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-12">
                            <a href="listado" type="reset" class="btn-block btn btn-info">Volver </a>
                        </div>
                    </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
