<?php $conexion = Yii::app()->db; ?>
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Solicitudes</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Registro y Documentación</a></li>
            <li><a href="#">Solicitudes</a></li>
            <li class="active">Registrar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php $this->renderPartial('pasos', array('ubicacion' => $ubicacion,'solicitud'=>$solicitud));?>

<form id='login-form' name='login-form' method="post">

<div class="row">   
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Datos Adicionales</h3>
        </div>
        <div class="panel-body" >
            <div class="row hide">
                <div class="col-sm-4">        
                    <div class="form-group">
                        <label>Pedido *</label>
                        <?php 
                            echo CHtml::hiddenField('pedid', $p, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                    </div>
                </div>
                <div class="col-sm-4">        
                    <div class="form-group">
                        <label>Código *</label>
                        <?php 
                            echo CHtml::hiddenField('codig', $c, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                    </div>
                </div>
                <div class="col-sm-4">        
                    <div class="form-group">
                        <label>Paso *</label>
                        <?php 
                            echo CHtml::hiddenField('pasos', $solicitud['prere_pasos'], array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                    </div>
                </div>
            </div>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
                
                <div class="row">
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Fecha de la Planilla *</label>
                            <?php

                                echo CHtml::textField('fsoli', $this->funciones->transformarFecha_v($solicitud['prere_fsoli']), array('class' => 'form-control', 'placeholder' => "Fecha de solicitud",'prompt'=>'Seleccione...')); ?>
                               
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Origen *</label>
                            <?php
                                $sql="SELECT * FROM p_solicitud_origen
                                    WHERE orige_codig=2
                                    ORDER BY 2";

                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'orige_codig','orige_descr');

                                echo CHtml::dropDownList('orige', 2, $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','readonly'=>'true')); ?>
                               
                        </div>
                    </div>
                    <div class="col-sm-4 hide">        
                        <div class="form-group">
                            <label>Cliente VIP *</label>
                            <?php
                                
                                $data= array('2' => 'NO' , '1' => 'SI');

                                echo CHtml::dropDownList('clvip', $solicitud['prere_clvip'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                               
                        </div>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observaciones  </label>

                            <?php
                                echo CHtml::textArea('obser', $solicitud['prere_obser'], array('class' => 'form-control', 'placeholder' => "Observaciones",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                </div>
            </div><!-- form -->
    </div>  
</div>
<div class="row">   
    <div class="panel panel-default">
        <div class="panel-body" >
                <!-- Button -->
                <div class="row controls">
                    <div class="col-sm-4 ">
                        <a href="modificar?c=<?php echo $c?>&s=3" type="reset" class="btn-block btn btn-default">Volver </a>
                    </div>
                    <div class="col-sm-4 ">
                        <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                    </div>
                    <div class="col-sm-4 ">
                        <button id="guardar" type="button" class="btn-block btn btn-info">Siguiente  </button>
                    </div>
                </div>


        </div><!-- form -->
    </div>  
</div>
</form>
<script type="text/javascript">
    $(document).ready(function () {
        jQuery('#fsoli').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        endDate: '0d',
      });
    });
</script>
<script type="text/javascript">
    var fsoli = {
            validators: {
                notEmpty: {
                    message: 'Estimado(a) Usuario(a) el campo "Nombre" es obligatorio',
                }
            }
        },
        image = {
            validators: {
                notEmpty: {
                    message: 'Estimado(a) Usuario(a) el campo "Imagen a Bordar" es obligatorio',
                }
            }
        },
        texto = {
            validators: {
                notEmpty: {
                    message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar" es obligatorio',
                }
            }
        },
        bookIndex = <?php echo $a; ?>,
        contador = 0;
        
</script>
<script>
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                fsoli: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fecha de Solicitud" es obligatorio',
                        },date: {
                            format: 'DD/MM/YYYY',
                            message: 'Estimado(a) Usuario(a) el campo "Fecha de Solicitud" debe poseer el siguiente formato DD/MM/YYYY',
                        }
                    }
                },
                orige: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Origen" es obligatorio',
                        }
                    }
                },
                clvip: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Cliente VIP" es obligatorio',
                        }
                    }
                }
                //'descr[0]': descr,
                //'image[0]': image,
                //'texto[0]': texto,

                
            }
        });
    });
    
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        var data = new FormData(jQuery('form')[0]);
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                url: 'paso4',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        /*swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){*/
                            window.open('modificar?c=<?php echo $c; ?>&s=5', '_parent');
                        /*});*/
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>