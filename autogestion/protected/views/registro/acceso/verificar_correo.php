<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Polerones Tiempo | Solicitud de Acceso</title>
</head>
<body style="margin:0px; background: #f8f8f8; ">
<div width="100%" style="background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;">
  <div style="max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px">
      <tbody>
        <tr>
          <td style="vertical-align: top; padding-bottom:30px;" align="center">
            <img src="https://autogestion.rapidpago.com/assets/img/logo.png"  width="350px" style="border:none"></td>
        </tr>
      </tbody>
    </table>
    <div style="padding: 40px; background: #fff;">
      <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tbody>
          <tr>
            <td style="border-bottom:1px solid #f6f6f6;"><h1 style="font-size:14px; font-family:arial; margin:0px; font-weight:bold;">Estimado(a) <?php echo $p_persona['perso_pnomb'].' '.$p_persona['perso_papel'];?>,</h1>
              <p>La información suministrada para su solicitud, ha sido: 
                <?php
                  if($estatus=='1'){
                    echo '<span style="color: #00ff00; font-weight: bold;">APROBADA</span>';
                  }else{
                    echo '<span style="color: #ff0000; font-weight: bold;">RECHAZADA</span>';
                  }
                ?></p>

                <?php
                  if($estatus=='1'){
                    echo '<p><b>Sus credenciales para acceder a nuestro Sistema de Autogestión son: : </b><br>';
                    echo '<b>Usuario: </b>'.$usuario.'<br>';
                    echo '<b>Contraseña: </b>'.$clave.'</p>';
                    
                  }else{
                    echo '<p><b>Motivo: </b>'.$motivo.'</p>';
                  }
                ?>
                <center>
                  <a href="https://autogestion.rapidpago.com/site/login" style="display: inline-block; padding: 11px 30px; margin: 20px 0px 30px; font-size: 15px; color: #fff; background: #5475ed; border-radius: 60px; text-decoration:none;">Haga clic aquí para continuar</a>
                </center>';
              
              <p>- Muchas gracias.</p> 
              <p style="text-align: center;">Para RapidPago, Tu eres el Punto.</p>
          </tr>
          <tr>
            <td  style="border-top:1px solid #f6f6f6; padding-top:20px; color:#777">
              <p>En caso de asistencia comercial, escriba a ventas@rapidpago.com.</p></td></td>
          </tr>
        </tbody>
      </table>
    </div>
    
    <div style="text-align: center; font-size: 12px; color: #b2b2b5; margin-top: 20px">
      <p> <a href="https://Innova Technology.net">Elaborado por Innova Technology</a> <p>
    </div>
  </div>
</div>
</body>
</html>
