<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

		<style>

			body {
				font-family: ipamp;
		 		font-size: 10pt;
		 	}
		 	
		 	td { 
		 		vertical-align: middle; 
		 	}
		 	p{
		 		text-align: justify;
		 	}
		 	.items td {
		 		/*border-left: 0.1mm solid #000000;
		 		border-right: 0.1mm solid #000000;*/
		 		border: 0.1mm solid #000000;
		 		font-size: 8pt;
		 		text-align: left;
		 		padding: 2pt;
		 	}
		 	.items td img {
		 		max-height: 300px;
		 		max-width: 300px
		 		width:100%;
		 		height: auto;
		 	}
		 	table thead td, table thead th, table tfoot td, table tfoot th { 
		 		background-color: #EEEEEE;
		 		text-align: center;
		 		border: 0.1mm solid #000000;
		 		font-size: 8pt;
		 		padding: 2pt;
		 	}
		 	.items td.blanktotal {
		 		background-color: #FFFFFF;
		 		border: 0mm none #000000;
		 		border-top: 0.1mm solid #000000;
		 	}
		 	.items td.totals {
		 		text-align: right;
		 		border: 0.1mm solid #000000;
		 	}
		 	.text-center{
		 		text-align: center;
		 	}
		 	.text-left{
		 		text-align: left;
		 	}
		 	.text-right{
		 		text-align: right;
		 	}
		 	.img-responsive{
		 		width: 100%;
		 		height: auto;
		 		max-width: 50px;
		 	}
		</style>
	</head>
	<body>
		<?php
			$conexion=yii::app()->db;
			
			$sql="SELECT * FROM solicitud_entrega WHERE solic_codig='".$c."'";
			$entrega=$conexion->createCommand($sql)->queryRow();		

			$sql="SELECT * FROM solicitud WHERE solic_codig='".$c."'";
			$solicitud=$conexion->createCommand($sql)->queryRow();		
			
			$sql="SELECT * FROM cliente WHERE clien_codig='".$solicitud['clien_codig']."'";
			
			$cliente=$conexion->createCommand($sql)->queryRow();

			$sql="SELECT * FROM cliente_direccion WHERE clien_codig='".$solicitud['clien_codig']."'";
			
			$direccion=$conexion->createCommand($sql)->queryRow();

			$sql="SELECT * FROM p_courier WHERE couri_codig='".$entrega['couri_codig']."'";
			
			$courier=$conexion->createCommand($sql)->queryRow();

        ?>
        <table width="100%" style="font-size: 8pt; border-collapse: collapse;" cellpadding="5">
        	<tr>
        		<td align="left"><b>CÓDIGO DE NOTA DE ENTREGA:</b> <?php echo $entrega['entre_numer'];?> </td>
        		<td align="right"><b>FECHA DE REGISTRO:</b> <?php echo $this->funciones->transformarFecha_v($entrega['entre_fcrea']);?></td>
        	</tr>
        </table>
		<h4 class="text-center">DATOS DEL CLIENTE</h4>
	        
	    <table class="items" width="100%" style="border-collapse: collapse;" cellpadding="5">
	    		<tr>
	    			<td width="25%">NÚMERO DE SOLICITUD </td>
	    			<td width="25%">RIF</td>
					<td width="50%">RAZON SOCIAL</td>
	    		</tr>
	    		<tr>
		    		<td><?php echo $solicitud['solic_numer']?></td>
		    		<td><?php echo $cliente['clien_rifco']?></td>
					<td><?php echo $cliente['clien_rsoci']?></td>
					
				</tr>
				<tr>
	    			<td >TÉLEFONO </td>
	    			<td colspan="2">DIRECCIÓN</td>
	    		</tr>
	    		<tr>
		    		<td><?php echo $cliente['clien_tmovi']?></td>
		    		<td colspan="2"><?php echo $direccion['direc_dfisc']?></td>
					
				</tr>
	    </table>
	    <?php
            if($entrega['tentr_codig']=='2'){
        ?>
            <div class="col-sm-12">
                <h4 class="text-center">DATOS DEL ENVIO</h4>
                <table class="items" width="100%" style="border-collapse: collapse;" cellpadding="5">
                    <thead>
                        <tr>
                            <th width="5%"># </th>
                            <th width="19%">Courier </th>
                            <th width="19%">Agencia</th>
                            <th width="19%">Direción</th> 
                            <th width="19%">Guia</th>    
                        </tr>    
                    </thead>
                    <tbody>
                    <?php
                    $i=1;
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $courier['couri_descr']; ?></td>
                            <td><?php echo $entrega['entre_agenc']; ?></td>
                            <td><?php echo $entrega['entre_direc']; ?></td>
                            <td><?php echo $entrega['entre_nguia']; ?></td>
                        </tr>
                        <?php                ?>
                    </tbody>
                </table>                           
            </div>
        <?php
            }
        ?>
	    <?php
            $sql="SELECT * FROM solicitud_asignacion a
                  WHERE solic_codig='".$solicitud['solic_codig']."'";
            $asignacion=$conexion->createCommand($sql)->queryAll();
            if($asignacion){
        ?>
            <div class="col-sm-12">
                <h4 class="text-center">DISPOSITIVOS</h4>
                <table class="items" width="100%" style="border-collapse: collapse;" cellpadding="5">
                    <thead>
                        <tr>
                            <th width="5%"># </th>
                            <th width="19%">Dispositivo </th>
                            <th width="19%">Modelo</th>
                            <th width="19%">Serial</th> 
                            <th width="19%">Operador Telefónico</th>    
                            <th width="19%">Serial SIM</th>    
                        </tr>    
                    </thead>
                    <tbody>
                    <?php
                    $i=0;
                    foreach ($asignacion as $key => $value) {
                        $i++;
                        $sql="SELECT * 
                              FROM inventario_seriales a
                              JOIN inventario_modelo b ON (a.model_codig = b.model_codig)
                              JOIN inventario c ON (a.inven_codig = c.inven_codig)
                              WHERE a.seria_codig='".$value['asign_dispo']."'";
                        $dispo=$conexion->createCommand($sql)->queryRow();

                        $sql="SELECT * 
                              FROM inventario_seriales a
                              JOIN inventario_modelo b ON (a.model_codig = b.model_codig)
                              JOIN inventario c ON (a.inven_codig = c.inven_codig)
                              WHERE a.seria_codig='".$value['asign_opera']."'";
                        $opera=$conexion->createCommand($sql)->queryRow();
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $dispo['inven_descr']; ?></td>
                            <td><?php echo $dispo['model_descr']; ?></td>
                            <td><?php echo $dispo['seria_numer']; ?></td>
                            <td><?php echo $opera['model_descr']; ?></td>
                            <td><?php echo $opera['seria_numer']; ?></td>
                        </tr>
                        <?php
                    }
                ?>
                    </tbody>
                </table>                           
            </div>
        <?php
            }
        ?>

    </body>
 </html>
 <?php //exit(); ?>