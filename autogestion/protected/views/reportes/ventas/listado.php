<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Reporte de Cobranza</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Reportes</a></li>            
            
            <li><a href="#">Cobranza</a></li>
            <li class="active">Listado</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Criterio de Busqueda</h3>
    </div>
    <div class="panel-body" >
        <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'form', 'action'=>'pdf', 'htmlOptions' => array('target'=>'_blank','id'=>'form','method' => 'post', 'enctype' => 'multipart/form-data')));
            $connection = Yii::app()->db;

        ?>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Número de Factura</label>
                    <?php echo CHtml::textField('nfact', '', array('class' => 'form-control', 'placeholder' => "Número de Factura")); ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Banco</label>
                    <?php 
                    $sql="SELECT * FROM p_banco ";
                    $result=$connection->createCommand($sql)->queryAll();
                    $data=CHtml::listData($result,'banco_codig','banco_descr');
                    echo CHtml::DropDownList('banco', '', $data,array('class' => 'form-control', 'placeholder' => "Banco", 'prompt'=>'Seleccione')); ?>
                </div>
            </div>
            <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Vendedor</label>
                                <?php 
                                    $sql="SELECT traba_codig, concat(perso_pnomb,' ',perso_papel) nombre FROM p_trabajador a 
                                          JOIN p_persona b ON (a.perso_codig=b.perso_codig)
                                          WHERE ttipo_codig=1";
                                    $result=$connection->createCommand($sql)->queryAll();
                                    $data=CHtml::listData($result,'traba_codig','nombre');
                                    echo CHtml::dropDownList('traba', '', $data,array('class' => 'form-control', 'placeholder' => "Descripción", 'prompt'=>'Seleccione...')); 
                                ?>
                        </div>
                    </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Fecha de Emision Desde</label>
                    <?php echo CHtml::textField('desde', '', array('class' => 'form-control', 'placeholder' => "DD/MM/AAAA")); ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Fecha de Emision Hasta</label>
                    <?php echo CHtml::textField('hasta', '', array('class' => 'form-control', 'placeholder' => "DD/MM/AAAA")); ?>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-6">
                <button type="button" id="buscar" class="btn btn-block btn-info">Buscar</button>
            </div>
            <div class="col-sm-6">
                <button type="reset" class="btn btn-block btn-default">Limpiar</button>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div> 
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-sm-8">
                <h3 class="panel-title">Listado</h3>
            </div>
            <div class="col-sm-4">
                <button type="button" id="imprimir" class="btn btn-block btn-info">Imprimir</button>
            </div>
        </div>
    </div>
    <div class="panel-body" >
        
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-p_persona'>
                <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="1%">#</th>
                            <th width="10%">FECHA EMISION </th>
                            <th width="10%">NRO FACTURA</th>
                            <th width="14%">CLIENTE</th>
                            <th width="10%">MONTO</th>
                            <th width="10%">COMISION</th>
                            <th width="10%">PUNTOS</th>
                            <th width="10%">VENTA</th>
                            <th width="15%">BANCO</th>
                            <th width="15%">OPERACION</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <?php
                        $sql = "SELECT *
                            FROM factura a
                            JOIN cliente b ON (a.clien_codig = b.clien_codig)
                            JOIN p_banco c ON (a.banco_codig = c.banco_codig)
                            JOIN factura_estatu d ON (a.efact_codig = d.efact_codig)
                            JOIN p_trabajador e ON (a.traba_codig=e.traba_codig)
                            JOIN p_persona f ON (e.perso_codig=f.perso_codig)";
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                            $venta=$row['factu_monto']-$row['factu_comis']-$row['factu_desc'];
                        ?>
                        <tr>
                            <td><?php echo $i ?></td>
                            <td><?php echo $this->funciones->TransformarFecha_v($row['factu_femis']) ?></td>
                            <td><?php echo $row['factu_nfact'] ?></td>
                            <td><?php echo $row['clien_denom'] ?></td>
                            <td><?php echo $this->funciones->TransformarMonto_v($row['factu_monto'],2) ?></td>
                            <td><?php echo $this->funciones->TransformarMonto_v($row['factu_comis'],2) ?></td>
                            
                            <td><?php echo $this->funciones->TransformarMonto_v($row['factu_desc'],2) ?></td>
                            <td><?php echo $this->funciones->TransformarMonto_v($venta,2) ?></td>
                            <td><?php echo $row['banco_descr'] ?></td>
                            <td><?php echo $row['factu_refer'] ?></td>
                        </tr>
                        <?php
                            }   
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>  
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
    $('#desde').mask('99/99/9999');
    $('#hasta').mask('99/99/9999');
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#buscar').click(function () {
        var formData = new FormData($("#form")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-p_persona').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
    $('#imprimir').click(function(){
        var formData = new FormData($("#form")[0]);
        $("#form").submit();
    });
    jQuery('#desde').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'

      });
    jQuery('#hasta').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'

      });
</script>
