<html>
	<head>
		<style>
			body {
				font-family: sans-serif;
		 		font-size: 10pt;
		 	}
		 	p { 
		 		margin: 0pt;
		 	}
		 	td { 
		 		vertical-align: bottom; 
		 	}
		 	.items td {
		 		/*border-left: 0.1mm solid #000000;
		 		border-right: 0.1mm solid #000000;*/
		 		border: 0.1mm solid #000000;
		 		font-size: 4pt;
		 		text-align: center;
		 		padding: 1pt;
		 	}
		 	table thead td, table thead th { 
		 		background-color: #EEEEEE;
		 		text-align: center;
		 		border: 0.1mm solid #000000;
		 		font-size: 4pt;
		 		padding: 2pt;
		 	}
		 	.items td.blanktotal {
		 		background-color: #FFFFFF;
		 		border: 0mm none #000000;
		 		border-top: 0.1mm solid #000000;
		 	}
		 	.items td.totals {
		 		text-align: right;
		 		border: 0.1mm solid #000000;
		 	}
		</style>
	</head>
	<body>
		<!--div style="text-align: right"><b>Fecha: </b><?php echo date("d/m/Y"); ?> </div>
		<b>Total Registros:</b> 12-->
		 
		 <table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse;" cellpadding="5">
		 <thead>
		 <tr>
		 	<th width="1%">#</th>
            <th width="11%">FECHA EMISION </th>
            <th width="11%">NRO FACTURA</th>
            <th width="11%">CLIENTE</th>
            <th width="11%">MONTO</th>
            <th width="11%">COMISION</th>
            <th width="11%">PUNTOS</th>
            <th width="11%">VENTA</th>
            <th width="11%">BANCO</th>
            <th width="11%">OPERACION</th>

		 </tr>
		 </thead>
		 <tbody>
		 <?php
            $sql = "SELECT *
                    FROM factura a
                    JOIN cliente b ON (a.clien_codig = b.clien_codig)
                    JOIN p_banco c ON (a.banco_codig = c.banco_codig)
                    JOIN factura_estatu d ON (a.efact_codig = d.efact_codig)
                    JOIN p_trabajador e ON (a.traba_codig=e.traba_codig)
                    JOIN p_persona f ON (e.perso_codig=f.perso_codig)
                ".$condicion;
            $connection=yii::app()->db;
            $command = $connection->createCommand($sql);
            $p_persona = $command->query();
            $i=0;
            while (($row = $p_persona->read()) !== false) {
                $i++;
                $venta=$row['factu_monto']-$row['factu_comis']-$row['factu_desc'];
            ?>
            <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $this->funciones->TransformarFecha_v($row['factu_femis']) ?></td>
                <td><?php echo $row['factu_nfact'] ?></td>
                <td><?php echo $row['clien_denom'] ?></td>
                <td><?php echo $this->funciones->TransformarMonto_v($row['factu_monto'],2) ?></td>
                <td><?php echo $this->funciones->TransformarMonto_v($row['factu_comis'],2) ?></td>
                
                <td><?php echo $this->funciones->TransformarMonto_v($row['factu_desc'],2) ?></td>
                <td><?php echo $this->funciones->TransformarMonto_v($venta,2) ?></td>
                <td><?php echo $row['banco_descr'] ?></td>
                <td><?php echo $row['factu_refer'] ?></td>
            </tr>
            <?php
                }   
            ?>
		 <!-- FIN ITEMS -->
		 <tr>
		 <td class="blanktotal" colspan="18"></td>
		 </tr>
		 </tbody>
		 </table>

	 </body>
 </html>
