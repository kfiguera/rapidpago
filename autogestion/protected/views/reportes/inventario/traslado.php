<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

		<style>

			body {
				font-family: ipamp;
		 		font-size: 10pt;
		 	}
		 	
		 	td { 
		 		vertical-align: middle; 
		 	}
		 	p{
		 		text-align: justify;
		 	}
		 	.items td {
		 		/*border-left: 0.1mm solid #000000;
		 		border-right: 0.1mm solid #000000;*/
		 		border: 0.1mm solid #000000;
		 		font-size: 8pt;
		 		text-align: left;
		 		padding: 2pt;
		 	}
		 	.items td img {
		 		max-height: 300px;
		 		max-width: 300px
		 		width:100%;
		 		height: auto;
		 	}
		 	table thead td, table thead th, table tfoot td, table tfoot th { 
		 		background-color: #EEEEEE;
		 		text-align: center;
		 		border: 0.1mm solid #000000;
		 		font-size: 8pt;
		 		padding: 2pt;
		 	}
		 	.items td.blanktotal {
		 		background-color: #FFFFFF;
		 		border: 0mm none #000000;
		 		border-top: 0.1mm solid #000000;
		 	}
		 	.items td.totals {
		 		text-align: right;
		 		border: 0.1mm solid #000000;
		 	}
		 	.text-center{
		 		text-align: center;
		 	}
		 	.text-left{
		 		text-align: left;
		 	}
		 	.text-right{
		 		text-align: right;
		 	}
		 	.img-responsive{
		 		width: 100%;
		 		height: auto;
		 		max-width: 50px;
		 	}
		</style>
	</head>
	<body>
		<?php
			$conexion=yii::app()->db;
			$sql="SELECT * FROM solicitud_traslado WHERE trasl_codig='".$c."'";
			$traslado=$conexion->createCommand($sql)->queryRow();		
			

			$sql="SELECT * 
				  FROM solicitud_traslado a
				  JOIN solicitud_movimiento_traslado b ON (a.trasl_codig = b.trasl_codig)
				  JOIN inventario_movimiento c ON (b.movim_codig = c.movim_codig)
				  WHERE a.trasl_codig='".$c."' 
				    AND c.mesta_codig<>'3'";
			$traslado=$conexion->createCommand($sql)->queryRow();

			$sql="SELECT * 
				  FROM inventario_almacen a
				  WHERE a.almac_codig='".$traslado['almac_orige']."'";
			$origen=$conexion->createCommand($sql)->queryRow();		
			$sql="SELECT * 
				  FROM inventario_almacen a
				  WHERE a.almac_codig='".$traslado['almac_desti']."'";
			$destino=$conexion->createCommand($sql)->queryRow();

			$sql="SELECT * 
				  FROM p_inventario_movimiento_estatus a
				  WHERE a.mesta_codig='".$traslado['mesta_codig']."'";
			$estatus=$conexion->createCommand($sql)->queryRow();		

        ?>
        <table width="100%" style="font-size: 8pt; border-collapse: collapse;" cellpadding="5">
        	<tr>
        		<td align="left"><b>CÓDIGO DE MOVIMIENTO:</b> <?php echo $traslado['movim_numer'];?> </td>
        		<td align="left"><b>TRASLADO NÚMERO:</b> <?php echo $traslado['trasl_codig'];?> </td>
        		<td align="right"><b>FECHA DE REGISTRO:</b> <?php echo $this->funciones->transformarFecha_v($traslado['trasl_fcrea']);?></td>
        	</tr>
        </table>
		<h3 class="text-center">DATOS DEL TRASLADO</h3>
	        
	    <table class="items" width="100%" style="border-collapse: collapse;" cellpadding="5">
	    	<thead>
	    		<tr>
	    			<th width="16,66%">CÓDIGO DE MOVIMIENTO</th>
					<th width="16,66%">TRASLADO NÚMERO</th>
					<th width="16,66%">ALMACEN ORIGEN</th>
					<th width="16,66%">ALMACEN DESTINO</th>
					<th width="16,66%">ESTATUS</th>
					<th width="16,66%">FECHA DE REGISTRO</th>
	    		</tr>
	    	</thead>
	    	<tbody>
	    		<tr>
		    		<td><?php echo $traslado['movim_numer']?></td>
					<td><?php echo $traslado['trasl_codig']?></td>
					<td><?php echo $origen['almac_numer'].' '.$origen['almac_descr']?></td>
					<td><?php echo $destino['almac_numer'].' '.$destino['almac_descr']?></td>
					<td><?php echo $estatus['mesta_descr']?></td>
					<td><?php echo $this->funciones->transformarFecha_v($traslado['trasl_fcrea']) ?></td>
				</tr>
	    	</tbody>
	    </table>
	    <h3 class="text-center">DISPOSITIVOS</h3>
	    <table class="items" width="100%" style="border-collapse: collapse;" cellpadding="5">
	    	<thead>
	    		<tr>
	    			<th width="5%">#</th>
	    			<th width="19%">SOLICITUD</th>
					<th width="19%">TIPO ARTICULO</th>
					<th width="19%">MODELO</th>
					<th width="19%">ARTICULO</th>
					<th width="19%">SERIAL</th>
	    		</tr>
	    	</thead>
	    	<tbody>
	    	<?php

	    		$sql="SELECT * 
                                        FROM inventario_movimiento_detalle a 
                                        JOIN inventario_seriales b ON (a.seria_codig = b.seria_codig)
                                        JOIN inventario c ON (b.inven_codig = c.inven_codig)
                                        JOIN p_inventario_tipo d ON (c.tinve_codig = d.tinve_codig)
                                        JOIN inventario_modelo e ON (c.model_codig = e.model_codig)
                                        JOIN solicitud_movimiento_traslado f ON (a.movim_codig = f.movim_codig)
                                        JOIN solicitud g ON (f.solic_codig = g.solic_codig)
                                        JOIN solicitud_asignacion h ON (g.solic_codig = h.solic_codig and a.seria_codig = h.asign_dispo)
                                        WHERE a.movim_codig = '".$traslado['movim_codig']."'
                                        UNION
                                        SELECT * 
                                        FROM inventario_movimiento_detalle a 
                                        JOIN inventario_seriales b ON (a.seria_codig = b.seria_codig)
                                        JOIN inventario c ON (b.inven_codig = c.inven_codig)
                                        JOIN p_inventario_tipo d ON (c.tinve_codig = d.tinve_codig)
                                        JOIN inventario_modelo e ON (c.model_codig = e.model_codig)
                                        JOIN solicitud_movimiento_traslado f ON (a.movim_codig = f.movim_codig)
                                        JOIN solicitud g ON (f.solic_codig = g.solic_codig)
                                        JOIN solicitud_asignacion h ON (g.solic_codig = h.solic_codig and a.seria_codig = h.asign_opera)
                                        WHERE a.movim_codig = '".$traslado['movim_codig']."'
                                        ORDER BY solic_numer";
		    	$dispositivos = $conexion->createCommand($sql)->queryAll();
		    	
		    	$i=0;
		    	foreach ($dispositivos as $key => $value) {
		    		$i++;
		    		?>
		    		<tr>
		    			<td><?php echo $i; ?></td>
		    			<td><?php echo $value['solic_numer']; ?></td>
		    			<td><?php echo $value['tinve_descr']; ?></td>
		    			<td><?php echo $value['model_descr']; ?></td>
		    			<td><?php echo $value['inven_descr']; ?></td>
		    			<td><?php echo $value['seria_numer']; ?></td>
		    		</tr>
		    		<?php
		    	}
		    ?>
			</tbody>
		</table>

    </body>
 </html>
 <?php //exit(); ?>