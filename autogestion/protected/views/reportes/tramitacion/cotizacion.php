<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

		<style>

			body {
				font-family: ipamp;
		 		font-size: 10pt;
		 	}
		 	
		 	td { 
		 		vertical-align: middle; 
		 	}
		 	p{
		 		text-align: justify;
		 	}
		 	.items td {
		 		/*border-left: 0.1mm solid #000000;
		 		border-right: 0.1mm solid #000000;*/
		 		border: 0.1mm solid #000000;
		 		font-size: 8pt;
		 		text-align: left;
		 		padding: 2pt;
		 	}
		 	.items td img {
		 		max-height: 300px;
		 		max-width: 300px
		 		width:100%;
		 		height: auto;
		 	}
		 	table thead td, table thead th, table tfoot td, table tfoot th { 
		 		background-color: #EEEEEE;
		 		text-align: center;
		 		border: 0.1mm solid #000000;
		 		font-size: 10pt;
		 		padding: 2pt;
		 	}
		 	.items td.blanktotal {
		 		background-color: #FFFFFF;
		 		border: 0mm none #000000;
		 		border-top: 0.1mm solid #000000;
		 	}
		 	.items td.totals {
		 		text-align: right;
		 		border: 0.1mm solid #000000;
		 	}
		 	.text-center{
		 		text-align: center;
		 	}
		 	.text-left{
		 		text-align: left;
		 	}
		 	.text-right{
		 		text-align: right;
		 	}
		 	.img-responsive{
		 		width: 100%;
		 		height: auto;
		 		max-width: 50px;
		 	}
		</style>
	</head>
	<body>
		<?php
			$conexion=yii::app()->db;
			$sql="SELECT * FROM solicitud WHERE solic_codig='".$c."'";
			$solicitud=$conexion->createCommand($sql)->queryRow();		

			$sql="SELECT * FROM solicitud_cotizacion WHERE solic_codig='".$c."' and cotiz_estat='1'";
			$cotizacion=$conexion->createCommand($sql)->queryRow();		

			$sql="SELECT * FROM cliente WHERE clien_codig='".$solicitud['clien_codig']."'";
			$cliente=$conexion->createCommand($sql)->queryRow();
        ?>
        <table width="100%" style="font-size: 8pt; border-collapse: collapse;" cellpadding="5">
        	<tr>
        		<td align="left"><b>CÓDIGO DE SOLICITUD:</b> <?php echo $solicitud['solic_numer'];?> </td>
        		<td align="left"><b>COTIZACIÓN NÚMERO:</b> <?php echo $cotizacion['cotiz_codig'];?> </td>
        		<td align="right"><b>FECHA DE REGISTRO:</b> <?php echo $this->funciones->transformarFecha_v($pedidos['pedid_fcrea']); echo date('d/m/Y');?></td>
        	</tr>
        </table>
		<h3 class="text-center">DATOS DE LA COTIZACIÓN</h3>


	    <p>Buen día, gusto en saludarle, <b><?php echo $cliente['clien_rsoci']; ?></b>, poseedor del <b> RIF: <?php echo $cliente['clien_rifco']; ?></b>, a continuación le informamos el monto del equipo vigente al día de hoy <b><?php echo $this->funciones->transformarFecha_v($cotizacion['cotiz_fcrea']); ?></b> para que realice la transferencia o depósito en la cuenta siguiente:</p>	    
	    <table class="items" width="100%" style="border-collapse: collapse;" cellpadding="5">
	    	<thead>
	    		<tr>
	    			<th width="20%">BANCO</th>
					<th width="20%">TIPO DE CUENTA</th>
					<th width="30%">NUMERO</th>
					<th width="15%">TITULAR</th>
					<th width="15%">RIF</th>
	    		</tr>
	    	</thead>
	    	<tbody>
	    	<?php

	    		$sql="SELECT * 
		    		  FROM p_banco_cuenta a 
		    		  JOIN p_banco b ON (a.banco_codig = b.banco_codig)
		    		  JOIN p_banco_tipo_cuenta c ON (a.tcuen_codig = c.tcuen_codig)
		    		  WHERE a.banco_codig = '".$solicitud['banco_codig']."'";
		    	$banco = $conexion->createCommand($sql)->queryAll();
		    	if($banco){

		    	}else{
		    		$sql="SELECT * 
		    		  FROM p_banco_cuenta a 
		    		  JOIN p_banco b ON (a.banco_codig = b.banco_codig)
		    		  JOIN p_banco_tipo_cuenta c ON (a.tcuen_codig = c.tcuen_codig)";
		    		$banco = $conexion->createCommand($sql)->queryAll();	
		    	}
		    	

		    	foreach ($banco as $key => $value) {
		    		?>
		    		<tr>
		    			<td><?php echo $value['banco_descr']; ?></td>
		    			<td><?php echo $value['tcuen_descr']; ?></td>
		    			<td><?php echo $value['bcuen_ncuen']; ?></td>
		    			<td><?php echo $value['bcuen_titul']; ?></td>
		    			<td><?php echo $value['bcuen_rifco']; ?></td>
		    		</tr>
		    		<?php
		    	}
		    ?>
			</tbody>
		</table>
		<p><b>RAPIDPAGO ES CONTRIBUYENTE ESPECIAL:</b> Si su empresa es agente de retención efectúe el pago completo (posteriormente se reintegrará el monto de la retención del impuesto).</p>
		<p><b>PRECIO UNITARIO POR EQUIPO:</b> <?php echo $this->funciones->transformarMonto_v($cotizacion['cotiz_tcamb'],2);?> Bs.</p>
		
		<p>Le invitamos a realizar el pago el día de hoy o mañana en horas de la mañana, de lo contrario, deberá pagar la diferencia del día si hubiese algún cambio en el precio. Motivado a políticas internas se agradece <b>NO EFECTUAR EL PAGO EN MÁS DE CINCO (5) TRANSACCIONES.</b></p>
		<p><b>ENVÍE EL SOPORTE DE PAGO AL CORREO:</b> ventas@rapidpago.com</p>
		<p><b>Indicar en el correo nombre de la empresa y RIF</b></p>
		<p><b>EL COMPROBANTE DEBE POSEER:</b></p>
		<ul>
			<li>Monto de la transacción</li>
			<li>Número de cuenta</li>
			<li>Banco origen</li>
			<li>Banco destino</li>
			<li>Fecha de pago</li>
			<li>Número de comprobante o referencia</li>
			<li>Si es depósito debe poseer número de cheque / monto / fecha / número de referencia</li>
		</ul>
		<p><b>IMPORTANTE:</b> No enviar comprobantes de aplicaciones móviles ya que la mayoría de ellos no reflejan los datos que requerimos para validar el pago.</p>

		<p><b>EL ENVÍO DE COMPROBANTES DE PAGO LEGIBLES PERMITIRÁ AGILIZAR SU VALIDACIÓN (evitar imágenes demasiado oscuras o brillantes, borrosas, manchadas, cortadas, etc.)</b></p>

		<p>También se exhorta a efectuar la transacción desde el mismo banco indicado a efectos de inmediatez en la operación, en caso de pagar desde otros bancos (Provincial, Mercantil, etc.) procure enviarnos el soporte de pago con todos los datos de la transferencia para poder verificarlo.</p>

		<p>Quedamos a la espera de su notificación de pago, al recibirla se verificará y una vez confirmada procederemos a configurar, probar y activar el equipo para posteriormente coordinar la entrega en los próximos 45 días hábiles.</p>

    </body>
 </html>
 <?php  ?>