<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

		<style>

			body {
				font-family: ipamp;
		 		font-size: 10pt;
		 	}
		 	p { 
		 		margin: 0pt;
		 	}
		 	td { 
		 		vertical-align: middle; 
		 	}
		 	.items td {
		 		/*border-left: 0.1mm solid #000000;
		 		border-right: 0.1mm solid #000000;*/
		 		border: 0.1mm solid #000000;
		 		font-size: 10pt;
		 		text-align: left;
		 		padding: 2pt;
		 	}
		 	.items td img {
		 		max-height: 300px;
		 		max-width: 300px
		 		width:100%;
		 		height: auto;
		 	}
		 	table thead td, table thead th, table tfoot td, table tfoot th { 
		 		background-color: #EEEEEE;
		 		text-align: center;
		 		border: 0.1mm solid #000000;
		 		font-size: 10pt;
		 		padding: 2pt;
		 	}
		 	.items td.blanktotal {
		 		background-color: #FFFFFF;
		 		border: 0mm none #000000;
		 		border-top: 0.1mm solid #000000;
		 	}
		 	.items td.totals {
		 		text-align: right;
		 		border: 0.1mm solid #000000;
		 	}
		 	.text-center{
		 		text-align: center;
		 	}
		 	.text-left{
		 		text-align: left;
		 	}
		 	.text-right{
		 		text-align: right;
		 	}
		 	.img-responsive{
		 		width: 100%;
		 		height: auto;
		 		max-width: 50px;
		 	}
		</style>
	</head>
	<body>
		<?php
			$conexion=yii::app()->db;

			$sql = "SELECT * FROM pedido_pedidos WHERE pedid_codig = '".$c."'";
            $pedidos = $conexion->createCommand($sql)->queryRow();

            $sql = "SELECT * FROM seguridad_usuarios WHERE usuar_codig = '".$pedidos['usuar_codig']."'";
            $usuario = $conexion->createCommand($sql)->queryRow();

			$sql = "SELECT * 
					FROM colegio a 
					WHERE a.coleg_codig='".$usuario['coleg_codig']."'";
			
            $colegio = $conexion->createCommand($sql)->queryRow();
            $sql = "SELECT * 
					FROM colegio_contacto a 
					JOIN colegio b ON (a.coleg_codig = b.coleg_codig)
					JOIN p_persona c ON (a.perso_codig = c.perso_codig)
					WHERE a.usuar_codig='".$pedidos['usuar_codig']."'";

            $contacto = $conexion->createCommand($sql)->queryAll();


        ?>
        <table width="100%" style="font-size: 8pt; border-collapse: collapse;" cellpadding="5">
        	<tr>
        		<td align="left"><b>CÓDIGO DE PEDIDO:</b> <?php echo $pedidos['pedid_numer'];?></td>
        		<td align="right"><b>FECHA DE REGISTRO:</b> <?php echo $this->funciones->transformarFecha_v($pedidos['pedid_fcrea']);?></td>
        	</tr>
        </table>
		<h3 class="text-center">DATOS DEL COLEGIO</h3>
		<table class="items" width="100%" style="border-collapse: collapse;" cellpadding="5">
			<tbody>
				<tr>
					<td style="font-size: 13pt" width="25%"><b>NOMBRE: </b></td>
					<td style="font-size: 13pt"><b><?php echo $colegio['coleg_nombr']; ?></b></td>
				</tr>
				<tr>
					<td align="center" colspan="2"><b>PRESONA DE CONTACTO 1</b></td>
				</tr>
				<tr>
					<td><b>NOMBRE:</b></td>
					<td><?php echo $contacto[0]['perso_pnomb'].' '.$contacto[0]['perso_papel'] ?></td>
				</tr>
				<tr>
					<td><b>TELÉFONO:</b></td>
					<td><?php echo $contacto[0]['ccole_tofic'].' / '.$contacto[0]['ccole_tcelu'] ?></td>
				</tr>
				<tr>
					<td><b>CURSO:</b></td>
					<td><?php echo $contacto[0]['ccole_curso'] ?></td>
				</tr>
				<tr>
					<td align="center" colspan="2"><b>PERSONA DE CONTACTO 2</b></td>
				</tr>
				<tr>
					<td><b>NOMBRE:</b></td>
					<td><?php echo $contacto[1]['perso_pnomb'].' '.$contacto[1]['perso_papel'] ?></td>
				</tr>
				<tr>
					<td><b>TELÉFONO:</b></td>
					<td><?php echo $contacto[1]['ccole_tofic'].' / '.$contacto[1]['ccole_tcelu'] ?></td>
				</tr>
				<tr>
					<td><b>CURSO:</b></td>
					<td><?php echo $contacto[1]['ccole_curso'] ?></td>

				</tr>
		 	</tbody>	 
	    </table>
	    <?php $totales=$this->funciones->CalcularMontoPedido($pedidos); ?>
	    <h3 class="text-left">TOTALES</h3>
	    <table class="items" width="100%" style="font-size: 12pt; border-collapse: collapse;" cellpadding="5">
			<tbody>
				<tr>
					<td align="center"><b>MODELOS</b></td>
					<td align="center"><?php echo $this->funciones->transformarMonto_v($totales['modelos'],0)?></td>
				</tr>
				<tr>
					<td align="center"><b>PERSONAS</b></td>
					<td align="center"><?php echo $this->funciones->transformarMonto_v($totales['p_personas'],0)?></td>
				</tr>
				<tr>
					<td align="center"><b>MONTO</b></td>
					<td align="center"><?php echo $this->funciones->transformarMonto_v($totales['monto'],0)?></td>
				</tr>				
			</tbody>
	    </table>
	    <?php
	    	

            $sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_modelo_color b ON (a.pedid_ccuer = b.mcolo_codig)
            		WHERE pedid_codig = '".$c."'";
            $cuerpo = $conexion->createCommand($sql)->queryRow();

            $sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_modelo_color b ON (a.pedid_cbols = b.mcolo_codig)
            		WHERE pedid_codig = '".$c."'";
            $bolsillos = $conexion->createCommand($sql)->queryRow();
            $sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_modelo_color b ON (a.pedid_ccier = b.mcolo_codig)
            		WHERE pedid_codig = '".$c."'";
            $cierre = $conexion->createCommand($sql)->queryRow();
            $sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_modelo_color b ON (a.pedid_cmang = b.mcolo_codig)
            		WHERE pedid_codig = '".$c."'";
            $mangas = $conexion->createCommand($sql)->queryRow();

            $sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_modelo_color b ON (a.pedid_cegor = b.mcolo_codig)
            		WHERE pedid_codig = '".$c."'";
            $egorro = $conexion->createCommand($sql)->queryRow();

            $sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_modelo_color b ON (a.pedid_cigor = b.mcolo_codig)
            		WHERE pedid_codig = '".$c."'";
            $igorro = $conexion->createCommand($sql)->queryRow();

            $sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_modelo_color b ON (a.pedid_cppre = b.mcolo_codig)
            		WHERE pedid_codig = '".$c."'";
            $pretina = $conexion->createCommand($sql)->queryRow();

            $sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_modelo_color b ON (a.pedid_cpprl = b.mcolo_codig)
            		WHERE pedid_codig = '".$c."'";
            $plinea = $conexion->createCommand($sql)->queryRow();
            //Color de Letras
            $sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_modelo_color b ON (a.pedid_ccurs = b.mcolo_codig)
            		WHERE pedid_codig = '".$c."'";
            $curso = $conexion->createCommand($sql)->queryRow();

            $sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_modelo_color b ON (a.pedid_cnper = b.mcolo_codig)
            		WHERE pedid_codig = '".$c."'";
            $npers = $conexion->createCommand($sql)->queryRow();

            $sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_modelo_color b ON (a.pedid_caesp = b.mcolo_codig)
            		WHERE pedid_codig = '".$c."'";
            $aespa = $conexion->createCommand($sql)->queryRow();

            $sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_modelo_color b ON (a.pedid_celec = b.mcolo_codig)
            		WHERE pedid_codig = '".$c."'";
            $elect = $conexion->createCommand($sql)->queryRow();

            $sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_modelo_color b ON (a.pedid_cfras = b.mcolo_codig)
            		WHERE pedid_codig = '".$c."'";
            $frase = $conexion->createCommand($sql)->queryRow();

            $sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_modelo_color b ON (a.pedid_cvivo = b.mcolo_codig)
            		WHERE pedid_codig = '".$c."'";
            $vivo = $conexion->createCommand($sql)->queryRow();
            $broche=array('1'=>'SIN BROCHE','2'=>'CRUDO','3'=>'GRIS','4'=>'BLANCO');

            $a=0;
            $tabla['dpedi'][$a]['descr']='COLOR DEL CUERPO';
            $tabla['dpedi'][$a]['value']=$cuerpo['mcolo_numer'].' - '.$cuerpo['mcolo_descr'];
            $a++;
            $tabla['dpedi'][$a]['descr']='COLOR DE BOLSILLOS';
            $tabla['dpedi'][$a]['value']=$bolsillos['mcolo_numer'].' - '.$bolsillos['mcolo_descr'];
            $a++;
            $tabla['dpedi'][$a]['descr']='COLOR DE LAS MANGAS';
            $tabla['dpedi'][$a]['value']=$mangas['mcolo_numer'].' - '.$mangas['mcolo_descr'];
            $a++;
            $tabla['dpedi'][$a]['descr']='COLOR DEL CIERRE';
            $tabla['dpedi'][$a]['value']=$cierre['mcolo_numer'].' - '.$cierre['mcolo_descr'];
            $a++;
            $tabla['dpedi'][$a]['descr']='COLOR INTERIOR DEL GORRO';
            $tabla['dpedi'][$a]['value']=$igorro['mcolo_numer'].' - '.$igorro['mcolo_descr'];
            $a++;
            $tabla['dpedi'][$a]['descr']='COLOR EXTERIOR DEL GORRO';
            $tabla['dpedi'][$a]['value']=$egorro['mcolo_numer'].' - '.$egorro['mcolo_descr'];
            $a++;
            $tabla['dpedi'][$a]['descr']='COLOR DE FONDO PUÑO Y PRETINAS';
            $tabla['dpedi'][$a]['value']=$pretina['mcolo_numer'].' - '.$pretina['mcolo_descr'];
            $a++;
            $tabla['dpedi'][$a]['descr']='COLOR DE LINEAS PUÑO Y PRETINAS';
            $tabla['dpedi'][$a]['value']=$plinea['mcolo_numer'].' - '.$plinea['mcolo_descr'];
            $a++;
            $tabla['dpedi'][$a]['descr']='BROCHES';
            $tabla['dpedi'][$a]['value']=$broche[$pedidos['pedid_broch']];
            $a++;
            $tabla['dpedi'][$a]['descr']='COLOR DEL VIVO EN EL BOLSILLO';
            $tabla['dpedi'][$a]['value']=$vivo['mcolo_numer'].' - '.$vivo['mcolo_descr'];

            $a=0;
            foreach ($tabla['dpedi'] as $key => $value) {
            	
            	if($value['value']!=null and $value['value'] != ' - '){
            		
            		$imprimir[$a]['descr']=$value['descr'];
            		$imprimir[$a]['value']=$value['value'];
            	}
            	$a++;
            }
			




        ?>
         <h3 class="text-center">DATOS DEL PEDIDO</h3>
	    <table class="items" width="100%" style="font-size: 12pt; border-collapse: collapse;" cellpadding="5">
			<tbody>
				<?php
					$a=0;
					$count=count($imprimir);
					foreach ($imprimir as $key => $value) {
						$a++;
						if($a%2>0){
		            		echo '<tr>';	
		            	}
						echo "<td width='25%'><b>".$value['descr']."</b></td>";
						
						if($a%2==0 and $a==$count){
							echo '<td width="25%">'.$value['value'].'</td>';
						}elseif($a==$count){
		            		echo '<td colspan=3>'.$value['value'].'</td>';	
		            	}else{
		            		echo '<td>'.$value['value'].'</td>';	
		            	}
						
		            	
		            	if($a%2==0 or $a==$count){
		            		echo '</tr>';	
		            	}
		            	
		            }
				?>
			</tbody>
		</table>
		<?php
		/*
	    <h3 class="text-center">DATOS DEL PEDIDO</h3>
	    <table class="items" width="100%" style="font-size: 12pt; border-collapse: collapse;" cellpadding="5">
			<tbody>
				<tr>
					<td width="25%"><b>COLOR DEL CUERPO: </b></td>
					<td width="25%"><?php echo $cuerpo['mcolo_numer'].' - '.$cuerpo['mcolo_descr']; ?></td>
				
					<td width="25%"><b>COLOR DE BOLSILLOS: </b></td>
					<td width="25%"><?php echo $bolsillos['mcolo_numer'].' - '.$bolsillos['mcolo_descr']; ?></td>
				</tr>
				<tr>
					<td width="25%"><b>COLOR DE LAS MANGAS: </b></td>
					<td width="25%"><?php echo $mangas['mcolo_numer'].' - '.$mangas['mcolo_descr']; ?></td>
					
					<td width="25%"><b>COLOR DEL CIERRE: </b></td>
					<td width="25%"><?php echo $cierre['mcolo_numer'].' - '.$cierre['mcolo_descr']; ?></td>	
				</tr>
				<tr>
					<td width="25%"><b>COLOR INTERIOR DEL GORRO: </b></td>
					<td width="25%"><?php echo $igorro['mcolo_numer'].' - '.$igorro['mcolo_descr']; ?></td>

					<td width="25%"><b>COLOR EXTERIOR DEL GORRO: </b></td>
					<td width="25%"><?php echo $egorro['mcolo_numer'].' - '.$egorro['mcolo_descr']; ?></td>
							
				</tr>
				<tr>
					<td width="25%"><b>COLOR DE FONDO PUÑO Y PRETINAS: </b></td>
					<td width="25%"><?php echo $pretina['mcolo_numer'].' - '.$pretina['mcolo_descr']; ?></td>
					<td width="25%"><b>COLOR DE LINEAS PUÑO Y PRETINAS: </b></td>
					<td width="25%"><?php echo $plinea['mcolo_numer'].' - '.$plinea['mcolo_descr']; ?></td>
				
				</tr>
				<tr>
					<td width="25%"><b>BROCHES: </b></td>
					<td width="25%" colspan="3"><?php echo $broche[$pedidos['pedid_broch']]; ?></td>
				</tr>
				<!--tr>
					<td align="center" colspan="4"><b>COLOR DE LETRAS</b></td>
				</tr>
				<tr>
					<td width="25%"><b>CURSO: </b></td>
					<td width="25%"><?php echo $curso['mcolo_numer'].' - '.$curso['mcolo_descr']; ?></td>
				
					<td width="25%"><b>NOMBRE PERSONALIZADO: </b></td>
					<td width="25%"><?php echo $npers['mcolo_numer'].' - '.$npers['mcolo_descr']; ?></td>
				</tr>
				<tr>
					<td width="25%"><b>APODO ESPALDA: </b></td>
					<td width="25%"><?php echo $aespa['mcolo_numer'].' - '.$aespa['mcolo_descr']; ?></td>
				
					<td width="25%"><b>ELECTIVA: </b></td>
					<td width="25%"><?php echo $elect['mcolo_numer'].' - '.$elect['mcolo_descr']; ?></td>
				</tr>
				<tr>
					<td width="25%"><b>FRASE: </b></td>
					<td width="25%"><?php echo $frase['mcolo_numer'].' - '.$frase['mcolo_descr']; ?></td>
				
					<td width="25%"><b>OTROS: </b></td>
					<td width="25%"><?php echo $pedidos['pedid_otros']; ?></td>
				</tr-->
			</tbody>
	    </table>
	    <?php
	    	*/
	    	$sql="SELECT * FROM pedido_pedidos_detalle WHERE pedid_codig = '".$pedidos['pedid_codig']."'";
			$modelos = $conexion->createCommand($sql)->queryAll();	 	
			$i=0;
			$j=0;	
			$totales['modelos']=0;
			$totales['p_personas']=0;
			$totales['monto']=0;
			foreach ($modelos as $key => $modelo) {
				$i++;
				$totales['modelos']++;
				//Color de Letras
	            $sql = "SELECT * FROM pedido_pedidos_detalle a
	            		JOIN pedido_modelo_tipo b ON (a.tmode_codig = b.tmode_codig)
	            		WHERE pdeta_codig = '".$modelo['pdeta_codig']."'";
	            $tmode = $conexion->createCommand($sql)->queryRow();
	            $sql = "SELECT * FROM pedido_pedidos_detalle a
	            		JOIN pedido_modelo b ON (a.model_codig = b.model_codig)
	            		WHERE pdeta_codig = '".$modelo['pdeta_codig']."'";
	            $model = $conexion->createCommand($sql)->queryRow();
	            $sql = "SELECT * FROM pedido_pedidos_detalle a
	            		JOIN pedido_modelo_categoria b ON (a.mcate_codig = b.mcate_codig)
	            		WHERE pdeta_codig = '".$modelo['pdeta_codig']."'";
	            $mcate = $conexion->createCommand($sql)->queryRow();
	            $sql = "SELECT * FROM pedido_pedidos_detalle a
	            		JOIN pedido_modelo_variante b ON (a.mvari_codig = b.mvari_codig)
	            		WHERE pdeta_codig = '".$modelo['pdeta_codig']."'";
	            $mvari = $conexion->createCommand($sql)->queryRow();
	            $sql = "SELECT * FROM pedido_pedidos_detalle a
	            		JOIN pedido_modelo_opcional b ON (a.mopci_codig = b.mopci_codig)
	            		WHERE pdeta_codig = '".$modelo['pdeta_codig']."'";
	            $mopci = $conexion->createCommand($sql)->queryRow();
	            $sql = "SELECT * FROM pedido_pedidos_detalle a
	            		JOIN pedido_modelo_opcional b ON (a.mopci_codi2 = b.mopci_codig)
	            		WHERE pdeta_codig = '".$modelo['pdeta_codig']."'";
	            $mopc2 = $conexion->createCommand($sql)->queryRow();
	            $sql = "SELECT * FROM pedido_pedidos_detalle a
	            		JOIN pedido_modelo_color b ON (a.pdeta_copci = b.mcolo_codig)
	            		WHERE pdeta_codig = '".$modelo['pdeta_codig']."'";
	            $copci = $conexion->createCommand($sql)->queryRow();

	            $sql = "SELECT * FROM pedido_modelo_conjunto
	            		WHERE tmode_codig = '".$modelo['tmode_codig']."'
	            		  AND model_codig = '".$modelo['model_codig']."'
	            		  AND mcate_codig = '".$modelo['mcate_codig']."'";
	            $conjunto = $conexion->createCommand($sql)->queryRow();

	            $a=0;
	            $tabla=array();
	            $tabla[$a]['descr']='TIPO';
	            $tabla[$a]['value']=$tmode['tmode_descr'];
	            $a++;
	            $tabla[$a]['descr']='MODELO';
	            $tabla[$a]['value']=$model['model_descr'];
	            $a++;
	            $tabla[$a]['descr']='CATEGORIA';
	            $tabla[$a]['value']=$mcate['mcate_descr'];
	            $a++;
	            $tabla[$a]['descr']='ADICIONAL';
	            $tabla[$a]['value']=$mopci['mopci_descr'];
	            $a++;
	            $tabla[$a]['descr']='SEGUNDO ADICIONAL';
	            $tabla[$a]['value']=$mopc2['mopci_descr'];
	            $a++;
	            $tabla[$a]['descr']='CANTIDAD A SOLICICTAR';
	            $tabla[$a]['value']=$modelo['pdeta_canti'];
	            $a++;
	            $tabla[$a]['descr']='MENSAJE';
	            $tabla[$a]['value']=$modelo['pdeta_mensa'];

	            $a=0;
	            $imprimir=array();
	            foreach ($tabla as $key => $value) {
	            	
	            	if($value['value']!=null and $value['value'] != ' - '){
	            		
	            		$imprimir[$a]['descr']=$value['descr'];
	            		$imprimir[$a]['value']=$value['value'];
	            	}
	            	$a++;
	            }
				?>
				<h3 class="text-center">DATOS DEL MODELO <?php echo $i; ?></h3>
				<table class="items" width="100%" style="font-size: 12pt; border-collapse: collapse;" cellpadding="5">
					<tbody>
						<?php
					$a=0;
					$count=count($imprimir);
					foreach ($imprimir as $key => $value) {
						$a++;
						if($a%2>0){
		            		echo '<tr>';	
		            	}
						echo "<td width='25%'><b>".$value['descr']."</b></td>";
						
		            	if($a==$count){
		            		echo '<td colspan=3>'.$value['value'].'</td>';	
		            	}else{
		            		echo '<td>'.$value['value'].'</td>';	
		            	}
		            	
		            	if($a%2==0 or $a==$count){
		            		echo '</tr>';	
		            	}
		            	
		            }
				?>
					</tbody>
	    		</table>
				<h3 class="text-center">PERSONAS ASOCIADAS AL MODELO <?php echo $i; ?></h3>
				<table width="741px" class="items"  style="overflow: wrap; border-collapse: collapse;" cellpadding="5">
					<thead>
						<tr>
							<th style="font-size: 8pt;" width="20px"><b>N° </b></th>
							<th style="font-size: 8pt;" width="60px"><b>NOMBRE </b></th>
							<th style="font-size: 8pt;" width="120px"><b>NOMBRE PERSONALIZADO </b></th>
							<th style="font-size: 8pt;" width="80px"><b>APODO ESPALDA </b></th>
							<th style="font-size: 8pt;" width="40px"><b>TALLA </b></th>
							<th style="font-size: 8pt;" width="50px"><b>AJUSTE DE TALLA </b></th>
							<th style="font-size: 8pt;" width="60px"><b>ELECTIVO </b></th>
							<th style="font-size: 8pt;" width="60px"><b>OBSERVACIONES </b></th>
							<th style="font-size: 8pt;" width="60px"><b>PALABRA O FRASE </b></th>
							<th style="font-size: 8pt;" width="60px"><b>IMAGENES </b></th>
							<th style="font-size: 8pt;" width="70px"><b>MONTO DETALLADO </b></th>
							<th style="font-size: 8pt;" width="60px"><b>MONTO GLOBAL </b></th>
						</tr>
					</thead>
					<tbody>
	    		<?php
					$sql="SELECT * FROM pedido_pedidos_imagen WHERE pedid_codig='".$pedidos['pedid_codig']."' and pimag_orden >=10";
					$elect=$conexion->createCommand($sql)->queryRow();
					if($elect){
						$electivo=1;
					}else{
						$electivo=0;
					}

					$sql="SELECT * FROM pedido_pedidos_detalle_listado WHERE pdeta_codig='".$modelo['pdeta_codig']."'";
					$p_personas = $conexion->createCommand($sql)->queryAll();

					
					$total=0; 


					foreach ($p_personas as $key => $p_persona) {
						$monto=0;
						$opcional=0;
						$opcional2=0;
						$montoPersona=0;
						$pmonto=0;
						$j++;
						$totales['p_personas']++;

						$total+=$conjunto['mconj_preci'];
						$total+=$mopci['mopci_preci'];
						$total+=$mopc2['mopci_preci'];

						$montoPersona=$conjunto['mconj_preci']+$mopci['mopci_preci']+$mopc2['mopci_preci'];

						$sql="SELECT * FROM pedido_detalle_listado_imagen WHERE pdlis_codig='".$p_persona['pdlis_codig']."'";
						$imagenes = $conexion->createCommand($sql)->queryAll();
						$e=0;
						$a=0;
						$t=0;
						$adici=0;
						$adicional='';
						$adicio=0;
						$tipo=array();
						$texto='';
						$electivos='';
						foreach ($imagenes as $key => $imagen) {

							$a++;
							$sql="SELECT * FROM pedido_precio WHERE pprec_codig = '".$imagen['pdlim_timag']."'";
							

							$precio=$conexion->createCommand($sql)->queryRow();
							$sql="SELECT * FROM pedido_talla WHERE talla_codig = '".$p_persona['talla_codig']."'";
							

							$talla=$conexion->createCommand($sql)->queryRow();
							$sql="SELECT * FROM pedido_precio_electivo WHERE pelec_codig='1'";
							

							$pelect=$conexion->createCommand($sql)->queryRow();
							//var_dump($precio);
							$tipo[$imagen['pdlim_timag']]++;
							if ($tipo[$imagen['pdlim_timag']]>1) {
								$total+=$precio['pprec_adici'];
								$montoPersona+=$precio['pprec_adici'];
								$adici=$precio['pprec_adici'];
							}else{
								$total+=$precio['pprec_preci'];
								$montoPersona+=$precio['pprec_preci'];	
								$adici=$precio['pprec_preci'];
							}
							
							if($a>1){
								$adicional.='<br>'.' +';
							}
							$adicio+=$adici;
							if($adicio>0){
								$adicional.=$this->funciones->transformarMonto_v($adici,0);
							}
							if($t>1){
								$texto.=' / ';
							}
							if ($imagen['pdlim_timag']==5) {
								$texto.=$imagen['pdlim_texto'];
								$t++;
							}

							
							
						}
						if($electivo=='1'){
							$e++;
							$electivos.=$this->funciones->transformarMonto_v($pelect['pelec_preci'],0);
							$montoPersona+=$pelect['pelec_preci'];
							$total+=$pelect['pelec_preci'];
						}
						
						$monto=$this->funciones->transformarMonto_v($conjunto['mconj_preci'],0);
	            		$opcional=$this->funciones->transformarMonto_v($mopci['mopci_preci'],0);
	            		$opcional2=$this->funciones->transformarMonto_v($mopc2['mopci_preci'],0);
						$montoPersona=$this->funciones->transformarMonto_v($montoPersona,0);

						$pmonto=$monto;
						if($mopci['mopci_preci']>0){
							$pmonto.='<br>'.' +'.$opcional;
						}
						if($mopc2['mopci_preci']>0){
							$pmonto.='<br>'.' +'.$opcional2;
						}
						if($adicio>0){
							$pmonto.='<br>'.' +'.$adicional;
						}
						if($electivo=='1'){
							$pmonto.='<br>'.' +'.$electivos;
						}
						?>		
							<tr>
								<td style="font-size: 8pt;"><?php echo $j; ?></td>
								<td style="font-size: 8pt;"><?php echo $p_persona['pdlis_nombr']; ?></td>
								<td style="font-size: 8pt;">&nbsp;<?php echo $p_persona['pdlis_npers']; ?>&nbsp;</td>
								<td style="font-size: 8pt;"><?php echo $p_persona['pdlis_aespa']; ?></td>
								<td style="font-size: 8pt;"><?php echo $talla['talla_descr']; ?></td>
								<td style="font-size: 8pt;"><?php echo $this->funciones->VerAjusteTalla(json_decode($p_persona['pdlis_tajus'])); ?></td>
								<td style="font-size: 8pt;"><?php echo $p_persona['pdlis_elect']; ?></td>
								<td style="font-size: 8pt;"><?php echo $p_persona['pdlis_obser']; ?></td>
								<td style="font-size: 8pt;"align="right" class="text-right"><?php echo $texto ?></td>
								<td style="font-size: 8pt;" class="text-right"><?php echo $this->funciones->VerImagenPersona($p_persona['pdlis_codig']); ?></td>
								

								<td style="font-size: 8pt;" align="right" class="text-right"><?php echo $pmonto ?></td>
								<td style="font-size: 8pt;" align="right" class="text-right"><?php echo $montoPersona ?></td>
							</tr>
			    		<?php
					}
					$totales['monto']+=$total;
					$totalm+=$total;
				?>
				</tbody>
				<tfoot>
					<tr>
						<th style="font-size: 8pt;" colspan="10">TOTAL </th>
						<th style="font-size: 8pt;" ><?php echo $this->funciones->transformarMonto_v($total,0); ?></th>
						<th style="font-size: 8pt;" ><?php echo $this->funciones->transformarMonto_v($total,0); ?></th>
					</tr>
				</tfoot>
			</table>
				<?php
			}
	    ?>

	    <?php
	    	
            $sql = "SELECT max(pimag_orden) pimag_orden FROM pedido_pedidos a
            		JOIN pedido_pedidos_imagen b ON (a.pedid_codig = b.pedid_codig)
            		WHERE a.pedid_codig = '".$c."'";
            $orden = $conexion->createCommand($sql)->queryRow();
            for($i=1;$i<=$orden['pimag_orden'];$i++){
            	$sql = "SELECT b.* FROM pedido_pedidos a
            		JOIN pedido_pedidos_imagen b ON (a.pedid_codig = b.pedid_codig)
            		WHERE a.pedid_codig = '".$c."'
            		AND pimag_orden = '".$i."'";
            	$pimagenes = $conexion->createCommand($sql)->queryRow();
            	$pimagen[$i]=$pimagenes;
            }

            $posicion =  array('1'=>'DERECHO', '2'=>'IZQUIERDO', '3'=>'AMBOS', '4'=>'HORIZONTAL', '5'=>'VERTICAL');
            
            $informacion =  array('1'=>'IMAGEN', '2'=>'TEXTO', '3'=>'AMBOS');

            $sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_fuente b ON (a.pedid_pfuen = b.fuent_codig)
            		WHERE pedid_codig = '".$c."'";
            $pfuen = $conexion->createCommand($sql)->queryRow();

            $sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_modelo_color b ON (a.pedid_pcolo = b.mcolo_codig)
            		WHERE pedid_codig = '".$c."'";
            $pcolo = $conexion->createCommand($sql)->queryRow();
            $sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_pedidos_imagen_tipo b ON (a.pedid_ptide = b.ptima_codig)
            		WHERE pedid_codig = '".$c."'";
            $ptide = $conexion->createCommand($sql)->queryRow();
            $sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_pedidos_imagen_tipo b ON (a.pedid_ptiiz = b.ptima_codig)
            		WHERE pedid_codig = '".$c."'";
            $ptiiz = $conexion->createCommand($sql)->queryRow();
            
            
            $a=0;
            $tabla=array();
            $tabla[$a]['descr']='FUENTE';
            $tabla[$a]['value']=$pfuen['fuent_descr'];
            $a++;
            $tabla[$a]['descr']='COLOR';
            $tabla[$a]['value']=$pcolo['mcolo_numer'].' - '.$pcolo['mcolo_descr'];
            $a++;
            $tabla[$a]['descr']='POSICIÓN';
            $tabla[$a]['value']=$posicion[$pedidos['pedid_pposi']];
            $a++;
            $tabla[$a]['descr']='INFORMACIÓN A BORDAR DERECHA';
            $tabla[$a]['value']=$informacion[$pedidos['pedid_pibde']];
            $a++;
            $tabla[$a]['descr']='INFORMACIÓN A BORDAR IZQUIERDA';
            $tabla[$a]['value']=$informacion[$pedidos['pedid_pibiz']];
            $a++;
            $tabla[$a]['descr']='TEXTO A BORDAR DERECHA';
            $tabla[$a]['value']=$pfuen['pedid_ptbde'];
            $a++;
            $tabla[$a]['descr']='TEXTO A BORDAR IZQUIERDA';
            $tabla[$a]['value']=$pedidos['pedid_ptbiz'];
            $a++;
            $tabla[$a]['descr']='TIPO DE IMAGEN DERECHA';
            $tabla[$a]['value']=$ptide['ptima_descr'];
            $a++;
            $tabla[$a]['descr']='TIPO DE IMAGEN IZQUIERDA';
            $tabla[$a]['value']=$ptiiz['ptima_descr'];

            $a=0;
            $imprimir=array();
            foreach ($tabla as $key => $value) {
            	
            	if($value['value']!=null and $value['value'] != ' - '){
            		
            		$imprimir[$a]['descr']=$value['descr'];
            		$imprimir[$a]['value']=$value['value'];
            	}
            	$a++;
            }
        ?>
	    <h3 class="text-center">INFORMACIÓN ADICIONAL A BORDAR </h3>
	    <?php
			$tabla2=array();
            $tabla2[$a]['descr']='IMAGEN A BORDAR DERECHA';
            if($pedidos['pedid_ptide']=='5'){
            	$tabla2[$a]['value']='<img src="'.Yii::app()->getBaseUrl(true).'/'.$pimagen[1]['pimag_ruta'].'" class="img-responsive" >';
            }else if($pedidos['pedid_ptide']=='6'){
            	$tabla2[$a]['value']=$this->funciones->VerElectivos($pedidos);
            }
            
            $a++;
            $tabla2[$a]['descr']='IMAGEN A BORDAR IZQUIERDA';
            if($pedidos['pedid_ptiiz']=='5'){
				$tabla2[$a]['value']='<img src="'.Yii::app()->getBaseUrl(true).'/'.$pimagen[2]['pimag_ruta'].'" class="img-responsive" >';
			}else if($pedidos['pedid_ptiiz']=='6'){
				$tabla2[$a]['value']=$this->funciones->VerElectivos($pedidos);
			}
            $a++;
            $tabla2[$a]['descr']='OBSERVACIONES';
            $tabla2[$a]['value']=$pedidos['pedid_pobse'];

            $imprimir2=array();
            foreach ($tabla2 as $key => $value) {
            	
            	if($value['value']!=null and $value['value'] != ' - '){
            		
            		$imprimir2[$a]['descr']=$value['descr'];
            		$imprimir2[$a]['value']=$value['value'];
            	}
            	$a++;
            }
            $contador1=count($imprimir);
            $contador2=count($imprimir2);
            if ($contador1>0 or $contador2>0) {
	    ?>

	    <h3 class="text-left">PECHO </h3>
	    <table class="items" width="100%" style="font-size: 12pt; border-collapse: collapse;" cellpadding="5">
			<tbody>
				<?php
					$a=0;
					$count=count($imprimir);
					foreach ($imprimir as $key => $value) {
						$a++;
						if($a%2>0){
		            		echo '<tr>';	
		            	}
						echo "<td width='25%'><b>".$value['descr']."</b></td>";
						
						if($a%2==0 or $a==$count){
							echo '<td width="25%">'.$value['value'].'</td>';	
						}elseif($a==$count){
		            		echo '<td colspan=3>'.$value['value'].'</td>';	
		            	}else{
		            		echo '<td width="25%">'.$value['value'].'</td>';	
		            	}
		            	
		            	if($a%2==0 or $a==$count){
		            		echo '</tr>';	
		            	}
		            	
		            }
		            $a=0;
					$count=count($imprimir2);
					foreach ($imprimir2 as $key => $value) {
						$a++;
						if($a%2>0){
		            		echo '<tr>';	
		            	}
						echo "<td width='25%'><b>".$value['descr']."</b></td>";
						
		            	if($a==$count){
		            		echo '<td colspan=3>'.$value['value'].'</td>';	
		            	}else{
		            		echo '<td width="25%">'.$value['value'].'</td>';	
		            	}
		            	
		            	if($a%2==0 or $a==$count){
		            		echo '</tr>';	
		            	}
		            	
		            }
				?>
			</tbody>
	    </table>
	    <?php
	     }
            $sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_fuente b ON (a.pedid_gfuen = b.fuent_codig)
            		WHERE pedid_codig = '".$c."'";
            $gfuen = $conexion->createCommand($sql)->queryRow();

            $sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_modelo_color b ON (a.pedid_gcolo = b.mcolo_codig)
            		WHERE pedid_codig = '".$c."'";
            $gcolo = $conexion->createCommand($sql)->queryRow();
            $sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_pedidos_imagen_tipo b ON (a.pedid_gtide = b.ptima_codig)
            		WHERE pedid_codig = '".$c."'";
            $gtide = $conexion->createCommand($sql)->queryRow();
            $sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_pedidos_imagen_tipo b ON (a.pedid_gtiiz = b.ptima_codig)
            		WHERE pedid_codig = '".$c."'";
            $gtiiz = $conexion->createCommand($sql)->queryRow();
            
            $a=0;
            $tabla=array();
            $tabla[$a]['descr']='FUENTE';
            $tabla[$a]['value']=$gfuen['fuent_descr'];
            $a++;
            $tabla[$a]['descr']='COLOR';
            $tabla[$a]['value']=$gcolo['mcolo_numer'].' - '.$gcolo['mcolo_descr'];
            $a++;
            $tabla[$a]['descr']='POSICIÓN';
            $tabla[$a]['value']=$posicion[$pedidos['pedid_gposi']];
            $a++;
            $tabla[$a]['descr']='INFORMACIÓN A BORDAR DERECHA';
            $tabla[$a]['value']=$informacion[$pedidos['pedid_gibde']];
            $a++;
            $tabla[$a]['descr']='INFORMACIÓN A BORDAR IZQUIERDA';
            $tabla[$a]['value']=$informacion[$pedidos['pedid_gibiz']];
            $a++;
            $tabla[$a]['descr']='TEXTO A BORDAR DERECHA';
            $tabla[$a]['value']=$pedidos['pedid_gtbde'];
            $a++;
            $tabla[$a]['descr']='TEXTO A BORDAR IZQUIERDA';
            $tabla[$a]['value']=$pedidos['pedid_gtbiz'];
            $a++;
            $tabla[$a]['descr']='TIPO DE IMAGEN DERECHA';
            $tabla[$a]['value']=$pgtide['ptima_descr'];
            $a++;
            $tabla[$a]['descr']='TIPO DE IMAGEN IZQUIERDA';
            $tabla[$a]['value']=$gtiiz['ptima_descr'];

            $a=0;
            $imprimir=array();
            foreach ($tabla as $key => $value) {
            	
            	if($value['value']!=null and $value['value'] != ' - '){
            		
            		$imprimir[$a]['descr']=$value['descr'];
            		$imprimir[$a]['value']=$value['value'];
            	}
            	$a++;
            }

            $tabla2=array();
            $tabla2[$a]['descr']='IMAGEN A BORDAR DERECHA';
            if($pedidos['pedid_gtide']=='5'){
            	$tabla2[$a]['value']='<img src="'.Yii::app()->getBaseUrl(true).'/'.$pimagen[3]['pimag_ruta'].'" class="img-responsive" >';
            }else if($pedidos['pedid_gtide']=='6'){
            	$tabla2[$a]['value']=$this->funciones->VerElectivos($pedidos);
            }
            
            $a++;
            $tabla2[$a]['descr']='IMAGEN A BORDAR IZQUIERDA';
            if($pedidos['pedid_gtiiz']=='5'){
				$tabla2[$a]['value']='<img src="'.Yii::app()->getBaseUrl(true).'/'.$pimagen[4]['pimag_ruta'].'" class="img-responsive" >';
			}else if($pedidos['pedid_gtiiz']=='6'){
				$tabla2[$a]['value']=$this->funciones->VerElectivos($pedidos);
			}
            $a++;
            $tabla2[$a]['descr']='OBSERVACIONES';
            $tabla2[$a]['value']=$pedidos['pedid_gobse'];

            $imprimir2=array();
            foreach ($tabla2 as $key => $value) {
            	
            	if($value['value']!=null and $value['value'] != ' - '){
            		
            		$imprimir2[$a]['descr']=$value['descr'];
            		$imprimir2[$a]['value']=$value['value'];
            	}
            	$a++;
            }
            $contador1=count($imprimir);
            $contador2=count($imprimir2);
            if ($contador1>0 or $contador2>0) {
        ?>
	    <h3 class="text-left">GORRO </h3>
	    <table class="items" width="100%" style="font-size: 12pt; border-collapse: collapse;" cellpadding="5">
			<tbody>
				<?php
					$a=0;
					$count=count($imprimir);
					foreach ($imprimir as $key => $value) {
						$a++;
						if($a%2>0){
		            		echo '<tr>';	
		            	}
						echo "<td width='25%'><b>".$value['descr']."</b></td>";
						
		            	if($a==$count){
		            		echo '<td colspan=3>'.$value['value'].'</td>';	
		            	}else{
		            		echo '<td width="25%">'.$value['value'].'</td>';	
		            	}
		            	
		            	if($a%2==0 or $a==$count){
		            		echo '</tr>';	
		            	}
		            	
		            }
		            $a=0;
					$count=count($imprimir2);
					foreach ($imprimir2 as $key => $value) {
						$a++;
						if($a%2>0){
		            		echo '<tr>';	
		            	}
						echo "<td width='25%'><b>".$value['descr']."</b></td>";
						
		            	if($a==$count){
		            		echo '<td colspan=3>'.$value['value'].'</td>';	
		            	}else{
		            		echo '<td width="25%">'.$value['value'].'</td>';	
		            	}
		            	
		            	if($a%2==0 or $a==$count){
		            		echo '</tr>';	
		            	}
		            	
		            }
				?>
			</tbody>
	    </table>
	    <?php
			}
            $sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_fuente b ON (a.pedid_mfuen = b.fuent_codig)
            		WHERE pedid_codig = '".$c."'";
            $mfuen = $conexion->createCommand($sql)->queryRow();

            $sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_modelo_color b ON (a.pedid_mcolo = b.mcolo_codig)
            		WHERE pedid_codig = '".$c."'";
            $mcolo = $conexion->createCommand($sql)->queryRow();
            $sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_pedidos_imagen_tipo b ON (a.pedid_mtide = b.ptima_codig)
            		WHERE pedid_codig = '".$c."'";
            $mtide = $conexion->createCommand($sql)->queryRow();
            $sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_pedidos_imagen_tipo b ON (a.pedid_mtiiz = b.ptima_codig)
            		WHERE pedid_codig = '".$c."'";
            $mtiiz = $conexion->createCommand($sql)->queryRow();

            $a=0;
            $tabla=array();
            $tabla[$a]['descr']='FUENTE';
            $tabla[$a]['value']=$mfuen['fuent_descr'];
            $a++;
            $tabla[$a]['descr']='COLOR';
            $tabla[$a]['value']=$mcolo['mcolo_numer'].' - '.$mcolo['mcolo_descr'];
            $a++;
            $tabla[$a]['descr']='POSICIÓN';
            $tabla[$a]['value']=$posicion[$pedidos['pedid_mposi']];
            $a++;
            $tabla[$a]['descr']='INFORMACIÓN A BORDAR DERECHA';
            $tabla[$a]['value']=$informacion[$pedidos['pedid_mibde']];
            $a++;
            $tabla[$a]['descr']='INFORMACIÓN A BORDAR IZQUIERDA';
            $tabla[$a]['value']=$informacion[$pedidos['pedid_mibiz']];
            $a++;
            $tabla[$a]['descr']='TEXTO A BORDAR DERECHA';
            $tabla[$a]['value']=$pedidos['pedid_mtbde'];
            $a++;
            $tabla[$a]['descr']='TEXTO A BORDAR IZQUIERDA';
            $tabla[$a]['value']=$pedidos['pedid_mtbiz'];
            $a++;
            $tabla[$a]['descr']='TIPO DE IMAGEN DERECHA';
            $tabla[$a]['value']=$mgtide['ptima_descr'];
            $a++;
            $tabla[$a]['descr']='TIPO DE IMAGEN IZQUIERDA';
            $tabla[$a]['value']=$mtiiz['ptima_descr'];

            $a=0;
            $imprimir=array();
            foreach ($tabla as $key => $value) {
            	
            	if($value['value']!=null and $value['value'] != ' - '){
            		
            		$imprimir[$a]['descr']=$value['descr'];
            		$imprimir[$a]['value']=$value['value'];
            	}
            	$a++;
            }

            $tabla2=array();
            $tabla2[$a]['descr']='IMAGEN A BORDAR DERECHA';
            if($pedidos['pedid_mtide']=='5'){
            	$tabla2[$a]['value']='<img src="'.Yii::app()->getBaseUrl(true).'/'.$pimagen[5]['pimag_ruta'].'" class="img-responsive" >';
            }else if($pedidos['pedid_mtide']=='6'){
            	$tabla2[$a]['value']=$this->funciones->VerElectivos($pedidos);
            }
            
            $a++;
            $tabla2[$a]['descr']='IMAGEN A BORDAR IZQUIERDA';
            if($pedidos['pedid_mtiiz']=='5'){
				$tabla2[$a]['value']='<img src="'.Yii::app()->getBaseUrl(true).'/'.$pimagen[6]['pimag_ruta'].'" class="img-responsive" >';
			}else if($pedidos['pedid_mtiiz']=='6'){
				$tabla2[$a]['value']=$this->funciones->VerElectivos($pedidos);
			}
            $a++;
            $tabla2[$a]['descr']='OBSERVACIONES';
            $tabla2[$a]['value']=$pedidos['pedid_mobse'];

            $imprimir2=array();
            foreach ($tabla2 as $key => $value) {
            	
            	if($value['value']!=null and $value['value'] != ' - '){
            		
            		$imprimir2[$a]['descr']=$value['descr'];
            		$imprimir2[$a]['value']=$value['value'];
            	}
            	$a++;
            }
            $contador1=count($imprimir);
            $contador2=count($imprimir2);
            if ($contador1>0 or $contador2>0) {
        ?>    	
            
        
	    <h3 class="text-left">MANGAS </h3>
	    <table class="items" width="100%" style="font-size: 12pt; border-collapse: collapse;" cellpadding="5">
			<tbody>
				<?php
					$a=0;
					$count=count($imprimir);
					foreach ($imprimir as $key => $value) {
						$a++;
						if($a%2>0){
		            		echo '<tr>';	
		            	}
						echo "<td width='25%'><b>".$value['descr']."</b></td>";
						
		            	if($a==$count){
		            		echo '<td colspan=3>'.$value['value'].'</td>';	
		            	}else{
		            		echo '<td width="25%">'.$value['value'].'</td>';	
		            	}
		            	
		            	if($a%2==0 or $a==$count){
		            		echo '</tr>';	
		            	}
		            	
		            }

					$a=0;
					$count=count($imprimir2);
					foreach ($imprimir2 as $key => $value) {
						$a++;
						if($a%2>0){
		            		echo '<tr>';	
		            	}
						echo "<td width='25%'><b>".$value['descr']."</b></td>";
						
		            	if($a==$count){
		            		echo '<td colspan=3>'.$value['value'].'</td>';	
		            	}else{
		            		echo '<td width="25%">'.$value['value'].'</td>';	
		            	}
		            	
		            	if($a%2==0 or $a==$count){
		            		echo '</tr>';	
		            	}
		            	
		            }
				?>
			</tbody>
	    </table>
	    <?php
		}
            $sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_fuente b ON (a.pedid_eafue = b.fuent_codig)
            		WHERE pedid_codig = '".$c."'";
            $eafue = $conexion->createCommand($sql)->queryRow();

            $sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_modelo_color b ON (a.pedid_eacol = b.mcolo_codig)
            		WHERE pedid_codig = '".$c."'";
            $eacol = $conexion->createCommand($sql)->queryRow();

           	$sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_fuente b ON (a.pedid_ebfue = b.fuent_codig)
            		WHERE pedid_codig = '".$c."'";
            $ebfue = $conexion->createCommand($sql)->queryRow();

            $sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_modelo_color b ON (a.pedid_ebcol = b.mcolo_codig)
            		WHERE pedid_codig = '".$c."'";
            $ebcol = $conexion->createCommand($sql)->queryRow();

            $sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_fuente b ON (a.pedid_aefue = b.fuent_codig)
            		WHERE pedid_codig = '".$c."'";
            $aefue = $conexion->createCommand($sql)->queryRow();

            $sql = "SELECT * FROM pedido_pedidos a
            		JOIN pedido_modelo_color b ON (a.pedid_aecol = b.mcolo_codig)
            		WHERE pedid_codig = '".$c."'";
            $aecol = $conexion->createCommand($sql)->queryRow();

            $tabla=array();

            $a=0;
            $tabla[$a]['titul']='ESPALDA ALTA';
            $tabla[$a]['descr']='TEXTO A BORDAR';
            $tabla[$a]['value']=$pedidos['pedid_eatbo'];
            $a++;
            $tabla[$a]['titul']='ESPALDA ALTA';
            $tabla[$a]['descr']='FUENTE';
            $tabla[$a]['value']=$eafue['fuent_descr'];
            $a++;
            $tabla[$a]['titul']='ESPALDA ALTA';
            $tabla[$a]['descr']='COLOR';
            $tabla[$a]['value']=$eacol['mcolo_numer'].' - '.$eacol['mcolo_descr'];
            $a++;
            $tabla[$a]['titul']='ESPALDA BAJA';
            $tabla[$a]['descr']='TEXTO A BORDAR';
            $tabla[$a]['value']=$pedidos['pedid_ebtbo'];
            $a++;
            $tabla[$a]['titul']='ESPALDA BAJA';
            $tabla[$a]['descr']='FUENTE';
            $tabla[$a]['value']=$ebfue['fuent_descr'];
            $a++;
            $tabla[$a]['titul']='ESPALDA BAJA';
            $tabla[$a]['descr']='COLOR';
            $tabla[$a]['value']=$ebcol['mcolo_numer'].' - '.$ebcol['mcolo_descr'];
            $a++;
            $tabla[$a]['titul']='APODO ESPALDA';
            $tabla[$a]['descr']='FUENTE';
            $tabla[$a]['value']=$aefue['fuent_descr'];
            $a++;
            $tabla[$a]['titul']='APODO ESPALDA';
            $tabla[$a]['descr']='COLOR';
            $tabla[$a]['value']=$aecol['mcolo_numer'].' - '.$aecol['mcolo_descr'];

            $a=0;
            $imprimir=array();
            foreach ($tabla as $key => $value) {
            	
            	if($value['value']!=null and $value['value'] != ' - '){
            		$imprimir[$a]['titul']=$value['titul'];
            		$imprimir[$a]['descr']=$value['descr'];
            		$imprimir[$a]['value']=$value['value'];
            	}
            	$a++;
            }

            $a=0;
            $tabla2=array();
            $tabla2[$a]['descr']='OBSERVACIONES';
            $tabla2[$a]['value']=$pedidos['pedid_eobse'];
            $a++;
            $tabla2[$a]['descr']='IMAGEN PRINCIPAL';
            if($pedidos['pedid_iprut']){
				$tabla2[$a]['value']= '<img src="'.Yii::app()->getBaseUrl(true).'/'.$pedidos['pedid_iprut'].'" class="img-responsive" >';
			}
           
            $imprimir2=array();
            foreach ($tabla2 as $key => $value) {
            	
            	if($value['value']!=null and $value['value'] != ' - '){
            		
            		$imprimir2[$a]['descr']=$value['descr'];
            		$imprimir2[$a]['value']=$value['value'];
            	}
            	$a++;
            }
            $contador1=count($imprimir);
            $contador2=count($imprimir2);
            if ($contador1>0 or $contador2>0) {
        ?>
	    <h3 class="text-left">ESPALDA</h3>
	    <table class="items" width="100%" style="font-size: 12pt; border-collapse: collapse;" cellpadding="5">
			<tbody>
				<?php
					$a=0;
					$titulo='';
					$count=count($imprimir);
					foreach ($imprimir as $key => $value) {
						$a++;
						if($titulo!=$value['titul']){
							$titulo=$value['titul'];
		            		echo '<tr>';
							echo "<td width='25%' colspan='4' align='center'><b>".$value['titul']."</b></td>";
							echo '</tr>';	
							$a=1;
						}
						if($a%2>0){
		            		echo '<tr>';	
		            	}
						echo "<td width='25%'><b>".$value['descr']."</b></td>";
						if($a%2==0 and $a==$count){
							echo '<td>'.$value['value'].'</td>';
						}else if($a==$count){
		            		echo '<td colspan=3>'.$value['value'].'</td>';	
		            	}else{
		            		echo '<td>'.$value['value'].'</td>';	
		            	}
		            	
		            	if($a%2==0 or $a==$count){
		            		echo '</tr>';	
		            	}
		            	
		            }

					$a=0;
					$count=count($imprimir2);
					foreach ($imprimir2 as $key => $value) {
						$a++;
						if($a%2>0){
		            		echo '<tr>';	
		            	}
						echo "<td width='25%'><b>".$value['descr']."</b></td>";
						
		            	if($a%2==0 and $a==$count){
							echo '<td width="25%">'.$value['value'].'</td>';
						}elseif($a==$count){
		            		echo '<td colspan=3>'.$value['value'].'</td>';	
		            	}else{
		            		echo '<td>'.$value['value'].'</td>';	
		            	}
		            	
		            	if($a%2==0 or $a==$count){
		            		echo '</tr>';	
		            	}
		            	
		            }
				?>
				
			</tbody>
	    </table>
	<?php } ?>
	    <h3 class="text-left">DIBUJO SIMPLE DEL POLERON COMPLETO POR AMBOS LADOS</h3>
	    <table class="items" width="100%" style="font-size: 12pt; border-collapse: collapse;" cellpadding="5">
			<tbody>
				<tr>
					<td width="50%" align="center"><b>IMAGEN FRONTAL</b></td>
					<td align="center"><b>IMAGEN ESPALDA</b></td>
				</tr>
				<tr>
					<td align="center">
						<?php 
							if($pedidos['pedid_rifro']){
								echo '<img src="'.Yii::app()->getBaseUrl(true).'/'.$pedidos['pedid_rifro'].'" class="img-responsive">';
							}
						?>
					</td>
					<td align="center">
						<?php 
							if($pedidos['pedid_riesp']){
								echo '<img src="'.Yii::app()->getBaseUrl(true).'/'.$pedidos['pedid_riesp'].'" class="img-responsive">';
							}
						?>
					</td>
				</tr>
				
			</tbody>
	    </table>
	    <?php
			
	    	$sql="SELECT * FROM pedido_pedidos_imagen WHERE pedid_codig='".$pedidos['pedid_codig']."' and pimag_orden >=10";
				$result=$conexion->createCommand($sql)->queryAll();
			if($result){
	    ?>
	    <h3 class="text-left">ELECTIVO</h3>
	    <table class="items" width="100%" style="font-size: 12pt; border-collapse: collapse;" cellpadding="5">
	    	<thead>
	    		<tr>
	    			<th width="5%">#</th>
	    			<th width="20%">Nombre</th>
	    			<th width="25%">Imagen</th>
	    			<th width="25%">Texto a Bordar</th>
	    			<!--th width="25%">Precio</th-->
	    		</tr>
	    	</thead>
			<tbody>
				<?php
				$sql="SELECT * FROM pedido_pedidos_imagen WHERE pedid_codig='".$pedidos['pedid_codig']."' and pimag_orden >=10";
				$result=$conexion->createCommand($sql)->queryAll();
				$a=1;
				foreach ($result as $key => $value) {
					if($value['pimag_tbord']!='' and $value['pimag_ruta']!=''){
						$preci=3;
					}else if($value['pimag_tbord']!='' and $value['pimag_ruta']==''){
						$preci=2;
					}else if($value['pimag_tbord']=='' and $value['pimag_ruta']!=''){
						$preci=1;
					}
					$sql="SELECT * FROM pedido_precio_electivo where pelec_codig='".$preci."'";
					$precio=$conexion->createCommand($sql)->queryRow();
					?>
					<tr>
						<td align="center"><b><?php echo $a ?></b></td>
						<td align="center"><b><?php echo $value['pimag_descr']?></b></td>
						<td align="center"><?php echo '<img src="'.Yii::app()->getBaseUrl(true).'/'.$value['pimag_ruta'].'" class="img-responsive" width="100px">'; ?></td>
						<td align="center"><b><?php echo $value['pimag_tbord']; ?></b></td>
						<!--td align="center"><b><?php echo $this->funciones->transformarMonto_v($precio['pelec_preci'],0); ?></b></td-->
					</tr>
					<?php
					//$totale+=$precio['pelec_preci'];
					$a++;
				}
				?>
			</tbody>
			<!--tfoot>
				<tr>
					<th colspan="4">TOTAL </th>
					<th ><?php echo $this->funciones->transformarMonto_v($totale,0); ?></th>
				</tr>
			</tfoot-->
	    </table>
	    <?php
			}
	    	$sql = "SELECT *
                    FROM pedido_pedidos_adicional a
                    JOIN pedido_costo b ON (a.costo_codig = b.costo_codig)
                    WHERE a.pedid_codig='".$c."'";
				$result=$conexion->createCommand($sql)->queryAll();
			if($result){
	    ?>
	    <h3 class="text-left">COSTOS ADICIONALES</h3>
	    <table class="items" width="100%" style="font-size: 12pt; border-collapse: collapse;" cellpadding="5">
	    	<thead>
	    		<tr>
	    			<th width="5%">#</th>
	    			<th width="35%">DESCRIPCIÓN</th>
	    			<th width="35%">OBSERVACIÓN</th>
	    			<th width="25%">MONTO</th>
	    		</tr>
	    	</thead>
			<tbody>
				<?php
				$sql = "SELECT *
                    FROM pedido_pedidos_adicional a
                    JOIN pedido_costo b ON (a.costo_codig = b.costo_codig)
                    WHERE a.pedid_codig='".$c."'";
				$result=$conexion->createCommand($sql)->queryAll();
				$a=1;
				foreach ($result as $key => $value) {
					
					?>
					<tr>
						<td align="center"><b><?php echo $a ?></b></td>
						<td align="center"><b><?php echo $value['costo_descr']?></b></td>
						<td align="center"><b><?php echo $value['adici_obser']?></b></td>
						<td align="center"><?php echo $this->funciones->transformarMonto_v($value['adici_monto'],0); ?></td>
						
					</tr>
					<?php
					$totalc+=$value['adici_monto'];
					$a++;
				}
				?>
			</tbody>
			<tfoot>
				<tr>
					<th colspan="3">TOTAL </th>
					<th ><?php echo $this->funciones->transformarMonto_v($totalc,0); ?></th>
				</tr>
			</tfoot>
	    </table>

	    <?php
			}
	    	 $sql = "SELECT *
                    FROM pedido_pedidos_deduccion a
                    JOIN pedido_deduccion b ON (a.deduc_codig = b.deduc_codig)
                    WHERE a.pedid_codig='".$c."'";
				$result=$conexion->createCommand($sql)->queryAll();
			if($result){
	    ?>

	    <h3 class="text-left">DEDUCCIÓN</h3>
	    <table class="items" width="100%" style="font-size: 12pt; border-collapse: collapse;" cellpadding="5">
	    	<thead>
	    		<tr>
	    			<th width="5%">#</th>
	    			<th width="35%">DESCRIPCIÓN</th>
	    			<th width="35%">OBSERVACIÓN</th>
	    			<th width="25%">MONTO</th>
	    		</tr>
	    	</thead>
			<tbody>
				<?php
				
				$a=1;
				foreach ($result as $key => $value) {
					
					?>
					<tr>
						<td align="center"><b><?php echo $a ?></b></td>
						<td align="center"><b><?php echo $value['deduc_descr']?></b></td>
						<td align="center"><b><?php echo $value['pdedu_obser']?></b></td>
						<td align="center"><?php echo $this->funciones->transformarMonto_v($value['pdedu_monto'],0); ?></td>
						
					</tr>
					<?php
					$totald+=$value['pdedu_monto'];
					$a++;
				}
				?>
			</tbody>
			<tfoot>
				<tr>
					<th colspan="3">TOTAL </th>
					<th ><?php echo $this->funciones->transformarMonto_v($totald,0); ?></th>
				</tr>
			</tfoot>
	    </table>
		<?php } ?>
	    <h3 class="text-left">TOTALES</h3>
	    <table class="items" width="100%" style="font-size: 12pt; border-collapse: collapse;" cellpadding="5">
	    	<thead>
	    		<tr>
	    			<th width="5%">#</th>
	    			<th width="70%">DESCRIPCIÓN</th>
	    			<th width="25%">MONTO</th>
	    		</tr>
	    		
	    	</thead>
			<tbody>
				<tr>
					<td align="center">1</td>
					<td align="center"><b>MODELOS</b></td>
					<td align="center"><?php echo $this->funciones->transformarMonto_v($totalm,0)?></td>
				</tr>
				<?php
				/*if($totale>0){
					?>
					<tr>
						<td align="center">2</td>
						<td align="center"><b>ELECTIVOS</b></td>
						<td align="center"><?php echo $this->funciones->transformarMonto_v($totale,0)?></td>
					</tr>
					<?php
				}*/
				?>
				<?php
				if($totalc>0){
					?>
					<tr>
						<td align="center">3</td>
						<td align="center"><b>COSTOS ADICIONALES</b></td>
						<td align="center"><?php echo $this->funciones->transformarMonto_v($totalc,0)?></td>
					</tr>
					<?php
				}
				?>
				<?php
				if($totald>0){
					?>
					<tr>
						<td align="center">4</td>
						<td align="center"><b>DEDUCCIÓN</b></td>
						<td align="center">-<?php echo  $this->funciones->transformarMonto_v($totald,0)?></td>
					</tr>
					<?php
				}
				?>
				
					
								
			</tbody>
			<tfoot>
				<tr>
					<th colspan="2">TOTAL </th>
					<th ><?php echo $this->funciones->transformarMonto_v($totalm+$totale+$totalc-$totald,0); ?></th>
				</tr>
			</tfoot>
	    </table>
    </body>
 </html>
 <?php  ?>