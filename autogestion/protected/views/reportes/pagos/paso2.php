<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Detalle del Pedido</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Pedidos</a></li>
            <li><a href="#">Detalle</a></li>
            <li class="active">Registrar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <div class="row line-steps">
                  <div class="col-md-4 column-step <?php echo $ubicacion[0]; ?>">
                    <a href="modificar?c=<?php echo $solicitud['solic_codig']?>&s=1">
                        <div class="step-number">1 </div>
                    <div class="step-title">Pedido</div>
                    <div class="step-info">Detalles del Pedido</div>
                 </div>

                 <div class="col-md-4 column-step <?php echo $ubicacion[1]; ?> ">
                    <div class="step-number">2</div>
                    <div class="step-title">Adicional</div>
                    <div class="step-info">Información Adicional a Bordar Parte 1</div>
                 </div>
                 <div class="col-md-4 column-step <?php echo $ubicacion[2]; ?> ">
                    <div class="step-number">3</div>
                    <div class="step-title">Adicional</div>
                    <div class="step-info">Información Adicional a Bordar Parte 2</div>
                 </div>
              </div>
        </div>
    </div>
</div>
<form id='login-form' name='login-form' method="post">

<div class="row">   
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Pecho</h3>
        </div>
        <div class="panel-body" >
           
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
                <div class="row hide">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Pedido *</label>
                            <?php 
                                echo CHtml::hiddenField('pedid', $p, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Código *</label>
                            <?php 
                                echo CHtml::hiddenField('codig', $c, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Paso *</label>
                            <?php 
                                echo CHtml::hiddenField('pasos', $pedidos['solic_pasos'], array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Posición *</label>
                            <?php 
                                
                                $data=array('1'=>'DERECHO', '2'=>'IZQUIERDO', '3'=>'AMBOS');
                                echo CHtml::dropDownList('pposi', $pedidos['solic_pposi'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Fuente *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.fuent_codig, fuent_descr
                                      FROM pedido_fuente a
                                      GROUP BY 1,2
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                echo CHtml::dropDownList('pfuen', $pedidos['solic_pfuen'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-1" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                      FROM pedido_modelo_color a
                                      GROUP BY 1,2
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                                echo CHtml::dropDownList('pcolo', $pedidos['solic_pcolo'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-2" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>

                    
                    
                </div>
                
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Lleva Imagen Bordada *</label>
                            <?php 
                                
                                $data=array('1'=>'SI', '2'=>'NO');
                                echo CHtml::dropDownList('plima', $pedidos['solic_plima'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    
                    
                
                <?php
                if($pedidos['solic_plima']=='1'){
                    $opcional['1']['hide']='';
                }else{
                    $opcional['1']['hide']='hide';  
                }
                ?>
                <div class=" <?php echo $opcional['1']['hide'] ?>" id="opcional-1">
                        <div class="col-sm-8">        
                            <div class="numero1 form-group">
                                <label>Imagen Libre</label>
                                <div  class="input-group image-preview" data-placement="top" >  

                                    <img id="dynamic">
                                    <!-- image-preview-filename input [CUT FROM HERE]-->
                                    <?php 
                                        $ruta=$pedidos['solic_prima'];
                                        $nombre=explode('/', $ruta);
                                        $nombre=end($nombre);
                                        if($nombre){
                                            $opcional['1']['hide']='';
                                            $opcional['1']['show']='true';
                                        }else{
                                            $opcional['1']['hide']='hide';
                                            $opcional['1']['show']='false';
                                        }
                                    ?>
                                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="true" value="<?php echo $nombre ?>" > <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-btn">
                                        <!-- image-preview-clear button -->
                                        <button type="button" class="btn btn-default image-preview-clear <?php echo $opcional['1']['hide'] ?>" >
                                            <span class="fa fa-times"></span> Limpiar
                                        </button>
                                        <!-- image-preview-input -->
                                        <div class="btn btn-default image-preview-input">
                                            <span class="fa fa-folder-open"></span>
                                            <span class="numero1 image-preview-input-title">Buscar</span>
                                            <input type="file" id="prima" name="prima"/> <!-- rename it -->
                                        </div>
                                    </span>
                               
                                </div> 
                            </div> 
                            <div class="hide" id="popover-numero-1">
                                <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$ruta ?>" class="img-responsive">
                            </div>
                            <?php
                                if($opcional['1']['show']=='true') { 
                            ?>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    var closebtn = $('<button/>', {
                                        type: "button",
                                        text: 'x',
                                        id: 'close-preview',
                                        style: 'font-size: initial;',
                                    });
                                    closebtn.attr("class","close pull-right");
                                    $('.numero1 .image-preview').popover({
                                      html: true,
                                      title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
                                      content: function() {
                                        return $('#popover-numero-1').html();
                                      },
                                      trigger: 'manual'
                                    });

                                    $(".numero1 .image-preview").popover("show");
                                });
                            </script>
                            <?php
                                }
                            ?>
                        </div>
                        
                    </div>

                </div>
                 <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observaciones</label>
                            <?php 
                                echo CHtml::textArea('pobse', $pedidos['solic_pobse'], array('class' => 'form-control', 'placeholder' => "Observaciones",'prompt'=>'Seleccione...')); ?>
                                
                               
                        </div>
                    </div>
                    
                </div>

            

        </div><!-- form -->
    </div>  
</div>
<?php
if($pedidos['gorro']=='1'){
    $gorro='';
}else{
    $gorro='true';
}
?>

<div class="row">   
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Gorro</h3>
        </div>
        <div class="panel-body" >
           
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Posición *</label>
                            <?php 
                                
                                $data=array('1'=>'DERECHO', '2'=>'IZQUIERDO', '3'=>'AMBOS');
                                echo CHtml::dropDownList('gposi', $pedidos['solic_gposi'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$gorro)); ?>
                        </div>
                    </div>
                    
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Fuente *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.fuent_codig, fuent_descr
                                      FROM pedido_fuente a
                                      GROUP BY 1,2
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                echo CHtml::dropDownList('gfuen', $pedidos['solic_gfuen'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$gorro)); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-7" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                      FROM pedido_modelo_color a
                                      GROUP BY 1,2
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                                echo CHtml::dropDownList('gcolo', $pedidos['solic_gcolo'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$gorro)); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-8" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>

                    
                    
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Lleva Imagen Bordada *</label>
                            <?php 
                                
                                $data=array('1'=>'SI', '2'=>'NO');
                                echo CHtml::dropDownList('glima', $pedidos['solic_glima'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$gorro)); ?>
                        </div>
                    </div>
                    
                    
                
                <?php
                if($pedidos['solic_glima']=='1'){
                    $opcional['2']['hide']='';
                }else{
                    $opcional['2']['hide']='hide';  
                }
                ?>
                <div class="<?php echo $opcional['2']['hide'] ?>" id="opcional-2">
                        <div class="col-sm-8">        
                            <div class="numero2 form-group">
                                <label>Imagen Libre</label>
                                <div  class="input-group image-preview" data-placement="top" >  

                                    <img id="dynamic">
                                    <!-- image-preview-filename input [CUT FROM HERE]-->
                                    <?php 
                                        $ruta=$pedidos['solic_grima'];
                                        $nombre=explode('/', $ruta);
                                        $nombre=end($nombre);
                                        if($nombre){
                                            $opcional['2']['hide']='';
                                            $opcional['2']['show']='true';
                                        }else{
                                            $opcional['2']['hide']='hide';
                                            $opcional['2']['show']='false';
                                        }
                                    ?>
                                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="true" value="<?php echo $nombre ?>" > <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-btn">
                                        <!-- image-preview-clear button -->
                                        <button type="button" class="btn btn-default image-preview-clear <?php echo $opcional['2']['hide'] ?>" >
                                            <span class="fa fa-times"></span> Limpiar
                                        </button>
                                        <!-- image-preview-input -->
                                        <div class="btn btn-default image-preview-input">
                                            <span class="fa fa-folder-open"></span>
                                            <span class="numero2 image-preview-input-title">Buscar</span>
                                            <input type="file" id="grima" name="grima"/> <!-- rename it -->
                                        </div>
                                    </span>
                               
                                </div> 
                            </div> 
                            <div class="hide" id="popover-numero-2">
                                <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$ruta ?>" class="img-responsive">
                            </div>
                            <?php
                                if($opcional['2']['show']=='true') { 
                            ?>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    var closebtn = $('<button/>', {
                                        type: "button",
                                        text: 'x',
                                        id: 'close-preview',
                                        style: 'font-size: initial;',
                                    });
                                    closebtn.attr("class","close pull-right");
                                    $('.numero2 .image-preview').popover({
                                      html: true,
                                      title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
                                      content: function() {
                                        return $('#popover-numero-2').html();
                                      },
                                      trigger: 'manual'
                                    });

                                    $(".numero2 .image-preview").popover("show");
                                });
                            </script>
                            <?php
                                }
                            ?>
                        </div>
                        
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observaciones</label>
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.fuent_codig, fuent_descr
                                      FROM pedido_fuente a
                                      GROUP BY 1,2
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                echo CHtml::textArea('gobse', $pedidos['solic_gobse'], array('class' => 'form-control', 'placeholder' => "Observaciones",'prompt'=>'Seleccione...','disabled'=>$gorro)); ?>
                                
                               
                        </div>
                    </div>
                    
                </div>

            

        </div><!-- form -->
    </div>  
</div>
<div class="row">                    
    <div class="panel panel-default" >
        <div class="panel-body">
            <div class="row controls">
                <div class="col-sm-4     ">
                    <a href="modificar?p=<?php echo $p?>&c=<?php echo $c?>&s=2" type="reset" class="btn-block btn btn-default">Volver </a>
                </div>
                <div class="col-sm-4 ">
                        <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                    </div>
                <div class="col-sm-4 ">
                    <button id="guardar" type="button" class="btn-block btn btn-info">Siguiente  </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hide" id="popover-img">
    <?php
        $sql="SELECT * FROM pedido_fuente where fuent_codig = '".$pedidos['solic_npfue']."'";
        $fuente=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$fuente['fuent_ruta'] ?>" class="img-responsive"></div>
<div class="hide" id="popover-img-2">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['solic_npcol']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive">
</div>
<div class="hide" id="popover-img-3">
    <?php
        $sql="SELECT * FROM pedido_fuente where fuent_codig = '".$pedidos['solic_aefue']."'";
        $fuente=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$fuente['fuent_ruta'] ?>" class="img-responsive"></div>
<div class="hide" id="popover-img-4">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['solic_aecol']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive">
</div>
<div class="hide" id="popover-img-5">
    <?php
        $sql="SELECT * FROM pedido_fuente where fuent_codig = '".$pedidos['solic_pfuen']."'";
        $fuente=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$fuente['fuent_ruta'] ?>" class="img-responsive"></div>
<div class="hide" id="popover-img-6">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['solic_pcolo']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive">
</div>
<div class="hide" id="popover-img-7">
    <?php
        $sql="SELECT * FROM pedido_fuente where fuent_codig = '".$pedidos['solic_gfuen']."'";
        $fuente=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$fuente['fuent_ruta'] ?>" class="img-responsive"></div>
<div class="hide" id="popover-img-8">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['solic_gcolo']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive">
</div>
</form>
<script>
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                                                npfue: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fuente" es obligatorio',
                        }
                    }
                },
                npcol: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color" es obligatorio',
                        }
                    }
                },
                nppos: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Posición" es obligatorio',
                        }
                    }
                },
                aefue: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fuente" es obligatorio',
                        }
                    }
                },
                aecol: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color" es obligatorio',
                        }
                    }
                },
                aepos: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Posición" es obligatorio',
                        }
                    }
                },
                pposi: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Posición" es obligatorio',
                        }
                    }
                },
                ptder: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar Derecha" es obligatorio',
                        }
                    }
                },
                ptizq: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar Izquierda" es obligatorio',
                        }
                    }
                },
                pfuen: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fuente" es obligatorio',
                        }
                    }
                },
                pcolo: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color" es obligatorio',
                        }
                    }
                },
                gposi: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Posición" es obligatorio',
                        }
                    }
                },
                gfuen: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fuente" es obligatorio',
                        }
                    }
                },
                gcolo: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color" es obligatorio',
                        }
                    }
                },



            }
        });
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        var data = new FormData(jQuery('form')[0]);
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                url: 'paso2',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        /*swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){*/
                            window.open('modificar?c=<?php echo $c; ?>&s=3', '_parent');
                        /*});*/
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>
<script type="text/javascript">
    $('#plima').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '1':
                jQuery("#opcional-1").removeClass('hide');
                jQuery("#prima").removeAttr('disabled');
                break;
            default:
                jQuery("#opcional-1").addClass('hide');
                jQuery("#prima").attr('disabled','true');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#glima').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '1':
                jQuery("#opcional-2").removeClass('hide');
                jQuery("#grima").removeAttr('disabled');
                break;
            default:
                jQuery("#opcional-2").addClass('hide');
                jQuery("#grima").attr('disabled','true');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#iopci').change(function () {
        var iopci = $(this).val();
        $.ajax({
            'type':'POST',
            'data':{'tmode':tmode.value},
            'url':'<?php echo CController::createUrl('funciones/PedidosColorOpcional'); ?>',
            'cache':false,
            'success':function(html){
                switch(iopci) {
                    case '1':
                        
                        jQuery("#opcional-1").removeClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#iclin").html(html);
                        jQuery("#iccie").html('');
                        jQuery("#icviv").html('');
                        jQuery("#ideta").removeAttr('disabled');
                        jQuery("#iclin").removeAttr('disabled');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").attr('disabled','true');

                        break;
                    case '2':
                        jQuery("#opcional-2").removeClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#iclin").html('');
                        jQuery("#iccie").html(html);
                        jQuery("#icviv").html('');
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").removeAttr('disabled');
                        jQuery("#icviv").attr('disabled','true');
                        break;
                    case '3':
                        jQuery("#opcional-3").removeClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#iclin").html('');
                        jQuery("#iccie").html('');
                        jQuery("#icviv").html(html);
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").removeAttr('disabled');
                        break;
                    default:
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#iclin").html(html);
                        jQuery("#iccie").html(html);
                        jQuery("#icviv").html(html);
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").attr('disabled','true');
                        break;
                }
                
            }
        });
    });
</script>
<script type="text/javascript">
    $('#ideta').change(function () {
        var ideta = $(this).val();
        switch(ideta) {
            case '1':
                jQuery("#iclin").removeAttr('disabled');
                break;
            default:
                jQuery("#iclin").attr('disabled','true');
                break;
        }
                
            
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-1').popover({
          html: true,
          content: function() {
            return $('#popover-img').html();
          },
          trigger: 'hover'
        });
});
$('#pfuen').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerFuente'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-2').popover({
          html: true,
          content: function() {
            return $('#popover-img-2').html();
          },
          trigger: 'hover'
        });
});
$('#pcolo').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-2").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-7').popover({
          html: true,
          content: function() {
            return $('#popover-img-7').html();
          },
          trigger: 'hover'
        });
});
$('#gfuen').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerFuente'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-7").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-8').popover({
          html: true,
          content: function() {
            return $('#popover-img-8').html();
          },
          trigger: 'hover'
        });
});
$('#gcolo').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-8").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script>

    $(document).on('click', '#close-preview', function () {
        $('.numero1 .image-preview').popover('hide');
        // Hover befor close the preview
        $('.numero1 .image-preview').hover(
            function () {
                $('.image-preview').popover('hide');
            },
            function () {
                $('.image-preview').popover('hide');
            }
        );
    });
    $(function () {
        // Create the close button
        var closebtn = $('<button/>', {
            type: "button",
            text: 'x',
            id: 'close-preview',
            style: 'font-size: initial;',
        });
        closebtn.attr("class","close pull-right");
        $('.numero1 .image-preview').popover({
            trigger:'manual',
            html:true,
            title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
            content: "No hay imagen",
            placement:'top'
        });



        
        // Clear event
        $('.numero1 .image-preview-clear').click(function () {
            $('.numero1 .image-preview').attr("data-content", "").popover('hide');
            $('.numero1 .image-preview-filename').val("");
            $('.numero1 .image-preview-clear').hide();
            $('.numero1 .image-preview-input input:file').val("");
            $(".numero1 .image-preview-input-title").text("Buscar");
        });
        // Create the preview image
        $(".numero1 .image-preview-input input:file").change(function () {
            var img = $('<img/>', {
                id: 'dynamic',
                width: 250,
                height: 200
            });
            var file = this.files[0];
            var reader = new FileReader();
            // Set preview image into the popover data-content
            reader.onload = function (e) {
                $(".numero1 .image-preview-input-title").text("Cambiar");
                $(".numero1 .image-preview-clear").show();
                $(".numero1 .image-preview-filename").val(file.name);
                img.attr('src', e.target.result);
                $(".numero1 .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
                }
            reader.readAsDataURL(file);
        });
    });
</script>
<script>

    $(document).on('click', '#close-preview', function () {
        $('.numero2 .image-preview').popover('hide');
        // Hover befor close the preview
        $('.numero2 .image-preview').hover(
            function () {
                $('.image-preview').popover('hide');
            },
            function () {
                $('.image-preview').popover('hide');
            }
        );
    });
    $(function () {
        // Create the close button
        var closebtn = $('<button/>', {
            type: "button",
            text: 'x',
            id: 'close-preview',
            style: 'font-size: initial;',
        });
        closebtn.attr("class","close pull-right");
        $('.numero2 .image-preview').popover({
            trigger:'manual',
            html:true,
            title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
            content: "No hay imagen",
            placement:'top'
        });



        
        // Clear event
        $('.numero2 .image-preview-clear').click(function () {
            $('.numero2 .image-preview').attr("data-content", "").popover('hide');
            $('.numero2 .image-preview-filename').val("");
            $('.numero2 .image-preview-clear').hide();
            $('.numero2 .image-preview-input input:file').val("");
            $(".numero2 .image-preview-input-title").text("Buscar");
        });
        // Create the preview image
        $(".numero2 .image-preview-input input:file").change(function () {
            var img = $('<img/>', {
                id: 'dynamic',
                width: 250,
                height: 200
            });
            var file = this.files[0];
            var reader = new FileReader();
            // Set preview image into the popover data-content
            reader.onload = function (e) {
                $(".numero2 .image-preview-input-title").text("Cambiar");
                $(".numero2 .image-preview-clear").show();
                $(".numero2 .image-preview-filename").val(file.name);
                img.attr('src', e.target.result);
                $(".numero2 .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
                }
            reader.readAsDataURL(file);
        });
    });
</script>