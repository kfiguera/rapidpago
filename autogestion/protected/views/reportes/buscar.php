<table id='auditoria' class="table table-bordered table-hover dataTable">
    <thead>
        <tr>
            <th width="1%">#</th>
            <th width="19%">CLIENTE</th>
            <th width="5%">NRO FACTURA</th>
            <th width="5%">NUMERO DE CONTROL</th>
            <th width="5%">MONTO FACTURA</th>
            <th width="5%">FLETE</th>
            <th width="5%">DEVOLUCION</th>
            <th width="5%">RETENCION</th>
            <th width="5%">FECHA EMISION </th>
            <th width="5%">FECHA RECIBIDA</th>
            <th width="5%">FECHA VENCE</th>
            <th width="5%">DIAS DE CREDITO</th>
            <th width="5%">FECHA PAGO</th>
            <th width="5%">MONTO PAGO </th>
            <th width="5%">DES</th>
            <th width="5%">DESCUENTO</th>
            <th width="5%">STATUS</th>
            <th width="5%">BANCO</th>
        </tr>
    </thead>
    <tbody>                
        <?php
        $sql = "SELECT factu_codig, b.clien_denom, factu_nfact, factu_ncont, factu_monto, factu_flete, factu_devol, factu_reten, factu_femis, factu_freci, factu_fvenc, factu_dcred, factu_fpago, factu_mpago, factu_pdesc, factu_desc, efact_descr, c.banco_descr
            FROM factura a
            JOIN cliente b ON (a.clien_codig = b.clien_codig)
            JOIN p_banco c ON (a.banco_codig = c.banco_codig)
            JOIN factura_estatu d ON (a.efact_codig = d.efact_codig)  
            ".$condicion;
            //echo $sql;
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $p_persona = $command->query();
        $i=0;
        while (($row = $p_persona->read()) !== false) {
            $i++;
        ?>
        <tr>
            <td><?php echo $i ?></td>
            <td><?php echo $row['clien_denom'] ?></td>
            <td><?php echo $this->TransformarMonto_v($row['factu_nfact'],0) ?></td>
            <td><?php echo $this->TransformarMonto_v($row['factu_ncont'],0) ?></td>
            <td><?php echo $this->TransformarMonto_v($row['factu_monto'],2) ?></td>
            <td><?php echo $this->TransformarMonto_v($row['factu_flete'],2) ?></td>
            <td><?php echo $this->TransformarMonto_v($row['factu_devol'],2) ?></td>
            <td><?php echo $this->TransformarMonto_v($row['factu_reten'],2) ?></td>
            <td><?php echo $this->TransformarFecha_v($row['factu_femis']) ?></td>
            <td><?php echo $this->TransformarFecha_v($row['factu_freci']) ?></td>
            <td><?php echo $this->TransformarFecha_v($row['factu_fvenc']) ?></td>
            <td><?php echo $row['factu_dcred'] ?></td>
            <td><?php echo $this->TransformarFecha_v($row['factu_fpago']) ?></td>
            <td><?php echo $this->TransformarMonto_v($row['factu_mpago'],2) ?></td>
            <td><?php echo $row['factu_pdesc'] ?>%</td>
            <td><?php echo $this->TransformarMonto_v($row['factu_desc'],2) ?></td>
            <td><?php echo $row['efact_descr'] ?></td>
            <td><?php echo $row['banco_descr'] ?></td>
        </tr>
        <?php
            }   
        ?>
    </tbody>
</table>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable(); 
    });
</script>