<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

		<style>

			body {
				font-family: ipamp;
		 		font-size: 10pt;
		 	}
		 	p { 
		 		margin: 0pt;
		 	}
		 	td { 
		 		vertical-align: middle; 
		 	}
		 	.items td {
		 		/*border-left: 0.1mm solid #000000;
		 		border-right: 0.1mm solid #000000;*/
		 		border: 0.1mm solid #000000;
		 		font-size: 10pt;
		 		text-align: left;
		 		padding: 2pt;
		 	}
		 	.items td img {
		 		max-height: 300px;
		 		max-width: 300px
		 		width:100%;
		 		height: auto;
		 	}
		 	table thead td, table thead th, table tfoot td, table tfoot th { 
		 		background-color: #EEEEEE;
		 		text-align: center;
		 		border: 0.1mm solid #000000;
		 		font-size: 10pt;
		 		padding: 2pt;
		 	}
		 	.items td.blanktotal {
		 		background-color: #FFFFFF;
		 		border: 0mm none #000000;
		 		border-top: 0.1mm solid #000000;
		 	}
		 	.items td.totals {
		 		text-align: right;
		 		border: 0.1mm solid #000000;
		 	}
		 	.text-center{
		 		text-align: center;
		 	}
		 	.text-left{
		 		text-align: left;
		 	}
		 	.text-right{
		 		text-align: right;
		 	}
		 	.img-responsive{
		 		width: 100%;
		 		height: auto;
		 		max-width: 50px;
		 	}
		</style>
	</head>
	<body>
		<?php
			$conexion=yii::app()->db;
            $sql = "SELECT * FROM pedido_pedidos WHERE pedid_codig = '".$c."'";
            $pedidos = $conexion->createCommand($sql)->queryRow();

            $sql = "SELECT * FROM pedido_pedidos a 
                    JOIN pedido_pedidos_detalle b ON (a.pedid_codig = b.pedid_codig)
                    JOIN pedido_pedidos_detalle_listado c ON (a.pedid_codig = c.pedid_codig)
                    JOIN pedido_talla d ON (c.talla_codig = d.talla_codig)
                    JOIN pedido_modelo e ON (b.model_codig = e.model_codig)
                    JOIN pedido_modelo_tipo f ON (b.tmode_codig = f.tmode_codig)
					LEFT JOIN pedido_corte g ON (c.pdlis_codig = g.pdlis_codig)
					LEFT JOIN p_trabajador h ON (g.traba_codig = h.traba_codig)
					LEFT JOIN p_persona i ON (h.perso_codig = i.perso_codig)
                    LEFT JOIN pedido_corte_estatu j ON (g.ecort_codig = j.ecort_codig)
                    WHERE a.pedid_codig = '".$c."'
                    ORDER BY h.traba_codig
                    ";

            $cortes = $conexion->createCommand($sql)->queryAll();

            $p_trabajador='no';
            $i=0;

            foreach ($cortes as $key => $corte) {
            	$i++;
            	if ($p_trabajador!=$corte['traba_codig']) {

            		if($i>1){
            		?>
            			</tbody>
            		</table>
            		<?php	
            		}
            		if($corte['traba_codig']==0){
            			?>
            			<h3>TRABAJADOR: SIN ASIGNAR</h3>
            			<?php
            		}else{
            			?>
            			<h3>TRABAJADOR: <?php echo $corte['perso_pnomb'].' '.$corte['perso_papel']; ?></h3>
            			<?php
            		}
            		?>

            		
            		<table class="items" width="100%" style="font-size: 10pt; border-collapse: collapse;" cellpadding="5">
	                    <thead>
	                        <tr>
	                            <th colspan="2">TRABAJADOR:</th>
	                            <th colspan="5" align="left"> <?php echo $corte['perso_pnomb'].' '.$corte['perso_papel']; ?> </th>
	                            <th colspan="2">Fecha de Entrega:</th>
	                            <th colspan="2"><?php echo $this->funciones->transformarFecha_v($corte['corte_fentr']) ?></th>
	                        </tr>
	                        <tr>
	                            <th width="2%">#</th>
	                            <th>Nro pedido</th>
	                            <th>Modelo</th>
	                            <th>Nombre</th>
	                            <th>Talla</th>
	                            <th>Ajuste de Talla</th>
	                            <th>Observaciones</th>
	                            <th>Trabajador</th>
	                            <th>Fecha de Entrega</th>
	                            <th>Estatus</th>
	                        </tr>
	                    </thead>
	                    <tbody>
            		<?php
            	}
            	?>
			            	<tr>
				            	<td class="tabla"><?php echo $i ?></td>
				                <td class="tabla"><?php echo $corte['pedid_numer'] ?></td>
				                <td class="tabla"><?php echo $corte['tmode_descr'].' '.$row['model_descr'] ?></td>
				                <td class="tabla"><?php echo $corte['pdlis_nombr'] ?></td>
				                <td class="tabla"><?php echo $corte['talla_descr'] ?></td>
				                <td class="tabla"><?php echo $this->funciones->VerAjusteTalla(json_decode($corte['pdlis_tajus'])) ?></td>
				                <td class="tabla"><?php echo $corte['pdlis_obser'] ?></td>
				                <td class="tabla"><?php echo $corte['perso_pnomb'].' '.$corte['perso_papel'] ?></td>
				                <td class="tabla"><?php echo $corte['corte_fentr'] ?></td>
				                <td class="tabla"><?php echo $corte['ecort_descr'] ?></td>
			            	</tr>
            	<?php
            	$p_trabajador=$corte['traba_codig'];
            }
        ?>
		 	</tbody>	 
	    </table>
	    
    </body>
 </html>
 <?php //exit; ?>