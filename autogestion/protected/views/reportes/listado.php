<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Reporte de Cobranza</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Reportes</a></li>            
            
            <li><a href="#">Cobranza</a></li>
            <li class="active">Listado</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Criterio de Busqueda</h3>
    </div>
    <div class="panel-body" >
        <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'form', 'action'=>'pdf', 'htmlOptions' => array('target'=>'_blank','id'=>'form','method' => 'post', 'enctype' => 'multipart/form-data')));
            $connection = Yii::app()->db;

        ?>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Número de Factura</label>
                    <?php echo CHtml::textField('nfact', '', array('class' => 'form-control', 'placeholder' => "Número de Factura")); ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Banco</label>
                    <?php 
                    $sql="SELECT * FROM p_banco ";
                    $result=$connection->createCommand($sql)->queryAll();
                    $data=CHtml::listData($result,'banco_codig','banco_descr');
                    echo CHtml::DropDownList('banco', '', $data,array('class' => 'form-control', 'placeholder' => "Banco", 'prompt'=>'Seleccione')); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Fecha de Emision Desde</label>
                    <?php echo CHtml::textField('desde', '', array('class' => 'form-control', 'placeholder' => "DD/MM/AAAA")); ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Fecha de Emision Hasta</label>
                    <?php echo CHtml::textField('hasta', '', array('class' => 'form-control', 'placeholder' => "DD/MM/AAAA")); ?>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-4">
                <button type="button" id="buscar" class="btn btn-block btn-info">Buscar</button>
            </div>
            <div class="col-sm-4">
                <button type="reset" class="btn btn-block btn-default">Limpiar</button>
            </div>
            <div class="col-sm-4">
                <a href="registrar" class="btn btn-block btn-info" >Nuevo</a>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div> 
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-sm-8">
                <h3 class="panel-title">Listado</h3>
            </div>
            <div class="col-sm-4">
                <button type="button" id="imprimir" class="btn btn-block btn-info">Imprimir</button>
            </div>
        </div>
    </div>
    <div class="panel-body" >
        
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-p_persona'>
                <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="1%">#</th>
                            <th width="19%">CLIENTE</th>
                            <th width="5%">NRO FACTURA</th>
                            <th width="5%">NUMERO DE CONTROL</th>
                            <th width="5%">MONTO FACTURA</th>
                            <th width="5%">FLETE</th>
                            <th width="5%">DEVOLUCION</th>
                            <th width="5%">RETENCION</th>
                            <th width="5%">FECHA EMISION </th>
                            <th width="5%">FECHA RECIBIDA</th>
                            <th width="5%">FECHA VENCE</th>
                            <th width="5%">DIAS DE CREDITO</th>
                            <th width="5%">FECHA PAGO</th>
                            <th width="5%">MONTO PAGO </th>
                            <th width="5%">DES</th>
                            <th width="5%">DESCUENTO</th>
                            <th width="5%">STATUS</th>
                            <th width="5%">BANCO</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <?php
                        $sql = "SELECT factu_codig, b.clien_denom, factu_nfact, factu_ncont, factu_monto, factu_flete, factu_devol, factu_reten, factu_femis, factu_freci, factu_fvenc, factu_dcred, factu_fpago, factu_mpago, factu_pdesc, factu_desc, efact_descr, c.banco_descr
                            FROM factura a
                            JOIN cliente b ON (a.clien_codig = b.clien_codig)
                            JOIN p_banco c ON (a.banco_codig = c.banco_codig)
                            JOIN factura_estatu d ON (a.efact_codig = d.efact_codig)";
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                        ?>
                        <tr>
                            <td><?php echo $i ?></td>
                            <td><?php echo $row['clien_denom'] ?></td>
                            <td><?php echo $this->TransformarMonto_v($row['factu_nfact'],0) ?></td>
                            <td><?php echo $this->TransformarMonto_v($row['factu_ncont'],0) ?></td>
                            <td><?php echo $this->TransformarMonto_v($row['factu_monto'],2) ?></td>
                            <td><?php echo $this->TransformarMonto_v($row['factu_flete'],2) ?></td>
                            <td><?php echo $this->TransformarMonto_v($row['factu_devol'],2) ?></td>
                            <td><?php echo $this->TransformarMonto_v($row['factu_reten'],2) ?></td>
                            <td><?php echo $this->TransformarFecha_v($row['factu_femis']) ?></td>
                            <td><?php echo $this->TransformarFecha_v($row['factu_freci']) ?></td>
                            <td><?php echo $this->TransformarFecha_v($row['factu_fvenc']) ?></td>
                            <td><?php echo $row['factu_dcred'] ?></td>
                            <td><?php echo $this->TransformarFecha_v($row['factu_fpago']) ?></td>
                            <td><?php echo $this->TransformarMonto_v($row['factu_mpago'],2) ?></td>
                            <td><?php echo $row['factu_pdesc'] ?>%</td>
                            <td><?php echo $this->TransformarMonto_v($row['factu_desc'],2) ?></td>
                            <td><?php echo $row['efact_descr'] ?></td>
                            <td><?php echo $row['banco_descr'] ?></td>
                        </tr>
                        <?php
                            }   
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>  
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
    $('#desde').mask('99/99/9999');
    $('#hasta').mask('99/99/9999');
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#buscar').click(function () {
        var formData = new FormData($("#form")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-p_persona').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
    $('#imprimir').click(function(){
        var formData = new FormData($("#form")[0]);
        $("#form").submit();
    });
    jQuery('#desde').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'

      });
    jQuery('#hasta').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'

      });
</script>
