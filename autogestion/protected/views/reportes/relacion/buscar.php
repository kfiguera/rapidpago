<table id='auditoria' class="table table-bordered table-hover dataTable">
    <thead>
        <tr>
            <th width="1%">#</th>
            <th width="15,66%">FECHA EGRESO</th>
            <th width="16,66%">MONTO EGRESO</th>
            <th width="16,66%">NRO FACTURA</th>
            <th width="16,66%">NRO REFERENCIA</th>
            <th width="16,66%">MOTIVO</th>
            <th width="16,66%">BANCO</th>
        </tr>
    </thead>
    <tbody>                
        <?php
        $sql = "SELECT *
                FROM egresos a
                JOIN p_banco b ON (a.banco_codig = b.banco_codig)  
            ".$condicion;
            //echo $sql;
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $p_persona = $command->query();
        $i=0;
        while (($row = $p_persona->read()) !== false) {
            $i++;
        ?>
        <tr>
            <td><?php echo $i ?></td>
            <td><?php echo $this->  TransformarFecha_v($row['egres_fegre']) ?></td>
            <td><?php echo $this->funciones->TransformarMonto_v($row['egres_monto'],0) ?></td>
            <td><?php echo $this->funciones->TransformarMonto_v($row['egres_nfact'],0) ?></td>
            <td><?php echo $row['egres_refer'] ?></td>
            <td><?php echo $row['egres_motiv'] ?></td>
            <td><?php echo $row['banco_descr'] ?></td>
        </tr>
        <?php
            }   
        ?>
    </tbody>
</table>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable(); 
    });
</script>