<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

		<style>

			body {
				font-family: ipamp;
		 		font-size: 10pt;
		 	}
		 	
		 	td { 
		 		vertical-align: middle; 
		 	}
		 	p{
		 		text-align: justify;
		 	}
		 	.items td {
		 		/*border-left: 0.1mm solid #000000;
		 		border-right: 0.1mm solid #000000;*/
		 		border: 0.1mm solid #000000;
		 		font-size: 8pt;
		 		text-align: left;
		 		padding: 2pt;
		 	}
		 	.items td img {
		 		max-height: 300px;
		 		max-width: 300px
		 		width:100%;
		 		height: auto;
		 	}
		 	table thead td, table thead th, table tfoot td, table tfoot th { 
		 		background-color: #EEEEEE;
		 		text-align: center;
		 		border: 0.1mm solid #000000;
		 		font-size: 10pt;
		 		padding: 2pt;
		 	}
		 	.items td.blanktotal {
		 		background-color: #FFFFFF;
		 		border: 0mm none #000000;
		 		border-top: 0.1mm solid #000000;
		 	}
		 	.items td.totals {
		 		text-align: right;
		 		border: 0.1mm solid #000000;
		 	}
		 	.text-center{
		 		text-align: center;
		 	}
		 	.text-left{
		 		text-align: left;
		 	}
		 	.text-right{
		 		text-align: right;
		 	}
		 	.img-responsive{
		 		width: 100%;
		 		height: auto;
		 		max-width: 50px;
		 	}
		</style>
	</head>
	<body>
		<h4 align="center">DICCIONARIO DE DATOS PROYECTO SIGO</h4>
		<?php
			$conexion=yii::app()->db;
			$sql="SELECT t1.TABLE_NAME AS tabla_nombre,
						 t1.TABLE_COMMENT AS tabla_descripcion
				  FROM INFORMATION_SCHEMA.TABLES AS t1
				  WHERE t1.TABLE_SCHEMA='rapidpago' 
				    AND t1.TABLE_COMMENT <> ''
				  ORDER BY t1.TABLE_NAME;";
			$tablas=$conexion->createCommand($sql)->queryAll();		
			foreach ($tablas as $key => $tabla) {
				?>
				<p><b>Tabla:</b> <?php echo $tabla['tabla_nombre']?></p>
				<p><b>Descripción:</b> <?php echo $tabla['tabla_descripcion']?></p>
				<table class="items" width="100%" style="font-size: 8pt; border-collapse: collapse;" cellpadding="5">
					<thead>
						
						<tr>
							<th width="15%">Nombre:</th>
							<th width="10%">Indice:</th>
							<th width="10%">Nulo:</th>
							<th width="15%">Tipo de Dato:</th>
							<th width="15%">Longitud:</th>
							<th width="35%">Descripcion:</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$sql="SELECT
								    t1.COLUMN_NAME AS columna_nombre,
								    t1.COLUMN_KEY AS columna_indice,
								    t1.IS_NULLABLE AS columna_nulo,
								    t1.DATA_TYPE AS columna_tipo_dato,
								    IFNULL(t1.NUMERIC_PRECISION,
								    t1.CHARACTER_MAXIMUM_LENGTH) AS columna_longitud,
								    t1.COLUMN_COMMENT AS columna_descripcion
								FROM 
								    INFORMATION_SCHEMA.COLUMNS t1
								WHERE 
								    t1.TABLE_SCHEMA = 'rapidpago' AND
								    t1.TABLE_NAME = '".$tabla['tabla_nombre']."'
								ORDER BY
								    t1.ORDINAL_POSITION;";

							$campos=$conexion->createCommand($sql)->queryAll();	
							foreach ($campos as $key => $campo) {
								?>
								<tr>
									<td><?php echo $campo['columna_nombre']?></td>
									<td><?php echo $campo['columna_indice']?></td>
									<td><?php echo $campo['columna_nulo']?></td>
									<td><?php echo $campo['columna_tipo_dato']?></td>
									<td><?php echo $campo['columna_longitud']?></td>
									<td><?php echo $campo['columna_descripcion']?></td>
								</tr>
								<?php
							}
						?>
					</tbody>
	        	</table>
				<?php
			}
        ?>
        
    </body>
 </html>
 <?php  ?>