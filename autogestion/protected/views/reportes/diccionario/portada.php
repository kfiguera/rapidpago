<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

		<style>

			body {
				font-family: ipamp;
		 		font-size: 10pt;
		 	}
		 	
		 	td { 
		 		vertical-align: middle; 
		 	}
		 	p{
		 		text-align: right;
		 		font-size: 8pt;
		 		line-height: 1.5;
		 	}
		 	.items td {
		 		/*border-left: 0.1mm solid #000000;
		 		border-right: 0.1mm solid #000000;*/
		 		border: 0.1mm solid #000000;
		 		font-size: 8pt;
		 		text-align: left;
		 		padding: 2pt;
		 	}
		 	.items td img {
		 		max-height: 300px;
		 		max-width: 300px
		 		width:100%;
		 		height: auto;
		 	}
		 	table thead td, table thead th, table tfoot td, table tfoot th { 
		 		
		 		font-size: 12pt;
		 		padding: 2pt;
		 	}
		 	.items td.blanktotal {
		 		background-color: #FFFFFF;
		 		border: 0mm none #000000;
		 		border-top: 0.1mm solid #000000;
		 	}
		 	.items td.totals {
		 		text-align: right;
		 		border: 0.1mm solid #000000;
		 	}
		 	.text-center{
		 		text-align: center;
		 	}
		 	.text-left{
		 		text-align: left;
		 	}
		 	.text-right{
		 		text-align: right;
		 	}
		 	.img-responsive{
		 		width: 100%;
		 		height: auto;
		 		max-width: 50px;
		 	}
		</style>
	</head>
	<body>
		 <table width="100%" style="border-collapse: collapse;" cellpadding="5">
	    	<thead>
	    		<tr>
	    			<th align="left">RAPIDPAGO C.A.</th>
	    		</tr>
	    	</thead>
	    	<tbody>
	    		<tr>
	    			<td height="900px" align="right">
	    				<img src="<?php echo Yii::app()->request->getBaseUrl(true)?>/assets/img/logo.png" >
	    			<p><span style=" font-size: 20pt"><b>DICCIONARIO DE DATOS PROYECTO SIGO</b></span></p>
	    			<p>En SmartWebTools desarrollamos soluciones tecnológicas de vanguardia,</p>
					<p>alineándose a los objetivos de su Empresa.</p>

	    			</td>
	    		</tr>
			</tbody>

			<tfoot>
				<tr>
	    			<td align="center"><b>Julio 2019</b></td>
	    		</tr>
	    	</tfoot>
		</table>
    </body>
 </html>
 <?php  ?>