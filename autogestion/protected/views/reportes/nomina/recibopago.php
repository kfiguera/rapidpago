<html>
	<head>
		<style>
			body {
				font-family: sans-serif;
		 		font-size: 10pt;
		 	}
		 	p { 
		 		margin: 0pt;
		 	}
		 	td { 
		 		vertical-align: bottom; 
		 	}
		 	.items, .detalle {
		 		border-collapse: collapse;
		 		width: 100%;
		 		margin-bottom: 10px;
		 	}
		 	.items td {
		 		/*border-left: 0.1mm solid #000000;
		 		border-right: 0.1mm solid #000000;*/
		 		border: 0.1mm solid #000000;
		 		font-size: 8pt;
		 		text-align: center;
		 		padding: 1pt;
		 	}
		 	.detalle td {
		 		border-left: 0.1mm solid #000000;
		 		border-right: 0.1mm solid #000000;
		 		font-size: 8pt;
		 		text-align: center;
		 		padding: 1pt;
		 	}
		 	table thead td, table thead th,table tfoot td, table tfoot th { 
		 		background-color: #EEEEEE;
		 		text-align: center;
		 		border: 0.1mm solid #000000;
		 		font-size: 8pt;
		 		padding: 2pt;
		 	}
		 	.items td.blanktotal, .detalle td.blanktotal {
		 		background-color: #FFFFFF;
		 		border: 0mm none #000000;
		 		border-top: 0.1mm solid #000000;
		 	}
		 	.items td.blankinicio, .detalle td.blankinicio {
		 		background-color: #FFFFFF;
		 		border: 0mm none #000000;
		 		border-bottom: 0.1mm solid #000000;
		 	}
		 	.items td.totals {
		 		text-align: right;
		 		border: 0.1mm solid #000000;
		 	}

		 	
		</style>
	</head>
	<body>
		<?php
            $sql="SELECT a.asign_codig, 
            		c.nacio_value,
            		c.perso_cedul, 
            		c.perso_pnomb, 
            		c.perso_papel,
            		d.perio_finic,
            		d.perio_ffina,
            		a.asing_dtrab,
            		a.nomin_codig,
            		a.perio_codig,
            		a.traba_codig,
            		(SELECT SUM(resum_monto) 
            			FROM nomina_resumen e 
            			WHERE  a.traba_codig=e.traba_codig 
            				and a.perio_codig=d.perio_codig 
            				and  a.nomin_codig=d.nomin_codig 
            				and e.conce_codig=1) monto,
            		d.perio_ano

                  FROM nomina_asignacion a 
                  JOIN p_trabajador b ON (a.traba_codig=b.traba_codig)
                  JOIN p_persona c ON (b.perso_codig = c.perso_codig)
                  JOIN nomina_periodo d ON (a.perio_codig=d.perio_codig)
                  WHERE a.asign_codig = '".$c."'";

            $connection=yii::app()->db;
            $command = $connection->createCommand($sql);
            $datos = $command->queryRow();
            $i=0;
        
        ?>
		<table class="items">
			<tr>
				<td width="25%">Nombre:</td>
				<td colspan="3"><?php echo $datos['perso_pnomb'].' '.$datos['perso_papel'] ?></td>
			</tr>
			<tr>
				<td>Cédula:</td>
				<td colspan="3"><?php echo $datos['nacio_value'].'-'.$this->funciones->transformarMonto_v($datos['perso_cedul'],0) ?></td>
			</tr>
			<tr>
				<td>Periodo desde:</td>
				<td><?php echo $this->funciones->transformarFecha_v($datos['perio_finic']) ?></td>
				<td>Hasta:</td>
				<td><?php echo $this->funciones->transformarFecha_v($datos['perio_ffina']) ?></td>
			</tr>
			<tr>
				<td>Dias Trabajados:</td>
				<td colspan="3"><?php echo $datos['asing_dtrab'] ?></td>
			</tr>
			<tr>
				<td>Salario Diario:</td>
				<td colspan="3"><?php echo $this->funciones->transformarMonto_v($datos['monto']/30,2)?></td>
			</tr>
			<tr>
				<td>Año:</td>
				<td colspan="3"><?php echo $datos['perio_ano'] ?></td>
			</tr>
		</table>
		<?php

		?>
		 <table class="items">
		 <thead>
		 	<tr>
		 		<th colspan="3">ASIGNACIONES</th>
		 	</tr>
		 <tr>
		 	<th width="1%">#</th>
			<th >DESCRIPCIÓN</th>
			<th width="20%">MONTO</th>
		 </tr>
		 </thead>
		 <tbody>
		 <?php
            $sql="SELECT * 
            	  FROM nomina_resumen a
            	  JOIN nomina_concepto b ON (a.nomin_codig=b.nomin_codig AND a.conce_codig = b.conce_codig)
            	  WHERE a.nomin_codig = '".$datos['nomin_codig']."'
            	    AND a.perio_codig = '".$datos['perio_codig']."'
            	    AND a.traba_codig = '".$datos['traba_codig']."'
            	    AND a.tconc_codig = '1'
            	    AND a.tconc_codig <> '5'
            	    AND resum_monto>0";
           	
            $connection=yii::app()->db;
            $command = $connection->createCommand($sql);
            $p_persona = $command->query();
            $i=0;
            while (($row = $p_persona->read()) !== false) {
                $i++;
            ?>
            <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $row['conce_descr'] ?></td>
                <td><?php echo $this->funciones->TransformarMonto_v($row['resum_monto'],2) ?></td>
            </tr>
            <?php
            $total['asign']+=$row['resum_monto'];
                }   
            ?>
		 <!-- FIN ITEMS -->
		 <tr>
		 	<td class="blanktotal" colspan="3"></td>
		 </tr>
		 </tbody>
		 <tfoot>
		 	<tr>
		 		<th colspan="2">TOTAL ASIGNACIONES</th>
		 		<th><?php echo $this->funciones->TransformarMonto_v($total['asign'],2) ?></th>
		 	</tr>
		 </tfoot>
		 </table>

		  <table class="items">
		 <thead>
		 	<tr>
		 		<th colspan="3">DEDUCCIONES</th>
		 	</tr>
		 <tr>
		 	<th width="1%">#</th>
			<th >DESCRIPCIÓN</th>
			<th width="20%">MONTO</th>
		 </tr>
		 </thead>
		 <tbody>
		 <?php
            $sql="SELECT * 
            	  FROM nomina_resumen a
            	  JOIN nomina_concepto b ON (a.nomin_codig=b.nomin_codig AND a.conce_codig = b.conce_codig)
            	  WHERE a.nomin_codig = '".$datos['nomin_codig']."'
            	    AND a.perio_codig = '".$datos['perio_codig']."'
            	    AND a.traba_codig = '".$datos['traba_codig']."'
            	    AND a.tconc_codig = '2'
            	    AND a.tconc_codig <> '5'";
           	
            $connection=yii::app()->db;
            $command = $connection->createCommand($sql);
            $p_persona = $command->query();
            $i=0;
            while (($row = $p_persona->read()) !== false) {
                $i++;
            ?>
            <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $row['conce_descr'] ?></td>
                <td><?php echo $this->funciones->TransformarMonto_v($row['resum_monto'],2) ?></td>
            </tr>
            <?php
            $total['deduc']+=$row['resum_monto'];
                }   
            ?>
		 <!-- FIN ITEMS -->
		 <tr>
		 	<td class="blanktotal" colspan="3"></td>
		 </tr>
		 </tbody>
		 <tfoot>
		 	<tr>
		 		<th colspan="2">TOTAL DEDUCCIONES</th>
		 		<th><?php echo $this->funciones->TransformarMonto_v($total['deduc'],2) ?></th>
		 	</tr>
		 	<tr>
		 		<td class="blanktotal" colspan="3"></td>
		 	</tr>
		 	<?php
		 	$total['gener']=$total['asign']-$total['deduc']
		 ?>
		 	<tr>
		 		<th colspan="2">TOTAL A PAGAR</th>
		 		<th width="20%"><?php echo $this->funciones->TransformarMonto_v($total['gener'],2) ?></th>
		 	</tr>
		 </tfoot>
		 </table>
		 
		  <table class="detalle" width="100%">
		 <tbody>
		 	 <tr>
		 		<td class="blankinicio" colspan="3"></td>
		 	</tr>
			<tr>
				<td  width='15%' rowspan="2">&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;<br>HUELLA DACTILAR</td>
				<td colspan="2">&nbsp;<br>RECIBO CONFORME: ____________________________________________________&nbsp;<br>&nbsp;<br> </td>
			</tr>
			<tr>

		 		<td colspan="2">&nbsp;<br>C.I.: ____________________________________________________&nbsp;<br>&nbsp;<br></td>

		 	</tr>
		 	<tr>
		 		<td class="blanktotal" colspan="3"></td>
		 	</tr>
		 

		 </tbody>
		 </table>
	 </body>
 </html>