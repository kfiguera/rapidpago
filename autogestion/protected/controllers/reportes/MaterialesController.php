<?php

class MaterialesController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->render('index');
	}
	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$condicion2='';
		$con=0;
		if($_POST['pedid']){
			$pedid=mb_strtoupper($_POST['pedid']);
			$condicion.=$condicion2."a.pedid_codig like '%".$pedid."%' ";
			$condicion2='AND ';
			$con++;
		}
		if($_POST['desde']){
			$desde=$this->funciones->TransformarFecha_bd($_POST['desde']);
			$condicion.=$condicion2."a.pedid_fcrea >= '".$desde."' ";
			$condicion2='AND ';
			$con++;
		}
		if($_POST['hasta']){
			$hasta=$this->funciones->TransformarFecha_bd($_POST['hasta']);
			$condicion.=$condicion2."a.pedid_fcrea <= '".$hasta."' ";
			$condicion2='AND ';
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function header($titulo){

		$html='<div name="myheader">
 				<table width="100%">
 					<tr>
 						<td width="25%" style="color:#0000BB">
 							<img src="'.Yii::app()->getBaseUrl(true).'/assets/img/logo-pdf.png" width="15%">
 						</td>
 						<td width="50%" style="text-align: center; vertical-align: bottom; ">
 							<b>'.$titulo.'</b>
 						</td>
 						<td width="25%" style="text-align: right; vertical-align: bottom;">
 							<b>Fecha: </b>'.date('d/m/Y').'<br>
 							<b>Hora: </b>'.date('h:i:s A').'<br>
 							<b>Usuario: </b>'.Yii::app()->user->id['usuario']['nombre'].'
 						</td>
 					<tr>
 				</table>
 				<div style="border-top: 1px solid #000000;">

 				</div>


 			</div>';
 		return $html;
	}
	public function footer(){
		$html='<div name="myfooter">
 					<div style="border-top: 1px solid #000000; font-size: 6pt; padding-top: 1mm; text-align: center;">
 						Página {PAGENO} de {nb}
 					</div>
 				</div>';
 		return $html;
	}

	public function actionPdf()
	{
		$condicion='';
		$condicion2='';
		$con=0;
		$titulo='Materiales por Pedidos<br>';

		if($_POST['pedid']){
			$pedid=mb_strtoupper($_POST['pedid']);
			$condicion.=$condicion2."a.pedid_codig like '%".$pedid."%' ";
			$condicion2='AND ';
			$sql="SELECT * FROM pedido_pedidos WHERE pedid_codig='".$pedid."'";
			$conexion=Yii::app()->db;
			$result=$conexion->createCommand($sql)->queryRow();
			$titulo.=' <b>Pedido:</b> '.$result['pedid_numer'].' <br>';
			$con++;
		}
		if($_POST['desde']){
			$desde=$this->funciones->TransformarFecha_bd($_POST['desde']);
			$condicion.=$condicion2."a.pedid_fcrea >= '".$desde."' ";
			$condicion2='AND ';
			$titulo.=' <b>Desde:</b> '.$_POST['desde'];
			$con++;
		}
		if($_POST['hasta']){
			$hasta=$this->funciones->TransformarFecha_bd($_POST['hasta']);
			$condicion.=$condicion2."a.pedid_fcrea <= '".$hasta."' ";
			$condicion2='AND ';
			$titulo.=' <b>Hasta:</b> '.$_POST['hasta'];
			$con++;
		}
		if($con>0){
			$condicion="AND ".$condicion;
		}

		$header=$this->header($titulo);
		$footer=$this->footer();
		$mPDF1 = Yii::app()->ePdf->mpdf('utf-8','LEGAL','','',10,10,29,13,9,9,'P'); 
 		$mPDF1->useOnlyCoreFonts = true;
 		$mPDF1->SetTitle("Materiales por Pedido | Polerones Tiempo");
 		$mPDF1->SetAuthor("Polerones Tiempo, Bordando Experiencias");
 		$mPDF1->SetHTMLHeader($header);
 		$mPDF1->SetHTMLFooter($footer);
 		$mPDF1->WriteHTML($this->renderpartial('pdf', array('condicion'=>$condicion), true)); //hacemos un render partial a una vista preparada, en este caso es la vista pdfReport
 		$mPDF1->Output('Material_Pedido_'.date('Ymd_His').'.pdf','I');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.	
	}
	
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}