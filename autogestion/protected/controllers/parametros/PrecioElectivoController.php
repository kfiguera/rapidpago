<?php
class PrecioElectivoController extends Controller
{
	public $funciones;

	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->render('index');
	}
	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descripcion']){
			$descripcion=mb_strtoupper($_POST['descripcion']);
			$condicion.="a.pelec_descr like '%".$descripcion."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM pedido_precio_electivo a
			  WHERE a.pelec_codig ='".$_GET['c']."'";
		$precio=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('precio' => $precio));
	}
	public function actionRegistrar()
	{
		if($_POST){
			
			$descr=mb_strtoupper($_POST['descr']);
			$preci=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['preci']));
			$adici=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['adici']));
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_precio_electivo WHERE pelec_descr = '".$descr."'";
				$precio=$conexion->createCommand($sql)->queryRow();
				if(!$precio){
					$sql="INSERT INTO pedido_precio_electivo(pelec_descr, pelec_preci, usuar_codig, pelec_fcrea, pelec_hcrea) VALUES ('".$descr."', '".$preci."', '".$usuar."', '".$fecha."', '".$hora."')";
					$res1=$conexion->createCommand($sql)->execute();
					if($res1){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Precio Electivo guardado correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al guardar El Precio Electivo');	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Precio Electivo ya existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codig=mb_strtoupper($_POST['codig']);
			$descr=mb_strtoupper($_POST['descr']);
			$preci=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['preci']));
			$adici=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['adici']));
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_precio_electivo WHERE pelec_codig ='".$codig."'";
				$precio=$conexion->createCommand($sql)->queryRow();
				if($precio){

					$sql="SELECT * FROM pedido_precio_electivo WHERE pelec_descr = '".$descr."' and pelec_preci = '".$preci."' and pelec_codig = '".$codig."'";
					$precio2=$conexion->createCommand($sql)->queryRow();
					
					if(!$precio2 or ($precio['pelec_codig']==$codig and $precio['pelec_preci']==$preci)){
							$sql="UPDATE pedido_precio_electivo
								  SET pelec_descr='".$descr."',
								      pelec_preci='".$preci."'		
								  WHERE pelec_codig='".$codig."'";
							$res1=$conexion->createCommand($sql)->execute();
							if($res1){
								$transaction->commit();
									$msg=array('success'=>'true','msg'=>'Precio Electivo actualizado correctamente');
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al actualizar El Precio Electivo');	
							}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Precio Electivo ya esta registrado');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Precio Electivo no existe');
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			
			$sql="SELECT * 
			  FROM pedido_precio_electivo a
			  WHERE a.pelec_codig ='".$_GET['c']."'";
			$precio=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('precio' => $precio));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				
					$sql="SELECT * FROM pedido_precio_electivo WHERE pelec_codig='".$codig."'";
					$precio=$conexion->createCommand($sql)->queryRow();
					if($precio){

						$sql="DELETE FROM pedido_precio_electivo WHERE pelec_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Precio Electivo eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Precio Electivo de Usuario');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Precio Electivo no existe');
					}
				
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_precio_electivo a
			  WHERE a.pelec_codig ='".$_GET['c']."'";
			$precio=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('precio' => $precio));
		}
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}