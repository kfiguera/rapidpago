<?php

class PaisesController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->redirect(Yii::app()->request->baseUrl.'/parametros/tipoempresa/listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descripcion']){
			$descripcion=mb_strtoupper($_POST['descripcion']);
			$condicion.="a.paise_descr like '%".$descripcion."%' ";
			$con++;
		}
		if($_POST['value']){
			if($con>0){
				$condicion.="AND ";
			}
			$value=mb_strtoupper($_POST['value']);
			$condicion.="a.paise_value like '%".$value."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM p_paises a
			  WHERE a.paise_codig ='".$_GET['c']."'";
		$roles=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('roles' => $roles));
	}
	public function actionRegistrar()
	{
		if($_POST){
			/*var_dump($_POST);
			exit();*/
			$descr=mb_strtoupper($_POST['descr']);
			$value=mb_strtoupper($_POST['value']);
			$cambi=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST['cambi']));
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM p_paises WHERE paise_descr = '".$descr."'";
				$roles=$conexion->createCommand($sql)->queryRow();
				if(!$roles){
					/*$sql="SELECT * FROM p_paises WHERE paise_value = '".$value."'";
					$roles=$conexion->createCommand($sql)->queryRow();
					if(!$roles){*/
						$sql="INSERT INTO p_paises(paise_descr,paise_value) 
							VALUES ('".$descr."','".$value."')";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Pais guardado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al guardar El Pais');	
						}
					/*}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Valor ya existe');
					}*/
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pais ya existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$descr=mb_strtoupper($_POST['descr']);
			$value=mb_strtoupper($_POST['value']);
			$cambi=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST['cambi']));

			if($contra==$ccontra){
				$conexion=Yii::app()->db;
				$transaction=$conexion->beginTransaction();
				try{
					$sql="SELECT * FROM p_paises WHERE paise_codig ='".$codig."'";
					$roles=$conexion->createCommand($sql)->queryRow();
					if($roles){
						$sql="SELECT * FROM p_paises WHERE paise_descr='".$descr."'";
						$rol=$conexion->createCommand($sql)->queryRow();
						$sql="SELECT * FROM p_paises WHERE paise_codig='".$codig."'";
						$p_paises=$conexion->createCommand($sql)->queryRow();
						if(!$rol or $p_paises['paise_descr']==$descr){
							$sql="SELECT * FROM p_paises WHERE paise_value = '".$value."'";
							$roles=$conexion->createCommand($sql)->queryRow();
							if(!$roles or $p_paises['paise_value']==$value){
								$sql="UPDATE p_paises
									  SET paise_descr='".$descr."',
									  paise_value='".$value."'
									  WHERE paise_codig='".$codig."'";
								$res1=$conexion->createCommand($sql)->execute();
								if($res1){
									$transaction->commit();
										$msg=array('success'=>'true','msg'=>'Pais actualizado correctamente');
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al actualizar El Pais');	
								}
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'El Valor ya existe');
							}
							
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'El Pais ya esta registrado');
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Pais no existe');
					}
				}catch(Exception $e){
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error al verificar la información');
				}
			}else{
				$msg=array('success'=>'false','msg'=>'La contraseña y su confirmación no son iguales');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			
			$sql="SELECT * 
			  FROM p_paises a
			  WHERE a.paise_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('roles' => $roles));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
					$sql="SELECT * FROM p_paises WHERE paise_codig='".$codig."'";
					$roles=$conexion->createCommand($sql)->queryRow();
					if($roles){

						$sql="DELETE FROM p_paises WHERE paise_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Pais eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Pais');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Pais no existe');
					}
					
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM p_paises a
			  WHERE a.paise_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('roles' => $roles));
		}
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}