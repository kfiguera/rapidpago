<?php
class ColegioController extends Controller
{
	public $funciones;

	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->render('index');
	}
	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descripcion']){
			$descripcion=mb_strtoupper($_POST['descripcion']);
			$condicion.="a.coleg_descr like '%".$descripcion."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM colegio a
			  WHERE a.coleg_codig ='".$_GET['c']."'";
		$colegio=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('colegio' => $colegio));
	}
	public function actionRegistrar()
	{
		if($_POST){
			$nombr=mb_strtoupper($_POST['nombr']);
			$direc=mb_strtoupper($_POST['direc']);
			$telef=mb_strtoupper($_POST['telef']);
			$pgweb=mb_strtoupper($_POST['pgweb']);
			$rede1=mb_strtoupper($_POST['rede1']);
			$rede2=mb_strtoupper($_POST['rede2']);
			$rede3=mb_strtoupper($_POST['rede3']);
			$obser=mb_strtoupper($_POST['obser']);
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM colegio WHERE coleg_nombr = '".$nombr."'";
				$colegio=$conexion->createCommand($sql)->queryRow();
				if(!$colegio){
					$sql="INSERT INTO colegio(coleg_nombr, coleg_direc, colec_telef, coleg_pgweb, coleg_rede1, coleg_rede2, coleg_rede3, coleg_obser, usuar_codig, coleg_fcrea, coleg_hcrea) VALUES ('".$nombr."', '".$direc."', '".$telef."', '".$pgweb."', '".$rede1."', '".$rede2."', '".$rede3."', '".$obser."', '".$usuar."', '".$fecha."', '".$hora."')";
					$res1=$conexion->createCommand($sql)->execute();
					if($res1){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Colegio guardado correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al guardar El Colegio');	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Colegio ya existe');
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codig=mb_strtoupper($_POST['codig']);
			$nombr=mb_strtoupper($_POST['nombr']);
			$direc=mb_strtoupper($_POST['direc']);
			$telef=mb_strtoupper($_POST['telef']);
			$pgweb=mb_strtoupper($_POST['pgweb']);
			$rede1=mb_strtoupper($_POST['rede1']);
			$rede2=mb_strtoupper($_POST['rede2']);
			$rede3=mb_strtoupper($_POST['rede3']);
			$obser=mb_strtoupper($_POST['obser']);
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM colegio WHERE coleg_codig ='".$codig."'";
				$colegio=$conexion->createCommand($sql)->queryRow();
				if($colegio){

					$sql="SELECT * FROM colegio WHERE coleg_nombr = '".$nombr."' and coleg_codig = '".$codig."'";
					$colegio2=$conexion->createCommand($sql)->queryRow();
					
					if(!$colegio2 or ($colegio['coleg_codig']==$codig )){
							$sql="UPDATE colegio
								  SET coleg_nombr = '".$nombr."',
									  coleg_direc = '".$direc."',
									  colec_telef = '".$telef."',
									  coleg_pgweb = '".$pgweb."',
									  coleg_rede1 = '".$rede1."',
									  coleg_rede2 = '".$rede2."',
									  coleg_rede3 = '".$rede3."',
									  coleg_obser = '".$obser."'		
								  WHERE coleg_codig='".$codig."'";
							$res1=$conexion->createCommand($sql)->execute();
							if($res1){
								$transaction->commit();
									$msg=array('success'=>'true','msg'=>'Colegio actualizado correctamente');
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al actualizar El Colegio');	
							}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Colegio ya esta registrado');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Colegio no existe');
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			
			$sql="SELECT * 
			  FROM colegio a
			  WHERE a.coleg_codig ='".$_GET['c']."'";
			$colegio=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('colegio' => $colegio));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				
					$sql="SELECT * FROM colegio WHERE coleg_codig='".$codig."'";
					$colegio=$conexion->createCommand($sql)->queryRow();
					if($colegio){

						$sql="DELETE FROM colegio WHERE coleg_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Colegio eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Colegio de Usuario');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Colegio no existe');
					}
				
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM colegio a
			  WHERE a.coleg_codig ='".$_GET['c']."'";
			$colegio=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('colegio' => $colegio));
		}
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}