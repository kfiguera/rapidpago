<?php

class MotivoController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->redirect(Yii::app()->request->baseUrl.'/parametros/motivo/listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descripcion']){
			$descripcion=mb_strtoupper($_POST['descripcion']);
			$condicion.="a.motiv_descr like '%".$descripcion."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM p_motivo a
			  WHERE a.motiv_codig ='".$_GET['c']."'";
		$roles=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('roles' => $roles));
	}
	public function actionRegistrar()
	{
		if($_POST){
			/*var_dump($_POST);
			exit();*/
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$descr=mb_strtoupper($_POST['descr']);
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM p_motivo WHERE motiv_descr = '".$descr."'";
				$roles=$conexion->createCommand($sql)->queryRow();
				if(!$roles){
					$sql="INSERT INTO p_motivo(motiv_descr,usuar_codig,motiv_fcrea,motiv_hcrea) 
						VALUES ('".$descr."','".$usuar."','".$fecha."','".$hora."')";
					$res1=$conexion->createCommand($sql)->execute();
					if($res1){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Motivo guardado correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al guardar el Motivo');	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El tipo de Unidad ya existe');
				}
			}catch(Exception $e){

					var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$descr=mb_strtoupper($_POST['descr']);
			if($contra==$ccontra){
				$conexion=Yii::app()->db;
				$transaction=$conexion->beginTransaction();
				try{
					$sql="SELECT * FROM p_motivo WHERE motiv_codig ='".$codig."'";
					$roles=$conexion->createCommand($sql)->queryRow();
					if($roles){
						$sql="SELECT * FROM p_motivo WHERE motiv_descr='".$descr."'";
						$rol=$conexion->createCommand($sql)->queryRow();
						if(!$rol){
								$sql="UPDATE p_motivo
								  SET motiv_descr='".$descr."' 
								  WHERE motiv_codig='".$codig."'";
							$res1=$conexion->createCommand($sql)->execute();
							if($res1){
								$transaction->commit();
									$msg=array('success'=>'true','msg'=>'Motivo actualizado correctamente');
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al actualizar el Motivo');	
							}
							
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'El Motivo ya esta registrado');
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Motivo no existe');
					}
				}catch(Exception $e){
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error al verificar la información');
				}
			}else{
				$msg=array('success'=>'false','msg'=>'La contraseña y su confirmación no son iguales');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			
			$sql="SELECT * 
			  FROM p_motivo a
			  WHERE a.motiv_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('roles' => $roles));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM inventario WHERE motiv_codig='".$codig."'";
				$inven=$conexion->createCommand($sql)->queryRow();
				if(!$inven){
					$sql="SELECT * FROM p_motivo WHERE motiv_codig='".$codig."'";
					$roles=$conexion->createCommand($sql)->queryRow();
					if($roles){

						$sql="DELETE FROM p_motivo WHERE motiv_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Motivo eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Tipo de Usuario');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Motivo no existe');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No se puede eliminar debido a, que esta asociado a un producto');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM p_motivo a
			  WHERE a.motiv_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('roles' => $roles));
		}
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}