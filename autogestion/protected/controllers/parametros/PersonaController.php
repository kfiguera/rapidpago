<?php

class PersonaController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		//$this->render('index');
		$this->redirect('listado');
	}
	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['p_nacionalidad']){
			$condicion.="a.nacio_value = '".$_POST['p_nacionalidad']."' ";
			$con++;
		}
		if($_POST['cedula']){
			$cedula = $this->funciones->TransformarMonto_bd($_POST['cedula']);
			if($con>0){
				$condicion.="AND ";
			}
			$condicion.="a.perso_cedul = '".$cedula."' ";
			$con++;
		}
		if($_POST['nombre']){
			$nombre=mb_strtoupper($_POST['nombre']);
			if($con>0){
				$condicion.="AND ";
			}
			$condicion.="a.perso_pnomb like '%".$nombre."%' or a.perso_snomb like '%".$nombre."%' ";
			$con++;
		}
		if($_POST['apellido']){
			$apellido=mb_strtoupper($_POST['apellido']);
			if($con>0){
				$condicion.="AND ";
			}
			$condicion.="a.perso_papel like '%".$apellido."%' or a.perso_sapel like '%".$apellido."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{

		$conexion=Yii::app()->db;
		$sql="SELECT * 
			FROM p_persona a
			WHERE a.perso_codig ='".$_GET['c']."'";
		$p_persona=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('p_persona' => $p_persona));
		
	}
	public function actionRegistrar()
	{
		if($_POST){

			//var_dump($_POST);
			$nacio = $_POST['nacio'];
			$cedul = $this->funciones->TransformarMonto_bd($_POST['cedula']);
			$perso_pnomb = mb_strtoupper($_POST['pnomb']);
			$perso_snomb = mb_strtoupper($_POST['snomb']);
			$perso_papel = mb_strtoupper($_POST['papel']);
			$perso_sapel = mb_strtoupper($_POST['sapel']);
			$fecha=explode('/', $_POST['fnaci']);
            $perso_fnaci=$this->funciones->TransformarFecha_bd($_POST['fnaci']);
            $const_fdesd=$this->funciones->TransformarFecha_bd($_POST['desde']);
            $p_persona['const_fdesd'];
            $ecivi = mb_strtoupper($_POST['ecivi']);
            $obser = mb_strtoupper($_POST['obser']);
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM p_persona WHERE nacio_value ='".$nacio."' and perso_cedul='".$cedul."'";
				$p_persona=$conexion->createCommand($sql)->queryRow();
				if(!$p_persona){
					$sql="INSERT INTO p_persona(nacio_value, perso_cedul, perso_pnomb, perso_snomb, perso_papel, perso_sapel, perso_fnaci, gener_value, ecivi_value, perso_obser) 
						VALUES ('".$_POST['nacio']."','".$_POST['cedula']."','".$perso_pnomb."','".$perso_snomb."','".$perso_papel."','".$perso_sapel."','".$perso_fnaci."','".$_POST['gener']."','".$ecivi."','".$obser."')";
					$res1=$conexion->createCommand($sql)->execute();
					if($res1){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Persona guardada correctamente');	
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al guardar la persona');	
					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'La persona ya existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información','e'=>$e);
			}
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){

			//var_dump($_POST);
			$codig = $_POST['codig'];
			$nacio = $_POST['nacio'];
			$cedul = $this->funciones->TransformarMonto_bd($_POST['cedula']);
			$perso_pnomb = mb_strtoupper($_POST['pnomb']);
			$perso_snomb = mb_strtoupper($_POST['snomb']);
			$perso_papel = mb_strtoupper($_POST['papel']);
			$perso_sapel = mb_strtoupper($_POST['sapel']);
            $perso_fnaci=$this->funciones->TransformarFecha_bd($_POST['fnaci']);
            $p_persona['const_fdesd'];
            $const_fdesd=$this->funciones->TransformarFecha_bd($_POST['desde']);
            $ecivi = mb_strtoupper($_POST['ecivi']);
            $obser = mb_strtoupper($_POST['obser']);
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM p_persona WHERE perso_codig='".$codig."' and nacio_value ='".$nacio."' and perso_cedul='".$cedul."'";
				$p_persona=$conexion->createCommand($sql)->queryRow();
				if($p_persona){
					
					$sql="UPDATE p_persona SET nacio_value='".$_POST['nacio']."',
							perso_pnomb='".$perso_pnomb."',
							perso_snomb='".$perso_snomb."',
							perso_papel='".$perso_papel."',
							perso_sapel='".$perso_sapel."',
							perso_fnaci='".$perso_fnaci."',
							gener_value='".$_POST['gener']."',
							ecivi_value='".$ecivi."',
							perso_obser='".$obser."'
							WHERE perso_codig='".$codig."'";
					$res1=$conexion->createCommand($sql)->execute();
					if($res1){
						$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Persona actualizar correctamente');
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al actualizar la persona');	
					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'La persona no existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información','e'=>$e);
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
				FROM p_persona a
				WHERE a.perso_codig ='".$_GET['c']."'";
			$p_persona=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('p_persona' => $p_persona));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM seguridad_usuarios WHERE perso_codig='".$codig."'";
				$usuario=$conexion->createCommand($sql)->queryRow();
				if(!$usuario){
					$sql="SELECT * FROM p_trabajador WHERE perso_codig='".$codig."'";
					$p_trabajador=$conexion->createCommand($sql)->queryRow();
					if(!$p_trabajador){
						$sql="SELECT * FROM p_persona WHERE perso_codig='".$codig."'";
						$p_persona=$conexion->createCommand($sql)->queryRow();
						if($p_persona){
							//echo $sql;
							$sql="DELETE FROM p_persona WHERE perso_codig='".$codig."'";
							$res2=$conexion->createCommand($sql)->execute();
							//var_dump($res1);
							if($res2){
								$transaction->commit();
								$msg=array('success'=>'true','msg'=>'Persona eliminada correctamente');	
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al eliminar la persona');	
							}
							
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'La persona no existe');
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'No se puede eliminar la persona, ya que esta asociada como p_trabajador');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No se puede eliminar la persona, ya que esta asociada como usuario');
				}
			}catch(CDbException $e){
				
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información','e'=>$e);
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
				FROM p_persona a
				WHERE a.perso_codig ='".$_GET['c']."'";
			$p_persona=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('p_persona' => $p_persona));
		}
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}