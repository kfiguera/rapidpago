<?php

class DeduccionController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->redirect('listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descripcion']){
			$descripcion=mb_strtoupper($_POST['descripcion']);
			$condicion.="a.deduc_descr like '%".$descripcion."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM pedido_deduccion a
			  WHERE a.deduc_codig ='".$_GET['c']."'";
			/*                                  var_dump($costo);
exit;*/
		$costo=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('costo' => $costo));
	}
	public function actionRegistrar()
	{
		if($_POST){
			/*var_dump($_POST);
			exit();*/
			$descr=mb_strtoupper($_POST['descr']);
			$conexion=Yii::app()->db;
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_deduccion WHERE deduc_descr = '".$descr."'";
				$costo=$conexion->createCommand($sql)->queryRow();
				if(!$costo){
					$sql="INSERT INTO pedido_deduccion(deduc_descr,usuar_codig,deduc_fcrea,deduc_hcrea) 
						VALUES ('".$descr."','".$usuar."','".$fecha."','".$hora."')";
					$res1=$conexion->createCommand($sql)->execute();
					if($res1){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Deducción guardado correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al guardar el Deducción');	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Deducción ya existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$descr=mb_strtoupper($_POST['descr']);
			if($contra==$ccontra){
				$conexion=Yii::app()->db;
				$transaction=$conexion->beginTransaction();
				try{
					$sql="SELECT * FROM pedido_deduccion WHERE deduc_codig ='".$codig."'";
					$costo=$conexion->createCommand($sql)->queryRow();
					if($costo){
						$sql="SELECT * FROM pedido_deduccion WHERE deduc_descr='".$descr."'";
						$rol=$conexion->createCommand($sql)->queryRow();
						if(!$rol){
								$sql="UPDATE pedido_deduccion
								  SET deduc_descr='".$descr."' 
								  WHERE deduc_codig='".$codig."'";
							$res1=$conexion->createCommand($sql)->execute();
							if($res1){
								$transaction->commit();
									$msg=array('success'=>'true','msg'=>'Deducción actualizado correctamente');
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al actualizar el Deducción');	
							}
							
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'El Deducción ya esta registrado');
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Deducción no existe');
					}
				}catch(Exception $e){
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error al verificar la información');
				}
			}else{
				$msg=array('success'=>'false','msg'=>'La contraseña y su confirmación no son iguales');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_deduccion a
			  WHERE a.deduc_codig ='".$_GET['c']."'";
			$costo=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('costo' => $costo));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM seguridad_usuarios WHERE deduc_codig='".$codig."'";
				$usuario=$conexion->createCommand($sql)->queryRow();
				if(!$usuario){
					$sql="SELECT * FROM pedido_deduccion WHERE deduc_codig='".$codig."'";
					$usuario=$conexion->createCommand($sql)->queryRow();
					if($usuario){

						$sql="DELETE FROM pedido_deduccion WHERE deduc_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Deducción eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar Deducción');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Deducción no existe');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No se puede eliminar, debido a aqe esta asociado con un usuario');
				}	
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_deduccion a
			  WHERE a.deduc_codig ='".$_GET['c']."'";
			$costo=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('costo' => $costo));
		}
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}