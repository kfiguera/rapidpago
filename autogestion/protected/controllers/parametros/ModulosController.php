<?php

class ModulosController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->redirect(Yii::app()->request->baseUrl.'/parametros/tipoempresa/listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descripcion']){
			$descripcion=mb_strtoupper($_POST['descripcion']);
			$condicion.="a.modul_descr like '%".$descripcion."%' ";
			$con++;
		}
		if($_POST['value']){
			if($con>0){
				$condicion.="AND ";
			}
			$value=mb_strtoupper($_POST['value']);
			$condicion.="a.modul_value like '%".$value."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM seguridad_modulos a
			  WHERE a.modul_codig ='".$_GET['c']."'";
		$roles=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('roles' => $roles));
	}
	public function actionRegistrar()
	{
		if($_POST){
			$codig=mb_strtoupper($_POST['codig']);
			$padre=mb_strtoupper($_POST['padre']);
			$descr=$_POST['descr'];
			$ruta=$_POST['ruta'];
			$orden=mb_strtoupper($_POST['orden']);
			$estat=mb_strtoupper($_POST['estat']);
			$super=mb_strtoupper($_POST['super']);
			$icono=$_POST['icono'];
			$obser=mb_strtoupper($_POST['obser']);
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM seguridad_modulos WHERE modul_codig = '".$codig."'";
				$roles=$conexion->createCommand($sql)->queryRow();
				if(!$roles){
					$sql="INSERT INTO seguridad_modulos(modul_codig, modul_padre, modul_descr, modul_ruta, modul_orden, modul_bborr, modul_bpadr, modul_obser, modul_icono) VALUES ('".$codig."', '".$padre."', '".$descr."', '".$ruta."', '".$orden."', '".$estat."', '".$super."', '".$obser."', '".$icono."')";
					$res1=$conexion->createCommand($sql)->execute();
					if($res1){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Modulo guardado correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al guardar El Modulo');	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Modulo ya existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codigo=mb_strtoupper($_POST['codigo']);
			$codig=mb_strtoupper($_POST['codig']);
			$padre=mb_strtoupper($_POST['padre']);
			$descr=$_POST['descr'];
			$ruta=$_POST['ruta'];
			$orden=mb_strtoupper($_POST['orden']);
			$estat=mb_strtoupper($_POST['estat']);
			$super=mb_strtoupper($_POST['super']);
			$icono=$_POST['icono'];
			$obser=mb_strtoupper($_POST['obser']);
			if($contra==$ccontra){
				$conexion=Yii::app()->db;
				$transaction=$conexion->beginTransaction();
				try{
					$sql="SELECT * FROM seguridad_modulos WHERE modul_codig ='".$codigo."'";
					$roles=$conexion->createCommand($sql)->queryRow();
					if($roles){

						$sql="SELECT * FROM seguridad_modulos WHERE modul_codig='".$codig."'";
						$rol=$conexion->createCommand($sql)->queryRow();
						$sql="SELECT * FROM seguridad_modulos WHERE modul_codig='".$codigo."'";
						$modulos=$conexion->createCommand($sql)->queryRow();
						if(!$rol or $modulos['modul_codig']==$codig){
								$sql="UPDATE seguridad_modulos
									  SET modul_codig='".$codig."',
										modul_padre='".$padre."',
										modul_descr='".$descr."',
										modul_ruta='".$ruta."',
										modul_orden='".$orden."',
										modul_bborr='".$estat."',
										modul_bpadr='".$super."',
										modul_obser='".$obser."',
										modul_icono='".$icono."'   
									  WHERE modul_codig='".$codigo."'";
								$res1=$conexion->createCommand($sql)->execute();
								if($res1){
									$transaction->commit();
										$msg=array('success'=>'true','msg'=>'Modulo actualizado correctamente');
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al actualizar El Modulo');	
								}
							
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'El Modulo ya esta registrado');
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Modulo no existe');
					}
				}catch(Exception $e){
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error al verificar la información');
				}
			}else{
				$msg=array('success'=>'false','msg'=>'El contraseña y su confirmación no son iguales');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			
			$sql="SELECT * 
			  FROM seguridad_modulos a
			  WHERE a.modul_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('roles' => $roles));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				
					$sql="SELECT * FROM seguridad_modulos WHERE modul_codig='".$codig."'";
					$roles=$conexion->createCommand($sql)->queryRow();
					if($roles){

						$sql="DELETE FROM seguridad_modulos WHERE modul_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Modulo eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Tipo de Usuario');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Modulo no existe');
					}
				
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM seguridad_modulos a
			  WHERE a.modul_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('roles' => $roles));
		}
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}