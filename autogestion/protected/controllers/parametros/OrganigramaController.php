<?php

class OrganigramaController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->redirect(Yii::app()->request->baseUrl.'/parametros/tipoempresa/listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descripcion']){
			$descripcion=mb_strtoupper($_POST['descripcion']);
			$condicion.="a.organ_descr like '%".$descripcion."%' ";
			$con++;
		}
		if($_POST['value']){
			if($con>0){
				$condicion.="AND ";
			}
			$value=mb_strtoupper($_POST['value']);
			$condicion.="a.organ_value like '%".$value."%' ";
			$con++;
		}
		if($con>0){
			$condicion="AND ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM p_organigrama a
			  WHERE a.organ_codig ='".$_GET['c']."'";
		$roles=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('roles' => $roles));
	}
	public function actionRegistrar()
	{
		if($_POST){
			/*var_dump($_POST);
			exit();*/
			$descr=mb_strtoupper($_POST['descr']);
			$padre=mb_strtoupper($_POST['padre']);
			$nivel=mb_strtoupper($_POST['nivel']);

			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM p_organigrama WHERE organ_descr = '".$descr."'";
				$p_organigrama=$conexion->createCommand($sql)->queryRow();
				if(!$p_organigrama){
					$sql="SELECT * FROM p_organigrama WHERE organ_codig = '".$padre."'";
					$superior=$conexion->createCommand($sql)->queryRow();
					if($superior){
						$sql="INSERT INTO p_organigrama(organ_descr,organ_padre,onive_codig) 
							VALUES ('".$descr."','".$padre."','".$nivel."')";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Organigrama guardado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al guardar El Organigrama');	
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Organigrama no existe');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Area ya existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$padre=mb_strtoupper($_POST['padre']);
			$nivel=mb_strtoupper($_POST['nivel']);
			$descr=mb_strtoupper($_POST['descr']);

			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM p_organigrama WHERE organ_codig ='".$codig."'";
				$p_organigrama=$conexion->createCommand($sql)->queryRow();
				if($p_organigrama){
					$sql="SELECT * FROM p_organigrama WHERE organ_descr='".$descr."'";
					$rol=$conexion->createCommand($sql)->queryRow();

					if(!$rol or ($p_organigrama['organ_descr']==$descr and $p_organigrama['organ_padre']!=$padre) or ($p_organigrama['organ_descr']==$descr and $p_organigrama['onive_codig']!=$nivel)){
						
						$sql="UPDATE p_organigrama
							  SET organ_descr='".$descr."',
							  organ_padre='".$padre."',
							  onive_codig='".$nivel."'
							  WHERE organ_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
								$msg=array('success'=>'true','msg'=>'Organigrama actualizado correctamente');
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar El Organigrama');	
						}
						
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Organigrama ya esta registrado');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Organigrama no existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			
			$sql="SELECT * 
			  FROM p_organigrama a
			  WHERE a.organ_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('roles' => $roles));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
					$sql="SELECT * FROM p_organigrama WHERE organ_codig='".$codig."'";
					$roles=$conexion->createCommand($sql)->queryRow();
					if($roles){

						$sql="DELETE FROM p_organigrama WHERE organ_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Organigrama eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Organigrama');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Organigrama no existe');
					}
					
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM p_organigrama a
			  WHERE a.organ_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('roles' => $roles));
		}
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}