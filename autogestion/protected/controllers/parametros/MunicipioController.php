<?php

class MunicipioController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->redirect(Yii::app()->request->baseUrl.'/parametros/tipoempresa/listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descripcion']){
			$descripcion=mb_strtoupper($_POST['descripcion']);
			$condicion.="a.munic_descr like '%".$descripcion."%' ";
			$con++;
		}
		if($_POST['value']){
			if($con>0){
				$condicion.="AND ";
			}
			$value=mb_strtoupper($_POST['value']);
			$condicion.="a.munic_value like '%".$value."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM p_municipio a
			  WHERE a.munic_codig ='".$_GET['c']."'";
		$roles=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('roles' => $roles));
	}
	public function actionRegistrar()
	{
		if($_POST){
			/*var_dump($_POST);
			exit();*/
			$descr=mb_strtoupper($_POST['descr']);
			$paise=mb_strtoupper($_POST['paise']);
			$estad=mb_strtoupper($_POST['estad']);

			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM p_municipio WHERE munic_descr = '".$descr."'";
				$p_municipio=$conexion->createCommand($sql)->queryRow();
				if(!$p_municipio){
					$sql="SELECT * FROM p_paises WHERE paise_codig = '".$paise."'";
					$pais=$conexion->createCommand($sql)->queryRow();
					if($pais){
						$sql="SELECT * FROM p_estados WHERE estad_codig = '".$estad."'";
						$p_estados=$conexion->createCommand($sql)->queryRow();
						if($p_estados){
							$sql="INSERT INTO p_municipio(munic_descr,paise_codig,estad_codig) 
								VALUES ('".$descr."','".$paise."','".$estad."')";
							$res1=$conexion->createCommand($sql)->execute();
							if($res1){
								$transaction->commit();
								$msg=array('success'=>'true','msg'=>'Municipio guardado correctamente');	
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al guardar El Municipio');	
							}
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'El Municipio no existe');
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Pais no existe');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Municipio ya existe');
				}
			}catch(Exception $e){

				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$descr=mb_strtoupper($_POST['descr']);
			$paise=mb_strtoupper($_POST['paise']);
			$estad=mb_strtoupper($_POST['estad']);
			
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM p_municipio WHERE munic_codig ='".$codig."'";
				$p_municipio=$conexion->createCommand($sql)->queryRow();
				if($p_municipio){
					$sql="SELECT * FROM p_municipio WHERE munic_descr='".$descr."'";
					$rol=$conexion->createCommand($sql)->queryRow();
					
					$sql="SELECT * FROM p_paises WHERE paise_codig='".$paise."'";
					$pais=$conexion->createCommand($sql)->queryRow();

					$sql="SELECT * FROM p_estados WHERE estad_codig='".$estad."'";
					$p_estados=$conexion->createCommand($sql)->queryRow();
					if(!$rol or ($p_municipio['munic_descr']==$descr and $p_municipio['paise_codig']!=$paise) or ($p_municipio['munic_descr']==$descr and $p_municipio['estad_codig']!=$estad)){
						
							$sql="UPDATE p_municipio
								  SET munic_descr='".$descr."',
								  paise_codig='".$paise."',
								  estad_codig='".$estad."'
								  WHERE munic_codig='".$codig."'";

							$res1=$conexion->createCommand($sql)->execute();
							if($res1){
								$transaction->commit();
									$msg=array('success'=>'true','msg'=>'Municipio actualizado correctamente');
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al actualizar El Municipio');	
							}
						
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Municipio ya esta registrado');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Municipio no existe');
				}
				}catch(Exception $e){
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error al verificar la información');
				}
			
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			
			$sql="SELECT * 
			  FROM p_municipio a
			  WHERE a.munic_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('roles' => $roles));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
					$sql="SELECT * FROM p_municipio WHERE munic_codig='".$codig."'";
					$roles=$conexion->createCommand($sql)->queryRow();
					if($roles){

						$sql="DELETE FROM p_municipio WHERE munic_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Municipio eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Municipio');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Municipio no existe');
					}
					
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM p_municipio a
			  WHERE a.munic_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('roles' => $roles));
		}
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}