<?php

class PermisosNivelController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->redirect(Yii::app()->request->baseUrl.'/parametros/tipoempresa/listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descripcion']){
			$descripcion=mb_strtoupper($_POST['descripcion']);
			$condicion.="a.spniv_descr like '%".$descripcion."%' ";
			$con++;
		}
		if($_POST['value']){
			if($con>0){
				$condicion.="AND ";
			}
			$value=mb_strtoupper($_POST['value']);
			$condicion.="a.spniv_value like '%".$value."%' ";
			$con++;
		}
		if($con>0){
			$condicion="AND ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM seguridad_permisos_nivel a
			  WHERE a.spniv_codig ='".$_GET['c']."'";
		$roles=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('roles' => $roles));
	}
	public function actionRegistrar()
	{
		if($_POST){
			/*var_dump($_POST);
			exit();*/
			$descr=mb_strtoupper($_POST['descr']);

			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM seguridad_permisos_nivel WHERE spniv_descr = '".$descr."'";
				$seguridad_permisos_nivel=$conexion->createCommand($sql)->queryRow();
				if(!$seguridad_permisos_nivel){
					
						$sql="INSERT INTO seguridad_permisos_nivel(spniv_descr) 
							VALUES ('".$descr."')";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Permisos Nivel guardado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al guardar El Permisos Nivel');	
						}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Area ya existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$descr=mb_strtoupper($_POST['descr']);

			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM seguridad_permisos_nivel WHERE spniv_codig ='".$codig."'";
				$seguridad_permisos_nivel=$conexion->createCommand($sql)->queryRow();
				if($seguridad_permisos_nivel){
					$sql="SELECT * FROM seguridad_permisos_nivel WHERE spniv_descr='".$descr."'";
					$rol=$conexion->createCommand($sql)->queryRow();

					if(!$rol or ($seguridad_permisos_nivel['spniv_descr']==$descr )){
						
						$sql="UPDATE seguridad_permisos_nivel
							  SET spniv_descr='".$descr."'
							  WHERE spniv_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
								$msg=array('success'=>'true','msg'=>'Permisos Nivel actualizado correctamente');
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar El Permisos Nivel');	
						}
						
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Permisos Nivel ya esta registrado');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Permisos Nivel no existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			
			$sql="SELECT * 
			  FROM seguridad_permisos_nivel a
			  WHERE a.spniv_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('roles' => $roles));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
					$sql="SELECT * FROM seguridad_permisos_nivel WHERE spniv_codig='".$codig."'";
					$roles=$conexion->createCommand($sql)->queryRow();
					if($roles){

						$sql="DELETE FROM seguridad_permisos_nivel WHERE spniv_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Permisos Nivel eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Permisos Nivel');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Permisos Nivel no existe');
					}
					
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM seguridad_permisos_nivel a
			  WHERE a.spniv_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('roles' => $roles));
		}
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}