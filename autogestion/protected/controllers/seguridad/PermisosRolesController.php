<?php

class PermisosRolesController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->redirect(Yii::app()->request->baseUrl.'/parametros/tipoempresa/listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descripcion']){
			$descripcion=mb_strtoupper($_POST['descripcion']);
			$condicion.="a.sprol_descr like '%".$descripcion."%' ";
			$con++;
		}
		if($_POST['value']){
			if($con>0){
				$condicion.="AND ";
			}
			$value=mb_strtoupper($_POST['value']);
			$condicion.="a.sprol_value like '%".$value."%' ";
			$con++;
		}
		if($con>0){
			$condicion="AND ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM seguridad_permisos_roles a
			  WHERE a.sprol_codig ='".$_GET['c']."'";
		$roles=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('roles' => $roles));
	}
	public function actionRegistrar()
	{
		if($_POST){

			$permi=$_POST['permi'];
			$permiso['ingre']=0;
			$permiso['modif']=0;
			$permiso['elimi']=0;
			$permiso['consu']=0;
			$usuar=mb_strtoupper($_POST['usuar']);
			$modul=mb_strtoupper($_POST['modul']);
			$spacc=mb_strtoupper($_POST['spacc']);
			$spniv=mb_strtoupper($_POST['spniv']);
			foreach ($permi as $key => $value) {
				switch ($value) {
					case '0':
						$permiso['consu']='1';
						break;
					case '1':
						$permiso['ingre']='1';
						break;
					case '2':
						$permiso['modif']='1';
						break;
					case '3':
						$permiso['elimi']='1';
						break;
					default:
						# code...
						break;
				}
			}
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM seguridad_permisos_roles WHERE srole_codig = '".$usuar."' AND modul_codig = '".$modul."' ";
				$seguridad_permisos_roles=$conexion->createCommand($sql)->queryRow();
				if(!$seguridad_permisos_roles){
					
						$sql="INSERT INTO seguridad_permisos_roles(srole_codig,modul_codig,spacc_codig,sprol_consu,sprol_ingre,sprol_modif,sprol_elimi,spniv_codig) 
							VALUES ('".$usuar."','".$modul."','".$spacc."','".$permiso['consu']."','".$permiso['ingre']."','".$permiso['modif']."','".$permiso['elimi']."','".$spniv."')";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Permisos Acceso guardado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al guardar El Permisos Acceso');	
						}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Area ya existe');
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$permi=$_POST['permi'];
			$permiso['ingre']=0;
			$permiso['modif']=0;
			$permiso['elimi']=0;
			$permiso['consu']=0;
			$usuar=mb_strtoupper($_POST['usuar']);
			$modul=mb_strtoupper($_POST['modul']);
			$spacc=mb_strtoupper($_POST['spacc']);
			$spniv=mb_strtoupper($_POST['spniv']);
			foreach ($permi as $key => $value) {
				switch ($value) {
					case '0':
						$permiso['consu']='1';
						break;
					case '1':
						$permiso['ingre']='1';
						break;
					case '2':
						$permiso['modif']='1';
						break;
					case '3':
						$permiso['elimi']='1';
						break;
					default:
						# code...
						break;
				}
			}
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM seguridad_permisos_roles WHERE sprol_codig ='".$codig."'";
				$seguridad_permisos_roles=$conexion->createCommand($sql)->queryRow();
				if($seguridad_permisos_roles){
					/*$sql="SELECT * FROM seguridad_permisos_roles WHERE sprol_descr='".$descr."'";
					$rol=$conexion->createCommand($sql)->queryRow();

					if(!$rol or ($seguridad_permisos_roles['sprol_descr']==$descr )){
					*/	
						
						$sql="UPDATE seguridad_permisos_roles
							  SET srole_codig = '".$usuar."',
									modul_codig = '".$modul."',
									spacc_codig = '".$spacc."',
									sprol_consu = '".$permiso['consu']."',
									sprol_ingre = '".$permiso['ingre']."',
									sprol_modif = '".$permiso['modif']."',
									sprol_elimi = '".$permiso['elimi']."',
									spniv_codig = '".$spniv."'
							  WHERE sprol_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
								$msg=array('success'=>'true','msg'=>'Permisos Acceso actualizado correctamente');
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar El Permisos Acceso');	
						}
						
						
					/*}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Permisos Acceso ya esta registrado');
					}*/
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Permisos Acceso no existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			
			$sql="SELECT * 
			  FROM seguridad_permisos_roles a
			  WHERE a.sprol_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('roles' => $roles));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
					$sql="SELECT * FROM seguridad_permisos_roles WHERE sprol_codig='".$codig."'";
					$roles=$conexion->createCommand($sql)->queryRow();
					if($roles){

						$sql="DELETE FROM seguridad_permisos_roles WHERE sprol_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Permisos Acceso eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Permisos Acceso');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Permisos Acceso no existe');
					}
					
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM seguridad_permisos_roles a
			  WHERE a.sprol_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('roles' => $roles));
		}
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}