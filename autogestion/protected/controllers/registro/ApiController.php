<?php

class ApiController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		

	}
	public function actionIndex()
	{
		$this->redirect(Yii::app()->request->baseUrl.'/Registro/listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionVerificarAcceso() {
        if ($_POST) {
            $post=$_POST;
            $codigo=$_POST['codigo'];
            $accio=mb_strtoupper($_POST['accio']);
            $motiv=mb_strtoupper($_POST['motiv']);
            $estat=1;
            $passw=$_POST['passw'];
            $conexion=Yii::app()->db;
            $transaction=$conexion->beginTransaction();

            try{
                $sql="SELECT * FROM seguridad_usuarios WHERE usuar_login ='".$codigo."'";
                $roles=$conexion->createCommand($sql)->queryRow();
                if($roles){

                    if($roles['uesta_codig']=='2'){
                        if($accio=='1'){
                            $sql="UPDATE seguridad_usuarios 
                                  SET uesta_codig = '".$estat."',
                                      usuar_passw= md5('".$passw."')
                                  WHERE usuar_codig ='".$roles['usuar_codig']."'";
                            $res1=$conexion->createCommand($sql)->execute();     
                            if($res1){

                                $msg=array('success'=>'true','msg'=>'Usuario aprobado correctamente', 'sql'=>$sql);
                            }else{
                                $transaction->rollBack();
                                $msg=array('success'=>'false','msg'=>'Error al aprobar el usuario');
                            }
                        }else{
                            $sql="DELETE FROM rd_acceso
                                  WHERE acces_corre ='".$codigo."'";
                            $res2=$conexion->createCommand($sql)->execute();     
                            if($res2){
                                $sql="DELETE FROM seguridad_usuarios 
                                      WHERE usuar_codig ='".$roles['usuar_codig']."'";
                                $res3=$conexion->createCommand($sql)->execute();
                                if($res3){
                                    $estat=2;
                                    $msg=array('success'=>'true','msg'=>'Usuario rechazado correctamente');
                                }else{
                                    $transaction->rollBack();
                                    $msg=array('success'=>'false','msg'=>'Error al eliminar el usuario');
                                }
                            }else{
                                $transaction->rollBack();
                                $msg=array('success'=>'false','msg'=>'Error al eliminar el registro');
                            }
                        }
                        
                        
                        if($msg['success']=='true'){
                            $sql="SELECT * 
                                      FROM p_persona 
                                      WHERE perso_codig='".$roles['perso_codig']."'";
                                $p_persona=$conexion->createCommand($sql)->queryRow();
                                $vista='../registro/acceso/verificar_correo';
                                $datos['p_persona']=$p_persona;
                                $datos['estatus']=$estat;
                                $datos['usuario']=$roles['usuar_login'];
                                $datos['clave']=$passw;
                                $datos['motivo']=$motiv;
                                $asunto='RapidPago | Solicitud de Acceso';
                                $destinatario=$roles['usuar_login'];
                                $msg=array('success'=>'true','msg'=>'Usuario aprobado correctamente', 'sql'=>$sql);
                                $this->funciones->enviarCorreo($vista,$datos,$asunto,$destinatario);
                                $transaction->commit();
                        }
                    }else{
                        $transaction->rollBack();
                        $msg=array('success'=>'false','msg'=>'El Usuario ya esta aprobado');
                    }   
                }else{
                    $transaction->rollBack();
                    $msg=array('success'=>'false','msg'=>'El Usuario no existe');
                }
            }catch(Exception $e){
                var_dump($e);
                $transaction->rollBack();
                $msg=array('success'=>'false','msg'=>'Error al verificar la información');
            }
            echo json_encode($msg);
        } else {
	        $msg=array('success'=>'false','msg'=>'No envio ningun dato');
	        echo json_encode($msg);
        }
    }

    public function actionPreRegistroVerificar() {
        if ($_POST) {
            $codig=($_POST['codigo']);
            $accio=mb_strtoupper($_POST['accio']);
            $motiv=mb_strtoupper($_POST['motiv']);
            $obser=mb_strtoupper($_POST['obser']);
            $id=($_POST['id']);
            if($accio==1){
                $estat=4;
                $titulo='Solicitud Cargada';
            }else{
                $estat=5;
                $titulo='Solicitud Rechazada';
            }

            $usuar=Yii::app()->user->id['usuario']['codigo'];
            $fecha=date('Y-m-d');
            $hora=date('H:i:s');
            $conexion=Yii::app()->db;
            $transaction=$conexion->beginTransaction();
            $sql="SELECT * FROM seguridad_usuarios WHERE usuar_login = '".$_POST['usuar']."'";
            $usuario=$conexion->createCommand($sql)->queryRow();
            Yii::app()->user->id['usuario']['codigo']=$usuar=$usuario['usuar_codig'];

            try{
                $sql="SELECT * FROM rd_preregistro WHERE prere_codig ='".$codig."'";
                $solicitud=$conexion->createCommand($sql)->queryRow();
                if($solicitud){
                    
                    $sql="UPDATE rd_preregistro
                             SET prere_accio='".$accio."',
                                 prere_rmoti='".$motiv."',
                                 prere_robse='".$obser."',
                                 estat_codig='".$estat."'
                           WHERE prere_codig ='".$codig."'";
                    $query=$sql;
                    $res1=$conexion->createCommand($sql)->execute();

                    //if($res1){

                    $sql="SELECT * FROM rd_preregistro WHERE prere_codig ='".$codig."'";
                    $solicitud2=$conexion->createCommand($sql)->queryRow();

                    $opera='U';

                    $antes=json_encode($solicitud);
                    $actual=json_encode($solicitud2);
                    $traza=$this->funciones->guardarTrazaApi($conexion, $opera, 'rd_preregistro', $antes,$actual,$query,$usuar);
                    
                    $trayectoria=$this->funciones->guardarTrayectoriaApi($conexion, $codig, $estat, $solicitud['estat_codig'], 3,$usuar);
                    $movimiento=$this->funciones->guardarMovimientoApi($conexion, $codig, $estat, $accio, $motiv, $obser,$usuar);
                        if($accio=='1'){
                            $cliente=$this->funciones->crearSolicitud($conexion, $codig, $id);
                            if($cliente['success']=='false'){
                                $transaction->rollBack();
                                echo json_encode($cliente);
                                exit();
                            }
                        }
                        //exit();
                        $datos['row']=$solicitud2;
                        $datos['accio']=$accio;
                        $datos['motiv']=$motiv;
                        $datos['obser']=$obser;
                        $corre=$solicitud['prere_celec'];
                        if($accio=='1'){
                        //$this->funciones->enviarCorreo('../registro/preregistro/verificar_correo',$datos,'RapidPago | '.$titulo,$corre);

                        }
                        $transaction->commit();
                        $msg=array('success'=>'true','msg'=>' Solicitud Verificada Correctamente'); 
                    /*}else{
                        $transaction->rollBack();
                        $msg=array('success'=>'false','msg'=>'Error al eliminado la Deducción');    
                    }*/
    
                }else{
                    $transaction->rollBack();
                    $msg=array('success'=>'false','msg'=>'La Solicitud no existe');
                }
                
            }catch(Exception $e){
                var_dump($e);
                $transaction->rollBack();
                $msg=array('success'=>'false','msg'=>'Error al verificar la información');
            }
            echo json_encode($msg);
        } else {
            $msg=array('success'=>'false','msg'=>'No envio ningun dato');
            echo json_encode($msg);
        }
    }


    
    private function getCurlData($url)
    {
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        $curlData = curl_exec($curl);
        curl_close($curl);
        return $curlData;
    }
    private function getCI($cedula, $return_raw = false) {
        define('APPID_CEDULA', '392');
        define('TOKEN_CEDULA', '7b4273ae051894742ea499b9cc818bd2');
        $res = $this->getCurlData("https://cuado.co:444/api/v1?app_id=".APPID_CEDULA."&token=".TOKEN_CEDULA."&cedula=".(int)$cedula);
        if($return_raw)
            return strlen($res)>3?$res:false;
        $res = json_decode($res, true);
        return isset($res['data']) && $res['data']?$res['data']:$res['error_str'];
    }
    public function actionGetCI(){
        $consulta = $this->getCI(24900845);
        if(is_array($consulta)) {
            echo '<pre>';
            print_r($consulta);

            var_dump($consulta);
        }else{
            echo "Ocurrio un error en la consulta: ".$consulta;
        }
    }
    
}