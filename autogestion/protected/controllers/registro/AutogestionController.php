<?php

class AutogestionController extends Controller
{
	public $funciones;

	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}

	public function actionIndex()
	{
		$this->redirect(Yii::app()->request->baseUrl.'/pedidos/pedidos/listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['numero']){
			$numero=mb_strtoupper($_POST['numero']);
			$condicion.="a.prere_numer like '%".$numero."%' ";
			$con++;
		}
		if($_POST['epedi']){
			if($con>0){
				$condicion.="AND ";
			}
			$epedi=mb_strtoupper($_POST['epedi']);
			$condicion.="a.epedi_codig like '%".$epedi."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db2;
		$sql="SELECT * 
			  FROM rd_preregistro  a
			  WHERE a.prere_codig ='".$_GET['c']."'";
		$solicitud=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('solicitud' => $solicitud));
	}
	public function actionRegistrar()
	{
		if($_POST){
			//Datos de la Solicitud
			$canti=mb_strtoupper($_POST["canti"]);
			$mcant=mb_strtoupper($_POST["mcant"]);
			$color=mb_strtoupper($_POST["color"]);
			$mensa=mb_strtoupper($_POST["mensa"]);
			//Datos de Auditoria
			$estat=1;
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();

			try{
				//Obtenemos el codigo del pedido 
				$sql="SELECT max(prere_codig) codigo FROM rd_preregistro";
				$solicitud=$conexion->createCommand($sql)->queryRow();
				$codig=$solicitud['codigo']+1;
				$cpedi=$this->funciones->generarCodigoPedido(7,$codig);

				//Guardamos la Solicitud
				
				$sql="INSERT INTO rd_preregistro( prere_numer, prere_total, prere_cmode, prere_obser, epedi_codig, usuar_codig, prere_fcrea, prere_hcrea) 
						VALUES ('".$cpedi."','".$canti."','".$mcant."','".$mensa."','".$estat."','".$usuar."','".$fecha."','".$hora."')";
				$res1=$conexion->createCommand($sql)->execute();
				if($res1){
					$transaction->commit();
					$msg=array('success'=>'true','msg'=>'Pedido guardado correctamente');	
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error al guardar la Solicitud');	
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			//$this->render('registrar');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			try{
				
				//Datos de la Solicitud
				$canti=0;
				$mcant=0;
				$color=0;
				$mensa="";
				$pasos=1;
				//Datos de Auditoria
				$estat=1;
				$usuar=Yii::app()->user->id['usuario']['codigo'];
				$fecha=date('Y-m-d');
				$hora=date('H:i:s');
				//Obtenemos el codigo del pedido 
				$sql="SELECT max(prere_codig) codigo FROM rd_preregistro";
				$solicitud=$conexion->createCommand($sql)->queryRow();
				$codig=$solicitud['codigo']+1;
				$csoli='S-'.$this->funciones->generarCodigoPedido(7,$codig);

				//Guardamos la Solicitud
				$sql="INSERT INTO rd_preregistro( prere_numer, estat_codig, prere_pasos, usuar_codig, prere_fcrea, prere_hcrea) 
							VALUES ('".$csoli."','".$estat."','".$pasos."','".$usuar."','".$fecha."','".$hora."')";
				$query=$sql;
				$res1=$conexion->createCommand($sql)->execute();
				if($res1){
					$sql="SELECT max(prere_codig) codigo, a.* FROM rd_preregistro a";
					$solicitud=$conexion->createCommand($sql)->queryRow();

					$opera='I';

					$antes=json_encode('');
					$actual=json_encode($solicitud);

					$traza=$this->funciones->guardarTraza($conexion, $opera, 'rd_preregistro', $antes,$actual,$query);
					
					$trayectoria=$this->funciones->guardarTrayectoria($conexion, $codig,$estat, '', 1);
					$transaction->commit();
					
					$this->redirect('modificar?c='.$solicitud['codigo'].'&s=1');
					
					$msg=array('success'=>'true','msg'=>'Solicitud guardado correctamente');	
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error al guardar la Solicitud');	
				}
				
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
		}
	}
	public function actionPaso1()
	{
		if($_POST){
			//Datos del Detalle
			$pedid=mb_strtoupper($_POST['pedid']);
			$codig=mb_strtoupper($_POST['codig']);
			$pasos=mb_strtoupper($_POST['pasos']);
			
			$rifco=mb_strtoupper($_POST['rifco']);
			$rsoci=mb_strtoupper($_POST['rsoci']);
			$nfant=mb_strtoupper($_POST['nfant']);
			$acome=mb_strtoupper($_POST['acome']);
			$tmovi=mb_strtoupper($_POST['tmovi']);
			$tfijo=mb_strtoupper($_POST['tfijo']);
			$estad=mb_strtoupper($_POST['estad']);
			$munic=mb_strtoupper($_POST['munic']);
			$parro=mb_strtoupper($_POST['parro']);
			$ciuda=mb_strtoupper($_POST['ciuda']);
			$zpost=mb_strtoupper($_POST['zpost']);
			$celec=mb_strtoupper($_POST['celec']);
			$dfisc=mb_strtoupper($_POST['dfisc']);
			$banco=mb_strtoupper($_POST['banco']);
			$cafil=mb_strtoupper($_POST['cafil']);
			$ncuen=mb_strtoupper($_POST['ncuen']);
			
			//Datos de Auditoria
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();

			$monto=0;
			if($pasos<'2'){
				$pasos='2';
			}
			
			try{
				$sql="SELECT * 
				  	  FROM rd_preregistro
					  WHERE prere_codig='".$codig."';";
				$solicitud=$conexion->createCommand($sql)->queryRow();
				$msg=array('success'=>'true','msg'=>'Solicitud Verificada Correctamente');	
				if($solicitud['prere_rifco']!=$rifco)
				{
					$sql="SELECT * FROM rd_preregistro
						WHERE prere_rifco='".$rifco."' and estat_codig NOT IN (0,4);";
					$preregistro=$conexion->createCommand($sql)->queryRow();
					if(!$preregistro){
						$sql="SELECT * FROM cliente
						WHERE clien_rifco='".$rifco."';";
						$cliente=$conexion->createCommand($sql)->queryRow();

						$sql="SELECT * FROM solicitud
						WHERE clien_codig='".$cliente['clien_codig']."' and estat_codig NOT IN (0,27);";
						$solic=$conexion->createCommand($sql)->queryRow();
						if($solic){
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Ya se encuentra una solicitud en curso con el mismo RIF');	
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Ya se encuentra un Pre Registro con el mismo RIF');	
					}
				}
				if($msg['success']=='true'){
					$sql="SELECT * FROM rd_preregistro
						WHERE prere_codig='".$codig."';";
					$solicitud=$conexion->createCommand($sql)->queryRow();
					if($solicitud){

						$sql="UPDATE rd_preregistro
						    SET prere_rifco= '".$rifco."',
								prere_rsoci= '".$rsoci."',
								prere_nfant= '".$nfant."',
								acome_codig= '".$acome."',
								prere_tmovi= '".$tmovi."',
								prere_tfijo= '".$tfijo."',
								estad_codig= '".$estad."',
								munic_codig= '".$munic."',
								parro_codig= '".$parro."',
								ciuda_codig= '".$ciuda."',
								prere_zpost= '".$zpost."',
								prere_celec= '".$celec."',
								prere_dfisc= '".$dfisc."',
								banco_codig= '".$banco."',
								prere_cafil= '".$cafil."',
								prere_ncuen= '".$ncuen."',
								prere_pasos= '".$pasos."',
								prere_fmodi= '".$fecha."',
								prere_hmodi= '".$hora."'
						WHERE prere_codig = '".$codig."'";
						$query=$sql;
						$res1=$conexion->createCommand($sql)->execute();
						if($res1 or $solicitud['prere_pasos']>='2'){
							

							$sql="SELECT * FROM rd_preregistro
								  WHERE prere_codig='".$codig."'";
							
							$solicitud2=$conexion->createCommand($sql)->queryRow();
							
							$antes=json_encode($solicitud);
							$actual=json_encode($solicitud2);

							$traza=$this->funciones->guardarTraza($conexion, 'U', 'rd_preregistro', $antes,$actual,$query);

							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Solicitud actualizada correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar la Solicitud');	
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error no existe la Solicitud');	
					}
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);

		}else{
			$p=$_GET['p'];

			$s=1;

			$this->render('registrar',array('p' => $p,'s' => $s ));
		}
	}

	public function actionPaso2()
	{
		if($_POST){
			//Datos del Detalle
			$pedid=mb_strtoupper($_POST['pedid']);
			$codig=mb_strtoupper($_POST['codig']);
			$pasos=mb_strtoupper($_POST['pasos']);
			
			//Datos del Formulario
			$otele=$_POST['otele'];
			$canti=$_POST['canti'];
			
			//Datos de Auditoria
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();


			if($pasos<'3'){
				$pasos='3';
			}
			$operadores = asort($otele);
			$ant='';
			foreach ($otele as $key => $value) {
				if($value==$ant){
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No puede incluir un Operador Telefónico más de una vez');
					echo json_encode($msg);
					exit();
				}
				$ant=$value;
			}

			try{
				$sql="SELECT * FROM rd_preregistro
					WHERE prere_codig='".$codig."';";
				$solicitud=$conexion->createCommand($sql)->queryRow();
				
				
				if($solicitud){

					foreach ($otele as $key => $value) {
						$sql="SELECT * 
							  FROM rd_preregistro_configuracion_equipo 
							  WHERE prere_codig ='".$codig."' 
							    AND otele_codig ='".$otele[$key]."'";	
						$cequipo=$conexion->createCommand($sql)->queryRow();
						if($cequipo){
							$sql="UPDATE rd_preregistro_configuracion_equipo 
								  SET cequi_canti = '".$canti[$key]."' 
								  WHERE cequi_codig='".$cequipo['cequi_codig']."'";
							$opera='U';
						}else{
							$sql="INSERT INTO rd_preregistro_configuracion_equipo(prere_codig, otele_codig, cequi_canti, usuar_codig, cequi_fcrea, cequi_hcrea) VALUES ('".$codig."', '".$otele[$key]."', '".$canti[$key]."', '".$usuar."', '".$fecha."', '".$hora."' )";
							$opera='I';

						}
						$query=$sql;
						$res2=$conexion->createCommand($sql)->execute();

						$sql="SELECT * 
							  FROM rd_preregistro_configuracion_equipo 
							  WHERE prere_codig ='".$codig."' 
							    AND otele_codig ='".$otele[$key]."'";	
						$cequipo2=$conexion->createCommand($sql)->queryRow();
						
						$antes=json_encode($cequipo);
						$actual=json_encode($cequipo2);

						$traza=$this->funciones->guardarTraza($conexion, $opera, 'rd_preregistro_configuracion_equipo', $antes,$actual,$query);
					}

					$sql="UPDATE rd_preregistro
						  SET prere_pasos='".$pasos."',
							  prere_fmodi= '".$fecha."',
							  prere_hmodi= '".$hora."' 
						  WHERE prere_codig='".$codig."';";
					$query=$sql;
					$res1=$conexion->createCommand($sql)->execute();



					
					if($res1 or $solicitud['prere_pasos']>='2'){

						$sql="SELECT * FROM rd_preregistro
							WHERE prere_codig='".$codig."';";
						$solicitud2=$conexion->createCommand($sql)->queryRow();
					
						
						$antes=json_encode($solicitud);
						$actual=json_encode($solicitud2);

						$traza=$this->funciones->guardarTraza($conexion, $opera, 'rd_preregistro', $antes,$actual,$query);

						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Solicitud actualizada correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al actualizar la Solicitud');	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error no existe la Solicitud');	
				}
					
				
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);

		}else{
			$p=$_GET['p'];
			$s=1;

			$this->render('registrar',array('p' => $p,'s' => $s ));
		}
	}
	public function actionPaso3()
	{
		if($_POST){

			//Datos del Detalle
			$pedid=mb_strtoupper($_POST['pedid']);
			$codig=mb_strtoupper($_POST['codig']);
			$pasos=mb_strtoupper($_POST['pasos']);
			//Datos del Formulario

			$rifrl=$_POST['rifrl'];
			$nombr=$_POST['nombr'];
			$apell=$_POST['apell'];
			$tdocu=$_POST['tdocu'];
			$ndocu=$_POST['ndocu'];
			$cargo=$_POST['cargo'];
			$tmovi=$_POST['tmovi'];
			$tfijo=$_POST['tfijo'];
			$celec=$_POST['celec'];



			//Datos de Auditoria
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();

			$monto=0;
			if($pasos<'3'){
				$pasos='3';
			}
			try{
				$sql="SELECT * FROM rd_preregistro
					WHERE prere_codig='".$codig."';";
				$solicitud=$conexion->createCommand($sql)->queryRow();
								
				if($solicitud){
					$r=0;
					foreach ($rifrl as $key => $value) {
						$rifrl[$key]=strtoupper($rifrl[$key]);
						$nombr[$key]=strtoupper($nombr[$key]);
						$apell[$key]=strtoupper($apell[$key]);
						$tdocu[$key]=strtoupper($tdocu[$key]);
						$ndocu[$key]=strtoupper($ndocu[$key]);
						$cargo[$key]=strtoupper($cargo[$key]);
						$tmovi[$key]=strtoupper($tmovi[$key]);
						$tfijo[$key]=strtoupper($tfijo[$key]);
						$celec[$key]=strtoupper($celec[$key]);


						$sql="SELECT * 
							  FROM rd_preregistro_representante_legal 
							  WHERE prere_codig ='".$codig."' 
							    AND rlega_rifrl ='".$rifrl[$key]."'";	
						$representante=$conexion->createCommand($sql)->queryRow();

						if($representante){
							
							$sql="UPDATE rd_preregistro_representante_legal 
								  SET rlega_rifrl= '".$rifrl[$key]."', 
									  rlega_nombr= '".$nombr[$key]."', 
									  rlega_apell= '".$apell[$key]."', 
									  tdocu_codig= '".$tdocu[$key]."', 
									  rlega_ndocu= '".$ndocu[$key]."', 
									  rlega_cargo= '".$cargo[$key]."', 
									  rlega_tmovi= '".$tmovi[$key]."', 
									  rlega_tfijo= '".$tfijo[$key]."', 
									  rlega_corre= '".$celec[$key]."'
								  WHERE rlega_codig='".$representante['rlega_codig']."'";	
							$opera='U';							  
						}else{
							$sql="INSERT INTO rd_preregistro_representante_legal(prere_codig, rlega_rifrl, rlega_nombr, rlega_apell, tdocu_codig, rlega_ndocu, rlega_cargo, rlega_tmovi, rlega_tfijo, rlega_corre, usuar_codig, rlega_fcrea, rlega_hcrea) 
							  VALUES ('".$codig."', '".$rifrl[$key]."', '".$nombr[$key]."', '".$apell[$key]."', '".$tdocu[$key]."', '".$ndocu[$key]."', '".$cargo[$key]."', '".$tmovi[$key]."', '".$tfijo[$key]."', '".$celec[$key]."', '".$usuar."', '".$fecha."', '".$hora."')";

							$opera='I';

						}
						$res2=$conexion->createCommand($sql)->execute();
						
						$query=$sql;
						$sql="SELECT * 
							  FROM rd_preregistro_representante_legal 
							  WHERE prere_codig ='".$codig."' 
							    AND rlega_rifrl ='".$rifrl[$key]."'";	
						$representante2=$conexion->createCommand($sql)->queryRow();
						
						$antes=json_encode($representante);
						$actual=json_encode($representante2);

						$traza=$this->funciones->guardarTraza($conexion, $opera, 'rd_preregistro_representante_legal', $antes,$actual,$query);

						if($r>0){
							$eliminar.=',';	
						}
						$eliminar.="'".$rifrl[$key]."'";
						$r++;
					}
					
					


					if($eliminar!=''){

						$sql="SELECT * 
							  FROM rd_preregistro_representante_legal 
							  WHERE prere_codig='".$codig."' 
								AND rlega_rifrl NOT IN (".$eliminar.")";
						$solicitudes=$conexion->createCommand($sql)->queryAll();

						$sql="DELETE FROM rd_preregistro_representante_legal 
							  WHERE prere_codig='".$codig."' 
							    AND rlega_rifrl NOT IN (".$eliminar.")";
						$query=$sql;						
						$res3=$conexion->createCommand($sql)->execute();

						$opera='D';
	
						$sql="SELECT * 
							  FROM rd_preregistro_representante_legal 
							  WHERE prere_codig='".$codig."' 
								AND rlega_rifrl NOT IN (".$eliminar.")";
						$solicitudes2=$conexion->createCommand($sql)->queryAll();
						
						$antes=json_encode($solicitudes);
						$actual=json_encode($solicitudes2);
						
						$traza=$this->funciones->guardarTraza($conexion, $opera, 'rd_preregistro_representante_legal', $antes,$actual,$query);
					
					}
					
					

					$sql="UPDATE rd_preregistro
						  SET prere_pasos='".$pasos."',
							  prere_fmodi= '".$fecha."',
							  prere_hmodi= '".$hora."' 
						  WHERE prere_codig='".$codig."';";
					$query=$sql;		
					$res1=$conexion->createCommand($sql)->execute();

					
					if($res1 or $solicitud['prere_pasos']>='3'){
						
						$sql="SELECT * FROM rd_preregistro
						WHERE prere_codig='".$codig."';";
						
						$solicitud2=$conexion->createCommand($sql)->queryRow();
						
						$antes=json_encode($solicitud);
						$actual=json_encode($solicitud2);		
						$opera='U';
						
						$traza=$this->funciones->guardarTraza($conexion, $opera, 'rd_preregistro', $antes,$actual,$query);

						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Solicitud actualizada correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al actualizar la Solicitud');	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error no existe la Solicitud');	
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);

		}else{
			$p=$_GET['p'];
			$s=1;

			$this->render('registrar',array('p' => $p,'s' => $s ));
		}
	}
	public function actionPaso4()
	{
		if($_POST){
			//Datos del Detalle
			$pedid=mb_strtoupper($_POST['pedid']);
			$codig=mb_strtoupper($_POST['codig']);
			$pasos=mb_strtoupper($_POST['pasos']);
			
			$fsoli=$this->funciones->TransformarFecha_bd(mb_strtoupper($_POST['fsoli']));
			$orige=mb_strtoupper($_POST['orige']);
			$clvip=mb_strtoupper($_POST['clvip']);
			$obser=mb_strtoupper($_POST['obser']);
			
			//Datos de Auditoria
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();

			$monto=0;
			if($pasos<'4'){
				$pasos='4';
			}
			
			try{
				$sql="SELECT * FROM rd_preregistro
					WHERE prere_codig='".$codig."';";
				$solicitud=$conexion->createCommand($sql)->queryRow();
				if($solicitud['estat_codig']==1){
					$estat=2;

					$trayectoria=$this->funciones->guardarTrayectoria($conexion, $codig, $estat, $solicitud['estat_codig'], 2);
				}else{
					$estat=$solicitud['estat_codig'];
				}
				if($solicitud){

					$sql="UPDATE rd_preregistro
					    SET prere_fsoli= '".$fsoli."',
							orige_codig= '".$orige."',
							prere_clvip= '".$clvip."',
							prere_obser= '".$obser."',
							prere_pasos= '".$pasos."',
							estat_codig= '".$estat."',
							prere_fmodi= '".$fecha."',
							prere_hmodi= '".$hora."' 
					WHERE prere_codig = '".$codig."'";
					
					$query=$sql;


					$res1=$conexion->createCommand($sql)->execute();
					if($res1 or $solicitud['prere_pasos']>='4'){
						
						$sql="SELECT * FROM rd_preregistro
							  WHERE prere_codig='".$codig."'";
						
						$solicitud2=$conexion->createCommand($sql)->queryRow();
						
						$antes=json_encode($solicitud);
						$actual=json_encode($solicitud2);

						$traza=$this->funciones->guardarTraza($conexion, 'U', 'rd_preregistro', $antes,$actual,$query);

						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Solicitud actualizada correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al actualizar la Solicitud');	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error no existe la Solicitud');	
				}
				
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);

		}else{
			$p=$_GET['p'];

			$s=1;

			$this->render('registrar',array('p' => $p,'s' => $s ));
		}
	}

	public function actionPaso5()
	{
		if($_POST){
			//Datos del Detalle
			$pedid=mb_strtoupper($_POST['pedid']);
			$codig=mb_strtoupper($_POST['codig']);
			$pasos=mb_strtoupper($_POST['pasos']);
			//DATOS DE LOS DOCUMENTOS
			$dtipo=$_POST["dtipo"];

			$file=$_FILES['image'];
			/*$ruta='files/detalle/listado/';
			$name=explode(".", $file['name']);
			$nombre=date('Ymd_His.').end($name);
			$destino=$ruta.$nombre;*/
			$codigo='S-'.$this->funciones->generarCodigoPedido(6,$codig);
			foreach ($_FILES['image']['name'] as $key => $value) {
				$ruta='files/solicitudes/'.$codigo.'/';
				$name[$key]=explode(".", $file['name'][$key]);
				$nombre[$key]=date('Ymd_His_').$key.'.'.end($name[$key]);
				$destino[$key]=$ruta.$nombre[$key];
			}


			//Datos de Auditoria
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();

			$monto=0;
			//if($pasos<'5'){
				$pasos='5';
			//}
			$verificar['success']='true';
			try{
				
				foreach ($dtipo as $key => $value) {
					
					if(!empty($_FILES['image']['tmp_name'][$key])){
						$archivo['tmp_name']=$_FILES['image']['tmp_name'][$key];
						$verificar=$this->funciones->CopiarEmoji($archivo,$ruta,$nombre[$key]);
						if($verificar['success']!='true'){
							$transaction->rollBack();
							$msg=$verificar;
							echo json_encode($msg);	
							exit();
						}
					}else{
						$destino[$key]='';	
					}
				}

				if($verificar['success']=='true'){

					$sql="SELECT * FROM rd_preregistro
						WHERE prere_codig='".$codig."';";
					$solicitudes=$conexion->createCommand($sql)->queryRow();

					if($solicitudes['estat_codig']<=2){
						$estat=3;
						$trayectoria=$this->funciones->guardarTrayectoria($conexion, $codig, $estat, $solicitudes['estat_codig'], 2);
					}else{
						$estat=$solicitudes['estat_codig'];
					}
					$estat=3;
					if($solicitudes){
						$a=0;
						$incluyen="";

						foreach ($dtipo as $key => $value) {
							
							
							if($dtipo[$key]!='' or $destino[$key]!=''){
								$dtipo[$key]=mb_strtoupper($dtipo[$key]);

								$sql="SELECT * FROM rd_preregistro_documento_digital WHERE prere_codig ='".$codig."' and 	dtipo_codig='".$dtipo[$key]."'";
								$result=$conexion->createCommand($sql)->queryRow();
								
								if($result){
									if($destino[$key]==''){
										$sql="UPDATE rd_preregistro_documento_digital 
											  SET docum_ruta = '".$result['docum_ruta']."',
											  	  dtipo_codig = '".$dtipo[$key]."'
											  WHERE docum_codig='".$result['docum_codig']."'";	
									}else{
										$sql="UPDATE rd_preregistro_documento_digital 
										SET docum_ruta = '".$destino[$key]."',
											dtipo_codig = '".$dtipo[$key]."'
										WHERE docum_codig='".$result['docum_codig']."'";
									}
									$opera='U';
								}else {
									$sql="INSERT INTO rd_preregistro_documento_digital(prere_codig, dtipo_codig, docum_ruta, docum_orden, usuar_codig, docum_fcrea, docum_hcrea)
										VALUES ('".$codig."','".$dtipo[$key]."','".$destino[$key]."','".$key."','".$usuar."','".$fecha."','".$hora."');";
									$opera='I';
								}
								$query=$sql;

								$res2=$conexion->createCommand($sql)->execute();

								$sql="SELECT * FROM rd_preregistro_documento_digital WHERE prere_codig ='".$codig."' and 	docum_orden='".$key."'";
								$result2=$conexion->createCommand($sql)->queryRow();

								$antes=json_encode($result);
								$actual=json_encode($result2);

								$traza=$this->funciones->guardarTraza($conexion, $opera, 'rd_preregistro_documento_digital', $antes,$actual,$query);


								if($a>0 and $incluyen!=''){
									$incluyen.=",";
								}
								$incluyen.="'".$key."'";
							}
							

							$a++;
						}

						

						if($incluyen!=''){
							$sql="SELECT * 
							  FROM rd_preregistro_documento_digital 
							  WHERE prere_codig = '".$codig."' 
							    AND docum_orden not in (".$incluyen.")";
							$elimi=$conexion->createCommand($sql)->queryAll();
								
							$sql="DELETE  FROM rd_preregistro_documento_digital 
							  WHERE prere_codig = '".$codig."' 
							    AND docum_orden not in (".$incluyen.")";
							$query=$sql;
							
							$res3=$conexion->createCommand($sql)->execute();

							$opera='D';

							$sql="SELECT * 
							  FROM rd_preregistro_documento_digital 
							  WHERE prere_codig = '".$codig."' 
							    AND docum_orden not in (".$incluyen.")";

							$elimi2=$conexion->createCommand($sql)->queryAll();
						
							$antes=json_encode($elimi);
							$actual=json_encode($elimi2);
							
							$traza=$this->funciones->guardarTraza($conexion, $opera, 'rd_preregistro_documento_digital', $antes,$actual,$query);
						}
						
						$sql="UPDATE rd_preregistro
						    SET prere_pasos = '".$pasos."',
								estat_codig= '".$estat."',
								prere_fmodi= '".$fecha."',
								prere_hmodi= '".$hora."' 
						WHERE prere_codig = '".$codig."'";
						$query=$sql;
						$res1=$conexion->createCommand($sql)->execute();

						if($res1 or $solicitudes['prere_pasos']>='5'){
							$opera='U';
							$sql="SELECT * FROM rd_preregistro
									WHERE prere_codig='".$codig."';";
							$solicitudes2=$conexion->createCommand($sql)->queryAll();
							$antes=json_encode($solicitudes);
							$actual=json_encode($solicitudes2);
						
							$traza=$this->funciones->guardarTraza($conexion, $opera, 'rd_preregistro', $antes,$actual,$query);

						
						$res2=$conexion->createCommand($sql)->execute();


						$sql="SELECT * 
							  FROM rd_preregistro_documento_digital_tipo a 
							  LEFT JOIN rd_preregistro_documento_digital b ON (
							  	a.dtipo_codig = b.dtipo_codig 
							  	AND b.prere_codig = '".$codig."') 
							  WHERE a.dtipo_oblig=1 ";
						$tdocu=$conexion->createCommand($sql)->queryAll();
						if($solicitudes['prere_clvip']!='1'){
							foreach ($tdocu as $key => $value) {
								if($value['docum_ruta']==''){
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'El documento "'.$value['dtipo_descr'].'", es obligatorio');
									echo json_encode($msg);		
									exit();
								}
							}	
						}
						

							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Solicitud actualizado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar el Pedido');	
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error no existe la Solicitud');	
					}
				}else{
					$transaction->rollBack();
					$msg=$verificar;
				}
			}catch(Exception $e){
				echo $sql;
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);

		}else{
			$p=$_GET['p'];
			$s=1;

			$this->render('registrar',array('p' => $p,'s' => $s ));
		}
	}

	public function actionModificar()
	{
		if($_POST){
			//Datos de la Solicitud
			$codig=mb_strtoupper($_POST["codig"]);
			$canti=mb_strtoupper($_POST["canti"]);
			$mcant=mb_strtoupper($_POST["mcant"]);
			$color=mb_strtoupper($_POST["color"]);
			$mensa=mb_strtoupper($_POST["mensa"]);
			//Datos de Auditoria
			$estat=1;
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				
				$sql="SELECT * FROM rd_preregistro WHERE prere_codig ='".$codig."'";
				$solicitud=$conexion->createCommand($sql)->queryRow();
				if($solicitud){
					$sql="UPDATE rd_preregistro
						  SET prere_total='".$canti."',
							  prere_cmode='".$mcant."',
							  prere_obser='".$mensa."'
						  WHERE prere_codig ='".$codig."'";
					$res1=$conexion->createCommand($sql)->execute();
					if($res1){
						$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Pedido actualizado correctamente');
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al actualizar la Solicitud');	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$s=$_GET['s'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM rd_preregistro  a
			  WHERE a.prere_codig ='".$_GET['c']."'";
			$solicitud=$conexion->createCommand($sql)->queryRow();
			$ubicacion[0]='start ';
			$ubicacion[4]='finish ';
			for ($i=0; $i < 6 ; $i++) { 
				if($i<$solicitud['prere_pasos']){
					$ubicacion[$i].='upcoming ';
				}
				if ($i==($s-1)) {
					$ubicacion[$i].='active ';
				}
			}
			
			switch ($s) {
				case '1':
					$this->render('paso1', array('solicitud' => $solicitud, 'ubicacion' => $ubicacion,'c' => $c,'s' => $s));
					break;
				case '2':
					$this->render('paso2', array('solicitud' => $solicitud, 'ubicacion' => $ubicacion,'c' => $c,'s' => $s));
					break;
				case '3':
					$this->render('paso3', array('solicitud' => $solicitud, 'ubicacion' => $ubicacion,'c' => $c,'s' => $s));
					break;
				case '3':
					$this->render('paso3', array('solicitud' => $solicitud, 'ubicacion' => $ubicacion,'p' => $p,'c' => $c,'s' => $s));
					break;
				case '4':
					$this->render('paso4', array('solicitud' => $solicitud, 'ubicacion' => $ubicacion,'p' => $p,'c' => $c,'s' => $s));
					break;
				case '5':
					$this->render('paso5', array('solicitud' => $solicitud, 'ubicacion' => $ubicacion,'p' => $p,'c' => $c,'s' => $s));
					break;
				default:
					$this->render('paso1', array('solicitud' => $solicitud,'ubicacion' => $ubicacion,'c' => $c,'s' => $s));
					break;
			}
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM rd_preregistro  WHERE prere_codig ='".$codig."'";
				$solicitud=$conexion->createCommand($sql)->queryRow();
				if($solicitud){
					$sql="SELECT * FROM rd_preregistro_detalle  WHERE prere_codig ='".$codig."'";
					$detalle=$conexion->createCommand($sql)->queryRow();
					if(!$detalle){	

						$sql="DELETE FROM rd_preregistro_adicional WHERE prere_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();

						$sql="DELETE FROM rd_preregistro_deduccion WHERE prere_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();

						$sql="DELETE FROM rd_preregistro_imagen WHERE prere_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();

						$sql="DELETE FROM rd_preregistro WHERE prere_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();

						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Pedido eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar la Solicitud');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Pedido no se puede eliminar debido a que tiene modelos asociados');
					}		
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
				
			}catch(Exception $e){
				$transaction->rollBack();
				var_dump($e);
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM rd_preregistro  a
			  WHERE a.prere_codig ='".$_GET['c']."'";
			$solicitud=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('pedidos' => $solicitud,'c'=>$c));
		}
	}
	public function actionEnviar()
	{
		if($_POST){

			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM rd_preregistro WHERE prere_codig ='".$codig."'";
				$solicitud=$conexion->createCommand($sql)->queryRow();
				if($solicitud){
					if($solicitud['prere_pasos']=='3'){
						$sql="SELECT * FROM rd_preregistro_detalle  WHERE prere_codig ='".$codig."'";
						$detalles=$conexion->createCommand($sql)->queryAll();
						if($detalles){

							foreach ($detalles as $key => $value) {
								$sql="SELECT * FROM rd_preregistro_detalle_listado  WHERE pdeta_codig ='".$value['pdeta_codig']."'";
								$modelo=$conexion->createCommand($sql)->queryAll();
								if($modelo){
									if($value['pdeta_pasos']>='2'){
										$msg=array('success'=>'true','msg'=>' Detalle eliminado correctamente');
										$cmode++;
										$cpers+=count($modelo);	
									}else{
										$transaction->rollBack();
										$msg=array('success'=>'false','msg'=>'Termine todos los pasos de los modelos y el pedido para continuar');	
										echo json_encode($msg);
										exit();
									}
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Existe Un modelo sin p_personas asociadas verificar');	
									echo json_encode($msg);
									exit();
								}
							}
							if($msg['success']=='true'){
								$sql="UPDATE rd_preregistro
									  SET prere_total='".$cpers."',
										  prere_cmode='".$cmode."',
										  epedi_codig='2'
									  WHERE prere_codig ='".$codig."'";
								$res1=$conexion->createCommand($sql)->execute();

								if($res1){
									$transaction->commit();
									$msg=array('success'=>'true','msg'=>' Pedido enviado correctamente');	
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al enviar la Solicitud');	
								}
							}
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'El pedido no tiene modelos asociados');
						}
					}else{
						$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Debe finalizar el pedido antes de enviarlo');
					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
				
			}catch(Exception $e){

				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$p=$_GET['p'];
			$c=$_GET['c'];
			$s=$_GET['s'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM rd_preregistro  a
			  WHERE a.prere_codig ='".$_GET['c']."'";
			$solicitud=$conexion->createCommand($sql)->queryRow();
			$this->render('enviar', array('pedidos' => $solicitud, 'ubicacion' => $ubicacion,'p' => $p,'c' => $c,'s' => $s));
		}
	}
	public function actionAprobar()
	{
		if($_POST){
			$codig=($_POST['codig']);
			$mcant=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['mcant']));
			$total=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['total']));
			$monto=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['monto']));
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM rd_preregistro WHERE prere_codig ='".$codig."'";
				$solicitud=$conexion->createCommand($sql)->queryRow();
				if($solicitud){
					if($solicitud['epedi_codig']=='2'){
						$sql="UPDATE rd_preregistro
							  SET prere_total='".$total."',
								  prere_cmode='".$mcant."',
								  prere_monto='".$monto."',
								  epedi_codig='7'
							  WHERE prere_codig ='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();

						if($res1){
							$sql="SELECT a.*, b.*
								  FROM p_persona a
								  JOIN seguridad_usuarios b ON (a.perso_codig=b.perso_codig)
								  WHERE b.usuar_codig='".$solicitud['usuar_codig']."'";
                    		$p_persona=$conexion->createCommand($sql)->queryRow();

                    		$sql="SELECT * FROM rd_preregistro WHERE prere_codig ='".$codig."'";
							$solicitud=$conexion->createCommand($sql)->queryRow();

							$datos['row']=$p_persona;
							$datos['totales']=$solicitud;
							$corre=$p_persona['usuar_login'];
                            $this->funciones->enviarCorreo('../pedidos/pedidos/aprobar_correo',$datos,'Polerones Tiempo | Pedido Aprobado',$corre);
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Pedido Aprobado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al enviar la Solicitud');	
						}
						
					}else{
						$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Debe envar el pedido antes de Aprobarlo');
					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
				
			}catch(Exception $e){

				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM rd_preregistro  a
			  WHERE a.prere_codig ='".$_GET['c']."'";
			$solicitud=$conexion->createCommand($sql)->queryRow();
			$totales=$this->funciones->CalcularMontoPedido($solicitud);
			$this->render('aprobar', array('pedidos' => $solicitud, 'totales' => $totales, 'c'=>$c));
		}
	}
	public function actionRechazar()
	{
		if($_POST){
			$codig=($_POST['codig']);
			$mcant=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['mcant']));
			$total=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['total']));
			$monto=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['monto']));
			$obser=mb_strtoupper($_POST['obser']);
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM rd_preregistro WHERE prere_codig ='".$codig."'";
				$solicitud=$conexion->createCommand($sql)->queryRow();
				if($solicitud){
					//if($solicitud['epedi_codig']=='2'){
						$sql="UPDATE rd_preregistro
							  SET prere_total='".$total."',
								  prere_cmode='".$mcant."',
								  prere_monto='".$monto."',
								  epedi_codig='1'
							  WHERE prere_codig ='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();

						if($res1){
							$sql="SELECT a.*, b.*
								  FROM p_persona a
								  JOIN seguridad_usuarios b ON (a.perso_codig=b.perso_codig)
								  WHERE b.usuar_codig='".$solicitud['usuar_codig']."'";
                    		$p_persona=$conexion->createCommand($sql)->queryRow();

                    		$sql="SELECT * FROM rd_preregistro WHERE prere_codig ='".$codig."'";
							$solicitud=$conexion->createCommand($sql)->queryRow();

							$datos['row']=$p_persona;
							$datos['totales']=$solicitud;
							$datos['obser']=$obser;
							$corre=$p_persona['usuar_login'];
                            $this->funciones->enviarCorreo('../pedidos/pedidos/rechazar_correo',$datos,'Polerones Tiempo | Pedido Rechazado',$corre);
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Pedido Rechazado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al enviar la Solicitud');	
						}
						
					/*}else{
						$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Debe envar el pedido antes de Rechazarlo');
					}*/
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
				
			}catch(Exception $e){

				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM rd_preregistro  a
			  WHERE a.prere_codig ='".$_GET['c']."'";
			$solicitud=$conexion->createCommand($sql)->queryRow();
			$totales=$this->funciones->CalcularMontoPedido($solicitud);
			$this->render('rechazar', array('pedidos' => $solicitud, 'totales' => $totales, 'c'=>$c));
		}
	}


	public function actionCostos()
	{
		if($_POST){
			$codig=($_POST['codig']);
			$costo=mb_strtoupper($_POST['costo']);
			$obser=mb_strtoupper($_POST['obser']);
			$monto=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['monto']));
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM rd_preregistro WHERE prere_codig ='".$codig."'";
				$solicitud=$conexion->createCommand($sql)->queryRow();
				if($solicitud){
					if($solicitud['epedi_codig']=='2'){
						$sql="INSERT INTO rd_preregistro_adicional(prere_codig, costo_codig, adici_monto, adici_obser, usuar_codig, adici_fcrea, adici_hcrea) 
							VALUES ('".$codig."','".$costo."','".$monto."','".$obser."','".$usuar."','".$fecha."','".$hora."');";
						$res1=$conexion->createCommand($sql)->execute();

						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Costo Adicional agregado Correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al agregar el Costo');	
						}
						
					}else{
						$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Debe envar el pedido antes de asignar un costo adicional');
					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
				
			}catch(Exception $e){

				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$p=$_GET['p'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM rd_preregistro  a
			  WHERE a.prere_codig ='".$_GET['c']."'";
			$solicitud=$conexion->createCommand($sql)->queryRow();
			$totales=$this->funciones->CalcularMontoPedido($solicitud);
			$this->render('costos', array('pedidos' => $solicitud, 'totales' => $totales, 'p'=>$p, 'c'=>$c));
		}
	}
	public function actionCostosEliminar()
	{
		if($_POST){
			$codig=($_POST['codig']);
			$costo=mb_strtoupper($_POST['costo']);
			$obser=mb_strtoupper($_POST['obser']);
			$monto=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['monto']));
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM rd_preregistro_adicional WHERE adici_codig ='".$codig."'";
				$solicitud=$conexion->createCommand($sql)->queryRow();
				if($solicitud){

					$sql="DELETE FROM rd_preregistro_adicional WHERE adici_codig ='".$codig."'";
					$res1=$conexion->createCommand($sql)->execute();

					if($res1){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>' Costo Adicional eliminado Correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al eliminado el Costo');	
					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El adicional no existe');
				}
				
			}catch(Exception $e){

				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$p=$_GET['p'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM rd_preregistro_adicional  a
			  WHERE a.adici_codig ='".$_GET['c']."'";
			$costos=$conexion->createCommand($sql)->queryRow();
			$totales=$this->funciones->CalcularMontoPedido($solicitud);
			$this->render('costosEliminar', array('costos' => $costos, 'totales' => $totales, 'c'=>$c, 'p'=>$p));
		}
	}

	public function actionDeduccion()
	{
		if($_POST){
			$codig=($_POST['codig']);
			$deduc=mb_strtoupper($_POST['deduc']);
			$obser=mb_strtoupper($_POST['obser']);
			$monto=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['monto']));
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM rd_preregistro WHERE prere_codig ='".$codig."'";
				$solicitud=$conexion->createCommand($sql)->queryRow();
				if($solicitud){
					$sql="SELECT * FROM rd_preregistro_deduccion WHERE deduc_codig='".$deduc."' and prere_codig = '".$codig."'";
					$deduciones=$conexion->createCommand($sql)->queryRow();
					if(!$deduciones){
						if($solicitud['epedi_codig']=='2'){
							$sql="INSERT INTO rd_preregistro_deduccion(prere_codig, deduc_codig, pdedu_monto, pdedu_obser, usuar_codig, pdedu_fcrea, pdedu_hcrea) 
								VALUES ('".$codig."','".$deduc."','".$monto."','".$obser."','".$usuar."','".$fecha."','".$hora."');";
							$res1=$conexion->createCommand($sql)->execute();

							if($res1){
								$transaction->commit();
								$msg=array('success'=>'true','msg'=>' Deducción agregado Correctamente');	
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al agregar la Deducción');	
							}
							
						}else{
							$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Debe envar el pedido antes de asignar una Deducción');
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Ya se agrego la deducción');
					}
						
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
				
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$p=$_GET['p'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM rd_preregistro  a
			  WHERE a.prere_codig ='".$_GET['c']."'";
			$solicitud=$conexion->createCommand($sql)->queryRow();
			$totales=$this->funciones->CalcularMontoPedido($solicitud);
			$this->render('deduccion', array('pedidos' => $solicitud, 'totales' => $totales, 'c'=>$c, 'p'=>$p));
		}
	}
	public function actionDeduccionEliminar()
	{
		if($_POST){
			$codig=($_POST['codig']);
			$deduc=mb_strtoupper($_POST['deduc']);
			$obser=mb_strtoupper($_POST['obser']);
			$monto=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['monto']));
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM rd_preregistro_deduccion WHERE pdedu_codig ='".$codig."'";
				$solicitud=$conexion->createCommand($sql)->queryRow();
				if($solicitud){
						
					$sql="DELETE FROM rd_preregistro_deduccion WHERE pdedu_codig ='".$codig."'";
					$res1=$conexion->createCommand($sql)->execute();

					if($res1){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>' Deducción eliminado Correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al eliminado la Deducción');	
					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
				
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$p=$_GET['p'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM rd_preregistro_deduccion  a
			  WHERE a.pdedu_codig ='".$_GET['c']."'";
			$deduccion=$conexion->createCommand($sql)->queryRow();
			$totales=$this->funciones->CalcularMontoPedido($solicitud);
			$this->render('deduccionEliminar', array('deduccion' => $deduccion, 'totales' => $totales, 'c'=>$c, 'p'=>$p));
		}
	}
	public function actionVerificar()
	{
		if($_POST){
			$codig=($_POST['codigo']);
			$accio=mb_strtoupper($_POST['accio']);
			$motiv=mb_strtoupper($_POST['motiv']);
			$obser=mb_strtoupper($_POST['obser']);
			$id=($_POST['id']);
			if($accio==1){
				$estat=4;
				$titulo='Solicitud Cargada';
			}else{
				$estat=5;
				$titulo='Solicitud Rechazada';
			}

			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();

			try{
				$sql="SELECT * FROM rd_preregistro WHERE prere_codig ='".$codig."'";
				$solicitud=$conexion->createCommand($sql)->queryRow();
				if($solicitud){
					
					$sql="UPDATE rd_preregistro
							 SET prere_accio='".$accio."',
								 prere_rmoti='".$motiv."',
								 prere_robse='".$obser."',
								 estat_codig='".$estat."'
						   WHERE prere_codig ='".$codig."'";
					$query=$sql;
					$res1=$conexion->createCommand($sql)->execute();

					//if($res1){

					$sql="SELECT * FROM rd_preregistro WHERE prere_codig ='".$codig."'";
					$solicitud2=$conexion->createCommand($sql)->queryRow();

					$opera='U';

					$antes=json_encode($solicitud);
					$actual=json_encode($solicitud2);
					$traza=$this->funciones->guardarTraza($conexion, $opera, 'rd_preregistro', $antes,$actual,$query);
					
					$trayectoria=$this->funciones->guardarTrayectoria($conexion, $codig, $estat, $solicitud['estat_codig'], 3);
					$movimiento=$this->funciones->guardarMovimiento($conexion, $codig, $estat, $accio, $motiv, $obser);
						if($accio=='1'){
							$cliente=$this->funciones->crearSolicitud($conexion, $codig, $id);
							if($cliente['success']=='false'){
								$transaction->rollBack();
								echo json_encode($cliente);
								exit();
							}
						}
						//exit();
						$datos['row']=$solicitud2;
						$datos['accio']=$accio;
						$datos['motiv']=$motiv;
						$datos['obser']=$obser;
						$corre=$solicitud['prere_celec'];
                        if($accio=='1'){
                        $this->funciones->enviarCorreo('../registro/preregistro/verificar_correo',$datos,'RapidPago | '.$titulo,$corre);

                        }
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>' Solicitud Verificada Correctamente');	
					/*}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al eliminado la Deducción');	
					}*/
	
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'La Solicitud no existe');
				}
				
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$p=$_GET['p'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
				  FROM rd_preregistro  a
				  WHERE a.prere_codig ='".$_GET['c']."'";
			$solicitud=$conexion->createCommand($sql)->queryRow();
			$this->render('verificar', array('solicitud' => $solicitud, 'c' => $c));
		}
	}
	public function actionAnular()
	{
		$c=$_GET['c'];
		$p=$_GET['p'];
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM rd_preregistro  a
			  WHERE a.prere_codig ='".$_GET['c']."'";
		$solicitud=$conexion->createCommand($sql)->queryRow();
		$this->render('anular', array('solicitud' => $solicitud));
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
