<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

    private $_id;
    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    public function authenticate() {



        
        $users = Usuarios::model()->findByAttributes(array('usuar_login' => $this->username));
        // here I use Email as user name which comes from database
        
        
        if ($users === null) {
            //$this->_id = 'user Null';
            //$this->errorCode = self::ERROR_USERNAME_INVALID;
          $err = "El usuario o contraseña no son validos";
          $this->errorCode = $err;
        } else if ($users->usuar_passw !== md5($this->password)) {            // here I compare db password with passwod field
            $this->_id = $this->username;
            $err = "El usuario o contraseña no son validos";

            $this->errorCode = $err;

        }else if ($users->uesta_codig == 2 ) {          // here I compare db password with passwod field
            $err = "Debe verificar su cuenta de correo";
            $this->errorCode = $err;
           
        }else if ($users->uesta_codig == 9 ) {          // here I compare db password with passwod field
            $err = "Usuario bloqueado contacte con el administrador";
            $this->errorCode = $err;
           
        }else{
            //$this->_id = $users['tusua_corre'];
            $perso = Persona::model()->findByAttributes(array('perso_codig' => $users["perso_codig"]));
            $datos['codigo']=$users["usuar_codig"];
            $datos['usuario']=$users["usuar_login"];
            $datos['estatus']=$users["uesta_codig"];
            $datos['permiso']=$users["urole_codig"];
            $datos['nombre']=$perso["perso_pnomb"].' '.$perso["perso_papel"];
            unset($users);
            unset($perso);
            $this->_id['usuario'] = $datos;
            $this->setState('title', $datos['nombre']);
            $this->errorCode = self::ERROR_NONE;

        }
       // var_dump($this->errorCode);
        return $this->errorCode;



          /* $users=array(
          // username => password
          'demo'=>'demo',
          'admin'=>'admin',
          );
          if(!isset($users[$this->username]))
          $this->errorCode=self::ERROR_USERNAME_INVALID;
          elseif($users[$this->username]!==$this->password)
          $this->errorCode=self::ERROR_PASSWORD_INVALID;
          else
          $this->errorCode=self::ERROR_NONE;
          return !$this->errorCode; */
    }
    public function getId()
    {
        return $this->_id;
    }

}
