<?php

/**
 * This is the model class for table "tusuario".
 *
 * The followings are the available columns in table 'tusuario':
 * @property integer $tusua_codig
 * @property string $tusua_corre
 * @property integer $tgene_permi
 * @property string $tpers_nacio
 * @property integer $tpers_cedul
 * @property integer $tusua_statu
 * @property string $tusua_corr2
 * @property string $tusua_passw
 * @property integer $organ_codig
 * @property integer $tgene_cargo
 * @property string $tusua_telef
 * @property string $tusua_obser
 * @property string $tusua_fcrea
 *
 * The followings are the available model relations:
 * @property Tafirecaudacion[] $tafirecaudacions
 * @property Tjubilacion[] $tjubilacions
 * @property Tdireccion[] $tdireccions
 * @property Tdatosnomina[] $tdatosnominas
 * @property Tjubsueldos[] $tjubsueldoses
 * @property Tcontactos[] $tcontactoses
 * @property Tfigura[] $tfiguras
 * @property Thijosinci[] $thijosincis
 * @property Tiporganoente[] $tiporganoentes
 * @property Tobservaciones[] $tobservaciones
 * @property Tsoportes[] $tsoportes
 * @property Tjubtrayectoria[] $tjubtrayectorias
 * @property Tafinomina[] $tafinominas
 * @property Tdetalles[] $tdetalles
 * @property Tdevolucion[] $tdevolucions
 * @property Tmaxautoridad[] $tmaxautoridads
 * @property Tgenerica $tgeneCargo
 */
class Tusuario extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tusuario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tgene_permi, tpers_nacio, tpers_cedul, tusua_statu', 'required'),
			array('tgene_permi, tpers_cedul, tusua_statu, organ_codig, tgene_cargo', 'numerical', 'integerOnly'=>true),
			array('tusua_corre, tusua_corr2, tusua_passw', 'length', 'max'=>50),
			array('tpers_nacio', 'length', 'max'=>2),
			array('tusua_telef', 'length', 'max'=>15),
			array('tusua_obser', 'length', 'max'=>500),
			array('tusua_fcrea', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('tusua_codig, tusua_corre, tgene_permi, tpers_nacio, tpers_cedul, tusua_statu, tusua_corr2, tusua_passw, organ_codig, tgene_cargo, tusua_telef, tusua_obser, tusua_fcrea', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tafirecaudacions' => array(self::HAS_MANY, 'Tafirecaudacion', 'tusua_codig'),
			'tjubilacions' => array(self::HAS_MANY, 'Tjubilacion', 'tusua_codig'),
			'tdireccions' => array(self::HAS_MANY, 'Tdireccion', 'tusua_codig'),
			'tdatosnominas' => array(self::HAS_MANY, 'Tdatosnomina', 'tusua_codig'),
			'tjubsueldoses' => array(self::HAS_MANY, 'Tjubsueldos', 'tusua_codig'),
			'tcontactoses' => array(self::HAS_MANY, 'Tcontactos', 'tusua_codig'),
			'tfiguras' => array(self::HAS_MANY, 'Tfigura', 'tusua_codig'),
			'thijosincis' => array(self::HAS_MANY, 'Thijosinci', 'tusua_codig'),
			'tiporganoentes' => array(self::HAS_MANY, 'Tiporganoente', 'tusua_codig'),
			'tobservaciones' => array(self::HAS_MANY, 'Tobservaciones', 'tusua_codig'),
			'tsoportes' => array(self::HAS_MANY, 'Tsoportes', 'tusua_codig'),
			'tjubtrayectorias' => array(self::HAS_MANY, 'Tjubtrayectoria', 'tusua_codig'),
			'tafinominas' => array(self::HAS_MANY, 'Tafinomina', 'tusua_codig'),
			'tdetalles' => array(self::HAS_MANY, 'Tdetalles', 'tusua_codig'),
			'tdevolucions' => array(self::HAS_MANY, 'Tdevolucion', 'tusua_codig'),
			'tmaxautoridads' => array(self::HAS_MANY, 'Tmaxautoridad', 'tusua_codig'),
			'tgeneCargo' => array(self::BELONGS_TO, 'Tgenerica', 'tgene_cargo'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tusua_codig' => 'Tusua Codig',
			'tusua_corre' => 'Tusua Corre',
			'tgene_permi' => 'Tgene Permi',
			'tpers_nacio' => 'Tpers Nacio',
			'tpers_cedul' => 'Tpers Cedul',
			'tusua_statu' => 'Tusua Statu',
			'tusua_corr2' => 'Tusua Corr2',
			'tusua_passw' => 'Tusua Passw',
			'organ_codig' => 'Organ Codig',
			'tgene_cargo' => 'Tgene Cargo',
			'tusua_telef' => 'Tusua Telef',
			'tusua_obser' => 'Tusua Obser',
			'tusua_fcrea' => 'Tusua Fcrea',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('tusua_codig',$this->tusua_codig);
		$criteria->compare('tusua_corre',$this->tusua_corre,true);
		$criteria->compare('tgene_permi',$this->tgene_permi);
		$criteria->compare('tpers_nacio',$this->tpers_nacio,true);
		$criteria->compare('tpers_cedul',$this->tpers_cedul);
		$criteria->compare('tusua_statu',$this->tusua_statu);
		$criteria->compare('tusua_corr2',$this->tusua_corr2,true);
		$criteria->compare('tusua_passw',$this->tusua_passw,true);
		$criteria->compare('organ_codig',$this->organ_codig);
		$criteria->compare('tgene_cargo',$this->tgene_cargo);
		$criteria->compare('tusua_telef',$this->tusua_telef,true);
		$criteria->compare('tusua_obser',$this->tusua_obser,true);
		$criteria->compare('tusua_fcrea',$this->tusua_fcrea,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tusuario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
