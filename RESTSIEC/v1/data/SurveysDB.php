<?php

class SurveysDB {

	final public function __construct() {}

	const SURVEY_OPEN = 35;
	const TRIMESTER_OPEN = 13;
	const SURVEY_CLOSE = 36;
	const ASIGN_PERMIT1 = 17;
	const ASIGN_PERMIT2 = 18;

	public function getCargaTrabajo($user) {
		/*Se busca la cedula del usuario*/
		$query = new BasicQuerysDB();
		$result = $query->getUser($user);
		/*Se buscan las asignaciones para esa cedula*/
		$selectParams = array();
		$selectParams["cedula"] = $result["cedula"];
		$selectParams["asignado"] = self::ASIGN_PERMIT1;
		$selectParams["procesado"] = self::ASIGN_PERMIT2;
		$connDB = new Connect();
		$result = $connDB->search("SELECT asign_codig, carga_codig, tgene_statu FROM asignacion WHERE perso_cedul = :cedula AND carga_codig IS NOT NULL AND (tgene_statu = :asignado OR tgene_statu = :procesado)",$selectParams,false);
		$connDB->close();
		if(!isset($result) || empty($result)) {
			return null;
		} else {
			$asignacion = array();
			$rowCount = 0;
			foreach ($result as $row) {
				if($this->isSurveyOpen($row["asign_codig"])){
					$asignacion[$rowCount]["encuestaID"] = $row["asign_codig"];
					$carga_asign = $this->getCargaData($row["carga_codig"]);
					$encuestaData = $this->getEncuesta($row["asign_codig"],0);
					if(isset($encuestaData) || !empty($encuestaData)) {
						$data = $this->dataSurveyToJson($encuestaData,$carga_asign["empresa"]["nombreComercial"]);
						$asignacion[$rowCount] = array_merge($asignacion[$rowCount],$data);
					} else {
						$asignacion[$rowCount]["empresa"] = $carga_asign["empresa"];
					}
					$asignacion[$rowCount]["trimestre"] = $carga_asign["trimestre"];
					$rowCount = $rowCount + 1;
				}
			}
			return $asignacion;
		}
	}

	private function dataSurveyToJson($data,$nombreEmpre){
		$basicquery = new BasicQuerysDB();
		$encuesta = $data["encuesta"];
		$cuantitativos = $data["cuantitativos"];
		$survey = array();
		$survey["empresa"]["nombreComercial"] = $nombreEmpre;
		$survey["empresa"]["razonSocial"] = $encuesta["encue_razon"];
		$survey["empresa"]["rif"] = $encuesta["encue_norif"];
		$survey["empresa"]["nroempre"] = $encuesta["encue_noemp"];
		$survey["empresa"]["estrato"] = $encuesta["encue_estra"];
		$survey["empresa"]["estado"] = $basicquery->getEstado($encuesta["estad_codig"]);
		$survey["empresa"]["municipio"] = $basicquery->getMunicipio($encuesta["munic_codig"]);
		$survey["empresa"]["parroquia"] = $basicquery->getParroquia($encuesta["parro_codig"]);
		$survey["empresa"]["poblado"] = $encuesta["encue_centr"];
		$survey["empresa"]["segmento"] = $encuesta["encue_segme"];
		$survey["empresa"]["manzana"] = $encuesta["encue_manza"];
		$survey["empresa"]["sector"] = $encuesta["encue_secto"];
		$survey["empresa"]["direccion"] = $encuesta["encue_direc"];
		$survey["empresa"]["telefono1"] = $encuesta["encue_tele1"];
		$survey["empresa"]["telefono2"] = $encuesta["encue_tele2"];
		$survey["empresa"]["movil"] = $encuesta["encue_celul"];
		$survey["empresa"]["email"] = $encuesta["encue_email"];
		$survey["empresa"]["sitioWEB"] = $encuesta["encue_pgweb"];
		$survey["empresa"]["actividadEconomica"] = $encuesta["encue_activ"];
		$survey["empresa"]["caev"] = $basicquery->getCaev($encuesta["tcaev_codig"]);
		$survey["empresa"]["producto1"] = $basicquery->getProducto($encuesta["tprod_codi1"]);
		$survey["empresa"]["producto2"] = $basicquery->getProducto($encuesta["tprod_codi2"]);

		$survey["informante"]["identificacion"] = $encuesta["encue_infno"];
		$survey["informante"]["cargo"] = $encuesta["encue_infca"];
		$survey["informante"]["telefono"] = $encuesta["encue_inftl"];
		$survey["informante"]["email"] = $encuesta["encue_infem"];
		$survey["informante"]["fechaEntregaCuest"] = $encuesta["encue_inffe"];
		$survey["informante"]["fechaDevolCuest"] = $encuesta["encue_inffd"];

		if(isset($encuesta["encue_visi1"]) && !empty($encuesta["encue_visi1"])) {
			$survey["visita1"]["date"] = $encuesta["encue_visi1"];
			if(isset($encuesta["tgene_encs1"])){
				$survey["visita1"]["observation"] = $basicquery->getGenerica($encuesta["tgene_encs1"]);
				if($encuesta["tgene_encs1"] == 23 || $encuesta["tgene_encs1"] == 24 || $encuesta["tgene_encs1"] == 25) {
					$survey["visita1"]["completed"] = false;
				} else {
					$survey["visita1"]["completed"] = true;
				}				
			} else {
				$survey["visita1"]["completed"] = false;
			}
		} else {
			$survey["visita1"] = null;
		}
		if(isset($encuesta["encue_visi2"]) && !empty($encuesta["encue_visi2"])) {
			$survey["visita2"]["date"] = $encuesta["encue_visi2"];
			if(isset($encuesta["tgene_encs2"])){
				$survey["visita2"]["observation"] = $basicquery->getGenerica($encuesta["tgene_encs2"]);
				if($encuesta["tgene_encs2"] == 23 || $encuesta["tgene_encs2"] == 24 || $encuesta["tgene_encs2"] == 25) {
					$survey["visita2"]["completed"] = false;
				} else {
					$survey["visita2"]["completed"] = true;
				}
			} else {
				$survey["visita2"]["completed"] = false;
			}
		} else {
			$survey["visita2"] = null;
		}
		if(isset($encuesta["encue_visi3"]) && !empty($encuesta["encue_visi3"])) {
			$survey["visita3"]["date"] = $encuesta["encue_visi3"];
			if(isset($encuesta["tgene_encs3"])){
				$survey["visita3"]["observation"] = $basicquery->getGenerica($encuesta["tgene_encs3"]);
				if($encuesta["tgene_encs3"] == 23 || $encuesta["tgene_encs3"] == 24 || $encuesta["tgene_encs3"] == 25) {
					$survey["visita3"]["completed"] = false;
				} else {
					$survey["visita3"]["completed"] = true;
				}
			} else {
				$survey["visita3"]["completed"] = false;
			}
		} else {
			$survey["visita3"] = null;
		}

		$survey["empleados"]["cantidad"] = $cuantitativos["tcuan_empno"];
		$survey["empleados"]["sueldos"] = $cuantitativos["tcuan_empsu"];
		$survey["empleados"]["otrosPagos"] = $cuantitativos["tcuan_empot"];
		$survey["empleados"]["costoComplementarioManoDeObra"] = $cuantitativos["tcuan_empco"];
		$survey["empleados"]["total"] = $cuantitativos["tcuan_empto"];
		
		$survey["obreros"]["cantidad"] = $cuantitativos["tcuan_obrno"];
		$survey["obreros"]["sueldos"] = $cuantitativos["tcuan_obrsu"];
		$survey["obreros"]["otrosPagos"] = $cuantitativos["tcuan_obrot"];
		$survey["obreros"]["costoComplementarioManoDeObra"] = $cuantitativos["tcuan_obrco"];
		$survey["obreros"]["total"] = $cuantitativos["tcuan_obrto"];
		
		$survey["inventarioProductos"]["valorProduccion"] = $cuantitativos["tcuan_valpr"];
		$survey["inventarioProductos"]["valorVentas"] = $cuantitativos["tcuan_valve"];
		$survey["inventarioProductos"]["exportaciones"]["valor1"] = $cuantitativos["tcuan_valbs"];
		$survey["inventarioProductos"]["exportaciones"]["valor2"] = $cuantitativos["tcuan_valdo"];
		$survey["inventarioProductos"]["inventarioProductosTerminados"]["valor1"] = $cuantitativos["tcuan_terin"];
		$survey["inventarioProductos"]["inventarioProductosTerminados"]["valor2"] = $cuantitativos["tcuan_terfi"];
		$survey["inventarioProductos"]["inventarioProductosEnProceso"]["valor1"] = $cuantitativos["tcuan_proin"];
		$survey["inventarioProductos"]["inventarioProductosEnProceso"]["valor2"] = $cuantitativos["tcuan_profi"];

		$survey["materiaPrimaNacional"]["valorMateriaPrima"] = $cuantitativos["tcuan_nacmp"];
		$survey["materiaPrimaNacional"]["valorEnvases"] = $cuantitativos["tcuan_nacem"];
		$survey["materiaPrimaNacional"]["valorCompraMateriaPrima"]["valor1"] = $cuantitativos["tcuan_nambs"];
		$survey["materiaPrimaNacional"]["valorCompraMateriaPrima"]["valor2"] = $cuantitativos["tcuan_namdo"];
		$survey["materiaPrimaNacional"]["valorConsumoMateriaPrima"]["valor1"] = $cuantitativos["tcuan_nacbs"];
		$survey["materiaPrimaNacional"]["valorConsumoMateriaPrima"]["valor2"] = $cuantitativos["tcuan_nacdo"];

		$survey["materiaPrimaImportadaDollars"]["valorMateriaPrima"] = $cuantitativos["tcuan_impmp"];
		$survey["materiaPrimaImportadaDollars"]["valorEnvases"] = $cuantitativos["tcuan_impem"];
		$survey["materiaPrimaImportadaDollars"]["valorCompraMateriaPrima"]["valor1"] = $cuantitativos["tcuan_immbs"];
		$survey["materiaPrimaImportadaDollars"]["valorCompraMateriaPrima"]["valor2"] = $cuantitativos["tcuan_immdo"];
		$survey["materiaPrimaImportadaDollars"]["valorConsumoMateriaPrima"]["valor1"] = $cuantitativos["tcuan_imcbs"];
		$survey["materiaPrimaImportadaDollars"]["valorConsumoMateriaPrima"]["valor2"] = $cuantitativos["tcuan_imcdo"];

		$survey["materiaPrimaImportadaBs"]["valorMateriaPrima"] = $cuantitativos["tcuan_naci1"];
		$survey["materiaPrimaImportadaBs"]["valorEnvases"] = $cuantitativos["tcuan_naci2"];
		$survey["materiaPrimaImportadaBs"]["valorCompraMateriaPrima"]["valor1"] = $cuantitativos["tcuan_impo1"];
		$survey["materiaPrimaImportadaBs"]["valorCompraMateriaPrima"]["valor2"] = $cuantitativos["tcuan_impo2"];
		$survey["materiaPrimaImportadaBs"]["valorConsumoMateriaPrima"]["valor1"] = $cuantitativos["tcuan_impo3"];
		$survey["materiaPrimaImportadaBs"]["valorConsumoMateriaPrima"]["valor2"] = $cuantitativos["tcuan_impo4"];

		$survey["observaciones"] = $cuantitativos["tcuan_obser"];

		return $survey;
	}

	private function getCargaData($cargaTrabajoId) {
		$selectParams = array();
		$selectParams["id"] = $cargaTrabajoId;
		$selectParams["trimester_sta"] = self::TRIMESTER_OPEN;
		$connDB = new Connect();
		$result = $connDB->search("SELECT ct.empre_codig AS empresa, ct.trime_codig AS trimestre, ct.tgene_statu FROM trimestre AS tri INNER JOIN cargatrabajo AS ct ON tri.trime_codig = ct.trime_codig WHERE tri.tgene_statu = :trimester_sta AND ct.carga_codig = :id",$selectParams,false);
		$connDB->close();
		if(!isset($result) || empty($result)) {
			return null;
		} else {
			$cargatrabajo = $result[0];

			$query = new BasicQuerysDB();
			$cargatrabajo["trimestre"] = $query->getTrimestre($cargatrabajo["trimestre"]);
			$cargatrabajo["empresa"] = $this->getEmpresa($cargatrabajo["empresa"]);

			return $cargatrabajo;
		}
	}

	private function isSurveyOpen($asignacion){
		$connDB = new Connect();
		$selectParams = array();
		$selectParams["id"] = $asignacion;
		$result = $connDB->search("SELECT encue_statu FROM encuesta WHERE asign_codig = :id",$selectParams,false);
		if(!isset($result) || empty($result)) {
			return true;
		} else {
			return ($result[0]["encue_statu"] == self::SURVEY_OPEN) ? true : false;
		}
	}

	private function getAsignStatus($asignacion) {
		$connDB = new Connect();
		$selectParams = array();
		$selectParams["id"] = $asignacion;
		$result = $connDB->search("SELECT tgene_statu FROM asignacion WHERE asign_codig = :id",$selectParams,false);
		if(!isset($result) || empty($result)) {
			return true;
		} else {
			return $result[0]["tgene_statu"];
		}		
	}

	private function getEncuesta($asignacion,$status) {
		$connDB = new Connect();
		$selectParams = array();
		$selectParams["id"] = $asignacion;
		if($status == -1) {
			$result = $connDB->search("SELECT * FROM encuesta WHERE asign_codig = :id",$selectParams,false);
		} else {
			if($status == 0) {
				$status = self::SURVEY_OPEN;
			} else {
				$status = self::SURVEY_CLOSE;
			}
			$selectParams["status"] = $status;
			$result = $connDB->search("SELECT * FROM encuesta WHERE asign_codig = :id AND encue_statu = :status",$selectParams,false);
		}
		if(!isset($result) || empty($result)) {
			$connDB->close();
			return null;
		} else {
			$encuesta = array();
			$encuesta["encuesta"] = $result[0];
			$selectParams = array();
			$selectParams["id"] = $asignacion;
			$result = $connDB->search("SELECT * FROM tcuantitativos WHERE asign_codig = :id",$selectParams,false);
			$connDB->close();
			if(isset($result) && !empty($result)) {
				$encuesta["cuantitativos"] = $result[0];
			}
			return $encuesta;
		}
	}

	private function getEmpresa($empresaId) {
		$selectParams = array();
		$selectParams["id"] = $empresaId;
		$connDB = new Connect();
		$result = $connDB->search("SELECT empre_codig, empre_descr, empre_norif, estad_codig, munic_codig, parro_codig, empre_direc, tcaev_codig, tprod_codi1, tprod_codi2, empre_cpobl, empre_segme, empre_manza, empre_secto, empre_nremp, empre_estra FROM empresa WHERE empre_codig = :id",$selectParams,false);
		$connDB->close();
		if(!isset($result) || empty($result)) {
			return null;
		} else {
			$empresa = $result[0];
			
			$basicquery = new BasicQuerysDB();
			$estado = $basicquery->getEstado($empresa["estad_codig"]);
			$municipio = $basicquery->getMunicipio($empresa["munic_codig"]);
			$parroquia = $basicquery->getParroquia($empresa["parro_codig"]);
			$caev = $basicquery->getCaev($empresa["tcaev_codig"]);
			$prod1 = $basicquery->getProducto($empresa["tprod_codi1"]);
			$prod2 = $basicquery->getProducto($empresa["tprod_codi2"]);

			$empresaFull = array();
			$empresaFull["empresaID"] =  $empresa["empre_codig"];
			$empresaFull["nombreComercial"] =  $empresa["empre_descr"];
			$empresaFull["rif"] =  $empresa["empre_norif"];
			$empresaFull["nroempre"] =  $empresa["empre_nremp"];
			$empresaFull["estrato"] =  $empresa["empre_estra"];
			$empresaFull["estado"] =  array("id"=>$estado["id"],"nombre"=>$estado["nombre"]);
			$empresaFull["municipio"] =  array("id"=>$municipio["id"],"nombre"=>$municipio["nombre"]);
			$empresaFull["parroquia"] =  array("id"=>$parroquia["id"],"nombre"=>$parroquia["nombre"]);
			$empresaFull["poblado"] =  $empresa["empre_cpobl"];
			$empresaFull["segmento"] =  $empresa["empre_segme"];
			$empresaFull["manzana"] =  $empresa["empre_manza"];
			$empresaFull["sector"] =  $empresa["empre_secto"];
			$empresaFull["direccion"] =  $empresa["empre_direc"];
			$empresaFull["caev"] =  $caev;
			$empresaFull["producto1"] = $prod1;
			$empresaFull["producto2"] = $prod2;

			return $empresaFull;
		}
	}

	public function sincSurveys($user,$apkv,$dataToSinc) {
		$basicquery = new BasicQuerysDB();
		$userId = $basicquery->getUser($user);
		$userId = $userId["userid"];
		$surveys = json_decode($dataToSinc);
		if(!isset($apkv)) {
			$apkv = "1.0";
		}
		$count = 0;
		$message = "";
		if (is_array($surveys)) {
			foreach ($surveys as $survey) {
				try {
					//print_r("sincSurveys".$survey->encuestaID);
					$this->processSurvey($survey,$userId,$user,$apkv);
					//print_r("sincSurveys".$survey->encuestaID);
				} catch (Exception $e) {
					$count++;
					$message .= " | Error ".$count." | msg: ".$e->getMessage();
				}
			}
			//print_r("sincSurveys error message".$message);
			//print_r("sincSurveys cant errors".$count);
			if($count > 0) {
				throw new Exception($message);
			}
		} else {
			$this->processSurvey($surveys,$userId,$user,$apkv);
		}
	}

	private function processSurvey($survey,$userId,$user,$apkv) {
		//print_r("processSurvey".$survey->encuestaID);
		if(isset($survey->encuestaID)) {
			$asignacion = $survey->encuestaID;
			$oldData = $this->getEncuesta($asignacion,-1);
			$idSurvey = -1;
			if(!isset($oldData) || empty($oldData)) {
				$oldData = "No existe data previa";
			} else {
				$idSurvey = $oldData["encuesta"]["encue_codig"];
				$oldData = json_encode($oldData);
			}
			$saved = $this->saveSurvey($userId,$survey, $idSurvey);
			//print_r("processSurvey".$survey->encuestaID.$saved);
			if($saved) {
				//GUARDA LAS TRAZA ASOCIADAS A LA SINCRONIZACION DE LA ENCUESTA
				$data = array();
				$connDB = new Connect();
				/*$id = 0;
				$result = $connDB->search("SELECT max(sincr_codig) AS id FROM sincronizacion");
				if(!isset($result) || empty($result)) {
					$id = 1;
				} else {
					$id = $result[0]["id"] + 1;
				}
				$data["sincr_codig"] = $id;*/
				$data["asign_codig"] = $asignacion;
				$data["sincr_apkvs"] = $apkv;
				$data["usuar_codig"] = $userId;
				$data["sincr_fcrea"] = array("function","now()");
				$connDB->insert("sincronizacion",$data);
				$connDB->close();
				$basicquery = new BasicQuerysDB();
				$basicquery->saveOperation("S","Sincronizando carga de trabajo user=".$user. " asign_codig=".$asignacion ,$oldData, $user);
			}
		} else {
			throw new Exception("Se debe indicar el codigo de encuesta a procesar");
		}
	}

	/*SOLO AGREGAR Y EDITAR, NO ELIMINAR*/
	/*SINCRONIZACION Completa, incompleta, acceso denegado*/
	private function saveSurvey($userId,$survey, $idSurvey) {
		//print_r("saveSurvey".$survey->encuestaID);
		$save = $this->dataSurveyFromJson($survey);
		if(isset($save) && !empty($save)) {
			$affectedRows = 0;
			if($idSurvey != -1) {
				$selectParams = array();
				$selectParams["encue_codig"] = $idSurvey;
				$selectParams["asign_codig"] = $survey->encuestaID;
				$selectParams["encue_statu"] = self::SURVEY_OPEN;
				$data = array();
				$data["usuar_codig"] = $userId;
				//$data["encue_fcrea"] = array("function","now()");
				$data = array_merge($data,$save["encuesta"]);
				$connDB = new Connect();
				$affectedRows = $connDB->update("encuesta",$data,$selectParams);
				$connDB->close();

				//CUANTITATIVOS
				if($affectedRows > 0 && isset($save["cuantitativos"]) && !empty($save["cuantitativos"])) {
					$selectParams = array();
					$selectParams["asign_codig"] = $survey->encuestaID;
					$data = array();
					$data["usuar_codig"] = $userId;
					$data["tcuan_fcrea"] = array("function","now()");
					$data = array_merge($data,$save["cuantitativos"]);
					$connDB = new Connect();
					$connDB->update("tcuantitativos",$data,$selectParams);
					$connDB->close();
				}
			} else {
				$data = array();
				$asignStatus = $this->getAsignStatus($survey->encuestaID);
				if($asignStatus == self::ASIGN_PERMIT1 || $asignStatus == self::ASIGN_PERMIT2) {
					$data["asign_codig"] = $survey->encuestaID;
					$data["usuar_codig"] = $userId;
					$data["encue_fcrea"] = array("function","now()");
					$data = array_merge($data,$save["encuesta"]);
					$connDB = new Connect();
					$affectedRows = $connDB->insert("encuesta",$data);
					$connDB->close();

					//CUANTITATIVOS
					if($affectedRows > 0 && isset($save["cuantitativos"]) && !empty($save["cuantitativos"])) {
						$data = array();
						$data["asign_codig"] = $survey->encuestaID;
						$data["usuar_codig"] = $userId;
						$data["tcuan_fcrea"] = array("function","now()");
						$data = array_merge($data,$save["cuantitativos"]);
						$connDB = new Connect();
						$connDB->insert("tcuantitativos",$data);
						$connDB->close();
					}
				} else {
					throw new Exception("La encuesta no se pudo sincronizar porque el trimestre fue eliminado o cerrado");
				}
			}
			return true;
		}
		return false;
	}

	private function dataSurveyFromJson($survey) {
		//print_r("dataSurveyFromJson".$survey->encuestaID);
		$empresa = $survey->empresa;
		//$trimestre = $survey->trimestre; //hacer metodo para buscar el trimestre
		$empleados = $survey->empleados;
		$obreros = $survey->obreros;
		$informante = $survey->informante;
		$materiaPrimaImportadaDollars = $survey->materiaPrimaImportadaDollars;
		$materiaPrimaImportadaBs = $survey->materiaPrimaImportadaBs;
		$materiaPrimaNacional = $survey->materiaPrimaNacional;
		$produccion = $survey->inventarioProductos;

		if(isset($survey->visita1) && !empty($survey->visita1)) {
			$visita1 = $survey->visita1;
		}
		if(isset($survey->visita2) && !empty($survey->visita2)) {
			$visita2 = $survey->visita2;
		}
		if(isset($survey->visita3) && !empty($survey->visita3)) {
			$visita3 = $survey->visita3;
		}

		$data=array();
		$cantVisitas = 0;
		$closeSurvey = self::SURVEY_OPEN;
		if(isset($visita1) && !empty($visita1)) {
			if(isset($visita1->completed) && $visita1->completed == false) {
				if(isset($visita1->observation) && !empty($visita1->observation)) {
					if($visita1->observation->id > 0) {
						$data["tgene_encs1"] = $visita1->observation->id;
						if($visita1->observation->id == 23 || $visita1->observation->id == 24 || $visita1->observation->id == 25) {
							$closeSurvey = self::SURVEY_OPEN;
						} else {
							$closeSurvey = self::SURVEY_CLOSE;
						}
					}
				} else {
					//throw new Exception("Debe indicar el motivo por el cual no se completo la 1era vista con exito EMPRESA ".$empresa->nombreComercial);
				}
			} else if(isset($visita1->completed) && $visita1->completed == true) {
				if(isset($visita1->observation) && !empty($visita1->observation) && $visita1->observation->id > 0) {
					$data["tgene_encs1"] = $visita1->observation->id;
				}
				$closeSurvey = self::SURVEY_CLOSE;
			}
			if(isset($visita1->date) && !empty($visita1->date)) {
				$date = DateTime::createFromFormat('M j, Y h:i:s A', $visita1->date);
				$data["encue_visi1"] = $date->format('Y-m-d');
			}
			if(isset($visita2) && !empty($visita2)) {
				if(isset($visita2->completed) && $visita2->completed == false) {
					if(isset($visita2->observation) && !empty($visita2->observation)) {
						if($visita2->observation->id > 0) {
							$data["tgene_encs2"] = $visita2->observation->id;
							if($visita2->observation->id == 23 || $visita2->observation->id == 24 || $visita2->observation->id == 25) {
								$closeSurvey = self::SURVEY_OPEN;
							} else {
								$closeSurvey = self::SURVEY_CLOSE;
							}
						}
					} else {
						//throw new Exception("Debe indicar el motivo por el cual no se completo la 1era vista con exito EMPRESA ".$empresa->nombreComercial);
					}
				} else if(isset($visita2->completed) && $visita2->completed == true){
					if(isset($visita2->observation) && !empty($visita2->observation) && $visita2->observation->id > 0) {
						$data["tgene_encs2"] = $visita2->observation->id;
					}
					$closeSurvey = self::SURVEY_CLOSE;
				}
				if(isset($visita2->date) && !empty($visita2->date)) {
					$date = DateTime::createFromFormat('M j, Y h:i:s A', $visita1->date);
					$data["encue_visi2"] = $date->format('Y-m-d');
				}
				if(isset($visita3) && !empty($visita3)) {
					if(isset($visita3->completed) && $visita3->completed == false) {
						if(isset($visita3->observation) && !empty($visita3->observation)) {
							if($visita3->observation->id > 0) {
								$data["tgene_encs3"] = $visita3->observation->id;
								if($visita3->observation->id == 23 || $visita3->observation->id == 24 || $visita3->observation->id == 25) {
									$closeSurvey = self::SURVEY_OPEN;
								} else {
									$closeSurvey = self::SURVEY_CLOSE;
								}
							}
						} else {
							//throw new Exception("Debe indicar el motivo por el cual no se completo la 1era vista con exito EMPRESA ".$empresa->nombreComercial);
						}
					} else if(isset($visita3->completed) && $visita3->completed == true) {
						if(isset($visita3->observation) && !empty($visita3->observation) && $visita3->observation->id > 0) {
							$data["tgene_encs3"] = $visita3->observation->id;
						}
						$closeSurvey = self::SURVEY_CLOSE;
					}
					if(isset($visita3->date) && !empty($visita3->date)) {
						$date = DateTime::createFromFormat('M j, Y h:i:s A', $visita1->date);
						$data["encue_visi3"] = $date->format('Y-m-d');
					}
				}
			}
		} else {
			return null; //No tiene informacion de visitas registradas por lo que no se guardara en base de datos
		}
		$data["encue_statu"] = $closeSurvey;
		if(isset($empresa) && !empty($empresa)) {
			$data["estad_codig"] = $empresa->estado->id;
			$data["munic_codig"] = $empresa->municipio->id;
			$data["parro_codig"] = $empresa->parroquia->id;
			if(isset($empresa->rif) && ($empresa->rif == "0" || !empty($empresa->rif))) {
				$data["encue_norif"] = array("string",$empresa->rif);
			} else {
				throw new Exception("Debe indicar el Rif de la empresa ".$empresa->nombreComercial);
			}
			if(isset($empresa->poblado) && ($empresa->poblado == "0" || !empty($empresa->poblado))) {
				$data["encue_centr"] = array("string",$empresa->poblado);
			} else {
				throw new Exception("Debe indicar el centro o poblado de la empresa ".$empresa->nombreComercial);
			}
			if(isset($empresa->segmento) && ($empresa->segmento == "0" || !empty($empresa->segmento))) {
				$data["encue_segme"] = array("string",$empresa->segmento);
			} else {
				throw new Exception("Debe indicar el segmento de la empresa ".$empresa->nombreComercial);
			}
			if(isset($empresa->manzana) && ($empresa->manzana == "0" || !empty($empresa->manzana))) {
				$data["encue_manza"] = array("string",$empresa->manzana);
			} else {
				throw new Exception("Debe indicar la manzana de la empresa ".$empresa->nombreComercial);
			}
			if(isset($empresa->sector) && ($empresa->sector == "0" || !empty($empresa->sector))) {
				$data["encue_secto"] = array("string",$empresa->sector);
			} else {
				throw new Exception("Debe indicar el sector de la empresa ".$empresa->nombreComercial);
			}
			if(isset($empresa->nroempre) && !empty($empresa->nroempre)) {
				$data["encue_noemp"] = $empresa->nroempre;
			}
			if(isset($empresa->estrato) && !empty($empresa->estrato)) {
				$data["encue_estra"] = $empresa->estrato;
			}
			if(isset($empresa->razonSocial) && !empty($empresa->razonSocial)) {
				$data["encue_razon"] = $empresa->razonSocial;
			}
			if(isset($empresa->direccion) && !empty($empresa->direccion) ) {
				$data["encue_direc"] = $empresa->direccion;
			}
			if(isset($empresa->telefono1) && !empty($empresa->telefono1)) {
				$data["encue_tele1"] = $empresa->telefono1;
			}
			if(isset($empresa->telefono2) && !empty($empresa->telefono2)) {
				$data["encue_tele2"] = $empresa->telefono2;
			}
			if(isset($empresa->movil) && !empty($empresa->movil)) {
				$data["encue_celul"] = $empresa->movil;
			}
			if(isset($empresa->email) && !empty($empresa->email)) {
				$data["encue_email"] = $empresa->email;
			}
			if(isset($empresa->sitioWEB) && !empty($empresa->sitioWEB)) {
				$data["encue_pgweb"] = $empresa->sitioWEB;
			}
			if(isset($empresa->actividadEconomica) && !empty($empresa->actividadEconomica)) {
				$data["encue_activ"] = $empresa->actividadEconomica;
			}
			$basicquery = new BasicQuerysDB();
			if(isset($empresa->caev) && !empty($empresa->caev) ) {
				if(isset($empresa->caev->id) && !empty($empresa->caev->id)) {
					$data["tcaev_codig"] = $empresa->caev->id;
					$data["encue_dcaev"] = $empresa->caev->tcaev_descr;
				} else {
					if(isset($empresa->caev->codigoCAEV) && !empty($empresa->caev->codigoCAEV)) {
						$id = $basicquery->getCaevId($empresa->caev->codigoCAEV);
						$data["tcaev_codig"] = $id["tcaev_codig"];
						$data["encue_dcaev"] = $id["tcaev_descr"];
					}
				}
			}
			if(isset($empresa->producto1) && !empty($empresa->producto1) ) {
				if(isset($empresa->producto1->id) && !empty($empresa->producto1->id)) {
					$data["tprod_codi1"] = $empresa->producto1->id;
				} else {
					if(isset($empresa->producto1->codigoCPV) && !empty($empresa->producto1->codigoCPV)) {
						$id = $basicquery->getProductId($empresa->producto1->codigoCPV);
						$data["tprod_codi1"] = $id["tprod_codig"];
					}
				}
			} else {
				//throw new Exception("Debe indicar el 1er producto de la empresa ".$empresa->nombreComercial);
			}
			if(isset($empresa->producto2) && !empty($empresa->producto2)) {
				if(isset($empresa->producto2->id) && !empty($empresa->producto2->id)) {
					$data["tprod_codi2"] = $empresa->producto2->id;
				} else {
					if(isset($empresa->producto2->codigoCPV) && !empty($empresa->producto2->codigoCPV)) {
						$id = $basicquery->getProductId($empresa->producto2->codigoCPV);
						$data["tprod_codi2"] = $id["tprod_codig"];
					}
				}
			} else {
				//throw new Exception("Debe indicar el 2do producto de la empresa ".$empresa->nombreComercial);
			}
		} else{
			//throw new Exception("Debe suministrar la información de la empresa ".$empresa->nombreComercial);
		}
		if(isset($informante) && !empty($informante)) {
			if(isset($informante->identificacion) && !empty($informante->identificacion)) {
				$data["encue_infno"] = $informante->identificacion;
			}
			if(isset($informante->cargo) && !empty($informante->cargo)) {
				$data["encue_infca"] = $informante->cargo;
			}
			if(isset($informante->telefono) && !empty($informante->telefono)) {
				$data["encue_inftl"] = $informante->telefono;
			}
			if(isset($informante->email) && !empty($informante->email)) {
				$data["encue_infem"] = $informante->email;
			}
			$data["encue_inffe"] = array("function","now()");
			$data["encue_inffd"] = array("function","now()");
		}
		$result = array();
		$result["encuesta"] = $data; 

		$data = array();
		if(isset($survey->observaciones) && !empty($survey->observaciones)) {
			$data["tcuan_obser"] = $survey->observaciones;
		}
		if(isset($empleados) && !empty($empleados)) {
			$total = 0;
			if(isset($empleados->cantidad) && !empty($empleados->cantidad)) {
				$data["tcuan_empno"] = $empleados->cantidad;
			}
			if(isset($empleados->sueldos) && !empty($empleados->sueldos)) {
				$data["tcuan_empsu"] = $empleados->sueldos;
				$total = $total + $empleados->sueldos;
			}
			if(isset($empleados->otrosPagos) && !empty($empleados->otrosPagos)) {
				$data["tcuan_empot"] = $empleados->otrosPagos;
				$total = $total + $empleados->otrosPagos;
			}
			if(isset($empleados->costoComplementarioManoDeObra) && !empty($empleados->costoComplementarioManoDeObra)) {
				$data["tcuan_empco"] = $empleados->costoComplementarioManoDeObra;
				$total = $total + $empleados->costoComplementarioManoDeObra;
			}
			$data["tcuan_empto"] = $total;
		}		
		if(isset($obreros) && !empty($obreros)) {
			$total = 0;
			if(isset($obreros->cantidad) && !empty($obreros->cantidad)) {
				$data["tcuan_obrno"] = $obreros->cantidad;
			}
			if(isset($obreros->sueldos) && !empty($obreros->sueldos)) {
				$data["tcuan_obrsu"] = $obreros->sueldos;
				$total = $total + $obreros->sueldos;
			}
			if(isset($obreros->otrosPagos) && !empty($obreros->otrosPagos)) {
				$data["tcuan_obrot"] = $obreros->otrosPagos;
				$total = $total + $obreros->otrosPagos;
			}
			if(isset($obreros->costoComplementarioManoDeObra) && !empty($obreros->costoComplementarioManoDeObra)) {
				$data["tcuan_obrco"] = $obreros->costoComplementarioManoDeObra;
				$total = $total + $obreros->costoComplementarioManoDeObra;
			}			
			$data["tcuan_obrto"] = $total;
		}

		if(isset($produccion) && !empty($produccion)) {
			if(isset($produccion->valorProduccion) && !empty($produccion->valorProduccion)){
				$data["tcuan_valpr"] = $produccion->valorProduccion;
			}
			if(isset($produccion->valorVentas) && !empty($produccion->valorVentas)){
				$data["tcuan_valve"] = $produccion->valorVentas;
			}
			if(isset($produccion->exportaciones) && !empty($produccion->exportaciones)){
				if(isset($produccion->exportaciones->valor1) && !empty($produccion->exportaciones->valor1)){
					$data["tcuan_valdo"] = $produccion->exportaciones->valor1;
				}
				if(isset($produccion->exportaciones->valor2) && !empty($produccion->exportaciones->valor2)){
					$data["tcuan_valbs"] = $produccion->exportaciones->valor2;
				}
			}
			if(isset($produccion->inventarioProductosTerminados) && !empty($produccion->inventarioProductosTerminados)){
				if(isset($produccion->inventarioProductosTerminados->valor1) && !empty($produccion->inventarioProductosTerminados->valor1)){
					$data["tcuan_terin"] = $produccion->inventarioProductosTerminados->valor1;
				}
				if(isset($produccion->inventarioProductosTerminados->valor2) && !empty($produccion->inventarioProductosTerminados->valor2)){
					$data["tcuan_terfi"] = $produccion->inventarioProductosTerminados->valor2;
				}
			}
			if(isset($produccion->inventarioProductosEnProceso) && !empty($produccion->inventarioProductosEnProceso)){
				if(isset($produccion->inventarioProductosEnProceso->valor1) && !empty($produccion->inventarioProductosEnProceso->valor1)){
					$data["tcuan_proin"] = $produccion->inventarioProductosEnProceso->valor1;
				}
				if(isset($produccion->inventarioProductosEnProceso->valor2) && !empty($produccion->inventarioProductosEnProceso->valor2)){
					$data["tcuan_profi"] = $produccion->inventarioProductosEnProceso->valor2;
				}
			}
		}

		if(isset($materiaPrimaNacional) && !empty($materiaPrimaNacional)) {
			if(isset($materiaPrimaNacional->valorMateriaPrima) && !empty($materiaPrimaNacional->valorMateriaPrima)) {
				$data["tcuan_nacmp"] = $materiaPrimaNacional->valorMateriaPrima;
			}
			if(isset($materiaPrimaNacional->valorEnvases) && !empty($materiaPrimaNacional->valorEnvases)){
				$data["tcuan_nacem"] = $materiaPrimaNacional->valorEnvases;
			}
			if(isset($materiaPrimaNacional->valorCompraMateriaPrima) && !empty($materiaPrimaNacional->valorCompraMateriaPrima)){
				if(isset($materiaPrimaNacional->valorCompraMateriaPrima->valor2) && !empty($materiaPrimaNacional->valorCompraMateriaPrima->valor2)){
					$data["tcuan_namdo"] = $materiaPrimaNacional->valorCompraMateriaPrima->valor2;
				}
				if(isset($materiaPrimaNacional->valorCompraMateriaPrima->valor1) && !empty($materiaPrimaNacional->valorCompraMateriaPrima->valor1)){
					$data["tcuan_nambs"] = $materiaPrimaNacional->valorCompraMateriaPrima->valor1;
				}
			}
			if(isset($materiaPrimaNacional->valorConsumoMateriaPrima) && !empty($materiaPrimaNacional->valorConsumoMateriaPrima)){
				if(isset($materiaPrimaNacional->valorConsumoMateriaPrima->valor2) && !empty($materiaPrimaNacional->valorConsumoMateriaPrima->valor2)){
					$data["tcuan_nacdo"] = $materiaPrimaNacional->valorConsumoMateriaPrima->valor2;
				}
				if(isset($materiaPrimaNacional->valorConsumoMateriaPrima->valor1) && !empty($materiaPrimaNacional->valorConsumoMateriaPrima->valor1)){
					$data["tcuan_nacbs"] = $materiaPrimaNacional->valorConsumoMateriaPrima->valor1;
				}
			}
		}

		if(isset($materiaPrimaImportadaDollars) && !empty($materiaPrimaImportadaDollars)) {
			if(isset($materiaPrimaImportadaDollars->valorMateriaPrima) && !empty($materiaPrimaImportadaDollars->valorMateriaPrima)) {
				$data["tcuan_impmp"] = $materiaPrimaImportadaDollars->valorMateriaPrima;
			}
			if(isset($materiaPrimaImportadaDollars->valorEnvases) && !empty($materiaPrimaImportadaDollars->valorEnvases)){
				$data["tcuan_impem"] = $materiaPrimaImportadaDollars->valorEnvases;
			}
			if(isset($materiaPrimaImportadaDollars->valorCompraMateriaPrima) && !empty($materiaPrimaImportadaDollars->valorCompraMateriaPrima)){
				if(isset($materiaPrimaImportadaDollars->valorCompraMateriaPrima->valor2) && !empty($materiaPrimaImportadaDollars->valorCompraMateriaPrima->valor2)){
					$data["tcuan_immdo"] = $materiaPrimaImportadaDollars->valorCompraMateriaPrima->valor2;
				}
				if(isset($materiaPrimaImportadaDollars->valorCompraMateriaPrima->valor1) && !empty($materiaPrimaImportadaDollars->valorCompraMateriaPrima->valor1)){
					$data["tcuan_immbs"] = $materiaPrimaImportadaDollars->valorCompraMateriaPrima->valor1;
				}
			}
			if(isset($materiaPrimaImportadaDollars->valorConsumoMateriaPrima) && !empty($materiaPrimaImportadaDollars->valorConsumoMateriaPrima)){
				if(isset($materiaPrimaImportadaDollars->valorConsumoMateriaPrima->valor2) && !empty($materiaPrimaImportadaDollars->valorConsumoMateriaPrima->valor2)){
					$data["tcuan_imcdo"] = $materiaPrimaImportadaDollars->valorConsumoMateriaPrima->valor2;
				}
				if(isset($materiaPrimaImportadaDollars->valorConsumoMateriaPrima->valor1) && !empty($materiaPrimaImportadaDollars->valorConsumoMateriaPrima->valor1)){
					$data["tcuan_imcbs"] = $materiaPrimaImportadaDollars->valorConsumoMateriaPrima->valor1;
				}
			}
		}

		if(isset($materiaPrimaImportadaBs) && !empty($materiaPrimaImportadaBs)) {
			if(isset($materiaPrimaImportadaBs->valorMateriaPrima) && !empty($materiaPrimaImportadaBs->valorMateriaPrima)){
				$data["tcuan_naci1"] = $materiaPrimaImportadaBs->valorMateriaPrima;
			}
			if(isset($materiaPrimaImportadaBs->valorEnvases) && !empty($materiaPrimaImportadaBs->valorEnvases)){
				$data["tcuan_naci2"] = $materiaPrimaImportadaBs->valorEnvases;
			}
			if(isset($materiaPrimaImportadaBs->valorCompraMateriaPrima) && !empty($materiaPrimaImportadaBs->valorCompraMateriaPrima)){
				if(isset($materiaPrimaImportadaBs->valorCompraMateriaPrima->valor1) && !empty($materiaPrimaImportadaBs->valorCompraMateriaPrima->valor1)){
					$data["tcuan_impo1"] = $materiaPrimaImportadaBs->valorCompraMateriaPrima->valor1;
				}
				if(isset($materiaPrimaImportadaBs->valorCompraMateriaPrima->valor2) && !empty($materiaPrimaImportadaBs->valorCompraMateriaPrima->valor2)){
					$data["tcuan_impo2"] = $materiaPrimaImportadaBs->valorCompraMateriaPrima->valor2;
				}
			}
			if(isset($materiaPrimaImportadaBs->valorConsumoMateriaPrima) && !empty($materiaPrimaImportadaBs->valorConsumoMateriaPrima)){
				if(isset($materiaPrimaImportadaBs->valorConsumoMateriaPrima->valor1) && !empty($materiaPrimaImportadaBs->valorConsumoMateriaPrima->valor1)){
					$data["tcuan_impo3"] = $materiaPrimaImportadaBs->valorConsumoMateriaPrima->valor1;
				}
				if(isset($materiaPrimaImportadaBs->valorConsumoMateriaPrima->valor2) && !empty($materiaPrimaImportadaBs->valorConsumoMateriaPrima->valor2)){
					$data["tcuan_impo4"] = $materiaPrimaImportadaBs->valorConsumoMateriaPrima->valor2;
				}
			}
		}

		$result["cuantitativos"] = $data;
		return $result;
	}
}

?>