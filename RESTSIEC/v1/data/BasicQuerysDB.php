<?php

class BasicQuerysDB {

    final public function __construct() {}

     

	public function searchUser($user,$pass) {
		$selectParams = array();
		$selectParams["user"] = $user;
		$selectParams["pass"] = $pass;
		$selectParams["status"] = 1;
		$connDB = new Connect();
		$result = $connDB->search("SELECT perso_cedul AS cedula FROM usuario WHERE usuar_login = :user AND usuar_passw = :pass AND usuar_statu = :status",$selectParams,false);
		$connDB->close();
		if(!isset($result) || empty($result)) {
			return null;
		} else {
			return $result[0];
		}
	}

	public function getUser($user) {
		$selectParams = array();
		$selectParams["user"] = $user;
		$selectParams["status"] = 1;
		$connDB = new Connect();
		$result = $connDB->search("SELECT usuar_codig AS userid, perso_cedul AS cedula FROM usuario WHERE usuar_login = :user AND usuar_statu = :status",$selectParams,false);
		$connDB->close();
		if(!isset($result) || empty($result)) {
			return null;
		} else {
			return $result[0];
		}
	}

	public function getUserData($personId) {
		$selectParams = array();
		$selectParams["cedula"] = $personId;
		$connDB = new Connect();
		$result = $connDB->search("SELECT perso_pnomb AS pnombre, perso_snomb AS snombre, perso_papel AS papel, perso_sapel AS sapel FROM persona WHERE perso_cedul = :cedula",$selectParams,false);
		$connDB->close();
		if(!isset($result) || empty($result)) {
			return null;
		} else {
			return $result[0];
		}
	}

// ******INFORMACION TOKEN
	public function validUser($user,$token) {
		$selectParams = array();
		$selectParams["user"] = $user;
		$selectParams["token"] = $token;
		$connDB = new Connect();
		$result = $connDB->search("SELECT usuar_login FROM logincontrol WHERE usuar_login = :user AND user_token = :token",$selectParams,false);
		$connDB->close();
		if(!isset($result) || empty($result)) {
			return false;
		} else {
			return true;
		}
	}

	public function registerUser($user, $token) {
		$selectParams = array();
		$selectParams["usuar_login"] = $user;
		$connDB = new Connect();
		$result = $connDB->search("SELECT usuar_login FROM logincontrol WHERE usuar_login = :usuar_login",$selectParams,false);
		if(!isset($result) || empty($result)) {
			$data = array();
			$data["usuar_login"] = $user;
			$data["user_token"] = $token;
			$data["user_access"] = array("function","now()");
			$connDB->insert("logincontrol",$data);
		} else {
			$data = array();
			$data["user_token"] = $token;
			$data["user_access"] = array("function","now()");
			$connDB->update("logincontrol",$data,$selectParams);
		}
		$connDB->close();
	}

	public function unregistUser($user) {
		$selectParams = array();
		$selectParams["usuar_login"] = $user;
		$connDB = new Connect();
		$result = $connDB->delete("logincontrol",$selectParams);
		$connDB->close();
	}
// ******INFORMACION GENERICA
	public function getGenerica($genericaId) {
		$selectParams = array();
		$selectParams["id"] = $genericaId;
		$connDB = new Connect();
		//$result = $connDB->search("SELECT tgene_codig, tgene_descr, tgene_tipod FROM tgenerica WHERE tgene_codig = :id",$selectParams,false);
		$result = $connDB->search("SELECT tgene_codig AS id, tgene_descr AS nombre FROM tgenerica WHERE tgene_codig = :id",$selectParams,false);
		$connDB->close();
		if(!isset($result) || empty($result)) {
			return null;
		} else {
			return $result[0];
		}
	}

	public function getEstatusEncuesta() {
		return $this->getTipoCategoria("encst");
	}

	private function getTipoCategoria($tipo) {
		$selectParams = array();
		$selectParams["tipo"] = $tipo;
		$connDB = new Connect();
		$result = $connDB->search("SELECT tgene_codig AS id, tgene_descr AS nombre FROM tgenerica WHERE tgene_tipod = :tipo",$selectParams,false);
		$connDB->close();
		if(!isset($result) || empty($result)) {
			return null;
		} else {
			return $result;
		}
	}

// ******INFORMACION DE CAEV
	public function getAllActiveCaev() {
		$selectParams = array();
		$selectParams["status"] = 1;
		$connDB = new Connect();
		$result = $connDB->search("SELECT tcaev_codig AS id, tcaev_coine AS codigoCAEV, tcaev_padre, tcaev_descr FROM tcaev WHERE tcaev_statu = :status",$selectParams,false);
		$connDB->close();
		return $result;
	}

	public function getCaev($caevId) {
		$selectParams = array();
		$selectParams["id"] = $caevId;
		$selectParams["status"] = 1;
		$connDB = new Connect();
		$result = $connDB->search("SELECT tcaev_codig AS id, tcaev_coine AS codigoCAEV, tcaev_padre, tcaev_descr FROM tcaev WHERE tcaev_codig = :id AND tcaev_statu = :status",$selectParams,false);
		$connDB->close();
		if(!isset($result) || empty($result)) {
			return null;
		} else {
			return $result[0];
		}
	}

	public function getCaevId($caevId) {
		$selectParams = array();
		$selectParams["id"] = $caevId;
		$selectParams["status"] = 1;
		$connDB = new Connect();
		$result = $connDB->search("SELECT tcaev_codig, tcaev_descr FROM tcaev WHERE tcaev_coine = :id AND tcaev_statu = :status",$selectParams,false);
		$connDB->close();
		if(!isset($result) || empty($result)) {
			return null;
		} else {
			return $result[0];
		}
	}

// ******INFORMACION DE PRODUCTOS
	public function getAllActiveProducts() {
		$selectParams = array();
		$selectParams["status"] = 1;
		$connDB = new Connect();
		$result = $connDB->search("SELECT tprod_codig AS id, tprod_coine AS codigoCPV, tprod_padre, tprod_descr FROM tproducto WHERE tprod_statu = :status",$selectParams,false);
		//$result = $connDB->search("SELECT tprod_coine AS codigoCPV, tprod_padre, tprod_descr FROM tproducto WHERE tprod_statu = :status",$selectParams,false);
		$connDB->close();
		return $result;
	}

	public function getProducto($productoId) {
		$selectParams = array();
		$selectParams["id"] = $productoId;
		$selectParams["status"] = 1;
		$connDB = new Connect();
		//$result = $connDB->search("SELECT tprod_codig AS id, tprod_coine AS codigoCPV, tprod_padre AS padre, tprod_descr AS descripcion FROM tproducto WHERE tprod_codig = :id AND tprod_statu = :status",$selectParams,false);
		$result = $connDB->search("SELECT tprod_codig AS id, tprod_coine AS codigoCPV, tprod_padre, tprod_descr FROM tproducto WHERE tprod_codig = :id AND tprod_statu = :status",$selectParams,false);
		$connDB->close();
		if(!isset($result) || empty($result)) {
			return null;
		} else {
			return $result[0];
		}
	}

	public function getProductId($product) {
		$selectParams = array();
		$selectParams["product"] = $product;
		$selectParams["status"] = 1;
		$connDB = new Connect();
		$result = $connDB->search("SELECT tprod_codig FROM tproducto WHERE tprod_coine = :product AND tprod_statu = :status",$selectParams,false);
		$connDB->close();
		if(!isset($result) || empty($result)) {
			return null;
		} else {
			return $result[0];
		}		
	}

// ******INFORMACION DE ESTADO, MUNICIPIO Y PARROQUIA
	public function getEstado($estadoId) {
		$selectParams = array();
		$selectParams["id"] = $estadoId;
		$connDB = new Connect();
		$result = $connDB->search("SELECT estad_codig AS id, estad_descr AS nombre FROM estado WHERE estad_codig = :id",$selectParams,false);
		$connDB->close();
		if(!isset($result) || empty($result)) {
			return null;
		} else {
			return $result[0];
		}
	}

	public function getMunicipio($municipioId) {
		$selectParams = array();
		$selectParams["municipio"] = $municipioId;
		//$selectParams["estado"] = $estadoId;
		$connDB = new Connect();
		$result = $connDB->search("SELECT munic_codig AS id, munic_descr AS nombre FROM municipio WHERE munic_codig = :municipio",$selectParams,false);
		$connDB->close();
		if(!isset($result) || empty($result)) {
			return null;
		} else {
			return $result[0];
		}
	}

	public function getParroquia($parroquiaId) {
		$selectParams = array();
		$selectParams["parroquia"] = $parroquiaId;
		//$selectParams["estado"] = $estadoId;
		//$selectParams["municipio"] = $municipioId;
		$connDB = new Connect();
		$result = $connDB->search("SELECT parro_codig AS id, parro_descr AS nombre FROM parroquia WHERE parro_codig = :parroquia",$selectParams,false);
		$connDB->close();
		if(!isset($result) || empty($result)) {
			return null;
		} else {
			return $result[0];
		}
	}
// ******INFORMACION DE TRIMESTRE
	public function getTrimestre($trimestreId) {
		$selectParams = array();
		$selectParams["id"] = $trimestreId;
		$connDB = new Connect();
		//$result = $connDB->search("SELECT trime_codig, trime_noano, trime_numer, tgene_statu FROM trimestre WHERE trime_codig = :id",$selectParams,false);
		$result = $connDB->search("SELECT trime_codig AS trimestreID, trime_noano AS anio, trime_numer AS numero FROM trimestre WHERE trime_codig = :id",$selectParams,false);
		$connDB->close();
		if(!isset($result) || empty($result)) {
			return null;
		} else {
/*			$trimestre = $result[0];
			$status = $this->getGenerica($trimestre["tgene_statu"]);
			if(isset($status)) {
				$trimestre["tgene_descr"] = $status["tgene_descr"];
			}
			return $trimestre;*/
			return $result[0];
		}
	}

	public function saveOperation($tipoOper,$descOper,$oldData,$user) {
		$data = array();
		$connDB = new Connect();
		/*$id = 0;
		$result = $connDB->search("SELECT max(toper_codig) AS id FROM toperacion");
		if(!isset($result) || empty($result)) {
			$id = 1;
		} else {
			$id = $result[0]["id"] + 1;
		}
		$data["toper_codig"] = $id;*/
		$userData = $this->getUser($user);
		$data["tgene_tipoo"] = $tipoOper;
		$data["toper_descr"] = $descOper;
		$data["toper_desc2"] = $oldData;
		$data["usuar_codig"] = $userData["userid"];
		$data["toper_fcrea"] = array("function","now()");

		$connDB->insert("toperacion",$data);
		$connDB->close();
	}
	// RAPIDPAGO
	public function getSolicitudes() {
		$selectParams = array();
		$selectParams["status"] = 27;
		$connDB = new Connect();
		$result = $connDB->search("SELECT solic_numer AS Codigo, clien_rsoci AS Cliente FROM solicitud a JOIN cliente b ON (a.clien_codig = b.clien_codig) WHERE estat_codig = :status",$selectParams,false);
		//$result = $connDB->search("SELECT tprod_coine AS codigoCPV, tprod_padre, tprod_descr FROM tproducto WHERE tprod_statu = :status",$selectParams,false);
		$connDB->close();
		return $result;
	}
	public function getPromedioProceso() {
		$selectParams = array();
		$connDB = new Connect();
		$result = $connDB->search("SELECT * FROM rapidpago.v_solicitud_proceso_promedio",$selectParams,false);
		//$result = $connDB->search("SELECT tprod_coine AS codigoCPV, tprod_padre, tprod_descr FROM tproducto WHERE tprod_statu = :status",$selectParams,false);
		$connDB->close();
		return $result;
	}
	public function getRetrasoProceso() {
		$selectParams = array();
		$connDB = new Connect();
		$result = $connDB->search("SELECT * FROM v_solicitud_retraso_proceso",$selectParams,false);
		//$result = $connDB->search("SELECT tprod_coine AS codigoCPV, tprod_padre, tprod_descr FROM tproducto WHERE tprod_statu = :status",$selectParams,false);
		$connDB->close();
		return $result;
	}
	public function getPromedioSolicitud() {

		$selectParams = array();
		$connDB = new Connect();
		$result = $connDB->search("SELECT * FROM rapidpago.v_solicitud_horas_proceso",$selectParams,false);
		//$result = $connDB->search("SELECT tprod_coine AS codigoCPV, tprod_padre, tprod_descr FROM tproducto WHERE tprod_statu = :status",$selectParams,false);
		$connDB->close();
		return $result;
	}
	public function getSemaforoSolicitud() {
		
		$selectParams = array();
		$connDB = new Connect();
		$result = $connDB->search("SELECT * FROM rapidpago.v_solicitud_semaforo",$selectParams,false);
		//$result = $connDB->search("SELECT tprod_coine AS codigoCPV, tprod_padre, tprod_descr FROM tproducto WHERE tprod_statu = :status",$selectParams,false);
		$connDB->close();
		return $result;
	}
	
}

?>