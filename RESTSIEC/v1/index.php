<?php 

header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: X-Requested-With");
//header('Content-Type: text/html; charset=utf-8');
header('Content-Type: application/json; charset=utf-8');
header('P3P: CP="IDC DSP COR CURa ADMa OUR IND PHY ONL COM STA"');

include("../include/RestConfig.php");
include("../include/Connect.php");
include("data/BasicQuerysDB.php");
require_once("data/SurveysDB.php");
require_once("../libs/Slim/Slim.php");
// $env['slim.errors'] = fopen('/path/to/output', 'w');
/*********************** INSTANCE OF RESTAPI AND CONFIGURATION **************************************/
\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim(array('mode' => REST_MODE));

// Only invoked if mode is "production"
$app->configureMode('production', function () use ($app) {
    $app->config(array(
        'log.enable' => true,
        'log.level' => \Slim\Log::ERROR,
        'debug' => false
    ));
});

// Only invoked if mode is "test"
$app->configureMode('test', function () use ($app) {
    $app->config(array(
        'log.enable' => true,
        'log.level' => \Slim\Log::DEBUG,
        'debug' => true
    ));
});

// Only invoked if mode is "development"
$app->configureMode('development', function () use ($app) {
    $app->config(array(
        'log.enable' => false,
        'debug' => true
    ));
});
/*********************** REST FUNCTIONS **************************************/
//with post
$app->post('/login','login');
$app->post('/logoff', 'logoff');
$app->post('/sync/products', 'authenticate2', 'listSolicitudes');
$app->post('/sync/caev', 'authenticate', 'listCaev');
$app->post('/sync/surveysStatus', 'authenticate', 'listEncSt');
$app->post('/sync/download', 'authenticate', 'downloadSurveys');
$app->post('/sync/load', 'authenticate', 'loadSurveys');

// Used to test service
$app->get('/promedioProceso', 'promedioProceso');
$app->get('/promedioSolicitud', 'promedioSolicitud');
$app->get('/semaforoSolicitud', 'semaforoSolicitud');
$app->get('/retrasoProceso', 'retrasoProceso');

$app->get('/checkService', 'checkService');
$app->get('/carga/:user', 'getCarga');
//$app->get('/md5/:pass', 'encriptPass');

$app->run();

/*********************** IMPLEMENTATION OF REST FUNCTIONS **************************************/
function checkService() {
	//EVALUAR SI ES NECESARIO INCLUIR VALIDACION DEL TOKEN
	$response["error"] = false;
	$response["message"] = "El servicio esta activo";
	echoResponse(200, $response);
}

function getCarga($user) {
	try {
		$query = new SurveysDB();
		$result = $query->getCargaTrabajo($user);
		$response = array();
		$response["error"] = false;
		$response["message"] = "Se consulto la información con exito";
		$response["data"] = $result;
		echoResponse(200,$response);
	} catch(Exception $e) {
		$response = array();
		$response["error"] = true;
		$response["message"] = $e->getMessage();
		$response["data"] = "errorCode 500";
		echoResponse(200, $response);
	}
}

function encriptPass($pass) {
		$response = array();
		$response["error"] = false;
		$response["message"] = "Se encripto la clave";
		$response["data"] = md5($pass);
		echoResponse(200,$response);
}

function login() {
	try{
		verifyRequiredParams(array('user', 'pass'));
		$request_params = array();
		$request_params = $_REQUEST;
		$user = trim($request_params["user"]);
		$pass = trim($request_params["pass"]);
		$query = new BasicQuerysDB();
		$result = $query->searchUser($user,md5($pass));
		if(!isset($result) || empty($result)) {
			$response = array();
			$response["error"] = true;
			$response["message"] = "Revise el usuario o clave. No se encontro información del usuario o esta inhabilitado";
			$response["data"] = "errorCode 401";
			echoResponse(200, $response);
		} else {
			$cedula = $result["cedula"];
			$result = $query->getUserData($cedula);
			if(!isset($result) || empty($result)) {
				$response = array();
				$response["error"] = true;
				$response["message"] = "Revise el usuario o clave. Los datos de la persona no estan registrados";
				$response["data"] = "errorCode 401";
				echoResponse(200, $response);
			} else {
				date_default_timezone_set('America/Caracas');
				$auth = array();
				$auth = $result;
				$auth["user"] = $user;
				//$auth["token"] = MD5(uniqid($user.$params["cedula"].date("Y-m-d H:i:s").$auth["pnombre"], true));
				$auth["token"] = MD5(uniqid($user.$cedula.date("Y-m-d H:i:s").$auth["pnombre"], true));
				$query->registerUser($auth["user"], $auth["token"]);
				$response = array();
				$response["error"] = false;
				$response["message"] = "Usuario autenticado correctamente";
				$response["data"] = $auth;
				$query->saveOperation("L","Inicio sesion DM","user ".$user." cedula ".(string)$cedula, $user);
				echoResponse(200,$response);
			}
		}
	} catch(Exception $e) {
		$response = array();
		$response["error"] = true;
		$response["message"] = $e->getMessage();
		$response["data"] = "errorCode 500";
		echoResponse(200, $response);
	}
}

function logoff() {
	try {
		verifyRequiredParams(array("user"));
		$consult = new BasicQuerysDB();
		$consult->unregistUser($_REQUEST["user"]);
		$response = array();
		$response["error"] = false;
		$response["message"] = "Se cerro la sesion del usuario.";
		$response["data"] = null;
		echoResponse(200,$response);
	} catch(Exception $e) {
		$response = array();
		$response["error"] = true;
		$response["message"] = $e->getMessage();
		$response["data"] = "errorCode 500";
		echoResponse(200, $response);
	}
}

function listProducts() {
	try {
		$query = new BasicQuerysDB();
		$result = $query->getAllActiveProducts();
		$response = array();
		$response["error"] = false;
		$response["message"] = "Se consulto la lista de productos con exito";
		$response["data"] = $result;
		$user = $_REQUEST["user"];
		$query->saveOperation("C","Consulta lista Productos","user ".$user, $user);
		echoResponse(200,$response);
	} catch(Exception $e) {
		$response = array();
		$response["error"] = true;
		$response["message"] = $e->getMessage();
		$response["data"] = "errorCode 500";
		echoResponse(200, $response);
	}
}
function listSolicitudes() {
	try {
		$query = new BasicQuerysDB();
		$result = $query->getSolicitudes();
		$response = array();
		$response["error"] = false;
		$response["message"] = "Se consulto la lista de productos con exito";
		$response["data"] = $result;
		$user = $_REQUEST["user"];
		//$query->saveOperation("C","Consulta lista Productos","user ".$user, $user);
		echoResponse(200,$response);
	} catch(Exception $e) {
		$response = array();
		$response["error"] = true;
		$response["message"] = $e->getMessage();
		$response["data"] = "errorCode 500";
		echoResponse(200, $response);
	}
}

function listCaev() {
	try {
		$query = new BasicQuerysDB();
		$result = $query->getAllActiveCaev();
		$response = array();
		$response["error"] = false;
		$response["message"] = "Se consulto la lista de CAEV con exito";
		$response["data"] = $result;
		$user = $_REQUEST["user"];
		$query->saveOperation("C","Consulta lista CAEV","user ".$user, $user);
		echoResponse(200,$response);
	} catch(Exception $e) {
		$response = array();
		$response["error"] = true;
		$response["message"] = $e->getMessage();
		$response["data"] = "errorCode 500";
		echoResponse(200, $response);
	}
}


function listEncSt() {
	try {
		$query = new BasicQuerysDB();
		$result = $query->getEstatusEncuesta();
		$response = array();
		$response["error"] = false;
		$response["message"] = "Se consulto la lista de Estatus de encuestas con exito";
		$response["data"] = $result;
		$user = $_REQUEST["user"];
		$query->saveOperation("C","Consulta lista estados de encuestas","user ".$user, $user);
		echoResponse(200,$response);
	} catch(Exception $e) {
		$response = array();
		$response["error"] = true;
		$response["message"] = $e->getMessage();
		$response["data"] = "errorCode 500";
		echoResponse(200, $response);
	}
}

function downloadSurveys() {
	try {
		$query = new SurveysDB();
		$basicquery = new BasicQuerysDB();
		$user = $_REQUEST["user"];
		$data = $query->getCargaTrabajo($user);
		$response = array();
		$response["error"] = false;
		$response["message"] = "Se consulto la información con exito";
		$response["data"] = $data;
		$basicquery->saveOperation("S","Descargando carga de trabajo","user ".$user, $user);
		echoResponse(200,$response);
	} catch(Exception $e) {
		$response = array();
		$response["error"] = true;
		$response["message"] = $e->getMessage();
		$response["data"] = "errorCode 500";
		echoResponse(200, $response);
	}
}

function loadSurveys() {
	try {
		verifyRequiredParams(array("apkv","data"));
		$user = $_REQUEST["user"];
		$apkvs = $_REQUEST["apkv"];
		$dataToSinc = $_REQUEST["data"];
		$query = new SurveysDB();
		$query->sincSurveys($user,$apkvs,$dataToSinc);
		$response = array();
		$response["error"] = false;
		$response["message"] = "Encuestas procesadas correctamente";
		$response["data"] = null;
		echoResponse(200,$response);
	} catch(Exception $e) {
		$response = array();
		$response["error"] = true;
		$response["message"] = $e->getMessage();
		$response["data"] = "errorCode 500";
		echoResponse(200, $response);
	}
}

/*********************** USEFULL FUNCTIONS **************************************/
/**
* Verificando los parametros requeridos en el metodo o endpoint
*/
function verifyRequiredParams($required_fields) {
	$error = false;
	$error_fields = "";
	$request_params = array();
	$request_params = $_REQUEST;
	// Handling PUT request params
	if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
		$app = \Slim\Slim::getInstance();
		parse_str($app->request()->getBody(), $request_params);
	}
	foreach ($required_fields as $field) {
		if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
			$error = true;
			$error_fields .= $field . ', ';
		}
	}

	if ($error) {
		// Required field(s) are missing or empty
		// echo error json and stop the app
		/*$response = array();
		$app = \Slim\Slim::getInstance();
		$response["error"] = true;
		$response["errorCode"] = 500;
		//$response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
		$response["message"] = 'La siguiente información es requerida ' . substr($error_fields, 0, -2);
		echoResponse(200, $response);

		$app->stop();*/
		$errorMsg = "La siguiente información es requerida: ".substr($error_fields, 0, -2);
		throw new Exception($errorMsg);
	}
}

/**
* Validando parametro email si necesario;
*/
function validateEmail($email) {
	$app = \Slim\Slim::getInstance();
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		$response["error"] = true;
		$response["message"] = 'Email address is not valid';
		$response["data"] = "errorCode 400";
		echoResponse(200, $response);

		$app->stop();
	}
}

/**
* Mostrando la respuesta en formato json al cliente o navegador
* @param String $status_code Http response code
* @param Int $response Json response
*/
function echoResponse($status_code, $response) {
	$app = \Slim\Slim::getInstance();
	// Http response code
	$app->status($status_code);
	// setting response content type to json
	$app->contentType('application/json');
	echo json_encode($response, JSON_UNESCAPED_UNICODE);
}

/**
* Agregando un leyer intermedio e autenticación para uno o todos los metodos, usar segun necesidad
* Revisa si la consulta contiene un Header "Authorization" para validar
*/
function authenticate(\Slim\Route $route) {
	echo json_encode($_REQUEST);
	exit();
	// Getting request headers
	$request_params = $_REQUEST;
	$user = $request_params['user'];
	$token = $request_params['token'];

	$response = array();
	$app = \Slim\Slim::getInstance();
	$consult = new BasicQuerysDB();

	// Verifying Authorization Header
	if (isset($user) && isset($token)) {		
		$valid = $consult->validUser($user,$token);
		// validating api key
		if(!$valid) {
			// api key is not present in users table
			$response["error"] = true;
			$response["message"] = "Acceso denegado.";
			$response["data"] = "errorCode 401 | La informacion suministrada no coincide con la de nuestro servidor";
			echoResponse(200, $response);
			$app->stop(); //Detenemos la ejecución del programa al no validar			
		} else {
		//procede utilizar el recurso o metodo del llamado
		}
	} else {
		// api key is missing in header
		$response["error"] = true;
		$response["message"] = "No se recibio la información necesaria para validar la autenticación";
		$response["data"] = "errorCode 400 | NO PASARON LOS PARAMETROS 'user' y 'token' en la peticion";
		echoResponse(200, $response);

		$app->stop();
	}
}

function authenticate2(\Slim\Route $route) {
	
	// Getting request headers
	$headers = apache_request_headers();
	$response = array();
	$app = \Slim\Slim::getInstance();
	
	// Verifying Authorization Header
	if (isset($headers['authorization'])) {
		//$db = new DbHandler(); //utilizar para manejar autenticacion contra base de datos
		 
		// get the api key
		$token = $headers['authorization'];
		 
		// validating api key
		if (!($token == API_KEY)) { //API_KEY declarada en Config.php
			 
			// api key is not present in users table
			$response["error"] = true;
			$response["message"] = "Acceso denegado. Token inválido";
			echoResponse(401, $response);
			 
			$app->stop(); //Detenemos la ejecución del programa al no validar
		 
		} else {
			//procede utilizar el recurso o metodo del llamado
		}
	} else {
		// api key is missing in header
		$response["error"] = true;
		$response["message"] = "Falta token de autorización";
		echoResponse(400, $response);
		 
		$app->stop();
	}

}
function promedioProceso2() {
	try {
		$query = new BasicQuerysDB();
		$result = $query->getpromedioProceso();
		$response = array();

		$response[0]['columns'][] = array('text' => 'Proceso' , "type" => "string" );
		$response[0]['columns'][] = array('text' => 'Cantidad' , "type" => "number" );
		$response[0]['rows'] = $result;
		$response[0]['type'] = 'table';
		//$query->saveOperation("C","Consulta lista Productos","user ".$user, $user);
		echoResponse(200,$response);
	} catch(Exception $e) {
		$response = array();
		$response["error"] = true;
		$response["message"] = $e->getMessage();
		$response["data"] = "errorCode 500";
		echoResponse(200, $response);
	}
}

function promedioProceso() {
	try {
		$query = new BasicQuerysDB();
		$result = $query->getPromedioProceso();
		$response = array();
		$response["error"] = false;
		$response["message"] = "Se consulto la lista con exito";
		$response["data"] = $result;
		$user = $_REQUEST["user"];
		//$query->saveOperation("C","Consulta lista Productos","user ".$user, $user);
		echoResponse(200,$response);
	} catch(Exception $e) {
		$response = array();
		$response["error"] = true;
		$response["message"] = $e->getMessage();
		$response["data"] = "errorCode 500";
		echoResponse(200, $response);
	}
}

function PromedioSolicitud() {
	try {
		$query = new BasicQuerysDB();
		$result = $query->getPromedioSolicitud();
		$response = array();
		$response["error"] = false;
		$response["message"] = "Se consulto la lista con exito";
		$response["data"] = $result;
		$user = $_REQUEST["user"];
		//$query->saveOperation("C","Consulta lista Productos","user ".$user, $user);
		echoResponse(200,$response);
	} catch(Exception $e) {
		$response = array();
		$response["error"] = true;
		$response["message"] = $e->getMessage();
		$response["data"] = "errorCode 500";
		echoResponse(200, $response);
	}
}

function semaforoSolicitud() {
	try {
		$query = new BasicQuerysDB();
		$result = $query->getSemaforoSolicitud();
		$response = array();
		$response["error"] = false;
		$response["message"] = "Se consulto la lista con exito";
		$response["data"] = $result;
		$user = $_REQUEST["user"];
		//$query->saveOperation("C","Consulta lista Productos","user ".$user, $user);
		echoResponse(200,$response);
	} catch(Exception $e) {
		$response = array();
		$response["error"] = true;
		$response["message"] = $e->getMessage();
		$response["data"] = "errorCode 500";
		echoResponse(200, $response);
	}
}

function retrasoProceso() {
	try {
		$query = new BasicQuerysDB();
		$result = $query->getRetrasoProceso();
		$response = array();
		$response["error"] = false;
		$response["message"] = "Se consulto la lista con exito";
		$response["data"] = $result;
		$user = $_REQUEST["user"];
		//$query->saveOperation("C","Consulta lista Productos","user ".$user, $user);
		echoResponse(200,$response);
	} catch(Exception $e) {
		$response = array();
		$response["error"] = true;
		$response["message"] = $e->getMessage();
		$response["data"] = "errorCode 500";
		echoResponse(200, $response);
	}
}
?>