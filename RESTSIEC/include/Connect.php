<?php

require_once("DBConfig.php");
require_once("String.php");

/**
 * Clase que envuelve una instancia de la clase PDO
 * para el manejo de la base de modelos
 */
class Connect
{
    private $conn;

    final public function __construct()
    {
        try {
        	$this->conn = NULL;
            // Crear nueva conexión PDO
            $this->getConnection();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

    }

    private function errorMessage($clientmessage, $servermessage)
    {
		if(SHOW_SERVER_ERROR) {
			$error = array();
			$error["clientmessage"] = utf8_encode($clientmessage);
			$error["servermessage"] = utf8_encode($servermessage);
		} else {
			$error = $clientmessage;
		}
		return  json_encode($error);
    }

    /**
     * Crear una nueva conexión PDO basada
     * en las constantes de conexión
     */
    private function getConnection()
    {
    	try {
			$connString = "";
	        if ($this->conn == null) {
				if(DRIVER == "mysql") {
					$connString = DRIVER.":host=".NOMBRE_HOST.";dbname=".BASE_DE_DATOS.";charset=utf8";
				} else if(DRIVER == "sqlsrv") {
					$connString = DRIVER.":Server=".NOMBRE_HOST.";Database=".BASE_DE_DATOS;
				} else if(DRIVER == "pgsql") {
					$connString = DRIVER.":host=".NOMBRE_HOST.";port=".PUERTO.";dbname=".BASE_DE_DATOS;
				}

				if(!isEmptyString($connString)) {
					$this->conn = new PDO($connString, USUARIO, CONTRASENA);
		            // Habilitar excepciones
					$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				} else {
		            throw new Exception($this->errorMessage("No hay informacion suficiente para establecer conexion.",""));
				}
	        }    		
    	} catch (Exception $e) {
            throw new Exception($this->errorMessage("No se pudo establecer conexion con la base de datos.",$e->getMessage()));
    	}
    }


	private function search1($query) {
		try {
			return $this->search2($query,NULL,false);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	private function search2($query,$likeObjects) {
		try {
			return $this->search3($query,NULL,$likeObjects);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	private function search3($query,$params,$likeObjects) {
		try {
			if ($this->conn == NULL) {
	            throw new Exception($this->errorMessage("No hay conexion con la base de datos.",""));
			}
			$sql = $this->conn->prepare($query);
			if ($params == NULL) {
				$sql->execute();
			} else {
				$sql->execute($params);
			}
			if($likeObjects) {
				return $sql->fetchAll(PDO::FETCH_OBJ);
			} else {
				return $sql->fetchAll(PDO::FETCH_ASSOC);				
			}
		} catch(PDOException $e) {
            throw new Exception($this->errorMessage("No se pudo realizar la consulta.","QUERY: ".$query." | ".$e->getMessage()));
		}
	}

	public function execTransactionQuery($query) {
		try {
			$count = 0;
			if($this->conn == NULL) {
	            throw new Exception($this->errorMessage("No hay conexion con la base de datos.",""));
			}
			$query = trim($query);
			if(!isEmptyString($query)) {
				$this->conn->beginTransaction();
				$count = $this->conn->exec($query);
				$this->conn->commit();
				//echo $query;
			} else {
	            throw new Exception($this->errorMessage("No existe instrucción SQLTransact para ejecutar la transaccion.",""));
			}
			return $count;
		} catch(PDOException $e) {
			$this->conn->rollBack();
			//throw new Exception($this->errorMessage("No se pudo ejecutar la transaccion.",$e->getMessage()));
            throw new Exception($this->errorMessage("No se pudo ejecutar la transaccion.","QUERY: ".$query." | ".$e->getMessage()));
		}
	}

	public function insert($table,$data) {
		try {
			$allcolumns="";
			$allvalues="";
			foreach($data as $key => $value) {
				$allcolumns=$allcolumns.$key.",";
				if(is_string($value)) {
					$allvalues = $allvalues."'".$value."',";
				} else if(is_int($value) || is_bool($value) 
					|| is_float($value)	|| is_numeric($value) 
					|| is_null($value)) {
					$allvalues = $allvalues.$value.",";
				} else if(is_array($value)) {
					switch ($value[0]) {
						case 'string':
							$allvalues = $allvalues."'".$value[1]."',";
							break;
						case 'int':
							$allvalues = $allvalues.$value[1].",";
							break;
						case 'function':
							$allvalues = $allvalues.$value[1].",";
							break;
					}
				} else{
					$allvalues = $allvalues."'".$value."',";
				}
			}
			if(endsWith(trim($allcolumns),",")) {
				$allcolumns=trimOffEnd(trim($allcolumns),1);
			}
			if(endsWith(trim($allvalues),",")) {
				$allvalues=trimOffEnd(trim($allvalues),1);
			}
			$sql="INSERT INTO $table ($allcolumns) VALUES ($allvalues)";
			return $this->execTransactionQuery($sql);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function update($table,$valuesToSet,$rulesOfSearch) {
		try {
			$setElement =" SET";
			foreach($valuesToSet as $key => $value) {
				if(is_string($value)) {
					$setElement = $setElement." ".$key."='".$value."',";
				} else if(is_int($value) || is_bool($value) 
					|| is_float($value)	|| is_numeric($value) 
					|| is_null($value)) {
					$setElement = $setElement." ".$key."=".$value.",";			
				} else if(gettype($value) == "array") {
					switch ($value[0]) {
						case 'string':
							$setElement = $setElement." ".$key."='".$value[1]."',";
							break;
						case 'int':
							$setElement = $setElement." ".$key."=".$value[1].",";
							break;
						case 'function':
							$setElement = $setElement." ".$key."=".$value[1].",";
							break;
					}
				} else {
					$setElement = $setElement." ".$key."='".$value."',";
				}
			}
			if(endsWith(trim($setElement),",")) {
				$setElement=trimOffEnd(trim($setElement),1);
			}
			$whereElement ="";
			foreach($rulesOfSearch as $key => $value) {
				if(is_string($value)) {
					$whereElement = $whereElement." ".$key."='".$value."' and ";
				} else if(is_int($value) || is_bool($value) 
					|| is_float($value)	|| is_numeric($value) 
					|| is_null($value)) {
					$whereElement = $whereElement." ".$key."=".$value." and ";
				} else {
					$whereElement = $whereElement." ".$key."='".$value."' and ";
				}
			}
			if(endsWith(trim($whereElement)," and")) {
				$whereElement=trimOffEnd(trim($whereElement),4);
			}
			$sql="UPDATE $table $setElement WHERE $whereElement";
			return $this->execTransactionQuery($sql);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function delete($table,$rulesOfSearch) {
		try{
			$whereElement ="";
			foreach($rulesOfSearch as $key => $value) {
				if(is_string($value)) {
					$whereElement = $whereElement." ".$key."='".$value."' and ";
				} else if(is_int($value) || is_bool($value) 
					|| is_float($value)	|| is_numeric($value) 
					|| is_null($value)) {
					$whereElement = $whereElement." ".$key."=".$value." and ";
				} else if(gettype($value) == "array") {
					switch ($value[0]) {
						case 'string':
							$whereElement = $whereElement." ".$key."='".$value[1]."' and ";
							break;
						case 'int':
							$whereElement = $whereElement." ".$key."=".$value[1]." and ";
							break;
					}
				} else {
					$whereElement = $whereElement." ".$key."='".$value."' and ";
				}
			}
			if(endsWith(trim($whereElement)," and")) {
				$whereElement=trimOffEnd(trim($whereElement),4);
			}
			$sql= "DELETE FROM $table WHERE $whereElement";
			return $this->execTransactionQuery($sql);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	private function listTable1($table) {
		$this->htmlList($table,NULL,NULL,NULL);
	}

	private function listTable2($table,$descriptionHeader) {
		$this->htmlList($table,NULL,NULL,$descriptionHeader);
	}

	private function listInHtml1($query) {
		$this->listInHtml2($query,NULL);
	}

	private function listInHtml2($query,$descriptionHeader) {
		$this->htmlList(NULL,$query,NULL,$descriptionHeader);
	}

	private function listInHtml3($query,$descriptionHeader,$params) {
		$this->htmlList(NULL,$query,$params,$descriptionHeader);
	}


	private function htmlList($table,$query,$params,$descriptionHeader) {
		try {
			$result = NULL;
			if (!isEmptyString($table)) {
				$query = "SELECT * FROM $table";
				$result = $this->search1($query);
			} else if (!isEmptyString($query) && $params == NULL){
				$result = $this->search1($query);
			} else {
				$result = $this->search2($query,$params,false);
			}
			if($result != NULL) {
				$fisrtRecord = true;
				$htmlTable = "<table>";
				foreach ($result as $row) {
					if($fisrtRecord) {
						$htmlTable =$htmlTable. "<tr>";
						if($descriptionHeader != NULL) {
							foreach ($descriptionHeader as $value) {
								$htmlTable =$htmlTable. "<th>$value</th>";
							}					
						} else {
							foreach ($row as $key => $value) {
								$htmlTable =$htmlTable. "<th>$key</th>";
							}					
						}
						$htmlTable =$htmlTable. "</tr>";
						$fisrtRecord = false;
					}
					$htmlTable =$htmlTable. "<tr>";
					foreach ($row as $key => $value) {
						$htmlTable =$htmlTable. "<td>$value</td>";
					}
					$htmlTable =$htmlTable. "</tr>";
				}	
				$htmlTable =$htmlTable. "</table>";
				print $htmlTable;		
			}
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getDriver() {
		return DRIVER;
	}

	public function close() {
		try {
			if(isset($this->conn)) {
				$this->conn = NULL;
			}	
		} catch (Exception $e) {
			$this->errorMessage("No se pudo cerrar la conexion a base de datos",$e->getMessage());
		}
	}


	/** se ejecuta justo cuando no exite el metodo de clase */
	public function __call($method_name, $arguments) {
		//la lista de metodos sobrecargados
		$accepted_methods = array("listInHtml","listTable","search");
		//la lista de metodos que realmente existen en la clase
		$existing_methods = array("listInHtml1","listInHtml2","listInHtml3","listTable1","listTable2","search1","search2","search3");

		$counter = count($arguments);
		$method_tocall = $method_name.$counter;
		if(!in_array($method_name, $accepted_methods)
			&& !in_array($method_tocall, $existing_methods)) {
			trigger_error("Metodo <strong>$method_name</strong> no existe", E_USER_ERROR);
		} else switch($counter) {
			case 0:
				return $this->{$method_tocall}();
				break;
			case 1:
				return $this->{$method_tocall}($arguments[0]);
				break;
			case 2:
				return $this->{$method_tocall}($arguments[0], $arguments[1]);
				break;
			case 3:
				return $this->{$method_tocall}($arguments[0], $arguments[1], $arguments[2]);
				break;
			case 5:
				return $this->{$method_tocall}($arguments[0], $arguments[1], $arguments[2], $arguments[3], $arguments[4]);
				break;
			case 6:
				return $this->{$method_tocall}($arguments[0], $arguments[1], $arguments[2], $arguments[3], $arguments[4], $arguments[5]);
				break;
			default:
				return false;
		}
	}


    /**
     * Evita la clonación del objeto
     */
    final protected function __clone()
    {
    }

    function _destructor()
    {
        $this->close();
    }
}

?>