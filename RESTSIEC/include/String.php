<?php

// returns trus if $str is empty
function isEmptyString($str)
{
	return (trim($str) === "" or $str === null);
}

// returns true if $str begins with $sub
function beginsWith($str, $sub) {
    return ( substr($str, 0, strlen($sub)) == $sub);
}

// return tru if $str ends with $sub
function endsWith($str, $sub) {
    return (substr($str, strlen($str) - strlen($sub)) == $sub);
}

// trims off x chars from the front of a string
// or the matching string in $off is trimmed off
function trimOffFront($str,$off) {
    if(is_numeric($off)) {
        return substr($str,$off);
    } else {
        return substr($str,strlen($off));
    }
}

// trims off x chars from the end of a string
// or the matching string in $off is trimmed off
function trimOffEnd($str,$off) {
    if(is_numeric($off)) {
        return substr($str, 0, strlen($str) - $off);
    } else {
        return substr($str, 0, strlen($str) - strlen($off));
    }
}

?>