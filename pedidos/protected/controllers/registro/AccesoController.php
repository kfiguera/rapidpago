<?php

class AccesoController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->redirect(Yii::app()->request->baseUrl.'/Registro/listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['usuario']){
			$usuario=mb_strtolower($_POST['usuario']);
			$condicion.="a.usuar_login like '%".$usuario."%' ";
			$con++;
		}
		if($_POST['nombre']){
			$nombre=mb_strtoupper($_POST['nombre']);
			if($con>0){
				$condicion.="AND ";
			}
			$condicion.="b.perso_pnomb like '%".$nombre."%' or b.perso_snomb like '%".$nombre."%' ";
			$con++;
		}
		if($_POST['apellido']){
			$apellido=mb_strtoupper($_POST['apellido']);
			if($con>0){
				$condicion.="AND ";
			}
			$condicion.="b.perso_papel like '%".$apellido."%' or b.perso_sapel like '%".$apellido."%' ";
			$con++;
		}
		if($con>0){
			$condicion="AND ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM rd_acceso a
			  WHERE a.acces_codig = '".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('roles' => $roles));
	}
	public function actionVerificar()
	{
		if($_POST){
			
			$codigo=$_POST['codigo'];
			$accio=mb_strtoupper($_POST['accio']);
			$motiv=mb_strtoupper($_POST['motiv']);
			$estat=1;
			$passw=$this->funciones->generarPassword(10);
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			$_POST['passw']=$passw;
			$post=$_POST;
			try{
				$sql="SELECT * FROM seguridad_usuarios WHERE usuar_login ='".$codigo."'";
				$roles=$conexion->createCommand($sql)->queryRow();
				if($roles){

					if($roles['uesta_codig']=='2'){
						if($accio=='1'){
							$sql="UPDATE seguridad_usuarios 
								  SET uesta_codig = '".$estat."',
								      usuar_passw= md5('".$passw."')
								  WHERE usuar_codig ='".$roles['usuar_codig']."'";
							$res1=$conexion->createCommand($sql)->execute();	 
							if($res1){

								$msg=array('success'=>'true','msg'=>'Usuario aprobado correctamente', 'sql'=>$sql);
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al aprobar el usuario');
							}
						}else{
							$sql="DELETE FROM rd_acceso
								  WHERE acces_corre ='".$codigo."'";
							$res2=$conexion->createCommand($sql)->execute();	 
							if($res2){
								$sql="DELETE FROM seguridad_usuarios 
									  WHERE usuar_codig ='".$roles['usuar_codig']."'";
								$res3=$conexion->createCommand($sql)->execute();
								if($res3){
									$estat=2;
									$msg=array('success'=>'true','msg'=>'Usuario rechazado correctamente');
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al eliminar el usuario');
								}
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al eliminar el registro');
							}
						}
						
						if($msg['success']=='true'){
							$sql="SELECT * 
									  FROM p_persona 
									  WHERE perso_codig='".$roles['perso_codig']."'";
								$p_persona=$conexion->createCommand($sql)->queryRow();
								$vista='../registro/acceso/verificar_correo';
								$datos['p_persona']=$p_persona;
								$datos['estatus']=$estat;
								$datos['usuario']=$roles['usuar_login'];
								$datos['clave']=$passw;
								$datos['motivo']=$motiv;
								$asunto='RapidPago | Solicitud de Acceso';
								$destinatario=$roles['usuar_login'];

								$verificar=$this->funciones->RegistroApiSigo('verificarAcceso',$post);

								
                            
	                            if($verificar['success']=='true'){
	                                //$this->funciones->enviarCorreo($vista,$datos,$asunto,$destinatario);
									$transaction->commit();

	                            }else{
	                                $transaction->rollBack();
	                                $msg=$verificar;    
	                            }
								
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Usuario ya esta aprobado');
					}	
				}else{
					echo $sql;
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Usuario no existe SIGO');
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			
			
			$sql="SELECT * 
			  FROM rd_acceso a
			  WHERE a.acces_codig = '".$_GET['c']."'";

			$roles=$conexion->createCommand($sql)->queryRow();
			


			$this->render('verificar', array('roles' => $roles));
		}
	}
	/*public function RegistroApiSigo($post) {
        //Lo primerito, creamos una variable iniciando curl, pasándole la url
        $ch = curl_init('http://autogestion.smartwebtools.net/registro/api/verificarAcceso');
         
        //especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
        curl_setopt ($ch, CURLOPT_POST, 1);
         
        //le decimos qué paramáetros enviamos (pares nombre/valor, también acepta un array)
        curl_setopt ($ch, CURLOPT_POSTFIELDS, $post);
         
        //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
         
        //recogemos la respuesta
        $respuesta = curl_exec ($ch);
         
        //o el error, por si falla
        $error = curl_error($ch);

        //y finalmente cerramos curl
        curl_close ($ch);

        return json_decode($respuesta, true);
    } */  
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}