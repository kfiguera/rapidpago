<?php

class EstatusPedidoController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->redirect('listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descripcion']){
			$descripcion=mb_strtoupper($_POST['descripcion']);
			$condicion.="a.estat_descr like '%".$descripcion."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM rd_preregistro_estatus a
			  WHERE a.estat_codig ='".$_GET['c']."'";
			
		$estatus=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('estatus' => $estatus));
	}
	public function actionRegistrar()
	{
		if($_POST){
			/*var_dump($_POST);
			exit();*/
			$descr=mb_strtoupper($_POST['descr']);
			$prime=mb_strtoupper($_POST['prime']);
			$segun=mb_strtoupper($_POST['segun']);
			$terce=mb_strtoupper($_POST['terce']);
			//Datos de Auditoria
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM rd_preregistro_estatus WHERE estat_descr = '".$descr."'";
				$estatus=$conexion->createCommand($sql)->queryRow();
				if(!$estatus){
					$sql="INSERT INTO rd_preregistro_estatus(estat_descr, estat_prime, estat_segun, estat_terce, usuar_codig, estat_fcrea, estat_hcrea) 
						VALUES ('".$descr."','".$prime."','".$segun."','".$terce."','".$usuar."','".$fecha."','".$hora."')";
					$res1=$conexion->createCommand($sql)->execute();
					if($res1){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Estatus del Pedido guardado correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al guardar el Estatus del Pedido');	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Estatus del Pedido ya existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$descr=mb_strtoupper($_POST['descr']);
			$prime=mb_strtoupper($_POST['prime']);
			$segun=mb_strtoupper($_POST['segun']);
			$terce=mb_strtoupper($_POST['terce']);
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM rd_preregistro_estatus WHERE estat_codig ='".$codig."'";
				$estatus=$conexion->createCommand($sql)->queryRow();
				if($estatus){
					$sql="SELECT * FROM rd_preregistro_estatus WHERE estat_descr='".$descr."'";
					$rol=$conexion->createCommand($sql)->queryRow();
					if(!$rol or $estatus['estat_prime']!=$prime or $estatus['estat_segun']!=$segun or $estatus['estat_terce']!=$terce){
							$sql="UPDATE rd_preregistro_estatus
							  SET estat_descr='".$descr."',
							  	  estat_prime='".$prime."',
							  	  estat_segun='".$segun."',
							  	  estat_terce='".$terce."' 
							  WHERE estat_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
								$msg=array('success'=>'true','msg'=>'Estatus del Pedido actualizado correctamente');
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar el Estatus del Pedido');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Estatus del Pedido ya esta registrado');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Estatus del Pedido no existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM rd_preregistro_estatus a
			  WHERE a.estat_codig ='".$_GET['c']."'";
			$estatus=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('estatus' => $estatus));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM seguridad_usuarios WHERE estat_codig='".$codig."'";
				$usuario=$conexion->createCommand($sql)->queryRow();
				if(!$usuario){
					$sql="SELECT * FROM rd_preregistro_estatus WHERE estat_codig='".$codig."'";
					$usuario=$conexion->createCommand($sql)->queryRow();
					if($usuario){

						$sql="DELETE FROM rd_preregistro_estatus WHERE estat_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Estatus del Pedido eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar Estatus del Pedido');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Estatus del Pedido no existe');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No se puede eliminar, debido a aqe esta asociado con un usuario');
				}	
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM rd_preregistro_estatus a
			  WHERE a.estat_codig ='".$_GET['c']."'";
			$estatus=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('estatus' => $estatus));
		}
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}