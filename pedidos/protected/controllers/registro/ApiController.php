<?php

class ApiController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		

	}
	public function actionIndex()
	{
		$this->redirect(Yii::app()->request->baseUrl.'/Registro/listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
    public function actionListarSolicitudes()
    {
        $conexion=Yii::app()->db;
        $transaction=$conexion->beginTransaction();
        $usuar=Yii::app()->user->id['usuario']['codigo'];

        $sql="SELECT * FROM seguridad_usuarios WHERE usuar_login = '".$_POST['usuar']."'";
        $usuario=$conexion->createCommand($sql)->queryRow();
        Yii::app()->user->id['usuario']['codigo']=$usuar=$usuario['usuar_codig'];
        if ($usuario) {
                        
            if($usuario['urole_codig']==13){
                $sql = "SELECT solic_numer, a.prere_codig, a.estat_codig, estat_descr
                    FROM solicitud a 
                    JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                    JOIN cliente c ON (a.clien_codig = c.clien_codig)
                    JOIN seguridad_usuarios d ON (a.usuar_codig = d.usuar_codig)
                    JOIN p_persona e ON (d.perso_codig = e.perso_codig)
                    WHERE a.estat_codig between 6 and 26
                    AND a.usuar_codig = '".$usuar."'
                    UNION 
                    SELECT prere_numer, a.prere_codig, a.estat_codig, estat_descr
                    FROM rd_preregistro a 
                    JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                    JOIN seguridad_usuarios d ON (a.usuar_codig = d.usuar_codig)
                    JOIN p_persona e ON (d.perso_codig = e.perso_codig)
                    WHERE a.estat_codig in(1,2,3,5)
                    AND a.usuar_codig = '".$usuar."'
                    ";
            }else{
                $sql = "SELECT *
                    FROM solicitud a 
                    JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                    JOIN cliente c ON (a.clien_codig = c.clien_codig)
                    JOIN seguridad_usuarios d ON (a.usuar_codig = d.usuar_codig)
                    JOIN p_persona e ON (d.perso_codig = e.perso_codig)
                    WHERE a.estat_codig between 6 and 30
                    ORDER BY solic_clvip, prere_codig
                    ";    
            }
        }
        $registro=$conexion->createCommand($sql)->queryAll();
        echo json_encode($registro);

    }
     
	public function actionRegistro() {
        if ($_POST) {
            //PERSONA
            $ndocu= $this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['ndocu']));
            $pnomb = mb_strtoupper($_POST['pnomb']);
            $snomb = mb_strtoupper($_POST['snomb']);
            $papel = mb_strtoupper($_POST['papel']);
            $sapel = mb_strtoupper($_POST['sapel']);
            
            //USUARIO
            $corre = mb_strtolower($_POST['corre']);
            $ccorr = mb_strtolower($_POST['ccorr']);
            $obser = mb_strtoupper($_POST['obser']);
            $permi = 13;
            $estat = 2;

            //CONTACTO
            $tcelu = mb_strtoupper($_POST['tcelu']);
            $tloca = mb_strtoupper($_POST['tloca']);

            $rifco = mb_strtoupper($_POST['rifco']);

            $conexion=Yii::app()->db;
            $transaction=$conexion->beginTransaction();
            if($corre==$ccorr){
                try{
                    //VERIFICAR SI EXISTE LA PERSONA
                    
                    /*$sql="SELECT max(perso_cedul) perso_cedul FROM p_persona";
                    $perso=$conexion->createCommand($sql)->queryRow();
                    $ndocu= $perso['perso_cedul']+1;*/

                    $sql="SELECT * FROM p_persona WHERE perso_cedul='".$ndocu."'";
                    $p_persona=$conexion->createCommand($sql)->queryRow();
                    if(!$p_persona){
                        //SI NO EXISTE SE GUARDA
                        $sql="INSERT INTO p_persona(perso_cedul, perso_pnomb, perso_snomb, perso_papel, perso_sapel, perso_obser) 
                            VALUES ('".$ndocu."','".$pnomb."','".$snomb."','".$papel."','".$sapel."','".$obser."')";

                        $res1=$conexion->createCommand($sql)->execute();
                        if($res1){
                            $sql="SELECT * FROM p_persona WHERE perso_cedul='".$ndocu."'";
                            $p_persona=$conexion->createCommand($sql)->queryRow();  
                            
                            $msg=array('success'=>'true','msg'=>'Persona guardada correctamente');    
                        }else{
                            $transaction->rollBack();
                            $msg=array('success'=>'false','msg'=>'Error al guardar la persona');    
                        }   
                    }
                    if($p_persona){
                        $perso=$p_persona['perso_codig'];
                        //VERIFICAR SI EXISTE LA PERSONA COMO USUARIO
                        $sql="SELECT * FROM seguridad_usuarios WHERE perso_codig ='".$perso."'";
                        $upers=$conexion->createCommand($sql)->queryRow();
                        if(!$upers){
                            //VERIFICAR SI EXISTE EL USUARIO
                            $sql="SELECT * FROM seguridad_usuarios WHERE usuar_login ='".$corre."'";
                            $usuario=$conexion->createCommand($sql)->queryRow();
                            if(!$usuario){
                                $sql="INSERT INTO seguridad_usuarios(perso_codig, usuar_login, urole_codig, uesta_codig, usuar_obser, usuar_fcrea) 
                                    VALUES ('".$perso."','".$corre."','".$permi."','".$estat."','".$obser."','".date('Y-m-d')."' )";
                                $res1=$conexion->createCommand($sql)->execute();
                                if($res1){
                                    $sql="SELECT * FROM seguridad_usuarios WHERE usuar_login ='".$corre."'";
                                    $usuario=$conexion->createCommand($sql)->queryRow();

                                    $msg=array('success'=>'true','msg'=>'Usuario guardado correctamente');  
                                }else{
                                    $transaction->rollBack();
                                    $msg=array('success'=>'false','msg'=>'Error al guardar el Usuario');    
                                }
                            }else{
                                $transaction->rollBack();
                                $msg=array('success'=>'false','msg'=>'Usuario ya existe');
                            }
                            
                        }else{
                            $transaction->rollBack();
                            $msg=array('success'=>'false','msg'=>'La persona ya tiene un usuario');
                        }
                    }else{
                        $transaction->rollBack();
                            $msg=array('success'=>'false','msg'=>'Error al guardar la persona');    
                    }
                    //GUARDAR EL REGISTRO PARA SU POSTERIOR APROVACION
                    if($usuario and $msg['success']=='true'){
                        $sql="INSERT INTO rd_acceso( acces_ndocu, acces_pnomb, acces_snomb, acces_papel, acces_sapel, acces_tmovi, acces_corre, acces_rsoci, acces_rifco, eacce_codig, usuar_codig, acces_fcrea, acces_hcrea) 
                        VALUES ('".$ndocu."', '".$pnomb."', '".$snomb."', '".$papel."', '".$sapel."', '".$tcelu."', '".$corre."', '".$obser."', '".$rifco."', '1', '".$usuario['usuar_codig']."', '".date('Y-m-d')."', '".date('H:i:s')."')";
                        $res1=$conexion->createCommand($sql)->execute();
                        if($res1){

                            $transaction->commit();
                            $msg=array('success'=>'true','msg'=>'registro guardado correctamente');  
                        }else{
                            $transaction->rollBack();
                            $msg=array('success'=>'false','msg'=>'Error al guardar el Registro');    
                        }
                    }
                }catch(Exception $e){
                    $transaction->rollBack();
                    var_dump($e);
                    $msg=array('success'=>'false','msg'=>'Error al verificar la información SIGO');
                }
            }else{
                $transaction->rollBack();
                $msg=array('success'=>'false','msg'=>'El Correo y su Confirmación no son iguales');
            }
            echo json_encode($msg);
        } else {
	        $msg=array('success'=>'false','msg'=>'No envio ningun dato');
	        echo json_encode($msg);
        }
    }

    public function actionPreregistroRegistrar()
    {
        
        //$this->render('registrar');

        $conexion=Yii::app()->db;
        $transaction=$conexion->beginTransaction();
        $usuar=Yii::app()->user->id['usuario']['codigo'];

        $sql="SELECT * FROM seguridad_usuarios WHERE usuar_login = '".$_POST['usuar']."'";
        $usuario=$conexion->createCommand($sql)->queryRow();

        try{
            
            //Datos de la Solicitud
            $canti=0;
            $mcant=0;
            $color=0;
            $mensa="";
            $pasos=1;
            //Datos de Auditoria
            $estat=1;
            Yii::app()->user->id['usuario']['codigo']=$usuar=$usuario['usuar_codig'];

            $fecha=date('Y-m-d');
            $hora=date('H:i:s');
            //Obtenemos el codigo del pedido 

            $sql="SELECT * 
                  FROM rd_preregistro 
                  WHERE usuar_codig = '".$usuar."'
                    AND estat_codig NOT IN (0,4)";
            $registro=$conexion->createCommand($sql)->queryRow();

            if(!$registro){
                $sql="SELECT * 
                  FROM solicitud 
                  WHERE usuar_codig = '".$usuar."'
                    AND estat_codig NOT IN (0,27)";
                $solicitud=$conexion->createCommand($sql)->queryRow();

                if(!$solicitud){


                    $sql="SELECT max(prere_codig) codigo FROM rd_preregistro";
                    $solicitud=$conexion->createCommand($sql)->queryRow();
                    $codig=$solicitud['codigo']+1;
                    $csoli='S-'.$this->funciones->generarCodigoPedido(7,$codig);

                    //Guardamos la Solicitud
                    $sql="INSERT INTO rd_preregistro( prere_numer, estat_codig, prere_pasos, usuar_codig, prere_fcrea, prere_hcrea) 
                                VALUES ('".$csoli."','".$estat."','".$pasos."','".$usuar."','".$fecha."','".$hora."')";
                    $query=$sql;
                    $res1=$conexion->createCommand($sql)->execute();
                    if($res1){
                        $sql="SELECT max(prere_codig) codigo, a.* FROM rd_preregistro a";
                        $solicitud=$conexion->createCommand($sql)->queryRow();
                        
                        $opera='I';

                        $antes=json_encode('');
                        $actual=json_encode($solicitud);

                        $traza=$this->funciones->guardarTrazaApi($conexion, $opera, 'rd_preregistro', $antes,$actual,$query, $usuar);
                        
                        $trayectoria=$this->funciones->guardarTrayectoriaApi($conexion, $codig,$estat, '', 1, $usuar);
                        $transaction->commit();
                        
                        
                        $msg=array('success'=>'true','msg'=>'Solicitud guardado correctamente', 'codigo' => $solicitud['codigo'] , 'numero' => $csoli );    
                    }else{
                        $transaction->rollBack();
                        $msg=array('success'=>'false','msg'=>'Error al guardar la Solicitud');  
                    }
                }else{
                    $transaction->rollBack();
                    $msg=array('success'=>'false','msg'=>'Ya posee una Progreso en progreso');  
                }
            }else{
                $transaction->rollBack();
                $msg=array('success'=>'false','msg'=>'Ya posee un Pre-registro en progreso');   
            }
            echo json_encode($msg);
        }catch(Exception $e){
            echo '<pre>';
            var_dump($e);
            $transaction->rollBack();
            $msg=array('success'=>'false','msg'=>'Error al verificar la información');
        }
        
    }
    public function actionPreregistroPaso1() {
        if ($_POST) {
            //Datos del Detalle
            $pedid=mb_strtoupper($_POST['pedid']);
            $codig=mb_strtoupper($_POST['codig']);
            $pasos=mb_strtoupper($_POST['pasos']);
            
            $rifco=mb_strtoupper($_POST['rifco']);
            $rsoci=mb_strtoupper($_POST['rsoci']);
            $nfant=mb_strtoupper($_POST['nfant']);
            $acome=mb_strtoupper($_POST['acome']);
            $tmovi=mb_strtoupper($_POST['tmovi']);
            $tfijo=mb_strtoupper($_POST['tfijo']);
            $estad=mb_strtoupper($_POST['estad']);
            $munic=mb_strtoupper($_POST['munic']);
            $parro=mb_strtoupper($_POST['parro']);
            $ciuda=mb_strtoupper($_POST['ciuda']);
            $zpost=mb_strtoupper($_POST['zpost']);
            $celec=mb_strtoupper($_POST['celec']);
            $dfisc=mb_strtoupper($_POST['dfisc']);
            $banco=mb_strtoupper($_POST['banco']);
            $cafil=mb_strtoupper($_POST['cafil']);
            $ncuen=mb_strtoupper($_POST['ncuen']);
            
            //Datos de Auditoria
           
            $fecha=date('Y-m-d');
            $hora=date('H:i:s');
            $conexion=Yii::app()->db;
            $transaction=$conexion->beginTransaction();

            $monto=0;
            if($pasos<'2'){
                $pasos='2';
            }
            $sql="SELECT * FROM seguridad_usuarios WHERE usuar_login = '".$_POST['usuar']."'";
            $usuario=$conexion->createCommand($sql)->queryRow();
            Yii::app()->user->id['usuario']['codigo']=$usuar=$usuario['usuar_codig'];

            try{
                $sql="SELECT * 
                      FROM rd_preregistro
                      WHERE prere_codig='".$codig."';";
                $solicitud=$conexion->createCommand($sql)->queryRow();
                $msg=array('success'=>'true','msg'=>'Solicitud Verificada Correctamente');  
                if($solicitud['prere_rifco']!=$rifco)
                {
                    $sql="SELECT * FROM rd_preregistro
                        WHERE prere_rifco='".$rifco."' 
                          AND banco_codig='".$banco."'
                          AND prere_cafil='".$cafil."' 
                          AND estat_codig NOT IN (0,4);";
                    $preregistro=$conexion->createCommand($sql)->queryRow();
                    if(!$preregistro){
                        $sql="SELECT * FROM cliente
                        WHERE clien_rifco='".$rifco."';";
                        $cliente=$conexion->createCommand($sql)->queryRow();

                        $sql="SELECT * FROM solicitud
                        WHERE clien_codig='".$cliente['clien_codig']."' 
                          AND banco_codig='".$banco."'
                          AND solic_cafil='".$cafil."' 
                          AND estat_codig NOT IN (0,27);";
                        $solic=$conexion->createCommand($sql)->queryRow();
                        if($solic){
                            $transaction->rollBack();
                            $msg=array('success'=>'false','msg'=>'Ya se encuentra una solicitud en curso con el mismo RIF, Banco y Código de Afiliado');    
                        }
                    }else{
                        $transaction->rollBack();
                        $msg=array('success'=>'false','msg'=>'Ya se encuentra un Pre Registro con el mismo RIF, Banco y Código de Afiliado');   
                    }
                }
                if($msg['success']=='true'){
                    $sql="SELECT * FROM rd_preregistro
                        WHERE prere_codig='".$codig."';";
                    $solicitud=$conexion->createCommand($sql)->queryRow();
                    if($solicitud){

                        $sql="UPDATE rd_preregistro
                            SET prere_rifco= '".$rifco."',
                                prere_rsoci= '".$rsoci."',
                                prere_nfant= '".$nfant."',
                                acome_codig= '".$acome."',
                                prere_tmovi= '".$tmovi."',
                                prere_tfijo= '".$tfijo."',
                                estad_codig= '".$estad."',
                                munic_codig= '".$munic."',
                                parro_codig= '".$parro."',
                                ciuda_codig= '".$ciuda."',
                                prere_zpost= '".$zpost."',
                                prere_celec= '".$celec."',
                                prere_dfisc= '".$dfisc."',
                                banco_codig= '".$banco."',
                                prere_cafil= '".$cafil."',
                                prere_ncuen= '".$ncuen."',
                                prere_pasos= '".$pasos."',
                                prere_fmodi= '".$fecha."',
                                prere_hmodi= '".$hora."'
                        WHERE prere_codig = '".$codig."'";
                        $query=$sql;
                        $res1=$conexion->createCommand($sql)->execute();
                        if($res1 or $solicitud['prere_pasos']>='2'){
                            

                            $sql="SELECT * FROM rd_preregistro
                                  WHERE prere_codig='".$codig."'";
                            
                            $solicitud2=$conexion->createCommand($sql)->queryRow();
                            
                            $antes=json_encode($solicitud);
                            $actual=json_encode($solicitud2);

                            $traza=$this->funciones->guardarTrazaApi($conexion, 'U', 'rd_preregistro', $antes,$actual,$query,$usuar);

                            $transaction->commit();
                            $msg=array('success'=>'true','msg'=>'Solicitud actualizada correctamente'); 
                        }else{
                            $transaction->rollBack();
                            $msg=array('success'=>'false','msg'=>'Error al actualizar la Solicitud');   
                        }
                    }else{
                        $transaction->rollBack();
                        $msg=array('success'=>'false','msg'=>'Error no existe la Solicitud');   
                    }
                }
            }catch(Exception $e){
                var_dump($e);
                $transaction->rollBack();
                $msg=array('success'=>'false','msg'=>'Error al verificar la información');
            }
            echo json_encode($msg);
        } else {
            $msg=array('success'=>'false','msg'=>'No envio ningun dato');
            echo json_encode($msg);
        }
    }

    public function actionPreregistroPaso2() {
        if ($_POST) {
            $post=base64_decode($_POST['datos']);
            $post=json_decode($post,true);
            $_POST=$post;
            //Datos del Detalle
            $pedid=mb_strtoupper($_POST['pedid']);
            $codig=mb_strtoupper($_POST['codig']);
            $pasos=mb_strtoupper($_POST['pasos']);
            
            //Datos del Formulario
            $otele=$_POST['otele'];
            $canti=$_POST['canti'];
            
            //Datos de Auditoria
            $usuar=Yii::app()->user->id['usuario']['codigo'];
            $fecha=date('Y-m-d');
            $hora=date('H:i:s');
            $conexion=Yii::app()->db;
            $transaction=$conexion->beginTransaction();


            if($pasos<'3'){
                $pasos='3';
            }


            $sql="SELECT * FROM seguridad_usuarios WHERE usuar_login = '".$_POST['usuar']."'";
            $usuario=$conexion->createCommand($sql)->queryRow();
            Yii::app()->user->id['usuario']['codigo']=$usuar=$usuario['usuar_codig'];

            $operadores = asort($otele);
            $ant='';
            foreach ($otele as $key => $value) {
                if($value==''){ 
                    $transaction->rollBack();
                    $msg=array('success'=>'false','msg'=>'Debe Selecionar un Operador Telefónico');
                    echo json_encode($msg);
                    exit();
                }
                if($value==$ant){
                    $transaction->rollBack();
                    $msg=array('success'=>'false','msg'=>'No puede incluir un Operador Telefónico más de una vez');
                    echo json_encode($msg);
                    exit();
                }
                $ant=$value;
            }

            try{
                $sql="SELECT * FROM rd_preregistro
                    WHERE prere_codig='".$codig."';";
                $solicitud=$conexion->createCommand($sql)->queryRow();
                
                
                if($solicitud){
                    $i=0;
                    $incluye='';
                    $sql="DELETE FROM rd_preregistro_configuracion_equipo 
                          WHERE prere_codig ='".$codig."'";
                    $res2=$conexion->createCommand($sql)->execute();
                    
                    foreach ($otele as $key => $value) {
                        $sql="SELECT * 
                              FROM rd_preregistro_configuracion_equipo 
                              WHERE prere_codig ='".$codig."' 
                                AND otele_codig ='".$otele[$key]."'";   
                        $cequipo=$conexion->createCommand($sql)->queryRow();
                        if($cequipo){
                            $sql="UPDATE rd_preregistro_configuracion_equipo 
                                  SET cequi_canti = '".$canti[$key]."' 
                                  WHERE cequi_codig='".$cequipo['cequi_codig']."'";
                            $opera='U';
                        }else{
                            $sql="INSERT INTO rd_preregistro_configuracion_equipo(prere_codig, otele_codig, cequi_canti, usuar_codig, cequi_fcrea, cequi_hcrea) VALUES ('".$codig."', '".$otele[$key]."', '".$canti[$key]."', '".$usuar."', '".$fecha."', '".$hora."' )";
                            $opera='I';

                        }

                        $query=$sql;
                        $res2=$conexion->createCommand($sql)->execute();

                        $sql="SELECT * 
                              FROM rd_preregistro_configuracion_equipo 
                              WHERE prere_codig ='".$codig."' 
                                AND otele_codig ='".$otele[$key]."'";   
                        $cequipo2=$conexion->createCommand($sql)->queryRow();
                        
                        $antes=json_encode($cequipo);
                        $actual=json_encode($cequipo2);

                        $traza=$this->funciones->guardarTrazaApi($conexion, $opera, 'rd_preregistro_configuracion_equipo', $antes,$actual,$query,$usuar);
                    }

                    $sql="UPDATE rd_preregistro
                          SET prere_pasos='".$pasos."',
                              prere_fmodi= '".$fecha."',
                              prere_hmodi= '".$hora."' 
                          WHERE prere_codig='".$codig."';";
                    $query=$sql;
                    $res1=$conexion->createCommand($sql)->execute();



                    
                    if($res1 or $solicitud['prere_pasos']>='2'){

                        $sql="SELECT * FROM rd_preregistro
                            WHERE prere_codig='".$codig."';";
                        $solicitud2=$conexion->createCommand($sql)->queryRow();
                    
                        
                        $antes=json_encode($solicitud);
                        $actual=json_encode($solicitud2);

                        $traza=$this->funciones->guardarTrazaApi($conexion, $opera, 'rd_preregistro', $antes,$actual,$query,$usuar);

                            $transaction->commit();
                            $msg=array('success'=>'true','msg'=>'Solicitud actualizada correctamente');     
                       
                    }else{
                        $transaction->rollBack();
                        $msg=array('success'=>'false','msg'=>'Error al actualizar la Solicitud');   
                    }
                }else{
                    $transaction->rollBack();
                    $msg=array('success'=>'false','msg'=>'Error no existe la Solicitud');   
                }
                    
                
            }catch(Exception $e){
                var_dump($e);
                $transaction->rollBack();
                $msg=array('success'=>'false','msg'=>'Error al verificar la información');
            }
            echo json_encode($msg);
        } else {
            $msg=array('success'=>'false','msg'=>'No envio ningun dato');
            echo json_encode($msg);
        }
    }

    public function actionPreregistroPaso3() {
        if ($_POST) {
            
            $post=base64_decode($_POST['datos']);
            $post=json_decode($post,true);
            $_POST=$post;

            //Datos del Detalle
            $pedid=mb_strtoupper($_POST['pedid']);
            $codig=mb_strtoupper($_POST['codig']);
            $pasos=mb_strtoupper($_POST['pasos']);
            //Datos del Formulario

            $rifrl=$_POST['rifrl'];
            $nombr=$_POST['nombr'];
            $apell=$_POST['apell'];
            $tdocu=$_POST['tdocu'];
            $ndocu=$_POST['ndocu'];
            $cargo=$_POST['cargo'];
            $tmovi=$_POST['tmovi'];
            $tfijo=$_POST['tfijo'];
            $celec=$_POST['celec'];



            //Datos de Auditoria
            $usuar=Yii::app()->user->id['usuario']['codigo'];
            $fecha=date('Y-m-d');
            $hora=date('H:i:s');
            $conexion=Yii::app()->db;
            $transaction=$conexion->beginTransaction();


            $sql="SELECT * FROM seguridad_usuarios WHERE usuar_login = '".$_POST['usuar']."'";
            $usuario=$conexion->createCommand($sql)->queryRow();
            Yii::app()->user->id['usuario']['codigo']=$usuar=$usuario['usuar_codig'];

            $monto=0;
            if($pasos<'3'){
                $pasos='3';
            }
            try{
                $sql="SELECT * FROM rd_preregistro
                    WHERE prere_codig='".$codig."';";
                $solicitud=$conexion->createCommand($sql)->queryRow();
                                
                if($solicitud){
                    $r=0;
                    foreach ($rifrl as $key => $value) {
                        $rifrl[$key]=strtoupper($rifrl[$key]);
                        $nombr[$key]=strtoupper($nombr[$key]);
                        $apell[$key]=strtoupper($apell[$key]);
                        $tdocu[$key]=strtoupper($tdocu[$key]);
                        $ndocu[$key]=strtoupper($ndocu[$key]);
                        $cargo[$key]=strtoupper($cargo[$key]);
                        $tmovi[$key]=strtoupper($tmovi[$key]);
                        $tfijo[$key]=strtoupper($tfijo[$key]);
                        $celec[$key]=strtoupper($celec[$key]);


                        $sql="SELECT * 
                              FROM rd_preregistro_representante_legal 
                              WHERE prere_codig ='".$codig."' 
                                AND rlega_rifrl ='".$rifrl[$key]."'";   
                        $representante=$conexion->createCommand($sql)->queryRow();

                        if($representante){
                            
                            $sql="UPDATE rd_preregistro_representante_legal 
                                  SET rlega_rifrl= '".$rifrl[$key]."', 
                                      rlega_nombr= '".$nombr[$key]."', 
                                      rlega_apell= '".$apell[$key]."', 
                                      tdocu_codig= '".$tdocu[$key]."', 
                                      rlega_ndocu= '".$ndocu[$key]."', 
                                      rlega_cargo= '".$cargo[$key]."', 
                                      rlega_tmovi= '".$tmovi[$key]."', 
                                      rlega_tfijo= '".$tfijo[$key]."', 
                                      rlega_corre= '".$celec[$key]."'
                                  WHERE rlega_codig='".$representante['rlega_codig']."'";   
                            $opera='U';                           
                        }else{
                            $sql="INSERT INTO rd_preregistro_representante_legal(prere_codig, rlega_rifrl, rlega_nombr, rlega_apell, tdocu_codig, rlega_ndocu, rlega_cargo, rlega_tmovi, rlega_tfijo, rlega_corre, usuar_codig, rlega_fcrea, rlega_hcrea) 
                              VALUES ('".$codig."', '".$rifrl[$key]."', '".$nombr[$key]."', '".$apell[$key]."', '".$tdocu[$key]."', '".$ndocu[$key]."', '".$cargo[$key]."', '".$tmovi[$key]."', '".$tfijo[$key]."', '".$celec[$key]."', '".$usuar."', '".$fecha."', '".$hora."')";

                            $opera='I';

                        }
                        $res2=$conexion->createCommand($sql)->execute();
                        
                        $query=$sql;
                        $sql="SELECT * 
                              FROM rd_preregistro_representante_legal 
                              WHERE prere_codig ='".$codig."' 
                                AND rlega_rifrl ='".$rifrl[$key]."'";   
                        $representante2=$conexion->createCommand($sql)->queryRow();
                        
                        $antes=json_encode($representante);
                        $actual=json_encode($representante2);

                        $traza=$this->funciones->guardarTrazaApi($conexion, $opera, 'rd_preregistro_representante_legal', $antes,$actual,$query,$usuar);

                        if($r>0){
                            $eliminar.=','; 
                        }
                        $eliminar.="'".$rifrl[$key]."'";
                        $r++;
                    }
                    
                    


                    if($eliminar!=''){

                        $sql="SELECT * 
                              FROM rd_preregistro_representante_legal 
                              WHERE prere_codig='".$codig."' 
                                AND rlega_rifrl NOT IN (".$eliminar.")";
                        $solicitudes=$conexion->createCommand($sql)->queryAll();

                        $sql="DELETE FROM rd_preregistro_representante_legal 
                              WHERE prere_codig='".$codig."' 
                                AND rlega_rifrl NOT IN (".$eliminar.")";
                        $query=$sql;                        
                        $res3=$conexion->createCommand($sql)->execute();

                        $opera='D';
    
                        $sql="SELECT * 
                              FROM rd_preregistro_representante_legal 
                              WHERE prere_codig='".$codig."' 
                                AND rlega_rifrl NOT IN (".$eliminar.")";
                        $solicitudes2=$conexion->createCommand($sql)->queryAll();
                        
                        $antes=json_encode($solicitudes);
                        $actual=json_encode($solicitudes2);
                        
                        $traza=$this->funciones->guardarTrazaApi($conexion, $opera, 'rd_preregistro_representante_legal', $antes,$actual,$query,$usuar);
                    
                    }
                    
                    

                    $sql="UPDATE rd_preregistro
                          SET prere_pasos='".$pasos."',
                              prere_fmodi= '".$fecha."',
                              prere_hmodi= '".$hora."' 
                          WHERE prere_codig='".$codig."';";
                    $query=$sql;        
                    $res1=$conexion->createCommand($sql)->execute();

                    
                    if($res1 or $solicitud['prere_pasos']>='3'){
                        
                        $sql="SELECT * FROM rd_preregistro
                        WHERE prere_codig='".$codig."';";
                        
                        $solicitud2=$conexion->createCommand($sql)->queryRow();
                        
                        $antes=json_encode($solicitud);
                        $actual=json_encode($solicitud2);       
                        $opera='U';
                        
                        $traza=$this->funciones->guardarTrazaApi($conexion, $opera, 'rd_preregistro', $antes,$actual,$query,$usuar);

                        $transaction->commit();
                        $msg=array('success'=>'true','msg'=>'Solicitud actualizada correctamente'); 
                    }else{
                        $transaction->rollBack();
                        $msg=array('success'=>'false','msg'=>'Error al actualizar la Solicitud');   
                    }
                }else{
                    $transaction->rollBack();
                    $msg=array('success'=>'false','msg'=>'Error no existe la Solicitud');   
                }
            }catch(Exception $e){
                var_dump($e);
                $transaction->rollBack();
                $msg=array('success'=>'false','msg'=>'Error al verificar la información');
            }
            echo json_encode($msg);
        } else {
            $msg=array('success'=>'false','msg'=>'No envio ningun dato');
            echo json_encode($msg);
        }
    }

    public function actionPreregistroPaso4() {
        if ($_POST) {
            
            $post=base64_decode($_POST['datos']);
            $post=json_decode($post,true);
            $_POST=$post;

            //Datos del Detalle
            $pedid=mb_strtoupper($_POST['pedid']);
            $codig=mb_strtoupper($_POST['codig']);
            $pasos=mb_strtoupper($_POST['pasos']);
            
            $fsoli=$this->funciones->TransformarFecha_bd(mb_strtoupper($_POST['fsoli']));
            $orige=mb_strtoupper($_POST['orige']);
            $clvip=mb_strtoupper($_POST['clvip']);
            $obser=mb_strtoupper($_POST['obser']);
            
            //Datos de Auditoria
            $usuar=Yii::app()->user->id['usuario']['codigo'];
            $fecha=date('Y-m-d');
            $hora=date('H:i:s');
            $conexion=Yii::app()->db;
            $transaction=$conexion->beginTransaction();
            $sql="SELECT * FROM seguridad_usuarios WHERE usuar_login = '".$_POST['usuar']."'";
            $usuario=$conexion->createCommand($sql)->queryRow();
            Yii::app()->user->id['usuario']['codigo']=$usuar=$usuario['usuar_codig'];

            $monto=0;
            if($pasos<'4'){
                $pasos='4';
            }
            
            try{
                $sql="SELECT * FROM rd_preregistro
                    WHERE prere_codig='".$codig."';";
                $solicitud=$conexion->createCommand($sql)->queryRow();
                if($solicitud['estat_codig']==1){
                    $estat=2;

                    $trayectoria=$this->funciones->guardarTrayectoriaApi($conexion, $codig, $estat, $solicitud['estat_codig'], 2, $usuar);
                }else{
                    $estat=$solicitud['estat_codig'];
                }
                if($solicitud){

                    $sql="UPDATE rd_preregistro
                        SET prere_fsoli= '".$fsoli."',
                            orige_codig= '".$orige."',
                            prere_clvip= '".$clvip."',
                            prere_obser= '".$obser."',
                            prere_pasos= '".$pasos."',
                            estat_codig= '".$estat."',
                            prere_fmodi= '".$fecha."',
                            prere_hmodi= '".$hora."' 
                    WHERE prere_codig = '".$codig."'";
                    
                    $query=$sql;


                    $res1=$conexion->createCommand($sql)->execute();
                    if($res1 or $solicitud['prere_pasos']>='4'){
                        
                        $sql="SELECT * FROM rd_preregistro
                              WHERE prere_codig='".$codig."'";
                        
                        $solicitud2=$conexion->createCommand($sql)->queryRow();
                        
                        $antes=json_encode($solicitud);
                        $actual=json_encode($solicitud2);

                        $traza=$this->funciones->guardarTrazaApi($conexion, 'U', 'rd_preregistro', $antes,$actual,$query, $usuar);

                        $transaction->commit();
                        $msg=array('success'=>'true','msg'=>'Solicitud actualizada correctamente'); 
                    }else{
                        $transaction->rollBack();
                        $msg=array('success'=>'false','msg'=>'Error al actualizar la Solicitud');   
                    }
                }else{
                    $transaction->rollBack();
                    $msg=array('success'=>'false','msg'=>'Error no existe la Solicitud');   
                }
                
            }catch(Exception $e){
                var_dump($e);
                $transaction->rollBack();
                $msg=array('success'=>'false','msg'=>'Error al verificar la información');
            }
            echo json_encode($msg);
        } else {
            $msg=array('success'=>'false','msg'=>'No envio ningun dato');
            echo json_encode($msg);
        }
    }
    public function actionPreregistroPaso5() {
        if ($_POST) {
            
            $conexion=Yii::app()->db;
            $post=base64_decode($_POST['datos']);
            $post=json_decode($post,true);
            $_POST=$post;


            $sql="SELECT * FROM seguridad_usuarios WHERE usuar_login = '".$_POST['usuar']."'";
            $usuario=$conexion->createCommand($sql)->queryRow();
            Yii::app()->user->id['usuario']['codigo']=$usuar=$usuario['usuar_codig'];

            $_FILES=$_POST['file'];
            $_POST=$_POST['post'];

            //Datos del Detalle
            $pedid=mb_strtoupper($_POST['pedid']);
            $codig=mb_strtoupper($_POST['codig']);
            $pasos=mb_strtoupper($_POST['pasos']);
            //DATOS DE LOS DOCUMENTOS
            $dtipo=$_POST["dtipo"];

            $file=$_FILES['image'];
            /*$ruta='files/detalle/listado/';
            $name=explode(".", $file['name']);
            $nombre=date('Ymd_His.').end($name);
            $destino=$ruta.$nombre;*/
            $codigo='S-'.$this->funciones->generarCodigoPedido(6,$codig);
            foreach ($_FILES['image']['name'] as $key => $value) {
                $ruta='files/solicitudes/'.$codigo.'/';
                $name[$key]=explode(".", $file['name'][$key]);
                $nombre[$key]=date('Ymd_His_').$key.'.'.end($name[$key]);
                $destino[$key]=$ruta.$nombre[$key];
                $terminacion = end($name[$key]);
                $terminacion = strtolower($terminacion);

                if($terminacion != 'pdf' and  $terminacion!='jpg' and  $terminacion!='png'  and  $terminacion!='xls'  and  $terminacion!='xlsx'   and  $terminacion!=''  ){
                    var_dump($terminacion);

                    $msg=array('success'=>'false','msg'=>'EL formato de los archivos permitidos son: IMAGEN, PDF y EXCEL.');
                    echo json_encode($msg);
                    exit();
                }
            }


            //Datos de Auditoria
            
            $fecha=date('Y-m-d');
            $hora=date('H:i:s');
            $transaction=$conexion->beginTransaction();

            
            
            $monto=0;
            //if($pasos<'5'){
                $pasos='5';
            //}

            
            $verificar['success']='true';
            try{
                
                foreach ($dtipo as $key => $value) {
                    $destino[$key]='';  
                    /*if(!empty($_FILES['image']['tmp_name'][$key])){
                        $archivo['tmp_name']=$_FILES['image']['tmp_name'][$key];
                        $verificar=$this->funciones->CopiarEmoji($archivo,$ruta,$nombre[$key]);
                        if($verificar['success']!='true'){
                            $transaction->rollBack();
                            $msg=$verificar;
                            echo json_encode($msg); 
                            exit();
                        }
                    }else{
                        $destino[$key]='';  
                    }*/
                }

                if($verificar['success']=='true'){

                    $sql="SELECT * FROM rd_preregistro
                        WHERE prere_codig='".$codig."';";
                    $solicitudes=$conexion->createCommand($sql)->queryRow();

                    if($solicitudes['estat_codig']<=2){
                        $estat=3;
                        $trayectoria=$this->funciones->guardarTrayectoriaApi($conexion, $codig, $estat, $solicitudes['estat_codig'], 2,$usuar);
                    }else{
                        $estat=$solicitudes['estat_codig'];
                    }
                    $estat=3;
                    if($solicitudes){
                        $a=0;
                        $incluyen="";

                        foreach ($dtipo as $key => $value) {
                            
                            
                            if($dtipo[$key]!='' or $destino[$key]!=''){
                                $dtipo[$key]=mb_strtoupper($dtipo[$key]);

                                $sql="SELECT * FROM rd_preregistro_documento_digital WHERE prere_codig ='".$codig."' and    dtipo_codig='".$dtipo[$key]."'";
                                $result=$conexion->createCommand($sql)->queryRow();
                                
                                if($result){
                                    if($destino[$key]==''){
                                        $sql="UPDATE rd_preregistro_documento_digital 
                                              SET docum_ruta = '".$result['docum_ruta']."',
                                                  dtipo_codig = '".$dtipo[$key]."'
                                              WHERE docum_codig='".$result['docum_codig']."'";  
                                    }else{
                                        $sql="UPDATE rd_preregistro_documento_digital 
                                        SET docum_ruta = '".$destino[$key]."',
                                            dtipo_codig = '".$dtipo[$key]."'
                                        WHERE docum_codig='".$result['docum_codig']."'";
                                    }
                                    $opera='U';
                                }else {
                                    $sql="INSERT INTO rd_preregistro_documento_digital(prere_codig, dtipo_codig, docum_ruta, docum_orden, usuar_codig, docum_fcrea, docum_hcrea)
                                        VALUES ('".$codig."','".$dtipo[$key]."','".$destino[$key]."','".$key."','".$usuar."','".$fecha."','".$hora."');";
                                    $opera='I';
                                }
                                $query=$sql;

                                $res2=$conexion->createCommand($sql)->execute();

                                $sql="SELECT * FROM rd_preregistro_documento_digital WHERE prere_codig ='".$codig."' and    docum_orden='".$key."'";
                                $result2=$conexion->createCommand($sql)->queryRow();

                                $antes=json_encode($result);
                                $actual=json_encode($result2);

                                $traza=$this->funciones->guardarTrazaApi($conexion, $opera, 'rd_preregistro_documento_digital', $antes,$actual,$query,$usuar);


                                if($a>0 and $incluyen!=''){
                                    $incluyen.=",";
                                }
                                $incluyen.="'".$key."'";
                            }
                            

                            $a++;
                        }

                        

                        if($incluyen!=''){
                            $sql="SELECT * 
                              FROM rd_preregistro_documento_digital 
                              WHERE prere_codig = '".$codig."' 
                                AND docum_orden not in (".$incluyen.")";
                            $elimi=$conexion->createCommand($sql)->queryAll();
                                
                            $sql="DELETE  FROM rd_preregistro_documento_digital 
                              WHERE prere_codig = '".$codig."' 
                                AND docum_orden not in (".$incluyen.")";
                            $query=$sql;
                            
                            $res3=$conexion->createCommand($sql)->execute();

                            $opera='D';

                            $sql="SELECT * 
                              FROM rd_preregistro_documento_digital 
                              WHERE prere_codig = '".$codig."' 
                                AND docum_orden not in (".$incluyen.")";

                            $elimi2=$conexion->createCommand($sql)->queryAll();
                        
                            $antes=json_encode($elimi);
                            $actual=json_encode($elimi2);
                            
                            $traza=$this->funciones->guardarTrazaApi($conexion, $opera, 'rd_preregistro_documento_digital', $antes,$actual,$query,$usuar);
                        }
                        
                        $sql="UPDATE rd_preregistro
                            SET prere_pasos = '".$pasos."',
                                estat_codig= '".$estat."',
                                prere_fmodi= '".$fecha."',
                                prere_hmodi= '".$hora."' 
                        WHERE prere_codig = '".$codig."'";
                        $query=$sql;
                        $res1=$conexion->createCommand($sql)->execute();

                        if($res1 or $solicitudes['prere_pasos']>='5'){
                            $opera='U';
                            $sql="SELECT * FROM rd_preregistro
                                    WHERE prere_codig='".$codig."';";
                            $solicitudes2=$conexion->createCommand($sql)->queryAll();
                            $antes=json_encode($solicitudes);
                            $actual=json_encode($solicitudes2);
                        
                            $traza=$this->funciones->guardarTrazaApi($conexion, $opera, 'rd_preregistro', $antes,$actual,$query,$usuar);

                        
                        $res2=$conexion->createCommand($sql)->execute();


                        $sql="SELECT * 
                              FROM rd_preregistro_documento_digital_tipo a 
                              LEFT JOIN rd_preregistro_documento_digital b ON (
                                a.dtipo_codig = b.dtipo_codig 
                                AND b.prere_codig = '".$codig."') 
                              WHERE a.dtipo_oblig=1 ";
                        $tdocu=$conexion->createCommand($sql)->queryAll();
                        /*if($solicitudes['prere_clvip']!='1'){
                            foreach ($tdocu as $key => $value) {
                                if($value['docum_ruta']==''){
                                    $transaction->rollBack();
                                    $msg=array('success'=>'false','msg'=>'El documento "'.$value['dtipo_descr'].'", es obligatorio');
                                    echo json_encode($msg);     
                                    exit();
                                }
                            }   
                        }*/
                            $sql="SELECT * FROM rd_preregistro WHERE prere_codig ='".$codig."'";
                            $solicitud=$conexion->createCommand($sql)->queryRow();

                            $datos['row']=$solicitud;
                            $corre=$solicitud['prere_celec'];
                            $transaction->commit();
                            $msg=array('success'=>'true','msg'=>'Solicitud actualizada correctamente');     
                        }else{
                            $transaction->rollBack();
                            $msg=array('success'=>'false','msg'=>'Error al actualizar el Pedido');  
                        }
                    }else{
                        $transaction->rollBack();
                        $msg=array('success'=>'false','msg'=>'Error no existe la Solicitud');   
                    }
                }else{
                    $transaction->rollBack();
                    $msg=$verificar;
                }
            }catch(Exception $e){
                $transaction->rollBack();
                $msg=array('success'=>'false','msg'=>'Error al verificar la información');
            }
            echo json_encode($msg);

        } else {
            $msg=array('success'=>'false','msg'=>'No envio ningun dato');
            echo json_encode($msg);
        }
    }

}