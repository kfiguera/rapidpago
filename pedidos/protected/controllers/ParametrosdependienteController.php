<?php

class ParametrosdependienteController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}
	public function actionGenerico_dependiente()
	{
		if($_POST){
			
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="CALL sp_admin_tgenerica_dependiente(
				'".$_POST['codigo']."'
				, '".$_POST['descripcion']."'
				, '".$_POST['tipo']."'
				, '0'
				, '".$_POST['operacion']."'
				,'".$_POST['dependencia']."'); ";

				$result=$conexion->createCommand($sql)->queryRow();
				$transaction->commit();
				$msg=array('success'=>$result['success'],'msg'=>$result['mensaje']);
			}catch (CDbException $e){
				//var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'true','msg'=>'Error al Ingesar la Descripción');
			}
			echo json_encode($msg);
		}else if($_GET){
			$conexion=Yii::app()->db;
			switch ($_GET['t']) {
				case 'CIU':
					if(isset($_GET['c'])){
						$sql="SELECT * FROM admin_tgenerica_dependiente 
						WHERE tdepe_codig='".$_GET['c']."'";
						$result=$conexion->createCommand($sql)->queryRow();
					}
					$param['titulo']='Ciudad';
					$param['tipo']='CIU';
					$param['codigo']=$result['tdepe_codig'];
					$param['operacion']=$_GET['o'];
					$param['descripcion']=$result['tdepe_descr'];
					$param['dependencia']=$_GET['dep'];
					//var_dump($param);
					$this->render('generico_dependiente',array('param' => $param ));
					break;
				case 'MOD':
					if(isset($_GET['c'])){
						$sql="SELECT * FROM admin_tgenerica_dependiente 
						WHERE tdepe_codig='".$_GET['c']."'";
						$result=$conexion->createCommand($sql)->queryRow();
					}
					$param['titulo']='Modelo';
					$param['tipo']='MOD';
					$param['codigo']=$result['tdepe_codig'];
					$param['operacion']=$_GET['o'];
					$param['descripcion']=$result['tdepe_descr'];
					$param['dependencia']=$_GET['dep'];

					//var_dump($param);
					$this->render('generico_dependiente',array('param' => $param ));
					break;
			}
			
		}else{
			$this->render('lista_dependiente');
		}
	}
	public function actionLista_dependiente()
	{
		if($_GET){
			$conexion=Yii::app()->db;
			switch ($_GET['t']) {
				case 'CIU':
					if(isset($_GET['c'])){
						$sql="SELECT * FROM admin_tgenerica_dependiente 
						WHERE tdepe_codig='".$_GET['c']."'";
						$result=$conexion->createCommand($sql)->queryRow();
					}
					$param['titulo']='Ciudad';
					$param['tipo']='CIU';
					$param['codigo']=$result['tdepe_codig'];
					$param['operacion']=$_GET['o'];
					$param['descripcion']=$result['tdepe_descr'];
					$param['dependencia']=$result['tgene_depen'];
					//var_dump($param);
					$this->render('lista_dependiente',array('param' => $param ));
					break;
				case 'MOD':
					if(isset($_GET['c'])){
						$sql="SELECT * FROM admin_tgenerica_dependiente 
						WHERE tdepe_codig='".$_GET['c']."'";
						$result=$conexion->createCommand($sql)->queryRow();
					}
					$param['titulo']='Modelo';
					$param['tipo']='MOD';
					$param['codigo']=$result['tdepe_codig'];
					$param['operacion']=$_GET['o'];
					$param['descripcion']=$result['tdepe_descr'];
					$param['dependencia']=$result['tgene_depen'];
					//var_dump($param);
					$this->render('lista_dependiente',array('param' => $param ));
					break;
				
			}
			
		}else{
			$this->render('lista_dependiente');
		}
	}
public function actionLista_dependienteEliminar()
	{
		if($_POST){
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
							
				$sql="CALL sp_admin_tgenerica_dependiente('".$_POST['c']."','','','','D','')";
				//echo $sql;
				//exit;
				$result=$conexion->createCommand($sql)->queryRow();
				//var_dump($result);
				$transaction->commit();
				$msg=array('success'=>$result['success'],'msg'=>$result['mensaje']);
			}catch (CDbException $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'true','msg'=>'Error al Eliminar el '.$param['titulo']);
			}
			echo json_encode($msg);
		}else if($_GET){
			$sql="SELECT * FROM admin_tgenerica_dependiente WHERE tdepe_codig ='".$_GET['c']."'";
			$conexion=yii::app()->db;
            $result=$conexion->createCommand($sql)->queryRow();
            
            $this->render('eliminar_lista_dependiente',array('model'=>$result,'c'=>$_GET['c']));
			
			//$this->render('eliminar',array('model'=>$result));
		}else{
			$this->render('lista_dependiente',array('model'=>$result));
		}
	}

public function actionModal2() {
        ?>
        <form method='post' id='modaleliminar'>
            <input type="hidden" id='c' name='c' value='<?php echo $_REQUEST['c'] ?>'> 
            <input type="hidden" id='t' name='t' value='<?php echo $_REQUEST['t'] ?>'>            
            ¿Está seguro de querer eliminar el <?php echo $_REQUEST['titulo'] ?> ?
            <br>
            <br>
            <button id='enviar' aprobar="true" estatus="2" class="btn btn-block btn-danger" type="button" archivo='<?php echo $nombre ?>' ><i class="fa fa-check"></i>Continuar</button>

        </form>
        <script>
            $('#enviar').click(function (e) {
                var formData = new FormData($("#modaleliminar")[0]);
                var c = document.getElementById("c").value;
                var t = document.getElementById("t").value;
                $.ajax({
                    dataType: "json",
                    'data': formData,
                    url: 'Lista_dependienteEliminar',
                    type: 'post',
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $("#resultado").html("Procesando, espere por favor...");
                    },
                    success: function (response) {
                        //alert(response['success']);
                        if (response['success'] == 'true') {
                            $('#cerrar').click();
                            bootbox.dialog({
                                message: response['msg'],
                                title: "Exito!",
                                buttons: {
                                    danger: {
                                        label: "Cerrar",
                                        className: "btn-uven",
                                        callback: function(){
                                        	window.open('lista_dependiente?t=' + t, '_parent');
                                        }
                                    },
                                }
                            });
                            $('.close').click( function(){
                            	window.open('lista_dependiente?t=' + t, '_parent');
                            });
                        } else {

                            bootbox.dialog({
                                message: response['msg'],
                                title: "Información!",
                                buttons: {
                                    danger: {
                                        label: "Cerrar",
                                        className: "btn-uven",
                                    },
                                }
                            });
                            setTimeout(function () {
                                $('.close').click();
                            }, 1000);
                            //alert('error');
                            // $("#subir").removeAttr('disabled');
                        }

                    }
                });
            });

        </script>

        <?php
    }

}