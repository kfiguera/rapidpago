<?php

class SalidasProductosController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$f=$_GET['f'];
		$this->redirect(Yii::app()->request->baseUrl.'/inventario/salidasProductos/listado?f='.$f);
	}

	public function actionListado()
	{
		$f=$_GET['f'];
		$this->render('listado',array('f' => $f ));
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		$f=$_POST['nfact'];
		if($_POST['descr']){
			$descr=mb_strtoupper($_POST['descr']);
			$condicion.="AND b.inven_descr like '%".$descr."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE factu_codig='".$f."' ".$condicion;
		}
		
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion,'f' => $f ));		
	}
	public function actionConsultar()
	{
		$f=$_GET['f'];
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM factura_producto  a
			  WHERE a.fprod_codig ='".$_GET['c']."'";
		$roles=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('roles' => $roles, 'f' => $f ));
	}
	public function actionRegistrar()
	{
		if($_POST){
			$nfact=mb_strtoupper($_POST["nfact"]);
			foreach ($_POST["produ"] as $key => $value) {
				$producto[$key]["produ"]=$_POST["produ"][$key];
				$producto[$key]["cprod"]=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["cprod"][$key]));
				$producto[$key]["pprod"]=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["pprod"][$key]));
				$producto[$key]["mprod"]=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["mprod"][$key]));
			}
			$obser=mb_strtoupper($_POST["obser"]);
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM inventario_salida WHERE isali_codig = '".$nfact."'";
				$salida=$conexion->createCommand($sql)->queryRow();
				if($salida){
					foreach ($producto as $key => $value) {
						$sql="INSERT INTO inventario_salida_producto(isali_codig, inven_codig, ispro_canti, ispro_preci, ispro_monto, ispro_obser, usuar_codig, ispro_fcrea, ispro_hcrea) 
						VALUES ( '".$nfact."', '".$value['produ']."', '".$value['cprod']."', '".$value['pprod']."', '".$value['mprod']."', '".$value['obser']."', '".Yii::app()->user->id['usuario']['codigo']."','".date('Y-m-d')."','".date('H:i:s')."')";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$msg=array('success'=>'true','msg'=>'Producto guardado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al guardar el producto');
							echo json_encode($msg);;
							exit();
						}	
					}
					if ($msg['success']=='true') {
						$sql="SELECT * FROM inventario_salida_producto WHERE isali_codig ='".$nfact."'";
						$prod=$conexion->createCommand($sql)->queryAll();
						
						foreach ($prod as $key => $value) {
							$mtotal+=$value['ispro_monto'];
						}
						$sql="UPDATE inventario_salida 
								  SET isali_monto = '".$mtotal."'
									 WHERE isali_codig ='".$nfact."'";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar la Salida');	
						}

					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'La inventario_salida no existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$e=$_GET['e'];
		$this->render('registrar',array('e' => $e ));
		}
	}
	public function actionModificar()
	{
		if($_POST){
			
			$codig=mb_strtoupper($_POST["codig"]);
			$nfact=mb_strtoupper($_POST["nfact"]);
			foreach ($_POST["produ"] as $key => $value) {
				$producto[$key]["produ"]=$_POST["produ"][$key];
				$producto[$key]["cprod"]=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["cprod"][$key]));
				$producto[$key]["pprod"]=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["pprod"][$key]));
				$producto[$key]["mprod"]=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["mprod"][$key]));
			}
			$obser=mb_strtoupper($_POST["obser"]);

			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM inventario_salida_producto  WHERE ispro_codig ='".$codig."'";
				$salida=$conexion->createCommand($sql)->queryRow();
				if($salida){
					foreach ($producto as $key => $value) {
						$sql="UPDATE inventario_salida_producto
						  SET isali_codig = '".$nfact."',
							  inven_codig = '".$value['produ']."',
							  ispro_canti = '".$value['cprod']."',
							  ispro_preci = '".$value['pprod']."',
							  ispro_monto = '".$value['mprod']."',
							  ispro_obser = '".$obser."'  
							 WHERE ispro_codig ='".$codig."'";
				
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$msg=array('success'=>'true','msg'=>'Producto Actualizado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar el producto');
							echo json_encode($msg);;
							exit();
						}	
					}
					if ($msg['success']=='true') {
						$sql="SELECT * FROM inventario_salida_producto WHERE isali_codig ='".$nfact."'";
						$prod=$conexion->createCommand($sql)->queryAll();
						
						foreach ($prod as $key => $value) {
							$mtotal+=$value['ispro_monto'];
						}
						$sql="UPDATE inventario_salida 
								  SET isali_monto = '".$mtotal."'
									 WHERE isali_codig ='".$nfact."'";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar la Salida');	
						}

					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'La Salida no existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$e=$_GET['e'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM inventario_salida_producto  a
			  WHERE a.ispro_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('roles' => $roles,'e'=>$e));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$nfact=mb_strtoupper($_POST["nfact"]);
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM inventario_salida_producto  WHERE ispro_codig ='".$codig."'";
				$roles=$conexion->createCommand($sql)->queryRow();
				if($roles){

					$sql="DELETE FROM inventario_salida_producto  WHERE ispro_codig ='".$codig."'";
					$res1=$conexion->createCommand($sql)->execute();
					//echo $sql;
					if($res1){
						$msg=array('success'=>'true','msg'=>' Producto eliminado correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al eliminar la Salida');	
					}
					if ($msg['success']=='true') {
						$sql="SELECT * FROM inventario_salida_producto WHERE isali_codig ='".$nfact."'";
						$prod=$conexion->createCommand($sql)->queryAll();
						
						foreach ($prod as $key => $value) {
							$mtotal+=$value['ispro_monto'];
						}
						$sql="UPDATE inventario_salida 
								  SET isali_monto = '".$mtotal."'
									 WHERE isali_codig ='".$nfact."'";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar la Salida');	
						}

					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'La Salida no existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$e=$_GET['e'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM inventario_salida_producto  a
			  WHERE a.ispro_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('roles' => $roles,'e'=>$e));
		}
	}
	
	public function TransformarMonto_bd($monto)
	{
		//DEBE DE ESTAR 0.000,00
		$monto=str_replace('.', '', $monto);
		$monto=str_replace(',', '.', $monto);
		//RETORNA 0000.00
		return $monto;
	}
	public function TransformarMonto_v($monto,$cantidad)
	{
		//DEBE DE ESTAR 0000.00
		$monto=number_format($monto,$cantidad,',','.');
		//RETORNA 0.000,00
		return $monto;
	}
	public function TransformarFecha_bd($fecha)
	{
		//DEBE DE ESTAR DD/MM/AAAA
		$fecha=explode('/',$fecha);
		$fecha=$fecha[2].'-'.$fecha[1].'-'.$fecha[0];
		//RETORNA AAAA-MM-DD
		return $fecha;
	}
	public function TransformarFecha_v($fecha)
	{
		//DEBE DE ESTAR AAAA-MM-DD
		$fecha=explode('/',$fecha);
		$fecha=$fecha[2].'-'.$fecha[1].'-'.$fecha[0];
		//RETORNA DD/MM/AAAA
		return $fecha;
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}