<?php

class SalidasController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->redirect(Yii::app()->request->baseUrl.'/inventario/salidas/listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descripcion']){
			$descripcion=mb_strtoupper($_POST['descripcion']);
			$condicion.="a.inven_descr like '%".$descripcion."%' ";
			$con++;
		}
		if($_POST['codigo']){
			if($con>0){
				$condicion.="AND ";
			}
			$codigo=mb_strtoupper($_POST['codigo']);
			$condicion.="a.inven_cprod like '%".$codigo."%' ";
			$con++;
		}
		if($_POST['cantidad']){
			if($con>0){
				$condicion.="AND ";
			}
			$cantidad = $this->funciones->TransformarMonto_bd($_POST['cantidad']);
			$condicion.="a.inven_canti like '%".$cantidad."%' ";
			$con++;
		}
		if($_POST['punidad']){
			if($con>0){
				$condicion.="AND ";
			}
			$punidad = $this->funciones->TransformarMonto_bd($_POST['punidad']);
			$condicion.="a.inven_punid like '%".$punidad."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM inventario_salida  a
			  WHERE a.isali_codig ='".$_GET['c']."'";
		$salida=$conexion->createCommand($sql)->queryRow();
		$sql="SELECT * 
			  FROM p_trabajador  a
			  WHERE a.traba_codig ='".$salida['traba_codig']."'";
		$p_trabajador=$conexion->createCommand($sql)->queryRow();
		$sql="SELECT a.* , b.inven_descr
			  FROM inventario_salida_producto  a
			  JOIN inventario b ON (a.inven_codig = b.inven_codig)
			  WHERE a.isali_codig ='".$salida['isali_codig']."'";
		$productos=$conexion->createCommand($sql)->queryAll();
		$this->render('consultar', array('salida' => $salida,'p_trabajador' => $p_trabajador,'productos' => $productos));
	}
	public function actionRegistrar()
	{
		if($_POST){
			//SALIDA
			$traba=mb_strtoupper($_POST["traba"]);
			$femis=mb_strtoupper($this->funciones->TransformarFecha_bd($_POST["femis"]));
			$obser=mb_strtoupper($_POST["obser"]);
			//PRODUCTOS
			foreach ($_POST["produ"] as $key => $value) {
				$producto[$key]["produ"]=$_POST["produ"][$key];
				$producto[$key]["cprod"]=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["cprod"][$key]));
				$producto[$key]["pprod"]=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["pprod"][$key]));
				$producto[$key]["mprod"]=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["mprod"][$key]));
			}
			$mtotal=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["mtotal"]));

			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				
						$sql="INSERT INTO inventario_salida(traba_codig, isali_femis, isali_monto, isest_codig, isali_obser, usuar_codig, isali_fcrea, isali_hcrea)
								VALUES ('".$traba."','".$femis."','".$mtotal."','1','".$obser."','".Yii::app()->user->id['usuario']['codigo']."','".date('Y-m-d')."','".date('H:i:s')."')";
						
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							
							$msg=array('success'=>'true','msg'=>'Salida guardada correctamente');
							$sql="SELECT * from inventario_salida WHERE isali_codig=(SELECT max(isali_codig) FROM inventario_salida)
	";
							$salida=$conexion->createCommand($sql)->queryRow();

						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al guardar la Salida');
						}	
					
					if ($msg['success']=='true') {
						if($salida){
							foreach ($producto as $key => $value) {

								$sql="INSERT INTO inventario_salida_producto(isali_codig, inven_codig, ispro_canti, ispro_preci, ispro_monto, ispro_obser, usuar_codig, ispro_fcrea, ispro_hcrea) 
								VALUES ( '".$salida['isali_codig']."', '".$value['produ']."', '".$value['cprod']."', '".$value['pprod']."', '".$value['mprod']."', '".$value['obser']."', '".Yii::app()->user->id['usuario']['codigo']."','".date('Y-m-d')."','".date('H:i:s')."')";
								$res1=$conexion->createCommand($sql)->execute();
								if($res1){
									$msg=array('success'=>'true','msg'=>'Producto guardado correctamente');	
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al guardar el producto');
									echo json_encode($msg);;
									exit();
								}	
							}
							$transaction->commit();
							
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'La factura no existe');
						}
					}
				
			}catch(Exception $e){
				//var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codig=mb_strtoupper($_POST['codig']);
			$traba=mb_strtoupper($_POST['traba']);
			$femis=mb_strtoupper($this->funciones->TransformarFecha_bd($_POST["femis"]));
			$obser=mb_strtoupper($_POST["obser"]);
			if($contra==$ccontra){
				$conexion=Yii::app()->db;
				$transaction=$conexion->beginTransaction();
				try{
					$sql="SELECT * FROM inventario_salida WHERE isali_codig ='".$codig."'";
					$salida=$conexion->createCommand($sql)->queryRow();
					if($salida){
						
							$sql="UPDATE inventario_salida
								  SET traba_codig = '".$traba."',
									  isali_femis = '".$femis."',
									  isali_obser = '".$obser."'
								  WHERE isali_codig ='".$codig."'";
							$res1=$conexion->createCommand($sql)->execute();
							if($res1){
								$transaction->commit();
								$msg=array('success'=>'true','msg'=>'Salida actualizada correctamente');
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al actualizar la Salida');	
							}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'La Salida no existe');
					}
				}catch(Exception $e){
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error al verificar la información');
				}
			}else{
				$msg=array('success'=>'false','msg'=>'La contraseña y su confirmación no son iguales');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM inventario_salida  a
			  WHERE a.isali_codig ='".$_GET['c']."'";
			$salida=$conexion->createCommand($sql)->queryRow();
			$sql="SELECT * 
				  FROM p_trabajador  a
				  WHERE a.traba_codig ='".$salida['traba_codig']."'";
			$p_trabajador=$conexion->createCommand($sql)->queryRow();
			$sql="SELECT a.* , b.inven_descr
				  FROM inventario_salida_producto  a
				  JOIN inventario b ON (a.inven_codig = b.inven_codig)
				  WHERE a.isali_codig ='".$salida['isali_codig']."'";
			$productos=$conexion->createCommand($sql)->queryAll();
			$this->render('modificar', array('salida' => $salida,'p_trabajador' => $p_trabajador,'productos' => $productos));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM inventario_salida_producto WHERE isali_codig='".$codig."'";
				$prod=$conexion->createCommand($sql)->queryRow();
				if(!$prod){
					$sql="SELECT * FROM inventario_salida WHERE isali_codig='".$codig."'";
					$roles=$conexion->createCommand($sql)->queryRow();
					if($roles){

						$sql="DELETE FROM inventario_salida  WHERE isali_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Salida eliminada correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar la Salida');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'La Salida no existe');
					}

				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No se puede eliminar debido a, que tiene productos asociados');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM inventario_salida  a
			  WHERE a.isali_codig ='".$_GET['c']."'";
			$salida=$conexion->createCommand($sql)->queryRow();
			$sql="SELECT * 
				  FROM p_trabajador  a
				  WHERE a.traba_codig ='".$salida['traba_codig']."'";
			$p_trabajador=$conexion->createCommand($sql)->queryRow();
			$sql="SELECT a.* , b.inven_descr
				  FROM inventario_salida_producto  a
				  JOIN inventario b ON (a.inven_codig = b.inven_codig)
				  WHERE a.isali_codig ='".$salida['isali_codig']."'";
			$productos=$conexion->createCommand($sql)->queryAll();
			$this->render('eliminar', array('salida' => $salida,'p_trabajador' => $p_trabajador,'productos' => $productos));
		}
	}
	public function actionAprobarSalidas()
	{
		if($_POST){
			
			$id=$_POST['id'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				foreach ($id as $key => $codig) {
					$sql="SELECT * FROM inventario_salida WHERE isali_codig='".$codig."'";
					$salida=$conexion->createCommand($sql)->queryRow();
					if($salida){
						$sql="UPDATE inventario_salida
						  	  SET isest_codig = '2'
							  WHERE isali_codig ='".$codig."'";
				
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$msg=array('success'=>'true','msg'=>'Salida Actualizada correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar la Salida');
							echo json_encode($msg);;
							exit();
						}	

					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'La Salida no existe');
						echo json_encode($msg);;
						exit();
					}

					if($msg['success']="true"){
						$sql="SELECT * FROM inventario_salida_producto WHERE isali_codig='".$codig."'";
						$productos=$conexion->createCommand($sql)->queryAll();
						foreach ($productos as $key => $producto) {
							$preci=$this->funciones->MontoCambioReverso($producto['inven_codig'],$producto['fprod_preci'],'1');

							$sql="UPDATE inventario
						  	  SET inven_canti = inven_canti-'".$producto['ispro_canti']."',
						  	  	  inven_punid = '".$preci."'
							  WHERE inven_codig ='".$producto['inven_codig']."'";
							$res1=$conexion->createCommand($sql)->execute();
							if($res1){
								$msg=array('success'=>'true','msg'=>'Salida Actualizada correctamente');	
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al actualizar el Producto');
								echo json_encode($msg);
								exit();
							}	
						}
						
					}
					
				}
				if($msg['success']="true"){
					$transaction->commit();
				}
				
				
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$this->render('aprobar');

		}
	}
	public function actionReversarAprobarSalidas()
	{
		if($_POST){
			
			$id=$_POST['id'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				foreach ($id as $key => $codig) {
					$sql="SELECT * FROM inventario_salida WHERE isali_codig='".$codig."'";
					$salida=$conexion->createCommand($sql)->queryRow();
					if($salida){
						$sql="UPDATE inventario_salida
						  	  SET isest_codig = '1'
							  WHERE isali_codig ='".$codig."'";
				
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$msg=array('success'=>'true','msg'=>'Salida Actualizada correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar la Salida');
							echo json_encode($msg);;
							exit();
						}	

					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'La Salida no existe');
						echo json_encode($msg);;
						exit();
					}

					if($msg['success']="true"){
						$sql="SELECT * FROM inventario_salida_producto WHERE isali_codig='".$codig."'";
						$productos=$conexion->createCommand($sql)->queryAll();
						foreach ($productos as $key => $producto) {
							
							$sql="UPDATE inventario
						  	  SET inven_canti = inven_canti+'".$producto['ispro_canti']."'
							  WHERE inven_codig ='".$producto['inven_codig']."'";
							$res1=$conexion->createCommand($sql)->execute();
							if($res1){
								$msg=array('success'=>'true','msg'=>'Salida Actualizada correctamente');	
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al actualizar el Producto');
								echo json_encode($msg);
								exit();
							}	
						}
						
					}
					
				}
				if($msg['success']="true"){
					$transaction->commit();
				}
				
				
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$this->render('aprobarreversar');

		}
	}
	public function actionAnularSalidas()
	{
		if($_POST){
			
			$id=$_POST['id'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				foreach ($id as $key => $codig) {
					$sql="SELECT * FROM inventario_salida WHERE isali_codig='".$codig."'";
					$salida=$conexion->createCommand($sql)->queryRow();
					if($salida){
						$sql="UPDATE inventario_salida
						  	  SET isest_codig = '3'
							  WHERE isali_codig ='".$codig."'";
				
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$msg=array('success'=>'true','msg'=>'Salida Actualizada correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar la Salida');
							echo json_encode($msg);;
							exit();
						}	

					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'La Salida no existe');
						echo json_encode($msg);;
						exit();
					}
					
				}
				if($msg['success']="true"){
					$transaction->commit();
				}
				
				
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$this->render('anular');

		}
	}
	public function actionReversarAnularSalidas()
	{
		if($_POST){
			
			$id=$_POST['id'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				foreach ($id as $key => $codig) {
					$sql="SELECT * FROM inventario_salida WHERE isali_codig='".$codig."'";
					$salida=$conexion->createCommand($sql)->queryRow();
					if($salida){
						$sql="UPDATE inventario_salida
						  	  SET isest_codig = '1'
							  WHERE isali_codig ='".$codig."'";
				
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$msg=array('success'=>'true','msg'=>'Salida Actualizada correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar la Salida');
							echo json_encode($msg);;
							exit();
						}	

					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'La Salida no existe');
						echo json_encode($msg);;
						exit();
					}
					
				}
				if($msg['success']="true"){
					$transaction->commit();
				}
				
				
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$this->render('anularreversar');

		}
	}
	public function TransformarMonto_bd($monto)
	{
		//DEBE DE ESTAR 0.000,00
		$monto=str_replace('.', '', $monto);
		$monto=str_replace(',', '.', $monto);
		//RETORNA 0000.00
		return $monto;
	}
	public function TransformarMonto_v($monto,$cantidad)
	{
		//DEBE DE ESTAR 0000.00
		$monto=number_format($monto,$cantidad,',','.');
		//RETORNA 0.000,00
		return $monto;
	}
	public function TransformarFecha_bd($fecha)
	{
		//DEBE DE ESTAR DD/MM/AAAA
		$fecha=explode('/',$fecha);
		$fecha=$fecha[2].'-'.$fecha[1].'-'.$fecha[0];
		//RETORNA AAAA-MM-DD
		return $fecha;
	}
	public function TransformarFecha_v($fecha)
	{
		//DEBE DE ESTAR AAAA-MM-DD
		$fecha=explode('-',$fecha);
		$fecha=$fecha[2].'/'.$fecha[1].'/'.$fecha[0];
		//RETORNA DD/MM/AAAA
		return $fecha;
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}