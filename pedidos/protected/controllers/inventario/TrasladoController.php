<?php

class TrasladoController extends Controller
{
    public $funciones;
    public $reportes;

    public function init()
    {
        $fun=Yii::app()->createController('funciones');
        $this->funciones=$fun[0];
        $this->funciones->init();
        $cot=Yii::app()->createController('reportes/inventario');
        $this->reportes=$cot[0];
        $this->reportes->init();

    }
    public function actionIndex()
    {
        $this->redirect(Yii::app()->request->baseUrl.'/Articulos/listado');
    }


    public function actionListado()
    {
        $this->render('listado');
    }
    public function actionBuscar()
    {
        $condicion='';
        $con=0;
        if($_POST['descripcion']){
            $descripcion=mb_strtoupper($_POST['descripcion']);
            $condicion.="a.solic_descr like '%".$descripcion."%' ";
            $con++;
        }
        if($_POST['codigo']){
            if($con>0){
                $condicion.="AND ";
            }
            $codigo=mb_strtoupper($_POST['codigo']);
            $condicion.="a.solic_cprod like '%".$codigo."%' ";
            $con++;
        }
        if($_POST['cantidad']){
            if($con>0){
                $condicion.="AND ";
            }
            $cantidad = $this->funciones->TransformarMonto_bd($_POST['cantidad']);
            $condicion.="a.solic_canti like '%".$cantidad."%' ";
            $con++;
        }
        if($_POST['punidad']){
            if($con>0){
                $condicion.="AND ";
            }
            $punidad = $this->funciones->TransformarMonto_bd($_POST['punidad']);
            $condicion.="a.solic_punid like '%".$punidad."%' ";
            $con++;
        }
        if($con>0){
            $condicion="WHERE ".$condicion;
        }
        //$_SESSION['where']=$condicion;
        $this->renderpartial('buscar', array('condicion' => $condicion));       
    }
    public function actionConsultar()
    {
        $conexion=Yii::app()->db;
        $sql="SELECT * 
              FROM solicitud  a
              WHERE a.solic_codig ='".$_GET['c']."'";
        $solicitud=$conexion->createCommand($sql)->queryRow();
        $this->render('consultar', array('solicitud' => $solicitud));
    }
    public function actionRegistrar()
    {
        if($_POST){
            /*var_dump($_POST);
            exit();*/
            $descr=mb_strtoupper($_POST['descr']);
            $codig=mb_strtoupper($_POST['codig']);
            $punid=mb_strtoupper($_POST['punid']);
            $canti=mb_strtoupper($_POST['canti']);
            $tunid=mb_strtoupper($_POST['tunid']);
            $moned=mb_strtoupper($_POST['moned']);
            $obser=mb_strtoupper($_POST['obser']);
            $conexion=Yii::app()->db;
            $transaction=$conexion->beginTransaction();
            try{
                $sql="SELECT * FROM solicitud WHERE solic_descr = '".$descr."'";
                $inven=$conexion->createCommand($sql)->queryRow();
                if(!$inven){
                    $sql="SELECT * FROM solicitud WHERE solic_cprod = '".$codig."'";
                    $codigo=$conexion->createCommand($sql)->queryRow();
                    if(!$codigo){
                        $sql="INSERT INTO solicitud(solic_cprod,solic_descr,solic_punid,solic_canti,tunid_codig,
                                moned_codig,solic_obser,usuar_codig,solic_fcrea,solic_hcrea)
                              VALUES('".$codig."','".$descr."','".$punid."','".$canti."','".$tunid."',
                                '".$moned."','".$obser."','".Yii::app()->user->id['usuario']['codigo']."','".date('Y-m-d')."','".date('H:i:s')."')";
                        $res1=$conexion->createCommand($sql)->execute();
                        if($res1){
                            $transaction->commit();
                            $msg=array('success'=>'true','msg'=>'Producto guardado correctamente'); 
                        }else{
                            $transaction->rollBack();
                            $msg=array('success'=>'false','msg'=>'Error al guardar el Producto');   
                        }   
                    }else{
                        $transaction->rollBack();
                        $msg=array('success'=>'false','msg'=>'El código de producto ya existe ya existe');
                    }   
                }else{
                    $transaction->rollBack();
                    $msg=array('success'=>'false','msg'=>'El producto ya existe');
                }
            }catch(Exception $e){
                $transaction->rollBack();
                $msg=array('success'=>'false','msg'=>'Error al verificar la información');
            }
            
            echo json_encode($msg);
        }else{

            $this->render('registrar');
        }
    }
    public function actionModificar()
    {
        if($_POST){
            $cod=mb_strtoupper($_POST['codigo']);
            $descr=mb_strtoupper($_POST['descr']);
            $codig=mb_strtoupper($_POST['codig']);
            $punid=$this->TransformarMonto_bd(mb_strtoupper($_POST['punid']));
            $canti=$this->TransformarMonto_bd(mb_strtoupper($_POST['canti']));
            
            $tunid=mb_strtoupper($_POST['tunid']);
            $moned=mb_strtoupper($_POST['moned']);
            $obser=mb_strtoupper($_POST['obser']);
            if($contra==$ccontra){
                $conexion=Yii::app()->db;
                $transaction=$conexion->beginTransaction();
                try{
                    $sql="SELECT * FROM solicitud  WHERE solic_codig ='".$cod."'";
                    $roles=$conexion->createCommand($sql)->queryRow();
                    if($roles){
                        $sql="SELECT * FROM solicitud WHERE solic_cprod = '".$codig."'";
                        $codigo=$conexion->createCommand($sql)->queryRow();
                        if(!$codigo or $roles['solic_cprod']==$codig){
                                $sql="UPDATE solicitud 
                                        SET solic_cprod='".$codig."',
                                            solic_descr='".$descr."',
                                            solic_punid='".$punid."',
                                            solic_canti='".$canti."',
                                            tunid_codig='".$tunid."',
                                            moned_codig='".$moned."',
                                            solic_obser='".$obser."',
                                            usuar_codig='".Yii::app()->user->id['usuario']['codigo']."',
                                            solic_fcrea='".date('Y-m-d')."',
                                            solic_hcrea='".date('H:i:s')."'
                                        WHERE solic_codig ='".$cod."'";
                            $res1=$conexion->createCommand($sql)->execute();
                            if($res1){
                                $transaction->commit();
                                    $msg=array('success'=>'true','msg'=>'Producto actualizado correctamente');
                            }else{
                                $transaction->rollBack();
                                $msg=array('success'=>'false','msg'=>'Error al actualizar el Producto');    
                            }
                        }else{
                            $transaction->rollBack();
                            $msg=array('success'=>'false','msg'=>'El Código de producto ya existe');
                        }
                    }else{
                        $transaction->rollBack();
                        $msg=array('success'=>'false','msg'=>'El Producto no existe');
                    }
                }catch(Exception $e){
                    $transaction->rollBack();
                    $msg=array('success'=>'false','msg'=>'Error al verificar la información');
                }
            }else{
                $msg=array('success'=>'false','msg'=>'La contraseña y su confirmación no son iguales');
            }
            echo json_encode($msg);
        }else{
            $conexion=Yii::app()->db;
            
            $sql="SELECT * 
              FROM solicitud  a
              WHERE a.solic_codig ='".$_GET['c']."'";
            $roles=$conexion->createCommand($sql)->queryRow();
            $this->render('modificar', array('roles' => $roles));
        }
    }
    public function actionEliminar()
    {
        if($_POST){
            $codig=$_POST['codigo'];
            $conexion=Yii::app()->db;
            $transaction=$conexion->beginTransaction();
            try{
                $sql="SELECT * FROM factura_producto  WHERE solic_codig='".$codig."'";
                $prod=$conexion->createCommand($sql)->queryRow();
                if(!$prod){
                    $sql="SELECT * FROM solicitud  WHERE solic_codig='".$codig."'";
                    $roles=$conexion->createCommand($sql)->queryRow();
                    if($roles){

                        $sql="DELETE FROM solicitud  WHERE solic_codig='".$codig."'";
                        $res1=$conexion->createCommand($sql)->execute();
                        //echo $sql;
                        if($res1){
                            $transaction->commit();
                            $msg=array('success'=>'true','msg'=>' Producto eliminado correctamente');   
                        }else{
                            $transaction->rollBack();
                            $msg=array('success'=>'false','msg'=>'Error al eliminar el Producto '); 
                        }
                        
                    }else{
                        $transaction->rollBack();
                        $msg=array('success'=>'false','msg'=>'El Producto no existe');
                    }
                }else{
                    $transaction->rollBack();
                    $msg=array('success'=>'false','msg'=>'No se puede eliinar debido a que, esta asociada a una factura');
                }
            }catch(Exception $e){
                $transaction->rollBack();
                $msg=array('success'=>'false','msg'=>'Error al verificar la información');
            }
            echo json_encode($msg);
        }else{
            $conexion=Yii::app()->db;
            $sql="SELECT * 
              FROM solicitud  a
              WHERE a.solic_codig ='".$_GET['c']."'";
            $roles=$conexion->createCommand($sql)->queryRow();
            $this->render('eliminar', array('roles' => $roles));
        }
    }
    public function actionSolicitudes()
    {
        if($_POST){
            exit();
            $codig=$_POST['codigo'];
            $conexion=Yii::app()->db;
            $transaction=$conexion->beginTransaction();
            try{
                $sql="SELECT * FROM factura_producto  WHERE solic_codig='".$codig."'";
                $prod=$conexion->createCommand($sql)->queryRow();
                if(!$prod){
                    $sql="SELECT * FROM solicitud  WHERE solic_codig='".$codig."'";
                    $roles=$conexion->createCommand($sql)->queryRow();
                    if($roles){

                        $sql="DELETE FROM solicitud  WHERE solic_codig='".$codig."'";
                        $res1=$conexion->createCommand($sql)->execute();
                        //echo $sql;
                        if($res1){
                            $transaction->commit();
                            $msg=array('success'=>'true','msg'=>' Producto eliminado correctamente');   
                        }else{
                            $transaction->rollBack();
                            $msg=array('success'=>'false','msg'=>'Error al eliminar el Producto '); 
                        }
                        
                    }else{
                        $transaction->rollBack();
                        $msg=array('success'=>'false','msg'=>'El Producto no existe');
                    }
                }else{
                    $transaction->rollBack();
                    $msg=array('success'=>'false','msg'=>'No se puede eliinar debido a que, esta asociada a una factura');
                }
            }catch(Exception $e){
                $transaction->rollBack();
                $msg=array('success'=>'false','msg'=>'Error al verificar la información');
            }
            echo json_encode($msg);
        }else{
            $conexion=Yii::app()->db;
            $sql="SELECT * 
              FROM solicitud  a
              WHERE a.solic_codig ='".$_GET['c']."'";
            $roles=$conexion->createCommand($sql)->queryRow();
            $this->render('solicitudes', array('roles' => $roles));
        }
    }

    public function actionGenerarNotaEntrega()
    {
        if($_POST){
            
            $id=$_POST['id'];
            if(!$id){
                $msg=array('success'=>'false','msg'=>'Debe seleccionar al menos una solicitud');
                echo json_encode($msg);
                exit();
            }
            $usuar=Yii::app()->user->id['usuario']['codigo'];
            $fecha=date('Y-m-d');
            $hora=date('H:i:s');
            $estat=17;


            $conexion=Yii::app()->db;
            $transaction=$conexion->beginTransaction();
            try{
                $sql="SELECT max(movim_codig) codigo FROM inventario_movimiento";
                $movimientos=$conexion->createCommand($sql)->queryRow();
                $codig=$movimientos['codigo']+1;
                $cmovi='M_'.$this->funciones->generarCodigoPedido(6,$codig);

                $sql="INSERT INTO inventario_movimiento(movim_numer, almac_orige, almac_desti, mesta_codig, movim_obser, usuar_codig, movim_fcrea, movim_hcrea) 
                VALUES ('".$cmovi."', '1', '2', '1', 'TRASLADO DESDE ALMACEN A CONFIGURACIÓN', '".$usuar."', '".$fecha."', '".$hora."')";

                $query=$sql;
                $res1=$conexion->createCommand($sql)->execute();

                $sql="SELECT * 
                      FROM inventario_movimiento
                      WHERE movim_codig=(
                        SELECT max(movim_codig) codigo 
                        FROM inventario_movimiento)";
                $movimientos2=$conexion->createCommand($sql)->queryRow();

                $traza=$this->funciones->guardarTraza($conexion, 'I', 'inventario_movimiento', json_encode($movimientos), json_encode($movimientos2),$query);


                $codigo=$cmovi.'_'.date('Ymd_His_').$c.'.pdf';
                $ruta='files/inventario/notaEntrega/'.$codigo;

                $sql="INSERT INTO solicitud_traslado(trasl_ruta, ttras_codig, pesta_codig, usuar_codig, trasl_fcrea, trasl_hcrea) 
                VALUES ('".$ruta."', '1', '1', '".$usuar."', '".$fecha."', '".$hora."')";
                $res3=$conexion->createCommand($sql)->execute();

                $sql="SELECT * 
                      FROM solicitud_traslado
                      WHERE trasl_codig=(
                        SELECT max(trasl_codig) codigo 
                        FROM solicitud_traslado)";

                $traslado=$conexion->createCommand($sql)->queryRow();

                foreach ($id as $key => $value) {

                    $sql="SELECT * FROM solicitud WHERE solic_codig='".$value."'";
                    $solicitud=$conexion->createCommand($sql)->queryRow();

                    $sql="SELECT * FROM solicitud_asignacion WHERE solic_codig='".$value."'";
                    $asignaciones=$conexion->createCommand($sql)->queryAll();

                    foreach ($asignaciones as $key => $asignacion) {
                        $sql="INSERT INTO inventario_movimiento_detalle(movim_codig, seria_codig, usuar_codig, mdeta_fcrea, mdeta_hcrea) 
                        VALUES ('".$movimientos2['movim_codig']."', '".$asignacion['asign_dispo']."', '".$usuar."', '".$fecha."', '".$hora."')";
                        $res1=$conexion->createCommand($sql)->execute();

                        $sql="INSERT INTO inventario_movimiento_detalle(movim_codig, seria_codig, usuar_codig, mdeta_fcrea, mdeta_hcrea) 
                        VALUES ('".$movimientos2['movim_codig']."', '".$asignacion['asign_opera']."', '".$usuar."', '".$fecha."', '".$hora."')";
                        $res2=$conexion->createCommand($sql)->execute();
                    }

                    $sql="INSERT INTO solicitud_movimiento_traslado(solic_codig, movim_codig, trasl_codig, usuar_codig, smtra_fcrea, smtra_hcrea)
                    VALUES ('".$solicitud['solic_codig']."', '".$movimientos2['movim_codig']."', '".$traslado['trasl_codig']."', '".$usuar."', '".$fecha."', '".$hora."')";
                    $res4=$conexion->createCommand($sql)->execute();

                    
                    if(!$res4){
                        $transaction->rollBack();
                        $msg=array('success'=>'false','msg'=>'No se pudo guardar la Nota de Entrega');
                        echo json_encode($msg);
                        exit();
                    }

                    $sql="UPDATE solicitud
                            SET estat_codig='".$estat."'
                          WHERE solic_codig='".$solicitud['solic_codig']."'";
                    $query=$sql;

                    $res1=$conexion->createCommand($sql)->execute();

                    $sql="SELECT * FROM solicitud WHERE solic_codig='".$value."'";
                    $solicitud2=$conexion->createCommand($sql)->queryRow();

                    $traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud', json_encode($solicitud), json_encode($solicitud2),$query);

                    
						$notificacion=$this->funciones->GuardarNotificacion($solicitud['solic_codig'], $estat);
						
						$trayectoria=$this->funciones->guardarTrayectoriaSolicitudes($conexion, $solicitud['solic_codig'], $estat, $solicitud['estat_codig'], 2);
                    $this->reportes->guardarNotaEntrega($solicitud['solic_codig'],$ruta);
                }
                

                $transaction->commit();
                $msg=array('success'=>'true','msg'=>'Nota de Entrega generada correctamente','destino'=>Yii::app()->request->getBaseUrl(true).'/reportes/inventario/traslado?c='.$traslado['trasl_codig']);
                
            }catch(Exception $e){
                var_dump($e);
                $transaction->rollBack();
                $msg=array('success'=>'false','msg'=>'Error al verificar la información');
            }
            echo json_encode($msg);
        }else{
            $this->render('generarNotaEntrega');
        }
    }

    public function actionReversarNotaEntrega()
    {
        if($_POST){
            $id=$_POST['id'];
            if(!$id){
                $msg=array('success'=>'false','msg'=>'Debe seleccionar al menos una solicitud');
                echo json_encode($msg);
                exit();
            }
            $usuar=Yii::app()->user->id['usuario']['codigo'];
            $fecha=date('Y-m-d');
            $hora=date('H:i:s');
            $estat=15;
            $ruta="files/tramitacion/certificacion/";
            $nombre=date('Ymd_His').'.csv';
            $destino=$ruta.$nombre;


            $conexion=Yii::app()->db;
            $transaction=$conexion->beginTransaction();
            try{
                foreach ($id as $key => $value) {
                    $sql="SELECT * FROM solicitud_traslado WHERE trasl_codig = '".$value."' ";
                    $tramitacion=$conexion->createCommand($sql)->queryRow();
                    if($tramitacion){
                        $sql="UPDATE solicitud_traslado 
                            SET pesta_codig=2
                            WHERE trasl_codig='".$value."'";
                        $query=$sql;
                        $res1=$conexion->createCommand($sql)->execute();

                        $sql="SELECT * FROM solicitud_traslado WHERE trasl_codig = '".$value."' ";
                        $tramitacion2=$conexion->createCommand($sql)->queryRow();
                        $traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud_traslado', json_encode($tramitacion),json_encode($tramitacion2),$query);
                        
                        $sql="SELECT * FROM solicitud_movimiento_traslado 
                                WHERE trasl_codig = '".$value."' ";
                        $solicitud_movimiento_traslado=$conexion->createCommand($sql)->queryAll();
                        foreach ($solicitud_movimiento_traslado as $k => $row) {
                            $sql="SELECT * 
                                  FROM solicitud 
                                  WHERE solic_codig='".$row['solic_codig']."'";
                            $solicitud=$conexion->createCommand($sql)->queryRow();

                            if($solicitud){
                                $sql="UPDATE solicitud 
                                    SET estat_codig='".$estat."'
                                    WHERE solic_codig='".$row['solic_codig']."'";
                                $query=$sql;
                                $res1=$conexion->createCommand($sql)->execute();

                                $sql="SELECT * 
                                  FROM solicitud 
                                  WHERE solic_codig='".$row['solic_codig']."'";
                                $solicitud2=$conexion->createCommand($sql)->queryRow();
                                $traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud', json_encode($solicitud),json_encode($solicitud2),$query);

                                
						$notificacion=$this->funciones->GuardarNotificacion($solicitud['solic_codig'], $estat);
						
						$trayectoria=$this->funciones->guardarTrayectoriaSolicitudes($conexion, $solicitud['solic_codig'], $estat, $solicitud['estat_codig'], 2);    
                            }else{
                                $transaction->rollBack();
                                $msg=array('success'=>'false','msg'=>'No se encontro la solicitud');
                                echo json_encode($msg);
                                exit();
                            }

                            $sql="SELECT * 
                                  FROM inventario_movimiento 
                                  WHERE movim_codig='".$row['movim_codig']."'";
                            $movimiento=$conexion->createCommand($sql)->queryRow();

                            if($movimiento){
                                $sql="UPDATE inventario_movimiento 
                                    SET mesta_codig='3'
                                    WHERE movim_codig='".$row['movim_codig']."'";
                                $query=$sql;
                                $res1=$conexion->createCommand($sql)->execute();

                                $sql="SELECT * 
                                  FROM inventario_movimiento 
                                  WHERE mesta_codig='".$row['mesta_codig']."'";
                                $movimiento2=$conexion->createCommand($sql)->queryRow();
                                $traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud', json_encode($movimiento),json_encode($movimiento2),$query);
                                   
                            }else{
                                $transaction->rollBack();
                                $msg=array('success'=>'false','msg'=>'No se encontro el movimiento');
                                echo json_encode($msg);
                                exit();
                            }
                        }

                    }else{
                        $transaction->rollBack();
                        $msg=array('success'=>'false','msg'=>'No se encontro el archivo');
                        echo json_encode($msg);
                        exit();
                    }
                }
                $transaction->commit();
                $msg=array('success'=>'true','msg'=>'Archivo reversado correctamente');             
            }catch(Exception $e){
                var_dump($e);
                $transaction->rollBack();
                $msg=array('success'=>'false','msg'=>'Error al verificar la información');
            }
            echo json_encode($msg);
        }else{
            $this->render('reversarNotaEntrega');
        }
    }

    public function actionRealizarTraslado()
    {
        if($_POST){
            $id=$_POST['id'];
            if(!$id){
                $msg=array('success'=>'false','msg'=>'Debe seleccionar al menos una solicitud');
                echo json_encode($msg);
                exit();
            }
            $usuar=Yii::app()->user->id['usuario']['codigo'];
            $fecha=date('Y-m-d');
            $hora=date('H:i:s');
            $estat=18;

            $conexion=Yii::app()->db;
            $transaction=$conexion->beginTransaction();
            try{
                foreach ($id as $key => $value) {
                    $sql="SELECT * FROM solicitud_traslado WHERE trasl_codig = '".$value."' ";
                    $traslado=$conexion->createCommand($sql)->queryRow();
                    $sql="UPDATE solicitud_traslado 
                        SET pesta_codig='3'
                        WHERE trasl_codig='".$value."'";
                    $query=$sql;
                    $res1=$conexion->createCommand($sql)->execute();

                    $sql="SELECT * FROM solicitud_traslado WHERE trasl_codig = '".$value."' ";
                    $traslado2=$conexion->createCommand($sql)->queryRow();

                    $traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud_traslado', json_encode($traslado2),json_encode($traslado),$query);

                    if($traslado){

                        $sql="SELECT solic_codig
                              FROM solicitud_movimiento_traslado a
                              WHERE trasl_codig='".$traslado['trasl_codig']."'
                              GROUP BY 1";
                        $solic=$conexion->createCommand($sql)->queryAll();

                        foreach ($solic as $k => $v) {
                            $sql="SELECT *
                              FROM solicitud a
                              WHERE solic_codig='".$v['solic_codig']."'
                              GROUP BY 1";
                            $solicitud=$conexion->createCommand($sql)->queryRow();

                            $sql="UPDATE solicitud 
                            SET estat_codig='".$estat."'
                            WHERE solic_codig='".$v['solic_codig']."'";
                            $query=$sql;
                            $res1=$conexion->createCommand($sql)->execute();

                            $sql="SELECT * FROM solicitud WHERE solic_codig = '".$v['solic_codig']."' ";
                            $solicitud2=$conexion->createCommand($sql)->queryRow();

                            $traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud', json_encode($solicitud),json_encode($solicitud2),$query);
                            
                            
						$notificacion=$this->funciones->GuardarNotificacion($solicitud['solic_codig'], $estat);
						
						$trayectoria=$this->funciones->guardarTrayectoriaSolicitudes($conexion, $solicitud['solic_codig'], $estat, $solicitud['estat_codig'], 2);
                        }

                        $sql="SELECT movim_codig
                              FROM solicitud_movimiento_traslado a
                              WHERE trasl_codig='".$traslado['trasl_codig']."'
                              GROUP BY 1";
                        $movim=$conexion->createCommand($sql)->queryAll();

                        foreach ($movim as $k => $v) {
                            $sql="SELECT *
                              FROM inventario_movimiento a
                              WHERE movim_codig='".$v['movim_codig']."'
                              GROUP BY 1";
                            $movimiento=$conexion->createCommand($sql)->queryAll();

                            $sql="UPDATE inventario_movimiento 
                            SET mesta_codig='2'
                            WHERE movim_codig='".$v['movim_codig']."'";
                            $query=$sql;
                            $res1=$conexion->createCommand($sql)->execute();

                            $sql="SELECT * FROM inventario_movimiento WHERE movim_codig = '".$v['movim_codig']."' ";
                            $movimiento2=$conexion->createCommand($sql)->queryRow();

                            $traza=$this->funciones->guardarTraza($conexion, 'U', 'inventario_movimiento', json_encode($movimiento),json_encode($movimiento2),$query);


                            $sql="SELECT * 
                                  FROM inventario_movimiento_detalle a 
                                  WHERE movim_codig='".$v['movim_codig']."'";
                            $seriales=$conexion->createCommand($sql)->queryAll();
                            foreach ($seriales as $l => $serial) {
                                $sql="SELECT *
                                  FROM inventario_seriales a
                                  WHERE seria_codig='".$serial['seria_codig']."'";
                                $seria=$conexion->createCommand($sql)->queryRow();

                                $sql="UPDATE inventario_seriales 
                                SET almac_codig='".$movimiento2['almac_desti']."'
                                WHERE seria_codig='".$serial['seria_codig']."'";
                                $query=$sql;
                                $res1=$conexion->createCommand($sql)->execute();

                                $sql="SELECT *
                                  FROM inventario_seriales a
                                  WHERE seria_codig='".$serial['seria_codig']."' ";
                                $seria2=$conexion->createCommand($sql)->queryRow();

                                $traza=$this->funciones->guardarTraza($conexion, 'U', 'inventario_seriales', json_encode($seria),json_encode($seria2),$query);
                            }
                            
                        }

                        $name=explode('/', $traslado['trasl_ruta']);
                        $name=end($name);
                        //$correo=$this->funciones->enviarCorreoAdjunto('../inventario/traslado/aprobar_correo',$solicitud,'RapidPago | Traslado',$solicitud['solic_celec'],$traslado['trasl_ruta'],$name);
                        
                    }else{
                        $transaction->rollBack();
                        $msg=array('success'=>'false','msg'=>'No se encontro el Traslado');
                        echo json_encode($msg);
                        exit();
                    }
                }
                $transaction->commit();
                $msg=array('success'=>'true','msg'=>'Solicitudes Aprobadas correctamente');             
            }catch(Exception $e){
                var_dump($e);
                $transaction->rollBack();
                $msg=array('success'=>'false','msg'=>'Error al verificar la información');
            }
            echo json_encode($msg);
        }else{
            $this->render('realizarTraslado');
        }
    }
    public function actionReversarTraslado()
    {
        if($_POST){
            $id=$_POST['id'];
            if(!$id){
                $msg=array('success'=>'false','msg'=>'Debe seleccionar al menos una solicitud');
                echo json_encode($msg);
                exit();
            }
            $usuar=Yii::app()->user->id['usuario']['codigo'];
            $fecha=date('Y-m-d');
            $hora=date('H:i:s');
            $estat=17;

            $conexion=Yii::app()->db;
            $transaction=$conexion->beginTransaction();
            try{
                foreach ($id as $key => $value) {
                    $sql="SELECT * FROM solicitud_traslado WHERE trasl_codig = '".$value."' ";
                    $traslado=$conexion->createCommand($sql)->queryRow();
                    $sql="UPDATE solicitud_traslado 
                        SET pesta_codig='1'
                        WHERE trasl_codig='".$value."'";
                    $query=$sql;
                    $res1=$conexion->createCommand($sql)->execute();

                    $sql="SELECT * FROM solicitud_traslado WHERE trasl_codig = '".$value."' ";
                    $traslado2=$conexion->createCommand($sql)->queryRow();

                    $traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud_traslado', json_encode($traslado2),json_encode($traslado),$query);

                    if($traslado){

                        $sql="SELECT * 
                              FROM solicitud 
                              WHERE solic_codig in (
                                SELECT solic_codig
                                FROM solicitud_movimiento_traslado a
                                WHERE trasl_codig='".$traslado['trasl_codig']."'
                                GROUP BY 1)";
                        $solitudes=$conexion->createCommand($sql)->queryAll();

                        foreach ($solitudes as $k => $solicitud) {

                            $sql="UPDATE solicitud 
                            SET estat_codig='".$estat."'
                            WHERE solic_codig='".$solicitud['solic_codig']."'";
                            $query=$sql;
                            $res1=$conexion->createCommand($sql)->execute();

                            $sql="SELECT * FROM solicitud WHERE solic_codig = '".$solicitud['solic_codig']."' ";
                            $solicitud2=$conexion->createCommand($sql)->queryRow();

                            $traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud', json_encode($solicitud),json_encode($solicitud2),$query);
                            
                            
						$notificacion=$this->funciones->GuardarNotificacion($solicitud['solic_codig'], $estat);
						
						$trayectoria=$this->funciones->guardarTrayectoriaSolicitudes($conexion, $solicitud['solic_codig'], $estat, $solicitud['estat_codig'], 2);
                        }

                        $sql="SELECT * 
                              FROM inventario_movimiento
                              WHERE movim_codig in (
                                SELECT movim_codig
                                FROM solicitud_movimiento_traslado a
                                WHERE trasl_codig='".$traslado['trasl_codig']."'
                                GROUP BY 1)";
                        $movimientos=$conexion->createCommand($sql)->queryAll();

                        foreach ($movimientos as $k => $movimiento) {
                            
                            $sql="UPDATE inventario_movimiento 
                            SET mesta_codig='2'
                            WHERE movim_codig='".$movimiento['movim_codig']."'";
                            $query=$sql;
                            $res1=$conexion->createCommand($sql)->execute();

                            $sql="SELECT * FROM inventario_movimiento WHERE movim_codig = '".$movimiento['movim_codig']."' ";
                            $movimiento2=$conexion->createCommand($sql)->queryRow();

                            $traza=$this->funciones->guardarTraza($conexion, 'U', 'inventario_movimiento', json_encode($movimiento),json_encode($movimiento2),$query);


                            $sql="SELECT *
                                  FROM inventario_seriales a
                                  WHERE seria_codig in (
                                    SELECT seria_codig
                                    FROM inventario_movimiento_detalle a 
                                    WHERE movim_codig='".$v['movim_codig']."'
                                    GROUP BY 1
                                )";
                            $seriales=$conexion->createCommand($sql)->queryAll();

                            foreach ($seriales as $l => $seria) {

                                $sql="UPDATE inventario_seriales 
                                SET almac_codig='".$movimiento['almac_orige']."'
                                WHERE seria_codig='".$seria['seria_codig']."'";
                                $query=$sql;
                                $res1=$conexion->createCommand($sql)->execute();

                                $sql="SELECT *
                                  FROM inventario_seriales a
                                  WHERE seria_codig='".$serial['seria_codig']."' ";
                                $seria2=$conexion->createCommand($sql)->queryRow();

                                $traza=$this->funciones->guardarTraza($conexion, 'U', 'inventario_seriales', json_encode($seria),json_encode($seria2),$query);
                            }
                            
                        }

                        $this->funciones->enviarCorreo('../inventario/traslado/rechazar_correo',$datos,'RapidPago | Traslado Anulado',$solicitud['solic_celec']);
                    }else{
                        $transaction->rollBack();
                        $msg=array('success'=>'false','msg'=>'No se encontro el Traslado');
                        echo json_encode($msg);
                        exit();
                    }
                }
                $transaction->commit();
                $msg=array('success'=>'true','msg'=>'Solicitudes Reversadas correctamente');                
            }catch(Exception $e){
                var_dump($e);
                $transaction->rollBack();
                $msg=array('success'=>'false','msg'=>'Error al verificar la información');
            }
            echo json_encode($msg);
        }else{
            $this->render('reversarTraslado');
        }
    }
    // Uncomment the following methods and override them if needed
    /*
    public function filters()
    {
        // return the filter configuration for this controller, e.g.:
        return array(
            'inlineFilterName',
            array(
                'class'=>'path.to.FilterClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }

    public function actions()
    {
        // return external action classes, e.g.:
        return array(
            'action1'=>'path.to.ActionClass',
            'action2'=>array(
                'class'=>'path.to.AnotherActionClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }
    */
}