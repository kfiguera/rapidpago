<?php

class AsignarController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->redirect(Yii::app()->request->baseUrl.'/Articulos/listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descripcion']){
			$descripcion=mb_strtoupper($_POST['descripcion']);
			$condicion.="a.inven_descr like '%".$descripcion."%' ";
			$con++;
		}
		if($_POST['codigo']){
			if($con>0){
				$condicion.="AND ";
			}
			$codigo=mb_strtoupper($_POST['codigo']);
			$condicion.="a.inven_cprod like '%".$codigo."%' ";
			$con++;
		}
		if($_POST['cantidad']){
			if($con>0){
				$condicion.="AND ";
			}
			$cantidad = $this->funciones->TransformarMonto_bd($_POST['cantidad']);
			$condicion.="a.inven_canti like '%".$cantidad."%' ";
			$con++;
		}
		if($_POST['punidad']){
			if($con>0){
				$condicion.="AND ";
			}
			$punidad = $this->funciones->TransformarMonto_bd($_POST['punidad']);
			$condicion.="a.inven_punid like '%".$punidad."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM inventario  a
			  WHERE a.inven_codig ='".$_GET['c']."'";
		$roles=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('roles' => $roles));
	}
	public function actionRegistrar()
	{
		if($_POST){
			/*var_dump($_POST);
			exit();*/
			$descr=mb_strtoupper($_POST['descr']);
			$codig=mb_strtoupper($_POST['codig']);
			$punid=mb_strtoupper($_POST['punid']);
			$canti=mb_strtoupper($_POST['canti']);
			$tunid=mb_strtoupper($_POST['tunid']);
			$moned=mb_strtoupper($_POST['moned']);
			$obser=mb_strtoupper($_POST['obser']);
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM inventario WHERE inven_descr = '".$descr."'";
				$inven=$conexion->createCommand($sql)->queryRow();
				if(!$inven){
					$sql="SELECT * FROM inventario WHERE inven_cprod = '".$codig."'";
					$codigo=$conexion->createCommand($sql)->queryRow();
					if(!$codigo){
						$sql="INSERT INTO inventario(inven_cprod,inven_descr,inven_punid,inven_canti,tunid_codig,
								moned_codig,inven_obser,usuar_codig,inven_fcrea,inven_hcrea)
							  VALUES('".$codig."','".$descr."','".$punid."','".$canti."','".$tunid."',
							  	'".$moned."','".$obser."','".Yii::app()->user->id['usuario']['codigo']."','".date('Y-m-d')."','".date('H:i:s')."')";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Producto guardado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al guardar el Producto');	
						}	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El código de producto ya existe ya existe');
					}	
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El producto ya existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$cod=mb_strtoupper($_POST['codigo']);
			$descr=mb_strtoupper($_POST['descr']);
			$codig=mb_strtoupper($_POST['codig']);
			$punid=$this->TransformarMonto_bd(mb_strtoupper($_POST['punid']));
			$canti=$this->TransformarMonto_bd(mb_strtoupper($_POST['canti']));
			
			$tunid=mb_strtoupper($_POST['tunid']);
			$moned=mb_strtoupper($_POST['moned']);
			$obser=mb_strtoupper($_POST['obser']);
			if($contra==$ccontra){
				$conexion=Yii::app()->db;
				$transaction=$conexion->beginTransaction();
				try{
					$sql="SELECT * FROM inventario  WHERE inven_codig ='".$cod."'";
					$roles=$conexion->createCommand($sql)->queryRow();
					if($roles){
						$sql="SELECT * FROM inventario WHERE inven_cprod = '".$codig."'";
						$codigo=$conexion->createCommand($sql)->queryRow();
						if(!$codigo or $roles['inven_cprod']==$codig){
								$sql="UPDATE inventario 
								  		SET inven_cprod='".$codig."',
											inven_descr='".$descr."',
											inven_punid='".$punid."',
											inven_canti='".$canti."',
											tunid_codig='".$tunid."',
											moned_codig='".$moned."',
											inven_obser='".$obser."',
											usuar_codig='".Yii::app()->user->id['usuario']['codigo']."',
											inven_fcrea='".date('Y-m-d')."',
											inven_hcrea='".date('H:i:s')."'
										WHERE inven_codig ='".$cod."'";
							$res1=$conexion->createCommand($sql)->execute();
							if($res1){
								$transaction->commit();
									$msg=array('success'=>'true','msg'=>'Producto actualizado correctamente');
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al actualizar el Producto');	
							}
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'El Código de producto ya existe');
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Producto no existe');
					}
				}catch(Exception $e){
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error al verificar la información');
				}
			}else{
				$msg=array('success'=>'false','msg'=>'La contraseña y su confirmación no son iguales');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			
			$sql="SELECT * 
			  FROM inventario  a
			  WHERE a.inven_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('roles' => $roles));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codigo'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM factura_producto  WHERE inven_codig='".$codig."'";
				$prod=$conexion->createCommand($sql)->queryRow();
				if(!$prod){
					$sql="SELECT * FROM inventario  WHERE inven_codig='".$codig."'";
					$roles=$conexion->createCommand($sql)->queryRow();
					if($roles){

						$sql="DELETE FROM inventario  WHERE inven_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Producto eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Producto ');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Producto no existe');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No se puede eliinar debido a que, esta asociada a una factura');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM inventario  a
			  WHERE a.inven_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('roles' => $roles));
		}
	}
	public function actionSolicitudes()
	{
		if($_POST){
			exit();
			$codig=$_POST['codigo'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM factura_producto  WHERE inven_codig='".$codig."'";
				$prod=$conexion->createCommand($sql)->queryRow();
				if(!$prod){
					$sql="SELECT * FROM inventario  WHERE inven_codig='".$codig."'";
					$roles=$conexion->createCommand($sql)->queryRow();
					if($roles){

						$sql="DELETE FROM inventario  WHERE inven_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Producto eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Producto ');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Producto no existe');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No se puede eliinar debido a que, esta asociada a una factura');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM inventario  a
			  WHERE a.inven_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('solicitudes', array('roles' => $roles));
		}
	}
	public function TransformarMonto_bd($monto)
	{
		//DEBE DE ESTAR 0.000,00
		$monto=str_replace('.', '', $monto);
		$monto=str_replace(',', '.', $monto);
		//RETORNA 0000.00
		return $monto;
	}
	public function TransformarMonto_v($monto,$cantidad)
	{
		//DEBE DE ESTAR 0000.00
		$monto=number_format($monto,$cantidad,',','.');
		//RETORNA 0.000,00
		return $monto;
	}
	public function TransformarFecha_bd($fecha)
	{
		//DEBE DE ESTAR DD/MM/AAAA
		$fecha=explode('/',$fecha);
		$fecha=$fecha[2].'-'.$fecha[1].'-'.$fecha[0];
		//RETORNA AAAA-MM-DD
		return $fecha;
	}
	public function TransformarFecha_v($fecha)
	{
		//DEBE DE ESTAR AAAA-MM-DD
		$fecha=explode('-',$fecha);
		$fecha=$fecha[2].'/'.$fecha[1].'/'.$fecha[0];
		//RETORNA DD/MM/AAAA
		return $fecha;
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}