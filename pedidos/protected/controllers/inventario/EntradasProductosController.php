<?php

class EntradasProductosController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$f=$_GET['f'];
		$this->redirect(Yii::app()->request->baseUrl.'/ventas/producto/listado?f='.$f);
	}

	public function actionListado()
	{
		$f=$_GET['f'];
		$this->render('listado',array('f' => $f ));
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		$f=$_POST['nfact'];
		if($_POST['descr']){
			$descr=mb_strtoupper($_POST['descr']);
			$condicion.="AND b.inven_descr like '%".$descr."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE factu_codig='".$f."' ".$condicion;
		}
		
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion,'f' => $f ));		
	}
	public function actionConsultar()
	{
		$f=$_GET['f'];
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM factura_producto  a
			  WHERE a.fprod_codig ='".$_GET['c']."'";
		$roles=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('roles' => $roles, 'f' => $f ));
	}
	public function actionRegistrar()
	{
		if($_POST){
			$nfact=mb_strtoupper($_POST["nfact"]);
			foreach ($_POST["produ"] as $key => $value) {
				$producto[$key]["model"]=$_POST["model"][$key];
				$producto[$key]["produ"]=$_POST["produ"][$key];
				$producto[$key]["seria"]=$_POST["seria"][$key];
				$producto[$key]["cprod"]=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["cprod"][$key]));
				$producto[$key]["pprod"]=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["pprod"][$key]));
				$producto[$key]["mprod"]=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["mprod"][$key]));
			}
			$obser=mb_strtoupper($_POST["obser"]);
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM inventario_entrada WHERE ientr_codig = '".$nfact."'";
				$entrada=$conexion->createCommand($sql)->queryRow();
				if($entrada){
					foreach ($producto as $key => $value) {
						$sql="INSERT INTO inventario_entrada_producto(ientr_codig, model_codig, inven_codig, inven_seria, iepro_canti, iepro_preci, iepro_monto, iepro_obser, usuar_codig, iepro_fcrea, iepro_hcrea) 
						VALUES ( '".$nfact."', '".$value['model']."', '".$value['produ']."', '".$value['seria']."', '".$value['cprod']."', '".$value['pprod']."', '".$value['mprod']."', '".$value['obser']."', '".Yii::app()->user->id['usuario']['codigo']."','".date('Y-m-d')."','".date('H:i:s')."')";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$msg=array('success'=>'true','msg'=>'Producto guardado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al guardar el producto');
							echo json_encode($msg);;
							exit();
						}	
					}
					if ($msg['success']=='true') {
						$sql="SELECT * FROM inventario_entrada_producto WHERE ientr_codig ='".$nfact."'";
						$prod=$conexion->createCommand($sql)->queryAll();
						
						foreach ($prod as $key => $value) {
							$mtotal+=$value['iepro_monto'];
						}
						$sql="UPDATE inventario_entrada 
								  SET ientr_monto = '".$mtotal."'
									 WHERE ientr_codig ='".$nfact."'";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar la Entrada');	
						}

					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'La inventario_entrada no existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$e=$_GET['e'];
		$this->render('registrar',array('e' => $e ));
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codig=mb_strtoupper($_POST["codig"]);
			$nfact=mb_strtoupper($_POST["nfact"]);
			foreach ($_POST["produ"] as $key => $value) {
				$producto[$key]["model"]=$_POST["model"][$key];
				$producto[$key]["produ"]=$_POST["produ"][$key];
				$producto[$key]["seria"]=$_POST["seria"][$key];
				$producto[$key]["cprod"]=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["cprod"][$key]));
				$producto[$key]["pprod"]=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["pprod"][$key]));
				$producto[$key]["mprod"]=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["mprod"][$key]));
			}
			$obser=mb_strtoupper($_POST["obser"]);

			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM inventario_entrada_producto  WHERE iepro_codig ='".$codig."'";
				$entrada=$conexion->createCommand($sql)->queryRow();
				if($entrada){
					foreach ($producto as $key => $value) {
						$sql="UPDATE inventario_entrada_producto
						  SET model_codig = '".$value['model']."',
							  inven_codig = '".$value['produ']."',
							  inven_seria = '".$value['seria']."',
							  iepro_canti = '".$value['cprod']."',
							  iepro_preci = '".$value['pprod']."',
							  iepro_monto = '".$value['mprod']."',
							  iepro_obser = '".$obser."'  
							 WHERE iepro_codig ='".$codig."'";
				
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$msg=array('success'=>'true','msg'=>'Producto Actualizado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar el producto');
							echo json_encode($msg);;
							exit();
						}	
					}
					if ($msg['success']=='true') {
						$sql="SELECT * FROM inventario_entrada_producto WHERE ientr_codig ='".$nfact."'";
						$prod=$conexion->createCommand($sql)->queryAll();
						
						foreach ($prod as $key => $value) {
							$mtotal+=$value['iepro_monto'];
						}
						$sql="UPDATE inventario_entrada 
								  SET ientr_monto = '".$mtotal."'
									 WHERE ientr_codig ='".$nfact."'";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar la Entrada');	
						}

					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'La Entrada no existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$e=$_GET['e'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM inventario_entrada_producto  a
			  WHERE a.iepro_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('roles' => $roles,'e'=>$e));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$nfact=mb_strtoupper($_POST["nfact"]);
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM inventario_entrada_producto  WHERE iepro_codig ='".$codig."'";
				$roles=$conexion->createCommand($sql)->queryRow();
				if($roles){

					$sql="DELETE FROM inventario_entrada_producto  WHERE iepro_codig ='".$codig."'";
					$res1=$conexion->createCommand($sql)->execute();
					//echo $sql;
					if($res1){
						$msg=array('success'=>'true','msg'=>' Producto eliminado correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al eliminar la Entrada');	
					}
					if ($msg['success']=='true') {
						$sql="SELECT * FROM inventario_entrada_producto WHERE ientr_codig ='".$nfact."'";
						$prod=$conexion->createCommand($sql)->queryAll();
						
						foreach ($prod as $key => $value) {
							$mtotal+=$value['iepro_monto'];
						}
						$sql="UPDATE inventario_entrada 
								  SET ientr_monto = '".$mtotal."'
									 WHERE ientr_codig ='".$nfact."'";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar la Entrada');	
						}

					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'La Entrada no existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$e=$_GET['e'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM inventario_entrada_producto  a
			  WHERE a.iepro_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('roles' => $roles,'e'=>$e));
		}
	}
	
	public function TransformarMonto_bd($monto)
	{
		//DEBE DE ESTAR 0.000,00
		$monto=str_replace('.', '', $monto);
		$monto=str_replace(',', '.', $monto);
		//RETORNA 0000.00
		return $monto;
	}
	public function TransformarMonto_v($monto,$cantidad)
	{
		//DEBE DE ESTAR 0000.00
		$monto=number_format($monto,$cantidad,',','.');
		//RETORNA 0.000,00
		return $monto;
	}
	public function TransformarFecha_bd($fecha)
	{
		//DEBE DE ESTAR DD/MM/AAAA
		$fecha=explode('/',$fecha);
		$fecha=$fecha[2].'-'.$fecha[1].'-'.$fecha[0];
		//RETORNA AAAA-MM-DD
		return $fecha;
	}
	public function TransformarFecha_v($fecha)
	{
		//DEBE DE ESTAR AAAA-MM-DD
		$fecha=explode('/',$fecha);
		$fecha=$fecha[2].'-'.$fecha[1].'-'.$fecha[0];
		//RETORNA DD/MM/AAAA
		return $fecha;
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}