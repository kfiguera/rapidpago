<?php

class EntradaController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->redirect(Yii::app()->request->baseUrl.'/inventario/entrada/listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descripcion']){
			$descripcion=mb_strtoupper($_POST['descripcion']);
			$condicion.="a.inven_descr like '%".$descripcion."%' ";
			$con++;
		}
		if($_POST['codigo']){
			if($con>0){
				$condicion.="AND ";
			}
			$codigo=mb_strtoupper($_POST['codigo']);
			$condicion.="a.inven_cprod like '%".$codigo."%' ";
			$con++;
		}
		if($_POST['cantidad']){
			if($con>0){
				$condicion.="AND ";
			}
			$cantidad = $this->funciones->TransformarMonto_bd($_POST['cantidad']);
			$condicion.="a.inven_canti like '%".$cantidad."%' ";
			$con++;
		}
		if($_POST['punidad']){
			if($con>0){
				$condicion.="AND ";
			}
			$punidad = $this->funciones->TransformarMonto_bd($_POST['punidad']);
			$condicion.="a.inven_punid like '%".$punidad."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM inventario_entrada  a
			  WHERE a.ientr_codig ='".$_GET['c']."'";
		$entrada=$conexion->createCommand($sql)->queryRow();
		$sql="SELECT * 
			  FROM inventario_proveedor  a
			  WHERE a.prove_codig ='".$entrada['prove_codig']."'";
		$inventario_proveedor=$conexion->createCommand($sql)->queryRow();
		$sql="SELECT a.* , b.inven_descr, c.model_descr
			  FROM inventario_entrada_producto  a
			  JOIN inventario b ON (a.inven_codig = b.inven_codig)
			  JOIN inventario_modelo c ON (a.model_codig = c.model_codig)
			  WHERE a.ientr_codig ='".$entrada['ientr_codig']."'";
		$productos=$conexion->createCommand($sql)->queryAll();
		$this->render('consultar', array('entrada' => $entrada,'inventario_proveedor' => $inventario_proveedor,'productos' => $productos));
	}
	public function actionRegistrar()
	{
		if($_POST){
			//PROVEEDOR
			$clien=mb_strtoupper($_POST["clien"]);
			$denom=mb_strtoupper($_POST["denom"]);
			$tclie=mb_strtoupper($_POST["tclie"]);
			$tdocu=mb_strtoupper($_POST["tdocu"]);
			$ndocu=mb_strtoupper($_POST["ndocu"]);
			$ccont=mb_strtoupper($_POST["ccont"]);
			$corre=mb_strtoupper($_POST["corre"]);
			$telef=mb_strtoupper($_POST["telef"]);
			$direc=mb_strtoupper($_POST["direc"]);
			$obser=mb_strtoupper($_POST["obser"]);
			//FACTURA
			$nfact=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["nfact"]));
			$ncont=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["ncont"]));
			$femis=mb_strtoupper($this->funciones->TransformarFecha_bd($_POST["femis"]));
			$obser=mb_strtoupper($_POST["obser"]);
			//ALMACEN
			$almac=mb_strtoupper($_POST["almac"]);
			//PRODUCTOS
			foreach ($_POST["produ"] as $key => $value) {
				$producto[$key]["model"]=$_POST["model"][$key];
				$producto[$key]["produ"]=$_POST["produ"][$key];
				$producto[$key]["seria"]=mb_strtoupper($_POST["seria"][$key]);
				$producto[$key]["cprod"]=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["cprod"][$key]));
				$producto[$key]["pprod"]=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["pprod"][$key]));
				$producto[$key]["mprod"]=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["mprod"][$key]));
			}
			$mtotal=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["mtotal"]));

			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				//registrar inventario_proveedor
				$sql="SELECT * FROM inventario_proveedor  WHERE prove_codig ='".$clien."'";
				$inventario_proveedor=$conexion->createCommand($sql)->queryRow();
				if($inventario_proveedor){
					$sql="SELECT * FROM inventario_proveedor WHERE tdocu_codig = '".$tdocu."' and prove_ndocu = '".$ndocu."'";
					$documento=$conexion->createCommand($sql)->queryRow();

					if(!$documento or ($tdocu==$inventario_proveedor['tdocu_codig'] and $ndocu==$inventario_proveedor['prove_ndocu'])){
								$sql="UPDATE inventario_proveedor 
									  SET tclie_codig='".$tclie."',
										  tdocu_codig='".$tdocu."',
										  prove_ndocu='".$ndocu."',
										  prove_denom='".$denom."',
										  prove_ccont='".$ccont."',
										  prove_corre='".$corre."',
										  prove_telef='".$telef."',
										  prove_direc='".$direc."',
										  prove_obser='".$obser."'
									  WHERE prove_codig ='".$clien."'";
							$res1=$conexion->createCommand($sql)->execute();
							/*if($res1){
									$msg=array('success'=>'true','msg'=>'inventario_proveedor actualizado correctamente');
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al actualizar el inventario_proveedor');	
							}*/

								$prove=$clien;
							$msg=array('success'=>'true','msg'=>'inventario_proveedor actualizado correctamente');
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Ya existe un inventario_proveedor con ese numero de documento');
					}
				}else{
					$sql="SELECT * FROM inventario_proveedor WHERE tdocu_codig = '".$tdocu."' and prove_ndocu = '".$ndocu."'";

					$documento=$conexion->createCommand($sql)->queryRow();

					if(!$documento){
						/*$sql="SELECT * FROM inventario_proveedor WHERE prove_ccont = '".$ccont."'";
						$contrato=$conexion->createCommand($sql)->queryRow();
						if(!$contrato){*/
							$sql="INSERT INTO inventario_proveedor(tclie_codig, tdocu_codig, prove_ndocu, prove_denom, prove_ccont, prove_corre, prove_telef, prove_direc, prove_obser, usuar_codig, prove_fcrea, prove_hcrea) 
								VALUES ('".$tclie."','".$tdocu."','".$ndocu."','".$denom."','".$ccont."','".$corre."','".$telef."','".$direc."','".$obser."','".Yii::app()->user->id['usuario']['codigo']."','".date('Y-m-d')."','".date('H:i:s')."')";
							$res1=$conexion->createCommand($sql)->execute();
							if($res1){
								$sql="SELECT * FROM inventario_proveedor WHERE tdocu_codig = '".$tdocu."' and prove_ndocu = '".$ndocu."'";
								$inventario_proveedor=$conexion->createCommand($sql)->queryRow();
								$prove=$inventario_proveedor['prove_codig'];
								$msg=array('success'=>'true','msg'=>'inventario_proveedor guardado correctamente');	
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al guardar el inventario_proveedor');	
							}	
						/*}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Ya existe un inventario_proveedor con ese Número de Contrato');
						}*/
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Ya existe un inventario_proveedor con ese número de documento');
					}
				}
				if ($msg['success']=='true') {
					$sql="SELECT * FROM inventario_entrada WHERE prove_codig='".$prove."' and ientr_nfact = '".$nfact."'";
					$entrada=$conexion->createCommand($sql)->queryRow();
					//if(!$entrada){

						$sql="INSERT INTO inventario_entrada(prove_codig, ientr_nfact, ientr_ncont, ientr_femis, ientr_monto, ieest_codig, ientr_obser, almac_codig, usuar_codig, ientr_fcrea, ientr_hcrea)
							VALUES ('".$prove."','".$nfact."','".$ncont."','".$femis."','".$mtotal."','1','".$obser."','".$almac."','".Yii::app()->user->id['usuario']['codigo']."','".date('Y-m-d')."','".date('H:i:s')."')";
						
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							
							$msg=array('success'=>'true','msg'=>'Entrada guardada correctamente');
							$sql="SELECT * from inventario_entrada WHERE ientr_codig=(SELECT max(ientr_codig) FROM inventario_entrada)";
							$entrada=$conexion->createCommand($sql)->queryRow();

						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al guardar la Entrada');
						}	
					/*}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Ya existe una Entrada con ese número');
					}*/
					if ($msg['success']=='true') {
						if($entrada){
							foreach ($producto as $key => $value) {

								$sql="INSERT INTO inventario_entrada_producto(ientr_codig, model_codig, inven_codig, inven_seria, iepro_canti, iepro_preci, iepro_monto, iepro_obser, usuar_codig, iepro_fcrea, iepro_hcrea) 
								VALUES ('".$entrada['ientr_codig']."', 
										'".$value['model']."', 
										'".$value['produ']."', 
										'".$value['seria']."', 
										'".$value['cprod']."', 
										'".$value['pprod']."', 
										'".$value['mprod']."', 
										'".$value['obser']."', 
										'".Yii::app()->user->id['usuario']['codigo']."',
										'".date('Y-m-d')."',
										'".date('H:i:s')."')";
								$res1=$conexion->createCommand($sql)->execute();
								if($res1){
									$msg=array('success'=>'true','msg'=>'Producto guardado correctamente');	
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al guardar el producto');
									echo json_encode($msg);;
									exit();
								}	
							}
							$transaction->commit();
							
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'La factura no existe');
						}
					}
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionRegistrarLote()
	{
		if($_POST){
			$archivos=$_FILES['image'];
			//PROVEEDOR
			$clien=mb_strtoupper($_POST["clien"]);
			$denom=mb_strtoupper($_POST["denom"]);
			$tclie=mb_strtoupper($_POST["tclie"]);
			$tdocu=mb_strtoupper($_POST["tdocu"]);
			$ndocu=mb_strtoupper($_POST["ndocu"]);
			$ccont=mb_strtoupper($_POST["ccont"]);
			$corre=mb_strtoupper($_POST["corre"]);
			$telef=mb_strtoupper($_POST["telef"]);
			$direc=mb_strtoupper($_POST["direc"]);
			$obser=mb_strtoupper($_POST["obser"]);
			//FACTURA
			$nfact=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["nfact"]));
			$ncont=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["ncont"]));
			$femis=mb_strtoupper($this->funciones->TransformarFecha_bd($_POST["femis"]));
			$obser=mb_strtoupper($_POST["obser"]);
			//ALMACEN
			$almac=mb_strtoupper($_POST["almac"]);
			//PRODUCTOS
			foreach ($_POST["produ"] as $key => $value) {
				$producto[$key]["model"]=$_POST["model"][$key];
				$producto[$key]["produ"]=$_POST["produ"][$key];
				$producto[$key]["seria"]=mb_strtoupper($_POST["seria"][$key]);
				$producto[$key]["cprod"]=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["cprod"][$key]));
				$producto[$key]["pprod"]=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["pprod"][$key]));
				$producto[$key]["mprod"]=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["mprod"][$key]));
			}
			$i=0;
			$j=0;
			foreach ($archivos["tmp_name"] as $key => $archivo) {
				$i++;
				$name=explode('.', $archivos['name'][$key]);
				$name=end($name);
				$name=mb_strtolower($name);

				if($name!='csv'){
					$msg=array('success'=>'false','msg'=>'El archivo número '.$i.' no es formato CSV');
					echo json_encode($msg);
					exit();
				}
				if (($fichero = fopen($archivos['tmp_name'][$key], "r")) !== FALSE) {

					
				    while (($datos = fgetcsv($fichero, 1000)) !== FALSE) {
				        

				        $insert[$j]["model"] = $producto[$key]["model"];
						$insert[$j]["produ"] = $producto[$key]["produ"];
						$insert[$j]["seria"] = $datos[0];
						$insert[$j]["cprod"] = $producto[$key]["cprod"];
						$insert[$j]["pprod"] = $producto[$key]["pprod"];
						$insert[$j]["mprod"] = $producto[$key]["mprod"];
						$j++;
				    }
				}
				
			}
			$mtotal=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["mtotal"]));

			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				//registrar inventario_proveedor
				$sql="SELECT * FROM inventario_proveedor  WHERE prove_codig ='".$clien."'";
				$inventario_proveedor=$conexion->createCommand($sql)->queryRow();
				if($inventario_proveedor){
					$sql="SELECT * FROM inventario_proveedor WHERE tdocu_codig = '".$tdocu."' and prove_ndocu = '".$ndocu."'";
					$documento=$conexion->createCommand($sql)->queryRow();

					if(!$documento or ($tdocu==$inventario_proveedor['tdocu_codig'] and $ndocu==$inventario_proveedor['prove_ndocu'])){
								$sql="UPDATE inventario_proveedor 
									  SET tclie_codig='".$tclie."',
										  tdocu_codig='".$tdocu."',
										  prove_ndocu='".$ndocu."',
										  prove_denom='".$denom."',
										  prove_ccont='".$ccont."',
										  prove_corre='".$corre."',
										  prove_telef='".$telef."',
										  prove_direc='".$direc."',
										  prove_obser='".$obser."'
									  WHERE prove_codig ='".$clien."'";
							$res1=$conexion->createCommand($sql)->execute();
							/*if($res1){
									$msg=array('success'=>'true','msg'=>'inventario_proveedor actualizado correctamente');
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al actualizar el inventario_proveedor');	
							}*/

								$prove=$clien;
							$msg=array('success'=>'true','msg'=>'inventario_proveedor actualizado correctamente');
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Ya existe un inventario_proveedor con ese numero de documento');
					}
				}else{
					$sql="SELECT * FROM inventario_proveedor WHERE tdocu_codig = '".$tdocu."' and prove_ndocu = '".$ndocu."'";

					$documento=$conexion->createCommand($sql)->queryRow();

					if(!$documento){
						/*$sql="SELECT * FROM inventario_proveedor WHERE prove_ccont = '".$ccont."'";
						$contrato=$conexion->createCommand($sql)->queryRow();
						if(!$contrato){*/
							$sql="INSERT INTO inventario_proveedor(tclie_codig, tdocu_codig, prove_ndocu, prove_denom, prove_ccont, prove_corre, prove_telef, prove_direc, prove_obser, usuar_codig, prove_fcrea, prove_hcrea) 
								VALUES ('".$tclie."','".$tdocu."','".$ndocu."','".$denom."','".$ccont."','".$corre."','".$telef."','".$direc."','".$obser."','".Yii::app()->user->id['usuario']['codigo']."','".date('Y-m-d')."','".date('H:i:s')."')";
							$res1=$conexion->createCommand($sql)->execute();
							if($res1){
								$sql="SELECT * FROM inventario_proveedor WHERE tdocu_codig = '".$tdocu."' and prove_ndocu = '".$ndocu."'";
								$inventario_proveedor=$conexion->createCommand($sql)->queryRow();
								$prove=$inventario_proveedor['prove_codig'];
								$msg=array('success'=>'true','msg'=>'inventario_proveedor guardado correctamente');	
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al guardar el inventario_proveedor');	
							}	
						/*}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Ya existe un inventario_proveedor con ese Número de Contrato');
						}*/
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Ya existe un inventario_proveedor con ese número de documento');
					}
				}
				if ($msg['success']=='true') {
					$sql="SELECT * FROM inventario_entrada WHERE prove_codig='".$prove."' and ientr_nfact = '".$nfact."'";
					$entrada=$conexion->createCommand($sql)->queryRow();
					//if(!$entrada){

						$sql="INSERT INTO inventario_entrada(prove_codig, ientr_nfact, ientr_ncont, ientr_femis, ientr_monto, ieest_codig, ientr_obser, almac_codig, usuar_codig, ientr_fcrea, ientr_hcrea)
							VALUES ('".$prove."','".$nfact."','".$ncont."','".$femis."','".$mtotal."','1','".$obser."','".$almac."','".Yii::app()->user->id['usuario']['codigo']."','".date('Y-m-d')."','".date('H:i:s')."')";
						
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							
							$msg=array('success'=>'true','msg'=>'Entrada guardada correctamente');
							$sql="SELECT * from inventario_entrada WHERE ientr_codig=(SELECT max(ientr_codig) FROM inventario_entrada)";
							$entrada=$conexion->createCommand($sql)->queryRow();

						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al guardar la Entrada');
						}	
					/*}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Ya existe una Entrada con ese número');
					}*/
					if ($msg['success']=='true') {
						if($entrada){
							foreach ($insert as $key => $value) {

								$sql="INSERT INTO inventario_entrada_producto(ientr_codig, model_codig, inven_codig, inven_seria, iepro_canti, iepro_preci, iepro_monto, iepro_obser, usuar_codig, iepro_fcrea, iepro_hcrea) 
								VALUES ('".$entrada['ientr_codig']."', 
										'".$value['model']."', 
										'".$value['produ']."', 
										'".$value['seria']."', 
										'".$value['cprod']."', 
										'".$value['pprod']."', 
										'".$value['mprod']."', 
										'".$value['obser']."', 
										'".Yii::app()->user->id['usuario']['codigo']."',
										'".date('Y-m-d')."',
										'".date('H:i:s')."')";
								$res1=$conexion->createCommand($sql)->execute();
								if($res1){
									$msg=array('success'=>'true','msg'=>'Entrada guardada correctamente');	
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al guardar el producto');
									echo json_encode($msg);;
									exit();
								}	
							}
							$transaction->commit();
							
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'La factura no existe');
						}
					}
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrarLote');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codig=mb_strtoupper($_POST['codig']);
			$nfact=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["nfact"]));
			$ncont=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["ncont"]));
			$femis=mb_strtoupper($this->funciones->TransformarFecha_bd($_POST["femis"]));
			$obser=mb_strtoupper($_POST["obser"]);
			if($contra==$ccontra){
				$conexion=Yii::app()->db;
				$transaction=$conexion->beginTransaction();
				try{
					$sql="SELECT * FROM inventario_entrada WHERE ientr_codig ='".$codig."'";
					$entrada=$conexion->createCommand($sql)->queryRow();
					if($entrada){
						$sql="SELECT * FROM inventario_entrada WHERE ientr_nfact = '".$nfact."'";
						$documento=$conexion->createCommand($sql)->queryRow();
						if(!$documento or $nfact==$entrada['ientr_nfact']){
							$sql="UPDATE inventario_entrada
								  SET ientr_nfact = '".$nfact."',
								  	  ientr_ncont = '".$ncont."',
									  ientr_femis = '".$femis."',
									  ientr_obser = '".$obser."'
								  WHERE ientr_codig ='".$codig."'";
							$res1=$conexion->createCommand($sql)->execute();
							if($res1){
								$transaction->commit();
								$msg=array('success'=>'true','msg'=>'Entrada actualizada correctamente');
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al actualizar la Entrada');	
							}
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Ya existe un Proveedor con ese numero de documento');
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'La Entrada no existe');
					}
				}catch(Exception $e){
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error al verificar la información');
				}
			}else{
				$msg=array('success'=>'false','msg'=>'La contraseña y su confirmación no son iguales');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM inventario_entrada  a
			  WHERE a.ientr_codig ='".$_GET['c']."'";
			$entrada=$conexion->createCommand($sql)->queryRow();
			$sql="SELECT * 
				  FROM inventario_proveedor  a
				  WHERE a.prove_codig ='".$entrada['prove_codig']."'";
			$inventario_proveedor=$conexion->createCommand($sql)->queryRow();
			$sql="SELECT a.* , b.inven_descr, c.model_descr
				  FROM inventario_entrada_producto  a
				  JOIN inventario b ON (a.inven_codig = b.inven_codig)
				  JOIN inventario_modelo c ON (a.model_codig = c.model_codig)
				  WHERE a.ientr_codig ='".$entrada['ientr_codig']."'";
			$productos=$conexion->createCommand($sql)->queryAll();
			$this->render('modificar', array('entrada' => $entrada,'inventario_proveedor' => $inventario_proveedor,'productos' => $productos));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM inventario_entrada_producto WHERE ientr_codig='".$codig."'";
				$prod=$conexion->createCommand($sql)->queryRow();
				if(!$prod){
					$sql="SELECT * FROM inventario_entrada WHERE ientr_codig='".$codig."'";
					$roles=$conexion->createCommand($sql)->queryRow();
					if($roles){

						$sql="DELETE FROM inventario_entrada  WHERE ientr_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Entrada eliminada correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar la Entrada');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'La Entrada no existe');
					}

				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No se puede eliminar debido a, que tiene productos asociados');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM inventario_entrada  a
			  WHERE a.ientr_codig ='".$_GET['c']."'";
			$entrada=$conexion->createCommand($sql)->queryRow();
			$sql="SELECT * 
				  FROM inventario_proveedor  a
				  WHERE a.prove_codig ='".$entrada['prove_codig']."'";
			$inventario_proveedor=$conexion->createCommand($sql)->queryRow();
			$sql="SELECT a.* , b.inven_descr, c.model_descr
				  FROM inventario_entrada_producto  a
				  JOIN inventario b ON (a.inven_codig = b.inven_codig)
				  JOIN inventario_modelo c ON (a.model_codig = c.model_codig)
				  WHERE a.ientr_codig ='".$entrada['ientr_codig']."'";
			$productos=$conexion->createCommand($sql)->queryAll();
			$this->render('eliminar', array('entrada' => $entrada,'inventario_proveedor' => $inventario_proveedor,'productos' => $productos));
		}
	}
	public function actionAprobarEntradas()
	{
		if($_POST){
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$id=$_POST['id'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				foreach ($id as $key => $codig) {
					$sql="SELECT * FROM inventario_entrada WHERE ientr_codig='".$codig."'";
					$entrada=$conexion->createCommand($sql)->queryRow();
					if($entrada){
						$sql="UPDATE inventario_entrada
						  	  SET ieest_codig = '2'
							  WHERE ientr_codig ='".$codig."'";
				
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$msg=array('success'=>'true','msg'=>'Entrada Actualizada correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar la Entrada');
							echo json_encode($msg);
							exit();
						}	

					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'La Entrada no existe');
						echo json_encode($msg);
						exit();
					}

					if($msg['success']="true"){
						$sql="SELECT * FROM inventario_entrada_producto WHERE ientr_codig='".$codig."'";
						$productos=$conexion->createCommand($sql)->queryAll();
						foreach ($productos as $key => $producto) {

							$sql="SELECT * FROM inventario_seriales WHERE seria_numer ='".$producto['inven_seria']."'";

							$res1=$conexion->createCommand($sql)->queryRow();
							
							if(!$res1){
								$preci=$this->funciones->MontoCambioReverso($producto['inven_codig'],$producto['iepro_preci'],'1');

								$sql="UPDATE inventario
							  	  SET inven_canti = inven_canti+'".$producto['iepro_canti']."',
							  	  	  inven_punid = '".$preci."'
								  WHERE inven_codig ='".$producto['inven_codig']."'";
								$res1=$conexion->createCommand($sql)->execute();
								$sql="INSERT INTO inventario_seriales(model_codig, inven_codig, almac_codig, seria_numer, ientr_codig, usuar_codig, seria_fcrea, seria_hcrea) 
								VALUES ('".$producto['model_codig']."','".$producto['inven_codig']."','".$entrada['almac_codig']."','".$producto['inven_seria']."','".$producto['ientr_codig']."','".$usuar."','".$fecha."','".$hora."')";
								$res2=$conexion->createCommand($sql)->execute();
								if($res1){
									$msg=array('success'=>'true','msg'=>'Entrada Actualizada correctamente');	
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al actualizar el Producto');
									echo json_encode($msg);
									exit();
								}	
							}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'El serial "'.$producto['inven_seria'].'", Ya se encuentra registrado');
									echo json_encode($msg);
									exit();
								}	
						}
						
					}
					
				}
				if($msg['success']="true"){
					$transaction->commit();
				}
				
				
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información','e'=>$e);
			}
			echo json_encode($msg);
		}else{
			$this->render('aprobar');

		}
	}
	public function actionReversarAprobarEntradas()
	{
		if($_POST){
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$id=$_POST['id'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				foreach ($id as $key => $codig) {
					$sql="SELECT * FROM inventario_entrada WHERE ientr_codig='".$codig."'";
					$entrada=$conexion->createCommand($sql)->queryRow();
					if($entrada){
						$sql="UPDATE inventario_entrada
						  	  SET ieest_codig = '1'
							  WHERE ientr_codig ='".$codig."'";
				
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$msg=array('success'=>'true','msg'=>'Entrada Actualizada correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar la Entrada');
							echo json_encode($msg);;
							exit();
						}	

					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'La Entrada no existe');
						echo json_encode($msg);
						exit();
					}

					if($msg['success']="true"){
						$sql="SELECT * FROM inventario_entrada_producto WHERE ientr_codig='".$codig."'";
						$productos=$conexion->createCommand($sql)->queryAll();
						foreach ($productos as $key => $producto) {
							


							$sql="UPDATE inventario
						  	  SET inven_canti = inven_canti-'".$producto['iepro_canti']."'
							  WHERE inven_codig ='".$producto['inven_codig']."'";
							$res1=$conexion->createCommand($sql)->execute();

							
							if($res1){
								$msg=array('success'=>'true','msg'=>'Entrada Actualizada correctamente');	
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al actualizar el Producto');
								echo json_encode($msg);
								exit();
							}

							$sql="DELETE FROM inventario_seriales
								WHERE seria_numer = '".$producto['inven_seria']."'
								AND ientr_codig ='".$producto['ientr_codig']."'";
							$res2=$conexion->createCommand($sql)->execute();

							if($res2){
								$msg=array('success'=>'true','msg'=>'Entrada Actualizada correctamente');	
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al eliminar el Serial');
								echo json_encode($msg);
								exit();
							}	
						}
						
					}
					
				}
				if($msg['success']="true"){
					$transaction->commit();
				}
				
				
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$this->render('aprobarreversar');

		}
	}
	public function actionAnularEntradas()
	{
		if($_POST){
			
			$id=$_POST['id'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				foreach ($id as $key => $codig) {
					$sql="SELECT * FROM inventario_entrada WHERE ientr_codig='".$codig."'";
					$entrada=$conexion->createCommand($sql)->queryRow();
					if($entrada){
						$sql="UPDATE inventario_entrada
						  	  SET ieest_codig = '3'
							  WHERE ientr_codig ='".$codig."'";
				
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$msg=array('success'=>'true','msg'=>'Entrada Actualizada correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar la Entrada');
							echo json_encode($msg);;
							exit();
						}	

					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'La Entrada no existe');
						echo json_encode($msg);;
						exit();
					}
					
				}
				if($msg['success']="true"){
					$transaction->commit();
				}
				
				
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$this->render('anular');

		}
	}
	public function actionReversarAnularEntradas()
	{
		if($_POST){
			
			$id=$_POST['id'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				foreach ($id as $key => $codig) {
					$sql="SELECT * FROM inventario_entrada WHERE ientr_codig='".$codig."'";
					$entrada=$conexion->createCommand($sql)->queryRow();
					if($entrada){
						$sql="UPDATE inventario_entrada
						  	  SET ieest_codig = '1'
							  WHERE ientr_codig ='".$codig."'";
				
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$msg=array('success'=>'true','msg'=>'Entrada Actualizada correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar la Entrada');
							echo json_encode($msg);;
							exit();
						}	

					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'La Entrada no existe');
						echo json_encode($msg);;
						exit();
					}
					
				}
				if($msg['success']="true"){
					$transaction->commit();
				}
				
				
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$this->render('anularreversar');

		}
	}
	public function TransformarMonto_bd($monto)
	{
		//DEBE DE ESTAR 0.000,00
		$monto=str_replace('.', '', $monto);
		$monto=str_replace(',', '.', $monto);
		//RETORNA 0000.00
		return $monto;
	}
	public function TransformarMonto_v($monto,$cantidad)
	{
		//DEBE DE ESTAR 0000.00
		$monto=number_format($monto,$cantidad,',','.');
		//RETORNA 0.000,00
		return $monto;
	}
	public function TransformarFecha_bd($fecha)
	{
		//DEBE DE ESTAR DD/MM/AAAA
		$fecha=explode('/',$fecha);
		$fecha=$fecha[2].'-'.$fecha[1].'-'.$fecha[0];
		//RETORNA AAAA-MM-DD
		return $fecha;
	}
	public function TransformarFecha_v($fecha)
	{
		//DEBE DE ESTAR AAAA-MM-DD
		$fecha=explode('-',$fecha);
		$fecha=$fecha[2].'/'.$fecha[1].'/'.$fecha[0];
		//RETORNA DD/MM/AAAA
		return $fecha;
	}
	public function actionEjemplo()
	{
		$this->render('ejemplo');
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}