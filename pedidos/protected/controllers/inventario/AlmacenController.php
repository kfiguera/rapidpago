<?php

class AlmacenController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->redirect(Yii::app()->request->baseUrl.'/Articulos/listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descripcion']){
			$descripcion=mb_strtoupper($_POST['descripcion']);
			$condicion.="a.almac_descr like '%".$descripcion."%' ";
			$con++;
		}
		if($_POST['codigo']){
			if($con>0){
				$condicion.="AND ";
			}
			$codigo=mb_strtoupper($_POST['codigo']);
			$condicion.="a.almac_cprod like '%".$codigo."%' ";
			$con++;
		}
		if($_POST['cantidad']){
			if($con>0){
				$condicion.="AND ";
			}
			$cantidad = $this->funciones->TransformarMonto_bd($_POST['cantidad']);
			$condicion.="a.almac_canti like '%".$cantidad."%' ";
			$con++;
		}
		if($_POST['punidad']){
			if($con>0){
				$condicion.="AND ";
			}
			$punidad = $this->funciones->TransformarMonto_bd($_POST['punidad']);
			$condicion.="a.almac_punid like '%".$punidad."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM inventario_almacen  a
			  WHERE a.almac_codig ='".$_GET['c']."'";
		$roles=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('roles' => $roles));
	}
	public function actionRegistrar()
	{
		if($_POST){
			/*var_dump($_POST);
			exit();*/
			$descr=mb_strtoupper($_POST['descr']);
			$numer=mb_strtoupper($_POST['numer']);
			$ubica=mb_strtoupper($_POST['ubica']);
			$respo=mb_strtoupper($_POST['respo']);
			$obser=mb_strtoupper($_POST['obser']);
			//Datos de Auditoria
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM inventario_almacen WHERE almac_descr = '".$descr."'";
				$inven=$conexion->createCommand($sql)->queryRow();
				if(!$inven){
					$sql="SELECT * FROM inventario_almacen WHERE almac_numer = '".$codig."'";
					$codigo=$conexion->createCommand($sql)->queryRow();
					if(!$codigo){
						$sql="INSERT INTO inventario_almacen(almac_numer,almac_descr,almac_ubica,almac_respo,almac_obser,usuar_codig,almac_fcrea,almac_hcrea)
							  VALUES('".$numer."','".$descr."','".$ubica."','".$respo."','".$obser."','".Yii::app()->user->id['usuario']['codigo']."','".date('Y-m-d')."','".date('H:i:s')."')";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Almacen guardado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al guardar el Almacen');	
						}	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El código de Almacen ya existe ya existe');
					}	
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Almacen ya existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codig=mb_strtoupper($_POST['codig']);
			$descr=mb_strtoupper($_POST['descr']);
			$numer=mb_strtoupper($_POST['numer']);
			$ubica=mb_strtoupper($_POST['ubica']);
			$respo=mb_strtoupper($_POST['respo']);
			$obser=mb_strtoupper($_POST['obser']);
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM inventario_almacen  WHERE almac_codig ='".$codig."'";
				$roles=$conexion->createCommand($sql)->queryRow();
				if($roles){
					$sql="SELECT * FROM inventario_almacen WHERE almac_numer = '".$numer."'";
					$codigo=$conexion->createCommand($sql)->queryRow();
					if(!$codigo or $roles['almac_numer']==$numer){
							$sql="UPDATE inventario_almacen 
							  		SET almac_numer='".$numer."',
										almac_descr='".$descr."',
										almac_ubica='".$ubica."',
										almac_respo='".$respo."',
										almac_obser='".$obser."'
									WHERE almac_codig ='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
								$msg=array('success'=>'true','msg'=>'Almacen actualizado correctamente');
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar el Almacen');	
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Código de producto ya existe');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Almacen no existe');
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			
			$sql="SELECT * 
			  FROM inventario_almacen  a
			  WHERE a.almac_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('roles' => $roles));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codigo'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM factura_producto  WHERE almac_codig='".$codig."'";
				$prod=$conexion->createCommand($sql)->queryRow();
				if(!$prod){
					$sql="SELECT * FROM inventario_almacen  WHERE almac_codig='".$codig."'";
					$roles=$conexion->createCommand($sql)->queryRow();
					if($roles){

						$sql="DELETE FROM inventario_almacen  WHERE almac_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Almacen eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Almacen ');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Almacen no existe');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No se puede eliinar debido a que, esta asociada a una factura');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM inventario_almacen  a
			  WHERE a.almac_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('roles' => $roles));
		}
	}
	public function actionSolicitudes()
	{
		if($_POST){
			exit();
			$codig=$_POST['codigo'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM factura_producto  WHERE almac_codig='".$codig."'";
				$prod=$conexion->createCommand($sql)->queryRow();
				if(!$prod){
					$sql="SELECT * FROM inventario_almacen  WHERE almac_codig='".$codig."'";
					$roles=$conexion->createCommand($sql)->queryRow();
					if($roles){

						$sql="DELETE FROM inventario_almacen  WHERE almac_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Almacen eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Almacen ');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Almacen no existe');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No se puede eliinar debido a que, esta asociada a una factura');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM inventario_almacen  a
			  WHERE a.almac_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('solicitudes', array('roles' => $roles));
		}
	}
	public function TransformarMonto_bd($monto)
	{
		//DEBE DE ESTAR 0.000,00
		$monto=str_replace('.', '', $monto);
		$monto=str_replace(',', '.', $monto);
		//RETORNA 0000.00
		return $monto;
	}
	public function TransformarMonto_v($monto,$cantidad)
	{
		//DEBE DE ESTAR 0000.00
		$monto=number_format($monto,$cantidad,',','.');
		//RETORNA 0.000,00
		return $monto;
	}
	public function TransformarFecha_bd($fecha)
	{
		//DEBE DE ESTAR DD/MM/AAAA
		$fecha=explode('/',$fecha);
		$fecha=$fecha[2].'-'.$fecha[1].'-'.$fecha[0];
		//RETORNA AAAA-MM-DD
		return $fecha;
	}
	public function TransformarFecha_v($fecha)
	{
		//DEBE DE ESTAR AAAA-MM-DD
		$fecha=explode('-',$fecha);
		$fecha=$fecha[2].'/'.$fecha[1].'/'.$fecha[0];
		//RETORNA DD/MM/AAAA
		return $fecha;
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}