<?php

class ModeloController extends Controller
{
	public $funciones;

	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->render('index');
	}
	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descripcion']){
			$descripcion=mb_strtoupper($_POST['descripcion']);
			$condicion.="a.model_descr like '%".$descripcion."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM pedido_modelo a
			  WHERE a.model_codig ='".$_GET['c']."'";
		$modelo=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('modelo' => $modelo));
	}
	public function actionRegistrar()
	{
		if($_POST){
			
			$descr=mb_strtoupper($_POST['descr']);
			$gorro=mb_strtoupper($_POST['gorro']);
			$preti=mb_strtoupper($_POST['preti']);
			$mensa=mb_strtoupper($_POST['mensa']);
			$obser=mb_strtoupper($_POST['obser']);
			
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_modelo WHERE model_descr = '".$descr."'";
				$modelo=$conexion->createCommand($sql)->queryRow();
				if(!$modelo){
					$sql="INSERT INTO pedido_modelo(model_descr, model_gorro, model_preti, model_mensa, model_obser, usuar_codig, model_fcrea, model_hcrea) VALUES ('".$descr."', '".$gorro."', '".$preti."', '".$mensa."', '".$obser."', '".$usuar."', '".$fecha."', '".$hora."')";
					$res1=$conexion->createCommand($sql)->execute();
					if($res1){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Modelo guardado correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al guardar El Modelo');	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Modelo ya existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codig=mb_strtoupper($_POST['codig']);
			$descr=mb_strtoupper($_POST['descr']);
			$gorro=mb_strtoupper($_POST['gorro']);
			$preti=mb_strtoupper($_POST['preti']);
			$mensa=mb_strtoupper($_POST['mensa']);
			$obser=mb_strtoupper($_POST['obser']);
			
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_modelo WHERE model_codig ='".$codig."'";
				$modelo=$conexion->createCommand($sql)->queryRow();
				if($modelo){

					$sql="SELECT * FROM pedido_modelo WHERE model_descr = '".$descr."' and model_codig = '".$codig."'";
					$modelo2=$conexion->createCommand($sql)->queryRow();
					
					if(!$modelo2 or ($modelo['model_codig']==$codig and $modelo['model_descr']==$descr)){
							$sql="UPDATE pedido_modelo
								  SET model_descr='".$descr."',
								  	  model_gorro='".$gorro."',
								  	  model_preti='".$preti."',
								  	  model_mensa='".$mensa."',
								      model_obser='".$obser."'
								  WHERE model_codig='".$codig."'";
							$res1=$conexion->createCommand($sql)->execute();
							if($res1){
								$transaction->commit();
									$msg=array('success'=>'true','msg'=>'Modelo actualizado correctamente');
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al actualizar El Modelo');	
							}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Modelo ya esta registrado');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Modelo no existe');
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			
			$sql="SELECT * 
			  FROM pedido_modelo a
			  WHERE a.model_codig ='".$_GET['c']."'";
			$modelo=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('modelo' => $modelo));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_modelo_conjunto WHERE model_codig='".$codig."'";
				$conjunto=$conexion->createCommand($sql)->queryRow();
				if(!$conjunto){
					$sql="SELECT * FROM pedido_modelo WHERE model_codig='".$codig."'";
					$modelo=$conexion->createCommand($sql)->queryRow();
					if($modelo){

						$sql="DELETE FROM pedido_modelo WHERE model_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Modelo eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Tipo de Usuario');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Modelo no existe');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No se puede eliminar el Modelo, debido a que esta asociado a un Conjunto');
				}	
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_modelo a
			  WHERE a.model_codig ='".$_GET['c']."'";
			$modelo=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('modelo' => $modelo));
		}
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}