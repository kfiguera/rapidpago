<?php
class ConjuntoController extends Controller
{
	public $funciones;

	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->render('index');
	}
	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descripcion']){
			$descripcion=mb_strtoupper($_POST['descripcion']);
			$condicion.="a.mconj_descr like '%".$descripcion."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM pedido_modelo_conjunto a
			  WHERE a.mconj_codig ='".$_GET['c']."'";
		$conjunto=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('conjunto' => $conjunto));
	}
	public function actionRegistrar()
	{
		if($_POST){
			//DATOS DEL FORMULARIO
			$tmode=mb_strtoupper($_POST["tmode"]);
			$model=mb_strtoupper($_POST["model"]);
			$categ=mb_strtoupper($_POST["categ"]);
			$varia=mb_strtoupper($_POST["varia"]);
			$opcio=json_encode($_POST["opcio"]);
			$opci2=json_encode($_POST["opci2"]);
			$preci=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['preci']));
			$obser=mb_strtoupper($_POST["obser"]);	
			$inclu=json_encode($_POST["inclu"]);

			//DATOS DE LA IMAGEN
			$file=$_FILES['image'];
			$ruta='files/conjunto/';
			$name=explode(".", $file['name']);
			$nombre=date('Ymd_His.').end($name);
			$destino=$ruta.$nombre;
			$descr=mb_strtoupper($_POST['descr']);

			//DATOS DE AUDITORIA						
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$verificar=$this->funciones->CopiarEmoji($file,$ruta,$nombre);
				if($verificar['success']=='true'){
					$sql="SELECT * FROM pedido_modelo_conjunto 
						  WHERE tmode_codig='".$tmode."' 
							AND model_codig='".$model."'
							AND mcate_codig='".$categ."'
							AND mvari_codig='".$varia."'
							AND mopci_codig='".$opcio."'
							AND mopci_codi2='".$opci2."'";
					$conjunto=$conexion->createCommand($sql)->queryRow();
					if(!$conjunto){
						$sql="INSERT INTO pedido_modelo_conjunto(tmode_codig, model_codig, mcate_codig, mvari_codig, mopci_codig, mopci_codi2, mconj_preci, mconj_inclu, mconj_ruta, mconj_obser, usuar_codig, mconj_fcrea, mconj_hcrea) 
							VALUES ('".$tmode."','".$model."','".$categ."','".$varia."','".$opcio."','".$opci2."','".$preci."','".$inclu."','".$destino."','".$obser."','".$usuar."','".$fecha."','".$hora."');";
						
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Conjunto guardado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al guardar El Conjunto');	
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Conjunto ya existe');
					}
				}else{
					$transaction->rollBack();
					$msg=$verificar;	
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codig=mb_strtoupper($_POST['codig']);
			$tmode=mb_strtoupper($_POST["tmode"]);
			$model=mb_strtoupper($_POST["model"]);
			$categ=mb_strtoupper($_POST["categ"]);
			$varia=mb_strtoupper($_POST["varia"]);
			$opcio=json_encode($_POST["opcio"]);
			$opci2=json_encode($_POST["opci2"]);
			$preci=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['preci']));
			$obser=mb_strtoupper($_POST["obser"]);
			$inclu=json_encode($_POST["inclu"]);
			//DATOS DE LA IMAGEN
			$file=$_FILES['image'];
			$ruta='files/conjunto/';
			$name=explode(".", $file['name']);
			$nombre=date('Ymd_His.').end($name);
			$destino=$ruta.$nombre;
			$descr=mb_strtoupper($_POST['descr']);

			$image=false;
			$verificar['success']='true';
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				if(!empty($file['name'])){
					$verificar=$this->funciones->CopiarEmoji($file,$ruta,$nombre);
					$image=true;
				}
				if($verificar['success']=='true'){

					$sql="SELECT * FROM pedido_modelo_conjunto WHERE mconj_codig ='".$codig."'";
					$conjunto=$conexion->createCommand($sql)->queryRow();
					if($image==false){
						$destino=$conjunto['mconj_ruta'];
					}
					if($conjunto){

						$sql="SELECT * FROM pedido_modelo_conjunto 
						  WHERE tmode_codig='".$tmode."' 
							AND model_codig='".$model."'
							AND mcate_codig='".$categ."'
							AND mvari_codig='".$varia."'
							AND mopci_codig='".$opcio."'
							AND mopci_codi2='".$opci2."'
							AND mconj_preci='".$preci."'
							AND mconj_inclu='".$inclu."'";
						$conjunto2=$conexion->createCommand($sql)->queryRow();
						
						if(!$conjunto2 or ($conjunto2['mconj_codig']==$codig and $conjunto2['mconj_preci']==$preci and $conjunto2['mconj_inclu']==$inclu and $image==true)){
								$sql="UPDATE pedido_modelo_conjunto
									  SET tmode_codig='".$tmode."',
										  model_codig='".$model."',
										  mcate_codig='".$categ."',
										  mvari_codig='".$varia."',
										  mopci_codig='".$opcio."',
										  mopci_codi2='".$opci2."',
										  mconj_preci='".$preci."',
										  mconj_inclu='".$inclu."',
										  mconj_ruta='".$destino."',
										  mconj_obser='".$obser."'		
									  WHERE mconj_codig='".$codig."'";
								$res1=$conexion->createCommand($sql)->execute();
								if($res1){
									$transaction->commit();
										$msg=array('success'=>'true','msg'=>'Conjunto actualizado correctamente');
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al actualizar El Conjunto');	
								}
							
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'El Conjunto ya esta registrado');
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Conjunto no existe');
					}
				}else{
					$transaction->rollBack();
					$msg=$verificar;	
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			
			$sql="SELECT * 
			  FROM pedido_modelo_conjunto a
			  WHERE a.mconj_codig ='".$_GET['c']."'";
			$conjunto=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('conjunto' => $conjunto));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				
				$sql="SELECT * FROM pedido_modelo_conjunto WHERE mconj_codig='".$codig."'";
				$conjunto=$conexion->createCommand($sql)->queryRow();
				if($conjunto){

					$sql="DELETE FROM pedido_modelo_conjunto WHERE mconj_codig='".$codig."'";
					$res1=$conexion->createCommand($sql)->execute();
					//echo $sql;
					if($res1){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Conjunto eliminado correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al eliminar el Conjunto de Usuario');	
					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Conjunto no existe');
				}
					
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_modelo_conjunto a
			  WHERE a.mconj_codig ='".$_GET['c']."'";
			$conjunto=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('conjunto' => $conjunto));
		}
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}