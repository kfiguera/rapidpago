<?php
class SimboloController extends Controller
{
	public $funciones;

	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->render('index');
	}
	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descripcion']){
			$descripcion=mb_strtoupper($_POST['descripcion']);
			$condicion.="a.ssimp_descr like '%".$descripcion."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM pedido_simbolo_simple a
			  WHERE a.ssimp_codig ='".$_GET['c']."'";
		$simple=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('simple' => $simple));
	}
	public function actionRegistrar()
	{
		if($_POST){
			//DATOS DEL FORMULARIO
			$descr=$_POST['descr'];

			//DATOS DE LA IMAGEN
			$file=$_FILES['image'];
			$ruta='files/simple/';
			$name=explode(".", $file['name']);
			$nombre=date('Ymd_His.').end($name);
			$destino=$ruta.$nombre;
			$descr=mb_strtoupper($_POST['descr']);
			
			//DATOS DE AUDITORIA
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{

				$verificar=$this->funciones->CopiarEmoji($file,$ruta,$nombre);
				
				if($verificar['success']=='true'){

					$sql="SELECT * FROM pedido_simbolo_simple WHERE ssimp_descr = '".$descr."'";
					$simple=$conexion->createCommand($sql)->queryRow();
					if(!$simple){
						$sql="INSERT INTO pedido_simbolo_simple(ssimp_descr, ssimp_ruta, usuar_codig, ssimp_fcrea, ssimp_hcrea) VALUES ('".$descr."', '".$destino."', '".$usuar."', '".$fecha."', '".$hora."')";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Simbolo Simple guardado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al guardar El Simbolo Simple');	
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Simbolo Simple ya existe');
					}
				}else{
					$transaction->rollBack();
					$msg=$verificar;	
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			//DATOS DEL FORMULARIO
			$descr=$_POST['descr'];
			$codig=mb_strtoupper($_POST['codig']);

			//DATOS DE LA IMAGEN
			$file=$_FILES['image'];
			$ruta='files/simple/';
			$name=explode(".", $file['name']);
			$nombre=date('Ymd_His.').end($name);
			$destino=$ruta.$nombre;
			$descr=mb_strtoupper($_POST['descr']);
			
			$image=false;
			$verificar['success']='true';

			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				if(!empty($file['name'])){
					$verificar=$this->funciones->CopiarEmoji($file,$ruta,$nombre);
					$image=true;
				}

				if($verificar['success']=='true'){
					$sql="SELECT * FROM pedido_simbolo_simple WHERE ssimp_codig ='".$codig."'";
					$simple=$conexion->createCommand($sql)->queryRow();
					if($image==false){
						$destino=$simple['ssimp_ruta'];
					}
					if($simple){

						$sql="SELECT * FROM pedido_simbolo_simple WHERE ssimp_descr = '".$descr."'";
						$simple2=$conexion->createCommand($sql)->queryRow();
						
						if(!$simple2 or ($simple['ssimp_codig']==$codig and $simple['ssimp_descr']==$descr and $image==true)){
								$sql="UPDATE pedido_simbolo_simple
									  SET ssimp_descr='".$descr."',
									      ssimp_ruta='".$destino."'		
									  WHERE ssimp_codig='".$codig."'";
								$res1=$conexion->createCommand($sql)->execute();
								if($res1){
									$transaction->commit();
										$msg=array('success'=>'true','msg'=>'Simbolo Simple actualizado correctamente');
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al actualizar El Simbolo Simple');	
								}
							
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'El Simbolo Simple ya esta registrado');
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Simbolo Simple no existe');
					}
				}else{
					$transaction->rollBack();
					$msg=$verificar;	
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			
			$sql="SELECT * 
			  FROM pedido_simbolo_simple a
			  WHERE a.ssimp_codig ='".$_GET['c']."'";
			$simple=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('simple' => $simple));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				/*$sql="SELECT * FROM pedido_modelo_conjunto WHERE ssimp_codig='".$codig."'";
				$conjunto=$conexion->createCommand($sql)->queryRow();
				if(!$conjunto){*/
					$sql="SELECT * FROM pedido_simbolo_simple WHERE ssimp_codig='".$codig."'";
					$simple=$conexion->createCommand($sql)->queryRow();
					if($simple){

						$sql="DELETE FROM pedido_simbolo_simple WHERE ssimp_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Simbolo Simple eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Simbolo Simple de Usuario');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Simbolo Simple no existe');
					}
				/*}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No se puede eliminar el Simbolo Simple, debido a que esta asociado a un Conjunto');
				}*/	
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_simbolo_simple a
			  WHERE a.ssimp_codig ='".$_GET['c']."'";
			$simple=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('simple' => $simple));
		}
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}