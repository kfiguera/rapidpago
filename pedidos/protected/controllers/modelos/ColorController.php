<?php
class ColorController extends Controller
{
	public $funciones;

	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->render('index');
	}
	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descripcion']){
			$descripcion=mb_strtoupper($_POST['descripcion']);
			$condicion.="a.mcolo_descr like '%".$descripcion."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM pedido_modelo_color a
			  WHERE a.mcolo_codig ='".$_GET['c']."'";
		$color=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('color' => $color));
	}
	public function actionRegistrar()
	{
		if($_POST){
			//DATOS DEL FORMULARIO
			$tmode=mb_strtoupper($_POST["tmode"]);
			$numer=mb_strtoupper($_POST["numer"]);
			$descr=mb_strtoupper($_POST["descr"]);

			//DATOS DE LA IMAGEN
			$file=$_FILES['image'];
			$ruta='files/color/';
			$name=explode(".", $file['name']);
			$nombre=date('Ymd_His.').end($name);
			$destino=$ruta.$nombre;
			$descr=mb_strtoupper($_POST['descr']);

			//DATOS DE AUDITORIA			
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$verificar=$this->funciones->CopiarEmoji($file,$ruta,$nombre);
				
				if($verificar['success']=='true'){
					$sql="SELECT * FROM pedido_modelo_color 
						  WHERE tmode_codig='".$tmode."' 
							AND mcolo_numer='".$numer."'
							AND mcolo_descr='".$color."'";
					$color=$conexion->createCommand($sql)->queryRow();
					if(!$color){
						$sql="INSERT INTO pedido_modelo_color(mcolo_numer, mcolo_descr, mcolo_ruta, tmode_codig, usuar_codig, mcolo_fcrea, mcolo_hcrea) 
							VALUES('".$numer."','".$descr."','".$destino."','".$tmode."','".$usuar."','".$fecha."','".$hora."');";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Color guardado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al guardar El Color');	
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Color ya existe');
					}
				}else{
					$transaction->rollBack();
					$msg=$verificar;	
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			//DATOS DEL FORMULARIO
			$codig=mb_strtoupper($_POST['codig']);
			$tmode=mb_strtoupper($_POST["tmode"]);
			$numer=mb_strtoupper($_POST["numer"]);
			$descr=mb_strtoupper($_POST["descr"]);

			//DATOS DE LA IMAGEN
			$file=$_FILES['image'];
			$ruta='files/emoji/';
			$name=explode(".", $file['name']);
			$nombre=date('Ymd_His.').end($name);
			$destino=$ruta.$nombre;
			$descr=mb_strtoupper($_POST['descr']);

			$image=false;
			$verificar['success']='true';
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				if(!empty($file['name'])){
					$verificar=$this->funciones->CopiarEmoji($file,$ruta,$nombre);
					$image=true;
				}
				if($verificar['success']=='true'){
					$sql="SELECT * FROM pedido_modelo_color WHERE mcolo_codig ='".$codig."'";
					$color=$conexion->createCommand($sql)->queryRow();
					if($image==false){
						$destino=$color['mcolo_ruta'];
					}
					if($color){

						$sql="SELECT * FROM pedido_modelo_color 
						  WHERE tmode_codig='".$tmode."' 
							AND mcolo_numer='".$numer."'
							AND mcolo_descr='".$descr."'
							AND mcolo_codig='".$codig."'";
						$color2=$conexion->createCommand($sql)->queryRow();
						
						if(!$color2 or ($color2['mcolo_codig']==$codig and $color2['mcolo_descr']==$descr and $image==true)){
								$sql="UPDATE pedido_modelo_color
									  SET tmode_codig='".$tmode."',
										  mcolo_numer='".$numer."',
										  mcolo_descr='".$descr."',
										  mcolo_ruta='".$destino."'	
									  WHERE mcolo_codig='".$codig."'";
								$res1=$conexion->createCommand($sql)->execute();
								if($res1){
									$transaction->commit();
										$msg=array('success'=>'true','msg'=>'Color actualizado correctamente');
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al actualizar El Color');	
								}
							
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'El Color ya esta registrado');
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Color no existe');
					}
				}else{
					$transaction->rollBack();
					$msg=$verificar;	
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			
			$sql="SELECT * 
			  FROM pedido_modelo_color a
			  WHERE a.mcolo_codig ='".$_GET['c']."'";
			$color=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('color' => $color));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				
				$sql="SELECT * FROM pedido_modelo_color WHERE mcolo_codig='".$codig."'";
				$color=$conexion->createCommand($sql)->queryRow();
				if($color){

					$sql="DELETE FROM pedido_modelo_color WHERE mcolo_codig='".$codig."'";
					$res1=$conexion->createCommand($sql)->execute();
					//echo $sql;
					if($res1){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Color eliminado correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al eliminar el Color de Usuario');	
					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Color no existe');
				}
					
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_modelo_color a
			  WHERE a.mcolo_codig ='".$_GET['c']."'";
			$color=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('color' => $color));
		}
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}