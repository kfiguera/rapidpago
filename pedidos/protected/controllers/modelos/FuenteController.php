<?php
class FuenteController extends Controller
{
	public $funciones;

	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->render('index');
	}
	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descripcion']){
			$descripcion=mb_strtoupper($_POST['descripcion']);
			$condicion.="a.fuent_descr like '%".$descripcion."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM pedido_fuente a
			  WHERE a.fuent_codig ='".$_GET['c']."'";
		$fuente=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('fuente' => $fuente));
	}
	public function actionRegistrar()
	{
		if($_POST){
			//DATOS DEL FORMULARIO
			$descr=$_POST['descr'];

			//DATOS DE LA IMAGEN
			$file=$_FILES['image'];
			$ruta='files/fuente/';
			$name=explode(".", $file['name']);
			$nombre=date('Ymd_His.').end($name);
			$destino=$ruta.$nombre;
			$descr=mb_strtoupper($_POST['descr']);
			
			//DATOS DE AUDITORIA
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{

				$verificar=$this->funciones->CopiarEmoji($file,$ruta,$nombre);
				
				if($verificar['success']=='true'){

					$sql="SELECT * FROM pedido_fuente WHERE fuent_descr = '".$descr."'";
					$fuente=$conexion->createCommand($sql)->queryRow();
					if(!$fuente){
						$sql="INSERT INTO pedido_fuente(fuent_descr, fuent_ruta, usuar_codig, fuent_fcrea, fuent_hcrea) VALUES ('".$descr."', '".$destino."', '".$usuar."', '".$fecha."', '".$hora."')";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Fuente guardado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al guardar El Fuente');	
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Fuente ya existe');
					}
				}else{
					$transaction->rollBack();
					$msg=$verificar;	
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			//DATOS DEL FORMULARIO
			$descr=$_POST['descr'];
			$codig=mb_strtoupper($_POST['codig']);

			//DATOS DE LA IMAGEN
			$file=$_FILES['image'];
			$ruta='files/fuente/';
			$name=explode(".", $file['name']);
			$nombre=date('Ymd_His.').end($name);
			$destino=$ruta.$nombre;
			$descr=mb_strtoupper($_POST['descr']);
			
			$image=false;
			$verificar['success']='true';

			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				if(!empty($file['name'])){
					$verificar=$this->funciones->CopiarEmoji($file,$ruta,$nombre);
					$image=true;
				}

				if($verificar['success']=='true'){
					$sql="SELECT * FROM pedido_fuente WHERE fuent_codig ='".$codig."'";
					$fuente=$conexion->createCommand($sql)->queryRow();
					if($image==false){
						$destino=$fuente['fuent_ruta'];
					}
					if($fuente){

						$sql="SELECT * FROM pedido_fuente WHERE fuent_descr = '".$descr."'";
						$fuente2=$conexion->createCommand($sql)->queryRow();
						
						if(!$fuente2 or ($fuente['fuent_codig']==$codig and $fuente['fuent_descr']==$descr and $image==true)){
								$sql="UPDATE pedido_fuente
									  SET fuent_descr='".$descr."',
									      fuent_ruta='".$destino."'		
									  WHERE fuent_codig='".$codig."'";
								$res1=$conexion->createCommand($sql)->execute();
								if($res1){
									$transaction->commit();
										$msg=array('success'=>'true','msg'=>'Fuente actualizado correctamente');
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al actualizar El Fuente');	
								}
							
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'El Fuente ya esta registrado');
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Fuente no existe');
					}
				}else{
					$transaction->rollBack();
					$msg=$verificar;	
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			
			$sql="SELECT * 
			  FROM pedido_fuente a
			  WHERE a.fuent_codig ='".$_GET['c']."'";
			$fuente=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('fuente' => $fuente));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				/*$sql="SELECT * FROM pedido_modelo_conjunto WHERE fuent_codig='".$codig."'";
				$conjunto=$conexion->createCommand($sql)->queryRow();
				if(!$conjunto){*/
					$sql="SELECT * FROM pedido_fuente WHERE fuent_codig='".$codig."'";
					$fuente=$conexion->createCommand($sql)->queryRow();
					if($fuente){

						$sql="DELETE FROM pedido_fuente WHERE fuent_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Fuente eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Fuente de Usuario');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Fuente no existe');
					}
				/*}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No se puede eliminar el Fuente, debido a que esta asociado a un Conjunto');
				}*/	
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_fuente a
			  WHERE a.fuent_codig ='".$_GET['c']."'";
			$fuente=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('fuente' => $fuente));
		}
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}