<?php

class CategoriaController extends Controller
{
	public $funciones;

	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->render('index');
	}
	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descripcion']){
			$descripcion=mb_strtoupper($_POST['descripcion']);
			$condicion.="a.mcate_descr like '%".$descripcion."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM pedido_modelo_categoria a
			  WHERE a.mcate_codig ='".$_GET['c']."'";
		$categoria=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('categoria' => $categoria));
	}
	public function actionRegistrar()
	{
		if($_POST){
			
			$descr=mb_strtoupper($_POST['descr']);
			
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_modelo_categoria WHERE mcate_descr = '".$descr."'";
				$categoria=$conexion->createCommand($sql)->queryRow();
				if(!$categoria){
					$sql="INSERT INTO pedido_modelo_categoria(mcate_descr,  usuar_codig, mcate_fcrea, mcate_hcrea) VALUES ('".$descr."', '".$usuar."', '".$fecha."', '".$hora."')";
					$res1=$conexion->createCommand($sql)->execute();
					if($res1){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Categoria guardado correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al guardar El Categoria');	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Categoria ya existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codig=mb_strtoupper($_POST['codig']);
			$descr=mb_strtoupper($_POST['descr']);
			
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_modelo_categoria WHERE mcate_codig ='".$codig."'";
				$categoria=$conexion->createCommand($sql)->queryRow();
				if($categoria){

					$sql="SELECT * FROM pedido_modelo_categoria WHERE mcate_descr = '".$descr."' and mcate_codig = '".$codig."'";
					$categoria2=$conexion->createCommand($sql)->queryRow();
					
					if(!$categoria2 or ($categoria['mcate_codig']==$codig and $categoria['mcate_descr']==$descr)){
							$sql="UPDATE pedido_modelo_categoria
								  SET mcate_descr='".$descr."'		
								  WHERE mcate_codig='".$codig."'";
							$res1=$conexion->createCommand($sql)->execute();
							if($res1){
								$transaction->commit();
									$msg=array('success'=>'true','msg'=>'Categoria actualizado correctamente');
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al actualizar El Categoria');	
							}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Categoria ya esta registrado');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Categoria no existe');
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			
			$sql="SELECT * 
			  FROM pedido_modelo_categoria a
			  WHERE a.mcate_codig ='".$_GET['c']."'";
			$categoria=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('categoria' => $categoria));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_modelo_conjunto WHERE mcate_codig='".$codig."'";
				$conjunto=$conexion->createCommand($sql)->queryRow();
				if(!$conjunto){
					$sql="SELECT * FROM pedido_modelo_categoria WHERE mcate_codig='".$codig."'";
					$categoria=$conexion->createCommand($sql)->queryRow();
					if($categoria){

						$sql="DELETE FROM pedido_modelo_categoria WHERE mcate_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Categoria eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Tipo de Usuario');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Categoria no existe');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No se puede eliminar el Categoria, debido a que esta asociado a un Conjunto');
				}	
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_modelo_categoria a
			  WHERE a.mcate_codig ='".$_GET['c']."'";
			$categoria=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('categoria' => $categoria));
		}
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}