<?php
class VarianteController extends Controller
{
	public $funciones;

	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->render('index');
	}
	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descripcion']){
			$descripcion=mb_strtoupper($_POST['descripcion']);
			$condicion.="a.mvari_descr like '%".$descripcion."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM pedido_modelo_variante a
			  WHERE a.mvari_codig ='".$_GET['c']."'";
		$variante=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('variante' => $variante));
	}
	public function actionRegistrar()
	{
		if($_POST){
			
			$descr=mb_strtoupper($_POST['descr']);
			$preci=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['preci']));
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_modelo_variante WHERE mvari_descr = '".$descr."'";
				$variante=$conexion->createCommand($sql)->queryRow();
				if(!$variante){
					$sql="INSERT INTO pedido_modelo_variante(mvari_descr, mvari_preci, usuar_codig, mvari_fcrea, mvari_hcrea) VALUES ('".$descr."', '".$preci."', '".$usuar."', '".$fecha."', '".$hora."')";
					$res1=$conexion->createCommand($sql)->execute();
					if($res1){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Variante guardado correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al guardar El Variante');	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Variante ya existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codig=mb_strtoupper($_POST['codig']);
			$descr=mb_strtoupper($_POST['descr']);
			$preci=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['preci']));
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_modelo_variante WHERE mvari_codig ='".$codig."'";
				$variante=$conexion->createCommand($sql)->queryRow();
				if($variante){

					$sql="SELECT * FROM pedido_modelo_variante WHERE mvari_descr = '".$descr."' and mvari_preci = '".$preci."' and mvari_codig = '".$codig."'";
					$variante2=$conexion->createCommand($sql)->queryRow();
					
					if(!$variante2 or ($variante['mvari_codig']==$codig and $variante['mvari_preci']==$preci)){
							$sql="UPDATE pedido_modelo_variante
								  SET mvari_descr='".$descr."',
								      mvari_preci='".$preci."'		
								  WHERE mvari_codig='".$codig."'";
							$res1=$conexion->createCommand($sql)->execute();
							if($res1){
								$transaction->commit();
									$msg=array('success'=>'true','msg'=>'Variante actualizado correctamente');
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al actualizar El Variante');	
							}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Variante ya esta registrado');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Variante no existe');
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			
			$sql="SELECT * 
			  FROM pedido_modelo_variante a
			  WHERE a.mvari_codig ='".$_GET['c']."'";
			$variante=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('variante' => $variante));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_modelo_conjunto WHERE mvari_codig='".$codig."'";
				$conjunto=$conexion->createCommand($sql)->queryRow();
				if(!$conjunto){
					$sql="SELECT * FROM pedido_modelo_variante WHERE mvari_codig='".$codig."'";
					$variante=$conexion->createCommand($sql)->queryRow();
					if($variante){

						$sql="DELETE FROM pedido_modelo_variante WHERE mvari_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Variante eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Variante de Usuario');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Variante no existe');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No se puede eliminar el Variante, debido a que esta asociado a un Conjunto');
				}	
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_modelo_variante a
			  WHERE a.mvari_codig ='".$_GET['c']."'";
			$variante=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('variante' => $variante));
		}
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}