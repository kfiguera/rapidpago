<?php
class EmojiController extends Controller
{
	public $funciones;

	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->render('index');
	}
	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descripcion']){
			$descripcion=mb_strtoupper($_POST['descripcion']);
			$condicion.="a.emoji_descr like '%".$descripcion."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM pedido_emoji a
			  WHERE a.emoji_codig ='".$_GET['c']."'";
		$emoji=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('emoji' => $emoji));
	}
	public function actionRegistrar()
	{
		if($_POST){
			//DATOS DEL FORMULARIO
			$descr=$_POST['descr'];

			//DATOS DE LA IMAGEN
			$file=$_FILES['image'];
			$ruta='files/emoji/';
			$name=explode(".", $file['name']);
			$nombre=date('Ymd_His.').end($name);
			$destino=$ruta.$nombre;
			$descr=mb_strtoupper($_POST['descr']);
			
			//DATOS DE AUDITORIA
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{

				$verificar=$this->funciones->CopiarEmoji($file,$ruta,$nombre);
				
				if($verificar['success']=='true'){

					$sql="SELECT * FROM pedido_emoji WHERE emoji_descr = '".$descr."'";
					$emoji=$conexion->createCommand($sql)->queryRow();
					if(!$emoji){
						$sql="INSERT INTO pedido_emoji(emoji_descr, emoji_ruta, usuar_codig, emoji_fcrea, emoji_hcrea) VALUES ('".$descr."', '".$destino."', '".$usuar."', '".$fecha."', '".$hora."')";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Emoji guardado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al guardar El Emoji');	
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Emoji ya existe');
					}
				}else{
					$transaction->rollBack();
					$msg=$verificar;	
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			//DATOS DEL FORMULARIO
			$descr=$_POST['descr'];
			$codig=mb_strtoupper($_POST['codig']);

			//DATOS DE LA IMAGEN
			$file=$_FILES['image'];
			$ruta='files/emoji/';
			$name=explode(".", $file['name']);
			$nombre=date('Ymd_His.').end($name);
			$destino=$ruta.$nombre;
			$descr=mb_strtoupper($_POST['descr']);
			
			$image=false;
			$verificar['success']='true';

			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				if(!empty($file['name'])){
					$verificar=$this->funciones->CopiarEmoji($file,$ruta,$nombre);
					$image=true;
				}

				if($verificar['success']=='true'){
					$sql="SELECT * FROM pedido_emoji WHERE emoji_codig ='".$codig."'";
					$emoji=$conexion->createCommand($sql)->queryRow();
					if($image==false){
						$destino=$emoji['emoji_ruta'];
					}
					if($emoji){

						$sql="SELECT * FROM pedido_emoji WHERE emoji_descr = '".$descr."'";
						$emoji2=$conexion->createCommand($sql)->queryRow();
						
						if(!$emoji2 or ($emoji['emoji_codig']==$codig and $emoji['emoji_descr']==$descr and $image==true)){
								$sql="UPDATE pedido_emoji
									  SET emoji_descr='".$descr."',
									      emoji_ruta='".$destino."'		
									  WHERE emoji_codig='".$codig."'";
								$res1=$conexion->createCommand($sql)->execute();
								if($res1){
									$transaction->commit();
										$msg=array('success'=>'true','msg'=>'Emoji actualizado correctamente');
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al actualizar El Emoji');	
								}
							
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'El Emoji ya esta registrado');
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Emoji no existe');
					}
				}else{
					$transaction->rollBack();
					$msg=$verificar;	
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			
			$sql="SELECT * 
			  FROM pedido_emoji a
			  WHERE a.emoji_codig ='".$_GET['c']."'";
			$emoji=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('emoji' => $emoji));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				/*$sql="SELECT * FROM pedido_modelo_conjunto WHERE emoji_codig='".$codig."'";
				$conjunto=$conexion->createCommand($sql)->queryRow();
				if(!$conjunto){*/
					$sql="SELECT * FROM pedido_emoji WHERE emoji_codig='".$codig."'";
					$emoji=$conexion->createCommand($sql)->queryRow();
					if($emoji){

						$sql="DELETE FROM pedido_emoji WHERE emoji_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Emoji eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Emoji de Usuario');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Emoji no existe');
					}
				/*}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No se puede eliminar el Emoji, debido a que esta asociado a un Conjunto');
				}*/	
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_emoji a
			  WHERE a.emoji_codig ='".$_GET['c']."'";
			$emoji=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('emoji' => $emoji));
		}
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}