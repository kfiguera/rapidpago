<?php
class OpcionalController extends Controller
{
	public $funciones;

	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->render('index');
	}
	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descripcion']){
			$descripcion=mb_strtoupper($_POST['descripcion']);
			$condicion.="a.mopci_descr like '%".$descripcion."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM pedido_modelo_opcional a
			  WHERE a.mopci_codig ='".$_GET['c']."'";
		$opcional=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('opcional' => $opcional));
	}
	public function actionRegistrar()
	{
		if($_POST){
			
			$descr=mb_strtoupper($_POST['descr']);
			$preci=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['preci']));
			$inclu=json_encode($_POST["inclu"]);
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_modelo_opcional WHERE mopci_descr = '".$descr."'";
				$opcional=$conexion->createCommand($sql)->queryRow();
				if(!$opcional){
					$sql="INSERT INTO pedido_modelo_opcional(mopci_descr, mopci_preci,  mopci_inclu, usuar_codig, mopci_fcrea, mopci_hcrea) VALUES ('".$descr."', '".$preci."', '".$inclu."', '".$usuar."', '".$fecha."', '".$hora."')";
					$res1=$conexion->createCommand($sql)->execute();
					if($res1){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Opcional guardado correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al guardar El Opcional');	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Opcional ya existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codig=mb_strtoupper($_POST['codig']);
			$descr=mb_strtoupper($_POST['descr']);
			$preci=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['preci']));
			$inclu=json_encode($_POST["inclu"]);
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_modelo_opcional WHERE mopci_codig ='".$codig."'";
				$opcional=$conexion->createCommand($sql)->queryRow();
				if($opcional){

					$sql="SELECT * FROM pedido_modelo_opcional WHERE mopci_descr = '".$descr."' and mopci_preci = '".$preci."' and mopci_codig = '".$codig."'";
					$opcional2=$conexion->createCommand($sql)->queryRow();
					
					if(!$opcional2 or ($opcional['mopci_codig']==$codig and $opcional['mopci_preci']==$preci)){
							$sql="UPDATE pedido_modelo_opcional
								  SET mopci_descr='".$descr."',
								      mopci_inclu='".$inclu."',
								      mopci_preci='".$preci."'		
								  WHERE mopci_codig='".$codig."'";
							$res1=$conexion->createCommand($sql)->execute();
							if($res1){
								$transaction->commit();
									$msg=array('success'=>'true','msg'=>'Opcional actualizado correctamente');
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al actualizar El Opcional');	
							}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Opcional ya esta registrado');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Opcional no existe');
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			
			$sql="SELECT * 
			  FROM pedido_modelo_opcional a
			  WHERE a.mopci_codig ='".$_GET['c']."'";
			$opcional=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('opcional' => $opcional));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_modelo_conjunto WHERE mopci_codig='".$codig."'";
				$conjunto=$conexion->createCommand($sql)->queryRow();
				if(!$conjunto){
					$sql="SELECT * FROM pedido_modelo_opcional WHERE mopci_codig='".$codig."'";
					$opcional=$conexion->createCommand($sql)->queryRow();
					if($opcional){

						$sql="DELETE FROM pedido_modelo_opcional WHERE mopci_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Opcional eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Opcional de Usuario');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Opcional no existe');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No se puede eliminar el Opcional, debido a que esta asociado a un Conjunto');
				}	
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_modelo_opcional a
			  WHERE a.mopci_codig ='".$_GET['c']."'";
			$opcional=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('opcional' => $opcional));
		}
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}