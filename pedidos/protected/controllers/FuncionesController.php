<?php

class FuncionesController extends Controller
{
	public $funciones;
	public function init()
	{
	    
	    
	    if (Yii::app()->user->isGuest) {
            $this->redirect(Yii::app()->request->baseUrl.'/site/login');
        }else{
        	$verificar=$this->VerificarSesion();
        	if(!$verificar){
        		$this->redirect(Yii::app()->request->baseUrl.'/site/logout');
        	}
        }
	}
	public function VerificarSesion(){
		$return=true;
		$conexion=Yii::app()->db;
		$hash=Yii::app()->user->id['usuario']['sesion'];
		$sql="SELECT * FROM seguridad_sesiones WHERE sesio_numer='".$hash."'";
		$sesion=$conexion->createCommand($sql)->queryRow();
		if($sesion){
			if($sesion['pesta_codig']=='1'){
				$init=new DateTime();
				$desde=new DateTime($sesion['sesio_finic'].' '.$sesion['sesio_hinic']);
                $hasta=new DateTime($sesion['sesio_fvenc'].' '.$sesion['sesio_hvenc']);
                if($desde <= $init AND $init <= $hasta){
                	$date = new DateTime();
                	$date->modify('+20 minutes');
                	$vencimiento= $date->format('Y-m-d H:i:s');
            		$fvenc= $date->format('Y-m-d');
            		$hvenc= $date->format('H:i:s');


                	$sql="UPDATE seguridad_sesiones 
                		  SET sesio_fvenc = '".$fvenc."',
						  	  sesio_hvenc = '".$hvenc."'
						  WHERE sesio_numer='".$hash."'";
					$sesion=$conexion->createCommand($sql)->execute();
                	$return=true;
                }else{
                	Yii::app()->user->setFlash('danger', 'Su sesion esta vencida');
                	$return=false;

                }
			}else{
				Yii::app()->user->setFlash('danger', 'Su sesion no esta activa');
				$return=false;

			}
		}else{

			Yii::app()->user->setFlash('danger', 'Su sesion no existe');
			$return=false;
		}
		return $return;
	}
	public function actionIndex()
	{
		$this->redirect(Yii::app()->request->baseUrl.'/site/inicio');
	}
	public function TransformarTelefono_bd($monto)
	{
		//DEBE DE ESTAR 0.000,00
		$monto=str_replace('.', '', $monto);
		$monto=str_replace('-', '', $monto);
		//RETORNA 0000.00
		return $monto;
	}
	public function TransformarMonto_bd($monto)
	{
		//DEBE DE ESTAR 0.000,00
		$monto=str_replace('.', '', $monto);
		$monto=str_replace(',', '.', $monto);
		//RETORNA 0000.00
		return $monto;
	}
	public function TransformarMonto_v($monto,$cantidad)
	{
		//DEBE DE ESTAR 0000.00
		$monto=number_format($monto,$cantidad,',','.');
		//RETORNA 0.000,00
		return $monto;
	}
	public function TransformarFecha_bd($fecha)
	{
		//DEBE DE ESTAR DD/MM/AAAA
		if($fecha!='' and $fecha!=' ' and $fecha!=null and $fecha!='00/00/0000'){
			$fecha=explode('/',$fecha);
			$fecha=$fecha[2].'-'.$fecha[1].'-'.$fecha[0];
		}
		//RETORNA AAAA-MM-DD
		return $fecha;
	}
	public function TransformarFecha_v($fecha)
	{
		//DEBE DE ESTAR AAAA-MM-DD
		
		if($fecha!='' and $fecha!=' ' and $fecha!=null and $fecha!='0000-00-00'){
			$fecha=explode('-',$fecha);
			$fecha=$fecha[2].'/'.$fecha[1].'/'.$fecha[0];
		}else{
			$fecha=null;
		}
		//RETORNA DD/MM/AAAA
		return $fecha;
	}
	public function Meses_letras($mes)
	{
		$meses=array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
		$m=$meses[$mes-1];
		return $m;
	}
	function Ultimo_Dia_Mes($elAnio,$elMes) {
 	 	return date("d",(mktime(0,0,0,$elMes+1,1,$elAnio)-1));
	}
	/*
		REPORTES
	*/
	public function header($titulo){

		$html='<div name="myheader">
 				<table width="100%">
 					<tr>
 						<td width="33%" style="color:#0000BB">
 							<img src="'.Yii::app()->request->baseUrl.'/assets/img/logo-importadora-pdf.png" width="15%">
 						</td>
 						<td width="34%" style="text-align: center; vertical-align: bottom; ">
 							<b>'.$titulo.'</b>
 						</td>
 						<td width="33%" style="text-align: right; vertical-align: bottom;">
 							<b>Fecha: </b>'.date('d/m/Y').'<br>
 							<b>Hora: </b>'.date('h:i:s A').'<br>
 							<b>Usuario: </b>'.Yii::app()->user->id['usuario']['nombre'].'
 						</td>
 					<tr>
 				</table>
 				<div style="border-top: 1px solid #000000;">

 				</div>


 			</div>';
 		return $html;
	}
	public function footer(){
		$html='<div name="myfooter">
 					<div style="border-top: 1px solid #000000; font-size: 6pt; padding-top: 1mm; text-align: center;">
 						Página {PAGENO} de {nb}
 					</div>
 				</div>';
 		return $html;
	}
	public function evaluarFuncion($formula){

		$str = '$resultado = '.$formula.';';

		eval($str);
		var_dump($resultado);
		exit();
		return $resultado;
	}

	public function actionGenerarValorProd()
	{

		$producto['success']='true';
		$producto['producto']=$_POST['producto'];
		$producto['tipo']=$_POST['tipo'];
		$producto['cantidad']=1;
		$producto['precio']=0;
		$producto['monto']=0;
		$tasa=1;
		$conexion=Yii::app()->db;
		$transaction=$conexion->beginTransaction();
		try{
			if($producto['tipo']=='1'){
				$sql="SELECT * 
					  FROM inventario a
					  JOIN p_moneda b on (a.moned_codig = b.moned_codig)
					  WHERE a.inven_codig='".$producto['producto']."'";
				$prod=$conexion->createCommand($sql)->queryRow();
				$tasa=$prod["moned_cambi"]*1.00;
				$precio=$prod["inven_punid"]*$tasa;
				$producto['cantidad']=$this->TransformarMonto_v($producto['cantidad'],0);
				$producto['precio']=$this->TransformarMonto_v($precio,2);
				$producto['monto']=$this->TransformarMonto_v($precio*$producto['cantidad'],2);
				$producto['tasa']=$tasa;
			}else if($producto['tipo']=='2'){
				$sql="SELECT * FROM servicios
				 WHERE servi_codig='".$producto['producto']."'";
				$prod=$conexion->createCommand($sql)->queryRow();
				$producto['cantidad']=$this->TransformarMonto_v($producto['cantidad'],0);
				$producto['precio']=$this->TransformarMonto_v($prod["servi_preci"],2);
				$producto['monto']=$this->TransformarMonto_v($prod["servi_preci"]*$producto['cantidad'],2);
				$producto['tasa']=$tasa;
			}else{
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');	
			}
			
			echo json_encode($producto);
		}catch(Exception $e){
			//var_dump($e);
			$transaction->rollBack();
			$msg=array('success'=>'false','msg'=>'Error al verificar la información');
		}
	}
	public function actionGenerarMontoTotal()
	{
		$productos=$_POST['mprod'];
		$montoTotal=0;
		foreach ($productos as $key => $producto) {
			$montoTotal+=$this->TransformarMonto_bd($producto);
		}
		$msg['success']='true';
		$msg['monto']=$this->TransformarMonto_v($montoTotal,2);
		echo json_encode($msg);
		
	}
	public function actionCambiarValorProd()
	{

		$cantidad=$this->TransformarMonto_bd($_POST['cantidad']);
		$precio=$this->TransformarMonto_bd($_POST['precio']);
		$monto=$cantidad*$precio;
		$msg['success']='true';
		$msg['monto']=$this->TransformarMonto_v($monto,2);
		echo json_encode($msg);
		
	}
	public function actionTipoDocumento(){
		$tclie=$_POST['tclie'];
		$sql="SELECT a.* FROM p_documento_tipo a 
				JOIN p_tipo_cliente_documento b ON (a.tdocu_codig=b.tdocu_codig) 
				WHERE b.tclie_codig='".$tclie."'";
		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryAll();
   		$data=CHtml::listData($data,'tdocu_codig','tdocu_descr');
   		echo CHtml::tag('option', array('value'=>''), CHtml::encode('Seleccione...'), true);
  		foreach($data as $value=>$name){
   			echo CHtml::tag('option', array('value'=>$value), CHtml::encode($name), true);
  		}
	}
	public function actionTipoProducto(){
		$tprod=$_POST['tprod'];
		$conexion=Yii::app()->db;
		if($tprod=='1'){
			$sql="SELECT inven_codig codigo, inven_descr descripcion FROM inventario a ";	
			$data=$conexion->createCommand($sql)->queryAll();
		}else if($tprod=='2'){
			$sql="SELECT servi_codig codigo, servi_descr descripcion FROM servicios a ";
			$data=$conexion->createCommand($sql)->queryAll();
		}
		//var_dump($data);
		
   		$data=CHtml::listData($data,'codigo','descripcion');
   		echo CHtml::tag('option', array('value'=>''), CHtml::encode('Seleccione...'), true);
  		foreach($data as $value=>$name){
   			echo CHtml::tag('option', array('value'=>$value), CHtml::encode($name), true);
  		}
	}
	public function actionListarCliente(){
		$tdocu=$_POST['tdocu'];
		$ndocu=$_POST['ndocu'];
		$sql="SELECT a.* FROM cliente a 
				WHERE a.tdocu_codig='".$tdocu."' 
				  AND a.clien_ndocu='".$ndocu."'";
		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryRow();
		$msg['success']='true';
		$msg['denom']=$data['clien_denom'];
		$msg['corre']=$data['clien_corre'];
		$msg['telef']=$data['clien_telef'];
		$msg['direc']=$data['clien_direc'];
		$msg['clien']=$data['clien_codig'];
   		echo json_encode($msg);
	}
	public function actionListarColegio(){
		$coleg=$_POST['coleg'];
		$sql="SELECT a.* FROM colegio a 
				WHERE a.coleg_codig='".$coleg."'";
		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryRow();
		$msg['success']='true';
		$msg['pgweb']=$data['coleg_pgweb'];
		$msg['telef']=$data['colec_telef'];
		$msg['direc']=$data['coleg_direc'];
		$msg['rede1']=$data['coleg_rede1'];
		$msg['rede2']=$data['coleg_rede2'];
		$msg['rede3']=$data['coleg_rede3'];
   		echo json_encode($msg);
	}
	public function actionListarProveedor(){
		$tdocu=$_POST['tdocu'];
		$ndocu=$_POST['ndocu'];
		$sql="SELECT a.* FROM inventario_proveedor a 
				WHERE a.tdocu_codig='".$tdocu."' 
				  AND a.prove_ndocu='".$ndocu."'";
		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryRow();
		$msg['success']='true';
		$msg['denom']=$data['prove_denom'];
		$msg['corre']=$data['prove_corre'];
		$msg['telef']=$data['prove_telef'];
		$msg['direc']=$data['prove_direc'];
		$msg['clien']=$data['prove_codig'];
   		echo json_encode($msg);
	}
	public function actionListarMontoPedido(){
		$pedid=$_POST['pedid'];
		$sql="SELECT a.* FROM solicitud a 
				WHERE a.solic_codig='".$pedid."'";

		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryRow();
		$sql="SELECT a.* FROM solicitud_pagos a 
				WHERE a.solic_codig='".$pedid."'
				AND a.epago_codig='2'";

		$pagos=$conexion->createCommand($sql)->queryAll();
		foreach ($pagos as $key => $pago) {
			$pagado+=($pago['pagos_monto']*1.0);
		}
		$msg['success']='true';
		$msg['mpedi']=$this->TransformarMonto_v($data['solic_monto'],0);
		$msg['resto']=$this->TransformarMonto_v($data['solic_monto']-$pagado,0);
   		echo json_encode($msg);
	}
	public function MontoCambio($producto,$tipo){
		$tasa=1;
		$conexion=Yii::app()->db;
		if($tipo=='1'){
			$sql="SELECT * 
				  FROM inventario a
				  JOIN p_moneda b on (a.moned_codig = b.moned_codig)
				  WHERE a.inven_codig='".$producto."'";
			$prod=$conexion->createCommand($sql)->queryRow();
			$tasa=$prod["moned_cambi"]*1.00;
			$precio=$prod["inven_punid"]*$tasa;
		}else if($tipo=='2'){
			$sql="SELECT * 
				  FROM servicios a 
				  JOIN p_moneda b on (a.moned_codig = b.moned_codig)
			 	  WHERE a.servi_codig='".$producto['producto']."'";
			$prod=$conexion->createCommand($sql)->queryRow();
			$prod=$conexion->createCommand($sql)->queryRow();
			$tasa=$prod["moned_cambi"]*1.00;
			$precio=$prod["servi_preci"]*$tasa;
		}
   		return $precio;
	}
	public function MontoCambioReverso($producto,$monto,$tipo){
		$tasa=1;
		$conexion=Yii::app()->db;
		if($tipo=='1'){
			$sql="SELECT * 
				  FROM inventario a
				  JOIN p_moneda b on (a.moned_codig = b.moned_codig)
				  WHERE a.inven_codig='".$producto."'";
			$prod=$conexion->createCommand($sql)->queryRow();
			$tasa=$prod["moned_cambi"]*1.00;
			$precio=$monto/$tasa;
		}else if($tipo=='2'){
			$sql="SELECT * 
				  FROM servicios a 
				  JOIN p_moneda b on (a.moned_codig = b.moned_codig)
			 	  WHERE a.servi_codig='".$producto['producto']."'";
			$prod=$conexion->createCommand($sql)->queryRow();
			$prod=$conexion->createCommand($sql)->queryRow();
			$tasa=$prod["moned_cambi"]*1.00;
			$precio=$monto/$tasa;
		}
   		return $precio;
	}
	public function enviarCorreo($vista,$datos,$asunto,$destinatario){
        $message = new YiiMailMessage;
        //mensaje
        $message->setBody($this->renderpartial($vista,$datos,true),'text/html');
        //asunto
        $message->subject = $asunto;
        //Destinatarios
        $message->addTo($destinatario);
        //$message->addTo(Yii::app()->params['adminEmail']);
        $message->setBcc(
        	array(/*'kevinalejandro3@gmail.com'=>'Kevin Alejandro Figuera',
        		  'kfiguera@smartwebtools.net'=>'Kevin Figuera',
        		  'iveymontoya@gmail.com'=>'Ivey Montoya',
        		  'imontoya@smartwebtools.net'=>'Ivey Montoya',*/
        		  'contacto@smartwebtools.net'=>'Contacto SWT',
        		  Yii::app()->params['adminEmail']=>'Contacto Rapidpago'));
        
        $message->setReplyTo(
        	array('ventas@rapidpago.com'=>'Ventas Rapidpago'));
        //$message->addTo('iveymontoya@gmail.com');*/
        //$message->addTo('kevinalejandro3@gmail.com');

        //$message->addTo(Yii::app()->params['adminEmail']);
        //Remitente
        $message->from=array(Yii::app()->params['adminEmail']=>'Contacto Rapidpago') ;
        
        $host=Yii::app()->mail->getTransport()->getHost();
        $ip= gethostbyname(Yii::app()->mail->getTransport()->getHost());
        if($host!=$ip){
            try{
		        //$msg=Yii::app()->mail->send($message);
		    }catch(\Swift_TransportException $e){
		        $response = $e->getMessage() ;
		    } 
         	//Yii::app()->user->setFlash('success', 'Pronto recibiras instrucciones!');
        }
        Yii::app()->mail->getTransport()->stop(); // < this line closes socket and at next iteration its reopened
        sleep(5);
        //Yii::app()->user->setFlash('success', 'Pronto recibiras instrucciones!');
    }
    public function enviarCorreoAdjunto($vista,$datos,$asunto,$destinatario,$archivo,$nombre){
        $message = new YiiMailMessage;
        //mensaje
        $message->setBody($this->renderpartial($vista,$datos,true),'text/html');
        //asunto
        $message->subject = $asunto;
        //Destinatarios
        $message->addTo($destinatario);
        //$message->addTo(Yii::app()->params['adminEmail']);
        $message->setBcc(
        	array(/*'kevinalejandro3@gmail.com'=>'Kevin Alejandro Figuera',
        		  'kfiguera@smartwebtools.net'=>'Kevin Figuera',
        		  'iveymontoya@gmail.com'=>'Ivey Montoya',
        		  'imontoya@smartwebtools.net'=>'Ivey Montoya',*/
        		  'contacto@smartwebtools.net'=>'Contacto SWT',
        		  Yii::app()->params['adminEmail']=>'Contacto Rapidpago'));
        
        $message->setReplyTo(
        	array('ventas@rapidpago.com'=>'Ventas Rapidpago'));
         $message->attach( Swift_Attachment::fromPath($archivo)->setFilename($nombre) ); 

        //$message->addTo('iveymontoya@gmail.com');*/
        //$message->addTo('kevinalejandro3@gmail.com');

        //$message->addTo(Yii::app()->params['adminEmail']);
        //Remitente
        $message->from=array(Yii::app()->params['adminEmail']=>'Contacto Rapidpago');
        $host=Yii::app()->mail->getTransport()->getHost();
        $ip= gethostbyname(Yii::app()->mail->getTransport()->getHost());
        if($host!=$ip){
              
		    try{
		        //$msg=Yii::app()->mail->send($message);
		    }catch(\Swift_TransportException $e){
		        $response = $e->getMessage() ;
		    }
         	//Yii::app()->user->setFlash('success', 'Pronto recibiras instrucciones!');
        }
        Yii::app()->mail->getTransport()->stop(); // < this line closes socket and at next iteration its reopened
        sleep(5);
        //Yii::app()->user->setFlash('success', 'Pronto recibiras instrucciones!');
    }
    
    public function actionModulos(){
		$urole=$_POST['urole'];
		$sql="SELECT * 
			  FROM modulos 
			  WHERE modul_codig NOT IN (
			  	SELECT a.modul_codig 
			  	FROM permisos a 
				WHERE a.urole_codig='".$urole."'
			  )";
		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryAll();
   		$data=CHtml::listData($data,'modul_codig','modul_descr');
   		echo CHtml::tag('option', array('value'=>''), CHtml::encode('Seleccione...'), true);
  		foreach($data as $value=>$name){
   			echo CHtml::tag('option', array('value'=>$value), CHtml::encode($name), true);
  		}
	}
	public function generarPassword($cantidad){
		$char='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		$passw=substr(str_shuffle($char) , 0 , $cantidad );
		return $passw; 

	}
	public function GenerarCodigoVerificacion($solicitud){
		$datos=date('YmdHis').$solicitud.rand(0,9);


	    $secret_iv = mb_strtoupper(substr(hash('sha256',$datos),0,16));
	    
		
		return $secret_iv; 

	}
	//MODULO DE PEDIDOS
	public function generarCodigoPedido($cantidad,$codigo){
		$codig=/*date('Ym').'-'.*/str_pad($codigo, $cantidad,"0", STR_PAD_LEFT);
		return $codig; 
	}
	public function ActionPedidosModelos(){
		$tmode=$_POST['tmode'];
		var_dump($_POST);
		$sql="SELECT a.model_codig, b.model_descr
			  FROM pedido_modelo_conjunto a
			  JOIN pedido_modelo b ON (a.model_codig = b.model_codig)
			  WHERE a.tmode_codig ='".$tmode."'";
			  echo $sql;

		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryAll();
   		$data=CHtml::listData($data,'model_codig','model_descr');
   		echo CHtml::tag('option', array('value'=>''), CHtml::encode('Seleccione...'), true);
  		foreach($data as $value=>$name){
   			echo CHtml::tag('option', array('value'=>$value), CHtml::encode($name), true);
  		}
	}
	public function ActionPedidosCategoria(){
		$tmode=$_POST['tmode'];
		$model=$_POST['model'];
		$sql="SELECT a.mcate_codig, b.mcate_descr
			  FROM pedido_modelo_conjunto a
			  JOIN pedido_modelo_categoria b ON (a.mcate_codig = b.mcate_codig)
			  WHERE a.tmode_codig ='".$tmode."'
			  	AND a.model_codig ='".$model."'";
		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryAll();
   		$data=CHtml::listData($data,'mcate_codig','mcate_descr');
   		echo CHtml::tag('option', array('value'=>''), CHtml::encode('Seleccione...'), true);
  		foreach($data as $value=>$name){
   			echo CHtml::tag('option', array('value'=>$value), CHtml::encode($name), true);
  		}
	}
	public function ActionPedidosVariante(){
		$tmode=$_POST['tmode'];
		$model=$_POST['model'];
		$categ=$_POST['categ'];
		$sql="SELECT a.mvari_codig, b.mvari_descr
			  FROM pedido_modelo_conjunto a
			  JOIN pedido_modelo_variante b ON (a.mvari_codig = b.mvari_codig)
			  WHERE a.tmode_codig ='".$tmode."'
			  	AND a.model_codig ='".$model."'
			  	AND a.mcate_codig ='".$categ."'";
		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryAll();
   		$data=CHtml::listData($data,'mvari_codig','mvari_descr');
   		echo CHtml::tag('option', array('value'=>''), CHtml::encode('Seleccione...'), true);
  		foreach($data as $value=>$name){
   			echo CHtml::tag('option', array('value'=>$value), CHtml::encode($name), true);
  		}
	}
	public function ActionPedidosColor(){
		$tmode=$_POST['tmode'];
		$sql="SELECT mcolo_codig, concat( mcolo_numer,' - ',mcolo_descr) mcolo_descr
              FROM pedido_modelo_color 
			  WHERE tmode_codig ='".$tmode."'";
		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryAll();
   		$data=CHtml::listData($data,'mcolo_codig','mcolo_descr');
   		echo CHtml::tag('option', array('value'=>''), CHtml::encode('Seleccione...'), true);
  		foreach($data as $value=>$name){
   			echo CHtml::tag('option', array('value'=>$value), CHtml::encode($name), true);
  		}
	}
	public function ActionPedidosOpcionales(){
		$tmode=$_POST['tmode'];
		$model=$_POST['model'];
		$categ=$_POST['categ'];
		$varia=$_POST['varia'];
		$sql="SELECT a.*
			  FROM pedido_modelo_conjunto a
			  WHERE a.tmode_codig ='".$tmode."'
			  	AND a.model_codig ='".$model."'
			  	AND a.mcate_codig ='".$categ."'";
		$conexion=Yii::app()->db;
		$result=$conexion->createCommand($sql)->queryRow();
		$incluyen=json_decode($result['mopci_codig']);
		$i=0;
		$opcio='(';
		foreach ($incluyen as $key => $value) {
			if($i>0){
				$opcio.=",";
			}
			$opcio.="'".$value."'";
			$i++;
		}
		$opcio.=')';
		$sql="SELECT a.*
			  FROM pedido_modelo_opcional a
			  WHERE a.mopci_codig in ".$opcio."";
		
		$result=$conexion->createCommand($sql)->queryAll();
   		$data=CHtml::listData($result,'mopci_codig','mopci_descr');
   		echo CHtml::tag('option', array('value'=>''), CHtml::encode('Seleccione...'), true);
  		foreach($data as $value=>$name){
   			echo CHtml::tag('option', array('value'=>$value), CHtml::encode($name), true);
  		}
	}
	public function ActionPedidosOpcionales2(){
		$tmode=$_POST['tmode'];
		$model=$_POST['model'];
		$categ=$_POST['categ'];
		$varia=$_POST['varia'];
		$sql="SELECT a.*
			  FROM pedido_modelo_conjunto a
			  WHERE a.tmode_codig ='".$tmode."'
			  	AND a.model_codig ='".$model."'
			  	AND a.mcate_codig ='".$categ."'";
		$conexion=Yii::app()->db;
		$result=$conexion->createCommand($sql)->queryRow();
		$incluyen=json_decode($result['mopci_codi2']);
		$i=0;
		$opcio='(';
		foreach ($incluyen as $key => $value) {
			if($i>0){
				$opcio.=",";
			}
			$opcio.="'".$value."'";
			$i++;
		}
		$opcio.=')';
		if($i>0){
			$sql="SELECT a.*
			  FROM pedido_modelo_opcional a
			  WHERE a.mopci_codig in ".$opcio."";
		
			$result=$conexion->createCommand($sql)->queryAll();
	   		$data=CHtml::listData($result,'mopci_codig','mopci_descr');
		}
		
   		echo CHtml::tag('option', array('value'=>''), CHtml::encode('Seleccione...'), true);
  		foreach($data as $value=>$name){
   			echo CHtml::tag('option', array('value'=>$value), CHtml::encode($name), true);
  		}
	}
	public function ListarOpcionales($pedidos){
		$sql="SELECT a.*
			  FROM pedido_modelo_conjunto a
			  WHERE a.tmode_codig ='".$pedidos['tmode_codig']."'
			  	AND a.model_codig ='".$pedidos['model_codig']."'
			  	AND a.mcate_codig ='".$pedidos['mcate_codig']."'";
		$conexion=Yii::app()->db;
		$result=$conexion->createCommand($sql)->queryRow();
		$incluyen=json_decode($result['mopci_codig']);
		$i=0;
		$opcio='(';
		foreach ($incluyen as $key => $value) {
			if($i>0){
				$opcio.=",";
			}
			$opcio.="'".$value."'";
			$i++;
		}
		$opcio.=')';
		$sql="SELECT a.*
			  FROM pedido_modelo_opcional a
			  WHERE a.mopci_codig in ".$opcio."";
		$conexion=Yii::app()->db;
		$result=$conexion->createCommand($sql)->queryAll();
		foreach ($result as $key => $value) {
			$result[$key]['mopci_descr']=$value['mopci_descr'].' '.$this->TransformarMonto_v($value['mopci_preci']).'$';
		}
   		$data=CHtml::listData($result,'mopci_codig','mopci_descr');
   		return $data;
	}
	public function ListarOpcionales2($pedidos){
		$sql="SELECT a.*
			  FROM pedido_modelo_conjunto a
			  WHERE a.tmode_codig ='".$pedidos['tmode_codig']."'
			  	AND a.model_codig ='".$pedidos['model_codig']."'
			  	AND a.mcate_codig ='".$pedidos['mcate_codig']."'";
		$conexion=Yii::app()->db;
		$result=$conexion->createCommand($sql)->queryRow();
		$incluyen=json_decode($result['mopci_codi2']);
		$i=0;
		$opcio='(';
		foreach ($incluyen as $key => $value) {
			if($i>0){
				$opcio.=",";
			}
			$opcio.="'".$value."'";
			$i++;
		}
		$opcio.=')';
		if($i>0){
			$sql="SELECT a.*
			  FROM pedido_modelo_opcional a
			  WHERE a.mopci_codig in ".$opcio."";
			$conexion=Yii::app()->db;
			$result=$conexion->createCommand($sql)->queryAll();
			foreach ($result as $key => $value) {
				$result[$key]['mopci_descr']=$value['mopci_descr'].' '.$this->TransformarMonto_v($value['mopci_preci']).'$';
			}
	   		$data=CHtml::listData($result,'mopci_codig','mopci_descr');
		}
		
   		return $data;
	}
	public function ActionPedidosColorLinea(){
		$tmode=$_POST['tmode'];
		$ideta=$_POST['ideta'];
		$sql="SELECT mcolo_codig, concat( mcolo_numer,' - ',mcolo_descr) mcolo_descr
              FROM pedido_modelo_color 
			  WHERE tmode_codig ='".$tmode."'";
		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryAll();
   		$data=CHtml::listData($data,'mcolo_codig','mcolo_descr');
   		echo CHtml::tag('option', array('value'=>''), CHtml::encode('Seleccione...'), true);
  		if($ideta=='1'){
  			foreach($data as $value=>$name){
   			echo CHtml::tag('option', array('value'=>$value), CHtml::encode($name), true);
  			}	
  		}
  		
	}
	public function ActionPedidosColorOpcional(){
		$tmode=$_POST['tmode'];
		var_dump($_POST);
		$sql="SELECT mcolo_codig, concat( mcolo_numer,' - ',mcolo_descr) mcolo_descr
              FROM pedido_modelo_color 
			  WHERE tmode_codig ='".$tmode."'";
			  echo "$sql";
		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryAll();
   		$data=CHtml::listData($data,'mcolo_codig','mcolo_descr');
   		echo CHtml::tag('option', array('value'=>''), CHtml::encode('Seleccione...'), true);
		foreach($data as $value=>$name){
			echo CHtml::tag('option', array('value'=>$value), CHtml::encode($name), true);
		}	
  		
	}

	public function ActionPedidosEmoji(){
		$emoji=$_POST['id'];
		$sql="SELECT *
              FROM pedido_emoji 
			  WHERE emoji_codig ='".$emoji."'";
		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryRow();
   		$output = '<img src="'.Yii::app()->request->getBaseUrl(true).'/'.$data["emoji_ruta"].'" class="img-responsive" />';  

   		echo $output;

  		
	}
	public function ActionPedidosSimbolo(){
		$emoji=$_POST['id'];
		$sql="SELECT *
              FROM pedido_simbolo_simple 
			  WHERE ssimp_codig ='".$emoji."'";
		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryRow();
   		$output = '<img src="'.Yii::app()->request->getBaseUrl(true).'/'.$data["ssimp_ruta"].'" class="img-responsive" />';  

   		echo $output;

  		
	}
	public function ActionPedidosVerColor(){
		$emoji=$_POST['id'];
		$sql="SELECT *
              FROM pedido_modelo_color 
			  WHERE mcolo_codig ='".$emoji."'";
		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryRow();
   		$output = '<img src="'.Yii::app()->request->getBaseUrl(true).'/'.$data["mcolo_ruta"].'" class="img-responsive" />';  

   		echo $output;

  		
	}
	public function ActionPedidosVerFuente(){
		$emoji=$_POST['id'];
		$sql="SELECT *
              FROM pedido_fuente 
			  WHERE fuent_codig ='".$emoji."'";
		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryRow();
   		$output = '<img src="'.Yii::app()->request->getBaseUrl(true).'/'.$data["fuent_ruta"].'" class="img-responsive" />';  

   		echo $output;

  		
	}
	public function ActionPedidosVerImagenModelo(){
		$tmode=$_POST['tmode'];
		$model=$_POST['model'];
		$categ=$_POST['categ'];
		$sql="SELECT *
			  FROM pedido_modelo_conjunto a
			  WHERE a.tmode_codig ='".$tmode."'
			  	AND a.model_codig ='".$model."'
			  	AND a.mcate_codig ='".$categ."'";
		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryRow();

   		$output = '<img src="'.Yii::app()->request->getBaseUrl(true).'/'.$data["mconj_ruta"].'" class="img-responsive" />';  

   		echo $output;

  		
	}

	public function VerImagenPersona($p_persona){
		$sql="SELECT * 
            FROM pedido_detalle_listado_imagen a 
            JOIN solicitud_detalle_listado b ON (a.pdlis_codig = b.pdlis_codig)
            JOIN pedido_precio c ON (a.pdlim_timag = c.pprec_codig)
            LEFT JOIN pedido_emoji d ON (a.emoji_codig = d.emoji_codig)
            LEFT JOIN pedido_simbolo_simple e ON (a.ssimp_codig = e.ssimp_codig)
            WHERE a.pdlis_codig = '".$p_persona."'
              AND pdlim_timag <> '4'";
        
        $conexion = Yii::app()->db;
        $p_persona = $conexion->createCommand($sql)->query();
        $i=0;
        $j=10;
        while (($row = $p_persona->read()) !== false) {
            $i++;
            $j++;
            
            if($row['pdlim_timag']=='1'){
                $ruta=$row['emoji_ruta'];
            }else if($row['pdlim_timag']=='3'){
                $ruta=$row['ssimp_ruta'];
            }else{
                $ruta=$row['pdlim_rimag'];
            }
            if($ruta){
            	$output .= '<img src="'.Yii::app()->request->getBaseUrl(true).'/'.$ruta.'" height="30px"/>';
            }
              

        }                 

   		echo $output; 		
	}

	public function VerImagenPersonaBordar($p_persona){
		$sql="SELECT * 
            FROM pedido_detalle_listado_imagen a 
            JOIN solicitud_detalle_listado b ON (a.pdlis_codig = b.pdlis_codig)
            JOIN pedido_precio c ON (a.pdlim_timag = c.pprec_codig)
            LEFT JOIN pedido_emoji d ON (a.emoji_codig = d.emoji_codig)
            LEFT JOIN pedido_simbolo_simple e ON (a.ssimp_codig = e.ssimp_codig)
            WHERE a.pdlis_codig = '".$p_persona."'
              AND pdlim_timag <> '4'";
        
        $conexion = Yii::app()->db;
        $p_persona = $conexion->createCommand($sql)->query();
        $i=0;
        $j=10;
        while (($row = $p_persona->read()) !== false) {
            $i++;
            $j++;
            
            if($row['pdlim_timag']=='1'){
                $ruta=$row['emoji_ruta'];
            }else if($row['pdlim_timag']=='3'){
                $ruta=$row['ssimp_ruta'];
            }else{
                $ruta=$row['pdlim_rimag'];
            }
            if($ruta){
            	$output .= '<img src="'.Yii::app()->request->getBaseUrl(true).'/'.$ruta.'" height="125px"/>';
            }
              

        }                 

   		echo $output; 		
	}

	public function VerElectivos($pedidos){
		$sql="SELECT * FROM solicitud_imagen WHERE solic_codig='".$pedidos['solic_codig']."' and pimag_orden >=10";      
        $conexion = Yii::app()->db;
        $p_persona = $conexion->createCommand($sql)->query();
        $i=0;
        $j=10;
        while (($row = $p_persona->read()) !== false) {
            $i++;
            if($row['pimag_ruta']){
            	$output .= '<img src="'.Yii::app()->request->getBaseUrl(true).'/'.$row['pimag_ruta'].'" class="img-responsive" width="30px">';
            }
              

        }                 

   		return $output; 		
	}
	public function VerElectivosBordar($pedidos){
		$sql="SELECT * FROM solicitud_imagen WHERE solic_codig='".$pedidos['solic_codig']."' and pimag_orden >=10";      
        $conexion = Yii::app()->db;
        $p_persona = $conexion->createCommand($sql)->query();
        $i=0;
        $j=10;
        while (($row = $p_persona->read()) !== false) {
            $i++;
            if($row['pimag_ruta']){
            	$output .= '<img src="'.Yii::app()->request->getBaseUrl(true).'/'.$row['pimag_ruta'].'" class="img-responsive" width="125px">';
            }
              

        }                 

   		return $output; 		
	}
	// FIN MODULO DE PEDIDOS

	//MODULO DE MODELOS
	
	public function ActionModeloModelos(){
		$tmode=$_POST['tmode'];
		$sql="SELECT a.model_codig, a.model_descr
			  FROM pedido_modelo a ";

		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryAll();
   		$data=CHtml::listData($data,'model_codig','model_descr');
   		echo CHtml::tag('option', array('value'=>''), CHtml::encode('Seleccione...'), true);
  		foreach($data as $value=>$name){
   			echo CHtml::tag('option', array('value'=>$value), CHtml::encode($name), true);
  		}
	}
	public function ActionModeloCategoria(){
		$tmode=$_POST['tmode'];
		$model=$_POST['model'];
		$sql="SELECT a.mcate_codig, b.mcate_descr
			  FROM pedido_modelo_conjunto a
			  JOIN pedido_modelo_categoria b ON (a.mcate_codig = b.mcate_codig)
			  WHERE a.tmode_codig ='".$tmode."'
			  	AND a.model_codig ='".$model."'";
		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryAll();
   		$data=CHtml::listData($data,'mcate_codig','mcate_descr');
   		echo CHtml::tag('option', array('value'=>''), CHtml::encode('Seleccione...'), true);
  		foreach($data as $value=>$name){
   			echo CHtml::tag('option', array('value'=>$value), CHtml::encode($name), true);
  		}
	}
	public function ActionModeloVariante(){
		$tmode=$_POST['tmode'];
		$model=$_POST['model'];
		$categ=$_POST['categ'];
		$sql="SELECT a.mvari_codig, b.mvari_descr
			  FROM pedido_modelo_conjunto a
			  JOIN pedido_modelo_variante b ON (a.mvari_codig = b.mvari_codig)
			  WHERE a.tmode_codig ='".$tmode."'
			  	AND a.model_codig ='".$model."'
			  	AND a.mcate_codig ='".$categ."'";
		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryAll();
   		$data=CHtml::listData($data,'mvari_codig','mvari_descr');
   		echo CHtml::tag('option', array('value'=>''), CHtml::encode('Seleccione...'), true);
  		foreach($data as $value=>$name){
   			echo CHtml::tag('option', array('value'=>$value), CHtml::encode($name), true);
  		}
	}
	
	public function ActionModeloOpcionales(){
		$tmode=$_POST['tmode'];
		$model=$_POST['model'];
		$categ=$_POST['categ'];
		$varia=$_POST['varia'];
		$sql="SELECT a.mopci_codig, b.mopci_descr
			  FROM pedido_modelo_conjunto a
			  JOIN pedido_modelo_opcional b ON (a.mopci_codig = b.mopci_codig)
			  WHERE a.tmode_codig ='".$tmode."'
			  	AND a.model_codig ='".$model."'
			  	AND a.mcate_codig ='".$categ."'";
		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryAll();
   		$data=CHtml::listData($data,'mopci_codig','mopci_descr');
   		echo CHtml::tag('option', array('value'=>''), CHtml::encode('Seleccione...'), true);
  		foreach($data as $value=>$name){
   			echo CHtml::tag('option', array('value'=>$value), CHtml::encode($name), true);
  		}
	}
	
	public function CopiarEmoji($file,$ruta,$nombre){
		if (!file_exists($ruta)) {
		    mkdir($ruta, 0755, true);
		}
		if (move_uploaded_file($file['tmp_name'], $ruta.$nombre)){ 
      		$msg = array('success'=>'true','msg'=>"El archivo ha sido cargado correctamente."); 
	   	}else{ 
	      	$msg = array('success'=>'false','msg'=>"Ocurrió algún error al subir el fichero. No pudo guardarse."); 
	   	} 
	   	return $msg;
	}
	// FIN MODULO DE MODELOS
	// MODULOS PEDIDOS
	public function CalcularMontoPedido($pedidos){
		$conexion=Yii::app()->db;
		$sql="SELECT * FROM solicitud_detalle WHERE solic_codig = '".$pedidos['solic_codig']."'";
		$modelos = $conexion->createCommand($sql)->queryAll();	 	
		$i=0;
		$totales['modelos']=0;
		$totales['p_personas']=0;
		$totales['monto']=0;
		foreach ($modelos as $key => $modelo) {
			$i++;
			$totales['modelos']++;

			$sql = "SELECT * FROM solicitud_detalle a
	            		JOIN pedido_modelo_opcional b ON (a.mopci_codig = b.mopci_codig)
	            		WHERE pdeta_codig = '".$modelo['pdeta_codig']."'";
	        $mopci = $conexion->createCommand($sql)->queryRow();
	        $sql = "SELECT * FROM solicitud_detalle a
	            		JOIN pedido_modelo_opcional b ON (a.mopci_codi2 = b.mopci_codig)
	            		WHERE pdeta_codig = '".$modelo['pdeta_codig']."'";
	        $mopc2 = $conexion->createCommand($sql)->queryRow();
			$sql = "SELECT * FROM pedido_modelo_conjunto
	            		WHERE tmode_codig = '".$modelo['tmode_codig']."'
	            		  AND model_codig = '".$modelo['model_codig']."'
	            		  AND mcate_codig = '".$modelo['mcate_codig']."'
	            		  AND mvari_codig = '".$modelo['mvari_codig']."'";
	        $conjunto = $conexion->createCommand($sql)->queryRow();
	        
	        $sql="SELECT * FROM solicitud_imagen WHERE solic_codig='".$pedidos['solic_codig']."' and pimag_orden >=10";
			$elect=$conexion->createCommand($sql)->queryRow();
			if($elect){
				$electivo=1;
			}else{
				$electivo=0;
			}
$sql="SELECT * FROM pedido_precio_electivo WHERE pelec_codig='1'";
							

							$pelect=$conexion->createCommand($sql)->queryRow();
			$sql="SELECT * FROM solicitud_detalle_listado WHERE pdeta_codig='".$modelo['pdeta_codig']."'";
			$p_personas = $conexion->createCommand($sql)->queryAll();

			$j=0;	
			$total=0; 	
			foreach ($p_personas as $key => $p_persona) {
				$monto=0;
				$opcional=0;
				$opcional2=0;
				$montoPersona=0;
				$pmonto=0;
				$j++;
				$totales['p_personas']++;

				$total+=$conjunto['mconj_preci'];
				$total+=$mopci['mopci_preci'];
				$total+=$mopc2['mopci_preci'];
				$montoPersona=$conjunto['mconj_preci']+$mopci['mopci_preci']+$mopc2['mopci_preci'];

				$sql="SELECT * FROM pedido_detalle_listado_imagen WHERE pdlis_codig='".$p_persona['pdlis_codig']."'";
				$imagenes = $conexion->createCommand($sql)->queryAll();
				
				$a=0;
				$tipo=array();
				foreach ($imagenes as $key => $imagen) {
					$a++;
					$sql="SELECT * FROM pedido_precio WHERE pprec_codig = '".$imagen['pdlim_timag']."'";
					$precio=$conexion->createCommand($sql)->queryRow();

					//var_dump($precio);
					$tipo[$imagen['pdlim_timag']]++;
					if ($tipo[$imagen['pdlim_timag']]>1) {
						$total+=$precio['pprec_adici'];
						$montoPersona+=$precio['pprec_adici'];
						$adici=$precio['pprec_adici'];
					}else{
						$total+=$precio['pprec_preci'];
						$montoPersona+=$precio['pprec_preci'];	
						$adici=$precio['pprec_preci'];
					}
					
					if($a>1){
						$adicional.='+';
					}
					$adicional.=$this->transformarMonto_v($adici,2);
				}
				if($electivo=='1'){
					$e++;
					$electivos.=$this->transformarMonto_v($pelect['pelec_preci'],0);
					$montoPersona+=$pelect['pelec_preci'];
					$total+=$pelect['pelec_preci'];
				}
				
				$monto=$this->transformarMonto_v($conjunto['mconj_preci'],2);
	    		$opcional=$this->transformarMonto_v($mopci['mopci_preci'],2);

	    		$opcional2=$this->transformarMonto_v($mopc2['mopci_preci'],2);
				$montoPersona=$this->transformarMonto_v($montoPersona,2);

			}

			$totales['monto']+=$total;


		}
		$sql = "SELECT *
                FROM solicitud_adicional a
                JOIN pedido_costo b ON (a.costo_codig = b.costo_codig)
                WHERE a.solic_codig='".$pedidos['solic_codig']."'";
		$result=$conexion->createCommand($sql)->queryAll();
		$a=1;
		$totales['costos']=0;
		foreach ($result as $key => $value) {
			
			$totales['monto']+=$value['adici_monto'];
			$totales['costos']+=$value['adici_monto'];
			$a++;
		}

		$sql = "SELECT *
                FROM solicitud_deduccion a
                JOIN pedido_deduccion b ON (a.deduc_codig = b.deduc_codig)
                WHERE a.solic_codig='".$pedidos['solic_codig']."'";
		$result=$conexion->createCommand($sql)->queryAll();
		$a=1;
		$totales['deduc']=0;
		foreach ($result as $key => $value) {
			
			$totales['monto']-=$value['pdedu_monto'];
			$totales['deduc']+=$value['pdedu_monto'];
			$a++;
		}

		$sql="SELECT * FROM solicitud_imagen WHERE solic_codig='".$pedidos['solic_codig']."' and pimag_orden >=10";
		$result=$conexion->createCommand($sql)->queryAll();
		$a=1;
		$totales['electivo']=0;
		foreach ($result as $key => $value) {
			if($value['pimag_tbord']!='' and $value['pimag_ruta']!=''){
				$preci=3;
			}else if($value['pimag_tbord']!='' and $value['pimag_ruta']==''){
				$preci=2;
			}else if($value['pimag_tbord']=='' and $value['pimag_ruta']!=''){
				$preci=1;
			}
			$sql="SELECT * FROM pedido_precio_electivo where pelec_codig='".$preci."'";
			$precio=$conexion->createCommand($sql)->queryRow();
			
			$totales['monto']+=$precio['pelec_preci'];
			$totales['electivo']+=$precio['pelec_preci'];

			$a++;
		}
	   	return $totales;
	}
	public function actionDescargar(){
		$a=$_GET['a'];
		$nombre=explode('/', $a);
		$nombre=end($nombre);
		header ("Content-Disposition: attachment; filename=".$nombre);
		header ("Content-Type: image/gif");
		header ("Content-Length: ".filesize($a));
		readfile($a);
	}
	public function VerAjusteTalla($ajustes){
		$i=0;
		$grupo='';
		$conexion=Yii::app()->db;
        foreach ($ajustes as $key => $ajuste) {
        	$i++;
            $sql="SELECT a.*, b.tajus_descr grupo 
                  FROM pedido_talla_ajuste a 
                  JOIN pedido_talla_ajuste b ON (a.tajus_padre = b.tajus_codig) 
                  WHERE a.tajus_codig='".$ajuste."'";
            $result= $conexion->createCommand($sql)->queryRow();
            
            if($i>1){
            	$texto.=', ';
            }
            if($grupo!=$result['grupo']){
            	$grupo=$result['grupo'];
            	$texto.='<b>'.$grupo.':</b> ';
            }
            $texto.=$result['tajus_descr'];
        }
        return $texto;
	}
	public function VerOpcionales($opcionales){
		$i=0;
		$grupo='';
		$conexion=Yii::app()->db;
        foreach ($opcionales as $key => $opcional) {
        	$i++;
            $sql="SELECT a.*
                  FROM pedido_modelo_opcional a 
                  WHERE a.mopci_codig='".$opcional."'";
            $result= $conexion->createCommand($sql)->queryRow();

            if($i>1){
            	$texto.=', ';
            }
            $texto.=$result['mopci_descr'];
        }
        return $texto;
	}
	public function VerQueIncluye($opcionales){
		$i=0;
		$grupo='';
		$conexion=Yii::app()->db;
        foreach ($opcionales as $key => $opcional) {
        	$i++;
            $sql="SELECT a.*
                  FROM pedido_incluye a 
                  WHERE a.inclu_codig='".$opcional."'";
            $result= $conexion->createCommand($sql)->queryRow();

            if($i>1){
            	$texto.=', ';
            }
            $texto.=$result['inclu_descr'];
        }
        return $texto;
	}
	public function AdicionalPersonas($c,$d)
	{
		$conexion=Yii::app()->db;
		$sql="SELECT b.*
			  FROM solicitud_detalle  a
			  JOIN pedido_modelo_conjunto b ON (a.tmode_codig = b.tmode_codig AND a.model_codig = b.model_codig AND a.mcate_codig = b.mcate_codig)
			  WHERE a.solic_codig ='".$c."' 
			    AND a.pdeta_codig ='".$d."'";

			$conjuntos=$conexion->createCommand($sql)->queryAll(); 
			$incluyen='';
			foreach ($conjuntos as $key => $value) {
				if($incluyen==''){
					$incluyen=json_decode($value['mconj_inclu']);
				}else{
					if(json_decode($value['mconj_inclu'])!=null){
						$incluyen=array_merge($incluyen,json_decode($value['mconj_inclu']));	
					}
					
				}
			}
			$sql="SELECT b.*
			  FROM solicitud_detalle  a
			  JOIN pedido_modelo_opcional b ON (a.mopci_codig = b.mopci_codig)
			  WHERE a.solic_codig ='".$c."' 
			    AND a.pdeta_codig ='".$d."'";

			$opcionales=$conexion->createCommand($sql)->queryAll(); 
			foreach ($opcionales as $key => $value) {
				if($incluyen==''){
					$incluyen=json_decode($value['mopci_inclu']);
				}else{
					if(json_decode($value['mopci_inclu'])!=null){
						$incluyen=array_merge($incluyen,json_decode($value['mopci_inclu']));	
					}
					
				}
			}
			$sql="SELECT b.*
			  FROM solicitud_detalle  a
			  JOIN pedido_modelo_opcional b ON (a.mopci_codi2 = b.mopci_codig)
			  WHERE a.solic_codig ='".$c."' 
			    AND a.pdeta_codig ='".$d."'";

			$opcionales=$conexion->createCommand($sql)->queryAll(); 
			foreach ($opcionales as $key => $value) {
				if($incluyen==''){
					$incluyen=json_decode($value['mopci_inclu']);
				}else{
					if(json_decode($value['mopci_inclu'])!=null){
						$incluyen=array_merge($incluyen,json_decode($value['mopci_inclu']));	
					}
					
				}
			}
		return $incluyen;	
	}
	public function AdicionalColor($p,$i)
	{
		$conexion=Yii::app()->db;
		$sql="SELECT *
			  FROM solicitud
			  WHERE solic_codig ='".$p."'";
		$pedido=$conexion->createCommand($sql)->queryRow(); 
		
		switch ($i) {
			case '1':
				$sql="SELECT * FROM pedido_modelo_color WHERE mcolo_codig='".$pedido['solic_ccier']."'";
				$color=$conexion->createCommand($sql)->queryRow();
				$descripcion=$color['mcolo_numer'].' - '.$color['mcolo_descr'];
				break;
			case '2':
				//Color Exterior
				$sql="SELECT * FROM pedido_modelo_color WHERE mcolo_codig='".$pedido['solic_cegor']."'";
				$color=$conexion->createCommand($sql)->queryRow();
				$descripcion='<b>EXTERIROR: </b>'.$color['mcolo_numer'].' - '.$color['mcolo_descr'];

				//Color Interior
				$sql="SELECT * FROM pedido_modelo_color WHERE mcolo_codig='".$pedido['solic_cigor']."'";
				$color=$conexion->createCommand($sql)->queryRow();
				$descripcion.=' / <b>INTERIOR: </b>'.$color['mcolo_numer'].' - '.$color['mcolo_descr'];
				
				break;
			case '3':
				//Color Fondo
				$sql="SELECT * FROM pedido_modelo_color WHERE mcolo_codig='".$pedido['solic_cppre']."'";
				$color=$conexion->createCommand($sql)->queryRow();
				$descripcion='<b>FONDO: </b>'.$color['mcolo_numer'].' - '.$color['mcolo_descr'];

				//Color Lineas
				$sql="SELECT * FROM pedido_modelo_color WHERE mcolo_codig='".$pedido['solic_cpprl']."'";
				$color=$conexion->createCommand($sql)->queryRow();
				$descripcion.=' / <b>LINEAS: </b>'.$color['mcolo_numer'].' - '.$color['mcolo_descr'];
				
				break;
			case '4':
				$broche=array('1'=>'SIN BROCHE','2'=>'CRUDO','3'=>'GRIS','4'=>'BLANCO');
				$descripcion=$broche[$pedido['solic_broch']];
				break;
			case '5':
				$sql="SELECT * FROM pedido_modelo_color WHERE mcolo_codig='".$pedido['solic_cvivo']."'";
				$color=$conexion->createCommand($sql)->queryRow();
				$descripcion=$color['mcolo_numer'].' - '.$color['mcolo_descr'];
				break;	
			default:
				$descripcion='N/D';
				break;
		}
			
		return $descripcion;	
	}
	// FIN MODULO PEDIDOS
	// PARAMETROS
	public function ActionComboEstado(){
		$paise=$_POST['paise'];
		$sql="SELECT a.estad_codig, a.estad_descr
			  FROM p_estados a
			  WHERE a.paise_codig ='".$paise."'";
			  //echo $sql;

		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryAll();
   		$data=CHtml::listData($data,'estad_codig','estad_descr');
   		echo CHtml::tag('option', array('value'=>''), CHtml::encode('Seleccione...'), true);
  		foreach($data as $value=>$name){
   			echo CHtml::tag('option', array('value'=>$value), CHtml::encode($name), true);
  		}
	}
	public function ActionComboMunicipios(){
		$paise=$_POST['paise'];
		$estad=$_POST['estad'];
		$sql="SELECT a.munic_codig, a.munic_descr
			  FROM p_municipio a
			  WHERE a.estad_codig ='".$estad."'";
			  //echo $sql;

		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryAll();
   		$data=CHtml::listData($data,'munic_codig','munic_descr');
   		echo CHtml::tag('option', array('value'=>''), CHtml::encode('Seleccione...'), true);
  		foreach($data as $value=>$name){
   			echo CHtml::tag('option', array('value'=>$value), CHtml::encode($name), true);
  		}
	}
	public function ActionComboParroquias(){
		$paise=$_POST['paise'];
		$estad=$_POST['estad'];
		$munic=$_POST['munic'];
		$sql="SELECT a.parro_codig, a.parro_descr
			  FROM p_parroquia a
			  WHERE a.estad_codig ='".$estad."'
			    AND a.munic_codig ='".$munic."'";
			  //echo $sql;

		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryAll();
   		$data=CHtml::listData($data,'parro_codig','parro_descr');
   		echo CHtml::tag('option', array('value'=>''), CHtml::encode('Seleccione...'), true);
  		foreach($data as $value=>$name){
   			echo CHtml::tag('option', array('value'=>$value), CHtml::encode($name), true);
  		}
	}

	public function ActionComboCiudades(){
		$paise=$_POST['paise'];
		$estad=$_POST['estad'];
		$sql="SELECT a.ciuda_codig, a.ciuda_descr
			  FROM p_ciudades a
			  WHERE a.estad_codig ='".$estad."'";
			  //echo $sql;

		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryAll();
   		$data=CHtml::listData($data,'ciuda_codig','ciuda_descr');
   		echo CHtml::tag('option', array('value'=>''), CHtml::encode('Seleccione...'), true);
  		foreach($data as $value=>$name){
   			echo CHtml::tag('option', array('value'=>$value), CHtml::encode($name), true);
  		}
	}

	public function openCypher($action='encrypt',$string=false)
	{
	    $action = trim($action);
	    $output = false;

	    $myKey = '2x.-#PÂUX75&a/j%CpV]EÂe6Ls?ZfQW8!¡DoT9NIOJ}3[ucv(iM+dwKht{|SRnk)Az!Gq41mlgFBHr¿yYb';
	    $myIV = 'hJ(i5s7N?ÂXKE4l3cq-¡8I¿fP6GFTy%arDo1R!O/VnjLZ.+g[Q|d]BSMv!}kt)CHU9{e#mAwYÂbp2zW&ux';
                    
	    $encrypt_method = 'AES-256-CBC';

	    $secret_key = hash('sha256',$myKey);
	    $secret_iv = substr(hash('sha256',$myIV),0,16);

	    if ( $action && ($action == 'encrypt' || $action == 'decrypt') && $string )
	    {
	        $string = trim(strval($string));

	        if ( $action == 'encrypt' )
	        {
	            $output = openssl_encrypt($string, $encrypt_method, $secret_key, 0, $secret_iv);
	        };

	        if ( $action == 'decrypt' )
	        {
	            $output = openssl_decrypt($string, $encrypt_method, $secret_key, 0, $secret_iv);
	        };
	    };

	    return $output;
	}
	public function CardenaAleatoria()
	{
		$caracteres = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-.#!%&/()?¡¿!|+{}[]';
        
        $cadena=utf8_encode(str_shuffle($caracteres));
        
        return $cadena;
	}
	public function guardarTraza($conexion, $opera, $tabla, $antes,$actual,$query)
	{
		$usuar=Yii::app()->user->id['usuario']['codigo'];
		$fecha=date('Y-m-d');
		$hora=date('H:i:s');
		/*$antes=$this->openCypher('encrypt',$antes);
		$actual=$this->openCypher('encrypt',$actual);*/
		$consulta=$this->openCypher('encrypt',$query);
			
		$sql = "INSERT INTO seguridad_traza(traza_opera, traza_tabla, traza_antes, traza_actua, traza_query, usuar_codig, traza_fcrea, traza_hcrea) 
				VALUES ('".$opera."', '".$tabla."', '".$antes."', '".$actual."', '".$consulta."', '".$usuar."', '".$fecha."', '".$hora."')";
        $res1=$conexion->createCommand($sql)->execute();
		if($res1){
			$msg=array('success'=>'true','msg'=>'Traza guardada correctamente');	
		}else{
		
			$msg=array('success'=>'false','msg'=>'Error al guardar la traza');	
		}
        
        
        return $msg;
	}
	public function guardarTrazaApi($conexion, $opera, $tabla, $antes,$actual,$query,$usuar)
	{
		$fecha=date('Y-m-d');
		$hora=date('H:i:s');
		/*$antes=$this->openCypher('encrypt',$antes);
		$actual=$this->openCypher('encrypt',$actual);*/
		$consulta=$this->openCypher('encrypt',$query);
			
		$sql = "INSERT INTO seguridad_traza(traza_opera, traza_tabla, traza_antes, traza_actua, traza_query, usuar_codig, traza_fcrea, traza_hcrea) 
				VALUES ('".$opera."', '".$tabla."', '".$antes."', '".$actual."', '".$consulta."', '".$usuar."', '".$fecha."', '".$hora."')";
        $res1=$conexion->createCommand($sql)->execute();
		if($res1){
			$msg=array('success'=>'true','msg'=>'Traza guardada correctamente');	
		}else{
		
			$msg=array('success'=>'false','msg'=>'Error al guardar la traza');	
		}
        
        
        return $msg;
	}

	public function guardarTrayectoria($conexion, $solicitud, $estatus_e, $estatus_s, $accion)
	{
		$usuar=Yii::app()->user->id['usuario']['codigo'];
		$fecha=date('Y-m-d');
		$hora=date('H:i:s');
		
		if($accion=="2" or $accion=="3"){
			$sql="SELECT * 
				  FROM solicitud_trayectoria 
				  WHERE prere_codig='".$solicitud."'
				    AND estat_codig='".$estatus_s."'";
			$solicitudes=$conexion->createCommand($sql)->queryRow();
			
			$inicio=$solicitudes['traye_finic'].' '.$solicitudes['traye_hinic'];
			$fin=$fecha.' '.$hora;
			$diff=json_encode($this->diferenciaHoras($inicio, $fin));
			
			$sql="UPDATE solicitud_trayectoria 
				  SET traye_ffina = '".$fecha."',
					  traye_hfina = '".$hora."',
					  traye_difer = '".$diff."'
				  WHERE prere_codig='".$solicitud."'
				    AND estat_codig='".$estatus_s."'";
			$res1=$conexion->createCommand($sql)->execute();

		}
		if($accion=="3"){
			$sql = "INSERT INTO solicitud_trayectoria(solic_codig, estat_codig, traye_finic, traye_hinic, traye_ffina, traye_hfina, usuar_codig, traye_fcrea, traye_hcrea) 
			VALUES ('".$solicitud."', '".$estatus_e."', '".$fecha."', '".$hora."', '".$fecha."', '".$hora."', '".$usuar."', '".$fecha."', '".$hora."')";

		}
		$sql = "INSERT INTO solicitud_trayectoria(prere_codig, estat_codig, traye_finic, traye_hinic, usuar_codig, traye_fcrea, traye_hcrea) 
			VALUES ('".$solicitud."', '".$estatus_e."', '".$fecha."', '".$hora."', '".$usuar."', '".$fecha."', '".$hora."')";
		$res1=$conexion->createCommand($sql)->execute();
		
		if($res1){
			$msg=array('success'=>'true','msg'=>'Trayectoria guardada correctamente');	
		}else{
		
			$msg=array('success'=>'false','msg'=>'Error al guardar la Trayectoria');	
		}
        
        return $msg;
	}
	public function guardarTrayectoriaApi($conexion, $solicitud, $estatus_e, $estatus_s, $accion, $usuar)
	{
		$fecha=date('Y-m-d');
		$hora=date('H:i:s');
		
		if($accion=="2" or $accion=="3"){
			$sql="SELECT * 
				  FROM solicitud_trayectoria 
				  WHERE prere_codig='".$solicitud."'
				    AND estat_codig='".$estatus_s."'";
			$solicitudes=$conexion->createCommand($sql)->queryRow();
			
			$inicio=$solicitudes['traye_finic'].' '.$solicitudes['traye_hinic'];
			$fin=$fecha.' '.$hora;
			$diff=json_encode($this->diferenciaHoras($inicio, $fin));
			
			$sql="UPDATE solicitud_trayectoria 
				  SET traye_ffina = '".$fecha."',
					  traye_hfina = '".$hora."',
					  traye_difer = '".$diff."'
				  WHERE prere_codig='".$solicitud."'
				    AND estat_codig='".$estatus_s."'";
			$res1=$conexion->createCommand($sql)->execute();

		}
		if($accion=="3"){
			$sql = "INSERT INTO solicitud_trayectoria(solic_codig, estat_codig, traye_finic, traye_hinic, traye_ffina, traye_hfina, usuar_codig, traye_fcrea, traye_hcrea) 
			VALUES ('".$solicitud."', '".$estatus_e."', '".$fecha."', '".$hora."', '".$fecha."', '".$hora."', '".$usuar."', '".$fecha."', '".$hora."')";

		}
		$sql = "INSERT INTO solicitud_trayectoria(prere_codig, estat_codig, traye_finic, traye_hinic, usuar_codig, traye_fcrea, traye_hcrea) 
			VALUES ('".$solicitud."', '".$estatus_e."', '".$fecha."', '".$hora."', '".$usuar."', '".$fecha."', '".$hora."')";
		$res1=$conexion->createCommand($sql)->execute();
		
		if($res1){
			$msg=array('success'=>'true','msg'=>'Trayectoria guardada correctamente');	
		}else{
		
			$msg=array('success'=>'false','msg'=>'Error al guardar la Trayectoria');	
		}
        
        return $msg;
	}
	public function guardarTrayectoriaSolicitudes($conexion, $solicitud, $estatus_e, $estatus_s, $accion)
	{

		$usuar=Yii::app()->user->id['usuario']['codigo'];
		$fecha=date('Y-m-d');
		$hora=date('H:i:s');
		
		if($accion=="2" or $accion=="3"){
			$sql="SELECT * 
				  FROM solicitud_trayectoria 
				  WHERE solic_codig='".$solicitud."'
				    AND estat_codig='".$estatus_s."'";
			$solicitudes=$conexion->createCommand($sql)->queryRow();
			
			$inicio=$solicitudes['traye_finic'].' '.$solicitudes['traye_hinic'];
			$fin=$fecha.' '.$hora;
			$diff=json_encode($this->diferenciaHoras($inicio, $fin));
			
			$sql="UPDATE solicitud_trayectoria 
				  SET traye_ffina = '".$fecha."',
					  traye_hfina = '".$hora."',
					  traye_difer = '".$diff."'
				  WHERE solic_codig='".$solicitud."'
				    AND estat_codig='".$estatus_s."'";
			$res1=$conexion->createCommand($sql)->execute();

		}
		$sql = "INSERT INTO solicitud_trayectoria(solic_codig, estat_codig, traye_finic, traye_hinic, usuar_codig, traye_fcrea, traye_hcrea) 
			VALUES ('".$solicitud."', '".$estatus_e."', '".$fecha."', '".$hora."', '".$usuar."', '".$fecha."', '".$hora."')";

		if($accion=="3"){
			$sql = "INSERT INTO solicitud_trayectoria(solic_codig, estat_codig, traye_finic, traye_hinic, traye_ffina, traye_hfina, usuar_codig, traye_fcrea, traye_hcrea) 
			VALUES ('".$solicitud."', '".$estatus_e."', '".$fecha."', '".$hora."', '".$fecha."', '".$hora."', '".$usuar."', '".$fecha."', '".$hora."')";

		}
		
		$res1=$conexion->createCommand($sql)->execute();
		
		if($res1){
			$msg=array('success'=>'true','msg'=>'Trayectoria guardada correctamente');	
		}else{
		
			$msg=array('success'=>'false','msg'=>'Error al guardar la Trayectoria');	
		}
        
        return $msg;
	}

	public function guardarMovimiento($conexion, $solicitud, $estatus, $accion, $motiv, $obser)
	{
		$usuar=Yii::app()->user->id['usuario']['codigo'];
		$fecha=date('Y-m-d');
		$hora=date('H:i:s');
		
		$sql = "INSERT INTO rd_preregistro_movimiento(prere_codig, estat_codig, accio_codig, motiv_codig, prmov_obser, usuar_codig, prmov_fcrea, prmov_hcrea)
			VALUES ('".$solicitud."', '".$estatus."', '".$accion."', '".$motiv."', '".$obser."', '".$usuar."', '".$fecha."', '".$hora."')";
		$res1=$conexion->createCommand($sql)->execute();
		
		if($res1){
			$msg=array('success'=>'true','msg'=>'Trayectoria guardada correctamente');	
		}else{
		
			$msg=array('success'=>'false','msg'=>'Error al guardar la Trayectoria');	
		}
        
        return $msg;
	}
	public function guardarMovimientoApi($conexion, $solicitud, $estatus, $accion, $motiv, $obser,$usuar)
	{
		$fecha=date('Y-m-d');
		$hora=date('H:i:s');
		
		$sql = "INSERT INTO rd_preregistro_movimiento(prere_codig, estat_codig, accio_codig, motiv_codig, prmov_obser, usuar_codig, prmov_fcrea, prmov_hcrea)
			VALUES ('".$solicitud."', '".$estatus."', '".$accion."', '".$motiv."', '".$obser."', '".$usuar."', '".$fecha."', '".$hora."')";
		$res1=$conexion->createCommand($sql)->execute();
		
		if($res1){
			$msg=array('success'=>'true','msg'=>'Trayectoria guardada correctamente');	
		}else{
		
			$msg=array('success'=>'false','msg'=>'Error al guardar la Trayectoria');	
		}
        
        return $msg;
	}

	public function guardarReasignacion($conexion, $solicitud, $estatus, $accion, $motiv, $obser)
	{
		$usuar=Yii::app()->user->id['usuario']['codigo'];
		$fecha=date('Y-m-d');
		$hora=date('H:i:s');

		$sql = "INSERT INTO solicitud_reasignacion(solic_codig, accio_codig, motiv_codig, reasi_obser, reasi_estat, usuar_codig, reasi_fcrea, reasi_hcrea)
			VALUES ('".$solicitud."', '".$accion."', '".$motiv."', '".$obser."', '".$estatus."', '".$usuar."', '".$fecha."', '".$hora."')";
		$res1=$conexion->createCommand($sql)->execute();
		
		$sql="SELECT * FROM solicitud_reasignacion WHERE reasi_codig = (SELECT max(reasi_codig) FROM solicitud_reasignacion)";
		$reasignacion=$conexion->createCommand($sql)->execute();

		$traza=$this->guardarTraza($conexion, 'I', 'solicitud_reasignacion', '',json_encode($reasignacion),$sql);

		if($res1){
			$msg=array('success'=>'true','msg'=>'Reasignacion guardada correctamente');	
		}else{
		
			$msg=array('success'=>'false','msg'=>'Error al guardar la Trayectoria');	
		}
        
        return $msg;
	}
	public function RegistroApiSigo($ruta,$post) {
        //Lo primerito, creamos una variable iniciando curl, pasándole la url
        $rute=Yii::app()->params['api'];
        
        $ch = curl_init($rute.$ruta);
         
        //especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
        curl_setopt ($ch, CURLOPT_POST, 1);
         
        //le decimos qué paramáetros enviamos (pares nombre/valor, también acepta un array)
        curl_setopt ($ch, CURLOPT_POSTFIELDS, $post);
         
        //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
         
        //recogemos la respuesta
        $respuesta = curl_exec ($ch);
        //o el error, por si falla
        $error = curl_error($ch);
        //y finalmente cerramos curl
        curl_close ($ch);

        if($respuesta=='' or !$respuesta ){
        	$respuesta=array('success'=>'false','msg'=>'Sin respuesta del API de AUTOGESTION');
        	echo json_encode($respuesta);
        	exit();
        }
        if($error!='' or $error){
        	$respuesta=array('success'=>'false','msg'=>'Error en el API de AUTOGESTION','error'=>$error);
        	echo json_encode($respuesta);
        	exit();
        }
        return json_decode($respuesta, true);
    } 
    public function enviarVportal($clien){
		$conexion=Yii::app()->db;
		
		$create=$this->ApiVportalCreate($clien);
		$post='{
				"type" : "merchant",
				"name" : "Comercio de Prueba 3",
				"fantasyName" : "Comercio de Prueba 3",
			 	"rif" : "J-123456789",
				"industryTypeId" : 117,
				"location" : {
				    "address" : {
				      "municipalityId" : 462,
				      "stateId" : 24,
				      "name" : "Comercio de Prueba 3",
				      "property" : "China",
				      "location" : "China",
				      "postalZoneId" : 1522,
				      "cityId" : 137,
				      "countryId" : 1,
				      "parishId" : 1124
					},
				    "contact" : {
				      "name" : "Emily",
				      "phone2" : "02121234567",
				      "email" : "correo@correo.com",
				      "lastname" : "Corro",
				      "phone1" : "02121234567"
					},
					"phone" : "02121234567",
				    "email" : "correo@correo.com"
				}
			}';
		$msg=array('success' => 'true' ,'msg' =>'Cliente Creado Correctamente');
		$api['create']=$this->ApiVportal($create['ruta'],$create['post']);
		if($api['create']['error']){
			$msg=array('success' => 'false' ,'msg' =>'Error al crear el Comercio en Vportal', 'api' => $create);
		}else{
			$affiliation=$this->ApiVportalAffiliation($api['create']['id']);
			$api['affiliation']=$this->ApiVportal($affiliation['ruta'],$affiliation['post']);
			if($api['affiliation']['error']){
				$msg=array('success' => 'false' ,'msg' =>'Error al crear el terminal en Vportal');
			}else{
				$acquirer=$this->ApiVportalAcquirer($api,$clien);
				$api['acquirer']=$this->ApiVportal($acquirer['ruta'],$acquirer['post']);
				if($api['acquirer']['error']){
					$msg=array('success' => 'false' ,'msg' =>'Error al crear el terminal en Vportal');
				}else{
					$terminal=$this->ApiVportalTerminal($api,$clien);
					$api['terminal']=$this->ApiVportal($terminal['ruta'],$terminal['post']);
					if($api['terminal']['error']){
						$msg=array('success' => 'false' ,'msg' =>'Error al crear el terminal en Vportal');
					}	
				}
				
			}
		}
		
		return $msg;
		
	}
    public function ApiVportal($ruta,$post) {
        //Lo primerito, creamos una variable iniciando curl, pasándole la url
        
        $rute=Yii::app()->params['vportal'];
        
        $ch = curl_init($rute.$ruta);
        curl_setopt($ch, CURLOPT_PORT, 5054);
        curl_setopt($ch, CURLOPT_USERPWD, 'portalsigo' . ":" . 'PortalSIGO.RP1');  
		curl_setopt($ch, CURLOPT_HTTPHEADER,
		    array(
		        'Content-Type:application/json',
		        'Content-Type:application/x-www-form-urlencoded',
		        'Content-Length: ' . strlen($post)
		    )
		);
        //especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
        curl_setopt ($ch, CURLOPT_POST, 1);
         
        //le decimos qué paramáetros enviamos (pares nombre/valor, también acepta un array)
        curl_setopt ($ch, CURLOPT_POSTFIELDS, $post);
         
        //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
         
        //recogemos la respuesta
        $respuesta = curl_exec ($ch);
        //var_dump($respuesta);
        //o el error, por si falla
        $error = curl_error($ch);
        //var_dump($error);
        //y finalmente cerramos curl
        curl_close ($ch);

        if($respuesta=='' or !$respuesta ){
        	$respuesta=array('success'=>'false','msg'=>'Sin respuesta del API de Vportal');
        	echo json_encode($respuesta);
        	exit();
        }
        if($error!='' or $error){
        	$respuesta=array('success'=>'false','msg'=>'Error en el API de Vportal','error'=>$error);
        	echo json_encode($respuesta);
        	exit();
        }
        return json_decode($respuesta, true);
    } 
    public function ApiVportalCreate($clien) {
        $conexion=Yii::app()->db;
        $ruta='merchant/create';

		$sql="SELECT * FROM cliente WHERE clien_codig='".$clien."'";
		$cliente=$conexion->createCommand($sql)->queryRow();

		$sql="SELECT * FROM cliente_direccion WHERE clien_codig='".$cliente['clien_codig']."'";
		$direccion=$conexion->createCommand($sql)->queryRow();

		$sql="SELECT * FROM p_paises a
			  JOIN p_estados b ON (a.paise_codig = b.paise_codig)
			  WHERE b.estad_codig='".$direccion['estad_codig']."'";
		$pais=$conexion->createCommand($sql)->queryRow();
		
		$sql="SELECT * FROM cliente_representante WHERE clien_codig='".$cliente['clien_codig']."'";
		$rlega=$conexion->createCommand($sql)->queryRow();
		
		$rif=substr($cliente['clien_rifco'],0,1).'-'.substr($cliente['clien_rifco'], 1);

		$comercio['type']='merchant';
		$comercio['name']=$cliente['clien_rsoci'];
		$comercio['fantasyName']=$cliente['clien_nfant'];
		$comercio['rif']=$rif;
		$comercio['industryTypeId']=$cliente['acome_codig'];

		//Direccion del Comercio
		$address['municipalityId']=$direccion['munic_codig'];
		$address['stateId']=$direccion['estad_codig'];
		$address['name']=$cliente['clien_rsoci'];
		$address['property']=$pais['paise_descr'];
		$address['location']=$pais['paise_descr'];
		$address['postalZoneId']=$direccion['direc_zpost'];
		$address['cityId']=$direccion['ciuda_codig'];
		$address['countryId']=$pais['paise_codig'];
		$address['parishId']=$direccion['parro_codig'];
		
		//Datos de Contacto
		$contact['name']=$rlega['rlega_nombr'];
		$contact['phone2']=$this->TransformarTelefono_bd($rlega['rlega_tmovi']);
		$contact['email']=$rlega['rlega_corre'];
		$contact['lastname']=$rlega['rlega_apell'];
		$contact['phone1']=$this->TransformarTelefono_bd($rlega['rlega_tfijo']);

		$comercio['location']['address']=$address;
		$comercio['location']['contact']=$contact;
		$comercio['location']['phone']=$this->TransformarTelefono_bd($cliente['clien_tmovi']);
		$comercio['location']['email']=$cliente['clien_celec'];
		
		var_dump(json_encode($comercio));

		$post = json_encode($comercio);
		exit();
		$return['ruta']=$ruta;
		$return['post']=$post;

		return $return;
    } 
    public function ApiVportalAffiliation($codig) {
        $conexion=Yii::app()->db;
        $ruta='merchant/affiliation/create';

		$comercio['type']='affiliation';
		//Solicitar el cambio
		$comercio['merchantId']=$codig;
		//Solicitar el api de consulta
		$comercio['paymentChannelId']='4';
		
		$post = json_encode($comercio);
		
		$return['ruta']=$ruta;
		$return['post']=$post;

		return $return;
    } 
    public function ApiVportalAcquirer($api,$codig) {
        $conexion=Yii::app()->db;
        $ruta='merchat/acquirer/create';

        $sql="SELECT * FROM cliente WHERE clien_codig='".$codig."'";
		$cliente=$conexion->createCommand($sql)->queryRow();

		$sql="SELECT * FROM solicitud WHERE clien_codig='".$cliente['clien_codig']."' order by solic_codig desc";
		$solicitud=$conexion->createCommand($sql)->queryRow();
		
		$comercio['type']='acquirer';
		//Solicitar el cambio
		$comercio['merchantIdCode']=$solicitud['solic_cafil'];
		$comercio['affiliationId']=$api['affiliation']['id'];
		//Solicitar el api de consulta
		$comercio['bankId']='26';
		//$comercio['bankId']=$solictud['banco_codig'];
		$post = json_encode($comercio);
		
		$return['ruta']=$ruta;
		$return['post']=$post;

		return $return;
    }
    public function ApiVportalTerminal($api,$codig) {
        $conexion=Yii::app()->db;
        $ruta='merchant/terminal/create/'.$api['acquirer']['id'].'/'.$api['affiliation']['paymentChannelId'];

        $sql="SELECT * FROM cliente WHERE clien_codig='".$codig."'";
		$cliente=$conexion->createCommand($sql)->queryRow();

		$sql="SELECT * FROM solicitud WHERE clien_codig='".$cliente['clien_codig']."' order by solic_codig desc";
		$solicitud=$conexion->createCommand($sql)->queryRow();

		$sql="SELECT SUM(cequi_canti) cantidad FROM solicitud_equipos WHERE solic_codig='".$solicitud['solic_codig']."'group by solic_codig";
		$equipos=$conexion->createCommand($sql)->queryRow();
		
		$comercio['type']='terminal';
		//Solicitar el cambio
		$comercio['affiliationId']=$api['affiliation']['id'];
		//Solicitar el api de consulta
		$comercio['cantTerminal']=$equipos['cantidad'];
		//$comercio['bankId']=$solictud['banco_codig'];
		$post = json_encode($comercio);
		
		$return['ruta']=$ruta;
		$return['post']=$post;

		return $return;
    } 
	public function diferenciaHoras($inicio, $fin)
	{
		//date_default_timezone_set('America/Caracas');
		$date1 = new DateTime($inicio);
		$date2 = new DateTime($fin);
		$diff = $date1->diff($date2);

        return $diff;
	}
	public function DateIntervalToSec($start,$end){ // as datetime object returns difference in seconds
	    $diff = $end->diff($start);
	    $diff_sec = $diff->format('%r').( // prepend the sign - if negative, change it to R if you want the +, too
	                ($diff->s)+ // seconds (no errors)
	                (60*($diff->i))+ // minutes (no errors)
	                (60*60*($diff->h))+ // hours (no errors)
	                (24*60*60*($diff->d))+ // days (no errors)
	                (30*24*60*60*($diff->m))+ // months (???)
	                (365*24*60*60*($diff->y)) // years (???)
	                );
	    return $diff_sec;
	}
	public function DiffToSec($diff){ // as datetime object returns difference in seconds
	    $diff_sec = $diff->format('%r').( // prepend the sign - if negative, change it to R if you want the +, too
	                ($diff->s)+ // seconds (no errors)
	                (60*($diff->i))+ // minutes (no errors)
	                (60*60*($diff->h))+ // hours (no errors)
	                (24*60*60*($diff->d))+ // days (no errors)
	                (30*24*60*60*($diff->m))+ // months (???)
	                (365*24*60*60*($diff->y)) // years (???)
	                );
	    return $diff_sec;
	}
	public function diferenciaEnLetras($fecha)
	{
		//date_default_timezone_set('America/Caracas');
		$result='';
		if($fecha->y>0){
			$letra=' Años ';
			if($fecha->y==1){
				$letra=' Año ';
			}
			$result.=$fecha->y.$letra;
		}
		if($fecha->m>0){
			$letra=' Meses ';
			if($fecha->m==1){
				$letra=' Mes ';
			}
			$result.=$fecha->m.$letra;
		}
		if($fecha->d>0){
			$letra=' Días ';
			if($fecha->d==1){
				$letra=' Día ';
			}
			$result.=$fecha->d.$letra;
		}
		if($fecha->h>0){
			$letra=' Horas ';
			if($fecha->h==1){
				$letra=' Hora ';
			}
			$result.=$fecha->h.$letra;
		}
		if($fecha->i>0){
			$letra=' Minutos ';
			if($fecha->i==1){
				$letra=' Minuto ';
			}
			$result.=$fecha->i.$letra;
		}
		if($fecha->s>0){
			$letra=' Segundos ';
			if($fecha->s==1){
				$letra=' Segundo ';
			}
			$result.=$fecha->s.$letra;
		}

        return $result;
	}
	public function diferenciaEnLetras2($fecha)
	{
		//date_default_timezone_set('America/Caracas');
		$result='';
		if($fecha->y>0){
			$letra=' Años ';
			if($fecha->y==1){
				$letra=' Año ';
			}
			$result.=$fecha->y.$letra;
		}
		if($fecha->m>0){
			$letra=' Meses ';
			if($fecha->m==1){
				$letra=' Mes ';
			}
			$result.=$fecha->m.$letra;
		}
		if($fecha->d>0){
			$letra=' Días ';
			if($fecha->d==1){
				$letra=' Día ';
			}
			$result.=$fecha->d.$letra;
		}
		if($fecha->h>0){
			$letra=' Horas ';
			if($fecha->h==1){
				$letra=' Hora ';
			}
			$result.=$fecha->h.$letra;
		}
		if($fecha->i>0){
			$letra=' Minutos ';
			if($fecha->i==1){
				$letra=' Minuto ';
			}
			$result.=$fecha->i.$letra;
		}

        return $result;
	}
	public function promedioSesion($conexion,$usuario, $fecha)
	{
		//date_default_timezone_set('America/Caracas');
		$sql="SELECT * 
			  FROM seguridad_sesiones 
			  WHERE usuar_codig = '".$usuario."'
			    AND sesio_finic = '".$fecha."'";
		$sesiones= $conexion->createCommand($sql)->queryAll();
		$i=0;
		foreach ($sesiones as $key => $sesion) {
			$diff=$this->diferenciaHoras($sesion['sesio_finic'].' '.$sesion['sesio_hinic'], $sesion['sesio_fvenc'].' '.$sesion['sesio_hvenc']);
			$fechas[$i]=$diff;
			if($i==0){
				$inicio = new DateTime($sesion['sesio_finic'].' '.$sesion['sesio_hinic']);
				$clonInicio = clone $inicio;
				$promedio=$diff;
			}else{

				$promedio->y += $diff->y;
			    $promedio->m += $diff->m;
			    $promedio->d += $diff->d;
			    $promedio->h += $diff->h;
			    $promedio->i += $diff->i;
			    $promedio->s += $diff->s;
			    $promedio->f += $diff->f;
			}
			$inicio->add($diff);
			$i++;
		}

		$promedio2=$clonInicio->diff($inicio);
		//echo '<pre>';
		//var_dump($fechas);
		//var_dump($promedio);

		//var_dump($promedio2);
		//exit();

        return $promedio2;
	}

	public function promedioPreVenta($conexion){
		$sql="SELECT prere_codig cantidad, prere_numer nsoli, c.smodu_codig, c.smodu_descr, c.smodu_icono, c.smodu_color, a.prere_fcrea fecha, a.prere_hcrea hora
              FROM rd_preregistro a 
              JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig and a.estat_codig not in (0,4))
              JOIN solicitud_modulo c ON (b.smodu_codig = c.smodu_codig)
              UNION
              SELECT solic_codig cantidad, solic_numer nsoli, c.smodu_codig, c.smodu_descr, c.smodu_icono, c.smodu_color, a.solic_fcrea fecha, a.solic_hcrea hora
              FROM solicitud a 
              JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig and a.estat_codig not in (0,27))
              RIGHT JOIN solicitud_modulo c ON (b.smodu_codig = c.smodu_codig)
              WHERE c.smodu_codig <> 1
                AND c.smodu_codig = 2

                AND a.estat_codig < 11
              GROUP BY 2,3,4,5
              ORDER BY 2";

		$solicitudes= $conexion->createCommand($sql)->queryAll();
		$i=0;
        //echo "<pre>";
        
		foreach ($solicitudes as $key => $solicitud) {
			//var_dump($solicitud);
			$diff2=$this->DiferenciaFechasLaboral($solicitud['fecha'].' '.$solicitud['hora'], date('Y-m-d H:i:s'));
			$dias+=$diff2;
			$i++;
			

		}
		/*var_dump($i);
		var_dump($dias);
		var_dump($dias/$i);*/
		$dias=$this->transformarMonto_v($dias/$i,2);
		return $dias;
	}
	public function DiferenciaFechasLaboral($start,$end){
		$start = new DateTime($start);
		$end = new DateTime($end);
		// otherwise the  end date is excluded (bug?)
		$end->modify('+1 day');

		$interval = $end->diff($start);

		// total days
		$days = $interval->days;

		// create an iterateable period of date (P1D equates to 1 day)
		$period = new DatePeriod($start, new DateInterval('P1D'), $end);

		// best stored as array, so you can add more than one
		//$holidays = array('2012-09-07');

		foreach($period as $dt) {
		    $curr = $dt->format('D');

		    // substract if Saturday or Sunday
		    if ($curr == 'Sat' || $curr == 'Sun') {
		        $days--;
		    }

		    // (optional) for the updated question
		    /*elseif (in_array($dt->format('Y-m-d'), $holidays)) {
		        $days--;
		    }*/
		}

		return $days;
	}
	public function DiferenciaFechasLaboralSegundos($start,$end){
		$start = new DateTime($start);
		$end = new DateTime($end);
		// otherwise the  end date is excluded (bug?)
		$end->modify('+1 day');

		$interval = $end->diff($start);

		// total days
		$days = $interval->days;

		// create an iterateable period of date (P1D equates to 1 day)
		$period = new DatePeriod($start, new DateInterval('P1D'), $end);

		// best stored as array, so you can add more than one
		//$holidays = array('2012-09-07');

		foreach($period as $dt) {
		    $curr = $dt->format('D');

		    // substract if Saturday or Sunday
		    if ($curr == 'Sat' || $curr == 'Sun') {
		        $days--;
		    }

		    // (optional) for the updated question
		    /*elseif (in_array($dt->format('Y-m-d'), $holidays)) {
		        $days--;
		    }*/
		}
		$segundos=$interval->s;
		$segundos+=$interval->i*60;
		$segundos+=(($interval->h*60)*60);
		$segundos+=((($days*24)*60)*60);
		return $segundos;
	}
	public function actionpromedioPreVenta(){
		$this->layout='main';
		$conexion=yii::app()->db;
		$sql="SELECT prere_codig cantidad, prere_numer nsoli, c.smodu_codig, c.smodu_descr, c.smodu_icono, c.smodu_color, a.prere_fcrea fecha, a.prere_hcrea hora
              FROM rd_preregistro a 
              JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig and a.estat_codig not in (0,4))
              JOIN solicitud_modulo c ON (b.smodu_codig = c.smodu_codig)
              UNION
              SELECT solic_codig cantidad, solic_numer nsoli, c.smodu_codig, c.smodu_descr, c.smodu_icono, c.smodu_color, a.solic_fcrea fecha, a.solic_hcrea hora
              FROM solicitud a 
              JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig and a.estat_codig not in (0,27))
              RIGHT JOIN solicitud_modulo c ON (b.smodu_codig = c.smodu_codig)
              WHERE c.smodu_codig <> 1
                AND c.smodu_codig = 2

                AND a.estat_codig < 11
              GROUP BY 2,3,4,5
              ORDER BY 2";

		$solicitudes= $conexion->createCommand($sql)->queryAll();
		$i=0;
        //echo "<pre>";
        
		foreach ($solicitudes as $key => $solicitud) {
			//var_dump($solicitud);
			$diff2=$this->DiferenciaFechasLaboral($solicitud['fecha'].' '.$solicitud['hora'], date('Y-m-d H:i:s'));
			$dias+=$diff2;
			$i++;
			

		}
		var_dump($i);
		var_dump($dias);
		var_dump($dias/$i);
		return $dias;
	}

	public function promedioPostVenta($conexion){
		$sql="SELECT solic_codig cantidad, solic_numer nsoli,  c.smodu_codig, c.smodu_descr, c.smodu_icono, c.smodu_color, a.solic_fcrea fecha, a.solic_hcrea hora
              FROM solicitud a 
              JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig and a.estat_codig not in (0,27))
              RIGHT JOIN solicitud_modulo c ON (b.smodu_codig = c.smodu_codig)
              WHERE c.smodu_codig <> 1
                AND c.smodu_codig >= 2

                AND a.estat_codig >= 11
              GROUP BY 2,3,4,5
              ORDER BY 2";

		$solicitudes= $conexion->createCommand($sql)->queryAll();
		$i=0;
        //echo "<pre>";
        
		foreach ($solicitudes as $key => $solicitud) {
			//var_dump($solicitud);
			$diff2=$this->DiferenciaFechasLaboral($solicitud['fecha'].' '.$solicitud['hora'], date('Y-m-d H:i:s'));
			$dias+=$diff2;
			$i++;
			

		}
		/*var_dump($i);
		var_dump($dias);
		var_dump($dias/$i);*/
		$dias=$this->transformarMonto_v($dias/$i,2);
		return $dias;
	}
	function secondsToTime($seconds) {
	    $dtF = new \DateTime('@0');
	    $dtT = new \DateTime("@$seconds");
	    return $dtF->diff($dtT)->format('%a Dias, %h Horas, %i Minutos Y %s Segundos');
	}
	public function promedioEstatus($conexion,$estatus){
		$sql="SELECT a.prere_codig codigo, prere_numer nsoli, b.traye_fcrea fecha, b.traye_hcrea hora  
			  FROM rapidpago.rd_preregistro a
              JOIN rapidpago.solicitud_trayectoria b ON (a.prere_codig = b.prere_codig)
              WHERE a.estat_codig='".$estatus."'
              GROUP BY 1
              UNION 
              SELECT a.solic_codig codigo, solic_numer nsoli, b.traye_fcrea fecha, b.traye_hcrea hora 
              FROM rapidpago.solicitud a
              JOIN rapidpago.solicitud_trayectoria b ON (a.solic_codig = b.solic_codig)
              WHERE a.estat_codig='".$estatus."'
              GROUP BY 1
              ";
        $solicitudes=$conexion->createCommand($sql)->queryAll();
        $total[0]=0; // General
        $total[1]=0;
        $total[2]=0;
        $total[3]=0;
        $total[4]=0;
        $total['promedio']=0;
        
        foreach ($solicitudes as $key => $solicitud) {
            $diff=$this->DiferenciaFechasLaboralSegundos($solicitud['fecha'].' '.$solicitud['hora'], date('Y-m-d H:i:s'));
            
            //$this->secondsToTime($diff);
            $horas=$diff/60/60;
           	$semaforo=$this->semaforoCodigo($conexion,$estatus,$horas); 
            $total[0]++;
            $total['promedio']+=$diff;
            switch ($semaforo) {
            	case 0:
            		$total[1]++;
            		break;
            	case 1:
            		$total[2]++;
            		break;
            	case 2:
            		$total[3]++;
            		break;
            	case 3:
            		$total[4]++;
            		break;		
            	default:
            		$default++;
            		break;
            }
        }
        $total['promedio']=$total['promedio']/$total[0];
        return $total;
	}
	public function promedioProceso($conexion,$proceso){
		$sql="SELECT a.prere_codig codigo, prere_numer nsoli, b.traye_fcrea fecha, b.traye_hcrea hora, c.smodu_codig, a.estat_codig  
			  FROM rapidpago.rd_preregistro a
              JOIN rapidpago.solicitud_trayectoria b ON (a.prere_codig = b.prere_codig)
              JOIN rapidpago.rd_preregistro_estatus c ON (a.estat_codig = c.estat_codig)
              WHERE c.smodu_codig='".$proceso."'
              AND a.estat_codig NOT IN (0,4)
              GROUP BY 1
              UNION 
              SELECT a.solic_codig codigo, solic_numer nsoli, b.traye_fcrea fecha, b.traye_hcrea hora, c.smodu_codig, a.estat_codig 
              FROM rapidpago.solicitud a
              JOIN rapidpago.solicitud_trayectoria b ON (a.solic_codig = b.solic_codig)
              JOIN rapidpago.rd_preregistro_estatus c ON (a.estat_codig = c.estat_codig)
              WHERE c.smodu_codig='".$proceso."'
              AND a.estat_codig NOT IN (0)
              GROUP BY 1
              ORDER BY 2
              ";
        $solicitudes=$conexion->createCommand($sql)->queryAll();
        $total[0]=0; // General
        $total[1]=0;
        $total[2]=0;
        $total[3]=0;
        $total[4]=0;
        $total['promedio']=0;
        
        foreach ($solicitudes as $key => $solicitud) {
            $diff=$this->DiferenciaFechasLaboralSegundos($solicitud['fecha'].' '.$solicitud['hora'], date('Y-m-d H:i:s'));
            
            $horas=$diff/60/60;
           	$semaforo=$this->semaforoCodigo($conexion,$solicitud['estat_codig'],$horas); 
            $total[0]++;
            $total['promedio']+=$diff;
            switch ($semaforo) {
            	case 0:
            		$total[1]++;
            		break;
            	case 1:
            		$total[2]++;
            		break;
            	case 2:
            		$total[3]++;
            		break;
            	case 3:
            		$total[4]++;
            		break;		
            	default:
            		$default++;
            		break;
            }
        }
        $total['promedio']=$total['promedio']/$total[0];
        return $total;
	}
	public function cantidadEstatus($conexion,$estatus){
		$sql="SELECT solic_codig cantidad, solic_numer nsoli,  c.smodu_codig, c.smodu_descr, c.smodu_icono, c.smodu_color, a.solic_fcrea fecha, a.solic_hcrea hora
              FROM solicitud a 
              JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig and a.estat_codig not in (0,27))
              RIGHT JOIN solicitud_modulo c ON (b.smodu_codig = c.smodu_codig)
              WHERE c.smodu_codig <> 1
                AND c.smodu_codig >= 2

                AND a.estat_codig >= 11
              GROUP BY 2,3,4,5
              ORDER BY 2";

		$solicitudes= $conexion->createCommand($sql)->queryAll();
		$i=0;
        //echo "<pre>";
        
		foreach ($solicitudes as $key => $solicitud) {
			//var_dump($solicitud);
			$diff2=$this->DiferenciaFechasLaboral($solicitud['fecha'].' '.$solicitud['hora'], date('Y-m-d H:i:s'));
			$dias+=$diff2;
			$i++;
			

		}
		/*var_dump($i);
		var_dump($dias);
		var_dump($dias/$i);*/
		$dias=$this->transformarMonto_v($dias/$i,2);
		return $dias;
	}
	public function solicitudesRetraso($conexion,$modulo)
	{
		
		$sql="SELECT * FROM rd_preregistro_estatus WHERE smodu_codig='".$modulo."'";

		$estat=$conexion->createCommand($sql)->queryAll();
		$i=0;
		$total=0;
		foreach ($estat as $key => $value) {
			if($i>0){

				$estatus.=',';
			}
			$estatus.=$value['estat_codig'];
			$i++;
		}
		
		if($modulo=='1'){
			$sql="SELECT prere_codig, estat_codig 
				  FROM rd_preregistro 
				  WHERE estat_codig in (".$estatus.")
				  ORDER BY 2,1";

			$solicitudes=$conexion->createCommand($sql)->queryAll();
			foreach ($solicitudes as $key => $solicitud) {
				$sql="SELECT * 
                  FROM solicitud_trayectoria 
                  WHERE prere_codig = '".$solicitud['prere_codig']."'
                    AND estat_codig = '".$solicitud['estat_codig']."'";
	            $trayectoria = $conexion->createCommand($sql)->queryRow();

	            $inicio=$trayectoria['traye_finic'].' '.$trayectoria['traye_hinic'];
	            $fin=date('Y-m-d H:i:s');

	            $diff=$this->diferenciaHoras($inicio,$fin);
	            $horas=($diff->days * 24 )  + ( $diff->h );
	            $retraso=$this->solicitudRetraso($conexion, $solicitud['estat_codig'], $horas);

	            if($retraso){
	            	$total++;
	            }

			}
			
		}else{
			$sql="SELECT solic_codig, estat_codig 
				  FROM solicitud 
				  WHERE estat_codig in (".$estatus.")
				  ORDER BY 2,1";

			$solicitudes=$conexion->createCommand($sql)->queryAll();
			foreach ($solicitudes as $key => $solicitud) {
				$sql="SELECT * 
                  FROM solicitud_trayectoria 
                  WHERE solic_codig = '".$solicitud['solic_codig']."'
                    AND estat_codig = '".$solicitud['estat_codig']."'";
	            $trayectoria = $conexion->createCommand($sql)->queryRow();

	            $inicio=$trayectoria['traye_finic'].' '.$trayectoria['traye_hinic'];
	            $fin=date('Y-m-d H:i:s');

	            $diff=$this->diferenciaHoras($inicio,$fin);
	            $horas=($diff->days * 24 )  + ( $diff->h );
	            $retraso=$this->solicitudRetraso($conexion, $solicitud['estat_codig'], $horas);

	            if($retraso){
	            	$total++;
	            }

			}
		}
		
        return $total;
	}
	public function solicitudRetraso($conexion, $estatus, $tiempo)
	{
		$return=false;
		$sql="SELECT * 
			  FROM rd_preregistro_estatus 
			  WHERE estat_codig = '".$estatus."'";
		$estat=$conexion->createCommand($sql)->queryRow();
		if($estat['estat_prime']==0 AND $estat['estat_segun']==0 AND $estat['estat_terce']==0){
			$return=false;
		}else{
			if($tiempo<$estat['estat_prime']){	
				$return=false;
			}else if($tiempo>=$estat['estat_prime'] and $tiempo<$estat['estat_segun']){
				$return=false;
			}else if($tiempo>=$estat['estat_segun'] and $tiempo<$estat['estat_terce']){
				$return=true;
			}else{
				$return=true;
			}
		}
		
        return $return;
	}
	public function semaforo($conexion, $estatus, $tiempo)
	{
	
		$sql="SELECT * 
			  FROM rd_preregistro_estatus 
			  WHERE estat_codig = '".$estatus."'";
		$estat=$conexion->createCommand($sql)->queryRow();
		if($estat['estat_prime']==0 AND $estat['estat_segun']==0 AND $estat['estat_terce']==0){
			$icono='fa fa-check-circle';
				$color='btn-success';
		}else{
			if($tiempo<$estat['estat_prime']){	
				$icono='fa fa-check-circle';
				$color='btn-info';
			}else if($tiempo>=$estat['estat_prime'] and $tiempo<$estat['estat_segun']){
				$icono='fa fa-check-circle';
				$color='btn-success';
			}else if($tiempo>=$estat['estat_segun'] and $tiempo<$estat['estat_terce']){
				$return='<span class="badge badge-warning"><i class="fa fa-warning"></i></span>';
				$icono='fa fa-exclamation-circle';
				$color='btn-warning';
			}else{
				$icono='fa fa-times-circle';
				$color='btn-danger';
			}
		}
		
		$return='<button type="button" class="btn '.$color.' btn-circle">
		            <i class="'.$icono.'"></i> 
		        </button>';
        return $return;
	}
	public function semaforoCodigo($conexion, $estatus, $tiempo)
	{
	
		$sql="SELECT * 
			  FROM rd_preregistro_estatus 
			  WHERE estat_codig = '".$estatus."'";
		$estat=$conexion->createCommand($sql)->queryRow();
		if($estat['estat_prime']==0 AND $estat['estat_segun']==0 AND $estat['estat_terce']==0){
			$return=1;
		}else{
			if($tiempo<$estat['estat_prime']){	
				$return=0;
			}else if($tiempo>=$estat['estat_prime'] and $tiempo<$estat['estat_segun']){
				$return=1;
			}else if($tiempo>=$estat['estat_segun'] and $tiempo<$estat['estat_terce']){
				$return=2;
			}else{
				$return=3;
			}
		}
		
        return $return;
	}


	public function actionSeguridadModulos(){
		$usuar=$_POST['usuar'];
		$sql="SELECT concat(modul_codig,' - ',modul_descr) as descripcion, a.* 
			  FROM seguridad_modulos a
			  WHERE modul_codig NOT IN (
			  	SELECT a.modul_codig 
			  	FROM seguridad_permisos_roles a 
				WHERE a.srole_codig='".$usuar."'
			  )";
		
		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryAll();
   		$data=CHtml::listData($data,'modul_codig','descripcion');
   		echo CHtml::tag('option', array('value'=>''), CHtml::encode('Seleccione...'), true);
  		foreach($data as $value=>$name){
   			echo CHtml::tag('option', array('value'=>$value), CHtml::encode($name), true);
  		}
	}

	public function actionSeguridadOrigen(){
		$usuar=$_POST['usuar'];
		$sql="SELECT concat(orige_codig,' - ',orige_descr) as descripcion, a.* 
			  FROM p_solicitud_origen a
			  WHERE orige_codig NOT IN (
			  	SELECT a.orige_codig 
			  	FROM config_asignacion a 
				WHERE a.srole_codig='".$usuar."'
			  )";
		
		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryAll();
   		$data=CHtml::listData($data,'orige_codig','descripcion');
   		echo CHtml::tag('option', array('value'=>''), CHtml::encode('Seleccione...'), true);
  		foreach($data as $value=>$name){
   			echo CHtml::tag('option', array('value'=>$value), CHtml::encode($name), true);
  		}
	}

	public function actionSeguridadBancos(){
		$usuar=$_POST['usuar'];
		$sql="SELECT concat(banco_value,' - ',banco_descr) as descripcion, a.* 
			  FROM p_banco a
			  WHERE banco_codig NOT IN (
			  	SELECT a.banco_codig 
			  	FROM seguridad_roles_banco a 
				WHERE a.srole_codig='".$usuar."'
			  )";
		
		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryAll();
   		$data=CHtml::listData($data,'banco_codig','descripcion');
   		echo CHtml::tag('option', array('value'=>''), CHtml::encode('Seleccione...'), true);
  		foreach($data as $value=>$name){
   			echo CHtml::tag('option', array('value'=>$value), CHtml::encode($name), true);
  		}
	}
	public function crearSolicitud($conexion, $solicitud,$id){
		
		$sql="SELECT * 
			  FROM rd_preregistro
			  WHERE prere_codig='".$solicitud."'";
		$prere=$conexion->createCommand($sql)->queryRow();
		$tdocu=substr($prere['prere_rifco'], 0,1);
		$sql="SELECT * FROM p_documento_tipo WHERE tdocu_descr='".$tdocu."'";
		$p_documento_tipo=$conexion->createCommand($sql)->queryRow();
		
		if($tdocu=='J' or $tdocu=='G'){
			$tclie='1';
		}else{
			$tclie='2';
		}
		$tdocu=$p_documento_tipo['tdocu_codig'];
		$ndocu=explode('-', $prere['prere_rifco']);
		$ndocu=$ndocu[1].$ndocu[2];

		$usuar=$prere['usuar_codig'];
		$fecha=$prere['prere_fcrea'];
		$hora=$prere['prere_hcrea'];

		$sql="SELECT * FROM cliente WHERE clien_rifco = '".$prere['prere_rifco']."'";
		$cliente=$conexion->createCommand($sql)->queryRow();

		if(!$cliente){
		
			$sql="INSERT INTO cliente(tclie_codig, tdocu_codig, clien_ndocu, clien_rifco, clien_rsoci, clien_nfant, acome_codig, clien_tmovi, clien_tfijo, clien_celec, clien_clvip, clien_obser, usuar_codig, clien_fcrea, clien_hcrea) 
			VALUES ('".$tclie."','".$tdocu."','".$ndocu."','".$prere['prere_rifco']."','".$prere['prere_rsoci']."','".$prere['prere_nfant']."','".$prere['acome_codig']."','".$prere['prere_tmovi']."','".$prere['prere_tfijo']."','".$prere['prere_celec']."','".$prere['prere_clvip']."','".$prere['prere_obser']."','".$prere['usuar_codig']."','".$fecha."','".$hora."')";
			$query=$sql;	
			$res1=$data=$conexion->createCommand($sql)->execute();
			
			$sql="SELECT * FROM cliente WHERE clien_rifco = '".$prere['prere_rifco']."'";
			$cliente=$conexion->createCommand($sql)->queryRow();
			$traza=$this->guardarTraza($conexion, 'I', 'cliente', json_encode(''),json_encode($cliente),$query);
		}
		
		$sql="SELECT * FROM cliente_direccion WHERE clien_codig = '".$cliente['clien_codig']."'";
		$direccion=$conexion->createCommand($sql)->queryRow();

		if(!$direccion){
			$sql="INSERT INTO cliente_direccion(clien_codig, estad_codig, munic_codig, parro_codig, ciuda_codig, direc_zpost, direc_dfisc, usuar_codig, direc_fcrea, direc_hcrea) 
			VALUES ('".$cliente['clien_codig']."','".$prere['estad_codig']."','".$prere['munic_codig']."','".$prere['parro_codig']."','".$prere['ciuda_codig']."','".$prere['prere_zpost']."','".$prere['prere_dfisc']."','".$usuar."','".$fecha."','".$hora."')";
			
			$query=$sql;
			$res2=$data=$conexion->createCommand($sql)->execute();
			
			$sql="SELECT * FROM cliente_direccion WHERE clien_codig = '".$cliente['clien_codig']."'";
			$direccion=$conexion->createCommand($sql)->queryRow();
			$traza=$this->guardarTraza($conexion, 'I', 'cliente_direccion', json_encode(''),json_encode($direccion),$query);

		}

		$sql="SELECT * FROM cliente_representante WHERE clien_codig = '".$cliente['clien_codig']."'";
		$rlegal=$conexion->createCommand($sql)->queryRow();

		if(!$rlegal){
			$sql="INSERT INTO cliente_representante(clien_codig, rlega_rifrl, rlega_nombr, rlega_apell, tdocu_codig, rlega_ndocu, rlega_cargo, rlega_tmovi, rlega_tfijo, rlega_corre, usuar_codig, rlega_fcrea, rlega_hcrea)
			(SELECT '".$cliente['clien_codig']."', rlega_rifrl, rlega_nombr, rlega_apell, tdocu_codig, rlega_ndocu, rlega_cargo, rlega_tmovi, rlega_tfijo, rlega_corre, '".$usuar."','".$fecha."','".$hora."' 
			FROM rd_preregistro_representante_legal 
			WHERE prere_codig='".$prere['prere_codig']."'
			  AND rlega_rifrl NOT IN (
			  	SELECT rlega_rifrl 
			  	FROM cliente_representante 
			  	WHERE clien_codig='".$cliente['clien_codig']."')
			)";

			$query=$sql;
			$res2=$data=$conexion->createCommand($sql)->execute();
			
			$sql="SELECT * FROM cliente_representante WHERE clien_codig = '".$cliente['clien_codig']."'";
			$rlegal=$conexion->createCommand($sql)->queryAll();
			$traza=$this->guardarTraza($conexion, 'I', 'cliente_representante', json_encode(''),json_encode($rlegal),$query);
		}

		
		$sql="SELECT * FROM solicitud WHERE clien_codig = '".$cliente['clien_codig']."' AND prere_codig='".$prere['prere_codig']."'";
		$solicitud=$conexion->createCommand($sql)->queryRow();

		if(!$solicitud){
			$sql="INSERT INTO solicitud( prere_codig, solic_numer, clien_codig, solic_celec, banco_codig, solic_cafil, solic_ncuen, solic_fsoli, orige_codig, solic_clvip, solic_obser, estat_codig, usuar_codig, solic_fcrea, solic_hcrea) 
			SELECT '".$prere['prere_codig']."', prere_numer, '".$cliente['clien_codig']."', prere_celec, banco_codig, prere_cafil, prere_ncuen, prere_fsoli, orige_codig, prere_clvip, prere_obser, '6', '".$usuar."','".$fecha."','".$hora."' FROM rd_preregistro WHERE prere_codig='".$prere['prere_codig']."'" ;
			$query=$sql;
			$res2=$conexion->createCommand($sql)->execute();
			
			$sql="SELECT * FROM solicitud WHERE clien_codig = '".$cliente['clien_codig']."' AND prere_codig='".$prere['prere_codig']."'";
			$solicitud=$conexion->createCommand($sql)->queryRow();
			$traza=$this->guardarTraza($conexion, 'I', 'solicitud', json_encode(''),json_encode($solicitud),$query);
		}
		
		$sql="SELECT * FROM solicitud_documento WHERE solic_codig = '".$solicitud['solic_codig']."'";
		$documento=$conexion->createCommand($sql)->queryRow();
		if(!$documento){
		
			$sql="INSERT INTO solicitud_documento(solic_codig, dtipo_codig, docum_ruta, docum_orden, usuar_codig, docum_fcrea, docum_hcrea) 
			(SELECT  '".$solicitud['solic_codig']."', dtipo_codig, docum_ruta, docum_orden, '".$usuar."','".$fecha."','".$hora."'
			FROM rd_preregistro_documento_digital 
			WHERE prere_codig='".$prere['prere_codig']."')";
			
			$query=$sql;
			$res2=$data=$conexion->createCommand($sql)->execute();
			
			$sql="SELECT * FROM solicitud_documento WHERE solic_codig = '".$solicitud['solic_codig']."'";
			$documento=$conexion->createCommand($sql)->queryAll();
			$traza=$this->guardarTraza($conexion, 'I', 'solicitud_documento', json_encode(''),json_encode($documento),$query);

			foreach ($id as $key => $value) {
				$sql="UPDATE solicitud_documento 
				SET docum_verif='1'
				WHERE solic_codig = '".$solicitud['solic_codig']."'
				AND dtipo_codig='".$value."'";
				$res2=$conexion->createCommand($sql)->execute();
			}
			$sql="SELECT * FROM solicitud_documento WHERE solic_codig = '".$solicitud['solic_codig']."'";
			$documento=$conexion->createCommand($sql)->queryAll();
			
		}

		$sql="SELECT * FROM solicitud_equipos WHERE solic_codig = '".$solicitud['solic_codig']."'";
		$equipos=$conexion->createCommand($sql)->queryRow();
		if(!$equipos){
		
			$sql="INSERT INTO solicitud_equipos(solic_codig, inven_codig, otele_codig, cequi_canti, usuar_codig, cequi_fcrea, cequi_hcrea)
			(SELECT '".$solicitud['solic_codig']."', inven_codig, otele_codig, cequi_canti, '".$usuar."','".$fecha."','".$hora."'
			FROM rd_preregistro_configuracion_equipo 
			WHERE prere_codig='".$prere['prere_codig']."')";
			$query=$sql;
			
			$res2=$data=$conexion->createCommand($sql)->execute();
			
			$sql="SELECT * FROM solicitud_equipos WHERE solic_codig = '".$solicitud['solic_codig']."'";
			$equipos=$conexion->createCommand($sql)->queryAll();
			$traza=$this->guardarTraza($conexion, 'I', 'solicitud', json_encode(''),json_encode($equipos),$query);
		}
		$trayectoria=$this->guardarTrayectoriaSolicitudes($conexion, $solicitud['solic_codig'],  $solicitud['estat_codig'], '', 2);

		if($cliente and $direccion and $rlegal and $solicitud and $documento and $equipos){
			$msg=array('success'=>'true','msg'=>'solicitud creada correctamente');
		}else{
			$msg=array('success'=>'false','msg'=>'Error al guardar la solicitud');
		}

		return $msg;
	}

	public function asignarSolicitud($conexion, $solicitud,$id){
		

		$user=Yii::app()->user->id['usuario']['codigo'];
		$fecha=date('Y-m-d');
		$hora=date('H:i:s');

		$sql="SELECT * 
			  FROM solicitud
			  WHERE prere_codig='".$solicitud."'";
		$solic=$conexion->createCommand($sql)->queryRow();
		$sql="SELECT * 
			  FROM config_asignacion
			  WHERE orige_codig = '".$solic['orige_codig']."'";
		$casign=$conexion->createCommand($sql)->queryAll();
		$canasi=0;

		foreach ($casign as $key => $asigna) {
			
			$verificar='';
			if($solic['solic_clvip']=='1'){
				if($asigna['solic_clvip']=='1'){
					$verificar=true;
				}else{
					$verificar=false;
				}
			}else{
				$verificar=true;
			}
			if($verificar){
				$sql="SELECT * 
					  FROM seguridad_usuarios a
					  WHERE urole_codig = '".$asigna['srole_codig']."'
					    AND spniv_codig='1'";
				$usuarios=$conexion->createCommand($sql)->queryAll();
				if($usuarios){
					$usuar=array();
					foreach ($usuarios as $key => $usuario) {
						$sql="SELECT count(solic_codig) cantidad
							  FROM solicitud_usuario a
							  WHERE a.usuar_codig='".$usuario['usuar_codig']."'";
						$cantidad=$conexion->createCommand($sql)->queryRow();
						
						$usuar[$usuario['usuar_codig']]=$cantidad['cantidad'];
					}

					$min=min($usuar);
					$res = end(array_keys($usuar, min($usuar)));
					$sql="INSERT INTO solicitud_usuario(solic_codig, usuar_codig, solus_ucrea, solus_fcrea, solus_hcrea) 
						VALUES ('".$solic['solic_codig']."','".$res."','".$user."','".$fecha."','".$hora."')";
					$result=$conexion->createCommand($sql)->execute();
					if($result){
						$canasi++;
					}else{
						$msg=array('success'=>'false','msg'=>'Error al guardar la asignacion');
					}	
				}
				
			}
		}
		if ($canasi>0) {
			$msg=array('success'=>'true','msg'=>'Solicitud Asignada Correctamente');
		}else{
			$msg=array('success'=>'false','msg'=>'No se pudo asignar la solicitud');
		}

		return $msg;
	}
	public function generarCsvCertificacion($conexion, $id, $destino)
	{
		$file = fopen($destino, "w");
		$cadena2='"Banco";"Modelo POS";"Tipo de Comunicación";"Operadora";"Serial Saliente";"Serial Entrante";"Razon Social";"Afiliado";"Terminal";"Rif";"Version del Aplicativo";"Tipo Gestión";"Observación";"Nro de Cuenta";"Actividad Comercial";"Dirección";"Parroquia";"Municipio";"Ciudad";"Estado";"Persona de contacto";"Correo";"Teléfono"';
		fwrite($file, $cadena2. PHP_EOL);
		foreach ($id as $key => $value) {
				$sql="SELECT b.banco_descr banco,
					'' modelo_pos,
					'' tipo_comunicacion,
					'' operadora,
					'' serial_saliente,
					'' serial_entrante,
					c.clien_rsoci razon_social,
					a.solic_cafil afiliado,
					'' terminal,
					c.clien_rifco rif,
					'' version_aplicativo,
					'' tipo_gestion,
					a.solic_obser observacion,
					concat('\'',a.solic_ncuen) nro_cuenta,
					d.acome_descr actividad_comercial,
					e.direc_dfisc direccion,
					f.parro_descr parroquia,
					g.munic_descr municipio,
					h.ciuda_descr ciudad,
					i.estad_descr estado,
					(SELECT concat(rlega_nombr,' ',rlega_apell) FROM cliente_representante j WHERE c.clien_codig = j.clien_codig limit 1 ) persona_contacto,
					a.solic_celec correo,
					c.clien_tmovi telefono
				FROM solicitud a
				JOIN p_banco b ON (a.banco_codig = b.banco_codig)
				JOIN cliente c ON (c.clien_codig = a.clien_codig)
				JOIN p_actividad_comercial d ON (c.acome_codig = d.acome_codig )
				JOIN cliente_direccion e ON (c.clien_codig = e.clien_codig )
				JOIN p_parroquia f ON (e.parro_codig = f.parro_codig )
				JOIN p_municipio g ON (g.munic_codig = e.munic_codig )
				JOIN p_ciudades h ON (h.ciuda_codig = e.ciuda_codig )
				JOIN p_estados i ON (i.estad_codig = e.estad_codig )
				WHERE a.solic_codig='".$value."'";
			

			$csv=$conexion->createCommand($sql)->queryRow();
			$i=0;

			$cadena2=implode('";"', $csv);
			$cadena2='"'.$cadena2.'"';
			foreach ($csv as $clave => $valor) {
				$i++;
				if($i>1){
					$cadena.=';';
				}
				$cadena.=$valor;
			}
			fwrite($file, $cadena2. PHP_EOL);

		}
		
		fclose($file);
		chmod($destino, 0777);
	}
	public function generarCsvDomiciliacion($conexion, $id, $destino)
	{
		$file = fopen($destino, "w");
		foreach ($id as $key => $value) {
			$sql="SELECT o.banco_descr, 
						   e.model_descr modelo_pos, 
						   'INALAMBRICO' tipo_comunicacion, 
						   g.model_descr operadora,
						   p.seria_numer serial_saliente,
						   c.seria_numer serial_entrante,
						   h.clien_rsoci razon_social,
						   a.solic_cafil afiliado,
						   q.termi_numer terminal,
						   h.clien_rifco rif,
						   'INSTALACION' tipo_gestion,
						   concat('\'',a.solic_ncuen) nro_cuenta

					FROM solicitud a
					JOIN solicitud_asignacion b ON (a.solic_codig = b.solic_codig )
					JOIN inventario_seriales c ON (b.asign_dispo = c.seria_codig)
					JOIN inventario d ON (c.inven_codig = d.inven_codig)
					JOIN inventario_modelo e ON (c.model_codig = e.model_codig)
					JOIN inventario_seriales f ON (f.seria_codig = b.asign_opera)
					JOIN inventario_modelo g ON (f.model_codig = g.model_codig)
					JOIN cliente h ON (h.clien_codig = a.clien_codig)
					JOIN p_actividad_comercial i ON (i.acome_codig = h.acome_codig)
					JOIN cliente_direccion j ON (h.clien_codig = j.clien_codig )
					JOIN p_parroquia k ON (j.parro_codig = k.parro_codig )
					JOIN p_municipio l ON (j.munic_codig = l.munic_codig )
					JOIN p_ciudades m ON (j.ciuda_codig = m.ciuda_codig )
					JOIN p_estados n ON (j.estad_codig = n.estad_codig )
					JOIN p_banco o ON (a.banco_codig = o.banco_codig)
					JOIN solicitud_terminal q ON (b.termi_codig = q.termi_codig)
					LEFT JOIN inventario_seriales p ON (b.asign_sdisp = p.seria_codig)
					WHERE a.solic_codig='".$value."'";
			

			$csv=$conexion->createCommand($sql)->queryRow();
			$i=0;
			$cadena2=implode('";"', $csv);
			$cadena2='"'.$cadena2.'"';
			foreach ($csv as $clave => $valor) {
				$i++;
				if($i>1){
					$cadena.=';';
				}
				$cadena.=$valor;
			}
			fwrite($file, $cadena2. PHP_EOL);

		}
		
		fclose($file);
		chmod($destino, 0777);
	}

	public function generarCsvConfirmacion($conexion, $id, $destino)
	{
		$file = fopen($destino, "w");
		$j=0;
		$cadena='"#";"Banco";"Modelo POS";"Tipo de Comunicación";"Operadora";"Serial Saliente";"Serial entrante";"Nombre del Comercio";"Afiliado";"Nº term.";"Rif";"Versión Aplicativo";"Tipo de Gestión";"Observaciones"';
		fwrite($file, $cadena. PHP_EOL);
		foreach ($id as $key => $value) {
			$j++;
			$sql="SELECT o.banco_descr, 
						   e.model_descr modelo_pos, 
						   'INALAMBRICO' tipo_comunicacion, 
						   g.model_descr operadora,
						   p.seria_numer serial_saliente,
						   c.seria_numer serial_entrante,
						   h.clien_rsoci razon_social,
						   a.solic_cafil afiliado,
						   q.termi_numer terminal,
						   h.clien_rifco rif,
						   '' version_aplicativo,
						   '' tipo_gestion,
						   a.solic_obser observacion/*,
						   concat('\'',a.solic_ncuen) nro_cuenta,
						   i.acome_descr actividad_comercial,
						   j.direc_dfisc direccion,
						   k.parro_descr parroquia,
						   l.munic_descr municipio,
						   m.ciuda_descr ciudad,
						   n.estad_descr estado*/

					FROM solicitud a
					JOIN solicitud_asignacion b ON (a.solic_codig = b.solic_codig )
					JOIN inventario_seriales c ON (b.asign_dispo = c.seria_codig)
					JOIN inventario d ON (c.inven_codig = d.inven_codig)
					JOIN inventario_modelo e ON (c.model_codig = e.model_codig)
					JOIN inventario_seriales f ON (f.seria_codig = b.asign_opera)
					JOIN inventario_modelo g ON (f.model_codig = g.model_codig)
					JOIN cliente h ON (h.clien_codig = a.clien_codig)
					JOIN p_actividad_comercial i ON (i.acome_codig = h.acome_codig)
					JOIN cliente_direccion j ON (h.clien_codig = j.clien_codig )
					JOIN p_parroquia k ON (j.parro_codig = k.parro_codig )
					JOIN p_municipio l ON (j.munic_codig = l.munic_codig )
					JOIN p_ciudades m ON (j.ciuda_codig = m.ciuda_codig )
					JOIN p_estados n ON (j.estad_codig = n.estad_codig )
					JOIN p_banco o ON (a.banco_codig = o.banco_codig)
					JOIN solicitud_terminal q ON (b.termi_codig = q.termi_codig)
					LEFT JOIN inventario_seriales p ON (b.asign_sdisp = p.seria_codig)
					WHERE a.solic_codig='".$value."'";
			

			$csv=$conexion->createCommand($sql)->queryRow();
			$i=0;
			$cadena2=implode('";"', $csv);
			$cadena2='"'.$j.'";"'.$cadena2.'"';
			foreach ($csv as $clave => $valor) {
				$i++;
				if($i>1){
					$cadena.=';';
				}
				$cadena.=$valor;
			}
			fwrite($file, $cadena2. PHP_EOL);

		}
		
		fclose($file);
		chmod($destino, 0777);
	}
	
	public function verSolicitudesArchivo($conexion, $tramitacion)
	{
		$sql="SELECT c.solic_numer
			  FROM solicitud_solicitud_tramitacion a 
			  JOIN solicitud_tramitacion b ON (a.trami_codig = b.trami_codig)
			  JOIN solicitud c ON (a.solic_codig = c. solic_codig) 
			  WHERE a.trami_codig = '".$tramitacion."'";
		$solicitudes=$conexion->createCommand($sql)->queryAll();
		$i=0;
		foreach ($solicitudes as $key => $value) {
			if($i>0){
				$solicitud.=', ';
			}
			$solicitud.=$value['solic_numer'];
			$i++;
		}
		return $solicitud;
	}

	public function verSolicitudesTraslado($conexion, $traslado)
	{
		$sql="SELECT d.solic_numer
			  FROM solicitud_movimiento_traslado a 
			  JOIN solicitud_traslado b ON (a.trasl_codig = b.trasl_codig)
			  JOIN inventario_movimiento c ON (a.movim_codig = c.movim_codig)
			  JOIN solicitud d ON (a.solic_codig = d.solic_codig) 
			  WHERE a.trasl_codig = '".$traslado."'";
		$solicitudes=$conexion->createCommand($sql)->queryAll();
		$i=0;
		foreach ($solicitudes as $key => $value) {
			if($i>0){
				$solicitud.=', ';
			}
			$solicitud.=$value['solic_numer'];
			$i++;
		}
		return $solicitud;
	}
	public function verSolicitudesVIPTraslado($conexion, $traslado)
	{
		$sql="SELECT d.solic_clvip
			  FROM solicitud_movimiento_traslado a 
			  JOIN solicitud_traslado b ON (a.trasl_codig = b.trasl_codig)
			  JOIN inventario_movimiento c ON (a.movim_codig = c.movim_codig)
			  JOIN solicitud d ON (a.solic_codig = d.solic_codig) 
			  WHERE a.trasl_codig = '".$traslado."'";
		$solicitudes=$conexion->createCommand($sql)->queryAll();
		$i=0;
		$vip='false';
		foreach ($solicitudes as $key => $value) {
			if($value['solic_clvip']=='1'){
				$vip='true';	
			}
			$i++;
		}
		return $vip;
	}
	public function verTerminalesSolicitudes($conexion, $codigo)
	{
		$sql="SELECT a.termi_numer
			  FROM solicitud_terminal a 
			  WHERE a.solic_codig = '".$codigo."'";
		$solicitudes=$conexion->createCommand($sql)->queryAll();
		$i=0;
		foreach ($solicitudes as $key => $value) {
			if($i>0){
				$solicitud.=', ';
			}
			$solicitud.=$value['termi_numer'];
			$i++;
		}
		return $solicitud;
	}
	public function verOperadoresSolicitudes($conexion, $codigo)
	{
		$sql="SELECT DISTINCT b.otele_descr
			  FROM solicitud_equipos a 
			  JOIN p_operador_telefonico b ON (a.otele_codig = b.otele_codig)
			  WHERE a.solic_codig = '".$codigo."'";
		$solicitudes=$conexion->createCommand($sql)->queryAll();
		$i=0;
		foreach ($solicitudes as $key => $value) {
			if($i>0){
				$solicitud.=', ';
			}
			$solicitud.=$value['otele_descr'];
			$i++;
		}
		return $solicitud;
	}
	public function actionListarMontoSolicitud(){
		$conexion=Yii::app()->db;

		$solic=$_POST['solic'];
		$sql="SELECT a.* FROM solicitud a 
				WHERE a.solic_codig='".$solic."'";
		$data=$conexion->createCommand($sql)->queryRow();
		
		$sql="SELECT a.* FROM solicitud_pagos a 
				WHERE a.solic_codig='".$solic."'
				AND a.epago_codig='2'";

		$pagos=$conexion->createCommand($sql)->queryAll();
		foreach ($pagos as $key => $pago) {
			$pagado+=($pago['pagos_monto']*1.0);
		}
		$msg['success']='true';
		$msg['mpedi']=$this->TransformarMonto_v($data['solic_monto'],0);
		$msg['resto']=$this->TransformarMonto_v($data['solic_monto']-$pagado,0);
   		echo json_encode($msg);
	}
	public function verificarPagoSolicitud($conexion, $solic){
		
		$sql="SELECT * FROM solicitud WHERE solic_codig='".$solic."'";
		$solicitud=$conexion->createCommand($sql)->queryRow();
			
		$estat='11';
		$estat2='10';
		$sql="SELECT a.* FROM solicitud_pagos a 
				WHERE a.solic_codig='".$solicitud['solic_codig']."'
				AND a.epago_codig='2'";
		$pagos=$conexion->createCommand($sql)->queryAll();
		foreach ($pagos as $key => $pago) {
			$pagado+=($pago['pagos_monto']*1.0);
		}
		$pendiente=$solicitud['solic_monto']-$pagado;
		$msg=array('success'=>'true','msg'=>'Pendiente por cancelar: '.$this->transformarMonto_v($pendiente,2));
		if($solicitud['solic_monto']<=$pagado){
			$sql="UPDATE solicitud 
					 SET estat_codig='".$estat."'
				WHERE solic_codig='".$solicitud['solic_codig']."'";
			$query=$sql;

			$res=$conexion->createCommand($sql)->execute();

			$sql="SELECT * FROM solicitud WHERE solic_codig='".$solicitud['solic_codig']."'";
			$solicitud2=$conexion->createCommand($sql)->queryRow();
			
			$traza=$this->guardarTraza($conexion, 'U', 'solicitud', json_encode($solicitud),json_encode($solicitud2),$query);
			$trayectoria=$this->guardarTrayectoriaSolicitudes($conexion, $solicitud['solic_codig'], $estat, $solicitud['estat_codig'], 2);
			$msg=array('success'=>'true','msg'=>'Solicitud totalmente pagada');
		}else{
			$estat='10';
			$sql="UPDATE solicitud 
					 SET estat_codig='".$estat."'
				WHERE solic_codig='".$solicitud['solic_codig']."'";
			$query=$sql;

			$res=$conexion->createCommand($sql)->execute();

			$sql="SELECT * FROM solicitud WHERE solic_codig='".$solicitud['solic_codig']."'";
			$solicitud2=$conexion->createCommand($sql)->queryRow();
			
			$traza=$this->guardarTraza($conexion, 'U', 'solicitud', json_encode($solicitud),json_encode($solicitud2),$query);
			$trayectoria=$this->guardarTrayectoriaSolicitudes($conexion, $solicitud['solic_codig'], $estat, $solicitud['estat_codig'], 2);
			$msg=array('success'=>'true','msg'=>'Solicitud totalmente pagada');
		}
		return $msg;
	}

	public function cotizacionesVencidas(){
		$estat=29;
		$usuar=Yii::app()->user->id['usuario']['codigo'];
		$fecha=date('Y-m-d');
		$hora=date('H:i:s');
		$conexion=Yii::app()->db;
		$transaction=$conexion->beginTransaction();

		try{
			$sql="SELECT * FROM solicitud a
	                JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
	                JOIN cliente c ON (a.clien_codig = c.clien_codig)
	                JOIN seguridad_usuarios d ON (a.usuar_codig = d.usuar_codig)
	                JOIN p_banco f ON (a.banco_codig = f.banco_codig)
	                JOIN p_persona e ON (d.perso_codig = e.perso_codig)
	                JOIN solicitud_cotizacion g ON (a.solic_codig = g.solic_codig AND g.cotiz_estat='1')
	              /*WHERE a.estat_codig in ('10','29')*/
	              WHERE a.estat_codig in ('10')
	              AND a.solic_codig not in (SELECT solic_codig FROM solicitud_pagos a  GROUP BY solic_codig)
	        ";
			$solicitudes=$conexion->createCommand($sql)->queryAll();
			foreach ($solicitudes as $key => $row) {
				$cinic=$row['cotiz_fcrea'].' '.$row['cotiz_hcrea'];
	            $cfin=date('Y-m-d H:i:s');
	            $cdiff=$this->diferenciaHoras($cinic, $cfin);
	            
	            $choras=($cdiff->days * 24 )  + ( $cdiff->h );

	            $sql="SELECT * 
	                  FROM p_tiempo_cotizacion 
	                  WHERE pesta_codig = '1'";

	            $tcoti = $conexion->createCommand($sql)->queryRow();
	            if($tcoti['tcoti_value']<$choras){
	            	$sql="SELECT * FROM solicitud
					WHERE solic_codig='".$row['solic_codig']."';";
					$solicitud=$conexion->createCommand($sql)->queryRow();

	            	$sql="UPDATE solicitud 
						 SET estat_codig='".$estat."'
					WHERE solic_codig='".$solicitud['solic_codig']."'";
					$query=$sql;

					$res=$conexion->createCommand($sql)->execute();

					$sql="SELECT * FROM solicitud WHERE solic_codig='".$solicitud['solic_codig']."'";
					$solicitud2=$conexion->createCommand($sql)->queryRow();
					
					$traza=$this->guardarTraza($conexion, 'U', 'solicitud', json_encode($solicitud),json_encode($solicitud2),$query);

					$trayectoria=$this->guardarTrayectoriaSolicitudes($conexion, $solicitud['solic_codig'], $estat, $solicitud['estat_codig'], 2);
					
	            }
			}

			$transaction->commit();
			$msg=array('success'=>'true','msg'=>'Solicitud actualizada correctamente');

		}catch(Exception $e){
			$transaction->rollBack();
			$msg=array('success'=>'false','msg'=>'Error al verificar la información');
		}
		return $msg;
	}
	public function ActionComboProductos(){
		$modelo=$_POST['modelo'];
		$sql="SELECT a.inven_codig, a.inven_descr
			  FROM inventario a
			  JOIN inventario_modelo b ON (a.model_codig =b.model_codig)
			  WHERE a.model_codig ='".$modelo."'";
			 

		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryAll();
   		$data=CHtml::listData($data,'inven_codig','inven_descr');
   		echo CHtml::tag('option', array('value'=>''), CHtml::encode('Seleccione...'), true);
  		foreach($data as $value=>$name){
   			echo CHtml::tag('option', array('value'=>$value), CHtml::encode($name), true);
  		}
	}
	public function ActionComboProductosAsignar(){
		$modelo=$_POST['modelo'];
		$sql="SELECT a.inven_codig, a.inven_descr
			  FROM inventario a
			  JOIN inventario_modelo b ON (a.model_codig =b.model_codig)
			  JOIN inventario_seriales c ON (a.inven_codig =c.inven_codig)
			  WHERE a.model_codig ='".$modelo."'";
			 

		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryAll();
   		$data=CHtml::listData($data,'inven_codig','inven_descr');
   		echo CHtml::tag('option', array('value'=>''), CHtml::encode('Seleccione...'), true);
  		foreach($data as $value=>$name){
   			echo CHtml::tag('option', array('value'=>$value), CHtml::encode($name), true);
  		}
	}
	public function ActionComboSerial(){
		$modelo=$_POST['modelo'];
		$producto=$_POST['producto'];
		$tipo=$_POST['tipo'];
		$sql="SELECT a.seria_codig, a.seria_numer
			  FROM inventario_seriales a
			  JOIN inventario b ON (a.inven_codig =b.inven_codig)
			  JOIN inventario_modelo c ON (a.model_codig =c.model_codig)
			  WHERE a.model_codig ='".$modelo."'
			  AND a.inven_codig ='".$producto."'
			  AND a.seria_codig not in (
		    	SELECT b.asign_dispo
		    	FROM solicitud_asignacion b)";
		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryAll();
   		$data=CHtml::listData($data,'seria_codig','seria_numer');
   		echo CHtml::tag('option', array('value'=>''), CHtml::encode('Seleccione...'), true);
  		foreach($data as $value=>$name){
   			echo CHtml::tag('option', array('value'=>$value), CHtml::encode($name), true);
  		}
	}
	public function ActionComboSerialSim(){
		$modelo=$_POST['modelo'];
		$producto=$_POST['producto'];
		$tipo=$_POST['tipo'];
		$sql="SELECT a.seria_codig, CONCAT(b.inven_descr,' - ', a.seria_numer) as descripcion
			  FROM inventario_seriales a
			  JOIN inventario b ON (a.inven_codig =b.inven_codig)
			  JOIN inventario_modelo c ON (a.model_codig =c.model_codig)
			  WHERE a.model_codig ='".$modelo."'
			  AND a.seria_codig not in (
		    	SELECT b.asign_opera
		    	FROM solicitud_asignacion b)";
			 

		$conexion=Yii::app()->db;
		$data=$conexion->createCommand($sql)->queryAll();
   		$data=CHtml::listData($data,'seria_codig','descripcion');
   		echo CHtml::tag('option', array('value'=>''), CHtml::encode('Seleccione...'), true);
  		foreach($data as $value=>$name){
   			echo CHtml::tag('option', array('value'=>$value), CHtml::encode($name), true);
  		}
	}
	public function calcularMontoCotizacion($conexion,$value)
	{
		$sql="SELECT * FROM solicitud_asignacion WHERE solic_codig='".$value."'";
		$asignacion=$conexion->createCommand($sql)->queryAll();
		$monto=0;
		$i=0;
		foreach ($asignacion as $key => $value) {

			$sql="SELECT * 
				  FROM inventario_seriales a
				  JOIN inventario b ON (a.inven_codig = b.inven_codig)
				  JOIN p_moneda c ON (b.moned_codig = c.moned_codig)
				  WHERE a.seria_codig='".$value['asign_dispo']."'";
			$dispo=$conexion->createCommand($sql)->queryRow();

			$monto += $dispo['inven_punid']*$dispo['moned_cambi'];
			$sql="SELECT * 
				  FROM inventario_seriales a
				  JOIN inventario b ON (a.inven_codig = b.inven_codig)
				  JOIN p_moneda c ON (b.moned_codig = c.moned_codig)
				  WHERE a.seria_codig='".$value['asign_opera']."'";
			$opera=$conexion->createCommand($sql)->queryRow();
			$monto += $opera['inven_punid']*$opera['moned_cambi'];
			$i++;
		}

		return $monto;

	}
	public function calcularCantidadCotizacion($conexion,$value)
	{
		$sql="SELECT sum(cequi_canti) cantidad FROM solicitud_equipos WHERE solic_codig='".$value."'";

		$asignacion=$conexion->createCommand($sql)->queryRow();

		$cantidad=$asignacion['cantidad'];

		return $cantidad;

	}

	public function calcularPrecioCotizacion($conexion,$value)
	{
		$sql="SELECT  inven_codig, sum(cequi_canti) cantidad 
			  FROM solicitud_equipos 
			  WHERE solic_codig='".$value."'
			  GROUP BY inven_codig";

		$asignaciones=$conexion->createCommand($sql)->queryAll();
		foreach ($asignaciones as $key => $asignacion) {
			$sql="SELECT * 
				  FROM p_precio_venta
				  WHERE inven_codig='".$asignacion['inven_codig']."'
					AND pesta_codig='1'";
			$precio=$conexion->createCommand($sql)->queryRow();

			$return['cantidad']+=$asignacion['cantidad'];
			$return['monto']+=$precio['pvent_value'];
			$return['total']+=$precio['pvent_value']*$asignacion['cantidad'];

		}

		//$cantidad=$asignacion['cantidad'];

		return $return;

	}

	public function imprimirDatosSolicitud($solicitud)
	{
		$this->renderPartial('datos_solicitud',array('solicitud' => $solicitud));
	}
	public function imprimirDatosPreRegistro($solicitud)
	{
		$this->renderPartial('datos_preregistro',array('solicitud' => $solicitud));
	}
	public function actionDescargarArchivo()
	{
		$this->layout='pdf/main';
		if($_GET){
			
			$c=$_GET['c'];
			$conexion=Yii::app()->db;

			$sql="SELECT * FROM solicitud_tramitacion WHERE trami_codig='".$c."'";
			$archivo=$conexion->createCommand($sql)->queryRow();
			$nombre=explode('/', $archivo['trami_ruta']);
			$nombre=end($nombre);
			
			header('Content-Type: application/csv');

			header("Content-disposition: attachment; filename=".$nombre);
			
			readfile(Yii::app()->request->getBaseUrl(true).'/'.$archivo['trami_ruta']);
	
		}else{
			$this->redirect('listado');
		}
	}

	public function organigrama(){
		$conexion=Yii::app()->db;
		$codigo=Yii::app()->user->id['usuario']['codigo'];
		$sql="SELECT * 
			  FROM seguridad_usuarios
			  WHERE usuar_codig='".$codigo."'";
		$usuario=$conexion->createCommand($sql)->queryRow();

		$sql="SELECT * 
			  FROM p_organigrama
			  WHERE organ_codig='".$usuario['organ_codig']."'";
		$organigrama=$conexion->createCommand($sql)->queryRow();
		
		$codigo="'".$organigrama['organ_codig']."'";
		
		$codigo.=$this->organigramaDependiente($usuario['organ_codig']);
		
		return $codigo;
	}
	public function organigramaDependiente($codigo){
		
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM p_organigrama
			  WHERE organ_padre='".$codigo."'";
		$organigrama=$conexion->createCommand($sql)->queryAll();
		
		foreach ($organigrama as $key => $value) {
			$return.=",'".$value['organ_codig']."'";
			;

			$return.= $this->organigramaDependiente($value['organ_codig']);
		}

		return $return;
	}

	public function actionPruebaProfit(){

		$conexion=Yii::app()->profit;
		$transaction=$conexion->beginTransaction();
		try{
			$sql="SELECT * FROM ALMACEN";
			$cliente=$conexion->createCommand($sql)->queryAll();

			$sql="SELECT * FROM tipo_cli";
			$tipcli=$conexion->createCommand($sql)->queryAll();

			echo '<pre>';
			var_dump($cliente);
			var_dump($tipcli);
			echo '</pre>';
			exit();
		}catch(Exception $e){
			
			$transaction->rollBack();
			$msg=array('success'=>'false','msg'=>'Error al verificar la información');
		}
		echo json_encode($msg);
    }

    public function UltimasNotificaciones(){

		$conexion=Yii::app()->db;

		$usuar=Yii::app()->user->id['usuario']['codigo'];

		$sql="SELECT * 
			  FROM solicitud_usuario_notificacion a
			  JOIN solicitud b ON (a.solic_codig = b.solic_codig) 
			  JOIN p_notificacion_estatus c ON (a.enoti_codig = c.enoti_codig) 
			  WHERE a.usuar_codig = '".$usuar."'
			    AND ((sunot_fcrea between DATE_SUB(now(), interval 5 minute) and now()) 
			     OR sunot_leido <> '1')";

		$notificaciones=$conexion->createCommand($sql)->queryAll();
		$sql='';
		if($notificaciones){
			$sql .='<li>';
            $sql .='<div class="drop-title">¡Tienes Mensajes!</div>';
            $sql .='</li>';
            $sql .='<li>';
            $sql .='<div class="message-center"> ';
        	foreach ($notificaciones as $key => $notificacion) {
				$sql .= '<a href="#">';
				$sql .= '<div class="mail-contnet">';
	            $sql .= '<h5>'.$notificacion['solic_numer'].' | '.$notificacion['enoti_titul'].'</h5>';
	            $sql .= '<span class="mail-desc">'.$notificacion['enoti_descr'].'</span> ';
	            $sql .= '<span class="time">'.$this->TransformarFecha_v($notificacion['sunot_fcrea']).' '.$notificacion['sunot_hcrea'].'</span> ';
	            $sql .= '</div>';
	        	$sql .= '</a>';	
	        	
			}
			$sql .= '</div>';	
			$sql .= '</li>';	    
		}else{
			$sql .='<li>';
            $sql .='<div class="drop-title">No hay Mensajes Nuevos</div>';
            $sql .='</li>';
		}
		
		echo $sql;
	
	}

	public function actionUltimasNotificaciones(){

		$conexion=Yii::app()->db;

		$usuar=Yii::app()->user->id['usuario']['codigo'];

		$sql="SELECT * 
			  FROM solicitud_usuario_notificacion a
			  JOIN solicitud b ON (a.solic_codig = b.solic_codig) 
			  JOIN p_notificacion_estatus c ON (a.enoti_codig = c.enoti_codig) 
			  WHERE a.usuar_codig = '".$usuar."'
			    AND ((sunot_fcrea between DATE_SUB(now(), interval 5 minute) and now()) 
			     OR sunot_leido <> '1')";

		$notificaciones=$conexion->createCommand($sql)->queryAll();
		$sql='';
		if($notificaciones){
			$sql .='<li>';
            $sql .='<div class="drop-title">¡Tienes Mensajes!</div>';
            $sql .='</li>';
            $sql .='<li>';
            $sql .='<div class="message-center"> ';
            $i=0;
			$return['notif'][$i]['url']=Yii::app()->request->getBaseUrl(true).'/notificaciones/listado';
			$return['notif'][$i]['title']='¡Tienes Mensajes!';
			$return['notif'][$i]['icon']=Yii::app()->request->getBaseUrl(true).'/assets/img/favicon.png';
			$return['notif'][$i]['msg']='Posees al menos un mensaje sin leer.';
			$return['result']='true';
		}else{
			
			$return['result']='false';
		}

		

		echo json_encode($return);
	
	}
	public function actionUltimasNotificacionesDetalle(){

		$conexion=Yii::app()->db;

		$usuar=Yii::app()->user->id['usuario']['codigo'];

		$sql="SELECT * 
			  FROM solicitud_usuario_notificacion a
			  JOIN solicitud b ON (a.solic_codig = b.solic_codig) 
			  JOIN p_notificacion_estatus c ON (a.enoti_codig = c.enoti_codig) 
			  WHERE a.usuar_codig = '".$usuar."'
			    AND ((sunot_fcrea between DATE_SUB(now(), interval 5 minute) and now()) 
			     OR sunot_leido <> '1')";

		$notificaciones=$conexion->createCommand($sql)->queryAll();
		$sql='';
		if($notificaciones){
			$sql .='<li>';
            $sql .='<div class="drop-title">¡Tienes Mensajes!</div>';
            $sql .='</li>';
            $sql .='<li>';
            $sql .='<div class="message-center"> ';
            $i=0;
        	foreach ($notificaciones as $key => $notificacion) {
        		$return['notif'][$i]['url']=Yii::app()->request->getBaseUrl(true).'/notificaciones/listado';
				$return['notif'][$i]['title']=$notificacion['solic_numer'].' | '.$notificacion['enoti_titul'];
				$return['notif'][$i]['icon']=Yii::app()->request->getBaseUrl(true).'/assets/img/favicon.png';
				$return['notif'][$i]['msg']=$notificacion['enoti_descr'];
				$i++;	
	        	
			}

			$return['result']='true';
		}else{
			
			$return['result']='false';
		}

		

		echo json_encode($return);
	
	}

	public function SqlListadoSolicitudes($estat)
	{
		$conexion=Yii::app()->db;
		$sql = "SELECT *
                FROM seguridad_usuarios a
                WHERE a.usuar_codig = '".Yii::app()->user->id['usuario']['codigo']."'";
        $usuario = $conexion->createCommand($sql)->queryRow();      
                        
		if($usuario['spniv_codig']=='2'){
            $sql = "SELECT *
                FROM solicitud a 
                JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                JOIN cliente c ON (a.clien_codig = c.clien_codig)
                JOIN seguridad_usuarios d ON (a.usuar_codig = d.usuar_codig)
                JOIN p_persona e ON (d.perso_codig = e.perso_codig)
                WHERE a.estat_codig in (".$estat.")
                ORDER BY solic_clvip, prere_codig
                ";
        }else{
            $sql = "SELECT *
                FROM solicitud a 
                JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                JOIN cliente c ON (a.clien_codig = c.clien_codig)
                JOIN seguridad_usuarios d ON (a.usuar_codig = d.usuar_codig)
                JOIN p_persona e ON (d.perso_codig = e.perso_codig)
                JOIN solicitud_usuario f ON (a.solic_codig = f.solic_codig)
                WHERE a.estat_codig in (".$estat.")
                  AND f.usuar_codig = '".Yii::app()->user->id['usuario']['codigo']."'
                ORDER BY solic_clvip, prere_codig
                ";
        }
        return $sql;
	}
	public function SqlListadoSolicitudesModulos($desde,$hasta)
	{
		$conexion=Yii::app()->db;
		$sql = "SELECT *
                FROM seguridad_usuarios a
                WHERE a.usuar_codig = '".Yii::app()->user->id['usuario']['codigo']."'";
        $usuario = $conexion->createCommand($sql)->queryRow();      
                        
		if($usuario['spniv_codig']=='2'){
		        $sql = "SELECT *
	            FROM solicitud a 
	            JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
	            JOIN cliente c ON (a.clien_codig = c.clien_codig)
	            JOIN seguridad_usuarios d ON (a.usuar_codig = d.usuar_codig)
	            JOIN p_persona e ON (d.perso_codig = e.perso_codig)
	            WHERE a.estat_codig BETWEEN ".$desde." AND ".$hasta."
	            ORDER BY solic_clvip, prere_codig
	            ";
	    }else{
	        $sql = "SELECT *
	            FROM solicitud a 
	            JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
	            JOIN cliente c ON (a.clien_codig = c.clien_codig)
	            JOIN seguridad_usuarios d ON (a.usuar_codig = d.usuar_codig)
	            JOIN p_persona e ON (d.perso_codig = e.perso_codig)
	            JOIN solicitud_usuario f ON (a.solic_codig = f.solic_codig)
	            WHERE a.estat_codig BETWEEN ".$desde." AND ".$hasta."
	              AND f.usuar_codig = '".Yii::app()->user->id['usuario']['codigo']."'
	            ORDER BY solic_clvip, prere_codig
	            ";
	    }
        return $sql;
	}
	public function SqlListadoPagos($estat)
	{
		$conexion=Yii::app()->db;
		$sql = "SELECT *
                FROM seguridad_usuarios a
                WHERE a.usuar_codig = '".Yii::app()->user->id['usuario']['codigo']."'";
        $usuario = $conexion->createCommand($sql)->queryRow();      
                        
		if($usuario['spniv_codig']=='2'){
            $sql = "SELECT * FROM solicitud_pagos a 
                    JOIN solicitud_estatus_pago b ON (a.epago_codig = b.epago_codig)
                    JOIN solicitud c ON (a.solic_codig = c.solic_codig)
                    JOIN seguridad_usuarios d ON (c.usuar_codig = d.usuar_codig)
                    JOIN p_persona e ON (d.perso_codig = e.perso_codig)

                    JOIN p_pago_tipo f ON (a.ptipo_codig = f.ptipo_codig)
                    AND c.estat_codig in (".$estat.")
                    ";
        }else{
            $sql = "SELECT * FROM solicitud_pagos a 
		            JOIN solicitud_estatus_pago b ON (a.epago_codig = b.epago_codig)
		            JOIN solicitud c ON (a.solic_codig = c.solic_codig)
		            JOIN seguridad_usuarios d ON (c.usuar_codig = d.usuar_codig)
		            JOIN p_persona e ON (d.perso_codig = e.perso_codig)
		            JOIN p_pago_tipo f ON (a.ptipo_codig = f.ptipo_codig)
		            JOIN solicitud_usuario g ON (c.solic_codig = g.solic_codig)
		            WHERE c.estat_codig in (".$estat.")
              		  AND g.usuar_codig = '".Yii::app()->user->id['usuario']['codigo']."'";
        }
        return $sql;
	}
	public function SqlListadoPagosRegistro($estat)
	{
		$conexion=Yii::app()->db;
		$sql = "SELECT *
                FROM seguridad_usuarios a
                WHERE a.usuar_codig = '".Yii::app()->user->id['usuario']['codigo']."'";
        $usuario = $conexion->createCommand($sql)->queryRow();      
                        
		if($usuario['spniv_codig']=='2'){
			
            $sql = "SELECT *, CONCAT(solic_numer,' | ',clien_rsoci) descripcion FROM solicitud a 
            		JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
            		JOIN cliente c ON (a.clien_codig = c.clien_codig)
            		WHERE a.estat_codig in (".$estat.")
            		ORDER BY a.solic_codig";
        }else{
        	$sql = "SELECT *, CONCAT(solic_numer,' | ',clien_rsoci) descripcion  FROM solicitud a 
            		JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
            		JOIN cliente c ON (a.clien_codig = c.clien_codig)
            		JOIN solicitud_usuario g ON (a.solic_codig = g.solic_codig)
		            WHERE a.estat_codig in (".$estat.")
              		  AND g.usuar_codig = '".Yii::app()->user->id['usuario']['codigo']."'
            		ORDER BY a.solic_codig";
        }
        return $sql;
	}
	public function GuardarNotificacion($solic,$estat)
	{
		$usuar=Yii::app()->user->id['usuario']['codigo'];
		$fecha=date('Y-m-d');
		$hora=date('H:i:s');

		$conexion=Yii::app()->db;
		$sql = "SELECT *
                FROM solicitud_usuario a
                WHERE a.solic_codig = '".$solic."'";

        $usuarios = $conexion->createCommand($sql)->queryAll();

        $sql = "SELECT * 
        		FROM p_notificacion_estatus 
        		WHERE estat_codig = '".$estat."'
        		  AND pesta_codig = '1'";

       	$estatus = $conexion->createCommand($sql)->queryRow();

       	$msg = array('success'=>'true','Notificación Guardada correctamente');
       	if($estatus){
       		foreach ($usuarios as $key => $value) {
	        	$sql = "INSERT INTO solicitud_usuario_notificacion(enoti_codig, solic_codig, sunot_leido, usuar_codig, sunot_usuar, sunot_fcrea, sunot_hcrea) 
	        		VALUES ('".$estatus['enoti_codig']."','".$solic."','0','".$value['usuar_codig']."','".$usuar."','".$fecha."','".$hora."')";
				$res = $conexion->createCommand($sql)->execute();        	
				
				if(!$res){
					$msg = array('success'=>'false','Error al guardar la notificación');
				}	
	        }	
       	}
        
        return $msg;
                        
	}

	// FIN PARAMETROS
	/*
		FIN REPORTES
	*/
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}