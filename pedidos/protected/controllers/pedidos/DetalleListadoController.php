<?php

class DetalleListadoController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$f=$_GET['f'];
		$this->redirect(Yii::app()->request->baseUrl.'/ventas/producto/listado?f='.$f);
	}

	public function actionListado()
	{
		$f=$_GET['f'];
		$this->render('listado',array('f' => $f ));
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		$f=$_POST['nfact'];
		if($_POST['descr']){
			$descr=mb_strtoupper($_POST['descr']);
			$condicion.="AND b.inven_descr like '%".$descr."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE factu_codig='".$f."' ".$condicion;
		}
		
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion,'f' => $f ));		
	}
	public function actionConsultar()
	{
		$f=$_GET['f'];
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM factura_producto  a
			  WHERE a.fprod_codig ='".$_GET['c']."'";
		$roles=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('roles' => $roles, 'f' => $f ));
	}
	public function actionRegistrar()
	{
		if($_POST){
			//DATOS DEL FORMULARIO
			$pedid=mb_strtoupper($_POST["pedid"]);
			$codig=mb_strtoupper($_POST["codig"]);
			$ndocu=mb_strtoupper($_POST["ndocu"]);
			$nombr=$_POST["nombr"];
			$talla=mb_strtoupper($_POST["talla"]);
			$tajus=json_encode($_POST["tajus"]);
			$npers=$_POST["npers"];
			$aespa=$_POST["aespa"];
			$elect=$_POST["elect"];
			$timag=$_POST["timag"];
			
			$obser=mb_strtoupper($_POST["obser"]);
			//DATOS DE LOS EMOJI
			$emoji=$_POST["emoji"];
			
			//SIMBOLO SIMPLE
			$simbo=$_POST["simbo"];
			//DATOS DE LOS TEXTO
			$texto=$_POST["texto"];
			//DATOS DE LA IMAGENES
			$file=$_FILES['image'];
			/*$ruta='files/detalle/listado/';
			$name=explode(".", $file['name']);
			$nombre=date('Ymd_His.').end($name);
			$destino=$ruta.$nombre;*/
			foreach ($_FILES['image']['name'] as $key => $value) {
				$ruta='files/detalle/listado/';
				$name[$key]=explode(".", $file['name'][$key]);
				$nombre[$key]=date('Ymd_His_').$key.'.'.end($name[$key]);
				$destino[$key]=$ruta.$nombre[$key];
			}
			
			
			$descr=mb_strtoupper($_POST['descr']);
			
			//DATOS DE AUDITORIA
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$verificar['success']='true';

			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				
				foreach ($timag as $key => $value) {
					if($value!=1 and $value!=3 and $value!=4 and $value!=5){
						$archivo['tmp_name']=$_FILES['image']['tmp_name'][$key];
						$verificar=$this->funciones->CopiarEmoji($archivo,$ruta,$nombre[$key]);
						if($verificar['success']!='true'){
							$transaction->rollBack();
							$msg=$verificar;
							echo json_encode($msg);	
							exit();
						}
					}else{
						$destino[$key]='';	
					}
				}
				if($verificar['success']=='true'){
					$sql="SELECT * FROM pedido_pedidos_detalle WHERE pedid_codig = '".$pedid."' and pdeta_codig = '".$codig."'";
					$detalle=$conexion->createCommand($sql)->queryRow();
					if($detalle){

						$sql="SELECT count(pdlis_codig) p_personas
						  	  FROM pedido_pedidos_detalle_listado
						  	  WHERE pdeta_codig='".$codig."';";
						$cantidad=$conexion->createCommand($sql)->queryRow();
						$p_personas=$cantidad['p_personas']+1;
						
						if($detalle['pdeta_canti']>=$p_personas){

							$sql="SELECT * FROM pedido_pedidos_detalle_listado WHERE pedid_codig = '".$pedid."' AND pdlis_ndocu = '".$ndocu."'";
							$listado=$conexion->createCommand($sql)->queryRow();
							if(!$listado){
								$sql="INSERT INTO pedido_pedidos_detalle_listado( pedid_codig, pdeta_codig, pdlis_ndocu, pdlis_nombr, talla_codig, pdlis_tajus, pdlis_npers, pdlis_aespa, pdlis_elect, pdlis_timag, emoji_codig, pdlis_rimag, pdlis_obser, usuar_codig, pdlis_fcrea, pdlis_hcrea) 
									VALUES ('".$pedid."','".$codig."','".$ndocu."','".$nombr."','".$talla."','".$tajus."','".$npers."','".$aespa."','".$elect."','','','','".$obser."','".$usuar."','".$fecha."','".$hora."')";
								
								$res1=$conexion->createCommand($sql)->execute();
								if($res1){

									$sql='SELECT max(pdlis_codig) pdlis_codig FROM pedido_pedidos_detalle_listado';
									$listado=$conexion->createCommand($sql)->queryRow();
									
									foreach ($timag as $key => $value) {
										$sql="INSERT INTO pedido_detalle_listado_imagen(pedid_codig, pdeta_codig, pdlis_codig, pdlim_timag, emoji_codig, ssimp_codig, pdlim_rimag, pdlim_texto,  usuar_codig, pdlim_fcrea, pdlim_hcrea) 
										VALUES ('".$pedid."','".$codig."','".$listado['pdlis_codig']."','".$value."','".$emoji[$key]."','".$simbo[$key]."','".$destino[$key]."','".$texto[$key]."','".$usuar."','".$fecha."','".$hora."')";
										
										$res2=$conexion->createCommand($sql)->execute();
										if(!$res2){
											$transaction->rollBack();
											$msg=array('success'=>'false','msg'=>'Error al guardar la imagen');
											echo json_encode($msg);
											exit();
										}
									}


									$transaction->commit();
									$msg=array('success'=>'true','msg'=>'Persona guardada correctamente','cantidad'=>$p_personas);	
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al guardar la Persona');
								}	
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'La persona ya esta en el pedido');
							}
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Completo total de p_personas por modelo');
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El detalle del Pedido no existe');
					}
				}else{
					$transaction->rollBack();
					$msg=$verificar;	
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$e=$_GET['e'];
		$this->render('registrar',array('e' => $e ));
		}
	}
	public function actionModificar()
	{
		if($_POST){
			//DATOS DEL FORMULARIO
			$pedid=mb_strtoupper($_POST["pedid"]);
			$detal=mb_strtoupper($_POST["detal"]);
			$codig=mb_strtoupper($_POST["codig"]);
			$ndocu=mb_strtoupper($_POST["ndocu"]);
			$nombr=$_POST["nombr"];
			$talla=mb_strtoupper($_POST["talla"]);
			$tajus=json_encode($_POST["tajus"]);
			$npers=$_POST["npers"];
			$aespa=$_POST["aespa"];
			$elect=$_POST["elect"];
			$timag=$_POST["timag"];
			$emoji=mb_strtoupper($_POST["emoji"]);
			$obser=mb_strtoupper($_POST["obser"]);


			//DATOS DE LOS EMOJI
			$emoji=$_POST["emoji"];
			
			//SIMBOLO SIMPLE
			$simbo=$_POST["simbo"];
			//DATOS DE LOS TEXTO
			$texto=$_POST["texto"];
			//DATOS DE LA IMAGENES
			$file=$_FILES['image'];
			/*$ruta='files/detalle/listado/';
			$name=explode(".", $file['name']);
			$nombre=date('Ymd_His.').end($name);
			$destino=$ruta.$nombre;*/
			foreach ($_FILES['image']['name'] as $key => $value) {
				$ruta='files/detalle/listado/';
				$name[$key]=explode(".", $file['name'][$key]);
				$nombre[$key]=date('Ymd_His_').$key.'.'.end($name[$key]);
				$destino[$key]=$ruta.$nombre[$key];
			}
			$descr=mb_strtoupper($_POST['descr']);
			$image=false;
			//DATOS DE AUDITORIA
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');

			$verificar['success']='true';

			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				foreach ($timag as $key => $value) {
					if($value!=1 and $value!=3 and $value!=4 and $value!=5){
						$archivo['tmp_name']=$_FILES['image']['tmp_name'][$key];
						$verificar=$this->funciones->CopiarEmoji($archivo,$ruta,$nombre[$key]);
						if($verificar['success']!='true'){
							$transaction->rollBack();
							$msg=$verificar;
							echo json_encode($msg);	
							exit();
						}
					}else{
						$destino[$key]='';	
					}
				}
				if($verificar['success']=='true'){
					$sql="SELECT * FROM pedido_pedidos_detalle WHERE pedid_codig = '".$pedid."' and pdeta_codig = '".$detal."'";
					$detalle=$conexion->createCommand($sql)->queryRow();
					if($detalle){
						$sql="SELECT * FROM pedido_pedidos_detalle_listado WHERE pdlis_codig='".$codig."'";
						$listado=$conexion->createCommand($sql)->queryRow();
						
						if($listado){
							
								
								$a=0;
								
								foreach ($timag as $key => $value) {

									$texto[$key]=mb_strtoupper($texto[$key]);
									$sql="SELECT * FROM pedido_detalle_listado_imagen WHERE pdlim_codig ='".$key."' and pdlis_codig ='".$listado['pdlis_codig']."'";
									$result=$conexion->createCommand($sql)->queryRow();
									if($result){
										if($destino[$key]==''){
											$sql="UPDATE pedido_detalle_listado_imagen
												  SET pdlim_rimag = '".$result['pdlim_ruta']."' ,
												      pdlim_texto = '".$texto[$key]."',
												      emoji_codig = '".$emoji[$key]."',
												      ssimp_codig = '".$simbo[$key]."',
												      pdlim_timag = '".$timag[$key]."'
												  WHERE pdlim_codig='".$result['pdlim_codig']."'";	
										}else{
											$sql="UPDATE pedido_detalle_listado_imagen
												  SET pdlim_rimag = '".$destino[$key]."' ,
												      pdlim_texto = '".$texto[$key]."',
												      emoji_codig = '".$emoji[$key]."',
												      ssimp_codig = '".$simbo[$key]."',
												      pdlim_timag = '".$timag[$key]."'
												  WHERE pdlim_codig='".$result['pdlim_codig']."'";
										}
										$res2=$conexion->createCommand($sql)->execute();
										if($a>0){
											$incluyen.=",";
										}
										$incluyen.="'".$key."'";
										
									}else {
										$sql="INSERT INTO pedido_detalle_listado_imagen(pedid_codig, pdeta_codig, pdlis_codig, pdlim_timag, emoji_codig, ssimp_codig, pdlim_rimag, pdlim_texto,  usuar_codig, pdlim_fcrea, pdlim_hcrea) 
										VALUES ('".$pedid."','".$detal."','".$listado['pdlis_codig']."','".$value."','".$emoji[$key]."','".$simbo[$key]."','".$destino[$key]."','".$texto[$key]."','".$usuar."','".$fecha."','".$hora."')";
										$res2=$conexion->createCommand($sql)->execute();
										$sql="SELECT max(pdlim_codig) pdlim_codig FROM pedido_detalle_listado_imagen";
										$res3=$conexion->createCommand($sql)->queryRow();
										if($a>0){
											$incluyen.=",";
										}
										$incluyen.="'".$res3['pdlim_codig']."'";
									}

									$res2=$conexion->createCommand($sql)->execute();

									
									$a++;
								}
								$sql="DELETE  FROM pedido_detalle_listado_imagen 
									  WHERE pedid_codig = '".$pedid."' 
									    AND pdeta_codig='".$detal."'
									  	AND pdlis_codig='".$codig."'
									    AND pdlim_codig not in (".$incluyen.")";
								
								$res2=$conexion->createCommand($sql)->execute();
									
								$sql="UPDATE pedido_pedidos_detalle_listado
									  SET pdlis_ndocu = '".$ndocu."', 
									  	  pdlis_nombr = '".$nombr."', 
									  	  talla_codig = '".$talla."', 
									  	  pdlis_tajus = '".$tajus."', 
									  	  pdlis_npers = '".$npers."', 
									  	  pdlis_aespa = '".$aespa."', 
									  	  pdlis_elect = '".$elect."',  
									  	  pdlis_obser = '".$obser."'
									  WHERE pdlis_codig='".$codig."'";
								$res1=$conexion->createCommand($sql)->execute();
								//if($res1){
									$transaction->commit();
									$msg=array('success'=>'true','msg'=>'Persona modificada correctamente');	
								/*}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al modificar la Persona');
								}*/
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'La persona no esta en el pedido');
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El detalle del Pedido no existe');
					}
				}else{
					$transaction->rollBack();
					$msg=$verificar;	
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$p=$_GET['p'];
			$d=$_GET['d'];
			$s=$_GET['s'];
			$c=$_GET['c'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_pedidos_detalle_listado  a
			  WHERE a.pdlis_codig ='".$_GET['c']."'";
			$lista=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('lista' => $lista,'p'=>$p,'d'=>$d,'s'=>$s,'c'=>$c));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_pedidos_detalle_listado  WHERE pdlis_codig ='".$codig."'";
				$listado=$conexion->createCommand($sql)->queryRow();
				if($listado){

					$sql="DELETE FROM pedido_detalle_listado_imagen  WHERE pdlis_codig ='".$codig."'";
					$res1=$conexion->createCommand($sql)->execute();

					$sql="DELETE FROM pedido_pedidos_detalle_listado  WHERE pdlis_codig ='".$codig."'";
					$res1=$conexion->createCommand($sql)->execute();
					//echo $sql;
					if($res1){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>' Persona eliminada correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al eliminar la persona');	
					}					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'La Entrada no existe');
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$p=$_GET['p'];
			$d=$_GET['d'];
			$s=$_GET['s'];
			$c=$_GET['c'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_pedidos_detalle_listado  a
			  WHERE a.pdlis_codig ='".$_GET['c']."'";
			$lista=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('lista' => $lista,'p'=>$p,'d'=>$d,'s'=>$s,'c'=>$c));
		}
	}
	
	public function TransformarMonto_bd($monto)
	{
		//DEBE DE ESTAR 0.000,00
		$monto=str_replace('.', '', $monto);
		$monto=str_replace(',', '.', $monto);
		//RETORNA 0000.00
		return $monto;
	}
	public function TransformarMonto_v($monto,$cantidad)
	{
		//DEBE DE ESTAR 0000.00
		$monto=number_format($monto,$cantidad,',','.');
		//RETORNA 0.000,00
		return $monto;
	}
	public function TransformarFecha_bd($fecha)
	{
		//DEBE DE ESTAR DD/MM/AAAA
		$fecha=explode('/',$fecha);
		$fecha=$fecha[2].'-'.$fecha[1].'-'.$fecha[0];
		//RETORNA AAAA-MM-DD
		return $fecha;
	}
	public function TransformarFecha_v($fecha)
	{
		//DEBE DE ESTAR AAAA-MM-DD
		$fecha=explode('/',$fecha);
		$fecha=$fecha[2].'-'.$fecha[1].'-'.$fecha[0];
		//RETORNA DD/MM/AAAA
		return $fecha;
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}