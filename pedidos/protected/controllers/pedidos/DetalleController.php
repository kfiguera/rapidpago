<?php

class DetalleController extends Controller
{
	public $funciones;

	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}

	public function actionIndex()
	{
		$p=$_GET['p'];
		$this->redirect(Yii::app()->request->baseUrl.'/pedidos/detalle/listado?p='.$p);
	}

	public function actionListado()
	{
		$p=$_GET['p'];
		$conexion=Yii::app()->db;
		$sql="SELECT * , (SELECT COUNT(pdeta_codig) FROM pedido_pedidos_detalle b WHERE a.pedid_codig = b.pedid_codig) modelos, (SELECT COUNT(pdlis_codig) FROM pedido_pedidos_detalle_listado b WHERE a.pedid_codig = b.pedid_codig) p_personas
			  FROM pedido_pedidos  a
			  WHERE a.pedid_codig ='".$_GET['p']."'";
		$pedidos=$conexion->createCommand($sql)->queryRow();
		$this->render('listado', array('p' => $p,'pedidos' => $pedidos));

	}
	public function actionBuscar()
	{
		$p=$_GET['p'];
		$condicion='';
		$con=0;
		if($_POST['numero']){
			$numero=mb_strtoupper($_POST['numero']);
			$condicion.="a.pedid_numer like '%".$numero."%' ";
			$con++;
		}
		if($_POST['epedi']){
			if($con>0){
				$condicion.="AND ";
			}
			$epedi=mb_strtoupper($_POST['epedi']);
			$condicion.="a.epedi_codig like '%".$epedi."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE pedid_codig='".$p."' ".$condicion;
		}
		
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('p' => $p,'condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$p=$_GET['p'];
		$c=$_GET['c'];
		$s=$_GET['s'];
		$conexion=Yii::app()->db;
		$sql="SELECT * 
		  FROM pedido_pedidos_detalle  a
		  WHERE a.pdeta_codig ='".$_GET['c']."'";
		$pedidos=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('pedidos' => $pedidos, 'ubicacion' => $ubicacion,'p' => $p,'c' => $c,'s' => $s));
	}
	public function actionRegistrar()
	{
		if($_POST){

			//Datos del Detalle
			$pedid=mb_strtoupper($_POST["pedid"]);
			$tmode=mb_strtoupper($_POST["tmode"]);
			$model=mb_strtoupper($_POST["model"]);
			$categ=mb_strtoupper($_POST["categ"]);
			$varia=mb_strtoupper($_POST["varia"]);
			$color=mb_strtoupper($_POST["color"]);
			$canti=mb_strtoupper($_POST["canti"]);
			$opcio=mb_strtoupper($_POST["opcio"]);
			$iopci=mb_strtoupper($_POST["iopci"]);
			$iopc2=mb_strtoupper($_POST["iopc2"]);
			$ideta=mb_strtoupper($_POST["ideta"]);
			$iclin=mb_strtoupper($_POST["iclin"]);
			$iccie=mb_strtoupper($_POST["iccie"]);
			$icviv=mb_strtoupper($_POST["icviv"]);
			$mensa=mb_strtoupper($_POST["mensa"]);

			switch ($iopci) {
				case '1':
					$copci=$iclin;
					break;
				case '2':
					$copci=$iccie;
					break;
				case '3':
					$copci=$icviv;
					break;
			}
			//Datos de Auditoria
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();

			$monto=0;
			$pasos=2;
			try{
				
				$sql="SELECT * FROM pedido_pedidos WHERE pedid_codig = '".$pedid."' ";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				if($pedidos){
					$sql="SELECT COUNT(pdeta_codig) modelos, SUM(pdeta_canti) p_personas 
						  FROM pedido_pedidos_detalle 
						  WHERE pedid_codig = '".$pedid."' ";

					$cantidad=$conexion->createCommand($sql)->queryRow();
					//if($cantidad['modelos']<$pedidos['pedid_cmode']){
						$p_personas=$cantidad['p_personas']+$canti;
						//if($p_personas<=$pedidos['pedid_total']){
							$sql="INSERT INTO pedido_pedidos_detalle(pedid_codig, tmode_codig, model_codig, mcate_codig, mvari_codig, mcolo_codig, pdeta_canti, pdeta_opci, mopci_codig, mopci_codi2, pdeta_dopci, pdeta_copci, pdeta_mensa, pdeta_monto, pdeta_pasos, usuar_codig, pdeta_fcrea, pdeta_hcrea) 
							VALUES ('".$pedid."','".$tmode."','".$model."','".$categ."','".$varia."','".$color."','".$canti."','".$opcio."','".$iopci."','".$iopc2."','".$ideta."','".$copci."','".$mensa."','".$monto."','".$pasos."','".$usuar."','".$fecha."','".$hora."');";
							$res1=$conexion->createCommand($sql)->execute();
							if($res1){
								$transaction->commit();
								$sql="SELECT max(pdeta_codig) codigo FROM pedido_pedidos_detalle";
								$pedido=$conexion->createCommand($sql)->queryRow();
								$msg=array('success'=>'true','msg'=>'Detalle guardado correctamente','c'=>$pedido['codigo']);	
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al guardar el Detalle');	
							}
						/*}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'La cantidad de p_personas a agregar supera la cantidad del pedido');	
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Ya no se pueden agregar mas modelos al pedido');
					}*/
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El pedido no existe');
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$p=$_GET['p'];
			$s=1;

			$this->render('registrar',array('p' => $p,'s' => $s ));
		}
	}
	public function actionPaso1()
	{
		if($_POST){
			//Datos del Detalle
			$codig=mb_strtoupper($_POST["codig"]);
			$pedid=mb_strtoupper($_POST["pedid"]);
			$pasos=mb_strtoupper($_POST["pasos"]);
			$tmode=mb_strtoupper($_POST["tmode"]);
			$model=mb_strtoupper($_POST["model"]);
			$categ=mb_strtoupper($_POST["categ"]);
			$varia=mb_strtoupper($_POST["varia"]);
			//$color=mb_strtoupper($_POST["color"]);
			$canti=mb_strtoupper($_POST["canti"]);
			$opcio=mb_strtoupper($_POST["opcio"]);
			$iopci=mb_strtoupper($_POST["iopci"]);

			$iopc2=mb_strtoupper($_POST["iopc2"]);
			$ideta=mb_strtoupper($_POST["ideta"]);
			$iclin=mb_strtoupper($_POST["iclin"]);
			$iccie=mb_strtoupper($_POST["iccie"]);
			$icviv=mb_strtoupper($_POST["icviv"]);
			$mensa=mb_strtoupper($_POST["mensa"]);
			switch ($iopci) {
				case '1':
					$copci=$iclin;
					break;
				case '2':
					$copci=$iccie;
					break;
				case '3':
					$copci=$icviv;
					break;
			}
			
			//Datos de Auditoria
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();

			$monto=0;
			if($pasos<'2'){
				$pasos='2';
			}
			try{
				$sql="SELECT * FROM pedido_pedidos_detalle
					WHERE pdeta_codig='".$codig."';";
				$detalle=$conexion->createCommand($sql)->queryRow();
				if($detalle){
				/*$sql="UPDATE pedido_pedidos_detalle
					SET pedid_codig = '".$pedid."',
						tmode_codig = '".$tmode."',
						model_codig = '".$model."',
						mcate_codig = '".$categ."',
						mvari_codig = '".$varia."',
						mcolo_codig = '".$color."',
						pdeta_canti = '".$canti."',
						pdeta_opci = '".$opcio."',
						mopci_codig = '".$iopci."',
						pdeta_dopci = '".$ideta."',
						pdeta_copci = '".$copci."',
						pdeta_mensa = '".$mensa."',
						pdeta_monto = '0',
						pdeta_pasos = '".$pasos."'
					WHERE pdeta_codig = '".$codig."'";*/
					$sql="UPDATE pedido_pedidos_detalle
					SET pedid_codig = '".$pedid."',
						tmode_codig = '".$tmode."',
						model_codig = '".$model."',
						mcate_codig = '".$categ."',
						mvari_codig = '".$varia."',
						pdeta_canti = '".$canti."',
						pdeta_opci = '".$opcio."',
						mopci_codig = '".$iopci."',
						mopci_codi2 = '".$iopc2."',
						pdeta_dopci = '".$ideta."',
						pdeta_copci = '".$copci."',
						pdeta_mensa = '".$mensa."',
						pdeta_monto = '0',
						pdeta_pasos = '".$pasos."'
					WHERE pdeta_codig = '".$codig."'";
					
					$res1=$conexion->createCommand($sql)->execute();
					if($res1 or $detalle['pdeta_pasos']>='2'){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Detalle guardado correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al guardar el Detalle');	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error no existe el Detalle');	
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);

		}else{
			$p=$_GET['p'];
			$s=1;

			$this->render('registrar',array('p' => $p,'s' => $s ));
		}
	}
	public function actionPaso2()
	{
		if($_POST){
			//Datos del Detalle
			$pedid=mb_strtoupper($_POST["pedid"]);
			$codig=mb_strtoupper($_POST["codig"]);
			$pasos=mb_strtoupper($_POST["pasos"]);
			//Datos de Auditoria
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();

			$monto=0;
			if($pasos<'2'){
				$pasos='2';
			}
			$pasos='2';
			try{
				$sql="SELECT * FROM pedido_pedidos_detalle
					WHERE pdeta_codig='".$codig."';";
				$detalle=$conexion->createCommand($sql)->queryRow();
				if($detalle){
					$sql="UPDATE pedido_pedidos_detalle
					  SET pdeta_pasos='".$pasos."' 
					WHERE pdeta_codig='".$codig."';";
					$res1=$conexion->createCommand($sql)->execute();
					
					if($res1 or $detalle['pdeta_pasos']>='2'){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Detalle guardado correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al guardar el Detalle');	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error no existe el Detalle');	
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);

		}else{
			$p=$_GET['p'];
			$s=1;

			$this->render('registrar',array('p' => $p,'s' => $s ));
		}
	}
	public function actionPaso3()
	{
		if($_POST){
			//Datos del Detalle
			$pedid=mb_strtoupper($_POST['pedid']);
			$codig=mb_strtoupper($_POST['codig']);
			$pasos=mb_strtoupper($_POST['pasos']);

			$npfue=mb_strtoupper($_POST['npfue']);
			$npcol=mb_strtoupper($_POST['npcol']);
			$nppos=mb_strtoupper($_POST['nppos']);
			$aefue=mb_strtoupper($_POST['aefue']);
			$aecol=mb_strtoupper($_POST['aecol']);
			$aepos=mb_strtoupper($_POST['aepos']);
			$pposi=mb_strtoupper($_POST['pposi']);
			$ptder=mb_strtoupper($_POST['ptder']);
			$ptizq=mb_strtoupper($_POST['ptizq']);
			$pfuen=mb_strtoupper($_POST['pfuen']);
			$pcolo=mb_strtoupper($_POST['pcolo']);
			$pobse=mb_strtoupper($_POST['pobse']);
			$gposi=mb_strtoupper($_POST['gposi']);
			$gfuen=mb_strtoupper($_POST['gfuen']);
			$gcolo=mb_strtoupper($_POST['gcolo']);
			$gobse=mb_strtoupper($_POST['gobse']);
			//Datos de Auditoria
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();

			$monto=0;
			if($pasos<'4'){
				$pasos='4';
			}
			try{
				$sql="SELECT * FROM pedido_pedidos_detalle
					WHERE pdeta_codig='".$codig."';";
				$detalle=$conexion->createCommand($sql)->queryRow();
				if($detalle){
					$sql="UPDATE pedido_pedidos_detalle
					  SET pdeta_npfue='".$npfue."',
						  pdeta_npcol='".$npcol."',
						  pdeta_nppos='".$nppos."',
						  pdeta_aefue='".$aefue."',
						  pdeta_aecol='".$aecol."',
						  pdeta_aepos='".$aepos."',
						  pdeta_pposi='".$pposi."',
						  pdeta_ptder='".$ptder."',
						  pdeta_ptizq='".$ptizq."',
						  pdeta_pfuen='".$pfuen."',
						  pdeta_pcolo='".$pcolo."',
						  pdeta_pobse='".$pobse."',
						  pdeta_gposi='".$gposi."',
						  pdeta_gfuen='".$gfuen."',
						  pdeta_gcolo='".$gcolo."',
						  pdeta_gobse='".$gobse."',
						  pdeta_pasos='".$pasos."' 
					WHERE pdeta_codig='".$codig."';";
					$res1=$conexion->createCommand($sql)->execute();
					
					if($res1 or $detalle['pdeta_pasos']>='4'){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Detalle guardado correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al guardar el Detalle');	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error no existe el Detalle');	
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);

		}else{
			$p=$_GET['p'];
			$s=1;

			$this->render('registrar',array('p' => $p,'s' => $s ));
		}
	}
	public function actionPaso4()
	{
		if($_POST){
			//Datos del Detalle
			$pedid=mb_strtoupper($_POST['pedid']);
			$codig=mb_strtoupper($_POST['codig']);
			$pasos=mb_strtoupper($_POST['pasos']);
			//Datos del Formulario
			$mposi=mb_strtoupper($_POST['mposi']);
			$mtder=mb_strtoupper($_POST['mtder']);
			$mtizq=mb_strtoupper($_POST['mtizq']);
			$mfuen=mb_strtoupper($_POST['mfuen']);
			$mcolo=mb_strtoupper($_POST['mcolo']);
			$limag=mb_strtoupper($_POST['limag']);
			$mobse=mb_strtoupper($_POST['mobse']);
			$eatbo=mb_strtoupper($_POST['eatbo']);
			$eafue=mb_strtoupper($_POST['eafue']);
			$eacol=mb_strtoupper($_POST['eacol']);
			$ebtbo=mb_strtoupper($_POST['ebtbo']);
			$ebfue=mb_strtoupper($_POST['ebfue']);
			$ebcol=mb_strtoupper($_POST['ebcol']);
			$aetbo=mb_strtoupper($_POST['aetbo']);
			$aefue=mb_strtoupper($_POST['aefue']);
			$aecol=mb_strtoupper($_POST['aecol']);
			$eobse=mb_strtoupper($_POST['eobse']);

			//DATOS DE LA IMAGEN
			$file['mrima']=$_FILES['mrima'];
			$ruta['mrima']='files/detalle/pedido/'.$pedid.'/'.$codig.'/';
			$name['mrima']=explode(".", $file['mrima']['name']);
			$nombre['mrima']=date('Ymd_His').'_mrima.'.end($name['mrima']);
			$destino['mrima']=$ruta['mrima'].$nombre['mrima'];

			$file['iprut']=$_FILES['iprut'];
			$ruta['iprut']='files/detalle/pedido/'.$pedid.'/'.$codig.'/';
			$name['iprut']=explode(".", $file['iprut']['name']);
			$nombre['iprut']=date('Ymd_His').'_iprut.'.end($name['iprut']);
			$destino['iprut']=$ruta['iprut'].$nombre['iprut'];

			$file['rifro']=$_FILES['rifro'];
			$ruta['rifro']='files/detalle/pedido/'.$pedid.'/'.$codig.'/';
			$name['rifro']=explode(".", $file['rifro']['name']);
			$nombre['rifro']=date('Ymd_His').'_rifro.'.end($name['rifro']);
			$destino['rifro']=$ruta['rifro'].$nombre['rifro'];

			$file['riesp']=$_FILES['riesp'];
			$ruta['riesp']='files/detalle/pedido/'.$pedid.'/'.$codig.'/';
			$name['riesp']=explode(".", $file['riesp']['name']);
			$nombre['riesp']=date('Ymd_His').'_riesp.'.end($name['riesp']);
			$destino['riesp']=$ruta['riesp'].$nombre['riesp'];


			//Datos de Auditoria
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();

			$monto=0;
			if($pasos<'4'){
				$pasos='4';
			}
			try{
				if(!empty($file['mrima']['name'])){
					$verificar['mrima']=$this->funciones->CopiarEmoji($file['mrima'],$ruta['mrima'],$nombre['mrima']);	
					$image['mrima']=true;
				}else{
					$destino['mrima']='';
					$verificar['mrima']['success']='true';
				}
				if(!empty($file['iprut']['name'])){
					$verificar['iprut']=$this->funciones->CopiarEmoji($file['iprut'],$ruta['iprut'],$nombre['iprut']);
					$image['iprut']=true;
				}else{
					$destino['iprut']='';
					$verificar['iprut']['success']='true';
				}
				if(!empty($file['rifro']['name'])){
					$verificar['rifro']=$this->funciones->CopiarEmoji($file['rifro'],$ruta['rifro'],$nombre['rifro']);
					$image['rifro']=true;
				}else{
					$destino['rifro']='';
					$verificar['rifro']['success']='true';
				}
				if(!empty($file['riesp']['name'])){
					$verificar['riesp']=$this->funciones->CopiarEmoji($file['riesp'],$ruta['riesp'],$nombre['riesp']);
					$image['riesp']=true;
				}else{
					$destino['riesp']='';
					$verificar['riesp']['success']='true';
				}
				if($verificar['mrima']['success']=='true' and $verificar['iprut']['success']=='true' and $verificar['rifro']['success']=='true' and $verificar['riesp']['success']=='true'){
					
					$sql="SELECT * FROM pedido_pedidos_detalle
						WHERE pdeta_codig='".$codig."';";
					$detalle=$conexion->createCommand($sql)->queryRow();
					if($image['mrima']==false){
						$destino['mrima']=$detalle['pdeta_mrima'];
					}
					if($image['iprut']==false){
						$destino['iprut']=$detalle['pdeta_iprut'];
					}
					if($image['rifro']==false){
						$destino['rifro']=$detalle['pdeta_rifro'];
					}
					if($image['riesp']==false){
						$destino['riesp']=$detalle['pdeta_riesp'];
					}
					if($detalle){

						$sql="UPDATE pedido_pedidos_detalle
						  SET pdeta_mposi='".$mposi."',
							  pdeta_mtder='".$mtder."',
							  pdeta_mtizq='".$mtizq."',
							  pdeta_mfuen='".$mfuen."',
							  pdeta_mcolo='".$mcolo."',
							  pdeta_limag='".$limag."',
							  pdeta_mobse='".$mobse."',
							  pdeta_eatbo='".$eatbo."',
							  pdeta_eafue='".$eafue."',
							  pdeta_eacol='".$eacol."',
							  pdeta_ebtbo='".$ebtbo."',
							  pdeta_ebfue='".$ebfue."',
							  pdeta_ebcol='".$ebcol."',
							  pdeta_aetbo='".$aetbo."',
							  pdeta_aefue='".$aefue."',
							  pdeta_aecol='".$aecol."',
							  pdeta_eobse='".$eobse."',
							  pdeta_mrima='".$destino['mrima']."',
							  pdeta_iprut='".$destino['iprut']."',
							  pdeta_rifro='".$destino['rifro']."',
							  pdeta_riesp='".$destino['riesp']."',
							  pdeta_pasos='".$pasos."' 
						WHERE pdeta_codig='".$codig."';";
						
						$res1=$conexion->createCommand($sql)->execute();
						
						if($res1 or $detalle['pdeta_pasos']>='4'){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Detalle guardado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al guardar el Detalle');	
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error no existe el Detalle');	
					}
				}else{
					$transaction->rollBack();
					$msg=$verificar;	
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);

		}else{
			$p=$_GET['p'];
			$s=1;

			$this->render('registrar',array('p' => $p,'s' => $s ));
		}
	}
	public function actionPaso2Listado(){
		$p=$_GET['p'];
		$c=$_GET['c'];
		$s=$_GET['s'];
		$this->renderpartial('paso2_listado',array('p' => $p,'s' => $s ,'c' => $c ));
	}
	public function actionModificar()
	{
		if($_POST){
			//Datos del Pedido
			$codig=mb_strtoupper($_POST["codig"]);
			$canti=mb_strtoupper($_POST["canti"]);
			$mcant=mb_strtoupper($_POST["mcant"]);
			$color=mb_strtoupper($_POST["color"]);
			$mensa=mb_strtoupper($_POST["mensa"]);
			//Datos de Auditoria
			$estat=1;
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				
				$sql="SELECT * FROM pedido_pedidos WHERE pedid_codig ='".$codig."'";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				if($pedidos){
					$sql="UPDATE pedido_pedidos
						  SET pedid_total='".$canti."',
							  pedid_cmode='".$mcant."',
							  color_codig='".$color."',
							  pedid_obser='".$mensa."'
						  WHERE pedid_codig ='".$codig."'";
					$res1=$conexion->createCommand($sql)->execute();
					if($res1){
						$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Pedido actualizado correctamente');
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al actualizar el Pedido');	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$p=$_GET['p'];
			$c=$_GET['c'];
			$s=$_GET['s'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_pedidos_detalle  a
			  WHERE a.pdeta_codig ='".$_GET['c']."'";
			$pedidos=$conexion->createCommand($sql)->queryRow();
			$ubicacion[0]='start ';
			$ubicacion[1]='finish ';
			for ($i=0; $i < 2 ; $i++) { 
				if($i<$pedidos['pdeta_pasos']){
					$ubicacion[$i].='upcoming ';
				}
				if ($i==($s-1)) {
					$ubicacion[$i].='active ';
				}
			}
			
			switch ($s) {
				case '1':
					$this->render('paso1', array('pedidos' => $pedidos,'ubicacion' => $ubicacion, 'p' => $p,'c' => $c,'s' => $s));
					break;
				case '2':
					$this->render('paso2', array('pedidos' => $pedidos, 'ubicacion' => $ubicacion,'p' => $p,'c' => $c,'s' => $s));
					break;
				/*case '3':
					$this->render('paso3', array('pedidos' => $pedidos, 'ubicacion' => $ubicacion,'p' => $p,'c' => $c,'s' => $s));
					break;
				case '4':
					$this->render('paso4', array('pedidos' => $pedidos, 'ubicacion' => $ubicacion,'p' => $p,'c' => $c,'s' => $s));
					break;*/
				default:
					$this->render('registrar', array('pedidos' => $pedidos,'ubicacion' => $ubicacion));
					break;
			}
			
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_pedidos_detalle  WHERE pdeta_codig ='".$codig."'";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				if($pedidos){
					$sql="SELECT * FROM pedido_pedidos_detalle_listado  WHERE pdeta_codig ='".$codig."'";
					$lista=$conexion->createCommand($sql)->queryRow();
					if(!$lista){	
						$sql="DELETE FROM pedido_pedidos_detalle WHERE pdeta_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();

						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Detalle eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Detalle');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'No se Puede Eliminar tiene p_personas asociadas al detalle');	
					}	
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Detalle no existe');
				}
				
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$p=$_GET['p'];
			$c=$_GET['c'];
			$s=$_GET['s'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_pedidos_detalle  a
			  WHERE a.pdeta_codig ='".$_GET['c']."'";
			$pedidos=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('pedidos' => $pedidos, 'ubicacion' => $ubicacion,'p' => $p,'c' => $c,'s' => $s));
		}
	}

	
	
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}