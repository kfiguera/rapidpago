<?php

class PagosController extends Controller
{
	public $funciones;

	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}

	public function actionIndex()
	{
		$this->redirect(Yii::app()->request->baseUrl.'/pedidos/pedidos/listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['numero']){
			$numero=mb_strtoupper($_POST['numero']);
			$condicion.="a.solic_numer like '%".$numero."%' ";
			$con++;
		}
		if($_POST['epago']){
			if($con>0){
				$condicion.="AND ";
			}
			$epedi=mb_strtoupper($_POST['epedi']);
			$condicion.="a.epago_codig like '%".$epedi."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM solicitud  a
			  WHERE a.solic_codig ='".$_GET['c']."'";
		$pedidos=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('pedidos' => $pedidos));
	}
	public function actionRegistrar()
	{
		if($_POST){
			//Datos del Pedido
			$pedid=mb_strtoupper($_POST["pedid"]);
			$fpago=mb_strtoupper($_POST["fpago"]);
			$refer=mb_strtoupper($_POST["refer"]);
			$monto=$this->funciones->transformarMonto_bd(mb_strtoupper($_POST["monto"]));
			$obser=mb_strtoupper($_POST["obser"]);
			//Datos de la Imagen
			$file=$_FILES['prima'];
			$ruta='files/detalle/pedido/'.$pedid.'/pagos/';
			$name=explode(".", $file['name']);
			
			//Datos de Auditoria
			$estat=1;
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();

			try{
				if($pedid!=''){


				$sql="SELECT a.* FROM solicitud a 
				WHERE a.solic_codig='".$pedid."'";

				$pedido=$conexion->createCommand($sql)->queryRow();
				$mpedi=$pedido['solic_monto']/2;
				//if($monto==$mpedi){

				

					//Obtenemos el codigo del pago 
					$sql="SELECT max(pagos_codig) codigo FROM solicitud_pagos";
					$pagos=$conexion->createCommand($sql)->queryRow();
					$codig=$pagos['codigo']+1;
					$cpago='P'.$this->funciones->generarCodigoPedido(5,$codig);
					$nombre=date('Ymd_His').'_'.$cpago.'.'.end($name);
					$destino=$ruta.$nombre;

					if(!empty($file['name'])){
						$verificar=$this->funciones->CopiarEmoji($file,$ruta,$nombre);	
						$image=true;
					}else{
						$verificar['success']='true';
						$destino='';
					}
					if($verificar['success']=='true'){
						//Guardamos el Pedido
						$sql="INSERT INTO solicitud_pagos(pagos_numer, solic_codig, ptipo_codig, epago_codig, pagos_refer, pagos_monto, pagos_rimag, pagos_obser, usuar_codig, pagos_fcrea, pagos_hcrea) 
							VALUES ('".$cpago."', '".$pedid."', '".$fpago."', '".$estat."', '".$refer."', '".$monto."', '".$destino."', '".$obser."', '".$usuar."', '".$fecha."', '".$hora."')";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$sql="SELECT a.*, b.*
								  FROM p_persona a
								  JOIN seguridad_usuarios b ON (a.perso_codig=b.perso_codig)
								  WHERE b.usuar_codig='".$usuar."'";
	                		$p_persona=$conexion->createCommand($sql)->queryRow();

							$datos['row']=$p_persona;
							$corre=$p_persona['usuar_login'];
	                        $this->funciones->enviarCorreo('../pedidos/pagos/registro_correo',$datos,'Polerones Tiempo | Registro de Pago',$corre);
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Pago guardado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al guardar el Pago');	
						}	
					}else{
						$transaction->rollBack();
						$msg=$verificar;	
					}
				/*}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error al guardar el Pago, el monto del pago debe ser el 50% del pedido');
				}*/
			}else{
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Debe seleccionar un pedido');
			}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionPaso1()
	{
		if($_POST){

			//Datos del Detalle
			$pedid=mb_strtoupper($_POST['pedid']);
			$codig=mb_strtoupper($_POST['codig']);
			$pasos=mb_strtoupper($_POST['pasos']);
			$ccuer=mb_strtoupper($_POST['ccuer']);
			$cbols=mb_strtoupper($_POST['cbols']);
			$cmang=mb_strtoupper($_POST['cmang']);
			$cegor=mb_strtoupper($_POST['cegor']);
			$cigor=mb_strtoupper($_POST['cigor']);
			$cppre=mb_strtoupper($_POST['cppre']);
			$cierr=mb_strtoupper($_POST['cierr']);
			$ccier=mb_strtoupper($_POST['ccier']);
			$broch=mb_strtoupper($_POST['broch']);
			$ccurs=mb_strtoupper($_POST['ccurs']);
			$cnper=mb_strtoupper($_POST['cnper']);
			$caesp=mb_strtoupper($_POST['caesp']);
			$celec=mb_strtoupper($_POST['celec']);
			$cfras=mb_strtoupper($_POST['cfras']);
			$otros=mb_strtoupper($_POST['otros']);
			
			//Datos de Auditoria
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();

			$monto=0;
			if($pasos<'2'){
				$pasos='2';
			}
			try{
				$sql="SELECT * FROM solicitud
					WHERE solic_codig='".$codig."';";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				if($pedidos){
					$sql="UPDATE solicitud
					    SET solic_ccuer = '".$ccuer."',
						solic_cbols = '".$cbols."',
						solic_cmang = '".$cmang."',
						solic_cegor = '".$cegor."',
						solic_cigor = '".$cigor."',
						solic_cppre = '".$cppre."',
						solic_cierr = '".$cierr."',
						solic_ccier = '".$ccier."',
						solic_broch = '".$broch."',
						solic_ccurs = '".$ccurs."',
						solic_cnper = '".$cnper."',
						solic_caesp = '".$caesp."',
						solic_celec = '".$celec."',
						solic_cfras = '".$cfras."',
						solic_otros = '".$otros."',
						solic_pasos = '".$pasos."'
					WHERE solic_codig = '".$codig."'";
					
					$res1=$conexion->createCommand($sql)->execute();
					if($res1 or $pedidos['solic_pasos']>='2'){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Pedido actualizado correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al actualizar el Pedido');	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error no existe el Pedido');	
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);

		}else{
			$p=$_GET['p'];
			$s=1;

			$this->render('registrar',array('p' => $p,'s' => $s ));
		}
	}

	public function actionPaso2()
	{
		if($_POST){
			//Datos del Detalle
			$pedid=mb_strtoupper($_POST['pedid']);
			$codig=mb_strtoupper($_POST['codig']);
			$pasos=mb_strtoupper($_POST['pasos']);
			//Datos del Formulario
			$pposi = mb_strtoupper($_POST['pposi']);
			$pfuen = mb_strtoupper($_POST['pfuen']);
			$pcolo = mb_strtoupper($_POST['pcolo']);
			$plima = mb_strtoupper($_POST['plima']);
			$pobse = mb_strtoupper($_POST['pobse']);
			$gposi = mb_strtoupper($_POST['gposi']);
			$gfuen = mb_strtoupper($_POST['gfuen']);
			$gcolo = mb_strtoupper($_POST['gcolo']);
			$glima = mb_strtoupper($_POST['glima']);
			$gobse = mb_strtoupper($_POST['gobse']);

			//DATOS DE LA IMAGEN
			$file['prima']=$_FILES['prima'];
			$ruta['prima']='files/detalle/pedido/'.$pedid.'/'.$codig.'/';
			$name['prima']=explode(".", $file['prima']['name']);
			$nombre['prima']=date('Ymd_His').'_iprut.'.end($name['prima']);
			$destino['prima']=$ruta['prima'].$nombre['prima'];

			$file['grima']=$_FILES['grima'];
			$ruta['grima']='files/detalle/pedido/'.$pedid.'/'.$codig.'/';
			$name['grima']=explode(".", $file['grima']['name']);
			$nombre['grima']=date('Ymd_His').'_grima.'.end($name['grima']);
			$destino['grima']=$ruta['grima'].$nombre['grima'];

			//Datos de Auditoria
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();

			$monto=0;
			if($pasos<'3'){
				$pasos='3';
			}
			try{
				if(!empty($file['grima']['name'])){
					$verificar['grima']=$this->funciones->CopiarEmoji($file['grima'],$ruta['grima'],$nombre['grima']);	
					$image['grima']=true;
				}else{
					$destino['grima']='';
					$verificar['grima']['success']='true';
				}
				if(!empty($file['prima']['name'])){
					$verificar['prima']=$this->funciones->CopiarEmoji($file['prima'],$ruta['prima'],$nombre['prima']);
					$image['prima']=true;
				}else{
					$destino['prima']='';
					$verificar['prima']['success']='true';
				}
				
				if($verificar['grima']['success']=='true' and $verificar['prima']['success']=='true' ){
					
					$sql="SELECT * FROM solicitud
						WHERE solic_codig='".$codig."';";
					$pedidos=$conexion->createCommand($sql)->queryRow();
					if($image['grima']==false){
						$destino['grima']=$pedidos['pdeta_grima'];
					}
					if($image['prima']==false){
						$destino['prima']=$pedidos['pdeta_prima'];
					}
					
					if($pedidos){

						$sql="UPDATE solicitud
						  SET solic_pposi = '".$pposi."',
							  solic_pfuen = '".$pfuen."',
							  solic_pcolo = '".$pcolo."',
							  solic_plima = '".$plima."',
							  solic_prima = '".$destino['prima']."',
							  solic_pobse = '".$pobse."',
							  solic_gposi = '".$gposi."',
							  solic_gfuen = '".$gfuen."',
							  solic_gcolo = '".$gcolo."',
							  solic_glima = '".$glima."',
							  solic_grima = '".$destino['grima']."',
							  solic_gobse = '".$gobse."',
							  solic_pasos='".$pasos."' 
						WHERE solic_codig='".$codig."';";
						
						$res1=$conexion->createCommand($sql)->execute();
						
						if($res1 or $pedidos['pdeta_pasos']>='4'){
							

							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Pedido actualizado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar el Pedido');	
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error no existe el Pedido');	
					}
				}else{
					$transaction->rollBack();
					$msg=$verificar;	
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);

		}else{
			$p=$_GET['p'];
			$s=1;

			$this->render('registrar',array('p' => $p,'s' => $s ));
		}
	}
	public function actionPaso3()
	{
		if($_POST){

			//Datos del Detalle
			$pedid=mb_strtoupper($_POST['pedid']);
			$codig=mb_strtoupper($_POST['codig']);
			$pasos=mb_strtoupper($_POST['pasos']);
			//Datos del Formulario
			$mposi=mb_strtoupper($_POST['mposi']);
			$mfuen=mb_strtoupper($_POST['mfuen']);
			$mcolo=mb_strtoupper($_POST['mcolo']);
			$limag=mb_strtoupper($_POST['limag']);
			$mobse=mb_strtoupper($_POST['mobse']);
			$eatbo=mb_strtoupper($_POST['eatbo']);
			$eafue=mb_strtoupper($_POST['eafue']);
			$eacol=mb_strtoupper($_POST['eacol']);
			$ebtbo=mb_strtoupper($_POST['ebtbo']);
			$ebfue=mb_strtoupper($_POST['ebfue']);
			$ebcol=mb_strtoupper($_POST['ebcol']);
			$aefue=mb_strtoupper($_POST['aefue']);
			$aecol=mb_strtoupper($_POST['aecol']);
			$eobse=mb_strtoupper($_POST['eobse']);

			//DATOS DE LA IMAGEN
			$file['mrima']=$_FILES['mrima'];
			$ruta['mrima']='files/detalle/pedido/'.$pedid.'/'.$codig.'/';
			$name['mrima']=explode(".", $file['mrima']['name']);
			$nombre['mrima']=date('Ymd_His').'_mrima.'.end($name['mrima']);
			$destino['mrima']=$ruta['mrima'].$nombre['mrima'];

			$file['iprut']=$_FILES['iprut'];
			$ruta['iprut']='files/detalle/pedido/'.$pedid.'/'.$codig.'/';
			$name['iprut']=explode(".", $file['iprut']['name']);
			$nombre['iprut']=date('Ymd_His').'_iprut.'.end($name['iprut']);
			$destino['iprut']=$ruta['iprut'].$nombre['iprut'];

			$file['rifro']=$_FILES['rifro'];
			$ruta['rifro']='files/detalle/pedido/'.$pedid.'/'.$codig.'/';
			$name['rifro']=explode(".", $file['rifro']['name']);
			$nombre['rifro']=date('Ymd_His').'_rifro.'.end($name['rifro']);
			$destino['rifro']=$ruta['rifro'].$nombre['rifro'];

			$file['riesp']=$_FILES['riesp'];
			$ruta['riesp']='files/detalle/pedido/'.$pedid.'/'.$codig.'/';
			$name['riesp']=explode(".", $file['riesp']['name']);
			$nombre['riesp']=date('Ymd_His').'_riesp.'.end($name['riesp']);
			$destino['riesp']=$ruta['riesp'].$nombre['riesp'];


			//Datos de Auditoria
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();

			$monto=0;
			if($pasos<'3'){
				$pasos='3';
			}
			try{
				if(!empty($file['mrima']['name'])){
					$verificar['mrima']=$this->funciones->CopiarEmoji($file['mrima'],$ruta['mrima'],$nombre['mrima']);	
					$image['mrima']=true;
				}else{
					$destino['mrima']='';
					$verificar['mrima']['success']='true';
				}
				if(!empty($file['iprut']['name'])){
					$verificar['iprut']=$this->funciones->CopiarEmoji($file['iprut'],$ruta['iprut'],$nombre['iprut']);
					$image['iprut']=true;
				}else{
					$destino['iprut']='';
					$verificar['iprut']['success']='true';
				}
				if(!empty($file['rifro']['name'])){
					$verificar['rifro']=$this->funciones->CopiarEmoji($file['rifro'],$ruta['rifro'],$nombre['rifro']);
					$image['rifro']=true;
				}else{
					$destino['rifro']='';
					$verificar['rifro']['success']='true';
				}
				if(!empty($file['riesp']['name'])){
					$verificar['riesp']=$this->funciones->CopiarEmoji($file['riesp'],$ruta['riesp'],$nombre['riesp']);
					$image['riesp']=true;
				}else{
					$destino['riesp']='';
					$verificar['riesp']['success']='true';
				}
				if($verificar['mrima']['success']=='true' and $verificar['iprut']['success']=='true' and $verificar['rifro']['success']=='true' and $verificar['riesp']['success']=='true'){
					
					$sql="SELECT * FROM solicitud
						WHERE solic_codig='".$codig."';";
					$pedidos=$conexion->createCommand($sql)->queryRow();
					if($image['mrima']==false){
						$destino['mrima']=$pedidos['pdeta_mrima'];
					}
					if($image['iprut']==false){
						$destino['iprut']=$pedidos['pdeta_iprut'];
					}
					if($image['rifro']==false){
						$destino['rifro']=$pedidos['pdeta_rifro'];
					}
					if($image['riesp']==false){
						$destino['riesp']=$pedidos['pdeta_riesp'];
					}
					if($pedidos){
						$sql="UPDATE solicitud
						  SET solic_mposi='".$mposi."',
							  solic_mfuen='".$mfuen."',
							  solic_mcolo='".$mcolo."',
							  solic_limag='".$limag."',
							  solic_mobse='".$mobse."',
							  solic_eatbo='".$eatbo."',
							  solic_eafue='".$eafue."',
							  solic_eacol='".$eacol."',
							  solic_ebtbo='".$ebtbo."',
							  solic_ebfue='".$ebfue."',
							  solic_ebcol='".$ebcol."',
							  solic_aefue='".$aefue."',
							  solic_aecol='".$aecol."',
							  solic_eobse='".$eobse."',
							  solic_mrima='".$destino['mrima']."',
							  solic_iprut='".$destino['iprut']."',
							  solic_rifro='".$destino['rifro']."',
							  solic_riesp='".$destino['riesp']."',
							  solic_pasos='".$pasos."' 
						WHERE solic_codig='".$codig."';";
						$res1=$conexion->createCommand($sql)->execute();
						
						if($res1 or $pedidos['solic_pasos']>='4'){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Pedido guardado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al guardar el Pedido');	
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error no existe el Pedido');	
					}
				}else{
					$transaction->rollBack();
					$msg=$verificar;	
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);

		}else{
			$p=$_GET['p'];
			$s=1;

			$this->render('registrar',array('p' => $p,'s' => $s ));
		}
	}
	public function actionModificar()
	{
		if($_POST){
			//Datos del Pedido
			$codig=mb_strtoupper($_POST["codig"]);
			$canti=mb_strtoupper($_POST["canti"]);
			$mcant=mb_strtoupper($_POST["mcant"]);
			$color=mb_strtoupper($_POST["color"]);
			$mensa=mb_strtoupper($_POST["mensa"]);
			//Datos de Auditoria
			$estat=1;
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				
				$sql="SELECT * FROM solicitud WHERE solic_codig ='".$codig."'";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				if($pedidos){
					$sql="UPDATE solicitud
						  SET solic_total='".$canti."',
							  solic_cmode='".$mcant."',
							  solic_obser='".$mensa."'
						  WHERE solic_codig ='".$codig."'";
					$res1=$conexion->createCommand($sql)->execute();
					if($res1){
						$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Pedido actualizado correctamente');
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al actualizar el Pedido');	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$s=$_GET['s'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM solicitud  a
			  WHERE a.solic_codig ='".$_GET['c']."'";
			$pedidos=$conexion->createCommand($sql)->queryRow();
			$ubicacion[0]='start ';
			$ubicacion[2]='finish ';
			for ($i=0; $i < 3 ; $i++) { 
				if($i<$pedidos['solic_pasos']){
					$ubicacion[$i].='upcoming ';
				}
				if ($i==($s-1)) {
					$ubicacion[$i].='active ';
				}
			}

			$sql="SELECT model_gorro, model_preti
			  FROM solicitud_detalle  a
			  JOIN pedido_modelo b ON (a.model_codig = b.model_codig)
			  WHERE a.solic_codig ='".$_GET['c']."'";
			$modelos=$conexion->createCommand($sql)->queryAll();
			$pedidos['gorro']=2;
			$pedidos['preti']=2;
			foreach ($modelos as $key => $value) {
				if($value['model_gorro']=='1'){
					$pedidos['gorro']=1;
				}
				if($value['model_preti']=='1'){
					$pedidos['preti']=1;
				}
			}
			switch ($s) {
				case '1':
					$this->render('paso1', array('pedidos' => $pedidos, 'ubicacion' => $ubicacion,'c' => $c,'s' => $s));
					break;
				case '2':
					$this->render('paso2', array('pedidos' => $pedidos, 'ubicacion' => $ubicacion,'c' => $c,'s' => $s));
					break;
				case '3':
					$this->render('paso3', array('pedidos' => $pedidos, 'ubicacion' => $ubicacion,'c' => $c,'s' => $s));
					break;
				/*case '3':
					$this->render('paso3', array('pedidos' => $pedidos, 'ubicacion' => $ubicacion,'p' => $p,'c' => $c,'s' => $s));
					break;
				case '4':
					$this->render('paso4', array('pedidos' => $pedidos, 'ubicacion' => $ubicacion,'p' => $p,'c' => $c,'s' => $s));
					break;*/
				default:
					$this->render('paso1', array('pedidos' => $pedidos,'ubicacion' => $ubicacion,'c' => $c,'s' => $s));
					break;
			}
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM solicitud  WHERE solic_codig ='".$codig."'";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				if($pedidos){
					$sql="SELECT * FROM solicitud_detalle  WHERE solic_codig ='".$codig."'";
					$detalle=$conexion->createCommand($sql)->queryRow();
					if(!$detalle){	
						$sql="DELETE FROM solicitud WHERE solic_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();

						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Pedido eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Pedido');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Pedido no se puede eliminar debido a que tiene modelos asociados');
					}		
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
				
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM solicitud  a
			  WHERE a.solic_codig ='".$_GET['c']."'";
			$pedidos=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('pedidos' => $pedidos,'c'=>$c));
		}
	}
	public function actionEnviar()
	{
		if($_POST){

			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM solicitud WHERE solic_codig ='".$codig."'";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				if($pedidos){
					if($pedidos['solic_pasos']=='3'){
						$sql="SELECT * FROM solicitud_detalle  WHERE solic_codig ='".$codig."'";
						$detalles=$conexion->createCommand($sql)->queryAll();
						if($detalles){

							foreach ($detalles as $key => $value) {
								$sql="SELECT * FROM solicitud_detalle_listado  WHERE pdeta_codig ='".$value['pdeta_codig']."'";
								$modelo=$conexion->createCommand($sql)->queryAll();
								if($modelo){
									if($value['pdeta_pasos']>='2'){
										$msg=array('success'=>'true','msg'=>' Detalle eliminado correctamente');
										$cmode++;
										$cpers+=count($modelo);	
									}else{
										$transaction->rollBack();
										$msg=array('success'=>'false','msg'=>'Termine todos los pasos de los modelos y el pedido para continuar');	
										echo json_encode($msg);
										exit();
									}
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Existe Un modelo sin p_personas asociadas verificar');	
									echo json_encode($msg);
									exit();
								}
							}
							if($msg['success']=='true'){
								$sql="UPDATE solicitud
									  SET solic_total='".$cpers."',
										  solic_cmode='".$cmode."',
										  epedi_codig='2'
									  WHERE solic_codig ='".$codig."'";
								$res1=$conexion->createCommand($sql)->execute();

								if($res1){
									$transaction->commit();
									$msg=array('success'=>'true','msg'=>' Pedido enviado correctamente');	
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al enviar el Pedido');	
								}
							}
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'El pedido no tiene modelos asociados');
						}
					}else{
						$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Debe finalizar el pedido antes de enviarlo');
					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
				
			}catch(Exception $e){

				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$p=$_GET['p'];
			$c=$_GET['c'];
			$s=$_GET['s'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM solicitud  a
			  WHERE a.solic_codig ='".$_GET['c']."'";
			$pedidos=$conexion->createCommand($sql)->queryRow();
			$this->render('enviar', array('pedidos' => $pedidos, 'ubicacion' => $ubicacion,'p' => $p,'c' => $c,'s' => $s));
		}
	}
	public function actionAprobar()
	{
		if($_POST){
			$codig=($_POST['codig']);
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM solicitud_pagos WHERE pagos_codig ='".$codig."'";
				$pagos=$conexion->createCommand($sql)->queryRow();
				if($pagos){
					if($pagos['epago_codig']=='1'){
						$sql="UPDATE solicitud_pagos
							  SET epago_codig='2'
							  WHERE pagos_codig ='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();

						if($res1){
							$sql="SELECT a.*, b.*
								  FROM p_persona a
								  JOIN seguridad_usuarios b ON (a.perso_codig=b.perso_codig)
								  WHERE b.usuar_codig='".$pagos['usuar_codig']."'";
                    		$p_persona=$conexion->createCommand($sql)->queryRow();

                    		
							$datos['row']=$p_persona;
							$corre=$p_persona['usuar_login'];
                            $this->funciones->enviarCorreo('../pedidos/pagos/aprobar_correo',$datos,'Polerones Tiempo | Pago Aprobado',$corre);
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Pago Aprobado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al Aprobar el Pago');	
						}
						
					}else{
						$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'El pago debe de estar en estatus pendiente antes de Aprobarlo');
					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pago no existe');
				}
				
			}catch(Exception $e){

				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM solicitud_pagos  a
			  WHERE a.pagos_codig ='".$_GET['c']."'";
			$pagos=$conexion->createCommand($sql)->queryRow();
			$totales=$this->funciones->CalcularMontoPedido($pedidos);
			$this->render('aprobar', array('pagos' => $pagos, 'c'=>$c));
		}
	}
	public function actionRechazar()
	{
		if($_POST){
			$codig=($_POST['codig']);
			$motiv=mb_strtoupper($_POST['motiv']);
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM solicitud_pagos WHERE pagos_codig ='".$codig."'";
				$pagos=$conexion->createCommand($sql)->queryRow();
				if($pagos){
					//if($pagos['epago_codig']=='1' or ($pagos['epago_codig']=='2')){
						
						$sql="DELETE FROM solicitud_pagos
							  WHERE pagos_codig ='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();

						if($res1){
							$sql="SELECT a.*, b.*
								  FROM p_persona a
								  JOIN seguridad_usuarios b ON (a.perso_codig=b.perso_codig)
								  WHERE b.usuar_codig='".$pagos['usuar_codig']."'";
                    		$p_persona=$conexion->createCommand($sql)->queryRow();

							$datos['row']=$p_persona;
							$datos['obser']=$obser;
							$corre=$p_persona['usuar_login'];
                            $this->funciones->enviarCorreo('../pedidos/pagos/rechazar_correo',$datos,'Polerones Tiempo | Pago Rechazado',$corre);
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Pago Rechazado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al Rechazar el Pago');	
						}
						
					/*}else{
						$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'El pago debe de estar en estatus pendiente antes de Rechazarlo');
					}*/
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pago no existe');
				}
				
			}catch(Exception $e){

				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM solicitud_pagos  a
			  WHERE a.pagos_codig ='".$_GET['c']."'";
			$pagos=$conexion->createCommand($sql)->queryRow();
			$this->render('rechazar', array('pagos' => $pagos, 'c'=>$c));
		}
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}