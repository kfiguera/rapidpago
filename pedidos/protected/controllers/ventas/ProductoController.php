<?php

class ProductoController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$f=$_GET['f'];
		$this->redirect(Yii::app()->request->baseUrl.'/ventas/producto/listado?f='.$f);
	}

	public function actionListado()
	{
		$f=$_GET['f'];
		$this->render('listado',array('f' => $f ));
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		$f=$_POST['nfact'];
		if($_POST['descr']){
			$descr=mb_strtoupper($_POST['descr']);
			$condicion.="AND b.inven_descr like '%".$descr."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE factu_codig='".$f."' ".$condicion;
		}
		
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion,'f' => $f ));		
	}
	public function actionConsultar()
	{
		$f=$_GET['f'];
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM factura_producto  a
			  WHERE a.fprod_codig ='".$_GET['c']."'";
		$roles=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('roles' => $roles, 'f' => $f ));
	}
	public function actionRegistrar()
	{
		if($_POST){
			$nfact=mb_strtoupper($_POST["nfact"]);
			foreach ($_POST["produ"] as $key => $value) {
				$producto[$key]["tprod"]=$_POST["tprod"][$key];
				$producto[$key]["produ"]=$_POST["produ"][$key];
				$producto[$key]["cprod"]=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["cprod"][$key]));
				$producto[$key]["pprod"]=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["pprod"][$key]));
				$producto[$key]["mprod"]=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["mprod"][$key]));
			}
			$obser=mb_strtoupper($_POST["obser"]);
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM factura WHERE factu_codig = '".$nfact."'";
				$factura=$conexion->createCommand($sql)->queryRow();
				if($factura){
					foreach ($producto as $key => $value) {
						$sql="INSERT INTO factura_producto(factu_codig, tprod_codig, inven_codig, fprod_canti, fprod_preci, fprod_monto, fprod_obser, usuar_codig, fprod_fcrea, fprod_hcrea) 
						VALUES ( '".$nfact."', '".$value['tprod']."', '".$value['produ']."', '".$value['cprod']."', '".$value['pprod']."', '".$value['mprod']."', '".$value['obser']."', '".Yii::app()->user->id['usuario']['codigo']."','".date('Y-m-d')."','".date('H:i:s')."')";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$msg=array('success'=>'true','msg'=>'Producto guardado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al guardar el producto');
							echo json_encode($msg);;
							exit();
						}	
					}
					if ($msg['success']=='true') {
						$sql="SELECT * FROM factura_producto WHERE factu_codig ='".$nfact."'";
						$prod=$conexion->createCommand($sql)->queryAll();
						
						foreach ($prod as $key => $value) {
							$mtotal+=$value['fprod_monto'];
						}
						$sql="UPDATE factura 
								  SET factu_monto = '".$mtotal."'
									 WHERE factu_codig ='".$nfact."'";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar la Factura');	
						}

					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'La factura no existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$f=$_GET['f'];
		$this->render('registrar',array('f' => $f ));
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codig=mb_strtoupper($_POST["codig"]);
			$nfact=mb_strtoupper($_POST["nfact"]);
			foreach ($_POST["produ"] as $key => $value) {
				$producto[$key]["tprod"]=$_POST["tprod"][$key];
				$producto[$key]["produ"]=$_POST["produ"][$key];
				$producto[$key]["cprod"]=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["cprod"][$key]));
				$producto[$key]["pprod"]=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["pprod"][$key]));
				$producto[$key]["mprod"]=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["mprod"][$key]));
			}
			$obser=mb_strtoupper($_POST["obser"]);

			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM factura_producto  WHERE fprod_codig ='".$codig."'";
				$factura=$conexion->createCommand($sql)->queryRow();
				if($factura){
					foreach ($producto as $key => $value) {
						$sql="UPDATE factura_producto
						  SET factu_codig = '".$nfact."',
							  tprod_codig = '".$value['tprod']."',
							  inven_codig = '".$value['produ']."',
							  fprod_canti = '".$value['cprod']."',
							  fprod_preci = '".$value['pprod']."',
							  fprod_monto = '".$value['mprod']."',
							  fprod_obser = '".$obser."'  
							 WHERE fprod_codig ='".$codig."'";
				
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$msg=array('success'=>'true','msg'=>'Producto Actualizado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar el producto');
							echo json_encode($msg);;
							exit();
						}	
					}
					if ($msg['success']=='true') {
						$sql="SELECT * FROM factura_producto WHERE factu_codig ='".$nfact."'";
						$prod=$conexion->createCommand($sql)->queryAll();
						
						foreach ($prod as $key => $value) {
							$mtotal+=$value['fprod_monto'];
						}
						$sql="UPDATE factura 
								  SET factu_monto = '".$mtotal."'
									 WHERE factu_codig ='".$nfact."'";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar la Factura');	
						}

					}
				
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'La Factura no existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$f=$_GET['f'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM factura_producto  a
			  WHERE a.fprod_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('roles' => $roles,'f'=>$f));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$nfact=mb_strtoupper($_POST["nfact"]);
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM factura_producto  WHERE fprod_codig ='".$codig."'";
				$roles=$conexion->createCommand($sql)->queryRow();
				if($roles){

					$sql="DELETE FROM factura_producto  WHERE fprod_codig ='".$codig."'";
					$res1=$conexion->createCommand($sql)->execute();
					//echo $sql;
					if($res1){
						$msg=array('success'=>'true','msg'=>' Producto eliminado correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al eliminar la Factura');	
					}
					if ($msg['success']=='true') {
						$sql="SELECT * FROM factura_producto WHERE factu_codig ='".$nfact."'";
						$prod=$conexion->createCommand($sql)->queryAll();
						
						foreach ($prod as $key => $value) {
							$mtotal+=$value['fprod_monto'];
						}
						$sql="UPDATE factura 
								  SET factu_monto = '".$mtotal."'
									 WHERE factu_codig ='".$nfact."'";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar la Factura');	
						}

					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'La Factura no existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$f=$_GET['f'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM factura_producto  a
			  WHERE a.fprod_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			var_dump($_POST);
			$this->render('eliminar', array('roles' => $roles,'f'=>$f));
		}
	}
	
	public function TransformarMonto_bd($monto)
	{
		//DEBE DE ESTAR 0.000,00
		$monto=str_replace('.', '', $monto);
		$monto=str_replace(',', '.', $monto);
		//RETORNA 0000.00
		return $monto;
	}
	public function TransformarMonto_v($monto,$cantidad)
	{
		//DEBE DE ESTAR 0000.00
		$monto=number_format($monto,$cantidad,',','.');
		//RETORNA 0.000,00
		return $monto;
	}
	public function TransformarFecha_bd($fecha)
	{
		//DEBE DE ESTAR DD/MM/AAAA
		$fecha=explode('/',$fecha);
		$fecha=$fecha[2].'-'.$fecha[1].'-'.$fecha[0];
		//RETORNA AAAA-MM-DD
		return $fecha;
	}
	public function TransformarFecha_v($fecha)
	{
		//DEBE DE ESTAR AAAA-MM-DD
		$fecha=explode('/',$fecha);
		$fecha=$fecha[2].'-'.$fecha[1].'-'.$fecha[0];
		//RETORNA DD/MM/AAAA
		return $fecha;
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}