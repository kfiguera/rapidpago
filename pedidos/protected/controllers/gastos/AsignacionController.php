<?php

class AsignacionController extends Controller
{
	public $funciones;

	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();
		if(!isset(yii::app()->session['nomina']['periodo'])){
			$this->redirect('../nomina/listado');
		}
	}
	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionConsultar(){
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM nomina_asignacion  a
			  WHERE a.asign_codig ='".$_GET['c']."'";
			$asign=$conexion->createCommand($sql)->queryRow();
			
			
			
			/*echo "<pre>";
			var_dump($resum);
			exit();*/

			
			$this->render('consultar', array('asign' => $asign,'resum' => $resum));
	}
	public function actionRegistrar()
	{
		if($_POST){
			$traba=mb_strtoupper($_POST["traba"]);
			$nomin=Yii::app()->session['nomina']['codigo'];
			$perio=Yii::app()->session['nomina']['periodo']['perio_codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();

			try{
				$sql="SELECT * FROM nomina_asignacion WHERE traba_codig = '".$traba."' and perio_codig = '".$perio."' and nomin_codig='".$nomin."' ";
				$asign=$conexion->createCommand($sql)->queryRow();
				if(!$asign){

					$sql="SELECT * FROM p_trabajador WHERE traba_codig = '".$traba."' and etrab_codig = 1";
					
					$trab=$conexion->createCommand($sql)->queryRow();
					if($trab){
						$sql="SELECT * FROM nomina_periodo WHERE perio_codig = '".$perio."' and nomin_codig='".$nomin."'";

						$periodo=$conexion->createCommand($sql)->queryRow();
						if($periodo){
							if($perio['eperi_codig']<>'1'){

								$sql="INSERT INTO nomina_asignacion( nomin_codig, perio_codig, traba_codig, usuar_codig, asign_fcrea, asign_hcrea) VALUES ('".$nomin."','".$perio."','".$traba."','".Yii::app()->user->id['usuario']['codigo']."','".date('Y-m-d')."','".date('H:i:s')."')";
								$res1=$conexion->createCommand($sql)->execute();
										

								if($res1){
									$transaction->commit();
									$msg=array('success'=>'true','msg'=>'Trabajador registrado correctamente');	
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al registrar el trabajdor');
								}
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'No se registrar el trbajador  debido a, que el periodo no esta abierto');	
							}
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'No se registrar el trbajador  debido a, que el periodo no existe');	
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'No se registrar el p_trabajador  debido a, que ya no esta con estatus activo');	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No se registrar el trbajador  debido a, que ya existe en el periodo seleccionado');	
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{
			$this->render('registrar');
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM nomina_asignacion  WHERE asign_codig ='".$codig."'";
				$asign=$conexion->createCommand($sql)->queryRow();
				if($asign){
					$sql="SELECT * FROM nomina_periodo  WHERE perio_codig = '".$asign['perio_codig']."'";
					$perio=$conexion->createCommand($sql)->queryRow();
					if($perio){
						if($perio['eperi_codig']=='1'){
							
								
							$sql="DELETE FROM nomina_asignacion WHERE asign_codig='".$codig."'";
							$res1=$conexion->createCommand($sql)->execute();
							//echo $sql;
							if($res1){
								$transaction->commit();
								$msg=array('success'=>'true','msg'=>' Trabajador Eliminado correctamente');	
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al eliminar el Trabajador');	
							}
								
							
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'No se puede eliminar debido a, que el periodo no esta abierto');
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'No se puede eliminar debido a, que el periodo no existe');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No se puede eliminar debido a, que el p_trabajador no esta registrado');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM nomina_asignacion  a
			  WHERE a.asign_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('roles' => $roles));
		}
	}
	public function actionModificar()
	{
		if($_POST){

			$traba_salar = $this->funciones->transformarMonto_bd(mb_strtoupper($_POST["salar"]),2);
			$hextr_canti = $this->funciones->transformarMonto_bd(mb_strtoupper($_POST["hextr_canti"]),0);
			$codig = mb_strtoupper($_POST["codig"]);
			$traba = mb_strtoupper($_POST["traba"]);
			$dtrab = $this->funciones->transformarMonto_bd(mb_strtoupper($_POST["dtrab"]),0);
			$salar = $this->funciones->transformarMonto_bd(mb_strtoupper($_POST["salar"]),2);
			//ASIGNACIONES
			$dferi = mb_strtoupper($_POST["dferi"]);
			$dferi_canti = $this->funciones->transformarMonto_bd(mb_strtoupper($_POST["dferi_canti"]),0);
			$hextr = mb_strtoupper($_POST["hextr"]);
			$hextr_canti = $this->funciones->transformarMonto_bd(mb_strtoupper($_POST["hextr_canti"]),0);
			$bonif = mb_strtoupper($_POST["bonif"]);
			$bonif_canti = $this->funciones->transformarMonto_bd(mb_strtoupper($_POST["bonif_canti"]),0);
			$balim = mb_strtoupper($_POST["balim"]);
			//DEDUCCIONES
			$ntrab = mb_strtoupper($_POST["ntrab"]);
			$ntrab_canti = $this->funciones->transformarMonto_bd(mb_strtoupper($_POST["ntrab_canti"]),0);
			$ssoci = mb_strtoupper($_POST["ssoci"]);
			$ssoci_canti = $this->funciones->transformarMonto_bd(mb_strtoupper($_POST["ssoci_canti"]),0);
			$faov = mb_strtoupper($_POST["faov"]);
			$faov_canti = $this->funciones->transformarMonto_bd(mb_strtoupper($_POST["faov_canti"]),0);
			$ince = mb_strtoupper($_POST["ince"]);
			$ince_canti = $this->funciones->transformarMonto_bd(mb_strtoupper($_POST["ince_canti"]),0);
			$spf = mb_strtoupper($_POST["spf"]);
			$spf_canti = $this->funciones->transformarMonto_bd(mb_strtoupper($_POST["spf_canti"]),0);
			//OBSERVACIONES
			$obser = mb_strtoupper($_POST["obser"]);
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			
			try{
				$sql="SELECT * 
			  		  FROM nomina_asignacion  a
			  		  WHERE a.asign_codig ='".$codig."'";
				$asign=$conexion->createCommand($sql)->queryRow();
				if ($asign) {
					//ASIGNACION TRABAJADOR
					$sql="UPDATE nomina_asignacion
						SET asing_dtrab='".$dtrab."',
							asign_dferi='".$dferi."',
							asign_cdfer='".$dferi_canti."',
							asign_hextr='".$hextr."',
							asign_chext='".$hextr_canti."',
							asign_bonif='".$bonif."',
							asign_balim='".$balim."',
							asign_ntrab='".$ntrab."',
							asign_cntra='".$ntrab_canti."',
							asign_obser='".$obser."'
						WHERE asign_codig ='".$codig."'";
					$res=$conexion->createCommand($sql)->execute();

					//CONCEPTOS TRABAJADOR
					$sql="SELECT * FROM nomina_concepto WHERE nomin_codig = '".Yii::app()->session['nomina']['codigo']."'  order by tconc_codig,conce_codig ";
					$conceptos=$conexion->createCommand($sql)->queryAll();
					$total_asign=0;
					$total_deduc=0;
					$total_gener=0;	
					$i=0;
					foreach ($conceptos as $key => $concepto) {

						//CALCULO
						
						$formula= $concepto['conce_value'];
						$str = '$resultado = '.$formula.';';
						eval($str);
						$monto = $resultado;
						if($concepto['tconc_codig']=='1'){
							$total_asign+=$monto;
							$total_gener+=$monto;
						}else{
							$total_deduc+=$monto;
							$total_gener-=$monto;
						}
						//GUARDADO EN ARREGLO
						$resumen[$i]['nomin']=Yii::app()->session['nomina']['codigo'];
						$resumen[$i]['perio']=Yii::app()->session['nomina']['periodo']['perio_codig'];
						$resumen[$i]['traba']=$traba;
						$resumen[$i]['cconc']=$concepto['conce_codig'];
						$resumen[$i]['dconc']=$concepto['conce_descr'];
						$resumen[$i]['mconc']=$monto;
						$resumen[$i]['tconc']=$concepto['tconc_codig'];
						$resumen[$i]['cantc']=$concepto['tconc_codig'];
						$i++;
					}
					//SI NO EXISTEN DATOS
					/*$sql="TRUNCATE nomina_resumen;";
					$res1=$conexion->createCommand($sql)->execute();*/
					$d=0;
					foreach ($resumen as $key => $row) {
						$res1=true;

						$sql="SELECT * FROM nomina_resumen WHERE nomin_codig = '".$row['nomin']."' AND perio_codig = '".$row['perio']."' AND traba_codig = '".$row['traba']."' AND conce_codig = '".$row['cconc']."'";
						$nresu=$conexion->createCommand($sql)->queryRow();
						
						if($nresu){

							if($row['mconc'] != $nresu['resum_monto']){
								$sql="UPDATE nomina_resumen
								  SET resum_monto = '".$row['mconc']."'
								  WHERE resum_codig='".$nresu['resum_codig']."'";
								
								$res1=$conexion->createCommand($sql)->execute();
							}
							$d=1;

						}else{

							$sql="INSERT INTO nomina_resumen(nomin_codig, perio_codig, traba_codig, conce_codig, resum_monto, tconc_codig, usuar_codig, resum_fcrea, resum_hcrea) 
								VALUES ('".$row['nomin']."','".$row['perio']."','".$row['traba']."','".$row['cconc']."','".$row['mconc']."','".$row['tconc']."','".Yii::app()->user->id['usuario']['codigo']."','".date('Y-m-d')."','".date('H:i:s')."')";
							$res1=$conexion->createCommand($sql)->execute();

						}
						
						if(!$res1){
							$transaction->rollBack();		
							$msg=array('success'=>'false','msg'=>'Error al ingresar los conceptos al p_trabajador','sql'=>$sql);	
							echo json_encode($msg);
							exit();
						}

					}
					$transaction->commit();
					if($d==1){
						$msg=array('success'=>'true','msg'=>'Datos actualizados correctamente');	
					}else{
						$msg=array('success'=>'true','msg'=>'Datos registrados correctamente');	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error al p_trabajador no esta asignado a la nomina');
				}
				
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM nomina_asignacion  a
			  WHERE a.asign_codig ='".$_GET['c']."'";
			$asign=$conexion->createCommand($sql)->queryRow();
			
			
			
			/*echo "<pre>";
			var_dump($resum);
			exit();*/

			
			$this->render('modificar', array('asign' => $asign,'resum' => $resum));
		}
	}
	public function actionCalcular()
	{
		if($_POST){
			
			$nomin=$this->funciones->transformarMonto_bd(mb_strtoupper($_POST["nomin"]),0);;
			$perio=$this->funciones->transformarMonto_bd(mb_strtoupper($_POST["perio"]),0);
			$finic=$this->funciones->transformarFecha_bd(mb_strtoupper($_POST["finic"]));
			$ffina=$this->funciones->transformarFecha_bd(mb_strtoupper($_POST["ffina"]));
			$canti=$this->funciones->transformarMonto_bd(mb_strtoupper($_POST["canti"]),0);
			$monto=$this->funciones->transformarMonto_bd(mb_strtoupper($_POST["monto"]),2);
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			
			try{
				$sql="SELECT * 
			  		  FROM nomina_periodo  a
			  		  WHERE a.perio_codig ='".$perio."'";
				$periodo=$conexion->createCommand($sql)->queryRow();

				if ($periodo) {
					if ($periodo['eperi_codig']=='1') {
						
						$sql="SELECT sum(resum_monto) monto FROM nomina_resumen a 
						WHERE tconc_codig='1'
						  AND nomin_codig='".$nomin."' 
						  AND perio_codig='".$perio."'";
						$resumen=$conexion->createCommand($sql)->queryRow();
						
						$sql="UPDATE nomina_periodo 
							  SET perio_monto = '".$resumen['monto']."',
							  	  eperi_codig = '3'
							  WHERE perio_codig ='".$perio."'";

						$res=$conexion->createCommand($sql)->execute();
						if($res){
							$transaction->commit();		
							$msg=array('success'=>'true','msg'=>'Nomina Calculada correctamente');	
						}else{
							$transaction->rollBack();		
							$msg=array('success'=>'false','msg'=>'Error al actualizar el monto de la nomina');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error el periodo no esta abierto');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error el periodo no existe');
				}
				
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
					
			$this->render('calcular');
		}
	}
	public function actionReversar()
	{
		if($_POST){
			
			$nomin=$this->funciones->transformarMonto_bd(mb_strtoupper($_POST["nomin"]),0);;
			$perio=$this->funciones->transformarMonto_bd(mb_strtoupper($_POST["perio"]),0);
			$finic=$this->funciones->transformarFecha_bd(mb_strtoupper($_POST["finic"]));
			$ffina=$this->funciones->transformarFecha_bd(mb_strtoupper($_POST["ffina"]));
			$canti=$this->funciones->transformarMonto_bd(mb_strtoupper($_POST["canti"]),0);
			$monto=$this->funciones->transformarMonto_bd(mb_strtoupper($_POST["monto"]),2);
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			
			try{
				$sql="SELECT * 
			  		  FROM nomina_periodo  a
			  		  WHERE a.perio_codig ='".$perio."'";
				$periodo=$conexion->createCommand($sql)->queryRow();

				if ($periodo) {
					if ($periodo['eperi_codig']=='3') {
						
						$sql="UPDATE nomina_periodo 
							  SET perio_monto = '0',
							  	  eperi_codig = '1'
							  WHERE perio_codig ='".$perio."' 
							  	AND nomin_codig  ='".$nomin."'";

						$res=$conexion->createCommand($sql)->execute();
						$sql="SELECT *
							  FROM nomina_resumen
							  WHERE nomin_codig ='".$nomin."'
							    AND perio_codig ='".$perio."'";
						$resumen=$conexion->createCommand($sql)->queryRow();
						if($resumen){
							$sql="DELETE 
						  		FROM nomina_resumen
						  		WHERE nomin_codig ='".$nomin."'
						    	AND perio_codig ='".$perio."'";
						    $res2=$conexion->createCommand($sql)->execute();
						}
						if($res){
							
							$transaction->commit();		
							$msg=array('success'=>'true','msg'=>'Nomina Reversada correctamente');	
						}else{
							$transaction->rollBack();		
							$msg=array('success'=>'false','msg'=>'Error al actualizar el monto de la nomina');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error el periodo no esta en estatus calculado');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error el periodo no existe');
				}
				
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información','e'=>$e);
			}
			echo json_encode($msg);
		}else{
					
			$this->render('reversar');
		}
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}