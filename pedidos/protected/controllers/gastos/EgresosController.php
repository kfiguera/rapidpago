<?php

class EgresosController extends Controller
{
	public $funciones;

	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->redirect(Yii::app()->request->baseUrl.'/egresos/listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['motiv']){
			$motiv=mb_strtoupper($_POST['motiv']);
			$condicion.="a.egres_motiv like '%".$motiv."%' ";
			$con++;
		}
		if($_POST['nfact']){
			if($con>0){
				$condicion.="AND ";
			}
			$nfact=mb_strtoupper($_POST['nfact']);
			$condicion.="a.egres_nfact like '%".$nfact."%' ";
			$con++;
		}
		if($_POST['nrefe']){
			if($con>0){
				$condicion.="AND ";
			}
			$nrefe=mb_strtoupper($_POST['nrefe']);
			$condicion.="a.egres_refer like '%".$nrefe."%' ";
			$con++;
		}
		if($_POST['banco']){
			if($con>0){
				$condicion.="AND ";
			}
			$banco=mb_strtoupper($_POST['banco']);
			$condicion.="a.banco_codig = '".$banco."' ";
			$con++;
		}
		if($_POST['banco']){
			if($con>0){
				$condicion.="AND ";
			}
			$banco=mb_strtoupper($_POST['banco']);
			$condicion.="a.banco_codig = '".$banco."' ";
			$con++;
		}
		if($_POST['banco']){
			if($con>0){
				$condicion.="AND ";
			}
			$banco=mb_strtoupper($_POST['banco']);
			$condicion.="a.banco_codig = '".$banco."' ";
			$con++;
		}
		if($_POST['desde']){
			if($con>0){
				$condicion.="AND ";
			}
			$desde=$this->funciones->TransformarFecha_bd($_POST['desde']);
			$condicion.="a.egres_fegre >= '".$desde."' ";
			$con++;
		}
		if($_POST['hasta']){
			if($con>0){
				$condicion.="AND ";
			}
			$hasta=$this->funciones->TransformarFecha_bd($_POST['hasta']);
			$condicion.="a.egres_fegre <= '".$hasta."' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM egresos a
			  WHERE a.egres_codig ='".$_GET['c']."'";
		$roles=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('roles' => $roles));
	}
	public function actionRegistrar()
	{
		if($_POST){
			$motiv=mb_strtoupper($_POST["motiv"]);
			$banco=mb_strtoupper($_POST["banco"]);
			$nfact=mb_strtoupper($this->TransformarMonto_bd($_POST["nfact"]));
			$refer=mb_strtoupper($this->TransformarMonto_bd($_POST["refer"]));
			$monto=mb_strtoupper($this->TransformarMonto_bd($_POST["monto"]));
			$fegre=mb_strtoupper($this->TransformarFecha_bd($_POST["fegre"]));
			$obser=mb_strtoupper($_POST["obser"]);

			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="INSERT INTO egresos( egres_motiv, banco_codig, egres_nfact, egres_refer, egres_monto, egres_fegre, egres_obser, usuar_codig, egres_fcrea, egres_hcrea) 
					VALUES ('".$motiv."','".$banco."','".$nfact."','".$refer."','".$monto."','".$fegre."','".$obser."','".Yii::app()->user->id['usuario']['codigo']."','".date('Y-m-d')."','".date('H:i:s')."')";
				/*echo $sql;
				exit;*/
				$res1=$conexion->createCommand($sql)->execute();
				if($res1){
					$transaction->commit();
					$msg=array('success'=>'true','msg'=>'Egreso guardado correctamente');	
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error al guardar el Egreso');
				}	
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codig=mb_strtoupper($_POST["codig"]);
			$motiv=mb_strtoupper($_POST["motiv"]);
			$banco=mb_strtoupper($_POST["banco"]);
			$nfact=mb_strtoupper($this->TransformarMonto_bd($_POST["nfact"]));
			$refer=mb_strtoupper($this->TransformarMonto_bd($_POST["refer"]));
			$monto=mb_strtoupper($this->TransformarMonto_bd($_POST["monto"]));
			$fegre=mb_strtoupper($this->TransformarFecha_bd($_POST["fegre"]));
			$obser=mb_strtoupper($_POST["obser"]);

			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM egresos WHERE egres_codig ='".$codig."'";
				$egreso=$conexion->createCommand($sql)->queryRow();
				if($egreso){
					$sql="UPDATE egresos 
					  SET egres_motiv='".$motiv."',
						  banco_codig='".$banco."',
						  egres_nfact='".$nfact."',
						  egres_refer='".$refer."',
						  egres_monto='".$monto."',
						  egres_fegre='".$fegre."',
						  egres_obser='".$obser."'
					  WHERE egres_codig ='".$codig."'";
					$res1=$conexion->createCommand($sql)->execute();
					if($res1){
						$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Egreso actualizado correctamente');
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al actualizar el Egreso');	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error el egreso no existe');
				}
	
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM egresos  a
			  WHERE a.egres_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('roles' => $roles));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM egresos WHERE egres_codig ='".$codig."'";
				$egreso=$conexion->createCommand($sql)->queryRow();
				if($egreso){

					$sql="DELETE FROM egresos  WHERE egres_codig='".$codig."'";
					$res1=$conexion->createCommand($sql)->execute();
					//echo $sql;
					if($res1){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>' Egreso eliminado correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al eliminar el egreso');	
					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'La Factura no existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM egresos  a
			  WHERE a.egres_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('roles' => $roles));
		}
	}
	
	public function TransformarMonto_bd($monto)
	{
		//DEBE DE ESTAR 0.000,00
		$monto=str_replace('.', '', $monto);
		$monto=str_replace(',', '.', $monto);
		//RETORNA 0000.00
		return $monto;
	}
	public function TransformarMonto_v($monto,$cantidad)
	{
		//DEBE DE ESTAR 0000.00
		$monto=number_format($monto,$cantidad,',','.');
		//RETORNA 0.000,00
		return $monto;
	}
	public function TransformarFecha_bd($fecha)
	{
		//DEBE DE ESTAR DD/MM/AAAA
		$fecha=explode('/',$fecha);
		$fecha=$fecha[2].'-'.$fecha[1].'-'.$fecha[0];
		//RETORNA AAAA-MM-DD
		return $fecha;
	}
	public function TransformarFecha_v($fecha)
	{
		//DEBE DE ESTAR AAAA-MM-DD
		$fecha=explode('-',$fecha);
		$fecha=$fecha[2].'/'.$fecha[1].'/'.$fecha[0];
		//RETORNA DD/MM/AAAA
		return $fecha;
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}