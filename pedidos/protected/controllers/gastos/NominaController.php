<?php

class NominaController extends Controller
{
	public $funciones;

	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();
		if(!isset(yii::app()->session['nomina']['codigo'])){
			yii::app()->session['nomina']=array('codigo'=>1);
		}

	}
	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionSeleccionar()
	{
		$conexion=Yii::app()->db;
		$nomina=yii::app()->session['nomina']['codigo'];
		$sql="SELECT * 
			  FROM nomina_periodo  a
			  WHERE a.perio_codig ='".$_GET['c']."'";
		$perio=$conexion->createCommand($sql)->queryRow();
		yii::app()->session['nomina']=array('codigo'=>$nomina,'periodo'=>$perio);
		$this->redirect('../asignacion/listado');
	}
	public function actionRegistrar()
	{
		if($_POST){
			$nomin=mb_strtoupper($_POST["nomin"]);
			$traba=mb_strtoupper($_POST["traba"]);
			$meses=mb_strtoupper($_POST["meses"]);
			$ano=mb_strtoupper($_POST["ano"]);
			$estat=mb_strtoupper($_POST["estat"]);
			$ffina=$ano.'-'.$meses.'-'.$this->funciones->Ultimo_Dia_Mes($ano,$meses);
			$finic=$ano.'-'.$meses.'-'.'01';
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();

			try{
				$sql="SELECT * FROM nomina_periodo WHERE eperi_codig <> '2' ";
				$periodo=$conexion->createCommand($sql)->queryRow();
				if(!$periodo){

					$sql="SELECT * FROM nomina_periodo WHERE perio_ano = '".$ano."' and meses_codig = '".$meses."' and nomin_codig='".$nomin."'";
					$periodo=$conexion->createCommand($sql)->queryRow();
					if(!$periodo){
						//CREAR EL PERIODO
						$sql="INSERT INTO nomina_periodo(nomin_codig, perio_ano, meses_codig, perio_finic, perio_ffina, eperi_codig, usuar_codig, perio_fcrea, perio_hcrea) 
							VALUES ('".$nomin."','".$ano."','".$meses."','".$finic."','".$ffina."','".$estat."','".Yii::app()->user->id['usuario']['codigo']."','".date('Y-m-d')."','".date('H:i:s')."')";
						$res1=$conexion->createCommand($sql)->execute();
						$res2=true;
						//ASIGNAR EL PERSONAL
						if($traba=='1'){
							$sql="SELECT * FROM nomina_periodo WHERE perio_ano = '".$ano."' and meses_codig = '".$meses."' and nomin_codig='".$nomin."'";
							$periodo=$conexion->createCommand($sql)->queryRow();

							$sql="SELECT * FROM p_trabajador WHERE etrab_codig='1'";
							$p_trabajadores=$conexion->createCommand($sql)->queryAll();
							foreach ($p_trabajadores as $key => $p_trabajador) {
								$sql="INSERT INTO nomina_asignacion( nomin_codig, perio_codig, traba_codig, usuar_codig, asign_fcrea, asign_hcrea) VALUES ('".$periodo['nomin_codig']."','".$periodo['perio_codig']."','".$p_trabajador['traba_codig']."','".Yii::app()->user->id['usuario']['codigo']."','".date('Y-m-d')."','".date('H:i:s')."')";
								$res2=$conexion->createCommand($sql)->execute();
								if(!$res2){
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al asiganar los p_trabajadores');
									echo json_encode($msg);
									exit();
								}
							}
							
						}

						if($res1 and $res2){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Periodo creado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al crear el periodo');
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El periodo ya existe');	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No se puede crear el perido  debido a, que existen periodos abiertos');	
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{
			$this->render('registrar');
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM nomina_periodo  WHERE perio_codig ='".$codig."'";
				$perio=$conexion->createCommand($sql)->queryRow();
				if($perio){
					if($perio['eperi_codig']<>'2'){
						$sql="SELECT * FROM nomina_periodo  WHERE perio_codig > '".$codig."'";
						$perio=$conexion->createCommand($sql)->queryRow();
						if(!$perio){
							$sql="SELECT * FROM nomina_asignacion  WHERE perio_codig = '".$codig."'";
							$asign=$conexion->createCommand($sql)->queryRow();
							if(!$asign){
								$sql="DELETE FROM nomina_periodo WHERE perio_codig='".$codig."'";
								$res1=$conexion->createCommand($sql)->execute();
								//echo $sql;
								if($res1){
									$transaction->commit();
									$msg=array('success'=>'true','msg'=>' Periodo Eliminado correctamente');	
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al eliminar el periodo');	
								}
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'No se puede eliminar debido a, que existen p_trabajadores asignados al periodo  debe eliminarlos primero');
							}
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'No se puede eliminar debido a, que existen periodos mas recientes debe eliminarlos primero');
						}	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'No se puede eliminar debido a, que el periodo esta cerrado');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No se puede eliminar debido a, que no existe el periodo');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM nomina_periodo  a
			  WHERE a.perio_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('roles' => $roles));
		}
	}
	public function actionCerrar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM nomina_periodo  WHERE perio_codig ='".$codig."'";
				$perio=$conexion->createCommand($sql)->queryRow();
				if($perio){
					if($perio['eperi_codig']<>'2'){
					
						$sql="UPDATE nomina_periodo SET eperi_codig=2 WHERE perio_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Periodo cerrado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al cerrar el periodo');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'No se puede cerrar debido a, que el periodo esta cerrado');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No se puede cerrar debido a, que no existe el periodo');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM nomina_periodo  a
			  WHERE a.perio_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('cerrar', array('roles' => $roles));
		}
	}
	public function actionAbrir()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM nomina_periodo  WHERE perio_codig ='".$codig."'";
				$perio=$conexion->createCommand($sql)->queryRow();
				if($perio){
					if($perio['eperi_codig']=='2'){
					
						$sql="UPDATE nomina_periodo SET eperi_codig=2 WHERE perio_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Periodo abierto correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al abrir el periodo');
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'No se puede abrir debido a, que el periodo no esta cerrado');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No se puede cerrar debido a, que no existe el periodo');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM nomina_periodo  a
			  WHERE a.perio_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('abrir', array('roles' => $roles));
		}
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}