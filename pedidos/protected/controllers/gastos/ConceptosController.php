<?php

class ConceptosController extends Controller
{
	public $funciones;

	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();
		if(!isset(yii::app()->session['nomina']['periodo'])){
			$this->redirect('../nomina/listado');
		}

	}
	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descr']){
			$descr=mb_strtoupper($_POST['descr']);
			$condicion.="a.conce_descr like '%".$descr."%' ";
			$con++;
		}
		if($_POST['tconc']){
			if($con>0){
				$condicion.="AND ";
			}
			$tconc=mb_strtoupper($_POST['tconc']);
			$condicion.="a.tconc_codig = '".$tconc."' ";
			$con++;
		}
		if($con>0){
			$condicion="AND ".$condicion;
		}
		
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
		  FROM nomina_concepto  a
		  WHERE a.conce_codig ='".$_GET['c']."'";
		$conce=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('conce' => $conce));
	}
	public function actionModificar()
	{
		if($_POST){
			$codig=$_POST["codig"];
			$descr=mb_strtoupper($_POST["descr"]);
			$valor=$_POST["valor"];
			$tconc=$_POST["tconc"];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();			
			try{
				$sql="SELECT * 
			  		  FROM nomina_concepto a
			  		  WHERE a.conce_codig ='".$codig."'
			  		  AND a.nomin_codig = '".Yii::app()->session['nomina']['codigo']."'";
				$conce=$conexion->createCommand($sql)->queryRow();
				if ($conce) {
					$sql="SELECT * 
			  		  FROM nomina_concepto a
			  		  WHERE a.conce_descr ='".$descr."'
			  		  AND a.nomin_codig = '".Yii::app()->session['nomina']['codigo']."'";
					$res=$conexion->createCommand($sql)->queryRow();
					if(!$res or $conce['conce_descr']==$descr){
						$sql="UPDATE nomina_concepto
						SET conce_descr='".$descr."',
							conce_value='".$valor."',
							tconc_codig='".$tconc."'
						WHERE conce_codig ='".$codig."'";
						$res2=$conexion->createCommand($sql)->execute();
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Concepto actualizados correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Ya existe un concepto con esa descripcion dentro de la Nomina');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error el Concepto no existe');
				}
				
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM nomina_concepto  a
			  WHERE a.conce_codig ='".$_GET['c']."'";
			$conce=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('conce' => $conce));
		}

	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST["codig"];
			$descr=mb_strtoupper($_POST["descr"]);
			$valor=$_POST["valor"];
			$tconc=$_POST["tconc"];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();			
			try{
				$sql="SELECT * 
			  		  FROM nomina_concepto a
			  		  WHERE a.conce_codig ='".$codig."'
			  		  AND a.nomin_codig = '".Yii::app()->session['nomina']['codigo']."'";
				$conce=$conexion->createCommand($sql)->queryRow();
				if ($conce) {
					$sql="SELECT * 
			  		  FROM nomina_resumen a
			  		  WHERE a.conce_codig ='".$codig."'
			  		  AND a.nomin_codig = '".Yii::app()->session['nomina']['codigo']."'";
					$res=$conexion->createCommand($sql)->queryRow();
					if(!$res){
						$sql="DELETE 
					  		  FROM nomina_concepto a
					  		  WHERE a.conce_codig ='".$codig."'
					  		  AND a.nomin_codig = '".Yii::app()->session['nomina']['codigo']."'";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Concepto Eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Concepto');	
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'No se puede eliminar un concepto utilizado en nominas ya calculadas');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error el Concepto no existe');
				}
				
			}catch(Exception $e){
				echo $e;
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM nomina_concepto  a
			  WHERE a.conce_codig ='".$_GET['c']."'";
			$conce=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('conce' => $conce));
		}

	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}