<?php

class ClienteController extends Controller
{
	public $funciones;

	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->redirect(Yii::app()->request->baseUrl.'/cliente/listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['denom']){
			$denom=mb_strtoupper($_POST['denom']);
			$condicion.="a.clien_denom like '%".$denom."%' ";
			$con++;
		}
		if($_POST['tdocu']){
			if($con>0){
				$condicion.="AND ";
			}
			$tdocu=mb_strtoupper($_POST['tdocu']);
			$condicion.="a.tdocu_codig like '%".$tdocu."%' ";
			$con++;
		}
		if($_POST['ndocu']){
			if($con>0){
				$condicion.="AND ";
			}
			$ndocu=mb_strtoupper($_POST['ndocu']);
			$condicion.="a.clien_ndocu like '%".$ndocu."%' ";
			$con++;
		}
		if($_POST['ccont']){
			if($con>0){
				$condicion.="AND ";
			}
			$ccont=mb_strtoupper($_POST['ccont']);
			$condicion.="a.clien_ccont like '%".$ccont."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM cliente  a
			  WHERE a.clien_codig ='".$_GET['c']."'";
		$roles=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('roles' => $roles));
	}
	public function actionRegistrar()
	{
		if($_POST){
			$denom=mb_strtoupper($_POST["denom"]);
			$tclie=mb_strtoupper($_POST["tclie"]);
			$tdocu=mb_strtoupper($_POST["tdocu"]);
			$ndocu=mb_strtoupper($_POST["ndocu"]);
			$ccont=mb_strtoupper($_POST["ccont"]);
			$corre=mb_strtoupper($_POST["corre"]);
			$telef=mb_strtoupper($_POST["telef"]);
			$direc=mb_strtoupper($_POST["direc"]);
			$obser=mb_strtoupper($_POST["obser"]);
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM cliente WHERE tdocu_codig = '".$tdocu."' and clien_ndocu = '".$ndocu."'";
				$documento=$conexion->createCommand($sql)->queryRow();
				if(!$documento){
					/*$sql="SELECT * FROM cliente WHERE clien_ccont = '".$ccont."'";
					$contrato=$conexion->createCommand($sql)->queryRow();
					if(!$contrato){*/
						$sql="INSERT INTO cliente(tclie_codig, tdocu_codig, clien_ndocu, clien_denom, clien_ccont, clien_corre, clien_telef, clien_direc, clien_obser, usuar_codig, clien_fcrea, clien_hcrea) 
							VALUES ('".$tclie."','".$tdocu."','".$ndocu."','".$denom."','".$ccont."','".$corre."','".$telef."','".$direc."','".$obser."','".Yii::app()->user->id['usuario']['codigo']."','".date('Y-m-d')."','".date('H:i:s')."')";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Cliente guardado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al guardar el Cliente');	
						}	
					/*}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Ya existe un Cliente con ese Número de Contrato');
					}*/
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Ya existe un cliente con ese numero de documento');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codig=mb_strtoupper($_POST["codig"]);
			$denom=mb_strtoupper($_POST["denom"]);
			$tclie=mb_strtoupper($_POST["tclie"]);
			$tdocu=mb_strtoupper($_POST["tdocu"]);
			$ndocu=mb_strtoupper($_POST["ndocu"]);
			$ccont=mb_strtoupper($_POST["ccont"]);
			$corre=mb_strtoupper($_POST["corre"]);
			$telef=mb_strtoupper($_POST["telef"]);
			$direc=mb_strtoupper($_POST["direc"]);
			$obser=mb_strtoupper($_POST["obser"]);
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				
				$sql="SELECT * FROM cliente  WHERE clien_codig ='".$codig."'";
				$cliente=$conexion->createCommand($sql)->queryRow();
				if($cliente){
					$sql="SELECT * FROM cliente WHERE tdocu_codig = '".$tdocu."' and clien_ndocu = '".$ndocu."'";
					$documento=$conexion->createCommand($sql)->queryRow();
					if(!$documento or ($tdocu==$cliente['tdocu_codig'] and $ndocu==$cliente['clien_ndocu'])){
						$sql="SELECT * FROM cliente WHERE clien_ccont = '".$ccont."'";
						$contrato=$conexion->createCommand($sql)->queryRow();
						if(!$contrato or ($ccont==$cliente['clien_ccont'])){
								$sql="UPDATE cliente 
									  SET tclie_codig='".$tclie."',
										  tdocu_codig='".$tdocu."',
										  clien_ndocu='".$ndocu."',
										  clien_denom='".$denom."',
										  clien_ccont='".$ccont."',
										  clien_corre='".$corre."',
										  clien_telef='".$telef."',
										  clien_direc='".$direc."',
										  clien_obser='".$obser."'
									  WHERE clien_codig ='".$codig."'";
							$res1=$conexion->createCommand($sql)->execute();
							if($res1){
								$transaction->commit();
									$msg=array('success'=>'true','msg'=>'Cliente actualizado correctamente');
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al actualizar el Cliente');	
							}
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Ya existe un Cliente con ese Número de Contrato');
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Ya existe un cliente con ese numero de documento');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Cliente no existe');
				}
				
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM cliente  a
			  WHERE a.clien_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('roles' => $roles));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM factura  WHERE clien_codig ='".$codig."'";
				$factu=$conexion->createCommand($sql)->queryRow();
				if(!$factu){
					$sql="SELECT * FROM cliente WHERE clien_codig='".$codig."'";
					$roles=$conexion->createCommand($sql)->queryRow();
					if($roles){

						$sql="DELETE FROM cliente  WHERE clien_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Cliente eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Cliente');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Producto no existe');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No se puede eliminar debido a, que tiene facturas asociados');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM cliente  a
			  WHERE a.clien_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('roles' => $roles));
		}
	}
	
	public function TransformarMonto_bd($monto)
	{
		//DEBE DE ESTAR 0.000,00
		$monto=str_replace('.', '', $monto);
		$monto=str_replace(',', '.', $monto);
		//RETORNA 0000.00
		return $monto;
	}
	public function TransformarMonto_v($monto,$cantidad)
	{
		//DEBE DE ESTAR 0000.00
		$monto=number_format($monto,$cantidad,',','.');
		//RETORNA 0.000,00
		return $monto;
	}
	public function TransformarFecha_bd($fecha)
	{
		//DEBE DE ESTAR DD/MM/AAAA
		$fecha=explode('/',$fecha);
		$fecha=$fecha[2].'-'.$fecha[1].'-'.$fecha[0];
		//RETORNA AAAA-MM-DD
		return $fecha;
	}
	public function TransformarFecha_v($fecha)
	{
		//DEBE DE ESTAR AAAA-MM-DD
		$fecha=explode('/',$fecha);
		$fecha=$fecha[2].'-'.$fecha[1].'-'.$fecha[0];
		//RETORNA DD/MM/AAAA
		return $fecha;
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}