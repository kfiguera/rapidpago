<?php

class AccesoController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->redirect(Yii::app()->request->baseUrl.'/Registro/listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['usuario']){
			$usuario=mb_strtolower($_POST['usuario']);
			$condicion.="a.usuar_login like '%".$usuario."%' ";
			$con++;
		}
		if($_POST['nombre']){
			$nombre=mb_strtoupper($_POST['nombre']);
			if($con>0){
				$condicion.="AND ";
			}
			$condicion.="b.perso_pnomb like '%".$nombre."%' or b.perso_snomb like '%".$nombre."%' ";
			$con++;
		}
		if($_POST['apellido']){
			$apellido=mb_strtoupper($_POST['apellido']);
			if($con>0){
				$condicion.="AND ";
			}
			$condicion.="b.perso_papel like '%".$apellido."%' or b.perso_sapel like '%".$apellido."%' ";
			$con++;
		}
		if($con>0){
			$condicion="AND ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM seguridad_usuarios  a
			  JOIN registro b ON (a.usuar_codig and b.usuar_codig)
			  WHERE a.usuar_codig = b.usuar_codig 
			  AND a.usuar_codig = '".$_GET['c']."'";
		$roles=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('roles' => $roles));
	}
	public function actionVerificar()
	{
		if($_POST){
			$codigo=mb_strtoupper($_POST['codigo']);
			$accio=mb_strtoupper($_POST['accio']);
			$motiv=mb_strtoupper($_POST['motiv']);
			$estat=1;
			$passw=$this->funciones->generarPassword(10);
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();

			try{
				$sql="SELECT * FROM seguridad_usuarios WHERE usuar_codig ='".$codigo."'";
				$roles=$conexion->createCommand($sql)->queryRow();
				if($roles){
					if($roles['uesta_codig']=='2'){
						if($accio=='1'){
							$sql="UPDATE seguridad_usuarios 
								  SET uesta_codig = '".$estat."',
								      usuar_passw= md5('".$passw."')
								  WHERE usuar_codig ='".$codigo."'";
							$res1=$conexion->createCommand($sql)->execute();	 
							if($res1){

								$msg=array('success'=>'true','msg'=>'Usuario aprobado correctamente', 'sql'=>$sql);
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al aprobar el usuario');
							}
						}else{
							$sql="DELETE FROM registro
								  WHERE usuar_codig ='".$codigo."'";
							$res2=$conexion->createCommand($sql)->execute();	 
							if($res2){
								$sql="DELETE FROM seguridad_usuarios 
									  WHERE usuar_codig ='".$codigo."'";
								$res3=$conexion->createCommand($sql)->execute();
								if($res3){
									$estat=2;
									$msg=array('success'=>'true','msg'=>'Usuario rechazado correctamente');
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al eliminar el usuario');
								}
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al eliminar el registro');
							}
						}
						
						if($msg['success']=='true'){
							$sql="SELECT * 
									  FROM p_persona 
									  WHERE perso_codig='".$roles['perso_codig']."'";
								$p_persona=$conexion->createCommand($sql)->queryRow();
								$vista='../registro/acceso/verificar_correo';
								$datos['p_persona']=$p_persona;
								$datos['estatus']=$estat;
								$datos['usuario']=$roles['usuar_login'];
								$datos['clave']=$passw;
								$datos['motivo']=$motiv;
								$asunto='Polerones Tiempo | Solicitud de Acceso';
								$destinatario=$roles['usuar_login'];
								$this->funciones->enviarCorreo($vista,$datos,$asunto,$destinatario);
								$transaction->commit();
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Usuario ya esta aprobado');
					}	
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Usuario no existe');
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			
			$sql="SELECT * 
			  FROM seguridad_usuarios  a
			  JOIN registro b ON (a.usuar_codig and b.usuar_codig)
			  WHERE a.usuar_codig = b.usuar_codig 
			  AND a.usuar_codig = '".$_GET['c']."'";

			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('verificar', array('roles' => $roles));
		}
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}