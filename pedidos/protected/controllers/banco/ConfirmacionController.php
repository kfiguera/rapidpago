<?php

class ConfirmacionController extends Controller
{
	public $funciones;

	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}

	public function actionIndex()
	{
		$this->redirect(Yii::app()->request->baseUrl.'/pedidos/pedidos/listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['numero']){
			$numero=mb_strtoupper($_POST['numero']);
			$condicion.="a.pedid_numer like '%".$numero."%' ";
			$con++;
		}
		if($_POST['epedi']){
			if($con>0){
				$condicion.="AND ";
			}
			$epedi=mb_strtoupper($_POST['epedi']);
			$condicion.="a.epedi_codig like '%".$epedi."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM solicitud  a
			  WHERE a.solic_codig ='".$_GET['c']."'";
		$solicitud=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('solicitud' => $solicitud));
	}
	public function actionRegistrar()
	{
		if($_POST){
			//Datos del Pedido
			$canti=mb_strtoupper($_POST["canti"]);
			$mcant=mb_strtoupper($_POST["mcant"]);
			$color=mb_strtoupper($_POST["color"]);
			$mensa=mb_strtoupper($_POST["mensa"]);
			//Datos de Auditoria
			$estat=1;
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();

			try{
				//Obtenemos el codigo del pedido 
				$sql="SELECT max(pedid_codig) codigo FROM pedido_pedidos";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				$codig=$pedidos['codigo']+1;
				$cpedi=$this->funciones->generarCodigoPedido(5,$codig);

				//Guardamos el Pedido
				
				$sql="INSERT INTO pedido_pedidos( pedid_numer, pedid_total, pedid_cmode, pedid_obser, epedi_codig, usuar_codig, pedid_fcrea, pedid_hcrea) 
						VALUES ('".$cpedi."','".$canti."','".$mcant."','".$mensa."','".$estat."','".$usuar."','".$fecha."','".$hora."')";
				$res1=$conexion->createCommand($sql)->execute();
				if($res1){
					$transaction->commit();
					$msg=array('success'=>'true','msg'=>'Pedido guardado correctamente');	
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error al guardar el Pedido');	
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			//$this->render('registrar');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			try{
				//Obtenemos el codigo del pedido 
				if(Yii::app()->user->id['usuario']['permiso']!=1){
					$sql="SELECT * FROM pedido_pedidos WHERE epedi_codig in ('1','2','7') and usuar_codig='".$usuar."'";
					$verificar=$conexion->createCommand($sql)->queryRow();
				}else{
					$verificar=false;
				}
				if(!$verificar){
					//Datos del Pedido
					$canti=0;
					$mcant=0;
					$color=0;
					$mensa="";
					//Datos de Auditoria
					$estat=1;
					$usuar=Yii::app()->user->id['usuario']['codigo'];
					$fecha=date('Y-m-d');
					$hora=date('H:i:s');
					//Obtenemos el codigo del pedido 
					$sql="SELECT max(pedid_codig) codigo FROM pedido_pedidos";
					$pedidos=$conexion->createCommand($sql)->queryRow();
					$codig=$pedidos['codigo']+1;
					$cpedi=$this->funciones->generarCodigoPedido(5,$codig);

					//Guardamos el Pedido
					$sql="INSERT INTO pedido_pedidos( pedid_numer, pedid_total, pedid_cmode, pedid_obser, epedi_codig, usuar_codig, pedid_fcrea, pedid_hcrea) 
								VALUES ('".$cpedi."','".$canti."','".$mcant."','".$mensa."','".$estat."','".$usuar."','".$fecha."','".$hora."')";
					$res1=$conexion->createCommand($sql)->execute();
					if($res1){
						$transaction->commit();
						$sql="SELECT max(pedid_codig) pedid_codig FROM pedido_pedidos";
						$pedido=$conexion->createCommand($sql)->queryRow();

						$this->redirect('../detalle/registrar?p='.$pedido['pedid_codig']);
						
						$msg=array('success'=>'true','msg'=>'Pedido guardado correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al guardar el Pedido');	
					}
				}else{
					$transaction->rollBack();
					Yii::app()->user->setFlash('danger', 'Error Solo puede tener un pedido activo');
					$this->redirect('listado');
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
		}
	}
	public function actionPaso1()
	{
		if($_POST){
			//Datos del Detalle
			$pedid=mb_strtoupper($_POST['pedid']);
			$codig=mb_strtoupper($_POST['codig']);
			$pasos=mb_strtoupper($_POST['pasos']);
			
			$rifco=mb_strtoupper($_POST['rifco']);
			$rsoci=mb_strtoupper($_POST['rsoci']);
			$nfant=mb_strtoupper($_POST['nfant']);
			$acome=mb_strtoupper($_POST['acome']);
			$tmovi=mb_strtoupper($_POST['tmovi']);
			$tfijo=mb_strtoupper($_POST['tfijo']);
			$estad=mb_strtoupper($_POST['estad']);
			$munic=mb_strtoupper($_POST['munic']);
			$parro=mb_strtoupper($_POST['parro']);
			$ciuda=mb_strtoupper($_POST['ciuda']);
			$celec=mb_strtoupper($_POST['celec']);
			$dfisc=mb_strtoupper($_POST['dfisc']);
			$banco=mb_strtoupper($_POST['banco']);
			$cafil=mb_strtoupper($_POST['cafil']);
			$ncuen=mb_strtoupper($_POST['ncuen']);
			$nterm=mb_strtoupper($_POST['nterm']);
			
			//Datos de Auditoria
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();

			$monto=0;
			if($pasos<'2'){
				$pasos='2';
			}
			
			try{

				$sql="SELECT * FROM solicitud
					WHERE solic_codig='".$codig."';";
				$solicitud=$conexion->createCommand($sql)->queryRow();
				if($solicitud){

					$sql="UPDATE solicitud
					    SET solic_celec= '".$celec."',
							banco_codig= '".$banco."',
							solic_cafil= '".$cafil."',
							solic_ncuen= '".$ncuen."',
							solic_nterm= '".$nterm."'
					WHERE solic_codig = '".$codig."'";
					$query=$sql;
					$res1=$conexion->createCommand($sql)->execute();

					$sql="SELECT * FROM solicitud
							  WHERE solic_codig='".$codig."'";
						
					$solicitud2=$conexion->createCommand($sql)->queryRow();
					
					$traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud', json_encode($solicitud),json_encode($solicitud2),$query);

					$sql="SELECT * FROM cliente
					WHERE clien_codig='".$solicitud['clien_codig']."';";
					$cliente=$conexion->createCommand($sql)->queryRow();

					if($cliente){
						$sql="UPDATE cliente
					    SET clien_rifco= '".$rifco."',
							clien_rsoci= '".$rsoci."',
							clien_nfant= '".$nfant."',
							acome_codig= '".$acome."',
							clien_tmovi= '".$tmovi."',
							clien_tmovi= '".$tmovi."',
							clien_tfijo= '".$tfijo."',
							clien_celec= '".$celec."'
						WHERE clien_codig = '".$solicitud['clien_codig']."'";
						$query=$sql;
						$res1=$conexion->createCommand($sql)->execute();

						$sql="SELECT * FROM cliente
								  WHERE clien_codig='".$solicitud['clien_codig']."'";
							
						$cliente2=$conexion->createCommand($sql)->queryRow();
						
						$traza=$this->funciones->guardarTraza($conexion, 'U', 'cliente', json_encode($cliente),json_encode($cliente),$query);

						$sql="SELECT * FROM cliente_direccion
						WHERE clien_codig='".$solicitud['clien_codig']."';";
						$direccion=	$conexion->createCommand($sql)->queryRow();
						if($direccion){

							$sql="UPDATE cliente_direccion
						    SET estad_codig= '".$estad."',
								munic_codig= '".$munic."',
								parro_codig= '".$parro."',
								ciuda_codig= '".$ciuda."',
								direc_dfisc= '".$dfisc."'
							WHERE clien_codig = '".$solicitud['clien_codig']."'";
							$query=$sql;
							$res1=$conexion->createCommand($sql)->execute();

							$sql="SELECT * FROM cliente
									  WHERE clien_codig='".$solicitud['clien_codig']."'";
								
							$direccion2=$conexion->createCommand($sql)->queryRow();
							
							$traza=$this->funciones->guardarTraza($conexion, 'U', 'cliente_direccion', json_encode($direccion),json_encode($direccion2),$query);
						}
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Solicitud actualizada Correctamente');	

					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error no existe el cliente');	
					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error no existe la Solicitud');	
				}
				
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);

		}else{
			$p=$_GET['p'];

			$s=1;

			$this->render('registrar',array('p' => $p,'s' => $s ));
		}
	}

	public function actionPaso2()
	{
		if($_POST){
			//Datos del Detalle
			$pedid=mb_strtoupper($_POST['pedid']);
			$codig=mb_strtoupper($_POST['codig']);
			$pasos=mb_strtoupper($_POST['pasos']);
			
			//Datos del Formulario
			$otele=$_POST['otele'];
			$canti=$_POST['canti'];
			
			//Datos de Auditoria
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();


			if($pasos<'3'){
				$pasos='3';
			}
			$operadores = asort($otele);
			$ant='';
			foreach ($otele as $key => $value) {
				if($value==$ant){
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No puede incluir un Operador Telefónico más de una vez');
					echo json_encode($msg);
					exit();
				}
				$ant=$value;
			}

			try{
				$sql="SELECT * FROM solicitud
					WHERE solic_codig='".$codig."';";
				$solicitud=$conexion->createCommand($sql)->queryRow();
				
				
				if($solicitud){
					$i=0;
					$incluye='';
					$sql="DELETE FROM solicitud_equipos 
						  WHERE solic_codig ='".$codig."'";
					$res2=$conexion->createCommand($sql)->execute();

					foreach ($otele as $key => $value) {
						$sql="SELECT * 
							  FROM solicitud_equipos 
							  WHERE solic_codig ='".$codig."' 
							    AND otele_codig ='".$otele[$key]."'";	
						$cequipo=$conexion->createCommand($sql)->queryRow();
						if($cequipo){
							$sql="UPDATE solicitud_equipos 
								  SET cequi_canti = '".$canti[$key]."' 
								  WHERE cequi_codig='".$cequipo['cequi_codig']."'";
							$opera='U';
						}else{
							$sql="INSERT INTO solicitud_equipos(solic_codig, otele_codig, cequi_canti, usuar_codig, cequi_fcrea, cequi_hcrea) VALUES ('".$codig."', '".$otele[$key]."', '".$canti[$key]."', '".$usuar."', '".$fecha."', '".$hora."' )";
							$opera='I';

						}
						$query=$sql;
						$res2=$conexion->createCommand($sql)->execute();

						$sql="SELECT * 
							  FROM solicitud_equipos 
							  WHERE solic_codig ='".$codig."' 
							    AND otele_codig ='".$otele[$key]."'";	
						$cequipo2=$conexion->createCommand($sql)->queryRow();
						
						$antes=json_encode($cequipo);
						$actual=json_encode($cequipo2);

						$traza=$this->funciones->guardarTraza($conexion, $opera, 'solicitud_equipos', $antes,$actual,$query);
						$i++;
					}

					    $transaction->commit();
						$msg=array('success'=>'true','msg'=>'Solicitud actualizada correctamente');	
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error no existe la Solicitud');	
				}
					
				
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);

		}else{
			$p=$_GET['p'];
			$s=1;

			$this->render('registrar',array('p' => $p,'s' => $s ));
		}
	}
	public function actionPaso3()
	{
		if($_POST){

			//Datos del Detalle
			$pedid=mb_strtoupper($_POST['pedid']);
			$codig=mb_strtoupper($_POST['codig']);
			$pasos=mb_strtoupper($_POST['pasos']);
			//Datos del Formulario

			$rifrl=$_POST['rifrl'];
			$nombr=$_POST['nombr'];
			$apell=$_POST['apell'];
			$tdocu=$_POST['tdocu'];
			$ndocu=$_POST['ndocu'];
			$cargo=$_POST['cargo'];
			$tmovi=$_POST['tmovi'];
			$tfijo=$_POST['tfijo'];
			$celec=$_POST['celec'];



			//Datos de Auditoria
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();

			$monto=0;
			if($pasos<'3'){
				$pasos='3';
			}
			try{
				$sql="SELECT * FROM solicitud
					WHERE solic_codig='".$codig."';";
				$solicitud=$conexion->createCommand($sql)->queryRow();
								
				if($solicitud){
					$r=0;
					foreach ($rifrl as $key => $value) {
						$rifrl[$key]=strtoupper($rifrl[$key]);
						$nombr[$key]=strtoupper($nombr[$key]);
						$apell[$key]=strtoupper($apell[$key]);
						$tdocu[$key]=strtoupper($tdocu[$key]);
						$ndocu[$key]=strtoupper($ndocu[$key]);
						$cargo[$key]=strtoupper($cargo[$key]);
						$tmovi[$key]=strtoupper($tmovi[$key]);
						$tfijo[$key]=strtoupper($tfijo[$key]);
						$celec[$key]=strtoupper($celec[$key]);


						$sql="SELECT * 
							  FROM cliente_representante 
							  WHERE clien_codig='".$solicitud['clien_codig']."'
							    AND rlega_rifrl ='".$rifrl[$key]."'";	
						$representante=$conexion->createCommand($sql)->queryRow();

						if($representante){
							
							$sql="UPDATE cliente_representante 
								  SET rlega_rifrl= '".$rifrl[$key]."', 
									  rlega_nombr= '".$nombr[$key]."', 
									  rlega_apell= '".$apell[$key]."', 
									  tdocu_codig= '".$tdocu[$key]."', 
									  rlega_ndocu= '".$ndocu[$key]."', 
									  rlega_cargo= '".$cargo[$key]."', 
									  rlega_tmovi= '".$tmovi[$key]."', 
									  rlega_tfijo= '".$tfijo[$key]."', 
									  rlega_corre= '".$celec[$key]."'
								  WHERE rlega_codig='".$representante['rlega_codig']."'";	
							$opera='U';							  
						}else{
							$sql="INSERT INTO cliente_representante(clien_codig, rlega_rifrl, rlega_nombr, rlega_apell, tdocu_codig, rlega_ndocu, rlega_cargo, rlega_tmovi, rlega_tfijo, rlega_corre, usuar_codig, rlega_fcrea, rlega_hcrea) 
							  VALUES ('".$solicitud['clien_codig']."', '".$rifrl[$key]."', '".$nombr[$key]."', '".$apell[$key]."', '".$tdocu[$key]."', '".$ndocu[$key]."', '".$cargo[$key]."', '".$tmovi[$key]."', '".$tfijo[$key]."', '".$celec[$key]."', '".$usuar."', '".$fecha."', '".$hora."')";

							$opera='I';

						}
						$res2=$conexion->createCommand($sql)->execute();
						
						$query=$sql;
						$sql="SELECT * 
							  FROM cliente_representante 
							  WHERE clien_codig='".$solicitud['clien_codig']."' 
							    AND rlega_rifrl ='".$rifrl[$key]."'";	
						$representante2=$conexion->createCommand($sql)->queryRow();
						
						$antes=json_encode($representante);
						$actual=json_encode($representante2);

						$traza=$this->funciones->guardarTraza($conexion, $opera, 'cliente_representante', $antes,$actual,$query);

						if($r>0){
							$eliminar.=',';	
						}
						$eliminar.="'".$rifrl[$key]."'";
						$r++;
					}
					
					


					if($eliminar!=''){

						$sql="SELECT * 
							  FROM cliente_representante 
							  WHERE clien_codig='".$solicitud['clien_codig']."'
								AND rlega_rifrl NOT IN (".$eliminar.")";
						$solicitudes=$conexion->createCommand($sql)->queryAll();

						$sql="DELETE FROM cliente_representante 
							  WHERE clien_codig='".$solicitud['clien_codig']."' 
							    AND rlega_rifrl NOT IN (".$eliminar.")";
						$query=$sql;						
						$res3=$conexion->createCommand($sql)->execute();

						$opera='D';
	
						$sql="SELECT * 
							  FROM cliente_representante 
							  WHERE clien_codig='".$solicitud['clien_codig']."' 
								AND rlega_rifrl NOT IN (".$eliminar.")";
						$solicitudes2=$conexion->createCommand($sql)->queryAll();
						
						$antes=json_encode($solicitudes);
						$actual=json_encode($solicitudes2);
						
						$traza=$this->funciones->guardarTraza($conexion, $opera, 'cliente_representante', $antes,$actual,$query);
					
					}
					
					$transaction->commit();
					$msg=array('success'=>'true','msg'=>'Solicitud actualizada correctamente');	
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error no existe la Solicitud');	
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);

		}else{
			$p=$_GET['p'];
			$s=1;

			$this->render('registrar',array('p' => $p,'s' => $s ));
		}
	}
	public function actionPaso4()
	{
		if($_POST){
			//Datos del Detalle
			$pedid=mb_strtoupper($_POST['pedid']);
			$codig=mb_strtoupper($_POST['codig']);
			$pasos=mb_strtoupper($_POST['pasos']);
			
			$fsoli=$this->funciones->TransformarFecha_bd(mb_strtoupper($_POST['fsoli']));
			$orige=mb_strtoupper($_POST['orige']);
			$clvip=mb_strtoupper($_POST['clvip']);
			$obser=mb_strtoupper($_POST['obser']);
			
			//Datos de Auditoria
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();

			$monto=0;
			if($pasos<'4'){
				$pasos='4';
			}
			
			try{
				$sql="SELECT * FROM solicitud
					WHERE solic_codig='".$codig."';";
				$solicitud=$conexion->createCommand($sql)->queryRow();
				if($solicitud['estat_codig']==1){
					$estat=2;

					$trayectoria=$this->funciones->guardarTrayectoria($conexion, $codig, $estat, $solicitud['estat_codig'], 2);
				}else{
					$estat=$solicitud['estat_codig'];
				}
				if($solicitud){

					$sql="UPDATE solicitud
					    SET solic_fsoli= '".$fsoli."',
							orige_codig= '".$orige."',
							solic_clvip= '".$clvip."',
							solic_obser= '".$obser."'
					WHERE solic_codig = '".$codig."'";
					
					$query=$sql;


					$res1=$conexion->createCommand($sql)->execute();
						
					$sql="SELECT * FROM solicitud
						  WHERE solic_codig='".$codig."'";
					
					$solicitud2=$conexion->createCommand($sql)->queryRow();
					
					$antes=json_encode($solicitud);
					$actual=json_encode($solicitud2);

					$traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud', $antes,$actual,$query);

					$transaction->commit();
					$msg=array('success'=>'true','msg'=>'Solicitud actualizada correctamente');	
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error no existe la Solicitud');	
				}
				
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);

		}else{
			$p=$_GET['p'];

			$s=1;

			$this->render('registrar',array('p' => $p,'s' => $s ));
		}
	}

	public function actionPaso5()
	{
		if($_POST){
			//Datos del Detalle
			$pedid=mb_strtoupper($_POST['pedid']);
			$codig=mb_strtoupper($_POST['codig']);
			$pasos=mb_strtoupper($_POST['pasos']);
			//DATOS DE LOS DOCUMENTOS
			$dtipo=$_POST["dtipo"];

			$file=$_FILES['image'];
			/*$ruta='files/detalle/listado/';
			$name=explode(".", $file['name']);
			$nombre=date('Ymd_His.').end($name);
			$destino=$ruta.$nombre;*/
			$codigo=$this->funciones->generarCodigoPedido(10,$codig);
			foreach ($_FILES['image']['name'] as $key => $value) {
				$ruta='files/solicitudes/'.$codigo.'/';
				$name[$key]=explode(".", $file['name'][$key]);
				$nombre[$key]=date('Ymd_His_').$key.'.'.end($name[$key]);
				$destino[$key]=$ruta.$nombre[$key];
			}


			//Datos de Auditoria
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();

			$monto=0;
			if($pasos<'5'){
				$pasos='5';
			}
			$verificar['success']='true';
			try{
				
				foreach ($dtipo as $key => $value) {
					
					if(!empty($_FILES['image']['tmp_name'][$key])){
						$archivo['tmp_name']=$_FILES['image']['tmp_name'][$key];
						$verificar=$this->funciones->CopiarEmoji($archivo,$ruta,$nombre[$key]);
						if($verificar['success']!='true'){
							$transaction->rollBack();
							$msg=$verificar;
							echo json_encode($msg);	
							exit();
						}
					}else{
						$destino[$key]='';	
					}
				}

				if($verificar['success']=='true'){

					$sql="SELECT * FROM solicitud
						WHERE solic_codig='".$codig."';";
					$solicitudes=$conexion->createCommand($sql)->queryRow();

					if($solicitudes['estat_codig']<=2){
						$estat=3;
						$trayectoria=$this->funciones->guardarTrayectoria($conexion, $codig, $estat, $solicitudes['estat_codig'], 2);
					}else{
						$estat=$solicitudes['estat_codig'];
					}
					if($solicitudes){
						$a=0;
						$incluyen="";

						foreach ($dtipo as $key => $value) {
							
							
							if($dtipo[$key]!='' or $destino[$key]!=''){
								$dtipo[$key]=mb_strtoupper($dtipo[$key]);

								$sql="SELECT * FROM solicitud_documento WHERE solic_codig ='".$codig."' and 	docum_orden='".$key."'";
								$result=$conexion->createCommand($sql)->queryRow();
								
								if($result){
									if($destino[$key]==''){
										$sql="UPDATE solicitud_documento 
											  SET docum_ruta = '".$result['docum_ruta']."',
											  	  dtipo_codig = '".$dtipo[$key]."'
											  WHERE docum_codig='".$result['docum_codig']."'";	
									}else{
										$sql="UPDATE solicitud_documento 
										SET docum_ruta = '".$destino[$key]."',
											dtipo_codig = '".$dtipo[$key]."'
										WHERE docum_codig='".$result['docum_codig']."'";
									}
									$opera='U';
								}else {
									$sql="INSERT INTO solicitud_documento(solic_codig, dtipo_codig, docum_ruta, docum_orden, usuar_codig, docum_fcrea, docum_hcrea)
										VALUES ('".$codig."','".$dtipo[$key]."','".$destino[$key]."','".$key."','".$usuar."','".$fecha."','".$hora."');";
									$opera='I';
								}
								$query=$sql;

								$res2=$conexion->createCommand($sql)->execute();

								$sql="SELECT * FROM solicitud_documento WHERE solic_codig ='".$codig."' and 	docum_orden='".$key."'";
								$result2=$conexion->createCommand($sql)->queryRow();

								$antes=json_encode($result);
								$actual=json_encode($result2);

								$traza=$this->funciones->guardarTraza($conexion, $opera, 'solicitud_documento', $antes,$actual,$query);


								if($a>0 and $incluyen!=''){
									$incluyen.=",";
								}
								$incluyen.="'".$key."'";
							}
							

							$a++;
						}

						

						if($incluyen!=''){
							$sql="SELECT * 
							  FROM solicitud_documento 
							  WHERE solic_codig = '".$codig."' 
							    AND docum_orden not in (".$incluyen.")";
							$elimi=$conexion->createCommand($sql)->queryAll();
								
							$sql="DELETE  FROM solicitud_documento 
							  WHERE solic_codig = '".$codig."' 
							    AND docum_orden not in (".$incluyen.")";
							$query=$sql;
							
							$res3=$conexion->createCommand($sql)->execute();

							$opera='D';

							$sql="SELECT * 
							  FROM solicitud_documento 
							  WHERE solic_codig = '".$codig."' 
							    AND docum_orden not in (".$incluyen.")";

							$elimi2=$conexion->createCommand($sql)->queryAll();
						
							$antes=json_encode($elimi);
							$actual=json_encode($elimi2);
							
							$traza=$this->funciones->guardarTraza($conexion, $opera, 'solicitud_documento', $antes,$actual,$query);
						}
						
						
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Solicitud actualizada correctamente');	
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error no existe la Solicitud');	
					}
				}else{
					$transaction->rollBack();
					$msg=$verificar;
				}
			}catch(Exception $e){
				echo $sql;
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);

		}else{
			$p=$_GET['p'];
			$s=1;

			$this->render('registrar',array('p' => $p,'s' => $s ));
		}
	}

	public function actionModificar()
	{
		if($_POST){
			//Datos de la Solicitud
			$codig=mb_strtoupper($_POST["codig"]);
			$canti=mb_strtoupper($_POST["canti"]);
			$mcant=mb_strtoupper($_POST["mcant"]);
			$color=mb_strtoupper($_POST["color"]);
			$mensa=mb_strtoupper($_POST["mensa"]);
			//Datos de Auditoria
			$estat=1;
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				
				$sql="SELECT * FROM rd_preregistro WHERE prere_codig ='".$codig."'";
				$solicitud=$conexion->createCommand($sql)->queryRow();
				if($solicitud){
					$sql="UPDATE rd_preregistro
						  SET prere_total='".$canti."',
							  prere_cmode='".$mcant."',
							  prere_obser='".$mensa."'
						  WHERE prere_codig ='".$codig."'";
					$res1=$conexion->createCommand($sql)->execute();
					if($res1){
						$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Pedido actualizado correctamente');
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al actualizar la Solicitud');	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$s=$_GET['s'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM solicitud  a
			  WHERE a.solic_codig ='".$_GET['c']."'";
			$solicitud=$conexion->createCommand($sql)->queryRow();
			$ubicacion[0]='start ';
			$ubicacion[4]='finish ';
			for ($i=0; $i < 6 ; $i++) { 
				if($i<6){
					$ubicacion[$i].='upcoming ';
				}
				if ($i==($s-1)) {
					$ubicacion[$i].='active ';
				}
			}
			
			switch ($s) {
				case '1':
					$this->render('paso1', array('solicitud' => $solicitud, 'ubicacion' => $ubicacion,'c' => $c,'s' => $s));
					break;
				case '2':
					$this->render('paso2', array('solicitud' => $solicitud, 'ubicacion' => $ubicacion,'c' => $c,'s' => $s));
					break;
				case '3':
					$this->render('paso3', array('solicitud' => $solicitud, 'ubicacion' => $ubicacion,'c' => $c,'s' => $s));
					break;
				case '3':
					$this->render('paso3', array('solicitud' => $solicitud, 'ubicacion' => $ubicacion,'p' => $p,'c' => $c,'s' => $s));
					break;
				case '4':
					$this->render('paso4', array('solicitud' => $solicitud, 'ubicacion' => $ubicacion,'p' => $p,'c' => $c,'s' => $s));
					break;
				case '5':
					$this->render('paso5', array('solicitud' => $solicitud, 'ubicacion' => $ubicacion,'p' => $p,'c' => $c,'s' => $s));
					break;
				default:
					$this->render('paso1', array('solicitud' => $solicitud,'ubicacion' => $ubicacion,'c' => $c,'s' => $s));
					break;
			}
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_pedidos  WHERE pedid_codig ='".$codig."'";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				if($pedidos){
					$sql="SELECT * FROM pedido_pedidos_detalle  WHERE pedid_codig ='".$codig."'";
					$detalle=$conexion->createCommand($sql)->queryRow();
					if(!$detalle){	

						$sql="DELETE FROM pedido_pedidos_adicional WHERE pedid_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();

						$sql="DELETE FROM pedido_pedidos_deduccion WHERE pedid_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();

						$sql="DELETE FROM pedido_pedidos_imagen WHERE pedid_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();

						$sql="DELETE FROM pedido_pedidos WHERE pedid_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();

						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Pedido eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Pedido');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Pedido no se puede eliminar debido a que tiene modelos asociados');
					}		
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
				
			}catch(Exception $e){
				$transaction->rollBack();
				var_dump($e);
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_pedidos  a
			  WHERE a.pedid_codig ='".$_GET['c']."'";
			$pedidos=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('pedidos' => $pedidos,'c'=>$c));
		}
	}
	public function actionEnviar()
	{
		if($_POST){

			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_pedidos WHERE pedid_codig ='".$codig."'";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				if($pedidos){
					if($pedidos['pedid_pasos']=='3'){
						$sql="SELECT * FROM pedido_pedidos_detalle  WHERE pedid_codig ='".$codig."'";
						$detalles=$conexion->createCommand($sql)->queryAll();
						if($detalles){

							foreach ($detalles as $key => $value) {
								$sql="SELECT * FROM pedido_pedidos_detalle_listado  WHERE pdeta_codig ='".$value['pdeta_codig']."'";
								$modelo=$conexion->createCommand($sql)->queryAll();
								if($modelo){
									if($value['pdeta_pasos']>='2'){
										$msg=array('success'=>'true','msg'=>' Detalle eliminado correctamente');
										$cmode++;
										$cpers+=count($modelo);	
									}else{
										$transaction->rollBack();
										$msg=array('success'=>'false','msg'=>'Termine todos los pasos de los modelos y el pedido para continuar');	
										echo json_encode($msg);
										exit();
									}
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Existe Un modelo sin p_personas asociadas verificar');	
									echo json_encode($msg);
									exit();
								}
							}
							if($msg['success']=='true'){
								$sql="UPDATE pedido_pedidos
									  SET pedid_total='".$cpers."',
										  pedid_cmode='".$cmode."',
										  epedi_codig='2'
									  WHERE pedid_codig ='".$codig."'";
								$res1=$conexion->createCommand($sql)->execute();

								if($res1){
									$transaction->commit();
									$msg=array('success'=>'true','msg'=>' Pedido enviado correctamente');	
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al enviar el Pedido');	
								}
							}
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'El pedido no tiene modelos asociados');
						}
					}else{
						$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Debe finalizar el pedido antes de enviarlo');
					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
				
			}catch(Exception $e){

				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$p=$_GET['p'];
			$c=$_GET['c'];
			$s=$_GET['s'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_pedidos  a
			  WHERE a.pedid_codig ='".$_GET['c']."'";
			$pedidos=$conexion->createCommand($sql)->queryRow();
			$this->render('enviar', array('pedidos' => $pedidos, 'ubicacion' => $ubicacion,'p' => $p,'c' => $c,'s' => $s));
		}
	}
	public function actionAprobar()
	{
		if($_POST){
			$codig=($_POST['codig']);
			$mcant=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['mcant']));
			$total=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['total']));
			$monto=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['monto']));
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_pedidos WHERE pedid_codig ='".$codig."'";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				if($pedidos){
					if($pedidos['epedi_codig']=='2'){
						$sql="UPDATE pedido_pedidos
							  SET pedid_total='".$total."',
								  pedid_cmode='".$mcant."',
								  pedid_monto='".$monto."',
								  epedi_codig='7'
							  WHERE pedid_codig ='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();

						if($res1){
							$sql="SELECT a.*, b.*
								  FROM p_persona a
								  JOIN seguridad_usuarios b ON (a.perso_codig=b.perso_codig)
								  WHERE b.usuar_codig='".$pedidos['usuar_codig']."'";
                    		$p_persona=$conexion->createCommand($sql)->queryRow();

                    		$sql="SELECT * FROM pedido_pedidos WHERE pedid_codig ='".$codig."'";
							$pedidos=$conexion->createCommand($sql)->queryRow();

							$datos['row']=$p_persona;
							$datos['totales']=$pedidos;
							$corre=$p_persona['usuar_login'];
                            $this->funciones->enviarCorreo('../pedidos/pedidos/aprobar_correo',$datos,'Polerones Tiempo | Pedido Aprobado',$corre);
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Pedido Aprobado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al enviar el Pedido');	
						}
						
					}else{
						$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Debe envar el pedido antes de Aprobarlo');
					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
				
			}catch(Exception $e){

				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_pedidos  a
			  WHERE a.pedid_codig ='".$_GET['c']."'";
			$pedidos=$conexion->createCommand($sql)->queryRow();
			$totales=$this->funciones->CalcularMontoPedido($pedidos);
			$this->render('aprobar', array('pedidos' => $pedidos, 'totales' => $totales, 'c'=>$c));
		}
	}
	public function actionRechazar()
	{
		if($_POST){
			$codig=($_POST['codig']);
			$mcant=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['mcant']));
			$total=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['total']));
			$monto=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['monto']));
			$obser=mb_strtoupper($_POST['obser']);
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_pedidos WHERE pedid_codig ='".$codig."'";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				if($pedidos){
					//if($pedidos['epedi_codig']=='2'){
						$sql="UPDATE pedido_pedidos
							  SET pedid_total='".$total."',
								  pedid_cmode='".$mcant."',
								  pedid_monto='".$monto."',
								  epedi_codig='1'
							  WHERE pedid_codig ='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();

						if($res1){
							$sql="SELECT a.*, b.*
								  FROM p_persona a
								  JOIN seguridad_usuarios b ON (a.perso_codig=b.perso_codig)
								  WHERE b.usuar_codig='".$pedidos['usuar_codig']."'";
                    		$p_persona=$conexion->createCommand($sql)->queryRow();

                    		$sql="SELECT * FROM pedido_pedidos WHERE pedid_codig ='".$codig."'";
							$pedidos=$conexion->createCommand($sql)->queryRow();

							$datos['row']=$p_persona;
							$datos['totales']=$pedidos;
							$datos['obser']=$obser;
							$corre=$p_persona['usuar_login'];
                            //$this->funciones->enviarCorreo('../pedidos/pedidos/rechazar_correo',$datos,'Polerones Tiempo | Pedido Rechazado',$corre);
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Pedido Rechazado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al enviar el Pedido');	
						}
						
					/*}else{
						$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Debe envar el pedido antes de Rechazarlo');
					}*/
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
				
			}catch(Exception $e){

				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_pedidos  a
			  WHERE a.pedid_codig ='".$_GET['c']."'";
			$pedidos=$conexion->createCommand($sql)->queryRow();
			$totales=$this->funciones->CalcularMontoPedido($pedidos);
			$this->render('rechazar', array('pedidos' => $pedidos, 'totales' => $totales, 'c'=>$c));
		}
	}


	public function actionCostos()
	{
		if($_POST){
			$codig=($_POST['codig']);
			$costo=mb_strtoupper($_POST['costo']);
			$obser=mb_strtoupper($_POST['obser']);
			$monto=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['monto']));
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_pedidos WHERE pedid_codig ='".$codig."'";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				if($pedidos){
					if($pedidos['epedi_codig']=='2'){
						$sql="INSERT INTO pedido_pedidos_adicional(pedid_codig, costo_codig, adici_monto, adici_obser, usuar_codig, adici_fcrea, adici_hcrea) 
							VALUES ('".$codig."','".$costo."','".$monto."','".$obser."','".$usuar."','".$fecha."','".$hora."');";
						$res1=$conexion->createCommand($sql)->execute();

						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Costo Adicional agregado Correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al agregar el Costo');	
						}
						
					}else{
						$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Debe envar el pedido antes de asignar un costo adicional');
					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
				
			}catch(Exception $e){

				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$p=$_GET['p'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_pedidos  a
			  WHERE a.pedid_codig ='".$_GET['c']."'";
			$pedidos=$conexion->createCommand($sql)->queryRow();
			$totales=$this->funciones->CalcularMontoPedido($pedidos);
			$this->render('costos', array('pedidos' => $pedidos, 'totales' => $totales, 'p'=>$p, 'c'=>$c));
		}
	}
	public function actionCostosEliminar()
	{
		if($_POST){
			$codig=($_POST['codig']);
			$costo=mb_strtoupper($_POST['costo']);
			$obser=mb_strtoupper($_POST['obser']);
			$monto=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['monto']));
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_pedidos_adicional WHERE adici_codig ='".$codig."'";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				if($pedidos){

					$sql="DELETE FROM pedido_pedidos_adicional WHERE adici_codig ='".$codig."'";
					$res1=$conexion->createCommand($sql)->execute();

					if($res1){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>' Costo Adicional eliminado Correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al eliminado el Costo');	
					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El adicional no existe');
				}
				
			}catch(Exception $e){

				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$p=$_GET['p'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_pedidos_adicional  a
			  WHERE a.adici_codig ='".$_GET['c']."'";
			$costos=$conexion->createCommand($sql)->queryRow();
			$totales=$this->funciones->CalcularMontoPedido($pedidos);
			$this->render('costosEliminar', array('costos' => $costos, 'totales' => $totales, 'c'=>$c, 'p'=>$p));
		}
	}

	public function actionDeduccion()
	{
		if($_POST){
			$codig=($_POST['codig']);
			$deduc=mb_strtoupper($_POST['deduc']);
			$obser=mb_strtoupper($_POST['obser']);
			$monto=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['monto']));
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_pedidos WHERE pedid_codig ='".$codig."'";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				if($pedidos){
					$sql="SELECT * FROM pedido_pedidos_deduccion WHERE deduc_codig='".$deduc."' and pedid_codig = '".$codig."'";
					$deduciones=$conexion->createCommand($sql)->queryRow();
					if(!$deduciones){
						if($pedidos['epedi_codig']=='2'){
							$sql="INSERT INTO pedido_pedidos_deduccion(pedid_codig, deduc_codig, pdedu_monto, pdedu_obser, usuar_codig, pdedu_fcrea, pdedu_hcrea) 
								VALUES ('".$codig."','".$deduc."','".$monto."','".$obser."','".$usuar."','".$fecha."','".$hora."');";
							$res1=$conexion->createCommand($sql)->execute();

							if($res1){
								$transaction->commit();
								$msg=array('success'=>'true','msg'=>' Deducción agregado Correctamente');	
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al agregar la Deducción');	
							}
							
						}else{
							$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Debe envar el pedido antes de asignar una Deducción');
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Ya se agrego la deducción');
					}
						
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
				
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$p=$_GET['p'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_pedidos  a
			  WHERE a.pedid_codig ='".$_GET['c']."'";
			$pedidos=$conexion->createCommand($sql)->queryRow();
			$totales=$this->funciones->CalcularMontoPedido($pedidos);
			$this->render('deduccion', array('pedidos' => $pedidos, 'totales' => $totales, 'c'=>$c, 'p'=>$p));
		}
	}
	public function actionDeduccionEliminar()
	{
		if($_POST){
			$codig=($_POST['codig']);
			$deduc=mb_strtoupper($_POST['deduc']);
			$obser=mb_strtoupper($_POST['obser']);
			$monto=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['monto']));
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_pedidos_deduccion WHERE pdedu_codig ='".$codig."'";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				if($pedidos){
						
					$sql="DELETE FROM pedido_pedidos_deduccion WHERE pdedu_codig ='".$codig."'";
					$res1=$conexion->createCommand($sql)->execute();

					if($res1){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>' Deducción eliminado Correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al eliminado la Deducción');	
					}
							
						
						
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
				
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$p=$_GET['p'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_pedidos_deduccion  a
			  WHERE a.pdedu_codig ='".$_GET['c']."'";
			$deduccion=$conexion->createCommand($sql)->queryRow();
			$totales=$this->funciones->CalcularMontoPedido($pedidos);
			$this->render('deduccionEliminar', array('deduccion' => $deduccion, 'totales' => $totales, 'c'=>$c, 'p'=>$p));
		}
	}
	public function actionVerificar()
	{
		if($_POST){
			//Datos del Detalle
			$pedid=mb_strtoupper($_POST['pedid']);
			$codig=mb_strtoupper($_POST['codig']);
			$pasos=mb_strtoupper($_POST['pasos']);
			$cafil=mb_strtoupper($_POST['cafil']);
			$ncuen=mb_strtoupper($_POST['ncuen']);

			$accio=mb_strtoupper($_POST['accio']);

			$motiv=mb_strtoupper($_POST['motiv']);
			$obser=mb_strtoupper($_POST['obser']);
			//DATOS DE LOS DOCUMENTOS
			$numer=$_POST["numer"];
			//Datos de Auditoria
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			$ant='';
			$pre='';
			$dispo=$numer;
			asort($dispo);
			$canti=0;
			if($accio=='1'){
				$estat=14;
			}else{
				$estat=0;
			}

			try{
				$sql="SELECT * FROM solicitud
					WHERE solic_codig='".$codig."';";
				$solicitud=$conexion->createCommand($sql)->queryRow();

				if($solicitud){

					$sql="UPDATE solicitud
					    SET estat_codig= '".$estat."'
					WHERE solic_codig = '".$codig."'";
					$query=$sql;
					$res1=$conexion->createCommand($sql)->execute();

					$sql="SELECT * FROM solicitud
							  WHERE solic_codig='".$codig."'";
						
					$solicitud2=$conexion->createCommand($sql)->queryRow();
					
					$traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud', json_encode($solicitud),json_encode($solicitud2),$query);

					
						$notificacion=$this->funciones->GuardarNotificacion($solicitud['solic_codig'], $estat);
						
						$trayectoria=$this->funciones->guardarTrayectoriaSolicitudes($conexion, $solicitud['solic_codig'], $estat, $solicitud['estat_codig'], 2);

						
						$sql="INSERT INTO solicitud_confirmacion_banco(solic_codig, banco_codig, accio_codig, bconf_obser, usuar_codig, bconf_fcrea, bconf_hcrea) 
							  VALUES ('".$codig."',  '".$solicitud['banco_codig']."',  '".$accio."',  '".$obser."', '".$usuar."', '".$fecha."', '".$hora."')";
						$res2=$conexion->createCommand($sql)->execute();
						
						$sql="SELECT * FROM solicitud_confirmacion_banco
							  WHERE solic_codig='".$codig."'";
						$query=$sql;
						$certificacion_banco=$conexion->createCommand($sql)->queryRow();
						$traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud_certificacion_banco', json_encode(''),json_encode($certificacion_banco),$query);

						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Solicitud actualizada correctamente');	
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error no existe la Solicitud');	
				}
			}catch(Exception $e){
				echo $sql;
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);

		}else{
			$conexion=Yii::app()->db;
			$c=$_GET['c'];
			$s=1;
			$sql="SELECT * 
			  FROM solicitud  a
			  WHERE a.solic_codig ='".$_GET['c']."'";
			$solicitud=$conexion->createCommand($sql)->queryRow();
			$this->render('verificar',array('solicitud' => $solicitud, 'c' => $c));
		}
	}
	public function actionAnular()
	{
		$this->render('anular');
	}
	
	public function actionGenerarCsv()
	{
		if($_POST){
			
			$id=$_POST['id'];
			if(!$id){
				$msg=array('success'=>'false','msg'=>'Debe seleccionar al menos una solicitud');
				echo json_encode($msg);
				exit();
			}
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$estat=13;
			$ruta="files/tramitacion/confirmacion/";
			$nombre=date('Ymd_His').'.csv';
			$destino=$ruta.$nombre;


			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{


				$i=0;
				$ant='';
				foreach ($id as $key => $value) {
					$sql="SELECT * FROM solicitud WHERE solic_codig='".$value."'";
			        	$solicitud=$conexion->createCommand($sql)->queryRow();
					if($i>0){


			        	$banco[$i]=$solicitud['banco_codig'];
			        	if($solicitud['banco_codig']!=$ant){
			        		$msg=array('success'=>'false','msg'=>'Los archivos deben ser generados por Banco');
							echo json_encode($msg);
							exit();
			        	}	
					}
	        		
		        	
					$ant=$solicitud['banco_codig'];
		        	$i++;


				}


				$this->funciones->generarCsvConfirmacion($conexion,$id,$destino);
				
					
					
				if(file_exists($destino)) 
			    {
			    	$sql="INSERT INTO solicitud_tramitacion( trami_ruta, ttram_codig, testa_codig, usuar_codig, trami_fcrea, trami_hcrea) VALUES ('".$destino."','2','1','".$usuar."','".$fecha."','".$hora."')";
			    	$query=$sql;
			        $res1=$conexion->createCommand($sql)->execute();

			        if($res1){

			        	$sql="SELECT max(trami_codig) trami_codig FROM solicitud_tramitacion a";
			        	$tramitacion=$conexion->createCommand($sql)->queryRow();

			        	$traza=$this->funciones->guardarTraza($conexion, 'I', 'solicitud_tramitacion', '',json_encode($tramitacion),$query);

			        	foreach ($id as $key => $value) {

			        		$sql="SELECT * FROM solicitud WHERE solic_codig='".$value."'";
				        	$solicitud=$conexion->createCommand($sql)->queryRow();

				        	$sql="INSERT INTO solicitud_solicitud_tramitacion(solic_codig, trami_codig, usuar_codig, sotra_fcrea, sotra_hcrea) 
				        		VALUES ('".$solicitud['solic_codig']."','".$tramitacion['trami_codig']."','".$usuar."','".$fecha."','".$hora."')";
				        	$query=$sql;
				        	$res1=$conexion->createCommand($sql)->execute();
				        	if(!$res1){
				        		$transaction->rollBack();
			        			$msg=array('success'=>'false','msg'=>'No se pudo guardar el archivo');
			        			echo json_encode($msg);
			        			exit();
				        	}

				        	$sql="SELECT max(sotra_codig) sotra_codig, a.* FROM solicitud_solicitud_tramitacion a";
				        	$solicitud_tramitacion=$conexion->createCommand($sql)->queryRow();

				        	$traza=$this->funciones->guardarTraza($conexion, 'I', 'solicitud_solicitud_tramitacion', '',json_encode($solicitud_tramitacion),$query);


				        	$sql="UPDATE solicitud
				        		  SET estat_codig='".$estat."'
				        		  WHERE solic_codig='".$solicitud['solic_codig']."'";
				        	$query=$sql;

				        	$res1=$conexion->createCommand($sql)->execute();

				        	$sql="SELECT * FROM solicitud WHERE solic_codig='".$value."'";
				        	$solicitud2=$conexion->createCommand($sql)->queryRow();

				        	$traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud', json_encode($solicitud), json_encode($solicitud2),$query);

				        	
						$notificacion=$this->funciones->GuardarNotificacion($solicitud['solic_codig'], $estat);
						
						$trayectoria=$this->funciones->guardarTrayectoriaSolicitudes($conexion, $solicitud['solic_codig'], $estat, $solicitud['estat_codig'], 2);

				        }

				        

				        $transaction->commit();
				        $msg=array('success'=>'true','msg'=>'Archivo generado correctamente','trami'=>$tramitacion['trami_codig']);
			        }else{
			        	$transaction->rollBack();
			        	$msg=array('success'=>'false','msg'=>'No se pudo guardar el archivo');
			        }
			        
			        
			    }
			    else
			    {
			    	$transaction->rollBack();
			        $msg=array('success'=>'false','msg'=>'No se pudo crear el archivo');
			    }
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$this->render('generarCsv');
		}
	}

	public function actionReversarCsv()
	{
		if($_POST){
			$id=$_POST['id'];
			if(!$id){
				$msg=array('success'=>'false','msg'=>'Debe seleccionar al menos una solicitud');
				echo json_encode($msg);
				exit();
			}
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$estat=12;
			$ruta="files/tramitacion/certificacion/";
			$nombre=date('Ymd_His').'.csv';
			$destino=$ruta.$nombre;


			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				foreach ($id as $key => $value) {
					/*$sql="SELECT * FROM solicitud_tramitacion WHERE trami_codig = '".$value."' ";
					$tramitacion=$conexion->createCommand($sql)->queryRow();
					if($tramitacion){
						$sql="UPDATE solicitud_tramitacion 
				    		SET testa_codig=2
				    		WHERE trami_codig='".$value."'";
				    	$query=$sql;
				   		$res1=$conexion->createCommand($sql)->execute();

				    	$sql="SELECT * FROM solicitud_tramitacion WHERE trami_codig = '".$value."' ";
						$tramitacion2=$conexion->createCommand($sql)->queryRow();
			        	$traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud_tramitacion', json_encode($tramitacion),json_encode($tramitacion2),$query);
			        	
			        	$sql="SELECT * FROM solicitud_solicitud_tramitacion 
			        			WHERE trami_codig = '".$value."' ";
						$solicitud_tramitacion=$conexion->createCommand($sql)->queryAll();
						foreach ($solicitud_tramitacion as $k => $row) {*/
							$sql="SELECT * 
								  FROM solicitud 
								  WHERE solic_codig='".$value."'";
							$solicitud=$conexion->createCommand($sql)->queryRow();

							if($solicitud){
								$sql="UPDATE solicitud 
						    		SET estat_codig='".$estat."'
						    		WHERE solic_codig='".$value."'";
						    	$query=$sql;
						   		$res1=$conexion->createCommand($sql)->execute();

						    	$sql="SELECT * 
								  FROM solicitud 
								  WHERE solic_codig='".$value."'";
								$solicitud2=$conexion->createCommand($sql)->queryRow();
					        	$traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud', json_encode($solicitud),json_encode($solicitud2),$query);

					        	
						$notificacion=$this->funciones->GuardarNotificacion($solicitud['solic_codig'], $estat);
						
						$trayectoria=$this->funciones->guardarTrayectoriaSolicitudes($conexion, $solicitud['solic_codig'], $estat, $solicitud['estat_codig'], 2);	
							}else{
								$transaction->rollBack();
			        			$msg=array('success'=>'false','msg'=>'No se encontro la solicitud');
			        			echo json_encode($msg);
			        			exit();
							}
						/*}

					}else{
		        		$transaction->rollBack();
	        			$msg=array('success'=>'false','msg'=>'No se encontro el archivo');
	        			echo json_encode($msg);
	        			exit();
					}*/
				}
				$transaction->commit();
				$msg=array('success'=>'true','msg'=>'Archivo reversado correctamente');			    
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$this->render('reversarCsv');
		}
	}

	public function actionAprobarSolicitud()
	{
		if($_POST){
			$id=$_POST['id'];
			if(!$id){
				$msg=array('success'=>'false','msg'=>'Debe seleccionar al menos una solicitud');
				echo json_encode($msg);
				exit();
			}
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$estat=14;

			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				foreach ($id as $key => $value) {
					$sql="SELECT * FROM solicitud WHERE solic_codig = '".$value."' ";
					$solicitud=$conexion->createCommand($sql)->queryRow();
					if($solicitud){
						$sql="UPDATE solicitud 
				    		SET estat_codig='".$estat."'
				    		WHERE solic_codig='".$value."'";
				    	$query=$sql;
				   		$res1=$conexion->createCommand($sql)->execute();

				   		$sql="SELECT * FROM solicitud WHERE solic_codig = '".$value."' ";
						$solicitud2=$conexion->createCommand($sql)->queryRow();

				    	$traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud', json_encode($solicitud),json_encode($solicitud2),$query);
			        	
			        	
						$notificacion=$this->funciones->GuardarNotificacion($solicitud['solic_codig'], $estat);
						
						$trayectoria=$this->funciones->guardarTrayectoriaSolicitudes($conexion, $solicitud['solic_codig'], $estat, $solicitud['estat_codig'], 2);
			        	

			        	$sql="INSERT INTO solicitud_confirmacion_banco(solic_codig, banco_codig, accio_codig, bconf_obser, usuar_codig, bconf_fcrea, bconf_hcrea) 
							  VALUES ('".$solicitud['solic_codig']."',  '".$solicitud['banco_codig']."',  '1',  'APROBADA POR BANCO', '".$usuar."', '".$fecha."', '".$hora."')";
						$res2=$conexion->createCommand($sql)->execute();
						
						$sql="SELECT * FROM solicitud_confirmacion_banco
							  WHERE solic_codig='".$codig."'";
						
						$query=$sql;

						$certificacion_banco=$conexion->createCommand($sql)->queryRow();
						$traza=$this->funciones->guardarTraza($conexion, 'i', 'solicitud_certificacion_banco', json_encode(''),json_encode($certificacion_banco),$query);
					}else{
		        		$transaction->rollBack();
	        			$msg=array('success'=>'false','msg'=>'No se encontro la solicitud');
	        			echo json_encode($msg);
	        			exit();
					}
				}
				$transaction->commit();
				$msg=array('success'=>'true','msg'=>'Solicitudes Aprobadas correctamente');			    
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$this->render('aprobarSolicitud');
		}
	}
	public function actionReversarSolicitud()
	{
		if($_POST){
			$id=$_POST['id'];
			if(!$id){
				$msg=array('success'=>'false','msg'=>'Debe seleccionar al menos una solicitud');
				echo json_encode($msg);
				exit();
			}
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$estat=13;

			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				foreach ($id as $key => $value) {
					$sql="SELECT * FROM solicitud WHERE solic_codig = '".$value."' ";
					$solicitud=$conexion->createCommand($sql)->queryRow();
					if($solicitud){
						if($solicitud['estat_codig']=='14'){
							$sql="UPDATE solicitud 
					    		SET estat_codig='".$estat."'
					    		WHERE solic_codig='".$value."'";
					    	$query=$sql;
					   		$res1=$conexion->createCommand($sql)->execute();

					   		$sql="SELECT * FROM solicitud WHERE solic_codig = '".$value."' ";
							$solicitud2=$conexion->createCommand($sql)->queryRow();

					    	$traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud', json_encode($solicitud),json_encode($solicitud2),$query);
				        	
				        	
						$notificacion=$this->funciones->GuardarNotificacion($solicitud['solic_codig'], $estat);
						
						$trayectoria=$this->funciones->guardarTrayectoriaSolicitudes($conexion, $solicitud['solic_codig'], $estat, $solicitud['estat_codig'], 2);
			        	}else{
			        		$transaction->rollBack();
		        			$msg=array('success'=>'false','msg'=>'No se puede reversar la solicitud');
		        			echo json_encode($msg);
		        			exit();
			        	}
					}else{
		        		$transaction->rollBack();
	        			$msg=array('success'=>'false','msg'=>'No se encontro la solicitud');
	        			echo json_encode($msg);
	        			exit();
					}
				}
				$transaction->commit();
				$msg=array('success'=>'true','msg'=>'Solicitudes Reversadas correctamente');			    
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$this->render('reversarSolicitud');
		}
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}