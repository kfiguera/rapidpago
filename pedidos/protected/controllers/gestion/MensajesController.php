<?php

class MensajesController extends Controller
{
	public $funciones;

	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}

	public function actionIndex()
	{
		$this->redirect(Yii::app()->request->baseUrl.'/pedidos/pedidos/listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['numero']){
			$numero=mb_strtoupper($_POST['numero']);
			$condicion.="a.pedid_numer like '%".$numero."%' ";
			$con++;
		}
		if($_POST['epedi']){
			if($con>0){
				$condicion.="AND ";
			}
			$epedi=mb_strtoupper($_POST['epedi']);
			$condicion.="a.epedi_codig like '%".$epedi."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
			$c=$_GET['c'];
			
			$sql="SELECT * 
			  FROM solicitud_mensajes  a
			  WHERE a.mensa_codig ='".$_GET['c']."'";
			$solicitud=$conexion->createCommand($sql)->queryRow();
			$this->render('consultar', array('solicitud' => $solicitud,'ubicacion' => $ubicacion,'c' => $c,'s' => $s));
			
	}
	public function actionRegistrar()
	{
		if($_POST){
			//Datos de la Solicitud
			$codig=mb_strtoupper($_POST["codig"]);
			$mensa=base64_encode($_POST["mensa"]);
			$asunt=mb_strtoupper($_POST["asunt"]);
			$pesta=mb_strtoupper($_POST["pesta"]);
			$cuerpo=mb_strtoupper($_POST["cuerpo"]);
			//Datos de Auditoria
			$estat=1;
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();

			try{
				$sql="SELECT * FROM solicitud_mensajes WHERE mensa_asunt ='".$asunt."'";
				$mensaje=$conexion->createCommand($sql)->queryRow();
				if(!$mensaje){
					$sql="INSERT INTO solicitud_mensajes(mensa_asunt, mensa_cuerp, pesta_codig, usuar_codig, mensa_fcrea, mensa_hcrea) 
						VALUES ('".$asunt."','".$mensa."','".$pesta."','".$usuar."','".$fecha."','".$hora."')";
					$res1=$conexion->createCommand($sql)->execute();
					if($res1){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Mensaje guardado correctamente');
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al guardar El Mensaje');	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Mensaje ya existe');
				}

			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	
	public function actionModificar()
	{
		if($_POST){
			//Datos de la Solicitud
			$codig=mb_strtoupper($_POST["codig"]);
			$mensa=base64_encode($_POST["mensa"]);
			$asunt=mb_strtoupper($_POST["asunt"]);
			$pesta=mb_strtoupper($_POST["pesta"]);
			$cuerpo=mb_strtoupper($_POST["cuerpo"]);
			//Datos de Auditoria
			$estat=1;
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				
				$sql="SELECT * FROM solicitud_mensajes WHERE mensa_codig ='".$codig."'";
				$mensaje=$conexion->createCommand($sql)->queryRow();
				if($mensaje){
					$sql="UPDATE solicitud_mensajes
						  SET mensa_cuerp='".$mensa."',
							  pesta_codig='".$pesta."',
							  mensa_asunt='".$asunt."'
						  WHERE mensa_codig ='".$codig."'";
					$res1=$conexion->createCommand($sql)->execute();
					if($res1){
						$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Mensaje actualizado correctamente');
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al actualizar El Mensaje');	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Mensaje no existe');
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$c=$_GET['c'];
			
			$sql="SELECT * 
			  FROM solicitud_mensajes  a
			  WHERE a.mensa_codig ='".$_GET['c']."'";
			$solicitud=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('solicitud' => $solicitud,'ubicacion' => $ubicacion,'c' => $c,'s' => $s));
			
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			//Datos del Detalle
			$pedid=mb_strtoupper($_POST['pedid']);
			$codig=mb_strtoupper($_POST['codigo']);
			$pasos=mb_strtoupper($_POST['pasos']);
			//DATOS DE LOS DOCUMENTOS
			$obser=$_POST["obser"];
			//Datos de Auditoria
			$estat=30;
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			
			try{
				$sql="SELECT * FROM solicitud
					WHERE solic_codig='".$codig."';";
				$solicitud=$conexion->createCommand($sql)->queryRow();
				if($solicitud['estat_codig']<='11'){
					$estat=33;
				}else if($solicitud['estat_codig']<='22'){
					$estat=30;
				}else{
					$estat=30;
				}

				if($solicitud){
					$sql="SELECT * FROM solicitud_eliminacion
					WHERE solic_codig='".$codig."'
					  AND pesta_codig='1';";
					$anulacion=$conexion->createCommand($sql)->queryRow();
					if(!$anulacion){

						$sql="INSERT INTO solicitud_eliminacion(solic_codig, pesta_codig, selim_obser, usuar_codig, selim_fcrea, selim_hcrea) 
						    VALUES ('".$codig."', '1', '".$obser."', '".$usuar."', '".$fecha."', '".$hora."')";
						$query=$sql;
						$res=$conexion->createCommand($sql)->execute();

						$sql="SELECT * FROM solicitud_eliminacion
							WHERE solic_codig='".$codig."'
							  AND pesta_codig='1';";
						$anulacion2=$conexion->createCommand($sql)->queryRow();

						$traza=$this->funciones->guardarTraza($conexion, 'I', 'solicitud_eliminacion', json_encode($anulacion),json_encode($anulacion2),$query);

						$sql="UPDATE solicitud
							    SET estat_codig= '".$estat."'
							WHERE solic_codig = '".$codig."'";
						$query=$sql;
						$res2=$conexion->createCommand($sql)->execute();

						$sql="SELECT * FROM solicitud
								  WHERE solic_codig='".$codig."'";
							
						$solicitud2=$conexion->createCommand($sql)->queryRow();
						
						$traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud', json_encode($solicitud),json_encode($solicitud2),$query);

						
						$notificacion=$this->funciones->GuardarNotificacion($solicitud['solic_codig'], $estat);
						
						$trayectoria=$this->funciones->guardarTrayectoriaSolicitudes($conexion, $solicitud['solic_codig'], $estat, $solicitud['estat_codig'], 3);

						$transaction->commit();
						$msg= array('success'=>'true','msg'=>'Solicitud eliminada correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Ya la solicitud posee una anulación activa');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error no existe la Solicitud');	
				}
			}catch(Exception $e){
				echo $sql;
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);

		}else{
			$conexion=Yii::app()->db;
			$c=$_GET['c'];
			$s=1;
			$sql="SELECT * 
			  FROM solicitud  a
			  WHERE a.solic_codig ='".$_GET['c']."'";
			$solicitud=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar',array('solicitud' => $solicitud, 'c' => $c));
		}
	}
	public function actionEnviar()
	{
		if($_POST){

			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_pedidos WHERE pedid_codig ='".$codig."'";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				if($pedidos){
					if($pedidos['pedid_pasos']=='3'){
						$sql="SELECT * FROM pedido_pedidos_detalle  WHERE pedid_codig ='".$codig."'";
						$detalles=$conexion->createCommand($sql)->queryAll();
						if($detalles){

							foreach ($detalles as $key => $value) {
								$sql="SELECT * FROM pedido_pedidos_detalle_listado  WHERE pdeta_codig ='".$value['pdeta_codig']."'";
								$modelo=$conexion->createCommand($sql)->queryAll();
								if($modelo){
									if($value['pdeta_pasos']>='2'){
										$msg=array('success'=>'true','msg'=>' Detalle eliminado correctamente');
										$cmode++;
										$cpers+=count($modelo);	
									}else{
										$transaction->rollBack();
										$msg=array('success'=>'false','msg'=>'Termine todos los pasos de los modelos y el pedido para continuar');	
										echo json_encode($msg);
										exit();
									}
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Existe Un modelo sin p_personas asociadas verificar');	
									echo json_encode($msg);
									exit();
								}
							}
							if($msg['success']=='true'){
								$sql="UPDATE pedido_pedidos
									  SET pedid_total='".$cpers."',
										  pedid_cmode='".$cmode."',
										  epedi_codig='2'
									  WHERE pedid_codig ='".$codig."'";
								$res1=$conexion->createCommand($sql)->execute();

								if($res1){
									$transaction->commit();
									$msg=array('success'=>'true','msg'=>' Pedido enviado correctamente');	
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al enviar el Pedido');	
								}
							}
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'El pedido no tiene modelos asociados');
						}
					}else{
						$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Debe finalizar el pedido antes de enviarlo');
					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
				
			}catch(Exception $e){

				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$p=$_GET['p'];
			$c=$_GET['c'];
			$s=$_GET['s'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_pedidos  a
			  WHERE a.pedid_codig ='".$_GET['c']."'";
			$pedidos=$conexion->createCommand($sql)->queryRow();
			$this->render('enviar', array('pedidos' => $pedidos, 'ubicacion' => $ubicacion,'p' => $p,'c' => $c,'s' => $s));
		}
	}
	public function actionAprobar()
	{
		if($_POST){
			$codig=($_POST['codig']);
			$mcant=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['mcant']));
			$total=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['total']));
			$monto=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['monto']));
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_pedidos WHERE pedid_codig ='".$codig."'";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				if($pedidos){
					if($pedidos['epedi_codig']=='2'){
						$sql="UPDATE pedido_pedidos
							  SET pedid_total='".$total."',
								  pedid_cmode='".$mcant."',
								  pedid_monto='".$monto."',
								  epedi_codig='7'
							  WHERE pedid_codig ='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();

						if($res1){
							$sql="SELECT a.*, b.*
								  FROM p_persona a
								  JOIN seguridad_usuarios b ON (a.perso_codig=b.perso_codig)
								  WHERE b.usuar_codig='".$pedidos['usuar_codig']."'";
                    		$p_persona=$conexion->createCommand($sql)->queryRow();

                    		$sql="SELECT * FROM pedido_pedidos WHERE pedid_codig ='".$codig."'";
							$pedidos=$conexion->createCommand($sql)->queryRow();

							$datos['row']=$p_persona;
							$datos['totales']=$pedidos;
							$corre=$p_persona['usuar_login'];
                            $this->funciones->enviarCorreo('../pedidos/pedidos/aprobar_correo',$datos,'Polerones Tiempo | Pedido Aprobado',$corre);
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Pedido Aprobado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al enviar el Pedido');	
						}
						
					}else{
						$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Debe envar el pedido antes de Aprobarlo');
					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
				
			}catch(Exception $e){

				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_pedidos  a
			  WHERE a.pedid_codig ='".$_GET['c']."'";
			$pedidos=$conexion->createCommand($sql)->queryRow();
			$totales=$this->funciones->CalcularMontoPedido($pedidos);
			$this->render('aprobar', array('pedidos' => $pedidos, 'totales' => $totales, 'c'=>$c));
		}
	}
	public function actionRechazar()
	{
		if($_POST){
			$codig=($_POST['codig']);
			$mcant=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['mcant']));
			$total=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['total']));
			$monto=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['monto']));
			$obser=mb_strtoupper($_POST['obser']);
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_pedidos WHERE pedid_codig ='".$codig."'";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				if($pedidos){
					//if($pedidos['epedi_codig']=='2'){
						$sql="UPDATE pedido_pedidos
							  SET pedid_total='".$total."',
								  pedid_cmode='".$mcant."',
								  pedid_monto='".$monto."',
								  epedi_codig='1'
							  WHERE pedid_codig ='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();

						if($res1){
							$sql="SELECT a.*, b.*
								  FROM p_persona a
								  JOIN seguridad_usuarios b ON (a.perso_codig=b.perso_codig)
								  WHERE b.usuar_codig='".$pedidos['usuar_codig']."'";
                    		$p_persona=$conexion->createCommand($sql)->queryRow();

                    		$sql="SELECT * FROM pedido_pedidos WHERE pedid_codig ='".$codig."'";
							$pedidos=$conexion->createCommand($sql)->queryRow();

							$datos['row']=$p_persona;
							$datos['totales']=$pedidos;
							$datos['obser']=$obser;
							$corre=$p_persona['usuar_login'];
                            //$this->funciones->enviarCorreo('../pedidos/pedidos/rechazar_correo',$datos,'Polerones Tiempo | Pedido Rechazado',$corre);
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Pedido Rechazado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al enviar el Pedido');	
						}
						
					/*}else{
						$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Debe envar el pedido antes de Rechazarlo');
					}*/
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
				
			}catch(Exception $e){

				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_pedidos  a
			  WHERE a.pedid_codig ='".$_GET['c']."'";
			$pedidos=$conexion->createCommand($sql)->queryRow();
			$totales=$this->funciones->CalcularMontoPedido($pedidos);
			$this->render('rechazar', array('pedidos' => $pedidos, 'totales' => $totales, 'c'=>$c));
		}
	}


	public function actionCostos()
	{
		if($_POST){
			$codig=($_POST['codig']);
			$costo=mb_strtoupper($_POST['costo']);
			$obser=mb_strtoupper($_POST['obser']);
			$monto=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['monto']));
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_pedidos WHERE pedid_codig ='".$codig."'";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				if($pedidos){
					if($pedidos['epedi_codig']=='2'){
						$sql="INSERT INTO pedido_pedidos_adicional(pedid_codig, costo_codig, adici_monto, adici_obser, usuar_codig, adici_fcrea, adici_hcrea) 
							VALUES ('".$codig."','".$costo."','".$monto."','".$obser."','".$usuar."','".$fecha."','".$hora."');";
						$res1=$conexion->createCommand($sql)->execute();

						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Costo Adicional agregado Correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al agregar el Costo');	
						}
						
					}else{
						$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Debe envar el pedido antes de asignar un costo adicional');
					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
				
			}catch(Exception $e){

				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$p=$_GET['p'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_pedidos  a
			  WHERE a.pedid_codig ='".$_GET['c']."'";
			$pedidos=$conexion->createCommand($sql)->queryRow();
			$totales=$this->funciones->CalcularMontoPedido($pedidos);
			$this->render('costos', array('pedidos' => $pedidos, 'totales' => $totales, 'p'=>$p, 'c'=>$c));
		}
	}
	public function actionCostosEliminar()
	{
		if($_POST){
			$codig=($_POST['codig']);
			$costo=mb_strtoupper($_POST['costo']);
			$obser=mb_strtoupper($_POST['obser']);
			$monto=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['monto']));
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_pedidos_adicional WHERE adici_codig ='".$codig."'";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				if($pedidos){

					$sql="DELETE FROM pedido_pedidos_adicional WHERE adici_codig ='".$codig."'";
					$res1=$conexion->createCommand($sql)->execute();

					if($res1){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>' Costo Adicional eliminado Correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al eliminado el Costo');	
					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El adicional no existe');
				}
				
			}catch(Exception $e){

				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$p=$_GET['p'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_pedidos_adicional  a
			  WHERE a.adici_codig ='".$_GET['c']."'";
			$costos=$conexion->createCommand($sql)->queryRow();
			$totales=$this->funciones->CalcularMontoPedido($pedidos);
			$this->render('costosEliminar', array('costos' => $costos, 'totales' => $totales, 'c'=>$c, 'p'=>$p));
		}
	}

	public function actionDeduccion()
	{
		if($_POST){
			$codig=($_POST['codig']);
			$deduc=mb_strtoupper($_POST['deduc']);
			$obser=mb_strtoupper($_POST['obser']);
			$monto=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['monto']));
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_pedidos WHERE pedid_codig ='".$codig."'";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				if($pedidos){
					$sql="SELECT * FROM pedido_pedidos_deduccion WHERE deduc_codig='".$deduc."' and pedid_codig = '".$codig."'";
					$deduciones=$conexion->createCommand($sql)->queryRow();
					if(!$deduciones){
						if($pedidos['epedi_codig']=='2'){
							$sql="INSERT INTO pedido_pedidos_deduccion(pedid_codig, deduc_codig, pdedu_monto, pdedu_obser, usuar_codig, pdedu_fcrea, pdedu_hcrea) 
								VALUES ('".$codig."','".$deduc."','".$monto."','".$obser."','".$usuar."','".$fecha."','".$hora."');";
							$res1=$conexion->createCommand($sql)->execute();

							if($res1){
								$transaction->commit();
								$msg=array('success'=>'true','msg'=>' Deducción agregado Correctamente');	
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al agregar la Deducción');	
							}
							
						}else{
							$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Debe envar el pedido antes de asignar una Deducción');
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Ya se agrego la deducción');
					}
						
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
				
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$p=$_GET['p'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_pedidos  a
			  WHERE a.pedid_codig ='".$_GET['c']."'";
			$pedidos=$conexion->createCommand($sql)->queryRow();
			$totales=$this->funciones->CalcularMontoPedido($pedidos);
			$this->render('deduccion', array('pedidos' => $pedidos, 'totales' => $totales, 'c'=>$c, 'p'=>$p));
		}
	}
	public function actionDeduccionEliminar()
	{
		if($_POST){
			$codig=($_POST['codig']);
			$deduc=mb_strtoupper($_POST['deduc']);
			$obser=mb_strtoupper($_POST['obser']);
			$monto=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['monto']));
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_pedidos_deduccion WHERE pdedu_codig ='".$codig."'";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				if($pedidos){
						
					$sql="DELETE FROM pedido_pedidos_deduccion WHERE pdedu_codig ='".$codig."'";
					$res1=$conexion->createCommand($sql)->execute();

					if($res1){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>' Deducción eliminado Correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al eliminado la Deducción');	
					}
							
						
						
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
				
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$p=$_GET['p'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_pedidos_deduccion  a
			  WHERE a.pdedu_codig ='".$_GET['c']."'";
			$deduccion=$conexion->createCommand($sql)->queryRow();
			$totales=$this->funciones->CalcularMontoPedido($pedidos);
			$this->render('deduccionEliminar', array('deduccion' => $deduccion, 'totales' => $totales, 'c'=>$c, 'p'=>$p));
		}
	}
	public function actionVerificar()
	{
		$this->render('verificar');
	}
	public function actionAnular()
	{
		$this->render('anular');
	}
	public function actionTerminal()
	{
		if($_POST){
			//Datos del Detalle
			$pedid=mb_strtoupper($_POST['pedid']);
			$codig=mb_strtoupper($_POST['codig']);
			$pasos=mb_strtoupper($_POST['pasos']);
			//DATOS DE LOS DOCUMENTOS
			$numer=$_POST["numer"];
			//Datos de Auditoria
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			$ant='';
			$pre='';
			$dispo=$numer;
			asort($dispo);
			$canti=0;
			foreach ($dispo as $key => $value) {
				if($value==$ant){
					$msg=array('success'=>'false','msg'=>'No puede incluir dos numeros de terminar iguales');
					echo json_encode($msg);
					exit();
				}
				$ant=$value;
				$canti++;
			}


			try{
				$sql="SELECT * FROM solicitud
					WHERE solic_codig='".$codig."';";
				$solicitud=$conexion->createCommand($sql)->queryRow();

				if($solicitud){

					$sql="SELECT SUM(cequi_canti) cantidad, a.* FROM solicitud_equipos a WHERE solic_codig = '".$codig."'";
					$equipos=$conexion->createCommand($sql)->queryRow();	

					if($equipos['cantidad']==$canti){

						$a=0;

						$sql="DELETE FROM solicitud_terminal
							  WHERE solic_codig ='".$codig."'";
						$result=$conexion->createCommand($sql)->execute();
						foreach ($numer as $key => $value) {
							
							

							$sql="INSERT INTO solicitud_terminal(solic_codig, termi_numer, usuar_codig, termi_fcrea, termi_hcrea) 
							VALUES ('".$codig."', '".$value."', '".$usuar."', '".$fecha."', '".$hora."')";
							$res2=$conexion->createCommand($sql)->execute();

							if(!$res2){
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'No se pudo guardar el numero de terminal');
								echo json_encode($msg);
								exit();
							}
						}
						
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Solicitud actualizada correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'La cantidad de dispositivos debe ser igual a la Solicitud');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error no existe la Solicitud');	
				}
			}catch(Exception $e){
				echo $sql;
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);

		}else{
			$conexion=Yii::app()->db;
			$c=$_GET['c'];
			$s=1;
			$sql="SELECT * 
			  FROM solicitud  a
			  WHERE a.solic_codig ='".$_GET['c']."'";
			$solicitud=$conexion->createCommand($sql)->queryRow();
			$this->render('terminal',array('solicitud' => $solicitud, 'c' => $c));
		}
	}
	public function actionGenerarCsv()
	{
		if($_POST){
			
			$id=$_POST['id'];
			if(!$id){
				$msg=array('success'=>'false','msg'=>'Debe seleccionar al menos una solicitud');
				echo json_encode($msg);
				exit();
			}
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$estat=7;
			$ruta="files/tramitacion/certificacion/";
			$nombre=date('Ymd_His').'.csv';
			$destino=$ruta.$nombre;


			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{

				$this->funciones->generarCsvCertificacion($conexion,$id,$destino);
				
					
					
				if(file_exists($destino)) 
			    {
			    	$sql="INSERT INTO solicitud_tramitacion( trami_ruta, ttram_codig, testa_codig, usuar_codig, trami_fcrea, trami_hcrea) VALUES ('".$destino."','1','1','".$usuar."','".$fecha."','".$hora."')";
			    	$query=$sql;
			        $res1=$conexion->createCommand($sql)->execute();

			        if($res1){

			        	$sql="SELECT max(trami_codig) trami_codig FROM solicitud_tramitacion a";
			        	$tramitacion=$conexion->createCommand($sql)->queryRow();

			        	$traza=$this->funciones->guardarTraza($conexion, 'I', 'solicitud_tramitacion', '',json_encode($tramitacion),$query);

			        	foreach ($id as $key => $value) {

			        		$sql="SELECT * FROM solicitud WHERE solic_codig='".$value."'";
				        	$solicitud=$conexion->createCommand($sql)->queryRow();

				        	$sql="INSERT INTO solicitud_solicitud_tramitacion(solic_codig, trami_codig, usuar_codig, sotra_fcrea, sotra_hcrea) 
				        		VALUES ('".$solicitud['solic_codig']."','".$tramitacion['trami_codig']."','".$usuar."','".$fecha."','".$hora."')";
				        	$query=$sql;
				        	$res1=$conexion->createCommand($sql)->execute();
				        	if(!$res1){
				        		$transaction->rollBack();
			        			$msg=array('success'=>'false','msg'=>'No se pudo guardar el archivo');
			        			echo json_encode($msg);
			        			exit();
				        	}

				        	$sql="SELECT max(sotra_codig) sotra_codig, a.* FROM solicitud_solicitud_tramitacion a";
				        	$solicitud_tramitacion=$conexion->createCommand($sql)->queryRow();

				        	$traza=$this->funciones->guardarTraza($conexion, 'I', 'solicitud_solicitud_tramitacion', '',json_encode($solicitud_tramitacion),$query);


				        	$sql="UPDATE solicitud
				        		  SET estat_codig='7'
				        		  WHERE solic_codig='".$solicitud['solic_codig']."'";
				        	$query=$sql;

				        	$res1=$conexion->createCommand($sql)->execute();

				        	$sql="SELECT * FROM solicitud WHERE solic_codig='".$value."'";
				        	$solicitud2=$conexion->createCommand($sql)->queryRow();

				        	$traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud', json_encode($solicitud), json_encode($solicitud2),$query);

				        	
						$notificacion=$this->funciones->GuardarNotificacion($solicitud['solic_codig'], $estat);
						
						$trayectoria=$this->funciones->guardarTrayectoriaSolicitudes($conexion, $solicitud['solic_codig'], $estat, $solicitud['estat_codig'], 2);

				        }

				        

				        $transaction->commit();
				        $msg=array('success'=>'true','msg'=>'Archivo generado correctamente','destino'=>$destino);
			        }else{
			        	$transaction->rollBack();
			        	$msg=array('success'=>'false','msg'=>'No se pudo guardar el archivo');
			        }
			        
			        
			    }
			    else
			    {
			    	$transaction->rollBack();
			        $msg=array('success'=>'false','msg'=>'No se pudo crear el archivo');
			    }
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$this->render('generarCsv');
		}
	}

	public function actionReversarCsv()
	{
		if($_POST){
			$id=$_POST['id'];
			if(!$id){
				$msg=array('success'=>'false','msg'=>'Debe seleccionar al menos una solicitud');
				echo json_encode($msg);
				exit();
			}
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$estat=6;
			$ruta="files/tramitacion/certificacion/";
			$nombre=date('Ymd_His').'.csv';
			$destino=$ruta.$nombre;


			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				foreach ($id as $key => $value) {
					$sql="SELECT * FROM solicitud_tramitacion WHERE trami_codig = '".$value."' ";
					$tramitacion=$conexion->createCommand($sql)->queryRow();
					if($tramitacion){
						$sql="UPDATE solicitud_tramitacion 
				    		SET testa_codig=2
				    		WHERE trami_codig='".$value."'";
				    	$query=$sql;
				   		$res1=$conexion->createCommand($sql)->execute();

				    	$sql="SELECT * FROM solicitud_tramitacion WHERE trami_codig = '".$value."' ";
						$tramitacion2=$conexion->createCommand($sql)->queryRow();
			        	$traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud_tramitacion', json_encode($tramitacion),json_encode($tramitacion2),$query);
			        	
			        	$sql="SELECT * FROM solicitud_solicitud_tramitacion 
			        			WHERE trami_codig = '".$value."' ";
						$solicitud_tramitacion=$conexion->createCommand($sql)->queryAll();
						foreach ($solicitud_tramitacion as $k => $row) {
							$sql="SELECT * 
								  FROM solicitud 
								  WHERE solic_codig='".$row['solic_codig']."'";
							$solicitud=$conexion->createCommand($sql)->queryRow();

							if($solicitud){
								$sql="UPDATE solicitud 
						    		SET estat_codig=6
						    		WHERE solic_codig='".$row['solic_codig']."'";
						    	$query=$sql;
						   		$res1=$conexion->createCommand($sql)->execute();

						    	$sql="SELECT * 
								  FROM solicitud 
								  WHERE solic_codig='".$row['solic_codig']."'";
								$solicitud2=$conexion->createCommand($sql)->queryRow();
					        	$traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud', json_encode($solicitud),json_encode($solicitud2),$query);

					        	
						$notificacion=$this->funciones->GuardarNotificacion($solicitud['solic_codig'], $estat);
						
						$trayectoria=$this->funciones->guardarTrayectoriaSolicitudes($conexion, $solicitud['solic_codig'], 6, $solicitud['estat_codig'], 2);	
							}else{
								$transaction->rollBack();
			        			$msg=array('success'=>'false','msg'=>'No se encontro la solicitud');
			        			echo json_encode($msg);
			        			exit();
							}
						}

					}else{
		        		$transaction->rollBack();
	        			$msg=array('success'=>'false','msg'=>'No se encontro el archivo');
	        			echo json_encode($msg);
	        			exit();
					}
				}
				$transaction->commit();
				$msg=array('success'=>'true','msg'=>'Archivo reversado correctamente');			    
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$this->render('reversarCsv');
		}
	}

	public function actionAprobarSolicitud()
	{
		if($_POST){
			$id=$_POST['id'];
			if(!$id){
				$msg=array('success'=>'false','msg'=>'Debe seleccionar al menos una solicitud');
				echo json_encode($msg);
				exit();
			}
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$estat=8;

			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				foreach ($id as $key => $value) {
					$sql="SELECT * FROM solicitud WHERE solic_codig = '".$value."' ";
					$solicitud=$conexion->createCommand($sql)->queryRow();
					if($solicitud){
						$sql="SELECT * FROM solicitud_terminal WHERE solic_codig = '".$value."' ";
						$terminal=$conexion->createCommand($sql)->queryRow();
						if($terminal){
							$sql="UPDATE solicitud 
					    		SET estat_codig='".$estat."'
					    		WHERE solic_codig='".$value."'";
					    	$query=$sql;
					   		$res1=$conexion->createCommand($sql)->execute();

					   		$sql="SELECT * FROM solicitud WHERE solic_codig = '".$value."' ";
							$solicitud2=$conexion->createCommand($sql)->queryRow();

					    	$traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud', json_encode($solicitud),json_encode($solicitud2),$query);
				        	
				        	
						$notificacion=$this->funciones->GuardarNotificacion($solicitud['solic_codig'], $estat);
						
						$trayectoria=$this->funciones->guardarTrayectoriaSolicitudes($conexion, $solicitud['solic_codig'], 8, $solicitud['estat_codig'], 2);
				        }else{
			        		$transaction->rollBack();
		        			$msg=array('success'=>'false','msg'=>'Debe ingresar el número de Terminal');
		        			echo json_encode($msg);
		        			exit();
						}
			        	
					}else{
		        		$transaction->rollBack();
	        			$msg=array('success'=>'false','msg'=>'No se encontro la solicitud');
	        			echo json_encode($msg);
	        			exit();
					}
				}
				$transaction->commit();
				$msg=array('success'=>'true','msg'=>'Solicitudes Aprobadas correctamente');			    
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$this->render('aprobarSolicitud');
		}
	}
	public function actionReversarEliminacion()
	{
		if($_POST){
			
			$id=$_POST['id'];
			if(!$id){
				$msg=array('success'=>'false','msg'=>'Debe seleccionar al menos una solicitud');
				echo json_encode($msg);
				exit();
			}
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$estat=8;
			$ruta="files/tramitacion/certificacion/";
			$nombre=date('Ymd_His').'.csv';
			$destino=$ruta.$nombre;


			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{

				  	

		        	foreach ($id as $key => $value) {

		        		$sql="SELECT * FROM solicitud_eliminacion WHERE solic_codig='".$value."' and pesta_codig='1'";
			        	$solicitud=$conexion->createCommand($sql)->queryRow();
			        	$sql="UPDATE solicitud_eliminacion 
			        		  SET pesta_codig ='2'
			        		  WHERE selim_codig = '".$solicitud['selim_codig']."'";
			        	$query=$sql;
			        	$res1=$conexion->createCommand($sql)->execute();
			        	$sql="SELECT * FROM solicitud_eliminacion WHERE selim_codig= '".$solicitud['selim_codig']."'";
			        	$solicitud2=$conexion->createCommand($sql)->queryRow();



			        	$traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud_eliminacion', json_encode($solicitud2),json_encode($solicitud2),$query);

			        	
			        	$sql="SELECT * FROM solicitud WHERE solic_codig='".$value."'";

			        	$solicitud=$conexion->createCommand($sql)->queryRow();

			        	$sql="UPDATE solicitud
			        		  SET estat_codig='".$estat."'
			        		  WHERE solic_codig='".$solicitud['solic_codig']."'";
			        	$query=$sql;

			        	$res1=$conexion->createCommand($sql)->execute();

			        	$sql="SELECT * FROM solicitud WHERE solic_codig='".$value."'";
			        	$solicitud2=$conexion->createCommand($sql)->queryRow();

			        	$traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud', json_encode($solicitud), json_encode($solicitud2),$query);

			        	
						$notificacion=$this->funciones->GuardarNotificacion($solicitud['solic_codig'], $estat);
						
						$trayectoria=$this->funciones->guardarTrayectoriaSolicitudes($conexion, $solicitud['solic_codig'], $estat, $solicitud['solic_codig'], 2);

			        }

			        

			        $transaction->commit();
			        $msg=array('success'=>'true','msg'=>'Eliminación reversada correctamente','destino'=>$destino);
			        
			        
			        
			    
			   
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$this->render('reversarEliminacion');
		}
	}
	public function actionReversarSolicitud()
	{
		if($_POST){
			$id=$_POST['id'];
			if(!$id){
				$msg=array('success'=>'false','msg'=>'Debe seleccionar al menos una solicitud');
				echo json_encode($msg);
				exit();
			}
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$estat=7;

			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				foreach ($id as $key => $value) {
					$sql="SELECT * FROM solicitud WHERE solic_codig = '".$value."' ";
					$solicitud=$conexion->createCommand($sql)->queryRow();
					if($solicitud){
						if($solicitud['estat_codig']=='8'){
							$sql="UPDATE solicitud 
					    		SET estat_codig='".$estat."'
					    		WHERE solic_codig='".$value."'";
					    	$query=$sql;
					   		$res1=$conexion->createCommand($sql)->execute();

					   		$sql="SELECT * FROM solicitud WHERE solic_codig = '".$value."' ";
							$solicitud2=$conexion->createCommand($sql)->queryRow();

					    	$traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud', json_encode($solicitud),json_encode($solicitud2),$query);
				        	
				        	
						$notificacion=$this->funciones->GuardarNotificacion($solicitud['solic_codig'], $estat);
						
						$trayectoria=$this->funciones->guardarTrayectoriaSolicitudes($conexion, $solicitud['solic_codig'], 7, $solicitud['estat_codig'], 2);
			        	}else{
			        		$transaction->rollBack();
		        			$msg=array('success'=>'false','msg'=>'No se puede reversar la solicitud');
		        			echo json_encode($msg);
		        			exit();
			        	}
					}else{
		        		$transaction->rollBack();
	        			$msg=array('success'=>'false','msg'=>'No se encontro la solicitud');
	        			echo json_encode($msg);
	        			exit();
					}
				}
				$transaction->commit();
				$msg=array('success'=>'true','msg'=>'Solicitudes Reversadas correctamente');			    
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$this->render('reversarSolicitud');
		}
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}