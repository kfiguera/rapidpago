<?php

class GrafanaController extends Controller
{
	public $funciones;

	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}

	public function actionIndex()
	{

		$this->redirect(Yii::app()->request->baseUrl.'/pedidos/pedidos/listado');
	}

	public function actionGrafana()
	{
		$this->layout = 'main4';
		$this->render('grafana');
	}
	
}