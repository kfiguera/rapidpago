<?php

class TarifadorController extends Controller
{
	public function actionlista()
	{
		
		if($_POST){
			
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="CALL sp_admin_tarifador('".$_POST['tarif_codigs']."'
				, '".$_POST['tarif_kiloms']."'
				, '".$_POST['tarif_diurns']."'
				, '".$_POST['tarif_diudes']."'
				, '".$_POST['tarif_noctus']."'
				, '".$_POST['tarif_nocdes']."'
				, '".$_POST['tarif_finses']."'
				, '".$_POST['tarif_ferias']."'
				, '".$_POST['tarif_params']."'
				, '".$_POST['tarif_pasams']."'
				, '".$_POST['tarif_malets']."'
				, '".$_POST['tarif_mascos']."'
				, '".$_POST['tarif_espers']."'
				, '1'
				, '".$_POST['tgene_status']."'
				, '".$_REQUEST['o']."'
				, '".$_POST['tarif_encoms']."')";
				//echo $sql;
				//exit;
				$result=$conexion->createCommand($sql)->queryRow();
				$transaction->commit();
				$msg=array('success'=>$result['success'],'msg'=>$result['mensaje']);
			}catch (CDbException $e){
				//var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al Ingresar los Datos');
			}
			echo json_encode($msg);
		}else if($_GET){
			$conexion=Yii::app()->db;
			
			
			if($_GET['o']=='U'){
				$sql="SELECT * FROM admin_tarifador where tarif_codig =".$_REQUEST['c'];
				$result = $conexion->createCommand($sql)->queryRow();
				$param['tarif_codigs']=$result['tarif_codig'];
				$param['operacion']   =$_GET['o'];
				$param['tarif_kiloms']=$result['tarif_kilom'];
				$param['tarif_diurns']=$result['tarif_diurn'];
				$param['tarif_diudes']=$result['tarif_diude'];
				$param['tarif_noctus']=$result['tarif_noctu'];
				$param['tarif_nocdes']=$result['tarif_nocde'];
				$param['tarif_finses']=$result['tarif_finse'];
				$param['tarif_ferias']=$result['tarif_feria'];
				$param['tarif_params']=$result['tarif_param'];
				$param['tarif_pasams']=$result['tarif_pasam'];
				$param['tarif_malets']=$result['tarif_malet'];
				$param['tarif_mascos']=$result['tarif_masco'];
				$param['tarif_espers']=$result['tarif_esper'];
				$param['tarif_encoms']=$result['tarif_encom'];
				$param['tgene_status']=$result['tgene_statu'];
			}
			//var_dump($param);
			$this->render('tarifador',array('param' => $param ));
					
			
		}else{
			$this->render('lista');
		}
	
	}
public function actionListaEliminar()
	{
		if($_POST){
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
							
				$sql="CALL sp_admin_tarifador('".$_POST['c']."','','','','','','','','','','','','','','','D','')";
				//echo $sql;
				//exit;
				$result=$conexion->createCommand($sql)->queryRow();
				//var_dump($result);
				$transaction->commit();
				$msg=array('success'=>$result['success'],'msg'=>$result['mensaje']);
			}catch (CDbException $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'true','msg'=>'Error al Eliminar la Tarifa');
			}
			echo json_encode($msg);
		}else if($_GET){
			$sql="SELECT * FROM admin_tarifador ";
			$conexion=yii::app()->db;
            $result=$conexion->createCommand($sql)->queryRow();
            
            $this->render('eliminar_lista',array('model'=>$result,'c'=>$_GET['c']));
			
			//$this->render('eliminar',array('model'=>$result));
		}else{
			$this->render('lista',array('model'=>$result));
		}
	}

public function actionModal2() {
        ?>
        <form method='post' id='modaleliminar'>
            <input type="hidden" id='c' name='c' value='<?php echo $_REQUEST['c'] ?>'> 
            <input type="hidden" id='t' name='t' value='<?php echo $_REQUEST['t'] ?>'>            
            ¿Está seguro de querer eliminar el <?php echo $_REQUEST['titulo'] ?> ?
            <br>
            <br>
            <button id='enviar' aprobar="true" estatus="2" class="btn btn-block btn-danger" type="button" archivo='<?php echo $nombre ?>' ><i class="fa fa-check"></i>Continuar</button>

        </form>
        <script>
            $('#enviar').click(function (e) {
                var formData = new FormData($("#modaleliminar")[0]);
                var c = document.getElementById("c").value;
                var t = document.getElementById("t").value;
                $.ajax({
                    dataType: "json",
                    'data': formData,
                    url: 'ListaEliminar',
                    type: 'post',
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $("#resultado").html("Procesando, espere por favor...");
                    },
                    success: function (response) {
                        //alert(response['success']);
                        if (response['success'] == 'true') {
                            $('#cerrar').click();
                            bootbox.dialog({
                                message: response['msg'],
                                title: "Exito!",
                                buttons: {
                                    danger: {
                                        label: "Cerrar",
                                        className: "btn-uven",
                                        callback: function(){
                                        	window.open('lista', '_parent');
                                        },
                                    },
                                }
                            });
                            $('.close').click( function(){
                            	window.open('lista', '_parent');
                            });
                        } else {

                            bootbox.dialog({
                                message: response['msg'],
                                title: "Información!",
                                buttons: {
                                    danger: {
                                        label: "Cerrar",
                                        className: "btn-uven",
                                    },
                                }
                            });
                           
                        }

                    }
                });
            });

        </script>

        <?php
    }

}