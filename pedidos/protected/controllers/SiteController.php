<?php

class SiteController extends Controller {

    /**
     * Declares class-based actions.
     */
    public $funciones;
    public function init()
    {
        $fun=Yii::app()->createController('funciones');
        $this->funciones=$fun[0];

    }
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }
    public function actionIndex() {
        if (Yii::app()->user->isGuest) {
            $this->redirect(Yii::app()->request->baseUrl.'/site/login');
        }else{
            if(Yii::app()->user->id['usuario']['permiso']==16){
                $this->render('vistas/banco');       
            }else{
                $this->render('vistas/inicio');       
            }
        }
    }
    public function actionInicio() {
        if (Yii::app()->user->isGuest) {
            $this->redirect(Yii::app()->request->baseUrl.'/site/login');
        }else{
            $verificar=$this->funciones->VerificarSesion();

            if(!$verificar){
                $this->redirect(Yii::app()->request->baseUrl.'/site/logout');
            }else{
                if(Yii::app()->user->id['usuario']['permiso']==16){
                    $this->render('vistas/banco');       
                }else{
                    $this->render('vistas/inicio');       
                }
            }
        }
    }
    public function actionBanco() {
        if (Yii::app()->user->isGuest) {
            $this->redirect(Yii::app()->request->baseUrl.'/site/login');
        }else{
            $verificar=$this->funciones->VerificarSesion();

            if(!$verificar){
                $this->redirect(Yii::app()->request->baseUrl.'/site/logout');
            }else{
                $this->render('vistas/banco');
            }
        }
    }

    public function actionEjecutiva() {
        if (Yii::app()->user->isGuest) {
            $this->redirect(Yii::app()->request->baseUrl.'/site/login');
        }else{
            $verificar=$this->funciones->VerificarSesion();

            if(!$verificar){
                $this->redirect(Yii::app()->request->baseUrl.'/site/logout');
            }else{
                $this->render('vistas/ejecutiva');
            }
        }
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        
        $error = Yii::app()->errorHandler->error;
        $this->render('error', $error);

    }

    /**
     * Displays the contact page
     */
    public function actionContact() {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                        "Reply-To: {$model->email}\r\n" .
                        "MIME-Version: 1.0\r\n" .
                        "Content-Type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
        $this->layout='login';

        if (Yii::app()->user->isGuest) {
            
            $model = new LoginForm;

            // if it is ajax validation request
            if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
           
            // collect user input data
            if (isset($_POST['LoginForm'])) {
                $model->attributes = $_POST['LoginForm'];
                // validate user input and redirect to the previous page if valid
                /*echo '<pre>';
                var_dump($model->login());
                echo '</pre>';
                exit;*/
                if ($model->validate() && $model->login())
                {
                    $this->redirect(Yii::app()->request->baseUrl.'/site/inicio');
                }else{
                    
                    $this->render('login', array('model' => $model));    
                }

            }else{
                $this->render('login', array('model' => $model));    
            }
            
            
        } else {
            $this->redirect(Yii::app()->request->baseUrl.'/site/inicio');
        }
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        //$this->redirect(Yii::app()->homeUrl);

        $conexion=Yii::app()->db;
        $hash=Yii::app()->user->id['usuario']['sesion'];
        $this->funciones->VerificarSesion();
        $sql="UPDATE seguridad_sesiones 
              SET pesta_codig = '2'
              WHERE sesio_numer='".$hash."'";
        $sesion=$conexion->createCommand($sql)->execute();


        Yii::app()->user->logout();
        Yii::app()->user->setFlash('danger', 'Su sesion esta vencida');
        $this->redirect(Yii::app()->request->baseUrl.'/site/login');
    }
    public function actionPruebaCorreo()
    {
        $nombre='KEVIN ALEJANDRO FIGUERA RODRIGUEZ';
        /*$this->renderpartial('olvido',array('nombre' => $nombre ));
        exit;*/

        $message = new YiiMailMessage;
         //mensaje
         $message->setBody($this->renderpartial('olvido',array('nombre' => $nombre ),true),'text/html');
         //asunto
         $message->subject = 'SWT - IMPORTADORA';
         //Destinatarios
         $message->addTo('kevinalejandro3@gmail.com');
         
         $message->setBcc(array(Yii::app()->params['adminEmail']));
        
        
         //Remitente
         $message->setFrom(Yii::app()->params['adminEmail'],'Contacto SWT');
         $mail=Yii::app()->mail->send($message);
         echo $mail;
    }

    public function actionVerificarTelefono(){
        if($_POST){
            $sql="SELECT * FROM admin_usuarios WHERE usuar_login = '".$_POST['correo']."'";
                $usu=yii::app()->db->createCommand($sql)->queryRow();
                $message = new YiiMailMessage;
                //mensaje
                $message->setBody($this->renderpartial('correo_registro',array("datos"=>$_POST,'usu'=>$usu),true),'text/html');
                //asunto
                $message->subject = 'UVEN - Verificación de Usuario';
                //Destinatarios
                $message->addTo($_POST['correo']);
                $message->addTo(Yii::app()->params['adminEmail']);
                //Remitente
                $message->from = Yii::app()->params['adminEmail'];
                Yii::app()->mail->send($message);
            
        }else{
            $this->renderpartial('validar_telefono');
        }
    }
    public function actionVerRegistro() {
        $conexion=yii::app()->db;
        $sql="SELECT usuar_codig, usuar_login, perso_pnomb, perso_papel, usuar_fcrea
            FROM seguridad_usuarios a
            JOIN p_persona b ON (a.perso_codig = b.perso_codig)
            WHERE usuar_login = 'kevinalejandro@gmail.com'";
        
        $row=$conexion->createCommand($sql)->queryRow();
        $this->renderpartial('registro_correo',array('row'=>$row));
        
    }
    public function actionOlvide() {
         //$this->layout='main2';
        /*var_dump($_REQUEST);
        exit();*/
        if ($_POST) {
            $conexion=yii::app()->db;
            $transaction=$conexion->beginTransaction();
            
            try{
                $sql="SELECT usuar_codig, usuar_login, perso_pnomb, perso_papel, usuar_fcrea
                    FROM seguridad_usuarios a
                    JOIN p_persona b ON (a.perso_codig = b.perso_codig)
                    WHERE usuar_login = '".$_POST['correo']."'";
                
                $row=$conexion->createCommand($sql)->queryRow();
                /*var_dump($row);
                exit();*/
                if($row){
                    $message = new YiiMailMessage;
                     //mensaje
                     $message->setBody($this->renderpartial('olvido',array('row'=>$row),true),'text/html');
                     //asunto
                     $message->subject = 'RapidPago | Restablecer Contraseña';
                     //Destinatarios
                     $message->addTo($_POST['correo']);
                     $message->setBcc(
                    array(/*'kevinalejandro3@gmail.com'=>'Kevin Alejandro Figuera',
                          'kfiguera@smartwebtools.net'=>'Kevin Figuera',
                          'iveymontoya@gmail.com'=>'Ivey Montoya',
                          'imontoya@smartwebtools.net'=>'Ivey Montoya',*/
                          Yii::app()->params['adminEmail']=>'Admin'));
                     //Remitente
                    $message->from=array(Yii::app()->params['adminEmail']=>'Contacto Rapidpago') ;
                    echo '<pre>';
                    $host=Yii::app()->mail->getTransport()->getHost();
                    $ip= gethostbyname(Yii::app()->mail->getTransport()->getHost());
                    if($host!=$ip){
                        $msg=Yii::app()->mail->send($message);  
                     Yii::app()->user->setFlash('success', 'Pronto recibiras instrucciones!');
                    }

                }else{
                    $transaction->rollBack();
                    $msg=array('success'=>'false','msg'=>'Usuario no existe');
                    Yii::app()->user->setFlash('danger', 'Error al Ejecutar el proceso');
                }

            }catch(CDbException $e){
                $transaction->rollBack();
                Yii::app()->user->setFlash('danger', 'Error al Ejecutar el proceso');
            }
            
            $this->redirect(Yii::app()->request->baseUrl.'/site/login');
           
        } else {
            Yii::app()->user->setFlash('danger', 'Debe ingresar su correo electronico para continuar con la recuperacion ');
            $this->redirect(Yii::app()->request->baseUrl.'/site/login');
        }
    }
        
    public function actionRegistro() {
        $this->layout='registro';
        if ($_POST) {
            //PERSONA
            $ndocu= $this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['ndocu']));
            $pnomb = mb_strtoupper($_POST['pnomb']);
            $snomb = mb_strtoupper($_POST['snomb']);
            $papel = mb_strtoupper($_POST['papel']);
            $sapel = mb_strtoupper($_POST['sapel']);
            
            //USUARIO
            $corre = mb_strtolower($_POST['corre']);
            $ccorr = mb_strtolower($_POST['ccorr']);
            $obser = mb_strtoupper($_POST['obser']);
            $permi = 2;
            $estat = 2;

            //CONTACTO
            $tcelu = mb_strtoupper($_POST['tcelu']);
            $tloca = mb_strtoupper($_POST['tloca']);


            $conexion=Yii::app()->db;
            $transaction=$conexion->beginTransaction();
            if($corre==$ccorr){
                try{
                    //VERIFICAR SI EXISTE LA PERSONA
                    
                    $sql="SELECT max(perso_cedul) perso_cedul FROM p_persona";
                    $perso=$conexion->createCommand($sql)->queryRow();
                    $ndocu= $perso['perso_cedul']+1;

                    $sql="SELECT * FROM p_persona WHERE perso_cedul='".$ndocu."'";
                    //$p_persona=$conexion->createCommand($sql)->queryRow();
                    if(!$p_persona){
                        //SI NO EXISTE SE GUARDA
                        $sql="INSERT INTO p_persona(perso_cedul, perso_pnomb, perso_snomb, perso_papel, perso_sapel, perso_obser) 
                            VALUES ('".$ndocu."','".$pnomb."','".$snomb."','".$papel."','".$sapel."','".$obser."')";

                        $res1=$conexion->createCommand($sql)->execute();
                        if($res1){
                            $sql="SELECT * FROM p_persona WHERE perso_cedul='".$ndocu."'";
                            $p_persona=$conexion->createCommand($sql)->queryRow();  
                            
                            $msg=array('success'=>'true','msg'=>'Persona guardada correctamente');    
                        }else{
                            $transaction->rollBack();
                            $msg=array('success'=>'false','msg'=>'Error al guardar la persona');    
                        }   
                    }
                    if($p_persona){
                        $perso=$p_persona['perso_codig'];
                        //VERIFICAR SI EXISTE LA PERSONA COMO USUARIO
                        $sql="SELECT * FROM seguridad_usuarios WHERE perso_codig ='".$perso."'";
                        $upers=$conexion->createCommand($sql)->queryRow();
                        if(!$upers){
                            //VERIFICAR SI EXISTE EL USUARIO
                            $sql="SELECT * FROM seguridad_usuarios WHERE usuar_login ='".$corre."'";
                            $usuario=$conexion->createCommand($sql)->queryRow();
                            if(!$usuario){
                                $sql="INSERT INTO seguridad_usuarios(perso_codig, usuar_login, urole_codig, uesta_codig, usuar_obser, usuar_fcrea) 
                                    VALUES ('".$perso."','".$corre."','".$permi."','".$estat."','".$obser."','".date('Y-m-d')."' )";
                                $res1=$conexion->createCommand($sql)->execute();
                                if($res1){
                                    $sql="SELECT * FROM seguridad_usuarios WHERE usuar_login ='".$corre."'";
                                    $usuario=$conexion->createCommand($sql)->queryRow();

                                    $msg=array('success'=>'true','msg'=>'Usuario guardado correctamente');  
                                }else{
                                    $transaction->rollBack();
                                    $msg=array('success'=>'false','msg'=>'Error al guardar el Usuario');    
                                }
                            }else{
                                $transaction->rollBack();
                                $msg=array('success'=>'false','msg'=>'Usuario ya existe');
                            }
                            
                        }else{
                            $transaction->rollBack();
                            $msg=array('success'=>'false','msg'=>'La persona ya tiene un usuario');
                        }
                    }else{
                        $transaction->rollBack();
                            $msg=array('success'=>'false','msg'=>'Error al guardar la persona');    
                    }
                    //GUARDAR EL REGISTRO PARA SU POSTERIOR APROVACION
                    if($usuario and $msg['success']=='true'){
                        $sql="INSERT INTO registro(cont1_ndocu, cont1_pnomb, cont1_snomb, cont1_papel, cont1_sapel, cont1_corre, cont1_tofic, cont1_tcelu, eregi_codig, regis_obser, usuar_codig, regis_fcrea, regis_hcrea) 
                            VALUES ('".$ndocu."', '".$pnomb."', '".$snomb."', '".$papel."', '".$sapel."', '".$corre."', '".$tloca."', '".$tcelu."', '1', '".$obser."', '".$usuario['usuar_codig']."', '".date('Y-m-d')."', '".date('H:i:s')."')";  
                        $res1=$conexion->createCommand($sql)->execute();
                        if($res1){
                            $datos['row']=$p_persona;
                            $this->funciones->enviarCorreo('../site/registro_correo',$datos,'Polerones Tiempo | Registro de Usuario',$corre);

                            $transaction->commit();
                            $msg=array('success'=>'true','msg'=>'registro guardado correctamente');  
                        }else{
                            $transaction->rollBack();
                            $msg=array('success'=>'false','msg'=>'Error al guardar el Registro');    
                        }
                    }
                }catch(Exception $e){
                    var_dump($e);
                    $transaction->rollBack();
                    $msg=array('success'=>'false','msg'=>'Error al verificar la información');
                }
            }else{
                $transaction->rollBack();
                $msg=array('success'=>'false','msg'=>'El Correo y su Confirmación no son iguales');
            }
            /*$usuario = new Tusuario();
            $usuario->tusua_corre = $_POST["correo"];
            $usuario->tgene_permi = $permi;
            $usuario->tpers_nacio = $_POST["nacio"];
            $usuario->tpers_cedul = $_POST["cedula"];
            $usuario->tusua_statu = 1;
            $usuario->tusua_corr2 = $_POST["correo2"];
            $usuario->tusua_passw = md5($_POST["password"]);
            $usuario->organ_codig = $_POST["organo"];
            $usuario->tgene_cargo = 369;
            $usuario->tusua_telef = $_POST["telefono"];
            $usuario->tusua_obser = $_POST["observaciones"];
            $usuario->tusua_fcrea = date('Y-m-d');
            if ($usuario->save()) {
                Yii::app()->user->setFlash('success', 'Usuario Registrado con exito');
                $msg = array('success' => 'true', 'msg' => 'Usuario Registrado con exito');
            } else {
                Yii::app()->user->setFlash('Danger', 'Error al Registrar el Usuario');
                $msg = array('success' => 'false', 'msg' => 'Error al Registrar el Usuario');
            }*/
            echo json_encode($msg);
        } else {
	            $this->render('registro');
        }
    }


    public function actionCompletar() {
        $this->layout='restaurar';

        if($_REQUEST){
            $conexion=yii::app()->db;
            $transaction=$conexion->beginTransaction();
            try {
                $sql="SELECT * FROM seguridad_usuarios WHERE usuar_codig = '".$_REQUEST['c']."'";
                $usu=$conexion->createCommand($sql)->queryRow();    
                /*var_dump($usu);
                exit()*/
                $c=$usu['usuar_codig'];
                $v=$usu['usuar_codig'].$usu['usuar_login'].$usu['usuar_fcrea'];
                $v2=md5($v);
                if($_REQUEST['v']===$v2){
                    //echo 'SI';
                    $this->render('restaurar',array('usu'=>$usu));
     
                }else{
                    Yii::app()->user->setFlash('danger', 'Error clave no valida');
                    $this->redirect(Yii::app()->request->baseUrl.'/site/login');

                }
            } catch (CDbException $e) {
                Yii::app()->user->setFlash('danger', 'Error al ejecutar el proceso');
                            $this->redirect(Yii::app()->request->baseUrl.'/site/login');

                $transaction->rollBack();
            }

        }else{
            
            $this->redirect(Yii::app()->request->baseUrl.'/site/login');
        }
    }
    public function actionRestaurar() {
        if($_REQUEST){
            $contra=$_REQUEST['contra'];
            $ccontra=$_REQUEST['ccontra'];
            if($contra==$ccontra){
                $conexion=yii::app()->db;
                $transaction=$conexion->beginTransaction();
                try {
                    $sql="SELECT * FROM seguridad_usuarios WHERE usuar_codig = '".$_REQUEST['c']."' ";
                    $usu=$conexion->createCommand($sql)->queryRow();    
                    if($usu){
                        $sql="UPDATE seguridad_usuarios SET usuar_passw='".md5($contra)."' WHERE usuar_codig = '".$_REQUEST['c']."'";
                        $res=$conexion->createCommand($sql)->execute(); 
                        if($res){
                            Yii::app()->user->setFlash('success', 'Contraseña restaurada exitosamente');
                            $transaction->commit();
                        }else{

                            Yii::app()->user->setFlash('danger', 'Error al restaurar la contraseña');
                            $transaction->rollBack();
                        }
                    }else{
                        Yii::app()->user->setFlash('danger', 'Error al buscvar el usuario');
                        $transaction->rollBack();
                    }
                } catch (CDbException $e) {
                    var_dump($e);
                    Yii::app()->user->setFlash('danger', 'Error al ejecutar el proceso');
                    $transaction->rollBack();
                }
            }else{
                Yii::app()->user->setFlash('danger', 'Las contraseñas no son iguales');    
            }
            $this->redirect(Yii::app()->request->baseUrl.'/site/login');
        }else{
            Yii::app()->user->setFlash('danger', 'Error al recibir la información');
            $this->redirect(Yii::app()->request->baseUrl.'/site/login');
        }
    }   


    public function actionGenerarTipoAna() {
        $this->layout = 'pdf/site';
        if ($_POST['organ'] == 988) {
            $this->render('tipoAna');
        } else {
            ?>
            <script>
                $('#login-form')
                        .bootstrapValidator('removeField','tipo');
            </script>
            <?php

        }
    }
    public function actionactualizarUsuario(){
        $actualizar=  Usuarios::model()->find("usuar_cedul = '".$_REQUEST['cedula']."'");
        $actualizar->tusua_statu=$_REQUEST['estatus'];
        if($actualizar->update()){
            $msg=array('success' => 'true', 'msg' => 'Usuario actualizado con exito');
            //return true;
        }  else {
            $msg=array('success' => 'false', 'msg' => 'El usuario no se pudo actualizar');
            //return FALSE;
        }
        echo json_encode($msg);
    }

}
