<?php

class ReasignarController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->redirect(Yii::app()->request->baseUrl.'/Articulos/listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descripcion']){
			$descripcion=mb_strtoupper($_POST['descripcion']);
			$condicion.="a.inven_descr like '%".$descripcion."%' ";
			$con++;
		}
		if($_POST['codigo']){
			if($con>0){
				$condicion.="AND ";
			}
			$codigo=mb_strtoupper($_POST['codigo']);
			$condicion.="a.inven_cprod like '%".$codigo."%' ";
			$con++;
		}
		if($_POST['cantidad']){
			if($con>0){
				$condicion.="AND ";
			}
			$cantidad = $this->funciones->TransformarMonto_bd($_POST['cantidad']);
			$condicion.="a.inven_canti like '%".$cantidad."%' ";
			$con++;
		}
		if($_POST['punidad']){
			if($con>0){
				$condicion.="AND ";
			}
			$punidad = $this->funciones->TransformarMonto_bd($_POST['punidad']);
			$condicion.="a.inven_punid like '%".$punidad."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM solicitud  a
			  WHERE a.solic_codig ='".$_GET['c']."'";
		$solicitud=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('solicitud' => $solicitud));
	}
	public function actionRegistrar()
	{
		if($_POST){
			/*var_dump($_POST);
			exit();*/
			$descr=mb_strtoupper($_POST['descr']);
			$codig=mb_strtoupper($_POST['codig']);
			$punid=mb_strtoupper($_POST['punid']);
			$canti=mb_strtoupper($_POST['canti']);
			$tunid=mb_strtoupper($_POST['tunid']);
			$moned=mb_strtoupper($_POST['moned']);
			$obser=mb_strtoupper($_POST['obser']);
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM inventario WHERE inven_descr = '".$descr."'";
				$inven=$conexion->createCommand($sql)->queryRow();
				if(!$inven){
					$sql="SELECT * FROM inventario WHERE inven_cprod = '".$codig."'";
					$codigo=$conexion->createCommand($sql)->queryRow();
					if(!$codigo){
						$sql="INSERT INTO inventario(inven_cprod,inven_descr,inven_punid,inven_canti,tunid_codig,
								moned_codig,inven_obser,usuar_codig,inven_fcrea,inven_hcrea)
							  VALUES('".$codig."','".$descr."','".$punid."','".$canti."','".$tunid."',
							  	'".$moned."','".$obser."','".Yii::app()->user->id['usuario']['codigo']."','".date('Y-m-d')."','".date('H:i:s')."')";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Producto guardado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al guardar el Producto');	
						}	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El código de producto ya existe ya existe');
					}	
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El producto ya existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){			
			
			//SOLICITUD
			$solic=mb_strtoupper($_POST['solic']);
			$codig=	($_POST['codig']);
			//PRODUCTOS
			$estat=12;
			$canti=0;
			foreach ($_POST["asign"] as $key => $value) {
				$producto[$key]["asign"]=$_POST["asign"][$key];
				$producto[$key]["sdisp"]=$_POST["sdisp"][$key];
				$producto[$key]["soper"]=$_POST["soper"][$key];
				$canti++;
			}
			$dispo=$producto;
			asort($dispo);
			$ant='';
			$pre='';
			$i=0;
			foreach ($dispo as $key => $value) {
				if($i>0){
					if($value['sdisp']==$ant){
						$msg=array('success'=>'false','msg'=>'No puede incluir un Dispositivo más de una vez');
						echo json_encode($msg);
					exit();
					}
					if($value['soper']==$pre){
						$msg=array('success'=>'false','msg'=>'No puede incluir una Tarjeta SIM más de una vez');
						echo json_encode($msg);
						exit();
					}
				}
				/*if($value['soper']=='' AND $value['sdisp']==''){
					$msg=array('success'=>'false','msg'=>'Debe seleccionar un Nuevo POS o SIM');
					echo json_encode($msg);
					exit();
				}*/
				$ant=$value['sdisp'];
				$pre=$value['soper'];
				$i++;
			}
			
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$estat='12';
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM solicitud WHERE solic_codig = '".$solic."'";
				$solicitud=$conexion->createCommand($sql)->queryRow();

				if($solicitud){
					$sql="SELECT SUM(cequi_canti) cantidad, a.* FROM solicitud_equipos a WHERE solic_codig = '".$solic."'";
					$equipos=$conexion->createCommand($sql)->queryRow();	
					if($equipos['cantidad']==$canti){
						foreach ($producto as $key => $value) {



							$sql="SELECT * 
								  FROM solicitud_asignacion 
								  WHERE asign_codig='".$value['asign']."'";
							$asignados=$conexion->createCommand($sql)->queryRow();
							if($asignados){

								if($value['sdisp']==''){
									
									$value['sdisp']=$asignados['asign_dispo'];
									$asignados['asign_dispo']='';

									
								}
								if($value['soper']==''){
									$value['soper']=$asignados['asign_opera'];
									$asignados['asign_opera']='';

									
								}

								if($asignados['asign_dispo']!=''){
									

									$sql="SELECT * 
									  		FROM inventario_seriales 
									  		WHERE seria_codig='".$asignados['asign_dispo']."'";
									$serial=$conexion->createCommand($sql)->queryRow();

									$sql="UPDATE inventario_seriales	
									  SET almac_codig='4' 
									  WHERE seria_codig='".$value['sdisp']."'";
									$query=$sql;
									$res2=$conexion->createCommand($sql)->execute();

									$sql="SELECT * 
									  		FROM inventario_seriales 
									  		WHERE seria_codig='".$asignados['asign_dispo']."'";
									$serial2=$conexion->createCommand($sql)->queryRow();

									$traza=$this->funciones->guardarTraza($conexion, 'U', 'inventario_seriales', json_encode($serial),json_encode($serial2),$query);
								}
								if($asignados['asign_opera']!=''){

									$sql="SELECT * 
									  		FROM inventario_seriales 
									  		WHERE seria_codig='".$value['sdisp']."'";
									$serial=$conexion->createCommand($sql)->queryRow();

									$sql="UPDATE inventario_seriales	
									  SET almac_codig='4' 
									  WHERE seria_codig='".$asignados['asign_opera']."'";
									$query=$sql;
									$res2=$conexion->createCommand($sql)->execute();

									$sql="SELECT * 
									  		FROM inventario_seriales 
									  		WHERE seria_codig='".$asignados['asign_opera']."'";
									$serial2=$conexion->createCommand($sql)->queryRow();

									$traza=$this->funciones->guardarTraza($conexion, 'U', 'inventario_seriales', json_encode($serial),json_encode($serial2),$query);
								}

								$sql="UPDATE solicitud_asignacion
									  SET asign_dispo='".$value['sdisp']."', 
									  	  asign_opera='".$value['soper']."', 
									  	  asign_sdisp='".$asignados['asign_dispo']."', 
									  	  asign_soper='".$asignados['asign_opera']."' 
									  WHERE asign_codig='".$value['asign']."'";
								$query=$sql;
								$res2=$conexion->createCommand($sql)->execute();
								
								$sql="SELECT * 
								  FROM solicitud_asignacion 
								  WHERE asign_codig='".$value['asign']."'";
								$asignados2=$conexion->createCommand($sql)->queryRow();

								$traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud_asignacion', json_encode($asignados),json_encode($asignados2),$query);




								/*if(!$res2){
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al guardar la Reasignacion');
									echo json_encode($msg);
									exit();
								}*/
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'El dispositivo no esta asignado');
								echo json_encode($msg);
									exit();
							}
						}

						$sql="UPDATE solicitud 
				    		SET estat_codig='".$estat."'
				    		WHERE solic_codig='".$solic."'";
				    	$query=$sql;
						$res2=$conexion->createCommand($sql)->execute();

						$sql="SELECT * FROM solicitud WHERE solic_codig = '".$value."' ";
						$solicitud2=$conexion->createCommand($sql)->queryRow();

				    	$traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud', json_encode($solicitud),json_encode($solicitud2),$query);
			        	
			        	
						$notificacion=$this->funciones->GuardarNotificacion($solicitud['solic_codig'], $estat);
						
						$trayectoria=$this->funciones->guardarTrayectoriaSolicitudes($conexion, $solicitud['solic_codig'], $estat, $solicitud['estat_codig'], 2);

						if(!$res2){
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar la solicitud');
							echo json_encode($msg);
							exit();
						}
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Dispositivos Asignados correctamente');
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'La cantidad de dispositivos debe ser igual a la Solicitud');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'La Solicitud no existe');
				}
				
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;

			$sql="SELECT * 
			  FROM solicitud  a
			  WHERE a.solic_codig ='".$_GET['c']."'";
			  
			$solicitud=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('solicitud' => $solicitud));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codigo'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM factura_producto  WHERE inven_codig='".$codig."'";
				$prod=$conexion->createCommand($sql)->queryRow();
				if(!$prod){
					$sql="SELECT * FROM inventario  WHERE inven_codig='".$codig."'";
					$roles=$conexion->createCommand($sql)->queryRow();
					if($roles){

						$sql="DELETE FROM inventario  WHERE inven_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Producto eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Producto ');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Producto no existe');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No se puede eliinar debido a que, esta asociada a una factura');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM inventario  a
			  WHERE a.inven_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('roles' => $roles));
		}
	}
	public function actionSolicitudes()
	{
		if($_POST){
			exit();
			$codig=$_POST['codigo'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM factura_producto  WHERE inven_codig='".$codig."'";
				$prod=$conexion->createCommand($sql)->queryRow();
				if(!$prod){
					$sql="SELECT * FROM inventario  WHERE inven_codig='".$codig."'";
					$roles=$conexion->createCommand($sql)->queryRow();
					if($roles){

						$sql="DELETE FROM inventario  WHERE inven_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Producto eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Producto ');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Producto no existe');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No se puede eliinar debido a que, esta asociada a una factura');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM inventario  a
			  WHERE a.inven_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('verificar', array('roles' => $roles));
		}
	}
	public function actionAprobarSolicitud()
	{
		if($_POST){
			$id=$_POST['id'];
			if(!$id){
				$msg=array('success'=>'false','msg'=>'Debe seleccionar al menos una solicitud');
				echo json_encode($msg);
				exit();
			}
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$estat=12;

			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				foreach ($id as $key => $value) {
					$sql="SELECT * FROM solicitud WHERE solic_codig = '".$value."' ";
					$solicitud=$conexion->createCommand($sql)->queryRow();
					if($solicitud){
						$sql="UPDATE solicitud 
				    		SET estat_codig='".$estat."'
				    		WHERE solic_codig='".$value."'";
				    	$query=$sql;
				   		$res1=$conexion->createCommand($sql)->execute();

				   		$sql="SELECT * FROM solicitud WHERE solic_codig = '".$value."' ";
						$solicitud2=$conexion->createCommand($sql)->queryRow();

				    	$traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud', json_encode($solicitud),json_encode($solicitud2),$query);
			        	
			        	
						$notificacion=$this->funciones->GuardarNotificacion($solicitud['solic_codig'], $estat);
						
						$trayectoria=$this->funciones->guardarTrayectoriaSolicitudes($conexion, $solicitud['solic_codig'], $estat, $solicitud['estat_codig'], 2);
			        	
					}else{
		        		$transaction->rollBack();
	        			$msg=array('success'=>'false','msg'=>'No se encontro la solicitud');
	        			echo json_encode($msg);
	        			exit();
					}
				}
				$transaction->commit();
				$msg=array('success'=>'true','msg'=>'Solicitudes Aprobadas correctamente');			    
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$this->render('aprobarSolicitud');
		}
	}
	public function actionReversarSolicitud()
	{
		if($_POST){
			$id=$_POST['id'];
			if(!$id){
				$msg=array('success'=>'false','msg'=>'Debe seleccionar al menos una solicitud');
				echo json_encode($msg);
				exit();
			}
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$estat=11;

			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				foreach ($id as $key => $value) {
					$sql="SELECT * FROM solicitud WHERE solic_codig = '".$value."' ";
					$solicitud=$conexion->createCommand($sql)->queryRow();
					if($solicitud){
						if($solicitud['estat_codig']=='9'){
							$sql="UPDATE solicitud 
					    		SET estat_codig='".$estat."'
					    		WHERE solic_codig='".$value."'";
					    	$query=$sql;
					   		$res1=$conexion->createCommand($sql)->execute();

					   		$sql="SELECT * FROM solicitud WHERE solic_codig = '".$value."' ";
							$solicitud2=$conexion->createCommand($sql)->queryRow();

					    	$traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud', json_encode($solicitud),json_encode($solicitud2),$query);
				        	
				        	
						$notificacion=$this->funciones->GuardarNotificacion($solicitud['solic_codig'], $estat);
						
						$trayectoria=$this->funciones->guardarTrayectoriaSolicitudes($conexion, $solicitud['solic_codig'], $estat , $solicitud['estat_codig'], 2);
			        	}else{
			        		$transaction->rollBack();
		        			$msg=array('success'=>'false','msg'=>'No se puede reversar la solicitud');
		        			echo json_encode($msg);
		        			exit();
			        	}
					}else{
		        		$transaction->rollBack();
	        			$msg=array('success'=>'false','msg'=>'No se encontro la solicitud');
	        			echo json_encode($msg);
	        			exit();
					}
				}
				$transaction->commit();
				$msg=array('success'=>'true','msg'=>'Solicitudes Reversadas correctamente');			    
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$this->render('reversarSolicitud');
		}
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}