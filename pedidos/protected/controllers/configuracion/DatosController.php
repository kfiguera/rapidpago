<?php

class DatosController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->redirect(Yii::app()->request->baseUrl.'/Registro/listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['colegio']){
			$colegio=mb_strtoupper($_POST['colegio']);
			$condicion.="a.regis_nombr like '%".$colegio."%' ";
			$con++;
		}
		if($_POST['usuario']){
			if($con>0){
				$condicion.="AND ";
			}
			$usuario=mb_strtoupper($_POST['usuario']);
			$condicion.="c.usuar_login like '%".$usuario."%' ";
			$con++;
		}
		if($_POST['estatus']){
			if($con>0){
				$condicion.="AND ";
			}
			$estatus=mb_strtoupper($_POST['estatus']);
			$condicion.="a.eregi_codig like '%".$estatus."%' ";
			$con++;
		}
		if($con>0){
			$condicion="AND ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM registro  a
			  WHERE a.regis_codig ='".$_GET['c']."'";
		$roles=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('roles' => $roles));
	}
	public function actionRegistrar()
	{
		if($_POST){
			
			$descr=mb_strtoupper($_POST['descr']);
			$codig=mb_strtoupper($_POST['codig']);
			$punid=mb_strtoupper($_POST['punid']);
			$canti=mb_strtoupper($_POST['canti']);
			$tunid=mb_strtoupper($_POST['tunid']);
			$moned=mb_strtoupper($_POST['moned']);
			$obser=mb_strtoupper($_POST['obser']);
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM registro WHERE regis_descr = '".$descr."'";
				$inven=$conexion->createCommand($sql)->queryRow();
				if(!$inven){
					$sql="SELECT * FROM registro WHERE regis_cprod = '".$codig."'";
					$codigo=$conexion->createCommand($sql)->queryRow();
					if(!$codigo){
						$sql="INSERT INTO registro(regis_cprod,regis_descr,regis_punid,regis_canti,tunid_codig,
								moned_codig,regis_obser,usuar_codig,regis_fcrea,regis_hcrea)
							  VALUES('".$codig."','".$descr."','".$punid."','".$canti."','".$tunid."',
							  	'".$moned."','".$obser."','".Yii::app()->user->id['usuario']['codigo']."','".date('Y-m-d')."','".date('H:i:s')."')";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$sql="SELECT a.*, b.*
								  FROM p_persona a
								  JOIN seguridad_usuarios b ON (a.perso_codig=b.perso_codig)
								  WHERE b.usuar_codig='".$pedidos['usuar_codig']."'";
                    		$p_persona=$conexion->createCommand($sql)->queryRow();

							$datos['row']=$p_persona;
							$corre=$p_persona['usuar_login'];
                            $this->funciones->enviarCorreo('../registro/datos/registro_correo',$datos,'Polerones Tiempo | Registro de Datos',$corre);
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Registro guardado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al guardar el Registro');	
						}	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El código de producto ya existe ya existe');
					}	
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El producto ya existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			/*var_dump($_POST);
			exit();*/
			$codigo=mb_strtoupper($_POST['codigo']);
			//COLEGIO
			$coleg=mb_strtoupper($_POST['coleg']);
			if(!$coleg){
				$msg=array('success'=>'false','msg'=>'Debe Seleccionar un colegio');
				echo json_encode($msg);
				exit();
			}
			$pgweb=mb_strtoupper($_POST['pgweb']);
			$telef=mb_strtoupper($_POST['telef']);
			$direc=mb_strtoupper($_POST['direc']);
			$rede1=mb_strtoupper($_POST['rede1']);
			$rede2=mb_strtoupper($_POST['rede2']);
			$rede3=mb_strtoupper($_POST['rede3']);
			//CONTACTO 1
			$cont1_ndocu=mb_strtoupper($_POST['cont1_ndocu']);
			$cont1_pnomb=mb_strtoupper($_POST['cont1_pnomb']);
			$cont1_snomb=mb_strtoupper($_POST['cont1_snomb']);
			$cont1_papel=mb_strtoupper($_POST['cont1_papel']);
			$cont1_sapel=mb_strtoupper($_POST['cont1_sapel']);
			$cont1_fnaci=$this->funciones->TransformarFecha_bd(mb_strtoupper($_POST['cont1_fnaci']));
			$cont1_gener=mb_strtoupper($_POST['cont1_gener']);
			$cont1_curso=mb_strtoupper($_POST['cont1_curso']);
			$cont1_tofic=mb_strtoupper($_POST['cont1_tofic']);
			$cont1_tcelu=mb_strtoupper($_POST['cont1_tcelu']);

			//CONTACTO 2
			$cont2_ndocu=mb_strtoupper($_POST['cont2_ndocu']);
			$cont2_pnomb=mb_strtoupper($_POST['cont2_pnomb']);
			$cont2_snomb=mb_strtoupper($_POST['cont2_snomb']);
			$cont2_papel=mb_strtoupper($_POST['cont2_papel']);
			$cont2_sapel=mb_strtoupper($_POST['cont2_sapel']);
			$cont2_corre=mb_strtoupper($_POST['cont2_corre']);
			$cont2_fnaci=$this->funciones->TransformarFecha_bd(mb_strtoupper($_POST['cont2_fnaci']));
			$cont2_gener=mb_strtoupper($_POST['cont2_gener']);
			$cont2_curso=mb_strtoupper($_POST['cont2_curso']);
			$cont2_tofic=mb_strtoupper($_POST['cont2_tofic']);
			$cont2_tcelu=mb_strtoupper($_POST['cont2_tcelu']);
			$obser=mb_strtoupper($_POST['obser']);

			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{

				$sql="SELECT * FROM registro  WHERE regis_codig ='".$codigo."'";
				$roles=$conexion->createCommand($sql)->queryRow();
				if($roles){
					$sql="UPDATE registro 
					  		SET coleg_codig = '".$coleg."',
								regis_pgweb = '".$pgweb."',
								regis_telef = '".$telef."',
								regis_direc = '".$direc."',
								regis_rede1 = '".$rede1."',
								regis_rede2 = '".$rede2."',
								regis_rede3 = '".$rede3."',
								cont1_ndocu = '".$cont1_ndocu."',
								cont1_pnomb = '".$cont1_pnomb."',
								cont1_snomb = '".$cont1_snomb."',
								cont1_papel = '".$cont1_papel."',
								cont1_sapel = '".$cont1_sapel."',
								cont1_fnaci = '".$cont1_fnaci."',
								cont1_gener = '".$cont1_gener."',
								cont1_curso = '".$cont1_curso."',
								cont1_tofic = '".$cont1_tofic."',
								cont1_tcelu = '".$cont1_tcelu."',
								cont2_ndocu = '".$cont2_ndocu."',
								cont2_pnomb = '".$cont2_pnomb."',
								cont2_snomb = '".$cont2_snomb."',
								cont2_papel = '".$cont2_papel."',
								cont2_sapel = '".$cont2_sapel."',
								cont2_corre = '".$cont2_corre."',
								cont2_fnaci = '".$cont2_fnaci."',
								cont2_gener = '".$cont2_gener."',
								cont2_curso = '".$cont2_curso."',
								cont2_tofic = '".$cont2_tofic."',
								cont2_tcelu = '".$cont2_tcelu."',
								regis_obser = '".$obser."',
								eregi_codig = '2'
							WHERE regis_codig ='".$codigo."'";
					$res1=$conexion->createCommand($sql)->execute();
					if($res1){
						$transaction->commit();
						$msg=$this->Verificar($codigo);
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al actualizar el Registro','sql'=>$sql);	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Registro no existe');
				}
			}catch(Exception $e){
				
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			
			$sql="SELECT * 
			  FROM registro  a
			  WHERE a.regis_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('roles' => $roles));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codigo'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM factura_producto  WHERE regis_codig='".$codig."'";
				$prod=$conexion->createCommand($sql)->queryRow();
				if(!$prod){
					$sql="SELECT * FROM registro  WHERE regis_codig='".$codig."'";
					$roles=$conexion->createCommand($sql)->queryRow();
					if($roles){

						$sql="DELETE FROM registro  WHERE regis_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Registro eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Registro ');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Registro no existe');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No se puede eliinar debido a que, esta asociada a una factura');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM registro  a
			  WHERE a.regis_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('roles' => $roles));
		}
	}
	public function Verificar($codigo)
	{
		$conexion=Yii::app()->db;
		$transaction=$conexion->beginTransaction();
		try{
			$motiv='APROBADO';
			$urole=3;
			$estat=3;
			$accio='1';
			$sql="SELECT * FROM registro  WHERE regis_codig ='".$codigo."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			
			if($roles){
				$sql="INSERT INTO registro_movimiento(regis_codig, eregi_codig, mregi_motiv, usuar_codig, mregi_fcrea, mregi_hcrea) VALUES ('".$codigo."', '".$estat."', '".$motiv."', '".Yii::app()->user->id['usuario']['codigo']."', '".date('Y-m-d')."', '".date('H:i:s')."');";
				$res1=$conexion->createCommand($sql)->execute();
				if($res1){
					$sql="SELECT * 
					  		FROM seguridad_usuarios 
					  		WHERE usuar_codig='".$roles['usuar_codig']."'";
					$usuario=$conexion->createCommand($sql)->queryRow();
					
					$sql="UPDATE registro 
				  		SET eregi_codig = '".$estat."'
						WHERE regis_codig ='".$codigo."'";
					$res2=$conexion->createCommand($sql)->execute();
					if($res2){
						if($accio=='1'){
							$sql="UPDATE seguridad_usuarios
					  				SET urole_codig = '".$urole."',
					  					coleg_codig = '".$roles['coleg_codig']."'
								  WHERE usuar_codig = '".$roles['usuar_codig']."'";
							$res3=$conexion->createCommand($sql)->execute();
							if($res3){
								$sql="SELECT * 
									  FROM p_persona 
									  WHERE perso_codig=(SELECT perso_codig FROM seguridad_usuarios WHERE usuar_codig = '".$roles['usuar_codig']."')";
								$p_persona=$conexion->createCommand($sql)->queryRow();

								$sql="UPDATE p_persona SET
										perso_cedul='".$roles['cont1_ndocu']."',
										perso_pnomb='".$roles['cont1_pnomb']."',
										perso_snomb='".$roles['cont1_snomb']."',
										perso_papel='".$roles['cont1_papel']."',
										perso_sapel='".$roles['cont1_sapel']."',
										perso_fnaci='".$roles['cont1_fnaci']."',
										gener_value='".$roles['cont1_gener']."',
										perso_obser='".$roles['regis_nombr']."'
								  	WHERE perso_codig = '".$p_persona['perso_codig']."'";
								$res4=$conexion->createCommand($sql)->execute();
								//if($res4){
									$sql="SELECT max(perso_cedul) perso_cedul FROM p_persona";
				                    $p_persona2=$conexion->createCommand($sql)->queryRow();
				                    $roles['cont2_ndocu']= $p_persona2['perso_cedul']+1;

									$sql="SELECT * 
										  FROM p_persona 
										  WHERE perso_cedul='".$roles['cont2_ndocu']."'";
									$p_persona2=$conexion->createCommand($sql)->queryRow();
									if($p_persona2){
										$sql="UPDATE p_persona SET
												perso_cedul='".$roles['cont2_ndocu']."',
												perso_pnomb='".$roles['cont2_pnomb']."',
												perso_snomb='".$roles['cont2_snomb']."',
												perso_papel='".$roles['cont2_papel']."',
												perso_sapel='".$roles['cont2_sapel']."',
												perso_fnaci='".$roles['cont2_fnaci']."',
												gener_value='".$roles['cont2_gener']."',
												perso_obser='".$roles['regis_nombr']."'
										  	WHERE perso_codig = '".$p_persona2['perso_codig']."'";	
									}else{
										$sql="INSERT INTO p_persona(perso_cedul, perso_pnomb, perso_snomb, perso_papel, perso_sapel, perso_fnaci, gener_value, perso_obser) 
											VALUES ('".$roles['cont2_ndocu']."', '".$roles['cont2_pnomb']."', '".$roles['cont2_snomb']."', '".$roles['cont2_papel']."', '".$roles['cont2_sapel']."', '".$roles['cont2_fnaci']."', '".$roles['cont2_gener']."', '".$roles['regis_nombr']."')";
									}
									
									$res5=$conexion->createCommand($sql)->execute();
									//if($res5){
										$sql="SELECT * 
											  FROM colegio 
											  WHERE coleg_codig='".$roles['coleg_codig']."'";
										$colegio=$conexion->createCommand($sql)->queryRow();
										if($colegio){
											$sql="UPDATE colegio SET
													coleg_direc='".$roles['regis_direc']."',
													colec_telef='".$roles['regis_telef']."',
													coleg_pgweb='".$roles['regis_pgweb']."',
													coleg_rede1='".$roles['regis_rede1']."',
													coleg_rede2='".$roles['regis_rede2']."',
													coleg_rede3='".$roles['regis_rede3']."'
											  	WHERE coleg_codig = '".$colegio['coleg_codig']."'";	
										}else{
											$sql="INSERT INTO colegio( coleg_nombr,
													 coleg_direc, colec_telef,
													 coleg_pgweb, coleg_rede1,
													 coleg_rede2, coleg_rede3,
													 usuar_codig, coleg_fcrea,
													 coleg_hcrea) 
												  VALUES ('".$roles['regis_nombr']."',  '".$roles['regis_direc']."',  '".$roles['regis_telef']."',  '".$roles['regis_pgweb']."',  '".$roles['regis_rede1']."',  '".$roles['regis_rede2']."',  '".$roles['regis_rede3']."', '".Yii::app()->user->id['usuario']['codigo']."', '".date('Y-m-d')."', '".date('H:i:s')."')";
										}
										
										$res6=$conexion->createCommand($sql)->execute();

										//if($res6){
											$sql="SELECT * 
												  FROM p_persona 
												  WHERE perso_codig='".$p_persona['perso_codig']."'";
											$p_persona1=$conexion->createCommand($sql)->queryRow();
											$sql="SELECT * 
												  FROM p_persona 
												  WHERE perso_cedul='".$roles['cont2_ndocu']."'";
											$p_persona2=$conexion->createCommand($sql)->queryRow();
											$sql="SELECT * 
												  FROM colegio 
												  WHERE coleg_codig='".$roles['coleg_codig']."'";
											$colegio=$conexion->createCommand($sql)->queryRow();
											$sql="INSERT INTO colegio_contacto(coleg_codig, perso_codig, ccole_tipo, ccole_tofic, ccole_tcelu, ccole_curso, usuar_codig, ccole_fcrea, ccole_hcrea) 
												  VALUES ('".$colegio['coleg_codig']."','".$p_persona1['perso_codig']."','1','".$roles['cont1_tofic']."','".$roles['cont1_tcelu']."','".$roles['cont1_curso']."','".$roles['usuar_codig']."', '".date('Y-m-d')."', '".date('H:i:s')."')";
											$res7=$conexion->createCommand($sql)->execute();
											$sql="INSERT INTO colegio_contacto(coleg_codig, perso_codig, ccole_tipo, ccole_tofic, ccole_tcelu, ccole_curso, usuar_codig, ccole_fcrea, ccole_hcrea) 
												  VALUES ('".$colegio['coleg_codig']."','".$p_persona2['perso_codig']."','2','".$roles['cont2_tofic']."','".$roles['cont2_tcelu']."','".$roles['cont2_curso']."','".$roles['usuar_codig']."', '".date('Y-m-d')."', '".date('H:i:s')."')";
											$res8=$conexion->createCommand($sql)->execute();
											if($res7 and $res8){
													
													$transaction->commit();
													$vista='../registro/datos/verificar_correo';
													$datos['p_persona']=$p_persona1;
													$datos['estatus']=$estat;
													$asunto='Polerones Tiempo | Verificación de Registro';
													$destinatario=$usuario['usuar_login'];
													$this->funciones->enviarCorreo($vista,$datos,$asunto,$destinatario);
												  	
												$msg=array('success'=>'true','msg'=>'Registro Aprobado correctamente');
											}else{
												$transaction->rollBack();
												$msg=array('success'=>'false','msg'=>'Error al asociar los contactos al colegio');	
											}
											
										/*}else{
											$transaction->rollBack();
											$msg=array('success'=>'false','msg'=>'Error al actualizar el Colegio');	
										}*/
									/*}else{
										$transaction->rollBack();
										$msg=array('success'=>'false','msg'=>'Error al actualizar el Contacto 2');	
									}*/
								/*}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al actualizar el Contacto 1');	
								}*/
								
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al actualizar el Usuario');	
							}
						}else{
						
							$vista='../registro/datos/verificar_correo';
							$datos['p_persona']=$p_persona1;
							$datos['estatus']=$estat;
							$asunto='Polerones Tiempo | Verificación de Registro';
							$destinatario=$usuario['usuar_login'];
							$this->funciones->enviarCorreo($vista,$datos,$asunto,$destinatario);
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Registro rechazado correctamente');
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al actualizar el Registro');	
					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error al registrar el movimiento');	
				}
				
			}else{
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'El Registro no existe');
			}
		}catch(Exception $e){
			var_dump($e);
			$transaction->rollBack();
			$msg=array('success'=>'false','msg'=>'Error al verificar la información');
		}
		return $msg;
	}
	public function actionVerificar()
	{
		if($_POST){
			$codigo=mb_strtoupper($_POST['codigo']);
			$accio=mb_strtoupper($_POST['accio']);
			$motiv=mb_strtoupper($_POST['motiv']);
			if($accio=='1'){
				$estat=3;
				$urole=3;
			}else{
				$urole=2;
				$estat=9;	
			}
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
 				$estat=3;
				$urole=3;
				$sql="SELECT * FROM registro  WHERE regis_codig ='".$codigo."'";
				$roles=$conexion->createCommand($sql)->queryRow();
				
				if($roles){
					$sql="INSERT INTO registro_movimiento(regis_codig, eregi_codig, mregi_motiv, usuar_codig, mregi_fcrea, mregi_hcrea) VALUES ('".$codigo."', '".$estat."', '".$motiv."', '".Yii::app()->user->id['usuario']['codigo']."', '".date('Y-m-d')."', '".date('H:i:s')."');";
					$res1=$conexion->createCommand($sql)->execute();
					if($res1){
						$sql="SELECT * 
						  		FROM seguridad_usuarios 
						  		WHERE usuar_codig='".$roles['usuar_codig']."'";
						$usuario=$conexion->createCommand($sql)->queryRow();
						
						$sql="UPDATE registro 
					  		SET eregi_codig = '".$estat."'
							WHERE regis_codig ='".$codigo."'";
						$res2=$conexion->createCommand($sql)->execute();
						if($res2){
							if($accio=='1'){
								$sql="UPDATE seguridad_usuarios
						  				SET urole_codig = '".$urole."',
						  					coleg_codig = '".$roles['coleg_codig']."'
									  WHERE usuar_codig = '".$roles['usuar_codig']."'";
								$res3=$conexion->createCommand($sql)->execute();
								if($res3){
									$sql="SELECT * 
										  FROM p_persona 
										  WHERE perso_codig=(SELECT perso_codig FROM seguridad_usuarios WHERE usuar_codig = '".$roles['usuar_codig']."')";
									$p_persona=$conexion->createCommand($sql)->queryRow();

									$sql="UPDATE p_persona SET
											perso_cedul='".$roles['cont1_ndocu']."',
											perso_pnomb='".$roles['cont1_pnomb']."',
											perso_snomb='".$roles['cont1_snomb']."',
											perso_papel='".$roles['cont1_papel']."',
											perso_sapel='".$roles['cont1_sapel']."',
											perso_fnaci='".$roles['cont1_fnaci']."',
											gener_value='".$roles['cont1_gener']."',
											perso_obser='".$roles['regis_nombr']."'
									  	WHERE perso_codig = '".$p_persona['perso_codig']."'";
									$res4=$conexion->createCommand($sql)->execute();
									//if($res4){
										$sql="SELECT max(perso_cedul) perso_cedul FROM p_persona";
					                    $p_persona2=$conexion->createCommand($sql)->queryRow();
					                    $roles['cont2_ndocu']= $p_persona2['perso_cedul']+1;

										$sql="SELECT * 
											  FROM p_persona 
											  WHERE perso_cedul='".$roles['cont2_ndocu']."'";
										$p_persona2=$conexion->createCommand($sql)->queryRow();
										if($p_persona2){
											$sql="UPDATE p_persona SET
													perso_cedul='".$roles['cont2_ndocu']."',
													perso_pnomb='".$roles['cont2_pnomb']."',
													perso_snomb='".$roles['cont2_snomb']."',
													perso_papel='".$roles['cont2_papel']."',
													perso_sapel='".$roles['cont2_sapel']."',
													perso_fnaci='".$roles['cont2_fnaci']."',
													gener_value='".$roles['cont2_gener']."',
													perso_obser='".$roles['regis_nombr']."'
											  	WHERE perso_codig = '".$p_persona2['perso_codig']."'";	
										}else{
											$sql="INSERT INTO p_persona(perso_cedul, perso_pnomb, perso_snomb, perso_papel, perso_sapel, perso_fnaci, gener_value, perso_obser) 
												VALUES ('".$roles['cont2_ndocu']."', '".$roles['cont2_pnomb']."', '".$roles['cont2_snomb']."', '".$roles['cont2_papel']."', '".$roles['cont2_sapel']."', '".$roles['cont2_fnaci']."', '".$roles['cont2_gener']."', '".$roles['regis_nombr']."')";
										}
										
										$res5=$conexion->createCommand($sql)->execute();
										//if($res5){
											$sql="SELECT * 
												  FROM colegio 
												  WHERE coleg_codig='".$roles['coleg_codig']."'";
											$colegio=$conexion->createCommand($sql)->queryRow();
											if($colegio){
												$sql="UPDATE colegio SET
														coleg_direc='".$roles['regis_direc']."',
														colec_telef='".$roles['regis_telef']."',
														coleg_pgweb='".$roles['regis_pgweb']."',
														coleg_rede1='".$roles['regis_rede1']."',
														coleg_rede2='".$roles['regis_rede2']."',
														coleg_rede3='".$roles['regis_rede3']."'
												  	WHERE coleg_codig = '".$colegio['coleg_codig']."'";	
											}else{
												$sql="INSERT INTO colegio( coleg_nombr,
														 coleg_direc, colec_telef,
														 coleg_pgweb, coleg_rede1,
														 coleg_rede2, coleg_rede3,
														 usuar_codig, coleg_fcrea,
														 coleg_hcrea) 
													  VALUES ('".$roles['regis_nombr']."',  '".$roles['regis_direc']."',  '".$roles['regis_telef']."',  '".$roles['regis_pgweb']."',  '".$roles['regis_rede1']."',  '".$roles['regis_rede2']."',  '".$roles['regis_rede3']."', '".Yii::app()->user->id['usuario']['codigo']."', '".date('Y-m-d')."', '".date('H:i:s')."')";
											}
											
											$res6=$conexion->createCommand($sql)->execute();

											//if($res6){
												$sql="SELECT * 
													  FROM p_persona 
													  WHERE perso_codig='".$p_persona['perso_codig']."'";
												$p_persona1=$conexion->createCommand($sql)->queryRow();
												$sql="SELECT * 
													  FROM p_persona 
													  WHERE perso_cedul='".$roles['cont2_ndocu']."'";
												$p_persona2=$conexion->createCommand($sql)->queryRow();
												$sql="SELECT * 
													  FROM colegio 
													  WHERE coleg_codig='".$roles['coleg_codig']."'";
												$colegio=$conexion->createCommand($sql)->queryRow();
												$sql="INSERT INTO colegio_contacto(coleg_codig, perso_codig, ccole_tipo, ccole_tofic, ccole_tcelu, ccole_curso, usuar_codig, ccole_fcrea, ccole_hcrea) 
													  VALUES ('".$colegio['coleg_codig']."','".$p_persona1['perso_codig']."','1','".$roles['cont1_tofic']."','".$roles['cont1_tcelu']."','".$roles['cont1_curso']."','".$roles['usuar_codig']."', '".date('Y-m-d')."', '".date('H:i:s')."')";
												$res7=$conexion->createCommand($sql)->execute();
												$sql="INSERT INTO colegio_contacto(coleg_codig, perso_codig, ccole_tipo, ccole_tofic, ccole_tcelu, ccole_curso, usuar_codig, ccole_fcrea, ccole_hcrea) 
													  VALUES ('".$colegio['coleg_codig']."','".$p_persona2['perso_codig']."','2','".$roles['cont2_tofic']."','".$roles['cont2_tcelu']."','".$roles['cont2_curso']."','".$roles['usuar_codig']."', '".date('Y-m-d')."', '".date('H:i:s')."')";
												$res8=$conexion->createCommand($sql)->execute();
												if($res7 and $res8){
														
														$transaction->commit();
														$vista='../registro/datos/verificar_correo';
														$datos['p_persona']=$p_persona1;
														$datos['estatus']=$estat;
														$asunto='Polerones Tiempo | Verificación de Registro';
														$destinatario=$usuario['usuar_login'];
														$this->funciones->enviarCorreo($vista,$datos,$asunto,$destinatario);
													  	
													$msg=array('success'=>'true','msg'=>'Registro Aprobado correctamente');
												}else{
													$transaction->rollBack();
													$msg=array('success'=>'false','msg'=>'Error al asociar los contactos al colegio');	
												}
												
											/*}else{
												$transaction->rollBack();
												$msg=array('success'=>'false','msg'=>'Error al actualizar el Colegio');	
											}*/
										/*}else{
											$transaction->rollBack();
											$msg=array('success'=>'false','msg'=>'Error al actualizar el Contacto 2');	
										}*/
									/*}else{
										$transaction->rollBack();
										$msg=array('success'=>'false','msg'=>'Error al actualizar el Contacto 1');	
									}*/
									
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al actualizar el Usuario');	
								}
							}else{
							
								$vista='../registro/datos/verificar_correo';
								$datos['p_persona']=$p_persona1;
								$datos['estatus']=$estat;
								$asunto='Polerones Tiempo | Verificación de Registro';
								$destinatario=$usuario['usuar_login'];
								$this->funciones->enviarCorreo($vista,$datos,$asunto,$destinatario);
								$transaction->commit();
								$msg=array('success'=>'true','msg'=>'Registro rechazado correctamente');
							}
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar el Registro');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al registrar el movimiento');	
					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Registro no existe');
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			
			$sql="SELECT * 
			  FROM registro  a
			  WHERE a.regis_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('verificar', array('roles' => $roles));
		}
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}