<?php

class FacturaController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->redirect(Yii::app()->request->baseUrl.'/Configuracion/factura/listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['nfact']){
			$nfact=mb_strtoupper($_POST['nfact']);
			$condicion.="a.factu_nfact like '%".$nfact."%' ";
			$con++;
		}
		if($_POST['banco']){
			$banco=mb_strtoupper($_POST['banco']);
			$condicion.="a.banco_codig like '%".$banco."%' ";
			$con++;
		}
		if($_POST['efact']){
			$efact=mb_strtoupper($_POST['efact']);
			$condicion.="a.efact_codig like '%".$efact."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM factura  a
			  WHERE a.factu_codig ='".$_GET['c']."'";
		$factura=$conexion->createCommand($sql)->queryRow();
		$sql="SELECT * 
			  FROM cliente  a
			  WHERE a.clien_codig ='".$factura['clien_codig']."'";
		$cliente=$conexion->createCommand($sql)->queryRow();
		$sql="SELECT a.* , b.inven_descr, c.tprod_descr
				  FROM factura_producto  a
				  JOIN inventario b ON (a.inven_codig = b.inven_codig)
				  JOIN p_producto_tipo c ON (a.tprod_codig=c.tprod_codig)
				  WHERE a.factu_codig ='".$factura['factu_codig']."'
				  AND a.tprod_codig='1'
				  UNION
				  SELECT a.* , b.servi_descr inven_descr, c.tprod_descr
				  FROM factura_producto  a
				  JOIN p_servicios b ON (a.inven_codig = b.servi_codig)
				  JOIN p_producto_tipo c ON (a.tprod_codig=c.tprod_codig)
				  WHERE a.factu_codig ='".$factura['factu_codig']."'
				  AND a.tprod_codig='2'";
		$productos=$conexion->createCommand($sql)->queryAll();
		$this->render('consultar', array('factura' => $factura,'cliente' => $cliente,'productos' => $productos));
	}
	public function actionRegistrar()
	{
		if($_POST){
			
			//ClIENTE
			$clien=mb_strtoupper($_POST["clien"]);
			$denom=mb_strtoupper($_POST["denom"]);
			$tclie=mb_strtoupper($_POST["tclie"]);
			$tdocu=mb_strtoupper($_POST["tdocu"]);
			$ndocu=mb_strtoupper($_POST["ndocu"]);
			$ccont=mb_strtoupper($_POST["ccont"]);
			$corre=mb_strtoupper($_POST["corre"]);
			$telef=mb_strtoupper($_POST["telef"]);
			$direc=mb_strtoupper($_POST["direc"]);
			$obser=mb_strtoupper($_POST["obser"]);
			//FACTURA
			$nfact=mb_strtoupper($_POST["nfact"]);
			$ncont=mb_strtoupper($_POST["ncont"]);
			$traba=mb_strtoupper($_POST["traba"]);
			$tvent=mb_strtoupper($_POST["tvent"]);
			$femis=mb_strtoupper($this->funciones->TransformarFecha_bd($_POST["femis"]));
			$descu=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["descu"]));
			$ptipo=mb_strtoupper($_POST["ptipo"]);
			$efact=mb_strtoupper($_POST["efact"]);
			$efpag=mb_strtoupper($_POST["efpag"]);
			$banco=mb_strtoupper($_POST["banco"]);
			$refer=mb_strtoupper($_POST["refer"]);
			$obser=mb_strtoupper($_POST["obser"]);
			//PRODUCTOS
			foreach ($_POST["produ"] as $key => $value) {
				$producto[$key]["tprod"]=$_POST["tprod"][$key];
				$producto[$key]["produ"]=$_POST["produ"][$key];
				$producto[$key]["cprod"]=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["cprod"][$key]));
				$producto[$key]["pprod"]=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["pprod"][$key]));
				$producto[$key]["mprod"]=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["mprod"][$key]));
			}
			$mtotal=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["mtotal"]));
			
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				//registrar Cliente
				$sql="SELECT * FROM cliente  WHERE clien_codig ='".$clien."'";
				$cliente=$conexion->createCommand($sql)->queryRow();
				if($cliente){
					$sql="SELECT * FROM cliente WHERE tdocu_codig = '".$tdocu."' and clien_ndocu = '".$ndocu."'";
					$documento=$conexion->createCommand($sql)->queryRow();
					if(!$documento or ($tdocu==$cliente['tdocu_codig'] and $ndocu==$cliente['clien_ndocu'])){
								$sql="UPDATE cliente 
									  SET tclie_codig='".$tclie."',
										  tdocu_codig='".$tdocu."',
										  clien_ndocu='".$ndocu."',
										  clien_denom='".$denom."',
										  clien_ccont='".$ccont."',
										  clien_corre='".$corre."',
										  clien_telef='".$telef."',
										  clien_direc='".$direc."',
										  clien_obser='".$obser."'
									  WHERE clien_codig ='".$clien."'";
							$res1=$conexion->createCommand($sql)->execute();
							/*if($res1){
									$msg=array('success'=>'true','msg'=>'Cliente actualizado correctamente');
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al actualizar el Cliente');	
							}*/
							$msg=array('success'=>'true','msg'=>'Cliente actualizado correctamente');
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Ya existe un cliente con ese numero de documento');
					}
				}else{
					$sql="SELECT * FROM cliente WHERE tdocu_codig = '".$tdocu."' and clien_ndocu = '".$ndocu."'";
					$documento=$conexion->createCommand($sql)->queryRow();
					if(!$documento){
						/*$sql="SELECT * FROM cliente WHERE clien_ccont = '".$ccont."'";
						$contrato=$conexion->createCommand($sql)->queryRow();
						if(!$contrato){*/
							$sql="INSERT INTO cliente(tclie_codig, tdocu_codig, clien_ndocu, clien_denom, clien_ccont, clien_corre, clien_telef, clien_direc, clien_obser, usuar_codig, clien_fcrea, clien_hcrea) 
								VALUES ('".$tclie."','".$tdocu."','".$ndocu."','".$denom."','".$ccont."','".$corre."','".$telef."','".$direc."','".$obser."','".Yii::app()->user->id['usuario']['codigo']."','".date('Y-m-d')."','".date('H:i:s')."')";
							$res1=$conexion->createCommand($sql)->execute();
							if($res1){
								$sql="SELECT clien_codig FROM cliente WHERE tdocu_codig = '".$tdocu."' and clien_ndocu = '".$ndocu."'";
								$cliente=$conexion->createCommand($sql)->queryRow();
								$clien=$cliente['clien_codig'];
								$msg=array('success'=>'true','msg'=>'Cliente guardado correctamente');	
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al guardar el Cliente');	
							}	
						/*}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Ya existe un Cliente con ese Número de Contrato');
						}*/
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Ya existe un cliente con ese número de documento');
					}
				}

				if ($msg['success']=='true') {
					$sql="SELECT * FROM factura WHERE factu_nfact = '".$nfact."'";
					$factura=$conexion->createCommand($sql)->queryRow();
					if(!$factura){
						/*preparar montos de la factura*/
						
						$sql="SELECT * FROM trabajador WHERE traba_codig='".$traba."'";
						$trabajador=$conexion->createCommand($sql)->queryRow();

						$sql="SELECT * FROM impuesto_iva WHERE iva_codig=(SELECT max(iva_codig) FROM impuesto_iva)";
						$iva=$conexion->createCommand($sql)->queryRow();
						


						$comision=($mtotal*$trabajador['traba_comis'])/100;
						$impuesto=($mtotal*$iva['iva_value'])/100;
						
						/*$sql="INSERT INTO factura(factu_nfact, traba_codig, clien_codig, tvent_codig, factu_femis, factu_dcred, factu_fentr, factu_freci, factu_fvenc, factu_fpago, factu_desc, factu_monto, ptipo_codig, factu_reten, factu_obser, usuar_codig, factu_fcrea, factu_hcrea) 
								VALUES ('".$nfact."','".$traba."','".$clien."','".$tvent."','".$femis."','".$dcred."','".$fentr."','".$freci."','".$fvenc."','".$fpago."','".$descu."','".$monto."','".$ptipo."','".$retens."','".$obser."','".Yii::app()->user->id['usuario']['codigo']."','".date('Y-m-d')."','".date('H:i:s')."')";*/
						$sql="INSERT INTO factura(factu_nfact, factu_ncont, traba_codig, clien_codig, tvent_codig, factu_femis, factu_desc, factu_monto, factu_impue, factu_comis, iva_codig, ptipo_codig, efpag_codig, efact_codig, banco_codig, factu_refer, factu_obser, usuar_codig, factu_fcrea, factu_hcrea)
								VALUES ('".$nfact."','".$ncont."','".$traba."','".$clien."','".$tvent."','".$femis."','".$descu."','".$mtotal."','".$impuesto."','".$comision."','".$iva['iva_codig']."','".$ptipo."','".$efpag."','1','".$banco."','".$refer."','".$obser."','".Yii::app()->user->id['usuario']['codigo']."','".date('Y-m-d')."','".date('H:i:s')."')";
						/*echo $sql;
						exit;*/
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							
							$msg=array('success'=>'true','msg'=>'Factura guardada correctamente');
							$sql="SELECT * from factura WHERE factu_codig=(SELECT max(factu_codig) FROM factura)
	";
							$factura=$conexion->createCommand($sql)->queryRow();

					$factura=$conexion->createCommand($sql)->queryRow();	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al guardar la Factura');
						}	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Ya existe una factura con ese número');
					}
					if ($msg['success']=='true') {
						if($factura){
							foreach ($producto as $key => $value) {


								$sql="INSERT INTO factura_producto(factu_codig, tprod_codig, inven_codig, fprod_canti, fprod_preci, fprod_monto, fprod_obser, usuar_codig, fprod_fcrea, fprod_hcrea) 
								VALUES ( '".$factura['factu_codig']."', '".$value['tprod']."', '".$value['produ']."', '".$value['cprod']."', '".$value['pprod']."', '".$value['mprod']."', '".$value['obser']."', '".Yii::app()->user->id['usuario']['codigo']."','".date('Y-m-d')."','".date('H:i:s')."')";
								$res1=$conexion->createCommand($sql)->execute();
								if($res1){
									$msg=array('success'=>'true','msg'=>'Producto guardado correctamente');	
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al guardar el producto');
									echo json_encode($msg);;
									exit();
								}	
							}
							$transaction->commit();
							
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'La factura no existe');
						}
					}
				}

			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codig=mb_strtoupper($_POST["codig"]);
			$nfact=mb_strtoupper($_POST["nfact"]);
			$ncont=mb_strtoupper($_POST["ncont"]);
			$traba=mb_strtoupper($_POST["traba"]);
			$tvent=mb_strtoupper($_POST["tvent"]);
			$femis=mb_strtoupper($this->funciones->TransformarFecha_bd($_POST["femis"]));
			$descu=mb_strtoupper($this->funciones->TransformarMonto_bd($_POST["descu"]));
			$ptipo=mb_strtoupper($_POST["ptipo"]);
			$efact=mb_strtoupper($_POST["efact"]);
			$efpag=mb_strtoupper($_POST["efpag"]);
			$banco=mb_strtoupper($_POST["banco"]);
			$refer=mb_strtoupper($_POST["refer"]);
			$obser=mb_strtoupper($_POST["obser"]);

			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM factura  WHERE factu_codig ='".$codig."'";
				$factura=$conexion->createCommand($sql)->queryRow();
				if($factura){
					$sql="SELECT * FROM factura WHERE factu_nfact = '".$nfact."'";
					$documento=$conexion->createCommand($sql)->queryRow();
					if(!$documento or $nfact==$factura['factu_nfact']){
						$sql="SELECT * FROM p_trabajador WHERE traba_codig='".$traba."'";
						$trabajador=$conexion->createCommand($sql)->queryRow();

						$sql="SELECT * FROM impuesto_iva WHERE iva_codig=(SELECT max(iva_codig) FROM impuesto_iva)";
						$iva=$conexion->createCommand($sql)->queryRow();
						
						$mtotal=$factura['factu_monto'];

						$comision=($mtotal*$trabajador['traba_comis'])/100;
						$impuesto=($mtotal*$iva['iva_value'])/100;

						$sql="UPDATE factura 
							  SET factu_nfact = '".$nfact."',
							  	  factu_ncont = '".$ncont."',
								  traba_codig = '".$traba."',
								  tvent_codig = '".$tvent."',
								  factu_femis = '".$femis."',
								  factu_desc = '".$descu."',
								  factu_comis = '".$comision."',
								  factu_impue = '".$impuesto."',
								  iva_codig = '".$$iva['iva_codig']."',
								  ptipo_codig = '".$ptipo."',
								  banco_codig = '".$banco."',
								  efpag_codig = '".$efpag."',
								  factu_refer = '".$refer."',
								  factu_obser = '".$obser."'
							  WHERE factu_codig ='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Factura actualizada correctamente');
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar la Factura');	
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Ya existe un cliente con ese numero de documento');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'La Factura no existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
				  FROM factura  a
				  WHERE a.factu_codig ='".$_GET['c']."'";
			$factura=$conexion->createCommand($sql)->queryRow();
			$sql="SELECT * 
				  FROM cliente  a
				  WHERE a.clien_codig ='".$factura['clien_codig']."'";
			$cliente=$conexion->createCommand($sql)->queryRow();
			$sql="SELECT a.* , b.inven_descr, c.tprod_descr
				  FROM factura_producto  a
				  JOIN inventario b ON (a.inven_codig = b.inven_codig)
				  JOIN p_producto_tipo c ON (a.tprod_codig=c.tprod_codig)
				  WHERE a.factu_codig ='".$factura['factu_codig']."'
				  AND a.tprod_codig='1'
				  UNION
				  SELECT a.* , b.servi_descr inven_descr, c.tprod_descr
				  FROM factura_producto  a
				  JOIN p_servicios b ON (a.inven_codig = b.servi_codig)
				  JOIN p_producto_tipo c ON (a.tprod_codig=c.tprod_codig)
				  WHERE a.factu_codig ='".$factura['factu_codig']."'
				  AND a.tprod_codig='2'";
			$productos=$conexion->createCommand($sql)->queryAll();
			$this->render('modificar', array('factura' => $factura,'cliente' => $cliente,'productos' => $productos));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM factura_producto WHERE factu_codig='".$codig."'";
				$prod=$conexion->createCommand($sql)->queryRow();
				if(!$prod){
					$sql="SELECT * FROM factura WHERE factu_codig='".$codig."'";
					$roles=$conexion->createCommand($sql)->queryRow();
					if($roles){

						$sql="DELETE FROM factura  WHERE factu_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Factura eliminada correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar la Factura');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'La Factura no existe');
					}

				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No se puede eliminar debido a, que tiene productos asociados');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
				  FROM factura  a
				  WHERE a.factu_codig ='".$_GET['c']."'";
			$factura=$conexion->createCommand($sql)->queryRow();
			$sql="SELECT * 
				  FROM cliente  a
				  WHERE a.clien_codig ='".$factura['clien_codig']."'";
			$cliente=$conexion->createCommand($sql)->queryRow();
			$sql="SELECT a.* , b.inven_descr, c.tprod_descr
				  FROM factura_producto  a
				  JOIN inventario b ON (a.inven_codig = b.inven_codig)
				  JOIN producto_tipo c ON (a.tprod_codig=c.tprod_codig)
				  WHERE a.factu_codig ='".$factura['factu_codig']."'
				  AND a.tprod_codig='1'
				  UNION
				  SELECT a.* , b.servi_descr inven_descr, c.tprod_descr
				  FROM factura_producto  a
				  JOIN servicios b ON (a.inven_codig = b.servi_codig)
				  JOIN producto_tipo c ON (a.tprod_codig=c.tprod_codig)
				  WHERE a.factu_codig ='".$factura['factu_codig']."'
				  AND a.tprod_codig='2'";

			$productos=$conexion->createCommand($sql)->queryAll();
			$this->render('eliminar', array('factura' => $factura,'cliente' => $cliente,'productos' => $productos));
		}
	}
	public function actionAprobarFacturas()
	{
		if($_POST){
			$id=$_POST['id'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				foreach ($id as $key => $codig) {
					$sql="SELECT * FROM factura WHERE factu_codig='".$codig."'";
					$salida=$conexion->createCommand($sql)->queryRow();
					if($salida){
						$sql="UPDATE factura
						  	  SET efact_codig = '2'
							  WHERE factu_codig ='".$codig."'";
				
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$msg=array('success'=>'true','msg'=>'Factura Actualizada correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar la Factura');
							echo json_encode($msg);;
							exit();
						}	

					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'La Factura no existe');
						echo json_encode($msg);;
						exit();
					}

					if($msg['success']="true"){
						$sql="SELECT * FROM factura_producto WHERE factu_codig='".$codig."'";
						$productos=$conexion->createCommand($sql)->queryAll();
						foreach ($productos as $key => $producto) {

							$preci=$this->funciones->MontoCambioReverso($producto['inven_codig'],$producto['fprod_preci'],$producto['tprod_codig']);
							if($producto['tprod_codig']=='2'){
								$sql="UPDATE servicios
						  	  		  	SET servi_preci = '".$preci."'
							  		  WHERE servi_codig ='".$producto['inven_codig']."'";
							}else {
								$sql="UPDATE inventario
						  	  		  	SET inven_canti = inven_canti-'".$producto['fprod_canti']."',
						  	  	  			inven_punid = '".$preci."'
							  		  WHERE inven_codig ='".$producto['inven_codig']."'";
							}
							
							$res1=$conexion->createCommand($sql)->execute();
							if($res1){
								$msg=array('success'=>'true','msg'=>'Factura Actualizada correctamente');	
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al actualizar el Producto');
								echo json_encode($msg);
								exit();
							}	
						}
						
					}
					
				}
				if($msg['success']="true"){
					$transaction->commit();
				}
				
				
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$this->render('aprobar');

		}
	}
	public function actionReversarAprobarFacturas()
	{
		if($_POST){
			
			$id=$_POST['id'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				foreach ($id as $key => $codig) {
					$sql="SELECT * FROM factura WHERE factu_codig='".$codig."'";
					$salida=$conexion->createCommand($sql)->queryRow();
					if($salida){
						$sql="UPDATE factura
						  	  SET efact_codig = '1'
							  WHERE factu_codig ='".$codig."'";
				
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$msg=array('success'=>'true','msg'=>'Factura Actualizada correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar la Factura');
							echo json_encode($msg);;
							exit();
						}	

					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'La Factura no existe');
						echo json_encode($msg);;
						exit();
					}

					if($msg['success']="true"){
						$sql="SELECT * FROM factura_producto WHERE factu_codig='".$codig."'";
						$productos=$conexion->createCommand($sql)->queryAll();
						foreach ($productos as $key => $producto) {
							if($producto['tprod_codig']=='1'){
								$sql="UPDATE inventario
							  	  SET inven_canti = inven_canti+'".$producto['fprod_canti']."'
								  WHERE inven_codig ='".$producto['inven_codig']."'";
								$res1=$conexion->createCommand($sql)->execute();
								if($res1){
									$msg=array('success'=>'true','msg'=>'Factura Actualizada correctamente');	
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al actualizar el Producto');
									echo json_encode($msg);
									exit();
								}	
							}
						}
						
					}
					
				}
				if($msg['success']="true"){
					$transaction->commit();
				}
				
				
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$this->render('aprobarreversar');

		}
	}
	public function actionAnularFacturas()
	{
		if($_POST){
			
			$id=$_POST['id'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				foreach ($id as $key => $codig) {
					$sql="SELECT * FROM factura WHERE factu_codig='".$codig."'";
					$salida=$conexion->createCommand($sql)->queryRow();
					if($salida){
						$sql="UPDATE factura
						  	  SET efact_codig = '3'
							  WHERE factu_codig ='".$codig."'";
				
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$msg=array('success'=>'true','msg'=>'Factura Actualizada correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar la Factura');
							echo json_encode($msg);;
							exit();
						}	

					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'La Factura no existe');
						echo json_encode($msg);;
						exit();
					}
					
				}
				if($msg['success']="true"){
					$transaction->commit();
				}
				
				
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$this->render('anular');

		}
	}
	public function actionReversarAnularFacturas()
	{
		if($_POST){
			
			$id=$_POST['id'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				foreach ($id as $key => $codig) {
					$sql="SELECT * FROM factura WHERE factu_codig='".$codig."'";
					$salida=$conexion->createCommand($sql)->queryRow();
					if($salida){
						$sql="UPDATE factura
						  	  SET efact_codig = '1'
							  WHERE factu_codig ='".$codig."'";
				
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$msg=array('success'=>'true','msg'=>'Factura Actualizada correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar la Factura');
							echo json_encode($msg);;
							exit();
						}	

					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'La Factura no existe');
						echo json_encode($msg);;
						exit();
					}
					
				}
				if($msg['success']="true"){
					$transaction->commit();
				}
				
				
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$this->render('anularreversar');

		}
	}
	
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}