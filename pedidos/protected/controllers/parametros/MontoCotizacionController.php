<?php

class MontoCotizacionController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->redirect(Yii::app()->request->baseUrl.'/parametros/tipoempresa/listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descripcion']){
			$descripcion=mb_strtoupper($_POST['descripcion']);
			$condicion.="a.mcoti_descr like '%".$descripcion."%' ";
			$con++;
		}
		if($_POST['value']){
			if($con>0){
				$condicion.="AND ";
			}
			$value=mb_strtoupper($_POST['value']);
			$condicion.="a.mcoti_value like '%".$value."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM p_monto_cotizacion a
			  WHERE a.mcoti_codig ='".$_GET['c']."'";
		$roles=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('roles' => $roles));
	}
	public function actionRegistrar()
	{
		if($_POST){
			/*var_dump($_POST);
			exit();*/
			$descr=mb_strtoupper($_POST['descr']);
			$value=$this->funciones->transformarMonto_bd(mb_strtoupper($_POST['value']));
			$estat=mb_strtoupper($_POST['estat']);

			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM p_monto_cotizacion WHERE pesta_codig = '1'";
				$roles=$conexion->createCommand($sql)->queryRow();
				if(!$roles){
					/*$sql="SELECT * FROM p_monto_cotizacion WHERE mcoti_value = '".$value."'";
					$roles=$conexion->createCommand($sql)->queryRow();
					if(!$roles){*/
						$sql="INSERT INTO p_monto_cotizacion(mcoti_value, pesta_codig, usuar_codig, mcoti_fcrea, mcoti_hcrea) 
							VALUES ('".$value."','".$estat."','".$usuar."','".$fecha."','".$hora."')";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Monto guardado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al guardar La Monto Cotización');	
						}
					/*}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Valor ya existe');
					}*/
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Unicamente puede estar un monto activo');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$descr=mb_strtoupper($_POST['descr']);
			$value=$this->funciones->transformarMonto_bd(mb_strtoupper($_POST['value']));
			$estat=mb_strtoupper($_POST['estat']);

			if($contra==$ccontra){
				$conexion=Yii::app()->db;
				$transaction=$conexion->beginTransaction();
				try{
					$sql="SELECT * FROM p_monto_cotizacion WHERE mcoti_codig ='".$codig."'";
					$cotiz=$conexion->createCommand($sql)->queryRow();
					if($cotiz){
						$sql="SELECT * 
							  FROM p_monto_cotizacion 
							  WHERE mcoti_codig<>'".$codig."' 
							    AND pesta_codig = '1'";
						$rol=$conexion->createCommand($sql)->queryRow();
						if(!$rol){
							$sql="SELECT * FROM p_monto_cotizacion WHERE mcoti_value = '".$value."'";
							$roles=$conexion->createCommand($sql)->queryRow();
							if(!$roles or $cotiz['mcoti_value']!=$value or $cotiz['pesta_codig']!=$estat){
								$sql="UPDATE p_monto_cotizacion
									  SET mcoti_value='".$value."', 
									  	  pesta_codig='".$estat."' 
									  WHERE mcoti_codig='".$codig."'";
								$res1=$conexion->createCommand($sql)->execute();
								if($res1){
									$transaction->commit();
										$msg=array('success'=>'true','msg'=>'Monto Cotización actualizado correctamente');
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al actualizar La Monto Cotización');	
								}
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'El Valor ya existe');
							}
							
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Unicamente puede estar un monto activo');
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'La Monto Cotización no existe');
					}
				}catch(Exception $e){
					var_dump($e);
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error al verificar la información');
				}
			}else{
				$msg=array('success'=>'false','msg'=>'La contraseña y su confirmación no son iguales');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			
			$sql="SELECT * 
			  FROM p_monto_cotizacion a
			  WHERE a.mcoti_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('roles' => $roles));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				
					$sql="SELECT * FROM p_monto_cotizacion WHERE mcoti_codig='".$codig."'";
					$roles=$conexion->createCommand($sql)->queryRow();
					if($roles){

						$sql="DELETE FROM p_monto_cotizacion WHERE mcoti_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Monto Cotización eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Monto de la Cotización');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'La Monto Cotización no existe');
					}
				
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM p_monto_cotizacion a
			  WHERE a.mcoti_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('roles' => $roles));
		}
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}