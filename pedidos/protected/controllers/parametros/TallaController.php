<?php

class TallaController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->redirect('listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descripcion']){
			$descripcion=mb_strtoupper($_POST['descripcion']);
			$condicion.="a.talla_descr like '%".$descripcion."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM pedido_talla a
			  WHERE a.talla_codig ='".$_GET['c']."'";
			/*                                  var_dump($talla);
exit;*/
		$talla=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('talla' => $talla));
	}
	public function actionRegistrar()
	{
		if($_POST){
			/*var_dump($_POST);
			exit();*/
			$descr=mb_strtoupper($_POST['descr']);
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM pedido_talla WHERE talla_descr = '".$descr."'";
				$talla=$conexion->createCommand($sql)->queryRow();
				if(!$talla){
					$sql="INSERT INTO pedido_talla(talla_descr, usuar_codig, talla_fcrea, talla_hcrea)
						VALUES ('".$descr."', '".$usuar."', '".$fecha."', '".$hora."')";
					$res1=$conexion->createCommand($sql)->execute();
					if($res1){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Talla guardado correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al guardar el Talla');	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Talla ya existe');
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$descr=mb_strtoupper($_POST['descr']);
			if($contra==$ccontra){
				$conexion=Yii::app()->db;
				$transaction=$conexion->beginTransaction();
				try{
					$sql="SELECT * FROM pedido_talla WHERE talla_codig ='".$codig."'";
					$talla=$conexion->createCommand($sql)->queryRow();
					if($talla){
						$sql="SELECT * FROM pedido_talla WHERE talla_descr='".$descr."'";
						$rol=$conexion->createCommand($sql)->queryRow();
						if(!$rol){
								$sql="UPDATE pedido_talla
								  SET talla_descr='".$descr."' 
								  WHERE talla_codig='".$codig."'";
							$res1=$conexion->createCommand($sql)->execute();
							if($res1){
								$transaction->commit();
									$msg=array('success'=>'true','msg'=>'Talla actualizado correctamente');
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al actualizar el Talla');	
							}
							
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'El Talla ya esta registrado');
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Talla no existe');
					}
				}catch(Exception $e){
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error al verificar la información');
				}
			}else{
				$msg=array('success'=>'false','msg'=>'La contraseña y su confirmación no son iguales');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_talla a
			  WHERE a.talla_codig ='".$_GET['c']."'";
			$talla=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('talla' => $talla));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM  pedido_pedidos_detalle_listado WHERE talla_codig='".$codig."'";
				$detalle=$conexion->createCommand($sql)->queryRow();
				if(!$detalle){
					$sql="SELECT * FROM  pedido_talla WHERE talla_codig='".$codig."'";
					$usuario=$conexion->createCommand($sql)->queryRow();
					if($usuario){

						$sql="DELETE FROM pedido_talla WHERE talla_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Talla eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar Talla');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Talla no existe');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No se puede eliminar, esta asociada a un pedido');
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM pedido_talla a
			  WHERE a.talla_codig ='".$_GET['c']."'";
			$talla=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('talla' => $talla));
		}
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}