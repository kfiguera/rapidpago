<?php

class NotificacionesEstatusController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->redirect('listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descripcion']){
			$descripcion=mb_strtoupper($_POST['descripcion']);
			$condicion.="a.estat_descr like '%".$descripcion."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM p_notificacion_estatus a
			  WHERE a.enoti_codig ='".$_GET['c']."'";
			
		$estatus=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('estatus' => $estatus));
	}
	public function actionRegistrar()
	{
		if($_POST){
			/*var_dump($_POST);
			exit();*/
			$estat=mb_strtoupper($_POST['estat']);
			$pesta=mb_strtoupper($_POST['pesta']);
			$titul=mb_strtoupper($_POST['titul']);
			$mensa=mb_strtoupper($_POST['mensa']);
			//Datos de Auditoria
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM p_notificacion_estatus WHERE enoti_titul = '".$descr."'";
				$estatus=$conexion->createCommand($sql)->queryRow();
				if(!$estatus){
					$sql="INSERT INTO p_notificacion_estatus(enoti_titul, enoti_descr, estat_codig, pesta_codig, usuar_codig, enoti_fcrea, enoti_hcrea) 
					VALUES ('".$titul."','".$mensa."','".$estat."','".$pesta."','".$usuar."','".$fecha."','".$hora."')";
					
					$res1=$conexion->createCommand($sql)->execute();
					if($res1){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Notificaciones por Estatus guardado correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al guardar el Notificaciones por Estatus');	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Notificaciones por Estatus ya existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$estat=mb_strtoupper($_POST['estat']);
			$pesta=mb_strtoupper($_POST['pesta']);
			$titul=mb_strtoupper($_POST['titul']);
			$mensa=mb_strtoupper($_POST['mensa']);
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM p_notificacion_estatus WHERE enoti_codig ='".$codig."'";
				$estatus=$conexion->createCommand($sql)->queryRow();
				if($estatus){
					$sql="SELECT * FROM p_notificacion_estatus WHERE enoti_titul ='".$titul."'";
					$rol=$conexion->createCommand($sql)->queryRow();
					if(!$rol or $estatus['enoti_descr']!=$mensa){
							$sql="UPDATE p_notificacion_estatus
							  SET estat_codig='".$estat."',
							  	  pesta_codig='".$pesta."',
							  	  enoti_descr='".$mensa."',
							  	  enoti_titul='".$titul."'
							  WHERE enoti_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
								$msg=array('success'=>'true','msg'=>'Notificaciones por Estatus actualizado correctamente');
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar el Notificaciones por Estatus');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Notificaciones por Estatus ya esta registrado');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Notificaciones por Estatus no existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM p_notificacion_estatus a
			  WHERE a.enoti_codig ='".$_GET['c']."'";
			$estatus=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('estatus' => $estatus));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM seguridad_usuarios WHERE estat_codig='".$codig."'";
				$usuario=$conexion->createCommand($sql)->queryRow();
				if(!$usuario){
					$sql="SELECT * FROM rd_preregistro_estatus WHERE estat_codig='".$codig."'";
					$usuario=$conexion->createCommand($sql)->queryRow();
					if($usuario){

						$sql="DELETE FROM rd_preregistro_estatus WHERE estat_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Notificaciones por Estatus eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar Notificaciones por Estatus');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Notificaciones por Estatus no existe');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'No se puede eliminar, debido a aqe esta asociado con un usuario');
				}	
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM rd_preregistro_estatus a
			  WHERE a.estat_codig ='".$_GET['c']."'";
			$estatus=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('estatus' => $estatus));
		}
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}