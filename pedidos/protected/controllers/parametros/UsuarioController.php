<?php
class UsuarioController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		//$this->render('index');
		$this->redirect('listado');
	}
	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['p_nacionalidad']){
			$condicion.="a.nacio_value = '".$_POST['p_nacionalidad']."' ";
			$con++;
		}
		if($_POST['cedula']){
			if($con>0){
				$condicion.="AND ";
			}
			$condicion.="a.perso_cedul = '".$_POST['cedula']."' ";
			$con++;
		}
		if($_POST['nombre']){
			$nombre=mb_strtoupper($_POST['nombre']);
			if($con>0){
				$condicion.="AND ";
			}
			$condicion.="(a.perso_pnomb like '%".$nombre."%' or a.perso_snomb like '%".$nombre."%') ";
			$con++;
		}
		if($_POST['apellido']){
			$apellido=mb_strtoupper($_POST['apellido']);
			if($con>0){
				$condicion.="AND ";
			}
			$condicion.="(a.perso_papel like '%".$apellido."%' or a.perso_sapel like '%".$apellido."%') ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		//var_dump($condicion);
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM seguridad_usuarios a
			  WHERE a.usuar_codig ='".$_GET['c']."'";
		$usuario=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('usuario' => $usuario));
	}
	public function actionRegistrar()
	{
		if($_POST){
			//PERSONA
			$nacio = mb_strtoupper($_POST['nacio']);
			$cedul = $this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['cedula']));
			$pnomb = mb_strtoupper($_POST['pnomb']);
			$snomb = mb_strtoupper($_POST['snomb']);
			$papel = mb_strtoupper($_POST['papel']);
			$sapel = mb_strtoupper($_POST['sapel']);
			$fnaci = $this->funciones->TransformarFecha_bd($_POST['fnaci']);
            $ecivi = mb_strtoupper($_POST['ecivi']);
            $gener = mb_strtoupper($_POST['gener']);
            //USUARIO
			$perso = $_POST['perso'];
			$corre = $_POST['corre'];
			$ccorre = $_POST['ccorre'];
			$contra = $_POST['contra'];
			$ccontra = $_POST['ccontra'];
			$nivel = $_POST['nivel'];
			$ccontra = $_POST['ccontra'];

			$perfil = $_POST['perfil'];
			$asigna = $_POST['asigna'];
			$organ = $_POST['organ'];
			$obser = mb_strtoupper($_POST['obser']);
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			if($corre==$ccorre){
				if($contra==$ccontra){
					try{
						//VERIFICAR SI EXISTE LA PERSONA
						$sql="SELECT * FROM p_persona WHERE nacio_value ='".$nacio."' and perso_cedul='".$cedul."'";
						$p_persona=$conexion->createCommand($sql)->queryRow();

						if(!$p_persona){
							//SI NO EXISTE SE GUARDA
							$sql="INSERT INTO p_persona(nacio_value, perso_cedul, perso_pnomb, perso_snomb, perso_papel, perso_sapel, perso_fnaci, gener_value, ecivi_value, perso_obser) 
								VALUES ('".$nacio."','".$cedul."','".$pnomb."','".$snomb."','".$papel."','".$sapel."','".$fnaci."','".$gener."','".$ecivi."','".$obser."')";
							$res1=$conexion->createCommand($sql)->execute();
							if($res1){
								$sql="SELECT * FROM p_persona WHERE nacio_value ='".$nacio."' and perso_cedul='".$cedul."'";
								$p_persona=$conexion->createCommand($sql)->queryRow();	
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al guardar la persona');	
							}	
						}
						
						if($p_persona){
							$perso=$p_persona['perso_codig'];
							//VERIFICAR SI EXISTE LA PERSONA COMO USUARIO
							$sql="SELECT * FROM seguridad_usuarios WHERE perso_codig ='".$perso."'";
							$upers=$conexion->createCommand($sql)->queryRow();
							if(!$upers){
								//VERIFICAR SI EXISTE EL USUARIO
								$sql="SELECT * FROM seguridad_usuarios WHERE usuar_login ='".$corre."'";
								$usuario=$conexion->createCommand($sql)->queryRow();
								if(!$usuario){
									$sql="INSERT INTO seguridad_usuarios(perso_codig, usuar_login, usuar_passw, urole_codig, uesta_codig, spniv_codig, pesta_codig, organ_codig, usuar_obser, usuar_fcrea) 
										VALUES ('".$perso."','".$corre."','".md5($contra)."','".$perfil."',1,'".$nivel."','".$asigna."','".$organ."','".$obser."','".date('Y-m-d')."' )";
									
									$res1=$conexion->createCommand($sql)->execute();
									if($res1){
										$transaction->commit();
										$msg=array('success'=>'true','msg'=>'Usuario guardado correctamente');	
									}else{
										$transaction->rollBack();
										$msg=array('success'=>'false','msg'=>'Error al guardar el Usuario');	
									}
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Usuario ya existe');
								}
								
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'La persona ya tiene un usuario');
							}
						}else{
							$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al guardar la persona');	
						}
					}catch(Exception $e){
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al verificar la información');
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'La Contraseña y su Confirmación no son iguales');	
				}
			}else{
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'El Correo y su Confirmación no son iguales');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codig = $_POST['codig'];
			$contra = $_POST['contra'];
			$ccontra = $_POST['ccontra'];
			$nivel = $_POST['nivel'];
			$estat = $_POST['estat'];
			$perso = $_POST['perso'];
			$obser = $_POST['obser'];
			$perfil = $_POST['perfil'];
			$asigna = $_POST['asigna'];
			$organ = $_POST['organ'];
			if($contra==$ccontra){
				$conexion=Yii::app()->db;
				$transaction=$conexion->beginTransaction();
				try{
					$sql="SELECT * FROM seguridad_usuarios WHERE perso_codig ='".$perso."'";
						$p_persona=$conexion->createCommand($sql)->queryRow();
						$sql="SELECT * FROM seguridad_usuarios WHERE usuar_codig='".$codig."'";
						$usuario=$conexion->createCommand($sql)->queryRow();
						if(!$p_persona or $usuario['perso_codig']==$perso){
							$sql="SELECT * FROM p_persona WHERE perso_codig='".$perso."'";
							$p_persona=$conexion->createCommand($sql)->queryRow();
							if($p_persona){
								$sql="SELECT * FROM seguridad_usuarios WHERE usuar_codig='".$codig."'";
								$usuario=$conexion->createCommand($sql)->queryRow();
								if($usuario){
									$sql="UPDATE seguridad_usuarios 
									SET urole_codig='".$perfil."',";
									if($contra!=null and $contra!='' and $contra!=' '){
										$sql.="usuar_passw='".md5($contra)."',";
									}
									$sql.="uesta_codig='".$estat."',
										  perso_codig='".$perso."',
										  pesta_codig='".$asigna."',
										  organ_codig='".$organ."',
										  spniv_codig='".$nivel."',
										  usuar_obser='".$obser."'
										  WHERE usuar_codig='".$codig."'";
									$res1=$conexion->createCommand($sql)->execute();
									if($res1){
										$transaction->commit();
											$msg=array('success'=>'true','msg'=>'Usuario actualizado correctamente');
									}else{
										$transaction->rollBack();
										$msg=array('success'=>'false','msg'=>'Error al actualizar la persona');	
									}
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'El usuario no existe');	
								}
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'La persona no existe');
							}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'La persona ya tiene un usuario');
					}
				}catch(Exception $e){
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error al verificar la información');
				}
			}else{
				$msg=array('success'=>'false','msg'=>'La contraseña y su confirmación no son iguales');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
				FROM seguridad_usuarios a
				WHERE a.usuar_codig ='".$_GET['c']."'";
			$usuario=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('usuario' => $usuario));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM seguridad_usuarios WHERE usuar_codig='".$codig."'";
				$usuario=$conexion->createCommand($sql)->queryRow();
				if($usuario){

					$sql="DELETE FROM seguridad_usuarios WHERE usuar_codig='".$codig."'";
					$res1=$conexion->createCommand($sql)->execute();
					//echo $sql;
					if($res1){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Usuario eliminado correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al eliminar el usuario');	
					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El usuario no existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
				FROM seguridad_usuarios a
				WHERE a.usuar_codig ='".$_GET['c']."'";
			$usuario=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('usuario' => $usuario));
		}
	}
	public function actionCambiarClave()
	{
		if($_POST){
			$acontra=$_POST['acontra'];
			$contra=$_POST['contra'];
			$ccontra=$_POST['ccontra'];
			$codig=Yii::app()->user->id['usuario']['codigo'];
			if($acontra!=$contra){
				if($contra==$ccontra){
					$conexion=Yii::app()->db;
					$transaction=$conexion->beginTransaction();
					try{
						$sql="SELECT * FROM seguridad_usuarios WHERE usuar_codig='".$codig."'";
						$usuario=$conexion->createCommand($sql)->queryRow();
						if($usuario['usuar_passw']==md5($acontra)){
							if($usuario){
								$sql="UPDATE seguridad_usuarios 
									SET usuar_passw='".md5($contra)."'
									WHERE usuar_codig='".$codig."'";
									
								$res1=$conexion->createCommand($sql)->execute();
						
								if($res1){
									$transaction->commit();
									$msg=array('success'=>'true','msg'=>'Contraseña actualizada correctamente');	
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al actualizar la contraseña');	
								}
						
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'El usuario no existe');
							}
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'La contraseña actual suministrada no es correcta');
						}
					}catch(Exception $e){
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al verificar la información');
					}
				}else{
					$msg=array('success'=>'false','msg'=>'La Nueva Contraseña y su confirmacion no son iguales');
				}
			}else{
				$msg=array('success'=>'false','msg'=>'La Contraseña Nueva no puede ser igual a la actual');
			}
			echo json_encode($msg);
		}else{
			$this->render('cambiarclave', array('usuario' => $usuario));
		}
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}