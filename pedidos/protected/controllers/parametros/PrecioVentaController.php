<?php

class PrecioVentaController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->redirect(Yii::app()->request->baseUrl.'/parametros/tipoempresa/listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descripcion']){
			$descripcion=mb_strtoupper($_POST['descripcion']);
			$condicion.="a.pvent_descr like '%".$descripcion."%' ";
			$con++;
		}
		if($_POST['value']){
			if($con>0){
				$condicion.="AND ";
			}
			$value=mb_strtoupper($_POST['value']);
			$condicion.="a.pvent_value like '%".$value."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM p_precio_venta a
			  WHERE a.pvent_codig ='".$_GET['c']."'";
		$roles=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('roles' => $roles));
	}
	public function actionRegistrar()
	{
		if($_POST){
			/*var_dump($_POST);
			exit();*/
			$inven=mb_strtoupper($_POST['inven']);
			$descr=mb_strtoupper($_POST['descr']);
			$value=$this->funciones->transformarMonto_bd(mb_strtoupper($_POST['value']));
			$estat=mb_strtoupper($_POST['estat']);

			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM p_precio_venta WHERE pesta_codig = '1' and inven_codig='".$inven."'";
				$roles=$conexion->createCommand($sql)->queryRow();
				if(!$roles){
					/*$sql="SELECT * FROM p_precio_venta WHERE pvent_value = '".$value."'";
					$roles=$conexion->createCommand($sql)->queryRow();
					if(!$roles){*/
						$sql="INSERT INTO p_precio_venta(pvent_value, inven_codig, pesta_codig, moned_codig, usuar_codig, pvent_fcrea, pvent_hcrea) 
							VALUES ('".$value."','".$inven."','".$estat."','1','".$usuar."','".$fecha."','".$hora."')";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Monto guardado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al guardar La Precio de Venta');	
						}
					/*}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Valor ya existe');
					}*/
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Unicamente puede estar un monto activo');
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$descr=mb_strtoupper($_POST['descr']);
			$value=$this->funciones->transformarMonto_bd(mb_strtoupper($_POST['value']));
			$estat=mb_strtoupper($_POST['estat']);

			if($contra==$ccontra){
				$conexion=Yii::app()->db;
				$transaction=$conexion->beginTransaction();
				try{
					$sql="SELECT * FROM p_precio_venta WHERE pvent_codig ='".$codig."'";
					$cotiz=$conexion->createCommand($sql)->queryRow();
					if($cotiz){
						$sql="SELECT * 
							  FROM p_precio_venta 
							  WHERE pvent_codig<>'".$codig."' 
							    AND pesta_codig = '1'";
						$rol=$conexion->createCommand($sql)->queryRow();
						if(!$rol){
							$sql="SELECT * FROM p_precio_venta WHERE pvent_value = '".$value."'";
							$roles=$conexion->createCommand($sql)->queryRow();
							if(!$roles or $cotiz['pvent_value']!=$value or $cotiz['pesta_codig']!=$estat){
								$sql="UPDATE p_precio_venta
									  SET pvent_value='".$value."', 
									  	  pesta_codig='".$estat."' 
									  WHERE pvent_codig='".$codig."'";
								$res1=$conexion->createCommand($sql)->execute();
								if($res1){
									$transaction->commit();
										$msg=array('success'=>'true','msg'=>'Precio de Venta actualizado correctamente');
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al actualizar La Precio de Venta');	
								}
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'El Valor ya existe');
							}
							
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Unicamente puede estar un monto activo');
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'La Precio de Venta no existe');
					}
				}catch(Exception $e){
					var_dump($e);
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error al verificar la información');
				}
			}else{
				$msg=array('success'=>'false','msg'=>'La contraseña y su confirmación no son iguales');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			
			$sql="SELECT * 
			  FROM p_precio_venta a
			  WHERE a.pvent_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('roles' => $roles));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				
					$sql="SELECT * FROM p_precio_venta WHERE pvent_codig='".$codig."'";
					$roles=$conexion->createCommand($sql)->queryRow();
					if($roles){

						$sql="DELETE FROM p_precio_venta WHERE pvent_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Precio de Venta eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Monto de la Cotización');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'La Precio de Venta no existe');
					}
				
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM p_precio_venta a
			  WHERE a.pvent_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('roles' => $roles));
		}
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}