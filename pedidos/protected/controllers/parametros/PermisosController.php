<?php

class PermisosController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->redirect(Yii::app()->request->baseUrl.'/parametros/permisos/listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descripcion']){
			$descripcion=mb_strtoupper($_POST['descripcion']);
			$condicion.="a.permi_descr like '%".$descripcion."%' ";
			$con++;
		}
		if($_POST['value']){
			if($con>0){
				$condicion.="AND ";
			}
			$value=mb_strtoupper($_POST['value']);
			$condicion.="a.permi_value like '%".$value."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM permisos a
			  WHERE a.permi_codig ='".$_GET['c']."'";
		$roles=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('roles' => $roles));
	}
	public function actionRegistrar()
	{
		if($_POST){
			
			$urole=mb_strtoupper($_POST['urole']);
			$modul=mb_strtoupper($_POST['modul']);
			$estat=mb_strtoupper($_POST['estat']);
			if($estat=='0'){
				$borra=1;
			}else{
				$borra=0;
			}
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM permisos WHERE urole_codig = '".$urole."' and modul_codig = '".$modul."'";
				$roles=$conexion->createCommand($sql)->queryRow();
				if(!$roles){
					$sql="INSERT INTO permisos(urole_codig, modul_codig, permi_bperm, permi_bborr) VALUES ('".$urole."', '".$modul."', '".$estat."', '".$borra."')";
					$res1=$conexion->createCommand($sql)->execute();
					if($res1){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Permiso guardado correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al guardar El Permiso');	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Permiso ya existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codig=mb_strtoupper($_POST['codig']);
			$urole=mb_strtoupper($_POST['urole']);
			$modul=mb_strtoupper($_POST['modul']);
			$estat=mb_strtoupper($_POST['estat']);
			if($estat=='0'){
				$borra=1;
			}else{
				$borra=0;
			}
			if($contra==$ccontra){
				$conexion=Yii::app()->db;
				$transaction=$conexion->beginTransaction();
				try{
					$sql="SELECT * FROM permisos WHERE permi_codig ='".$codig."'";
					$roles=$conexion->createCommand($sql)->queryRow();
					if($roles){

						$sql="SELECT * FROM permisos WHERE urole_codig = '".$urole."' and modul_codig = '".$modul."'";
						$rol=$conexion->createCommand($sql)->queryRow();
						$sql="SELECT * FROM permisos WHERE permi_codig='".$codig."'";
						$permisos=$conexion->createCommand($sql)->queryRow();
						if(!$rol or ($permisos['modul_codig']==$modul and $permisos['urole_codig']==$urole)){
								$sql="UPDATE permisos
									  SET permi_bperm='".$estat."',
									  	  permi_bborr='".$borra."',
									      modul_codig='".$modul."',
										  urole_codig='".$urole."'   
									  WHERE permi_codig='".$codig."'";
								$res1=$conexion->createCommand($sql)->execute();
								if($res1){
									$transaction->commit();
										$msg=array('success'=>'true','msg'=>'Permiso actualizado correctamente');
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al actualizar El Permiso');	
								}
							
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'El Permiso ya esta registrado');
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Permiso no existe');
					}
				}catch(Exception $e){
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error al verificar la información');
				}
			}else{
				$msg=array('success'=>'false','msg'=>'El contraseña y su confirmación no son iguales');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			
			$sql="SELECT * 
			  FROM permisos a
			  WHERE a.permi_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('roles' => $roles));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				
				$sql="SELECT * FROM permisos WHERE permi_codig='".$codig."'";
				$roles=$conexion->createCommand($sql)->queryRow();
				if($roles){

					$sql="DELETE FROM permisos WHERE permi_codig='".$codig."'";
					$res1=$conexion->createCommand($sql)->execute();
					//echo $sql;
					if($res1){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>'Permiso eliminado correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al eliminar el Tipo de Usuario');	
					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Permiso no existe');
				}
					
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM permisos a
			  WHERE a.permi_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('roles' => $roles));
		}
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}