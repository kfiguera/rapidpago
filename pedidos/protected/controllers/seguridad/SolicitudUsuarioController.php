<?php

class SolicitudUsuarioController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->redirect(Yii::app()->request->baseUrl.'/parametros/tipoempresa/listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['descripcion']){
			$descripcion=mb_strtoupper($_POST['descripcion']);
			$condicion.="a.sprol_descr like '%".$descripcion."%' ";
			$con++;
		}
		if($_POST['value']){
			if($con>0){
				$condicion.="AND ";
			}
			$value=mb_strtoupper($_POST['value']);
			$condicion.="a.sprol_value like '%".$value."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE  ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM solicitud  a
			  WHERE a.solic_codig ='".$_GET['c']."'";
		$solicitud=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('solicitud' => $solicitud));
	}
	public function actionRegistrar()
	{
		if($_POST){

			
			$roles=mb_strtoupper($_POST['usuar']);
			$modul=mb_strtoupper($_POST['modul']);
			$clvip=mb_strtoupper($_POST['clvip']);
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM config_asignacion WHERE srole_codig = '".$roles."' AND orige_codig = '".$modul."' ";
				$config_asignacion=$conexion->createCommand($sql)->queryRow();
				if(!$config_asignacion){
						$sql="INSERT INTO config_asignacion(srole_codig, orige_codig, solic_clvip, usuar_codig, casig_fcrea, casig_hcrea) 
							  VALUES ('".$roles."','".$modul."','".$clvip."','".$usuar."','".$fecha."','".$hora."')";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Configuración guardada correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al guardar La Configuración');	
						}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'La Configuración ya existe');
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$permi=$_POST['permi'];
			$permiso['ingre']=0;
			$permiso['modif']=0;
			$permiso['elimi']=0;
			$permiso['consu']=0;
			$usuar=mb_strtoupper($_POST['usuar']);
			$modul=mb_strtoupper($_POST['modul']);
			$spacc=mb_strtoupper($_POST['spacc']);
			$spniv=mb_strtoupper($_POST['spniv']);
			foreach ($permi as $key => $value) {
				switch ($value) {
					case '0':
						$permiso['consu']='1';
						break;
					case '1':
						$permiso['ingre']='1';
						break;
					case '2':
						$permiso['modif']='1';
						break;
					case '3':
						$permiso['elimi']='1';
						break;
					default:
						# code...
						break;
				}
			}
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM config_asignacion WHERE srole_codig ='".$codig."'";
				$config_asignacion=$conexion->createCommand($sql)->queryRow();
				if($config_asignacion){
					/*$sql="SELECT * FROM config_asignacion WHERE sprol_descr='".$descr."'";
					$rol=$conexion->createCommand($sql)->queryRow();

					if(!$rol or ($config_asignacion['sprol_descr']==$descr )){
					*/	
						
						$sql="UPDATE config_asignacion
							  SET srole_codig = '".$usuar."',
									modul_codig = '".$modul."',
									spacc_codig = '".$spacc."',
									sprol_consu = '".$permiso['consu']."',
									sprol_ingre = '".$permiso['ingre']."',
									sprol_modif = '".$permiso['modif']."',
									sprol_elimi = '".$permiso['elimi']."',
									spniv_codig = '".$spniv."'
							  WHERE srole_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						if($res1){
							$transaction->commit();
								$msg=array('success'=>'true','msg'=>'Configuración actualizado correctamente');
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar El Configuración');	
						}
						
						
					/*}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Configuración ya esta registrado');
					}*/
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Configuración no existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			
			$sql="SELECT * 
			  FROM config_asignacion a
			  WHERE a.srole_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('roles' => $roles));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
					$sql="SELECT * FROM config_asignacion WHERE srole_codig='".$codig."'";
					$roles=$conexion->createCommand($sql)->queryRow();
					if($roles){

						$sql="DELETE FROM config_asignacion WHERE srole_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Configuración eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Configuración');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Configuración no existe');
					}
					
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM config_asignacion a
			  WHERE a.srole_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('roles' => $roles));
		}
	}

	public function actionAsignacion()
	{
		if($_POST){
			$id=$_POST['id'];
			if(!$id){
				$msg=array('success'=>'false','msg'=>'Debe seleccionar al menos una solicitud');
				echo json_encode($msg);
				exit();
			}
			$traba=$_POST['traba'];

			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$estat=8;

			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				foreach ($id as $key => $value) {
					$sql="SELECT * 
						  FROM seguridad_usuarios
					 	  WHERE usuar_codig = '".$traba."'";

					$usuario=$conexion->createCommand($sql)->queryRow();

					$sql="SELECT * 
						  FROM solicitud_usuario a
						  JOIN seguridad_usuarios b ON (a.usuar_codig = b.usuar_codig)
					 	  WHERE solic_codig = '".$value."'
					 	    AND (b.organ_codig = '".$usuario['organ_codig']."' or b.urole_codig = '".$usuario['urole_codig']."')";

					$solicitud=$conexion->createCommand($sql)->queryRow();
					if(!$solicitud){
						
						$sql="INSERT INTO solicitud_usuario(solic_codig, usuar_codig, solus_ucrea, solus_fcrea, solus_hcrea) 
							VALUES ('".$value."','".$traba."','".$usuar."','".$fecha."','".$hora."')";
						$query=$sql;
						$res=$conexion->createCommand($sql)->execute();

						$sql="SELECT * 
							  FROM solicitud_usuario 
							  WHERE solic_codig = '".$value."'
							    AND usuar_codig = '".$traba."'";
						
						$solicitud2=$conexion->createCommand($sql)->queryRow();

					    $traza=$this->funciones->guardarTraza($conexion, 'I', 'solicitud_usuario', json_encode(''),json_encode($solicitud2),$query);				
					}else{
						$transaction->rollBack();
	        			$msg=array('success'=>'false','msg'=>'La solicitud ya se encuentra asignada');
	        			echo json_encode($msg);
	        			exit();
					}
				}		
				$transaction->commit();
				$msg=array('success'=>'true','msg'=>'Solicitudes Asignadas correctamente');    
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$this->render('asignacion');
		}
	}
	public function actionReasignacion()
	{
		if($_POST){
			$id=$_POST['id'];
			if(!$id){
				$msg=array('success'=>'false','msg'=>'Debe seleccionar al menos una solicitud');
				echo json_encode($msg);
				exit();
			}
			$traba=$_POST['traba'];

			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$estat=8;

			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				foreach ($id as $key => $value) {
					
					$sql="SELECT * 
						  FROM solicitud_usuario a
					 	  WHERE solic_codig = '".$value."'
					 	    AND usuar_codig = '".$traba."'";
					$solicitud=$conexion->createCommand($sql)->queryRow();
					$solicitudes=$solicitud;
					if(!$solicitud){
						$sql="SELECT * 
							  FROM seguridad_usuarios
						 	  WHERE usuar_codig = '".$traba."'";

						$usuario=$conexion->createCommand($sql)->queryRow();

						
						if(!$solicitud){

							$sql = "DELETE FROM solicitud_usuario 
									WHERE solic_codig = '".$value."'
						 	    	  AND usuar_codig in (SELECT usuar_codig
						 	    	  FROM seguridad_usuarios 
					 	  			  WHERE usuar_codig <> '".$traba."'
					 	  			  AND (organ_codig = '".$usuario['organ_codig']."' or urole_codig = '".$usuario['urole_codig']."'))";
					 	  	
							$query=$sql;
							$res=$conexion->createCommand($sql)->execute();

							$traza=$this->funciones->guardarTraza($conexion, 'D', 'solicitud_usuario', json_encode($solicitudes),json_encode(''),$query);

							$sql="INSERT INTO solicitud_usuario(solic_codig, usuar_codig, solus_ucrea, solus_fcrea, solus_hcrea) 
								VALUES ('".$value."','".$traba."','".$usuar."','".$fecha."','".$hora."')";
							$query=$sql;
							$res=$conexion->createCommand($sql)->execute();

							$sql="SELECT * 
								  FROM solicitud_usuario 
								  WHERE solic_codig = '".$value."'
								    AND usuar_codig = '".$traba."'";
							
							$solicitud2=$conexion->createCommand($sql)->queryRow();

						    $traza=$this->funciones->guardarTraza($conexion, 'I', 'solicitud_usuario', json_encode(''),json_encode($solicitud2),$query);				
						}else{
							$transaction->rollBack();
		        			$msg=array('success'=>'false','msg'=>'La solicitud ya se encuentra asignada');
		        			echo json_encode($msg);
		        			exit();
						}
					}else{
						$transaction->rollBack();
	        			$msg=array('success'=>'false','msg'=>'La solicitud ya se encuentra asignada a esa persona');
	        			echo json_encode($msg);
	        			exit();
					}
				}		
				$transaction->commit();
				$msg=array('success'=>'true','msg'=>'Solicitudes Asignadas correctamente');    
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$this->render('reasignacion');
		}
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}