<?php

class VentasController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->render('index');
	}
	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$condiFcion2='';
		$con=0;
		if($_POST['nfact']){
			$nfact=mb_strtoupper($_POST['nfact']);
			$condicion.=$condicion2."a.factu_nfact like '%".$nfact."%' ";
			$condicion2='AND ';
			$con++;
		}
		if($_POST['banco']){
			$banco=mb_strtoupper($_POST['banco']);
			$condicion.=$condicion2."a.banco_codig like '%".$banco."%' ";
			$condicion2='AND ';
			$con++;
		}
		if($_POST['traba']){
			$traba=mb_strtoupper($_POST['traba']);
			$condicion.=$condicion2."a.traba_codig = '".$traba."' ";
			$condicion2='AND ';
			$con++;
		}
		if($_POST['desde']){
			$desde=$this->funciones->TransformarFecha_bd($_POST['desde']);
			$condicion.=$condicion2."a.factu_femis >= '".$desde."' ";
			$condicion2='AND ';
			$con++;
		}
		if($_POST['hasta']){
			$hasta=$this->funciones->TransformarFecha_bd($_POST['hasta']);
			$condicion.=$condicion2."a.factu_femis <= '".$hasta."' ";
			$condicion2='AND ';
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}

	public function actionPdf()
	{
		$condicion='';
		$condicion2='';
		$con=0;
		$titulo='Relacion de Ventas <br>';
		if($_POST['nfact']){
			$nfact=mb_strtoupper($_POST['nfact']);
			$condicion.=$condicion2."a.factu_nfact like '%".$nfact."%' ";
			$condicion2='AND ';
			$con++;
		}
		if($_POST['banco']){
			$banco=mb_strtoupper($_POST['banco']);
			$condicion.=$condicion2."a.banco_codig like '%".$banco."%' ";
			$condicion2='AND ';
			$sql="SELECT * FROM p_banco WHERE banco_codig='".$banco."'";
			$conexion=Yii::app()->db;
			$result=$conexion->createCommand($sql)->queryRow();
			$titulo.=' Banco: '.$result['banco_descr'].' <br>';
			$con++;
		}
		$vende=0;
		if($_POST['traba']){
			$traba=mb_strtoupper($_POST['traba']);
			$condicion.=$condicion2."a.traba_codig = '".$traba."' ";
			$condicion2='AND ';
			$sql="SELECT * FROM p_trabajador a
				  JOIN p_persona b ON (a.perso_codig=b.perso_codig)
			      WHERE traba_codig='".$traba."'";
			$conexion=Yii::app()->db;
			$result=$conexion->createCommand($sql)->queryRow();
			$titulo.=' Vendedor: '.$result['perso_pnomb'].' '.$result['perso_papel'].' <br>';
			$vende=1;
			$con++;
		}
		if($_POST['desde']){
			$desde=$this->funciones->TransformarFecha_bd($_POST['desde']);
			$condicion.=$condicion2."a.factu_femis >= '".$desde."' ";
			$condicion2='AND ';
			$titulo.=' Desde: '.$_POST['desde'];
			$con++;
		}
		if($_POST['hasta']){
			$hasta=$this->funciones->TransformarFecha_bd($_POST['hasta']);
			$condicion.=$condicion2."a.factu_femis <= '".$hasta."' ";
			$condicion2='AND ';
			$titulo.=' Hasta: '.$_POST['hasta'];
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}

		$header=$this->funciones->header($titulo);
		$footer=$this->funciones->footer();
		$mPDF1 = Yii::app()->ePdf->mpdf('utf-8','LETTER-L','','',10,10,29,13,9,9,'P'); 
 		$mPDF1->useOnlyCoreFonts = true;
 		$mPDF1->SetTitle("SWT | Sistema de Control Interno - Reporte de Cobranza");
 		$mPDF1->SetAuthor("SWT - SmartWebTools");
 		$mPDF1->SetHTMLHeader($header);
 		$mPDF1->SetHTMLFooter($footer);
 		$mPDF1->WriteHTML($this->renderpartial('pdf', array('condicion'=>$condicion,'vende'=>$vende), true)); //hacemos un render partial a una vista preparada, en este caso es la vista pdfReport
 		$mPDF1->Output('Reporte_Cobranza_'.date('Ymd_His').'.pdf','I');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.	
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}