<?php

class SolicitudesEntregadasController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->render('index');
	}
	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM solicitud  a
			  WHERE a.solic_codig ='".$_GET['c']."'";
		$solicitud=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('solicitud' => $solicitud));
	}
	public function actionBuscar()
	{
		$condicion='';
		$condicion2='';
		$con=0;
		if($_POST['nfact']){
			$nfact=mb_strtoupper($_POST['nfact']);
			$condicion.=$condicion2."a.egres_nfact like '%".$nfact."%' ";
			$condicion2='AND ';
			$con++;
		}
		if($_POST['banco']){
			$banco=mb_strtoupper($_POST['banco']);
			$condicion.=$condicion2."a.banco_codig like '%".$banco."%' ";
			$condicion2='AND ';
			$con++;
		}
		if($_POST['desde']){
			$desde=$this->funciones->TransformarFecha_bd($_POST['desde']);
			$condicion.=$condicion2."a.egres_fegre >= '".$desde."' ";
			$condicion2='AND ';
			$con++;
		}
		if($_POST['hasta']){
			$hasta=$this->funciones->TransformarFecha_bd($_POST['hasta']);
			$condicion.=$condicion2."a.egres_fegre <= '".$hasta."' ";
			$condicion2='AND ';
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function header($titulo){

		$html='<div name="myheader">
 				<table width="100%">
 					<tr>
 						<td width="33%" style="color:#0000BB">
 							<img src="http://rapidpago.smartwebtools.net/assets/img/logo.png" width="20%">
 						</td>
 						<td width="34%" style="text-align: center; vertical-align: bottom; ">
 							<b><h2>'.$titulo.'</h2></b>
 						</td>
 						<td width="33%" style="text-align: right; vertical-align: bottom;">
 							<b>Fecha: </b>'.date('d/m/Y').'<br>
 							<b>Hora: </b>'.date('h:i:s A').'<br>
 							<b>Usuario: </b>'.Yii::app()->user->id['usuario']['nombre'].'
 						</td>
 					<tr>
 				</table>
 				<div style="border-top: 1px solid #000000;">

 				</div>


 			</div>';
 		return $html;
	}
	public function footer($cotizacion=''){

		$html='<div name="myfooter" style="border-top: 1px solid #000000; font-size: 6pt; text-align: center;">
					<p style="font-size: 6pt; text-align: center;">'.$cotizacion['cotiz_verif'].'</p>
 					<p style="font-size: 6pt; text-align: center;">
 						Av. La Estancia. CC Centro Comercial Ciudad Tamanaco. (Torre La Pirámide Invertida), Nivel 5. Oficina 529, Urb. Chuao. Caracas. (Chacao). Miranda. Zona Postal 1060.
					</p>
					<p style="font-size: 6pt; text-align: center;">
						Teléfonos: (0212) 959.40.04 / 61.28 / 12.27 / 20.06 - http://www.rapidpago.com
					</p>
					<p style="font-size: 6pt; text-align: center;">
 						Página {PAGENO} de {nb}	
 					</p>

 				</div>';
 		return $html;
	}

	public function actionPdf()
	{
		$c=$_GET['c'];
		$conexion=yii::app()->db;	


		$titulo='Solicitudes Entregadas<br>';
		$header=$this->header($titulo);
		$footer=$this->footer($cotizacion);
		$mPDF1 = Yii::app()->ePdf->mpdf('euc-jp','letter-L','','',10,10,29,25,9,9,'L'); 
 		$mPDF1->useOnlyCoreFonts = true;
 		$mPDF1->SetTitle("Seriales en Stock | RapidPago");
 		$mPDF1->SetAuthor("RapidPago, Tu Eres El Punto");
 		$mPDF1->SetHTMLHeader($header);
 		$mPDF1->SetHTMLFooter($footer);
 		$mPDF1->allow_charset_conversion = true;
 		$mPDF1->charset_in = 'iso-8859-4';
 		$mPDF1->shrink_tables_to_fit=0;


		$html=$this->renderpartial('pdf', array('c'=>$c), true);
 		
 		$mPDF1->WriteHTML($html); //hacemos un render partial a una vista preparada, en este caso es la vista pdfReport
 		$mPDF1->Output(date('Ymd_His').'_SE.pdf','I');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.	
	}
	public function guardarCotizacion($c,$ruta)
	{
		$conexion=yii::app()->db;	

		$sql="SELECT * FROM solicitud_cotizacion WHERE solic_codig='".$c."' and cotiz_estat='1'";
		$cotizacion=$conexion->createCommand($sql)->queryRow();	
		
		$titulo='Cotizacion <br>';
		$header=$this->header($titulo);
		$footer=$this->footer($cotizacion);
		$mPDF1 = Yii::app()->ePdf->mpdf('euc-jp','letter','','',10,10,29,13,9,9,'P'); 
 		$mPDF1->useOnlyCoreFonts = true;
 		$mPDF1->SetTitle("Cotizacion número ".$c." | RapidPago");
 		$mPDF1->SetAuthor("RapidPago, Tu Eres El Punto");
 		$mPDF1->SetHTMLHeader($header);
 		$mPDF1->SetHTMLFooter($footer);
 		$mPDF1->allow_charset_conversion = true;
 		$mPDF1->charset_in = 'iso-8859-4';
 		$mPDF1->shrink_tables_to_fit=0;


		$html=$this->renderpartial('cotizacion', array('c'=>$c), true);
 		
 		$mPDF1->WriteHTML($html); //hacemos un render partial a una vista preparada, en este caso es la vista pdfReport
 		$mPDF1->Output($ruta,'F');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.	
 		
		chmod($ruta, 0777);
		return $ruta;
	}
	public function actionBordados()
	{
		$c=$_GET['c'];
		$titulo='Bordados del Pedido <br>';
		$header=$this->header($titulo);
		$footer=$this->footer();
		$mPDF1 = Yii::app()->ePdf->mpdf('utf-8','LEGAL','','',10,10,29,13,9,9,'L'); 
 		$mPDF1->useOnlyCoreFonts = true;
 		$mPDF1->SetTitle("Pedido número ".$c." | Polerones Tiempo");
 		$mPDF1->SetAuthor("Polerones Tiempo, Bordando Experiencias");
 		$mPDF1->SetHTMLHeader($header);
 		$mPDF1->SetHTMLFooter($footer);
 		
 		$mPDF1->WriteHTML($this->renderpartial('bordados', array('c'=>$c), true)); //hacemos un render partial a una vista preparada, en este caso es la vista pdfReport
 		$mPDF1->Output(date('Ymd_His').'_Bordados_Pedido_'.$c.'.pdf','I');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.	
	}
	public function actionPagos()
	{
		$c=$_GET['c'];
		$titulo='Detalle del Pago <br>';
		$header=$this->header($titulo);
		$footer=$this->footer();
		$mPDF1 = Yii::app()->ePdf->mpdf('utf-8','LEGAL','','',10,10,29,13,9,9,'P'); 
 		$mPDF1->useOnlyCoreFonts = true;
 		$mPDF1->SetTitle("Pago número ".$c." | Polerones Tiempo");
 		$mPDF1->SetAuthor("Polerones Tiempo, Bordando Experiencias");
 		$mPDF1->SetHTMLHeader($header);
 		$mPDF1->SetHTMLFooter($footer);
 		$mPDF1->WriteHTML($this->renderpartial('pagos', array('c'=>$c), true)); //hacemos un render partial a una vista preparada, en este caso es la vista pdfReport
 		$mPDF1->Output(date('Ymd_His').'_Detalle_Pago_'.$c.'.pdf','I');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.	
	}
	public function actionCorte()
	{
		$c=$_GET['c'];
		$titulo='Detalle del Corte <br>';
		$header=$this->header($titulo);
		$footer=$this->footer();
		$mPDF1 = Yii::app()->ePdf->mpdf('utf-8','LEGAL-L','','',10,10,29,13,9,9,'P'); 
 		$mPDF1->useOnlyCoreFonts = true;
 		$mPDF1->SetTitle("Pedido número ".$c." | Polerones Tiempo");
 		$mPDF1->SetAuthor("Polerones Tiempo, Bordando Experiencias");
 		$mPDF1->SetHTMLHeader($header);
 		$mPDF1->SetHTMLFooter($footer);
 		$mPDF1->WriteHTML($this->renderpartial('corte', array('c'=>$c), true)); //hacemos un render partial a una vista preparada, en este caso es la vista pdfReport
 		$mPDF1->Output(date('Ymd_His').'_Detalle_Corte_'.$c.'.pdf','I');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.	
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}