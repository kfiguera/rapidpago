<?php

class EgresosController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->render('index');
	}
	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$condicion2='';
		$con=0;
		if($_POST['nfact']){
			$nfact=mb_strtoupper($_POST['nfact']);
			$condicion.=$condicion2."a.egres_nfact like '%".$nfact."%' ";
			$condicion2='AND ';
			$con++;
		}
		if($_POST['banco']){
			$banco=mb_strtoupper($_POST['banco']);
			$condicion.=$condicion2."a.banco_codig like '%".$banco."%' ";
			$condicion2='AND ';
			$con++;
		}
		if($_POST['desde']){
			$desde=$this->funciones->TransformarFecha_bd($_POST['desde']);
			$condicion.=$condicion2."a.egres_fegre >= '".$desde."' ";
			$condicion2='AND ';
			$con++;
		}
		if($_POST['hasta']){
			$hasta=$this->funciones->TransformarFecha_bd($_POST['hasta']);
			$condicion.=$condicion2."a.egres_fegre <= '".$hasta."' ";
			$condicion2='AND ';
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function header($titulo){

		$html='<div name="myheader">
 				<table width="100%">
 					<tr>
 						<td width="33%" style="color:#0000BB">
 							<img src="'.Yii::app()->request->baseUrl.'/assets/img/logo-importadora-pdf.png" width="15%">
 						</td>
 						<td width="34%" style="text-align: center; vertical-align: bottom; ">
 							<b>'.$titulo.'</b>
 						</td>
 						<td width="33%" style="text-align: right; vertical-align: bottom;">
 							<b>Fecha: </b>'.date('d/m/Y').'<br>
 							<b>Hora: </b>'.date('h:i:s A').'<br>
 							<b>Usuario: </b>'.Yii::app()->user->id['usuario']['nombre'].'
 						</td>
 					<tr>
 				</table>
 				<div style="border-top: 1px solid #000000;">

 				</div>


 			</div>';
 		return $html;
	}
	public function footer(){
		$html='<div name="myfooter">
 					<div style="border-top: 1px solid #000000; font-size: 6pt; padding-top: 1mm; text-align: center;">
 						Página {PAGENO} de {nb}
 					</div>
 				</div>';
 		return $html;
	}

	public function actionPdf()
	{
		$condicion='';
		$condicion2='';
		$con=0;
		$titulo='Reporte de Egresos <br>';

		if($_POST['nfact']){
			$nfact=mb_strtoupper($_POST['nfact']);
			$condicion.=$condicion2."a.egres_nfact like '%".$nfact."%' ";
			$condicion2='AND ';
			$con++;
		}
		if($_POST['banco']){
			$banco=mb_strtoupper($_POST['banco']);
			$condicion.=$condicion2."a.banco_codig like '%".$banco."%' ";
			$condicion2='AND ';
			$sql="SELECT * FROM p_banco WHERE banco_codig='".$banco."'";
			$conexion=Yii::app()->db;
			$result=$conexion->createCommand($sql)->queryRow();
			$titulo.=' Banco: '.$result['banco_descr'].' <br>';
			$con++;
		}
		if($_POST['desde']){
			$desde=$this->funciones->TransformarFecha_bd($_POST['desde']);
			$condicion.=$condicion2."a.egres_fegre >= '".$desde."' ";
			$condicion2='AND ';
			$titulo.=' Desde: '.$_POST['desde'];
			$con++;
		}
		if($_POST['hasta']){
			$hasta=$this->funciones->TransformarFecha_bd($_POST['hasta']);
			$condicion.=$condicion2."a.egres_fegre <= '".$hasta."' ";
			$condicion2='AND ';
			$titulo.=' Hasta: '.$_POST['hasta'];
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}

		$header=$this->header($titulo);
		$footer=$this->footer();
		$mPDF1 = Yii::app()->ePdf->mpdf('utf-8','LETTER','','',10,10,29,13,9,9,'P'); 
 		$mPDF1->useOnlyCoreFonts = true;
 		$mPDF1->SetTitle("SWT - Reporte de Egresos");
 		$mPDF1->SetAuthor("SWT");
 		$mPDF1->SetHTMLHeader($header);
 		$mPDF1->SetHTMLFooter($footer);
 		$mPDF1->WriteHTML($this->renderpartial('pdf', array('condicion'=>$condicion), true)); //hacemos un render partial a una vista preparada, en este caso es la vista pdfReport
 		$mPDF1->Output('Reporte_Egresos_'.date('Ymd_His').'.pdf','I');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.	
	}
	
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}