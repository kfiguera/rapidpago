<?php

class TrabajadorController extends Controller
{
	public $funciones;
	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}
	public function actionIndex()
	{
		$this->redirect(Yii::app()->request->baseUrl.'/p_trabajador/listado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;

		if($_POST['cedul']){
			$cedul=mb_strtoupper($_POST['cedul']);
			$condicion.="b.perso_cedul like '%".$cedul."%' ";
			$con++;
		}
		if($_POST['nombr']){
			$nombr=mb_strtoupper($_POST['nombr']);
			if($con>0){
				$condicion.="AND ";
			}
			$condicion.="b.perso_pnomb like '%".$nombr."%' or b.perso_snomb like '%".$nombr."%' ";
			$con++;
		}
		if($_POST['apell']){
			$apell=mb_strtoupper($_POST['apell']);
			if($con>0){
				$condicion.="AND ";
			}
			$condicion.="b.perso_papel like '%".$apell."%' or b.perso_sapel like '%".$apell."%' ";
			$con++;
		}
		if($_POST['codig']){
			$codig=mb_strtoupper($_POST['codig']);
			if($con>0){
				$condicion.="AND ";
			}
			$condicion.="a.traba_ctrab like '%".$codig."%' ";
			$con++;
		}
		if($_POST['ttrab']){
			$ttrab=mb_strtoupper($_POST['ttrab']);
			if($con>0){
				$condicion.="AND ";
			}
			$condicion.="a.traba_ttrab like '%".$ttrab."%' ";
			$con++;
		}
		if($_POST['etrab']){
			if($con>0){
				$condicion.="AND ";
			}
			$etrab=mb_strtoupper($_POST['etrab']);
			$condicion.="a.etrab_codig = '".$etrab."' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM p_trabajador  a
			  WHERE a.traba_codig ='".$_GET['c']."'";
		$roles=$conexion->createCommand($sql)->queryRow();

		$this->render('consultar', array('roles' => $roles));
	}
	public function actionRegistrar()
	{
		if($_POST){
			/*var_dump($_POST);
			exit();*/
			//PERSONA
			$nacio = mb_strtoupper($_POST['nacio']);
			$cedul = $this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['cedula']));
			$pnomb = mb_strtoupper($_POST['pnomb']);
			$snomb = mb_strtoupper($_POST['snomb']);
			$papel = mb_strtoupper($_POST['papel']);
			$sapel = mb_strtoupper($_POST['sapel']);
			$fnaci = $this->funciones->TransformarFecha_bd($_POST['fnaci']);
            $ecivi = mb_strtoupper($_POST['ecivi']);
            $gener = mb_strtoupper($_POST['gener']);
            //TRABAJADOR
            $salar=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['salar']));
            $comis=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['comis']));
            $etrab=mb_strtoupper($_POST['etrab']);
			$ctrab=mb_strtoupper($_POST['ctrab']);
			$ttipo=mb_strtoupper($_POST['ttipo']);
			$corre=mb_strtoupper($_POST['corre']);
			$telef=mb_strtoupper($_POST['telef']);
			$banco=mb_strtoupper($_POST['banco']);
			$tcuen=mb_strtoupper($_POST['tcuen']);
			$ncuen=mb_strtoupper($_POST['ncuen']);
			$direc=mb_strtoupper($_POST['direc']);
			$obser=mb_strtoupper($_POST['obser']);
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				//VERIFICAR SI EXISTE LA PERSONA
				$sql="SELECT * FROM p_persona WHERE nacio_value ='".$nacio."' and perso_cedul='".$cedul."'";
				$p_persona=$conexion->createCommand($sql)->queryRow();

				if(!$p_persona){
					//SI NO EXISTE SE GUARDA
					$sql="INSERT INTO p_persona(nacio_value, perso_cedul, perso_pnomb, perso_snomb, perso_papel, perso_sapel, perso_fnaci, gener_value, ecivi_value, perso_obser) 
						VALUES ('".$nacio."','".$cedul."','".$pnomb."','".$snomb."','".$papel."','".$sapel."','".$fnaci."','".$gener."','".$ecivi."','".$obser."')";
					$res1=$conexion->createCommand($sql)->execute();
					if($res1){
						$sql="SELECT * FROM p_persona WHERE nacio_value ='".$nacio."' and perso_cedul='".$cedul."'";
						$p_persona=$conexion->createCommand($sql)->queryRow();	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al guardar la persona');	
					}	
				}
				
				if($p_persona){
					$perso=$p_persona['perso_codig'];
					$sql="SELECT * FROM p_trabajador WHERE perso_codig = '".$perso."'";
					$p_persona=$conexion->createCommand($sql)->queryRow();
					if(!$p_persona){
						$sql="SELECT * FROM p_trabajador WHERE traba_corre = '".$corre."'";
						$correo=$conexion->createCommand($sql)->queryRow();
						if(!$correo){
							$sql="SELECT * FROM p_trabajador WHERE traba_ncuen = '".$ncuen."'";
							$cuenta=$conexion->createCommand($sql)->queryRow();
							if(!$cuenta){
								$sql="INSERT INTO p_trabajador(perso_codig, ttipo_codig, etrab_codig, traba_corre, traba_telef, traba_direc, traba_salar, traba_comis, banco_value, traba_ncuen, tcuen_codig, traba_ctrab, traba_obser, usuar_codig, traba_fcrea, traba_hcrea) 
								VALUES ('".$perso."','".$ttipo."','".$etrab."','".$corre."','".$telef."','".$direc."','".$salar."','".$comis."','".$banco."','".$ncuen."','".$tcuen."','".$ctrab."','".$obser."','".Yii::app()->user->id['usuario']['codigo']."','".date('Y-m-d')."','".date('H:i:s')."')";
								$res1=$conexion->createCommand($sql)->execute();
								if($res1){
									$transaction->commit();
									$msg=array('success'=>'true','msg'=>'Trabajador guardado correctamente');	
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al guardar el Trabajador');	
								}		
							}else{	
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'La Cuenta ya esta registrada');
							}
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'El correo electronico ya esta registrado');
						}	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'La persona ya existe como p_trabajador');
					}
				}else{
					$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al guardar la persona','sql'=>$sql);	
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			$this->render('registrar');
		}
	}
	public function actionModificar()
	{
		if($_POST){
			$codig=mb_strtoupper($_POST['codig']);
			$etrab=mb_strtoupper($_POST['etrab']);
			$perso=mb_strtoupper($_POST['perso']);
            $salar=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['salar']));
            $comis=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['comis']));

			$ctrab=mb_strtoupper($_POST['ctrab']);
			$ttipo=mb_strtoupper($_POST['ttipo']);
			$corre=mb_strtoupper($_POST['corre']);
			$telef=mb_strtoupper($_POST['telef']);
			$banco=mb_strtoupper($_POST['banco']);
			$tcuen=mb_strtoupper($_POST['tcuen']);
			$ncuen=mb_strtoupper($_POST['ncuen']);
			$direc=mb_strtoupper($_POST['direc']);
			$obser=mb_strtoupper($_POST['obser']);

			if($contra==$ccontra){
				$conexion=Yii::app()->db;
				$transaction=$conexion->beginTransaction();
				try{
					$sql="SELECT * FROM p_trabajador  WHERE traba_codig ='".$codig."'";
					$codigo=$conexion->createCommand($sql)->queryRow();
					if($codigo){
						$sql="SELECT * FROM p_trabajador WHERE perso_codig = '".$perso."'";
						$p_persona=$conexion->createCommand($sql)->queryRow();
						if(!$p_persona or $codigo['perso_codig']==$perso){
							
							$sql="SELECT * FROM nomina_asignacion a 
								  JOIN nomina_periodo b ON ( a.perio_codig = b.perio_codig)
								  WHERE a.traba_codig='".$codig."' AND b.eperi_codig <> '2'";
							$asign=$conexion->createCommand($sql)->queryRow();
							if(($asign and $etrab=='1') or (!$asign and $etrab=='1') or (!$asign and $etrab<>'1')){

								$sql="SELECT * FROM p_trabajador WHERE traba_corre = '".$corre."'";
								$correo=$conexion->createCommand($sql)->queryRow();
								
								if(!$correo or $codigo['traba_corre']==$corre){
									$sql="SELECT * FROM p_trabajador WHERE traba_ncuen = '".$ncuen."'";
									$cuenta=$conexion->createCommand($sql)->queryRow();
									if(!$cuenta or $codigo['traba_ncuen']==$ncuen){
										$sql="UPDATE p_trabajador 
										  		SET perso_codig='".$perso."',
													ttipo_codig='".$ttipo."',
													etrab_codig='".$etrab."',
													traba_salar='".$salar."',
													traba_comis='".$comis."',
													traba_corre='".$corre."',
													traba_telef='".$telef."',
													traba_direc='".$direc."',
													banco_value='".$banco."',
													traba_ncuen='".$ncuen."',
													tcuen_codig='".$tcuen."',
													traba_ctrab='".$ctrab."',
													traba_obser='".$obser."'
												WHERE traba_codig ='".$codig."'";
										$res1=$conexion->createCommand($sql)->execute();
										if($res1){
											$transaction->commit();
												$msg=array('success'=>'true','msg'=>'Trabajador actualizado correctamente');
										}else{
											$transaction->rollBack();
											$msg=array('success'=>'false','msg'=>'Error al actualizar el Trabajador');	
										}
									}else{	
										$transaction->rollBack();
										$msg=array('success'=>'false','msg'=>'La Cuenta ya esta registrada');
									}
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'El correo electronico ya esta registrado');
								}
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'No se puede modificar
								 debido a, que esta asignado en una nomina con periodos sin cerrar');
							}
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'La persona ya existe como p_trabajador');
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Trabajador no existe');
					}
				}catch(Exception $e){
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error al verificar la información');
				}
			}else{
				$msg=array('success'=>'false','msg'=>'La contraseña y su confirmación no son iguales');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			
			$sql="SELECT * 
			  FROM p_trabajador  a
			  WHERE a.traba_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('modificar', array('roles' => $roles));
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM p_trabajador  WHERE traba_codig='".$codig."'";
				$roles=$conexion->createCommand($sql)->queryRow();
				if($roles){
					/*$sql="SELECT * FROM nomina_asignacion  WHERE traba_codig='".$codig."'";
					$asign=$conexion->createCommand($sql)->queryRow();
					if($asign){*/

						$sql="DELETE FROM p_trabajador  WHERE traba_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();
						//echo $sql;
						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Trabajador eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Trabajador ');	
						}
					/*}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'No se puede eliminar debido a, que esta asignado en una nomina');
					}*/	
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Trabajador no existe');
				}
			}catch(Exception $e){
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM p_trabajador  a
			  WHERE a.traba_codig ='".$_GET['c']."'";
			$roles=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('roles' => $roles));
		}
	}
	
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}