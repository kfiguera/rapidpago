<?php

class PrepararVipController extends Controller
{
	public $funciones;

	public function init()
	{
		$fun=Yii::app()->createController('funciones');
		$this->funciones=$fun[0];
		$this->funciones->init();

	}

	public function actionIndex()
	{
		$this->redirect(Yii::app()->request->baseUrl.'/configuracion/Recepcion/lisado');
	}

	public function actionListado()
	{
		$this->render('listado');
	}
	public function actionBuscar()
	{
		$condicion='';
		$con=0;
		if($_POST['numero']){
			$numero=mb_strtoupper($_POST['numero']);
			$condicion.="a.solic_numer like '%".$numero."%' ";
			$con++;
		}
		if($_POST['epedi']){
			if($con>0){
				$condicion.="AND ";
			}
			$epedi=mb_strtoupper($_POST['epedi']);
			$condicion.="a.epedi_codig like '%".$epedi."%' ";
			$con++;
		}
		if($con>0){
			$condicion="WHERE ".$condicion;
		}
		
		//$_SESSION['where']=$condicion;
		$this->renderpartial('buscar', array('condicion' => $condicion));		
	}
	public function actionConsultar()
	{
		$conexion=Yii::app()->db;
		$sql="SELECT * 
			  FROM solicitud  a
			  WHERE a.solic_codig ='".$_GET['c']."'";
		$solicitud=$conexion->createCommand($sql)->queryRow();
		$this->render('consultar', array('solicitud' => $solicitud));
	}
	public function actionRegistrar()
	{
		if($_POST){
			//Datos del Pedido
			$canti=mb_strtoupper($_POST["canti"]);
			$mcant=mb_strtoupper($_POST["mcant"]);
			$color=mb_strtoupper($_POST["color"]);
			$mensa=mb_strtoupper($_POST["mensa"]);
			//Datos de Auditoria
			$estat=1;
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();

			try{
				//Obtenemos el codigo del pedido 
				$sql="SELECT max(solic_codig) codigo FROM solicitud";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				$codig=$pedidos['codigo']+1;
				$cpedi=$this->funciones->generarCodigoPedido(5,$codig);

				//Guardamos el Pedido
				
				$sql="INSERT INTO solicitud( solic_numer, solic_total, solic_cmode, solic_obser, epedi_codig, usuar_codig, solic_fcrea, solic_hcrea) 
						VALUES ('".$cpedi."','".$canti."','".$mcant."','".$mensa."','".$estat."','".$usuar."','".$fecha."','".$hora."')";
				$res1=$conexion->createCommand($sql)->execute();
				if($res1){
					$transaction->commit();
					$msg=array('success'=>'true','msg'=>'Pedido guardado correctamente');	
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Error al guardar el Pedido');	
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			
			echo json_encode($msg);
		}else{

			//$this->render('registrar');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			try{
				//Obtenemos el codigo del pedido 
				if(Yii::app()->user->id['usuario']['permiso']!=1){
					$sql="SELECT * FROM solicitud WHERE epedi_codig in ('1','2','7') and usuar_codig='".$usuar."'";
					$verificar=$conexion->createCommand($sql)->queryRow();
				}else{
					$verificar=false;
				}
				if(!$verificar){
					//Datos del Pedido
					$canti=0;
					$mcant=0;
					$color=0;
					$mensa="";
					//Datos de Auditoria
					$estat=1;
					$usuar=Yii::app()->user->id['usuario']['codigo'];
					$fecha=date('Y-m-d');
					$hora=date('H:i:s');
					//Obtenemos el codigo del pedido 
					$sql="SELECT max(solic_codig) codigo FROM solicitud";
					$pedidos=$conexion->createCommand($sql)->queryRow();
					$codig=$pedidos['codigo']+1;
					$cpedi=$this->funciones->generarCodigoPedido(5,$codig);

					//Guardamos el Pedido
					$sql="INSERT INTO solicitud( solic_numer, solic_total, solic_cmode, solic_obser, epedi_codig, usuar_codig, solic_fcrea, solic_hcrea) 
								VALUES ('".$cpedi."','".$canti."','".$mcant."','".$mensa."','".$estat."','".$usuar."','".$fecha."','".$hora."')";
					$res1=$conexion->createCommand($sql)->execute();
					if($res1){
						$transaction->commit();
						$sql="SELECT max(solic_codig) solic_codig FROM solicitud";
						$pedido=$conexion->createCommand($sql)->queryRow();

						$this->redirect('../detalle/registrar?p='.$pedido['solic_codig']);
						
						$msg=array('success'=>'true','msg'=>'Pedido guardado correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al guardar el Pedido');	
					}
				}else{
					$transaction->rollBack();
					Yii::app()->user->setFlash('danger', 'Error Solo puede tener un pedido activo');
					$this->redirect('listado');
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
		}
	}
	public function actionPaso1()
	{
		if($_POST){
			//Datos del Detalle
			$pedid=mb_strtoupper($_POST['pedid']);
			$codig=mb_strtoupper($_POST['codig']);
			$pasos=mb_strtoupper($_POST['pasos']);
			$ccuer=mb_strtoupper($_POST['ccuer']);
			$cbols=mb_strtoupper($_POST['cbols']);
			$cmang=mb_strtoupper($_POST['cmang']);
			$cegor=mb_strtoupper($_POST['cegor']);
			$cigor=mb_strtoupper($_POST['cigor']);
			$cppre=mb_strtoupper($_POST['cppre']);
			$cpprl=mb_strtoupper($_POST['cpprl']);
			$cierr=mb_strtoupper($_POST['cierr']);
			$ccier=mb_strtoupper($_POST['ccier']);
			$broch=mb_strtoupper($_POST['broch']);
			$ccurs=mb_strtoupper($_POST['ccurs']);
			$cnper=mb_strtoupper($_POST['cnper']);
			$caesp=mb_strtoupper($_POST['caesp']);
			$celec=mb_strtoupper($_POST['celec']);
			$cfras=mb_strtoupper($_POST['cfras']);
			$cvivo=mb_strtoupper($_POST['cvivo']);
			$otros=mb_strtoupper($_POST['otros']);
			
			//DATOS DE LAS ELECTIVAS
			
			$descr=$_POST["descr"];
			$texto=$_POST["texto"];

			$file=$_FILES['image'];
			/*$ruta='files/detalle/listado/';
			$name=explode(".", $file['name']);
			$nombre=date('Ymd_His.').end($name);
			$destino=$ruta.$nombre;*/
			foreach ($_FILES['image']['name'] as $key => $value) {
				$ruta='files/detalle/listado/';
				$name[$key]=explode(".", $file['name'][$key]);
				$nombre[$key]=date('Ymd_His_').$key.'.'.end($name[$key]);
				$destino[$key]=$ruta.$nombre[$key];
			}

			//Datos de Auditoria
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();

			$monto=0;
			if($pasos<'2'){
				$pasos='2';
			}
			$verificar['success']='true';
			try{
				
				foreach ($descr as $key => $value) {
					if(!empty($_FILES['image']['tmp_name'][$key])){
						$archivo['tmp_name']=$_FILES['image']['tmp_name'][$key];
						$verificar=$this->funciones->CopiarEmoji($archivo,$ruta,$nombre[$key]);
						if($verificar['success']!='true'){
							$transaction->rollBack();
							$msg=$verificar;
							echo json_encode($msg);	
							exit();
						}
					}else{
						$destino[$key]='';	
					}
				}
				if($verificar['success']=='true'){

					$sql="SELECT * FROM solicitud
						WHERE solic_codig='".$codig."';";
					$pedidos=$conexion->createCommand($sql)->queryRow();
					if($pedidos){
						$a=10;
						$incluyen="";

						foreach ($descr as $key => $value) {
							
							
							if($descr[$key]!='' or $texto[$key]!='' or $destino[$key]!=''){
								$descr[$key]=mb_strtoupper($descr[$key]);
								$texto[$key]=mb_strtoupper($texto[$key]);

								$sql="SELECT * FROM solicitud_imagen WHERE solic_codig ='".$codig."' and pimag_cimag='".$key."'";
								$result=$conexion->createCommand($sql)->queryRow();
								
								if($result ){
									if($destino[$key]==''){
										$sql="UPDATE solicitud_imagen 
											  SET pimag_ruta = '".$result['pimag_ruta']."' ,
											      pimag_tbord = '".$texto[$key]."',
											      pimag_descr = '".$descr[$key]."'
											  WHERE pimag_codig='".$result['pimag_codig']."'";	
									}else{
										$sql="UPDATE solicitud_imagen 
										SET pimag_ruta = '".$destino[$key]."',
											pimag_descr = '".$descr[$key]."',
											pimag_tbord = '".$texto[$key]."' 
										WHERE pimag_codig='".$result['pimag_codig']."'";
									}
									
								}else {
									$sql="INSERT INTO solicitud_imagen(solic_codig, pimag_orden, pimag_descr,  pimag_tbord, ptima_codig, pimag_cimag, pimag_ruta, usuar_codig, pimag_fcrea, pimag_hcrea) 
										VALUES ('".$codig."','".$a."','".$descr[$key]."', '".$texto[$key]."','5','".$key."','".$destino[$key]."','".$usuar."','".$fecha."','".$hora."');";
								}
								$res2=$conexion->createCommand($sql)->execute();
								if($a>10 and $incluyen!=''){
									$incluyen.=",";
								}
								$incluyen.="'".$key."'";
							}
							

							$a++;
						}
						if($incluyen==''){
							$sql="DELETE  FROM solicitud_imagen 
							  WHERE solic_codig = '".$codig."' 
							    AND pimag_orden>='10'";	
						}else{
							$sql="DELETE  FROM solicitud_imagen 
							  WHERE solic_codig = '".$codig."' 
							    AND pimag_cimag not in (".$incluyen.") 
							    AND pimag_orden>='10'";
							}
						
						$res2=$conexion->createCommand($sql)->execute();

						$sql="UPDATE solicitud
						    SET solic_ccuer = '".$ccuer."',
							solic_cbols = '".$cbols."',
							solic_cmang = '".$cmang."',
							solic_cegor = '".$cegor."',
							solic_cigor = '".$cigor."',
							solic_cppre = '".$cppre."',
							solic_cpprl = '".$cpprl."',
							solic_cierr = '".$cierr."',
							solic_ccier = '".$ccier."',
							solic_broch = '".$broch."',
							solic_ccurs = '".$ccurs."',
							solic_cnper = '".$cnper."',
							solic_caesp = '".$caesp."',
							solic_celec = '".$celec."',
							solic_cfras = '".$cfras."',
							solic_cvivo = '".$cvivo."',
							solic_otros = '".$otros."',
							solic_pasos = '".$pasos."'
						WHERE solic_codig = '".$codig."'";
						
						$res1=$conexion->createCommand($sql)->execute();
						if($res1 or $pedidos['solic_pasos']>='2'){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Pedido actualizado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al actualizar el Pedido');	
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error no existe el Pedido');	
					}
				}else{
					$transaction->rollBack();
					$msg=$verificar;
				}
			}catch(Exception $e){
				echo $sql;
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);

		}else{
			$p=$_GET['p'];
			$s=1;

			$this->render('registrar',array('p' => $p,'s' => $s ));
		}
	}

	public function actionPaso2()
	{
		if($_POST){
			/*var_dump($_POST);
			var_dump($_FILES);
			exit();*/
			//Datos del Detalle
			$pedid=mb_strtoupper($_POST['pedid']);
			$codig=mb_strtoupper($_POST['codig']);
			$pasos=mb_strtoupper($_POST['pasos']);
			//Datos del Formulario

			//Pecho
			$pfuen=mb_strtoupper($_POST['pfuen']);
			$pcolo=mb_strtoupper($_POST['pcolo']);
			$pposi=mb_strtoupper($_POST['pposi']);
			$pibde=mb_strtoupper($_POST['pibde']);
			$ptbde=$_POST['ptbde'];
			$ptide=mb_strtoupper($_POST['ptide']);
			$pibiz=mb_strtoupper($_POST['pibiz']);
			$ptbiz=$_POST['ptbiz'];
			$ptiiz=mb_strtoupper($_POST['ptiiz']);
			$pobse=mb_strtoupper($_POST['pobse']);
			
			//Gorro
			$gorie=mb_strtoupper($_POST['gorie']);
			$gfuen=mb_strtoupper($_POST['gfuen']);
			$gcolo=mb_strtoupper($_POST['gcolo']);
			$gposi=mb_strtoupper($_POST['gposi']);
			$gibde=mb_strtoupper($_POST['gibde']);
			$gtbde=$_POST['gtbde'];
			$gtide=mb_strtoupper($_POST['gtide']);
			$gibiz=mb_strtoupper($_POST['gibiz']);
			$gtbiz=$_POST['gtbiz'];
			$gtiiz=mb_strtoupper($_POST['gtiiz']);
			$gobse=mb_strtoupper($_POST['gobse']);

			//MANGA
			$mfuen=mb_strtoupper($_POST['mfuen']);
			$mcolo=mb_strtoupper($_POST['mcolo']);
			$mposi=mb_strtoupper($_POST['mposi']);
			$mibde=mb_strtoupper($_POST['mibde']);
			$mtbde=$_POST['mtbde'];
			$mtide=mb_strtoupper($_POST['mtide']);
			$mibiz=mb_strtoupper($_POST['mibiz']);
			$mtbiz=$_POST['mtbiz'];
			$mtiiz=mb_strtoupper($_POST['mtiiz']);
			$mobse=mb_strtoupper($_POST['mobse']);

			//DATOS DE LA IMAGEN
			$file['pride']=$_FILES['pride'];
			$ruta['pride']='files/detalle/pedido/'.$pedid.'/'.$codig.'/';
			$name['pride']=explode(".", $file['pride']['name']);
			$nombre['pride']=date('Ymd_His').'_pride.'.end($name['pride']);
			$destino['pride']=$ruta['pride'].$nombre['pride'];

			$file['priiz']=$_FILES['priiz'];
			$ruta['priiz']='files/detalle/pedido/'.$pedid.'/'.$codig.'/';
			$name['priiz']=explode(".", $file['priiz']['name']);
			$nombre['priiz']=date('Ymd_His').'_priiz.'.end($name['priiz']);
			$destino['priiz']=$ruta['priiz'].$nombre['priiz'];

			$file['gride']=$_FILES['gride'];
			$ruta['gride']='files/detalle/pedido/'.$pedid.'/'.$codig.'/';
			$name['gride']=explode(".", $file['gride']['name']);
			$nombre['gride']=date('Ymd_His').'_gride.'.end($name['gride']);
			$destino['gride']=$ruta['gride'].$nombre['gride'];

			$file['griiz']=$_FILES['griiz'];
			$ruta['griiz']='files/detalle/pedido/'.$pedid.'/'.$codig.'/';
			$name['griiz']=explode(".", $file['griiz']['name']);
			$nombre['griiz']=date('Ymd_His').'_griiz.'.end($name['griiz']);
			$destino['griiz']=$ruta['griiz'].$nombre['griiz'];

			//DATOS DE LA IMAGEN
			$file['mride']=$_FILES['mride'];
			$ruta['mride']='files/detalle/pedido/'.$pedid.'/'.$codig.'/';
			$name['mride']=explode(".", $file['mride']['name']);
			$nombre['mride']=date('Ymd_His').'_mride.'.end($name['mride']);
			$destino['mride']=$ruta['mride'].$nombre['mride'];

			$file['mriiz']=$_FILES['mriiz'];
			$ruta['mriiz']='files/detalle/pedido/'.$pedid.'/'.$codig.'/';
			$name['mriiz']=explode(".", $file['mriiz']['name']);
			$nombre['mriiz']=date('Ymd_His').'_mriiz.'.end($name['mriiz']);
			$destino['mriiz']=$ruta['mriiz'].$nombre['mriiz'];
			//Datos de Auditoria
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();

			$monto=0;
			if($pasos<'3'){
				$pasos='3';
			}


			try{
				$sql="SELECT * FROM solicitud_imagen WHERE solic_codig='".$codig."' and pimag_orden >=10";     
				$electivos = $conexion->createCommand($sql)->queryRow();

				if($electivos and $ptide!='6' AND $ptiiz!='6' AND $gtide!='6' AND $gtiiz!='6' AND $mtide!='6' AND $mtiiz!='6'){
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'Debes asociar al menos un electivo');
				}else{
					if(!empty($file['pride']['name'])){
						$verificar['pride']=$this->funciones->CopiarEmoji($file['pride'],$ruta['pride'],$nombre['pride']);	
						$image['pride']=true;
					}else{
						$destino['pride']='';
						$verificar['pride']['success']='true';
					}
					if(!empty($file['priiz']['name'])){
						$verificar['priiz']=$this->funciones->CopiarEmoji($file['priiz'],$ruta['priiz'],$nombre['priiz']);
						$image['priiz']=true;
					}else{
						$destino['priiz']='';
						$verificar['priiz']['success']='true';
					}
					if(!empty($file['gride']['name'])){
						$verificar['gride']=$this->funciones->CopiarEmoji($file['gride'],$ruta['gride'],$nombre['gride']);	
						$image['gride']=true;
					}else{
						$destino['gride']='';
						$verificar['gride']['success']='true';
					}
					if(!empty($file['griiz']['name'])){
						$verificar['griiz']=$this->funciones->CopiarEmoji($file['griiz'],$ruta['griiz'],$nombre['griiz']);
						$image['griiz']=true;
					}else{
						$destino['griiz']='';
						$verificar['griiz']['success']='true';
					}
					if(!empty($file['mride']['name'])){
						$verificar['mride']=$this->funciones->CopiarEmoji($file['mride'],$ruta['mride'],$nombre['mride']);	
						$image['mride']=true;
					}else{
						$destino['mride']='';
						$verificar['mride']['success']='true';
					}
					if(!empty($file['mriiz']['name'])){
						$verificar['mriiz']=$this->funciones->CopiarEmoji($file['mriiz'],$ruta['mriiz'],$nombre['mriiz']);	
						$image['mriiz']=true;
					}else{
						$destino['mriiz']='';
						$verificar['mriiz']['success']='true';
					}
					if($verificar['pride']['success']=='true' and $verificar['priiz']['success']=='true' and $verificar['gride']['success']=='true' and $verificar['griiz']['success']=='true' ){
						
						$sql="SELECT * FROM solicitud
							WHERE solic_codig='".$codig."';";
						$pedidos=$conexion->createCommand($sql)->queryRow();
						/*if($image['pride']==false){
							$destino['pride']=$pedidos['solic_pride'];
						}
						if($image['priiz']==false){
							$destino['priiz']=$pedidos['solic_priiz'];
						}
						if($image['gride']==false){
							$destino['gride']=$pedidos['solic_gride'];
						}
						if($image['griiz']==false){
							$destino['griiz']=$pedidos['solic_griiz'];
						}*/
						
						if($pedidos){

							$sql="SELECT * FROM solicitud_imagen WHERE solic_codig ='".$codig."' and pimag_orden=1";
							$result=$conexion->createCommand($sql)->queryRow();
							if($image['pride']==false){
								$destino['pride']=$result['pimag_ruta'];
							}
							if($result){
								$sql="UPDATE solicitud_imagen SET pimag_ruta = '".$destino['pride']."' WHERE pimag_codig='".$result['pimag_codig']."'";
							}else if($image['pride']==true){
								$sql="INSERT INTO solicitud_imagen(solic_codig, pimag_orden, pimag_descr, ptima_codig, pimag_cimag, pimag_ruta, usuar_codig, pimag_fcrea, pimag_hcrea) 
									VALUES ('".$codig."','1','PECHO DERECHA','".$ptide."','".$pcim1."','".$destino['pride']."','".$usuar."','".$fecha."','".$hora."');";
							}

							$res2=$conexion->createCommand($sql)->execute();

							$sql="SELECT * FROM solicitud_imagen WHERE solic_codig ='".$codig."' and pimag_orden=2";
							$result=$conexion->createCommand($sql)->queryRow();
							if($image['priiz']==false){
								$destino['priiz']=$result['pimag_ruta'];
							}
							if($result){
								$sql="UPDATE solicitud_imagen SET pimag_ruta = '".$destino['priiz']."' WHERE pimag_codig='".$result['pimag_codig']."'";
							}else if($image['priiz']==true){
								$sql="INSERT INTO solicitud_imagen(solic_codig, pimag_orden, pimag_descr, ptima_codig, pimag_cimag, pimag_ruta, usuar_codig, pimag_fcrea, pimag_hcrea) 
									VALUES ('".$codig."','2','PECHO IZQUIERDA','".$ptiiz."','".$pcim1."','".$destino['priiz']."','".$usuar."','".$fecha."','".$hora."');";
							}

							$res2=$conexion->createCommand($sql)->execute();

							$sql="SELECT * FROM solicitud_imagen WHERE solic_codig ='".$codig."' and pimag_orden=3";
							$result=$conexion->createCommand($sql)->queryRow();
							if($image['gride']==false){
								$destino['gride']=$result['pimag_ruta'];
							}
							if($result){
								$sql="UPDATE solicitud_imagen SET pimag_ruta = '".$destino['gride']."' WHERE pimag_codig='".$result['pimag_codig']."'";
							}else if($image['gride']==true){
								$sql="INSERT INTO solicitud_imagen(solic_codig, pimag_orden, pimag_descr, ptima_codig, pimag_cimag, pimag_ruta, usuar_codig, pimag_fcrea, pimag_hcrea) 
									VALUES ('".$codig."','3','GORRO DERECHA','".$gtide."','".$pcim1."','".$destino['gride']."','".$usuar."','".$fecha."','".$hora."');";
							}

							$res2=$conexion->createCommand($sql)->execute();

							$sql="SELECT * FROM solicitud_imagen WHERE solic_codig ='".$codig."' and pimag_orden=4";
							$result=$conexion->createCommand($sql)->queryRow();
							if($image['griiz']==false){
								$destino['griiz']=$result['pimag_ruta'];
							}
							if($result){
								$sql="UPDATE solicitud_imagen SET pimag_ruta = '".$destino['griiz']."' WHERE pimag_codig='".$result['pimag_codig']."'";
							}else if($image['griiz']==true){
								$sql="INSERT INTO solicitud_imagen(solic_codig, pimag_orden, pimag_descr, ptima_codig, pimag_cimag, pimag_ruta, usuar_codig, pimag_fcrea, pimag_hcrea) 
									VALUES ('".$codig."','4','GORRO IZQUIERDA','".$gtiiz."','".$pcim1."','".$destino['griiz']."','".$usuar."','".$fecha."','".$hora."');";
							}

							$res2=$conexion->createCommand($sql)->execute();

							$sql="SELECT * FROM solicitud_imagen WHERE solic_codig ='".$codig."' and pimag_orden=5";
							$result=$conexion->createCommand($sql)->queryRow();
							if($image['mride']==false){
								$destino['mride']=$result['pimag_ruta'];
							}
							if($result){
								$sql="UPDATE solicitud_imagen SET pimag_ruta = '".$destino['mride']."' WHERE pimag_codig='".$result['pimag_codig']."'";
							}else if($image['mride']==true){
								$sql="INSERT INTO solicitud_imagen(solic_codig, pimag_orden, pimag_descr, ptima_codig, pimag_cimag, pimag_ruta, usuar_codig, pimag_fcrea, pimag_hcrea) 
									VALUES ('".$codig."','5','MANGA DERECHA','".$mtide."','".$pcim1."','".$destino['mride']."','".$usuar."','".$fecha."','".$hora."');";
							}

							$res2=$conexion->createCommand($sql)->execute();

							$sql="SELECT * FROM solicitud_imagen WHERE solic_codig ='".$codig."' and pimag_orden=6";
							$result=$conexion->createCommand($sql)->queryRow();
							if($image['mriiz']==false){
								$destino['mriiz']=$result['pimag_ruta'];
							}
							if($result){
								$sql="UPDATE solicitud_imagen SET pimag_ruta = '".$destino['mriiz']."' WHERE pimag_codig='".$result['pimag_codig']."'";
							}else if($image['mriiz']==true){
								$sql="INSERT INTO solicitud_imagen(solic_codig, pimag_orden, pimag_descr, ptima_codig, pimag_cimag, pimag_ruta, usuar_codig, pimag_fcrea, pimag_hcrea) 
									VALUES ('".$codig."','6','MANGA IZQUIERDA','".$mtiiz."','".$pcim1."','".$destino['mriiz']."','".$usuar."','".$fecha."','".$hora."');";
							}

							$res2=$conexion->createCommand($sql)->execute();


							/*$sql="UPDATE solicitud
							  SET solic_pposi = '".$pposi."',
							  	  solic_ptder = '".$ptder."',
							  	  solic_ptizq = '".$ptizq."',
							  	  solic_ptbor = '".$ptbor."',
								  solic_pfuen = '".$pfuen."',
								  solic_pcolo = '".$pcolo."',
								  solic_plima = '".$plima."',
								  solic_prima = '".$destino['prima']."',
								  solic_pobse = '".$pobse."',
								  solic_gposi = '".$gposi."',
								  solic_gtder = '".$gtder."',
								  solic_gtizq = '".$gtizq."',
								  solic_gtbor = '".$gtbor."',
								  solic_gfuen = '".$gfuen."',
								  solic_gcolo = '".$gcolo."',
								  solic_glima = '".$glima."',
								  solic_grima = '".$destino['grima']."',
								  solic_gobse = '".$gobse."',
								  solic_pasos='".$pasos."' 
							WHERE solic_codig='".$codig."';";*/
							$sql="UPDATE solicitud
							  SET solic_pposi='".$pposi."',
								  solic_pibde='".$pibde."',
								  solic_ptbde='".$ptbde."',
								  solic_ptide='".$ptide."',
								  solic_pibiz='".$pibiz."',
								  solic_ptbiz='".$ptbiz."',
								  solic_ptiiz='".$ptiiz."',
								  solic_pfuen='".$pfuen."',
								  solic_pcolo='".$pcolo."',
								  solic_pobse='".$pobse."',
								  solic_gposi='".$gposi."',
								  solic_gibde='".$gibde."',
								  solic_gtbde='".$gtbde."',
								  solic_gtide='".$gtide."',
								  solic_gibiz='".$gibiz."',
								  solic_gtbiz='".$gtbiz."',
								  solic_gtiiz='".$gtiiz."',
								  solic_gorie='".$gorie."',
								  solic_gfuen='".$gfuen."',
								  solic_gcolo='".$gcolo."',
								  solic_gobse='".$gobse."',
								  solic_mfuen='".$mfuen."',
								  solic_mcolo='".$mcolo."',
								  solic_mposi='".$mposi."',
								  solic_mibde='".$mibde."',
								  solic_mtbde='".$mtbde."',
								  solic_mtide='".$mtide."',
								  solic_mibiz='".$mibiz."',
								  solic_mtbiz='".$mtbiz."',
								  solic_mtiiz='".$mtiiz."',
								  solic_mobse='".$mobse."',
								  solic_pasos='".$pasos."' 
							WHERE solic_codig='".$codig."';";





							$res1=$conexion->createCommand($sql)->execute();

							
							
							
							if($res1 or $pedidos['solic_pasos']>='2'){
								$transaction->commit();
								$msg=array('success'=>'true','msg'=>'Pedido actualizado correctamente');	
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al actualizar el Pedido');	
							}
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error no existe el Pedido');	
						}
					}else{
						$transaction->rollBack();
						$msg=$verificar;	
					}
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);

		}else{
			$p=$_GET['p'];
			$s=1;

			$this->render('registrar',array('p' => $p,'s' => $s ));
		}
	}
	public function actionPaso3()
	{
		if($_POST){

			//Datos del Detalle
			$pedid=mb_strtoupper($_POST['pedid']);
			$codig=mb_strtoupper($_POST['codig']);
			$pasos=mb_strtoupper($_POST['pasos']);
			//Datos del Formulario
			//MANGA
			/*$mfuen=mb_strtoupper($_POST['mfuen']);
			$mcolo=mb_strtoupper($_POST['mcolo']);
			$mposi=mb_strtoupper($_POST['mposi']);
			$mibde=mb_strtoupper($_POST['mibde']);
			$mtbde=mb_strtoupper($_POST['mtbde']);
			$mtide=mb_strtoupper($_POST['mtide']);
			$mibiz=mb_strtoupper($_POST['mibiz']);
			$mtbiz=mb_strtoupper($_POST['mtbiz']);
			$mtiiz=mb_strtoupper($_POST['mtiiz']);
			$mobse=mb_strtoupper($_POST['mobse']);*/

			//ESPALDA
			$eatbo=$_POST['eatbo'];
			$eafue=mb_strtoupper($_POST['eafue']);
			$eacol=mb_strtoupper($_POST['eacol']);
			$ebtbo=$_POST['ebtbo'];
			$ebfue=mb_strtoupper($_POST['ebfue']);
			$ebcol=mb_strtoupper($_POST['ebcol']);
			$aefue=mb_strtoupper($_POST['aefue']);
			$aecol=mb_strtoupper($_POST['aecol']);
			$eobse=mb_strtoupper($_POST['eobse']);

			//DATOS DE LA IMAGEN
			/*$file['mride']=$_FILES['mride'];
			$ruta['mride']='files/detalle/pedido/'.$pedid.'/'.$codig.'/';
			$name['mride']=explode(".", $file['mride']['name']);
			$nombre['mride']=date('Ymd_His').'_mride.'.end($name['mride']);
			$destino['mride']=$ruta['mride'].$nombre['mride'];

			$file['mriiz']=$_FILES['mriiz'];
			$ruta['mriiz']='files/detalle/pedido/'.$pedid.'/'.$codig.'/';
			$name['mriiz']=explode(".", $file['mriiz']['name']);
			$nombre['mriiz']=date('Ymd_His').'_mriiz.'.end($name['mriiz']);
			$destino['mriiz']=$ruta['mriiz'].$nombre['mriiz'];*/

			$file['iprut']=$_FILES['iprut'];
			$ruta['iprut']='files/detalle/pedido/'.$pedid.'/'.$codig.'/';
			$name['iprut']=explode(".", $file['iprut']['name']);
			$nombre['iprut']=date('Ymd_His').'_iprut.'.end($name['iprut']);
			$destino['iprut']=$ruta['iprut'].$nombre['iprut'];

			$file['rifro']=$_FILES['rifro'];
			$ruta['rifro']='files/detalle/pedido/'.$pedid.'/'.$codig.'/';
			$name['rifro']=explode(".", $file['rifro']['name']);
			$nombre['rifro']=date('Ymd_His').'_rifro.'.end($name['rifro']);
			$destino['rifro']=$ruta['rifro'].$nombre['rifro'];

			$file['riesp']=$_FILES['riesp'];
			$ruta['riesp']='files/detalle/pedido/'.$pedid.'/'.$codig.'/';
			$name['riesp']=explode(".", $file['riesp']['name']);
			$nombre['riesp']=date('Ymd_His').'_riesp.'.end($name['riesp']);
			$destino['riesp']=$ruta['riesp'].$nombre['riesp'];


			//Datos de Auditoria
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();

			$monto=0;
			if($pasos<'3'){
				$pasos='3';
			}
			try{
				/*if(!empty($file['mride']['name'])){
					$verificar['mride']=$this->funciones->CopiarEmoji($file['mride'],$ruta['mride'],$nombre['mride']);	
					$image['mride']=true;
				}else{
					$destino['mride']='';
					$verificar['mride']['success']='true';
				}
				if(!empty($file['mriiz']['name'])){
					$verificar['mriiz']=$this->funciones->CopiarEmoji($file['mriiz'],$ruta['mriiz'],$nombre['mriiz']);	
					$image['mriiz']=true;
				}else{
					$destino['mriiz']='';
					$verificar['mriiz']['success']='true';
				}*/
				if(!empty($file['iprut']['name'])){
					$verificar['iprut']=$this->funciones->CopiarEmoji($file['iprut'],$ruta['iprut'],$nombre['iprut']);
					$image['iprut']=true;
				}else{
					$destino['iprut']='';
					$verificar['iprut']['success']='true';
				}
				if(!empty($file['rifro']['name'])){
					$verificar['rifro']=$this->funciones->CopiarEmoji($file['rifro'],$ruta['rifro'],$nombre['rifro']);
					$image['rifro']=true;
				}else{
					$destino['rifro']='';
					$verificar['rifro']['success']='true';
				}
				if(!empty($file['riesp']['name'])){
					$verificar['riesp']=$this->funciones->CopiarEmoji($file['riesp'],$ruta['riesp'],$nombre['riesp']);
					$image['riesp']=true;
				}else{
					$destino['riesp']='';
					$verificar['riesp']['success']='true';
				}
				if($verificar['iprut']['success']=='true' and $verificar['rifro']['success']=='true' and $verificar['riesp']['success']=='true'){
					
					$sql="SELECT * FROM solicitud
						WHERE solic_codig='".$codig."';";

					$pedidos=$conexion->createCommand($sql)->queryRow();
					/*if($image['mrima']==false){
						$destino['mrima']=$pedidos['pdeta_mrima'];
					}
					if($image['iprut']==false){
						$destino['iprut']=$pedidos['pdeta_iprut'];
					}
					if($image['rifro']==false){
						$destino['rifro']=$pedidos['pdeta_rifro'];
					}
					if($image['riesp']==false){
						$destino['riesp']=$pedidos['pdeta_riesp'];
					}*/
					if($pedidos){
						

						$sql="SELECT * FROM solicitud_imagen WHERE solic_codig ='".$codig."' and pimag_orden=7";
						$result=$conexion->createCommand($sql)->queryRow();
						if($image['iprut']==false){
							$destino['iprut']=$result['pimag_ruta'];
						}
						if($result){
							$sql="UPDATE solicitud_imagen SET pimag_ruta = '".$destino['iprut']."' WHERE pimag_codig='".$result['pimag_codig']."'";
						}else if($image['iprut']==true){
							$sql="INSERT INTO solicitud_imagen(solic_codig, pimag_orden, pimag_descr, ptima_codig, pimag_cimag, pimag_ruta, usuar_codig, pimag_fcrea, pimag_hcrea) 
								VALUES ('".$codig."','7','IMAGEN PRINCIPAL','5','".$pcim1."','".$destino['iprut']."','".$usuar."','".$fecha."','".$hora."');";
						}

						$res2=$conexion->createCommand($sql)->execute();

						$sql="SELECT * FROM solicitud_imagen WHERE solic_codig ='".$codig."' and pimag_orden=8";
						$result=$conexion->createCommand($sql)->queryRow();
						if($image['rifro']==false){
							$destino['rifro']=$result['pimag_ruta'];
						}
						if($result){
							$sql="UPDATE solicitud_imagen SET pimag_ruta = '".$destino['rifro']."' WHERE pimag_codig='".$result['pimag_codig']."'";
						}else if($image['rifro']==true){
							$sql="INSERT INTO solicitud_imagen(solic_codig, pimag_orden, pimag_descr, ptima_codig, pimag_cimag, pimag_ruta, usuar_codig, pimag_fcrea, pimag_hcrea) 
								VALUES ('".$codig."','8','IMAGEN FRONTAL','5','".$pcim1."','".$destino['rifro']."','".$usuar."','".$fecha."','".$hora."');";
						}

						$res2=$conexion->createCommand($sql)->execute();

						$sql="SELECT * FROM solicitud_imagen WHERE solic_codig ='".$codig."' and pimag_orden=9";
						$result=$conexion->createCommand($sql)->queryRow();
						if($image['riesp']==false){
							$destino['riesp']=$result['pimag_ruta'];
						}
						if($result){
							$sql="UPDATE solicitud_imagen SET pimag_ruta = '".$destino['riesp']."' WHERE pimag_codig='".$result['pimag_codig']."'";
						}else if($image['riesp']==true){
							$sql="INSERT INTO solicitud_imagen(solic_codig, pimag_orden, pimag_descr, ptima_codig, pimag_cimag, pimag_ruta, usuar_codig, pimag_fcrea, pimag_hcrea) 
								VALUES ('".$codig."','9','IMAGEN ESPALDA','5','".$pcim1."','".$destino['riesp']."','".$usuar."','".$fecha."','".$hora."');";
						}

						$res2=$conexion->createCommand($sql)->execute();

						$sql="UPDATE solicitud
						  SET solic_eatbo='".$eatbo."',
							  solic_eafue='".$eafue."',
							  solic_eacol='".$eacol."',
							  solic_ebtbo='".$ebtbo."',
							  solic_ebfue='".$ebfue."',
							  solic_ebcol='".$ebcol."',
							  solic_aefue='".$aefue."',
							  solic_aecol='".$aecol."',
							  solic_eobse='".$eobse."',
							  solic_mrima='".$destino['mrima']."',
							  solic_iprut='".$destino['iprut']."',
							  solic_rifro='".$destino['rifro']."',
							  solic_riesp='".$destino['riesp']."',
							  solic_pasos='".$pasos."' 
						WHERE solic_codig='".$codig."';";
						$res1=$conexion->createCommand($sql)->execute();
						
						if($res1 or $pedidos['solic_pasos']>='3'){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Pedido guardado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al guardar el Pedido');	
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error no existe el Pedido');	
					}
				}else{
					$transaction->rollBack();
					$msg=$verificar;	
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);

		}else{
			$p=$_GET['p'];
			$s=1;

			$this->render('registrar',array('p' => $p,'s' => $s ));
		}
	}
	public function actionModificar()
	{
		if($_POST){
			//Datos del Pedido
			$codig=mb_strtoupper($_POST["codig"]);
			$canti=mb_strtoupper($_POST["canti"]);
			$mcant=mb_strtoupper($_POST["mcant"]);
			$color=mb_strtoupper($_POST["color"]);
			$mensa=mb_strtoupper($_POST["mensa"]);
			//Datos de Auditoria
			$estat=1;
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				
				$sql="SELECT * FROM solicitud WHERE solic_codig ='".$codig."'";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				if($pedidos){
					$sql="UPDATE solicitud
						  SET solic_total='".$canti."',
							  solic_cmode='".$mcant."',
							  solic_obser='".$mensa."'
						  WHERE solic_codig ='".$codig."'";
					$res1=$conexion->createCommand($sql)->execute();
					if($res1){
						$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Pedido actualizado correctamente');
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al actualizar el Pedido');	
					}
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$s=$_GET['s'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM solicitud  a
			  WHERE a.solic_codig ='".$_GET['c']."'";
			$pedidos=$conexion->createCommand($sql)->queryRow();
			$ubicacion[0]='start ';
			$ubicacion[1]='finish ';
			for ($i=0; $i < 3 ; $i++) { 
				if($i<$pedidos['solic_pasos']){
					$ubicacion[$i].='upcoming ';
				}
				if ($i==($s-1)) {
					$ubicacion[$i].='active ';
				}
			}
			$sql="SELECT b.*
			  FROM solicitud_detalle  a
			  JOIN pedido_modelo_conjunto b ON (a.tmode_codig = b.tmode_codig AND a.model_codig = b.model_codig AND a.mcate_codig = b.mcate_codig)
			  WHERE a.solic_codig ='".$_GET['c']."'";

			$conjuntos=$conexion->createCommand($sql)->queryAll(); 
			$incluyen='';
			foreach ($conjuntos as $key => $value) {
				if($incluyen==''){
					$incluyen=json_decode($value['mconj_inclu']);
				}else{
					if(json_decode($value['mconj_inclu'])!=null){
						$incluyen=array_merge($incluyen,json_decode($value['mconj_inclu']));	
					}
					
				}
			}
			$sql="SELECT b.*
			  FROM solicitud_detalle  a
			  JOIN pedido_modelo_opcional b ON (a.mopci_codig = b.mopci_codig)
			  WHERE a.solic_codig ='".$_GET['c']."'";

			$opcionales=$conexion->createCommand($sql)->queryAll(); 
			foreach ($opcionales as $key => $value) {
				if($incluyen==''){
					$incluyen=json_decode($value['mopci_inclu']);
				}else{
					if(json_decode($value['mopci_inclu'])!=null){
						$incluyen=array_merge($incluyen,json_decode($value['mopci_inclu']));	
					}
					
				}
			}
			$sql="SELECT b.*
			  FROM solicitud_detalle  a
			  JOIN pedido_modelo_opcional b ON (a.mopci_codi2 = b.mopci_codig)
			  WHERE a.solic_codig ='".$_GET['c']."'";

			$opcionales=$conexion->createCommand($sql)->queryAll(); 
			foreach ($opcionales as $key => $value) {
				if($incluyen==''){
					$incluyen=json_decode($value['mopci_inclu']);
				}else{
					if(json_decode($value['mopci_inclu'])!=null){
						$incluyen=array_merge($incluyen,json_decode($value['mopci_inclu']));	
					}
					
				}
			}
			$pedidos['cierr']=2;
			$pedidos['gorro']=2;
			$pedidos['preti']=2;
			$pedidos['broch']=2;
			$pedidos['vivo']=2;
			foreach ($incluyen as $key => $value) {
				if($value=='1'){
					$pedidos['cierr']=1;
				}
				if($value=='2'){
					$pedidos['gorro']=1;
				}
				if($value=='3'){
					$pedidos['preti']=1;
				}
				if($value=='4'){
					$pedidos['broch']=1;
				}
				if($value=='5'){
					$pedidos['vivo']=1;
				}
			}
			switch ($s) {
				case '1':
					$this->render('paso1', array('pedidos' => $pedidos, 'ubicacion' => $ubicacion,'c' => $c,'s' => $s));
					break;
				case '2':
					$this->render('paso2', array('pedidos' => $pedidos, 'ubicacion' => $ubicacion,'c' => $c,'s' => $s));
					break;
				case '3':
					$this->render('paso3', array('pedidos' => $pedidos, 'ubicacion' => $ubicacion,'c' => $c,'s' => $s));
					break;
				/*case '3':
					$this->render('paso3', array('pedidos' => $pedidos, 'ubicacion' => $ubicacion,'p' => $p,'c' => $c,'s' => $s));
					break;
				case '4':
					$this->render('paso4', array('pedidos' => $pedidos, 'ubicacion' => $ubicacion,'p' => $p,'c' => $c,'s' => $s));
					break;*/
				default:
					$this->render('paso1', array('pedidos' => $pedidos,'ubicacion' => $ubicacion,'c' => $c,'s' => $s));
					break;
			}
		}
	}
	public function actionEliminar()
	{
		if($_POST){
			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM solicitud  WHERE solic_codig ='".$codig."'";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				if($pedidos){
					$sql="SELECT * FROM solicitud_detalle  WHERE solic_codig ='".$codig."'";
					$detalle=$conexion->createCommand($sql)->queryRow();
					if(!$detalle){	

						$sql="DELETE FROM solicitud_adicional WHERE solic_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();

						$sql="DELETE FROM solicitud_deduccion WHERE solic_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();

						$sql="DELETE FROM solicitud_imagen WHERE solic_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();

						$sql="DELETE FROM solicitud WHERE solic_codig='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();

						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Pedido eliminado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al eliminar el Pedido');	
						}
						
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'El Pedido no se puede eliminar debido a que tiene modelos asociados');
					}		
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
				
			}catch(Exception $e){
				$transaction->rollBack();
				var_dump($e);
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM solicitud  a
			  WHERE a.solic_codig ='".$_GET['c']."'";
			$pedidos=$conexion->createCommand($sql)->queryRow();
			$this->render('eliminar', array('pedidos' => $pedidos,'c'=>$c));
		}
	}
	public function actionEnviar()
	{
		if($_POST){

			$codig=$_POST['codig'];
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM solicitud WHERE solic_codig ='".$codig."'";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				if($pedidos){
					if($pedidos['solic_pasos']=='3'){
						$sql="SELECT * FROM solicitud_detalle  WHERE solic_codig ='".$codig."'";
						$detalles=$conexion->createCommand($sql)->queryAll();
						if($detalles){

							foreach ($detalles as $key => $value) {
								$sql="SELECT * FROM solicitud_detalle_listado  WHERE pdeta_codig ='".$value['pdeta_codig']."'";
								$modelo=$conexion->createCommand($sql)->queryAll();
								if($modelo){
									if($value['pdeta_pasos']>='2'){
										$msg=array('success'=>'true','msg'=>' Detalle eliminado correctamente');
										$cmode++;
										$cpers+=count($modelo);	
									}else{
										$transaction->rollBack();
										$msg=array('success'=>'false','msg'=>'Termine todos los pasos de los modelos y el pedido para continuar');	
										echo json_encode($msg);
										exit();
									}
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Existe Un modelo sin p_personas asociadas verificar');	
									echo json_encode($msg);
									exit();
								}
							}
							if($msg['success']=='true'){
								$sql="UPDATE solicitud
									  SET solic_total='".$cpers."',
										  solic_cmode='".$cmode."',
										  epedi_codig='2'
									  WHERE solic_codig ='".$codig."'";
								$res1=$conexion->createCommand($sql)->execute();

								if($res1){
									$transaction->commit();
									$msg=array('success'=>'true','msg'=>' Pedido enviado correctamente');	
								}else{
									$transaction->rollBack();
									$msg=array('success'=>'false','msg'=>'Error al enviar el Pedido');	
								}
							}
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'El pedido no tiene modelos asociados');
						}
					}else{
						$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Debe finalizar el pedido antes de enviarlo');
					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
				
			}catch(Exception $e){

				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$p=$_GET['p'];
			$c=$_GET['c'];
			$s=$_GET['s'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM solicitud  a
			  WHERE a.solic_codig ='".$_GET['c']."'";
			$pedidos=$conexion->createCommand($sql)->queryRow();
			$this->render('enviar', array('pedidos' => $pedidos, 'ubicacion' => $ubicacion,'p' => $p,'c' => $c,'s' => $s));
		}
	}
	public function actionAprobar()
	{
		if($_POST){
			$codig=($_POST['codig']);
			$mcant=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['mcant']));
			$total=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['total']));
			$monto=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['monto']));
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM solicitud WHERE solic_codig ='".$codig."'";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				if($pedidos){
					if($pedidos['epedi_codig']=='2'){
						$sql="UPDATE solicitud
							  SET solic_total='".$total."',
								  solic_cmode='".$mcant."',
								  solic_monto='".$monto."',
								  epedi_codig='7'
							  WHERE solic_codig ='".$codig."'";
						$res1=$conexion->createCommand($sql)->execute();

						if($res1){
							$sql="SELECT a.*, b.*
								  FROM p_persona a
								  JOIN seguridad_usuarios b ON (a.perso_codig=b.perso_codig)
								  WHERE b.usuar_codig='".$pedidos['usuar_codig']."'";
                    		$p_persona=$conexion->createCommand($sql)->queryRow();

                    		$sql="SELECT * FROM solicitud WHERE solic_codig ='".$codig."'";
							$pedidos=$conexion->createCommand($sql)->queryRow();

							$datos['row']=$p_persona;
							$datos['totales']=$pedidos;
							$corre=$p_persona['usuar_login'];
                            $this->funciones->enviarCorreo('../pedidos/pedidos/aprobar_correo',$datos,'Polerones Tiempo | Pedido Aprobado',$corre);
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Pedido Aprobado correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al enviar el Pedido');	
						}
						
					}else{
						$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Debe envar el pedido antes de Aprobarlo');
					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
				
			}catch(Exception $e){

				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM solicitud  a
			  WHERE a.solic_codig ='".$_GET['c']."'";
			$pedidos=$conexion->createCommand($sql)->queryRow();
			$totales=$this->funciones->CalcularMontoPedido($pedidos);
			$this->render('aprobar', array('pedidos' => $pedidos, 'totales' => $totales, 'c'=>$c));
		}
	}
	public function actionRechazar()
	{
		if($_POST){
			$codig=($_POST['codigo']);
			$obser=mb_strtoupper($_POST['obser']);
			$motiv=mb_strtoupper($_POST['motiv']);
			$accio='2';
			$estat=16;
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM solicitud WHERE solic_codig ='".$codig."'";
				$solicitud=$conexion->createCommand($sql)->queryRow();
				if($solicitud){
					//if($pedidos['epedi_codig']=='2'){
					$sql="UPDATE solicitud
						  SET estat_codig='".$estat."',
						 	 accio_codig='".$accio."',
						 	 motiv_codig='".$motiv."',
						 	 solic_robse='".$obser."'
						  WHERE solic_codig ='".$codig."'";
					$query=$sql;
					$res1=$conexion->createCommand($sql)->execute();

					$sql="SELECT * FROM solicitud WHERE solic_codig ='".$codig."'";
					$solicitud2=$conexion->createCommand($sql)->queryRow();

					$opera='U';

					$antes=json_encode($solicitud);
					$actual=json_encode($solicitud2);
					$traza=$this->funciones->guardarTraza($conexion, $opera, 'solicitud', $antes,$actual,$query);
					

					$trayectoria=$this->funciones->guardarTrayectoria($conexion, $codig, $estat, $solicitud['estat_codig'], 2);
					
						if($accio=='2'){
							$movimiento=$this->funciones->guardarReasignacion($conexion, $codig, 1, $accio, $motiv, $obser);
							
						}
						//exit();
						$datos['row']=$solicitud2;
						$datos['accio']=$accio;
						$datos['motiv']=$motiv;
						$datos['obser']=$obser;
						$corre=$solicitud['prere_celec'];
                        
                        //$this->funciones->enviarCorreo('../registro/solicitudes/verificar_correo',$datos,'RapidPago | '.$titulo,$corre);


						$transaction->commit();
						$msg=array('success'=>'true','msg'=>' Solicitud Verificada Correctamente');	
					/*}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al eliminado la Deducción');	
					}*/
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'La solicitud no existe');
				}
				
			}catch(Exception $e){

				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM solicitud  a
			  WHERE a.solic_codig ='".$_GET['c']."'";
			$solicitud=$conexion->createCommand($sql)->queryRow();
			$this->render('rechazar', array('solicitud' => $solicitud, 'totales' => $totales, 'c'=>$c));
		}
	}


	public function actionCostos()
	{
		if($_POST){
			$codig=($_POST['codig']);
			$costo=mb_strtoupper($_POST['costo']);
			$obser=mb_strtoupper($_POST['obser']);
			$monto=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['monto']));
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM solicitud WHERE solic_codig ='".$codig."'";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				if($pedidos){
					if($pedidos['epedi_codig']=='2'){
						$sql="INSERT INTO solicitud_adicional(solic_codig, costo_codig, adici_monto, adici_obser, usuar_codig, adici_fcrea, adici_hcrea) 
							VALUES ('".$codig."','".$costo."','".$monto."','".$obser."','".$usuar."','".$fecha."','".$hora."');";
						$res1=$conexion->createCommand($sql)->execute();

						if($res1){
							$transaction->commit();
							$msg=array('success'=>'true','msg'=>' Costo Adicional agregado Correctamente');	
						}else{
							$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Error al agregar el Costo');	
						}
						
					}else{
						$transaction->rollBack();
							$msg=array('success'=>'false','msg'=>'Debe envar el pedido antes de asignar un costo adicional');
					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
				
			}catch(Exception $e){

				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$p=$_GET['p'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM solicitud  a
			  WHERE a.solic_codig ='".$_GET['c']."'";
			$pedidos=$conexion->createCommand($sql)->queryRow();
			$totales=$this->funciones->CalcularMontoPedido($pedidos);
			$this->render('costos', array('pedidos' => $pedidos, 'totales' => $totales, 'p'=>$p, 'c'=>$c));
		}
	}
	public function actionCostosEliminar()
	{
		if($_POST){
			$codig=($_POST['codig']);
			$costo=mb_strtoupper($_POST['costo']);
			$obser=mb_strtoupper($_POST['obser']);
			$monto=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['monto']));
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM solicitud_adicional WHERE adici_codig ='".$codig."'";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				if($pedidos){

					$sql="DELETE FROM solicitud_adicional WHERE adici_codig ='".$codig."'";
					$res1=$conexion->createCommand($sql)->execute();

					if($res1){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>' Costo Adicional eliminado Correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al eliminado el Costo');	
					}
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El adicional no existe');
				}
				
			}catch(Exception $e){

				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$p=$_GET['p'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM solicitud_adicional  a
			  WHERE a.adici_codig ='".$_GET['c']."'";
			$costos=$conexion->createCommand($sql)->queryRow();
			$totales=$this->funciones->CalcularMontoPedido($pedidos);
			$this->render('costosEliminar', array('costos' => $costos, 'totales' => $totales, 'c'=>$c, 'p'=>$p));
		}
	}

	public function actionDeduccion()
	{
		if($_POST){
			$codig=($_POST['codig']);
			$deduc=mb_strtoupper($_POST['deduc']);
			$obser=mb_strtoupper($_POST['obser']);
			$monto=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['monto']));
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM solicitud WHERE solic_codig ='".$codig."'";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				if($pedidos){
					$sql="SELECT * FROM solicitud_deduccion WHERE deduc_codig='".$deduc."' and solic_codig = '".$codig."'";
					$deduciones=$conexion->createCommand($sql)->queryRow();
					if(!$deduciones){
						if($pedidos['epedi_codig']=='2'){
							$sql="INSERT INTO solicitud_deduccion(solic_codig, deduc_codig, pdedu_monto, pdedu_obser, usuar_codig, pdedu_fcrea, pdedu_hcrea) 
								VALUES ('".$codig."','".$deduc."','".$monto."','".$obser."','".$usuar."','".$fecha."','".$hora."');";
							$res1=$conexion->createCommand($sql)->execute();

							if($res1){
								$transaction->commit();
								$msg=array('success'=>'true','msg'=>' Deducción agregado Correctamente');	
							}else{
								$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Error al agregar la Deducción');	
							}
							
						}else{
							$transaction->rollBack();
								$msg=array('success'=>'false','msg'=>'Debe envar el pedido antes de asignar una Deducción');
						}
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Ya se agrego la deducción');
					}
						
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
				
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$p=$_GET['p'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM solicitud  a
			  WHERE a.solic_codig ='".$_GET['c']."'";
			$pedidos=$conexion->createCommand($sql)->queryRow();
			$totales=$this->funciones->CalcularMontoPedido($pedidos);
			$this->render('deduccion', array('pedidos' => $pedidos, 'totales' => $totales, 'c'=>$c, 'p'=>$p));
		}
	}
	public function actionDeduccionEliminar()
	{
		if($_POST){
			$codig=($_POST['codig']);
			$deduc=mb_strtoupper($_POST['deduc']);
			$obser=mb_strtoupper($_POST['obser']);
			$monto=$this->funciones->TransformarMonto_bd(mb_strtoupper($_POST['monto']));
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
				$sql="SELECT * FROM solicitud_deduccion WHERE pdedu_codig ='".$codig."'";
				$pedidos=$conexion->createCommand($sql)->queryRow();
				if($pedidos){
						
					$sql="DELETE FROM solicitud_deduccion WHERE pdedu_codig ='".$codig."'";
					$res1=$conexion->createCommand($sql)->execute();

					if($res1){
						$transaction->commit();
						$msg=array('success'=>'true','msg'=>' Deducción eliminado Correctamente');	
					}else{
						$transaction->rollBack();
						$msg=array('success'=>'false','msg'=>'Error al eliminado la Deducción');	
					}
							
						
						
					
				}else{
					$transaction->rollBack();
					$msg=array('success'=>'false','msg'=>'El Pedido no existe');
				}
				
			}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$p=$_GET['p'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM solicitud_deduccion  a
			  WHERE a.pdedu_codig ='".$_GET['c']."'";
			$deduccion=$conexion->createCommand($sql)->queryRow();
			$totales=$this->funciones->CalcularMontoPedido($pedidos);
			$this->render('deduccionEliminar', array('deduccion' => $deduccion, 'totales' => $totales, 'c'=>$c, 'p'=>$p));
		}
	}
	public function actionEntregar()
	{
		if($_POST){
			$codig=mb_strtoupper($_POST['codigo']);
			$fentr=mb_strtoupper($this->funciones->transformarFecha_bd($_POST['fentr']));
			$tentr=mb_strtoupper($_POST['tentr']);
			$couri=mb_strtoupper($_POST['couri']);
			$agenc=mb_strtoupper($_POST['agenc']);
			$nguia=mb_strtoupper($_POST['nguia']);
			$direc=mb_strtoupper($_POST['direc']);
			$usuar=Yii::app()->user->id['usuario']['codigo'];
			$estat=28;
			$fecha=date('Y-m-d');
			$hora=date('H:i:s');
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();

			try{
				$sql="SELECT max(entre_codig) codigo FROM solicitud_entrega";
	        	$entrega=$conexion->createCommand($sql)->queryRow();
	        	$entrega['codigo']=$entrega['codigo']+1;
				$numero='NE-'.$this->funciones->generarCodigoPedido(6,$entrega['codigo']);
				
				$sql="SELECT * FROM solicitud WHERE solic_codig='".$codig."'";
	        	$solicitud=$conexion->createCommand($sql)->queryRow();

	        	$sql="UPDATE solicitud
	        		    SET estat_codig='".$estat."'
	        		  WHERE solic_codig='".$solicitud['solic_codig']."'";
	        	$query=$sql;

	        	$res1=$conexion->createCommand($sql)->execute();

	        	$sql="SELECT * FROM solicitud WHERE solic_codig='".$codig."'";
	        	$solicitud2=$conexion->createCommand($sql)->queryRow();

	        	$traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud', json_encode($solicitud), json_encode($solicitud2),$query);

	        	
						$notificacion=$this->funciones->GuardarNotificacion($solicitud['solic_codig'], $estat);
						
						$trayectoria=$this->funciones->guardarTrayectoriaSolicitudes($conexion, $solicitud['solic_codig'], $estat, $solicitud['estat_codig'], 2);

	        	$sql="SELECT * FROM solicitud_entrega WHERE solic_codig = '".$solicitud['solic_codig']."'";
	        	$entrega=$conexion->createCommand($sql)->queryRow();

	        	if(!$entrega){
						$sql="INSERT INTO solicitud_entrega(entre_numer, solic_codig, entre_fentr, tentr_codig, couri_codig, entre_agenc, entre_direc, entre_nguia, usuar_codig, entre_fcrea, entre_hcrea) 
						VALUES ('".$numero."','".$solicitud['solic_codig']."', '".$fentr."', '".$tentr."', '".$couri."',  '".$agenc."', '".$direc."', '".$nguia."',  '".$usuar."', '".$fecha."', '".$hora."')";	

					}else{
						$sql="UPDATE solicitud_entrega
						  	SET entre_fentr='".$fentr."',
							tentr_codig='".$tentr."',
							couri_codig='".$couri."',
							entre_agenc='".$agenc."',
							entre_direc='".$direc."',
							entre_nguia='".$nguia."'
						  	WHERE solic_codig ='".$codig."'";
						
					}
					$res1=$conexion->createCommand($sql)->execute();
					$transaction->commit();
							$msg=array('success'=>'true','msg'=>'Solicitud Entregada correctamente','destino'=>Yii::app()->request->getBaseUrl(true).'/reportes/despacho/notaEntrega?c='.$solicitud['solic_codig']);
        	}catch(Exception $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'false','msg'=>'Error al verificar la información');
			}
			echo json_encode($msg);
		}else{
			$c=$_GET['c'];
			$p=$_GET['p'];
			$conexion=Yii::app()->db;
			$sql="SELECT * 
			  FROM solicitud a
			  WHERE a.solic_codig ='".$_GET['c']."'";
			$solicitud=$conexion->createCommand($sql)->queryRow();
			$this->render('entregar',array('solicitud'=>$solicitud));
		}
			
	}
	public function actionVerificar()
	{
		$this->render('verificar');
	}
	public function actionAnular()
	{
		$this->render('anular');
	}
	public function actionAprobarDocumentacion()
	{
		if($_POST){
            
            $id=$_POST['id'];
            if(!$id){
                $msg=array('success'=>'false','msg'=>'Debe seleccionar al menos una solicitud');
                echo json_encode($msg);
                exit();
            }
            $usuar=Yii::app()->user->id['usuario']['codigo'];
            $fecha=date('Y-m-d');
            $hora=date('H:i:s');
            $estat=27;


            $conexion=Yii::app()->db;
            $transaction=$conexion->beginTransaction();
            try{
                foreach ($id as $key => $value) {

	        		$sql="SELECT * FROM solicitud WHERE solic_codig='".$value."'";
		        	$solicitud=$conexion->createCommand($sql)->queryRow();

		        	$sql="UPDATE solicitud
		        		    SET estat_codig='".$estat."',
		        		    	solic_monto='".$total."'
		        		  WHERE solic_codig='".$solicitud['solic_codig']."'";
		        	$query=$sql;

		        	$res1=$conexion->createCommand($sql)->execute();

		        	$sql="SELECT * FROM solicitud WHERE solic_codig='".$value."'";
		        	$solicitud2=$conexion->createCommand($sql)->queryRow();

		        	$traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud', json_encode($solicitud), json_encode($solicitud2),$query);

		        	
						$notificacion=$this->funciones->GuardarNotificacion($solicitud['solic_codig'], $estat);
						
						$trayectoria=$this->funciones->guardarTrayectoriaSolicitudes($conexion, $solicitud['solic_codig'], $estat, $solicitud['estat_codig'], 3);
		        }
		        

		        $transaction->commit();
		        $msg=array('success'=>'true','msg'=>'Solicitud Cerrada Correctamente');
                
            }catch(Exception $e){
                var_dump($e);
                $transaction->rollBack();
                $msg=array('success'=>'false','msg'=>'Error al verificar la información');
            }
            echo json_encode($msg);
        }else{
            $this->render('aprobarDocumentacion');
        }
	}
	public function actionReversarCargaParametros()
	{
		if($_POST){
            
            $id=$_POST['id'];
            if(!$id){
                $msg=array('success'=>'false','msg'=>'Debe seleccionar al menos una solicitud');
                echo json_encode($msg);
                exit();
            }
            $usuar=Yii::app()->user->id['usuario']['codigo'];
            $fecha=date('Y-m-d');
            $hora=date('H:i:s');
            $estat=19;


            $conexion=Yii::app()->db;
            $transaction=$conexion->beginTransaction();
            try{
                foreach ($id as $key => $value) {

	        		$sql="SELECT * FROM solicitud WHERE solic_codig='".$value."'";
		        	$solicitud=$conexion->createCommand($sql)->queryRow();

		        	$sql="UPDATE solicitud
		        		    SET estat_codig='".$estat."',
		        		    	solic_monto='".$total."'
		        		  WHERE solic_codig='".$solicitud['solic_codig']."'";
		        	$query=$sql;

		        	$res1=$conexion->createCommand($sql)->execute();

		        	$sql="SELECT * FROM solicitud WHERE solic_codig='".$value."'";
		        	$solicitud2=$conexion->createCommand($sql)->queryRow();

		        	$traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud', json_encode($solicitud), json_encode($solicitud2),$query);

		        	
						$notificacion=$this->funciones->GuardarNotificacion($solicitud['solic_codig'], $estat);
						
						$trayectoria=$this->funciones->guardarTrayectoriaSolicitudes($conexion, $solicitud['solic_codig'], $estat, $solicitud['estat_codig'], 2);
		        }
		        

		        $transaction->commit();
		        $msg=array('success'=>'true','msg'=>'Carga de Parametros Aprobada correctamente');
                
            }catch(Exception $e){
                var_dump($e);
                $transaction->rollBack();
                $msg=array('success'=>'false','msg'=>'Error al verificar la información');
            }
            echo json_encode($msg);
        }else{
            $this->render('reversarCargaParametros');
        }
	}
	public function actionAprobarVportal()
	{
		if($_POST){
            
            $id=$_POST['id'];
            if(!$id){
                $msg=array('success'=>'false','msg'=>'Debe seleccionar al menos una solicitud');
                echo json_encode($msg);
                exit();
            }
            $usuar=Yii::app()->user->id['usuario']['codigo'];
            $fecha=date('Y-m-d');
            $hora=date('H:i:s');
            $estat=21;


            $conexion=Yii::app()->db;
            $transaction=$conexion->beginTransaction();
            try{
                foreach ($id as $key => $value) {

	        		$sql="SELECT * FROM solicitud WHERE solic_codig='".$value."'";
		        	$solicitud=$conexion->createCommand($sql)->queryRow();

		        	$sql="UPDATE solicitud
		        		    SET estat_codig='".$estat."',
		        		    	solic_monto='".$total."'
		        		  WHERE solic_codig='".$solicitud['solic_codig']."'";
		        	$query=$sql;

		        	$res1=$conexion->createCommand($sql)->execute();

		        	$sql="SELECT * FROM solicitud WHERE solic_codig='".$value."'";
		        	$solicitud2=$conexion->createCommand($sql)->queryRow();

		        	$traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud', json_encode($solicitud), json_encode($solicitud2),$query);

		        	
						$notificacion=$this->funciones->GuardarNotificacion($solicitud['solic_codig'], $estat);
						
						$trayectoria=$this->funciones->guardarTrayectoriaSolicitudes($conexion, $solicitud['solic_codig'], $estat, $solicitud['estat_codig'], 2);
		        }
		        

		        $transaction->commit();
		        $msg=array('success'=>'true','msg'=>'Alta en V-PORTAl Aprobada correctamente');
                
            }catch(Exception $e){
                var_dump($e);
                $transaction->rollBack();
                $msg=array('success'=>'false','msg'=>'Error al verificar la información');
            }
            echo json_encode($msg);
        }else{
            $this->render('aprobarVportal');
        }
	}
	public function actionReversarVportal()
	{
		if($_POST){
            
            $id=$_POST['id'];
            if(!$id){
                $msg=array('success'=>'false','msg'=>'Debe seleccionar al menos una solicitud');
                echo json_encode($msg);
                exit();
            }
            $usuar=Yii::app()->user->id['usuario']['codigo'];
            $fecha=date('Y-m-d');
            $hora=date('H:i:s');
            $estat=20;


            $conexion=Yii::app()->db;
            $transaction=$conexion->beginTransaction();
            try{
                foreach ($id as $key => $value) {

	        		$sql="SELECT * FROM solicitud WHERE solic_codig='".$value."'";
		        	$solicitud=$conexion->createCommand($sql)->queryRow();

		        	$sql="UPDATE solicitud
		        		    SET estat_codig='".$estat."',
		        		    	solic_monto='".$total."'
		        		  WHERE solic_codig='".$solicitud['solic_codig']."'";
		        	$query=$sql;

		        	$res1=$conexion->createCommand($sql)->execute();

		        	$sql="SELECT * FROM solicitud WHERE solic_codig='".$value."'";
		        	$solicitud2=$conexion->createCommand($sql)->queryRow();

		        	$traza=$this->funciones->guardarTraza($conexion, 'U', 'solicitud', json_encode($solicitud), json_encode($solicitud2),$query);

		        	
						$notificacion=$this->funciones->GuardarNotificacion($solicitud['solic_codig'], $estat);
						
						$trayectoria=$this->funciones->guardarTrayectoriaSolicitudes($conexion, $solicitud['solic_codig'], $estat, $solicitud['estat_codig'], 2);
		        }
		        

		        $transaction->commit();
		        $msg=array('success'=>'true','msg'=>'Alta en V-PORTAl reversada correctamente');
                
            }catch(Exception $e){
                var_dump($e);
                $transaction->rollBack();
                $msg=array('success'=>'false','msg'=>'Error al verificar la información');
            }
            echo json_encode($msg);
        }else{
            $this->render('reversarVportal');
        }
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}