<?php

class RecargaController extends Controller
{
	
public function actionReporteRecarga()
	{

		//$cedula = $_GET['ced'];

		$sql="SELECT * from admin_uvencar";
		$conexion=yii::app()->db;
		$result=$conexion->createCommand($sql)->queryRow();
		//$model=Reportes::model()->find("Reportes_serial = '".$_REQUEST['codigo']."'");

		$fecha = explode('-',$result['banco_fcrea']);
		$fecha = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];
		

		$this->layout = 'pdf/Reportes';
		$mPDF1 = Yii::app()->ePdf->mpdf('', 'Letter','','',15,15,25,45);
		$html='<table width="100%" style="margin-top:25px">
			  <tr>
			  <td align="center" ><h1 align="right">Bancos registrados en UVEN</h6></td>
			  </tr>
			  <tr>
			  </tr>
			  </table>
			  <table width="100%" >
			  <tr>
		  <td align="lefth"><h3 align="lefth"></h3></td>
		  </tr>
		  <tr>
		  <td align="lefth"><h3 align="lefth"></h3></td>
		  </tr>
		  </table>
		  
		  
		  <table width="100%" border="1" >
			 
		  <tr>
		  <td align="lefth" ><h6 align="lefth"><b>Nombre del Banco</b></h6></td>
		  <td align="lefth" ><h6 align="lefth"><b>Número</b></h6></td>
		  <td align="lefth" ><h6 align="lefth"><b>URL</b></h6></td>
		  <td align="lefth" ><h6 align="lefth"><b>Estatus</b></h6></td>
		  <td align="lefth" ><h6 align="lefth"><b>Fecha de Creación</b></h6></td>
		  </tr>';

		  $sql="select * from admin_banco";
		  	$conexion=yii::app()->db;
			$result=$conexion->createCommand($sql)->queryAll();
			if($result!=''){
		    	foreach ($result as $key => $row) {
		    		$fecha = explode('-',$row['banco_fcrea']);
		    		$fecha = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];
		    		# code...
		    		$estatus = "Activo";
		    		if($row['banco_statu']==0){
		    			$estatus = "Inactivo";
		    		}
					$html.='<tr>
		    		<td align="lefth" ><h6 align="lefth">'.$row['banco_descr'].'</h6></td>
		    		<td align="lefth" ><h6 align="lefth">'.$row['banco_numer'].'</h6></td>
		    		<td align="lefth" ><h6 align="lefth">'.$row['banco_urlur'].'</h6></td>
		    		<td align="lefth" ><h6 align="lefth">'.$estatus.'</h6></td>
		    		<td align="lefth" ><h6 align="lefth">'.$fecha.'</h6></td>
		    		</tr>';
		    	}
		    }else{
		    	$html.='<tr>
				<td align="lefth" ><h6 align="lefth">No existen registros</h6></td>
		    	</tr>';

		    }

		  $html.='</table>
		 
		  </div>';
		
		$mPDF1->SetHTMLHeader('
		  
		  <table width="100%" border="0" style="margin-top:5px">
		  <tr>
		 <td align="left"><img src="./assets/img/logo_reportes.jpg" /> </td>
		  <td align="right"><h6 align="right">Página {PAGENO} de {nbpg}</h6></td>
		  </tr>
		  
		  </table>');

		$mPDF1->SetHTMLFooter('<div id="footer">
		<div style="text-align: center; font-size:11px">
		    <p>UVEN</p>
		    <p>Los Chaguaramos. Caracas. Teléfono: (0212)-661/73/36 </p>
		    <p>Correo Electrónico: uven@uven.com.ve </p>
		</div>
		</div>');
		$mPDF1->WriteHTML($html);
		$mPDF1->SetMargins(50, 45, 30);
		$mPDF1->SetAutoPageBreak(true, 10);

		$nombre=str_replace('/', '-', 'ReporteBanco');			
		$mPDF1->Output($nombre.'.pdf','I');

	}
	public function actionlista()
	{
		
		
			$this->render('lista');
		
	
	}
public function actionListaEliminar()
	{
		if($_POST){
			$conexion=Yii::app()->db;
			$transaction=$conexion->beginTransaction();
			try{
							
				$sql="CALL sp_admin_uvencar('".$_POST['c']."','','','','','','D')";
				//echo $sql;
				//exit;
				$result=$conexion->createCommand($sql)->queryRow();
				//var_dump($result);
				$transaction->commit();
				$msg=array('success'=>$result['success'],'msg'=>$result['mensaje']);
			}catch (CDbException $e){
				var_dump($e);
				$transaction->rollBack();
				$msg=array('success'=>'true','msg'=>'Error al Eliminar la Tarifa');
			}
			echo json_encode($msg);
		}else if($_GET){
			$sql="SELECT * FROM admin_banco ";
			$conexion=yii::app()->db;
            $result=$conexion->createCommand($sql)->queryRow();
            
            $this->render('eliminar_lista',array('model'=>$result,'c'=>$_GET['c']));
			
			//$this->render('eliminar',array('model'=>$result));
		}else{
			$this->render('lista',array('model'=>$result));
		}
	}

public function actionModal2() {
        ?>
        <form method='post' id='modaleliminar'>
            <input type="hidden" id='c' name='c' value='<?php echo $_REQUEST['c'] ?>'> 
            <input type="hidden" id='t' name='t' value='<?php echo $_REQUEST['t'] ?>'>            
            ¿Está seguro de querer eliminar la Recarga ?
            <br>
            <br>
            <button id='enviar' aprobar="true" estatus="2" class="btn btn-block btn-danger" type="button" archivo='<?php echo $nombre ?>' ><i class="fa fa-check"></i>Si</button>

        </form>
        <script>
            $('#enviar').click(function (e) {
                var formData = new FormData($("#modaleliminar")[0]);
                var c = document.getElementById("c").value;
                var t = document.getElementById("t").value;
                $.ajax({
                    dataType: "json",
                    'data': formData,
                    url: 'ListaEliminar',
                    type: 'post',
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $("#resultado").html("Procesando, espere por favor...");
                    },
                    success: function (response) {
                        //alert(response['success']);
                        if (response['success'] == 'true') {
                            $('#cerrar').click();
                            bootbox.dialog({
                                message: response['msg'],
                                title: "Exito!",
                                buttons: {
                                    danger: {
                                        label: "Cerrar",
                                        className: "btn-uven",
                                        callback: function(){
                                        	window.open('lista', '_parent');
                                        },
                                    },
                                }
                            });
                            $('.close').click( function(){
                            	window.open('lista', '_parent');
                            });
                        } else {

                            bootbox.dialog({
                                message: response['msg'],
                                title: "Información!",
                                buttons: {
                                    danger: {
                                        label: "Cerrar",
                                        className: "btn-uven",
                                    },
                                }
                            });
                           
                        }

                    }
                });
            });

        </script>

        <?php
    }

public function actionModalRecargaTaxista()
	{

		echo '<div class="modal-header">';
        
        
		echo 'Forma de Pago';
		
        echo '</h4>';
        echo '</div>';
        echo '<form id="form-modal2">';
        echo '<div class="modal-body">';
		
		$conexion=yii::app()->db;
		
		switch ($_REQUEST['t']) {
			case '1':
			case '2':
			case '3':
				?>

				<div class="row">
					<div class="col-md-12">
						<?php
                        $sql="SELECT * FROM admin_tgenerica 
                        WHERE tgene_tipod = 'FORPA' and tgene_codig  in (36,37)";
                        $conexion=Yii::app()->db;
                        $formapago=$conexion->createCommand($sql)->query();
                        ?>
					    <div class="funkyradio">
					        <div class="row">
						        <?php 
						        	$i=1;
			                        while ( $row=$formapago->read()) {
			                        	
			                        	echo '<div class="col-md-6">
										        <div class="funkyradio-uven">
										            <input type="radio" name="radio" id="radio'.$i.'" value="'.$row['tgene_codig'].'"/>
										            <label for="radio'.$i.'">'.$row['tgene_descr'].'</label>
										        </div>
									        </div>';
									    $i++;
			                        }
			                    ?>
					       </div>
					    </div>
					</div>
		        </div>
		        
		        
					<input type="hidden" name="t" value="<?php echo $_REQUEST['t'];?>">
					<input type="hidden" name="c" value="<?php echo $_REQUEST['c'];?>">
					<input type="hidden" name="u" value="<?php echo yii::app()->user->id['usuario']->usuar_codig;?>">

				
	            	<div class="modal-footer">
		            	<?php 
		            	if($servicio['perso_taxis']==0){
		            		?>
		            		<input type="hidden" name="operacion" value="I">
		            		<button  id='operacion' type="button" class="btn btn-uven" >Aceptar</button>
		            		<?php
		            	}
		            	?>
		            	<button  id='cerrar' type="button" class="btn btn-uven" data-dismiss="modal">Cancelar</button>  
		            </div>
	            </form>
		        <?php
				break;
			case '5':
				?>

				<div class="row">
					<div class="col-md-12">

					    <div class="funkyradio">
					        <div class="row">
						        <div class="col-md-6">
							        <div class="funkyradio-uven">
							            <input type="radio" name="radio" id="radio2" value="34"/>
							            <label for="radio2">Efectivo</label>
							        </div>
						        </div>
						        <div class="col-md-6">
							        <div class="funkyradio-uven">
							            <input type="radio" name="radio" id="radio3" value="39" />
							            <label for="radio3">Punto de Venta</label>
							        </div>
						        </div>
					       </div>
					    </div>
					</div>
		        </div>
		        
		        
					<input type="hidden" name="t" value="<?php echo $_REQUEST['t'];?>">
					<input type="hidden" name="c" value="<?php echo $_REQUEST['c'];?>">
					<input type="hidden" name="u" value="<?php echo yii::app()->user->id['usuario']->usuar_codig;?>">

				
	            	<div class="modal-footer">
		            	<?php 
		            	if($servicio['perso_taxis']==0){
		            		?>
		            		<input type="hidden" name="operacion" value="I">
		            		<button  id='operacion' type="button" class="btn btn-uven" >Aceptar</button>
		            		<?php
		            	}
		            	?>
		            	<button  id='cerrar' type="button" class="btn btn-uven" data-dismiss="modal">Cancelar</button>  
		            </div>
	            </form>
		        <?php
				break;
			
			case '2':
				$sql="";
				//$banco=$conexion->createCommand($sql)->queryRow();
		        ?>
			        <p class="text-center"><b>Las Transferencias deben ser del mismo banco</b></p>
			        <div class="row">
			        	<div class="col-md-6">
			        		<form id="form-banco">
				        		<div class="form-group">
				        			<label> Seleccione un Banco</label>
				        			<select class="form-control" name="banco" id="banco">
				        				<option value="">Seleccione</option>
				        				<option value="1">Banesco</option>
				        			</select>
				        		</div>
			        		</form>
			        	</div>
			        </div>
		        </div>
		        <div class='table-responsive' id="tabla-banco">
				
				</div>
				<script type="text/javascript">
            		$('#banco').change(function (e) {
            			var formData = new FormData($("#form-banco")[0]);
            			
            			$.ajax({
                			url: 'TablaBanco',
                			type: 'POST',
                			data: formData,
                			cache: false,
                			contentType: false,
                			processData: false,
	    	            	beforeSend: function () {
		                    	$("#resultado").html("Procesando, espere por favor...");
	                		},
	                		success: function (html) {
	                    		$('#tabla-banco').html(html);
			                }
			            });

			        });
		        </script>
		        <div class="modal-header">
		        	<h4 class="modal-title">
		        		Datos de la Transferencia
		        	</h4>
		        </div>
		        <form id='form-modal2' class='form-horizontal'>
		        	<div class="modal-body">
		        		<div class="form-group">
		        			<div class="col-md-2">
		        				<label class='control-label'>Fecha</label>
		        			</div>
		        			<div class="col-md-10">
		        			<input type="text" class='form-control' name="fecha" id="fecha" value="<?php echo date('d/m/Y')?>" readonly>
		        			</div>
		        		</div>
		        		<div class="form-group">
		        			<div class="col-md-2">
		        				<label class='control-label'>Monto</label>
		        			</div>
		        			<div class="col-md-10">
		        			<input type="text" class='form-control' name="monto" id="fecha" value="0,00" readonly>
		        			</div>
		        		</div>
		        		<div class="form-group">
		        			<div class="col-md-2">
		        				<label class='control-label'>Referencia</label>
		        			</div>
		        			<div class="col-md-10">
		        			<input type="text" class='form-control' name="referencia" id="referencia">
		        			</div>
		        		</div>
		        	
					<input type="hidden" name="t" value="<?php echo $_REQUEST['t'];?>">
					<input type="hidden" name="c" value="<?php echo $_REQUEST['c'];?>">
					<input type="hidden" name="u" value="<?php echo yii::app()->user->id['usuario']->usuar_codig;?>">
					</div>
	            	<div class="modal-footer">
		            	<input type="hidden" name="operacion" value="I">
		            	<button  id='operacion' type="button" class="btn btn-uven" >Aceptar</button>
		            	<button  id='cerrar' type="button" class="btn btn-uven" data-dismiss="modal">Cerrar</button>  
		            </div>

	            </form>

		        <?php
				break;
			default:
				# code...
				break;
		}
		?>
		<script type="text/javascript">
		$(document).ready(function () {
	        $('#form-modal2').bootstrapValidator({
	            message: 'No es un valor valido',
	            feedbackIcons: {
	                valid: 'glyphicon glyphicon-ok',
	                invalid: 'glyphicon glyphicon-remove',
	                validating: 'glyphicon glyphicon-refresh'
	            },
	            fields: {
	                monto:{
	                    validators: {
	                        notEmpty: {
                            	message: 'Este campo es obligatorio',
                        	},

	                    }
	                },
	                fecha:{
	                    validators: {
	                        notEmpty: {
                            	message: 'Este campo es obligatorio',
                        	},

	                    }
	                },
	                referencia: {
	                    validators: {
	                        notEmpty: {
	                            message: 'Este campo es obligatorio',
	                        },
	                    }
	                },
	                
	            }
	        
	        });
	    });
        $('#operacion').click(function (e) {
            var formData = new FormData($("#form-modal2")[0]);
            e.preventDefault();
            $('#form-modal2').bootstrapValidator('validate'); 
        	var bootstrapValidator = $('#form-modal2').data('bootstrapValidator');
        	if (bootstrapValidator.isValid()) {
            $.ajax({
                dataType: "json",
                url: 'EstatusPago',
                type: 'POST',
                cache: false,
                data: formData,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $("#resultado").html("Procesando, espere por favor...");
                },
                success: function (response) {
                    //alert(response['success']);
                    if (response['success'] == 'true') {
                    	//alert('hola');
                        bootbox.dialog({
                            message: response['msg'],
                            title: "Exito!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                    callback: function(){
                                       window.open('servicio', '_parent');
                                    }
                                },
                            }
                        });
                        $('.close').click( function(){
                            window.open('servicio', '_parent');
                        });
                    }else{
                    	//alert('error');
                        bootbox.dialog({
			        		message: response['msg'],
			                title: "Información!",
			                buttons: {
			                	danger: {
			                    	label: "Cerrar",
			                        className: "btn-uven",
			                    },
			                }
			            });
                        
                        // $("#subir").removeAttr('disabled');
                    }

                }
            });
        }else{
        	bootbox.dialog({
        		message: 'Verifique todos los campos antes de continuar',
                title: "Información!",
                buttons: {
                	danger: {
                    	label: "Cerrar",
                        className: "btn-uven",
                    },
                }
            });
        }

        });


        
        </script>
        <?php
	}


public function actionEstatusPago() {
		//var_dump($_POST);
		$estatus  = $_REQUEST['estatus'];
        $codigo   = $_REQUEST['c'];
        $tipopago = $_REQUEST['radio'];
        
		if($tipopago>0){
			switch ($tipopago) {
				case '35':
					# va a cancelar en efectivo
					$sql = "update admin_servicios
					set tgene_fpago = ".$tipopago.", tgene_statu = 3
					where servi_codig = ".$codigo;

					break;
				case '36':
					# va a cancelar en TDC
					break;
				case '37':
					# va a cancelar por transferencia
					echo '<div class="modal-header">';
			        echo '<h4 class="modal-title" id="myModalLabel">';
			        echo 'Pago por Transferencia';
					echo '</h4>';
			        echo '</div>';
			        echo '<form id="form-modal2">';
			        echo '<div class="modal-body">';
					
					$conexion=yii::app()->db;
							
					$sql="";
					//$banco=$conexion->createCommand($sql)->queryRow();
			        ?>
				        <p class="text-center"><b>Las Transferencias deben ser del mismo banco</b></p>
				        <div class="row">
				        	<div class="col-md-6">
				        		<form id="form-banco">
					        		<div class="form-group">
					        			<label> Seleccione un Banco</label>
					        			<select class="form-control" name="banco" id="banco">
					        				<option value="">Seleccione</option>
					        				<option value="1">Banesco</option>
					        			</select>
					        		</div>
				        		</form>
				        	</div>
				        </div>
			        </div>
			        <div class='table-responsive' id="tabla-banco">
					
					</div>
					<script type="text/javascript">
	            		$('#banco').change(function (e) {
	            			var formData = new FormData($("#form-banco")[0]);
	            			
	            			$.ajax({
	                			url: 'TablaBanco',
	                			type: 'POST',
	                			data: formData,
	                			cache: false,
	                			contentType: false,
	                			processData: false,
		    	            	beforeSend: function () {
			                    	$("#resultado").html("Procesando, espere por favor...");
		                		},
		                		success: function (html) {
		                    		$('#tabla-banco').html(html);
				                }
				            });

				        });
			        </script>
			        <div class="modal-header">
			        	<h4 class="modal-title">
			        		Datos de la Transferencia
			        	</h4>
			        </div>
			        <form id='form-modal2' class='form-horizontal'>
			        	<div class="modal-body">
			        		<div class="form-group">
			        			<div class="col-md-2">
			        				<label class='control-label'>Fecha</label>
			        			</div>
			        			<div class="col-md-10">
			        			<input type="text" class='form-control' name="fecha" id="fecha" value="<?php echo date('d/m/Y')?>" readonly>
			        			</div>
			        		</div>
			        		<div class="form-group">
			        			<div class="col-md-2">
			        				<label class='control-label'>Monto</label>
			        			</div>
			        			<div class="col-md-10">
			        			<input type="text" class='form-control' name="monto" id="fecha" value="0,00" readonly>
			        			</div>
			        		</div>
			        		<div class="form-group">
			        			<div class="col-md-2">
			        				<label class='control-label'>Referencia</label>
			        			</div>
			        			<div class="col-md-10">
			        			<input type="text" class='form-control' name="referencia" id="referencia">
			        			</div>
			        		</div>
			        	
						<input type="hidden" name="t" value="<?php echo $_REQUEST['t'];?>">
						<input type="hidden" name="c" value="<?php echo $_REQUEST['c'];?>">
						<input type="hidden" name="u" value="<?php echo yii::app()->user->id['usuario']->usuar_codig;?>">
						</div>
		            	<div class="modal-footer">
			            	<input type="hidden" name="operacion" value="I">
			            	<button  id='operacion' type="button" class="btn btn-uven" >Aceptar</button>
			            	<button  id='cerrar' type="button" class="btn btn-uven" data-dismiss="modal">Cerrar</button>  
			            </div>

		            </form>

					<script type="text/javascript">
					$(document).ready(function () {
				        $('#form-modal2').bootstrapValidator({
				            message: 'No es un valor valido',
				            feedbackIcons: {
				                valid: 'glyphicon glyphicon-ok',
				                invalid: 'glyphicon glyphicon-remove',
				                validating: 'glyphicon glyphicon-refresh'
				            },
				            fields: {
				                monto:{
				                    validators: {
				                        notEmpty: {
			                            	message: 'Este campo es obligatorio',
			                        	},

				                    }
				                },
				                fecha:{
				                    validators: {
				                        notEmpty: {
			                            	message: 'Este campo es obligatorio',
			                        	},

				                    }
				                },
				                referencia: {
				                    validators: {
				                        notEmpty: {
				                            message: 'Este campo es obligatorio',
				                        },
				                    }
				                },
				                
				            }
				        
				        });
				    });
			        $('#operacion').click(function (e) {
			            var formData = new FormData($("#form-modal2")[0]);
			            e.preventDefault();
			            $('#form-modal2').bootstrapValidator('validate'); 
			        	var bootstrapValidator = $('#form-modal2').data('bootstrapValidator');
			        	if (bootstrapValidator.isValid()) {
			            $.ajax({
			                dataType: "json",
			                url: 'EstatusPago',
			                type: 'POST',
			                cache: false,
			                data: formData,
			                contentType: false,
			                processData: false,
			                beforeSend: function () {
			                    $("#resultado").html("Procesando, espere por favor...");
			                },
			                success: function (response) {
			                    //alert(response['success']);
			                    if (response['success'] == 'true') {
			                        bootbox.dialog({
			                            message: response['msg'],
			                            title: "Exito!",
			                            buttons: {
			                                danger: {
			                                    label: "Cerrar",
			                                    className: "btn-uven",
			                                    callback: function(){
			                                       window.open('servicio', '_parent');
			                                    }
			                                },
			                            }
			                        });
			                        $('.close').click( function(){
			                            window.open('servicio', '_parent');
			                        });
			                    }else{
			                    	//alert('error');
			                        bootbox.dialog({
						        		message: response['msg'],
						                title: "Información!",
						                buttons: {
						                	danger: {
						                    	label: "Cerrar",
						                        className: "btn-uven",
						                    },
						                }
						            });
			                        
			                        // $("#subir").removeAttr('disabled');
			                    }

			                }
			            });
			        }else{
			        	bootbox.dialog({
			        		message: 'Verifique todos los campos antes de continuar',
			                title: "Información!",
			                buttons: {
			                	danger: {
			                    	label: "Cerrar",
			                        className: "btn-uven",
			                    },
			                }
			            });
			        }

			        });


			        
			        </script>

					<?php
					break;
				case '38':
					# va a cancelar con UVENCAR
					break;
				default:
					# code...
					break;

			
			}
			$conexion = Yii::app()->db;


			
			$command = $conexion->createCommand($sql);
	        $dataReader = $command->execute();


	        $sqlEstatus = "select * from admin_servicios s 
			inner join admin_tgenerica e on (s.tgene_statu = e.tgene_codig)
			where servi_codig = ".$codigo;
			$est =$conexion->createCommand($sqlEstatus)->queryRow();
			$mensajeEstatus = $est['tgene_descr'];

	        if ($dataReader) {
	            //var_dump($dataReader);
	            $msg = array('success' => 'true','msg' => $mensajeEstatus );
	        } else {
	            $msg = array('success' => 'false','msg' => $mensajeEstatus );
	        }
	    }else{
	    	$msg = array('success' => 'false','msg' => 'Debe indicar una forma de pago' );

	    }

        echo json_encode($msg);

        //echo $sql;
        //exit();
        
       
    }

}