<?php

/**
 * This is the model class for table "admin_corporativo".
 *
 * The followings are the available columns in table 'admin_corporativo':
 * @property integer $corpo_codig
 * @property string $corpo_norif
 * @property string $corpo_nombr
 * @property integer $estad_codig
 * @property integer $munic_codig
 * @property integer $parro_codig
 * @property string $corpo_direc
 * @property string $corpo_telof
 * @property string $corpo_telot
 * @property integer $corpo_conta
 * @property string $conpo_email
 * @property string $corpo_clave
 */
class Corporativo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'admin_corporativo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('corpo_norif, corpo_nombr, estad_codig, munic_codig, parro_codig, corpo_direc, corpo_telof, corpo_telot, corpo_conta, conpo_email, corpo_clave', 'required'),
			array('estad_codig, munic_codig, parro_codig, corpo_conta', 'numerical', 'integerOnly'=>true),
			array('corpo_norif, corpo_telof, corpo_telot', 'length', 'max'=>20),
			array('conpo_email', 'length', 'max'=>50),
			array('corpo_clave', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('corpo_codig, corpo_norif, corpo_nombr, estad_codig, munic_codig, parro_codig, corpo_direc, corpo_telof, corpo_telot, corpo_conta, conpo_email, corpo_clave', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'corpo_codig' => 'Corpo Codig',
			'corpo_norif' => 'Corpo Norif',
			'corpo_nombr' => 'Corpo Nombr',
			'estad_codig' => 'Estad Codig',
			'munic_codig' => 'Munic Codig',
			'parro_codig' => 'Parro Codig',
			'corpo_direc' => 'Corpo Direc',
			'corpo_telof' => 'Corpo Telof',
			'corpo_telot' => 'Corpo Telot',
			'corpo_conta' => 'Corpo Conta',
			'conpo_email' => 'Conpo Email',
			'corpo_clave' => 'Corpo Clave',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('corpo_codig',$this->corpo_codig);
		$criteria->compare('corpo_norif',$this->corpo_norif,true);
		$criteria->compare('corpo_nombr',$this->corpo_nombr,true);
		$criteria->compare('estad_codig',$this->estad_codig);
		$criteria->compare('munic_codig',$this->munic_codig);
		$criteria->compare('parro_codig',$this->parro_codig);
		$criteria->compare('corpo_direc',$this->corpo_direc,true);
		$criteria->compare('corpo_telof',$this->corpo_telof,true);
		$criteria->compare('corpo_telot',$this->corpo_telot,true);
		$criteria->compare('corpo_conta',$this->corpo_conta);
		$criteria->compare('conpo_email',$this->conpo_email,true);
		$criteria->compare('corpo_clave',$this->corpo_clave,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Corporativo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
