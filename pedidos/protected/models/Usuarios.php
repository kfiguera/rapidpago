<?php

/**
 * This is the model class for table "seguridad_usuarios".
 *
 * The followings are the available columns in table 'seguridad_usuarios':
 * @property integer $usuar_codig
 * @property integer $perso_codig
 * @property string $usuar_login
 * @property string $usuar_passw
 * @property integer $urole_codig
 * @property integer $uesta_codig
 * @property string $usuar_obser
 *
 * The followings are the available model relations:
 * @property UsuariosEstatus $uestaCodig
 * @property UsuariosRoles $uroleCodig
 * @property Voceros[] $voceroses
 */
class Usuarios extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'seguridad_usuarios';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('perso_codig, usuar_login, usuar_passw, urole_codig, uesta_codig, usuar_obser', 'required'),
			array('perso_codig, urole_codig, uesta_codig', 'numerical', 'integerOnly'=>true),
			array('usuar_login, usuar_passw', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('usuar_codig, perso_codig, usuar_login, usuar_passw, urole_codig, uesta_codig, usuar_obser', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'uestaCodig' => array(self::BELONGS_TO, 'UsuariosEstatus', 'uesta_codig'),
			'uroleCodig' => array(self::BELONGS_TO, 'UsuariosRoles', 'urole_codig'),
			'voceroses' => array(self::HAS_MANY, 'Voceros', 'usuar_codig'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'usuar_codig' => 'Usuar Codig',
			'perso_codig' => 'Perso Codig',
			'usuar_login' => 'Usuar Login',
			'usuar_passw' => 'Usuar Passw',
			'urole_codig' => 'Urole Codig',
			'uesta_codig' => 'Uesta Codig',
			'usuar_obser' => 'Usuar Obser',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('usuar_codig',$this->usuar_codig);
		$criteria->compare('perso_codig',$this->perso_codig);
		$criteria->compare('usuar_login',$this->usuar_login,true);
		$criteria->compare('usuar_passw',$this->usuar_passw,true);
		$criteria->compare('urole_codig',$this->urole_codig);
		$criteria->compare('uesta_codig',$this->uesta_codig);
		$criteria->compare('usuar_obser',$this->usuar_obser,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Usuarios the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
