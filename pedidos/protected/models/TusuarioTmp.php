<?php

/**
 * This is the model class for table "tusuario_tmp".
 *
 * The followings are the available columns in table 'tusuario_tmp':
 * @property integer $tusua_codig
 * @property string $tusua_corre
 * @property integer $tgene_permi
 * @property string $tpers_nacio
 * @property integer $tpers_cedul
 * @property integer $tusua_entec
 * @property integer $tusua_statu
 * @property string $tusua_corr2
 * @property string $tusua_ente
 * @property string $tusua_cargo
 * @property string $tusua_passw
 * @property string $tusua_verif
 * @property string $tusua_obser
 * @property string $tusua_telef
 * @property string $tusua_fcrea
 *
 * The followings are the available model relations:
 * @property Tp_persona $tpersNacio
 * @property Tp_persona $tpersCedul
 */
class TusuarioTmp extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tusuario_tmp';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tgene_permi, tpers_nacio, tpers_cedul, tusua_statu', 'required'),
			array('tgene_permi, tpers_cedul, tusua_entec, tusua_statu', 'numerical', 'integerOnly'=>true),
			array('tusua_corre, tusua_corr2, tusua_cargo, tusua_passw, tusua_verif', 'length', 'max'=>50),
			array('tpers_nacio', 'length', 'max'=>2),
			array('tusua_ente', 'length', 'max'=>300),
			array('tusua_telef', 'length', 'max'=>15),
			array('tusua_obser, tusua_fcrea', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('tusua_codig, tusua_corre, tgene_permi, tpers_nacio, tpers_cedul, tusua_entec, tusua_statu, tusua_corr2, tusua_ente, tusua_cargo, tusua_passw, tusua_verif, tusua_obser, tusua_telef, tusua_fcrea', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tpersNacio' => array(self::BELONGS_TO, 'Tp_persona', 'tpers_nacio'),
			'tpersCedul' => array(self::BELONGS_TO, 'Tp_persona', 'tpers_cedul'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tusua_codig' => 'Tusua Codig',
			'tusua_corre' => 'Tusua Corre',
			'tgene_permi' => 'Tgene Permi',
			'tpers_nacio' => 'Tpers Nacio',
			'tpers_cedul' => 'Tpers Cedul',
			'tusua_entec' => 'Tusua Entec',
			'tusua_statu' => 'Tusua Statu',
			'tusua_corr2' => 'Tusua Corr2',
			'tusua_ente' => 'Tusua Ente',
			'tusua_cargo' => 'Tusua Cargo',
			'tusua_passw' => 'Tusua Passw',
			'tusua_verif' => 'Tusua Verif',
			'tusua_obser' => 'Tusua Obser',
			'tusua_telef' => 'Tusua Telef',
			'tusua_fcrea' => 'Tusua Fcrea',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('tusua_codig',$this->tusua_codig);
		$criteria->compare('tusua_corre',$this->tusua_corre,true);
		$criteria->compare('tgene_permi',$this->tgene_permi);
		$criteria->compare('tpers_nacio',$this->tpers_nacio,true);
		$criteria->compare('tpers_cedul',$this->tpers_cedul);
		$criteria->compare('tusua_entec',$this->tusua_entec);
		$criteria->compare('tusua_statu',$this->tusua_statu);
		$criteria->compare('tusua_corr2',$this->tusua_corr2,true);
		$criteria->compare('tusua_ente',$this->tusua_ente,true);
		$criteria->compare('tusua_cargo',$this->tusua_cargo,true);
		$criteria->compare('tusua_passw',$this->tusua_passw,true);
		$criteria->compare('tusua_verif',$this->tusua_verif,true);
		$criteria->compare('tusua_obser',$this->tusua_obser,true);
		$criteria->compare('tusua_telef',$this->tusua_telef,true);
		$criteria->compare('tusua_fcrea',$this->tusua_fcrea,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TusuarioTmp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
