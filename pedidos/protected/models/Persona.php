<?php

/**
 * This is the model class for table "p_persona".
 *
 * The followings are the available columns in table 'p_persona':
 * @property integer $perso_codig
 * @property string $nacio_value
 * @property integer $perso_cedul
 * @property string $perso_pnomb
 * @property string $perso_snomb
 * @property string $perso_papel
 * @property string $perso_sapel
 * @property string $perso_fnaci
 * @property string $gener_value
 *
 * The followings are the available model relations:
 * @property Constancia[] $constancias
 * @property Genero $generValue
 * @property Nacionalidad $nacioValue
 * @property Voceros[] $voceroses
 */
class Persona extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'p_persona';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nacio_value, perso_cedul, perso_pnomb, perso_papel, perso_fnaci, gener_value', 'required'),
			array('perso_cedul', 'numerical', 'integerOnly'=>true),
			array('nacio_value, gener_value', 'length', 'max'=>1),
			array('perso_pnomb, perso_snomb, perso_papel, perso_sapel', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('perso_codig, nacio_value, perso_cedul, perso_pnomb, perso_snomb, perso_papel, perso_sapel, perso_fnaci, gener_value', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'constancias' => array(self::HAS_MANY, 'Constancia', 'perso_codig'),
			'generValue' => array(self::BELONGS_TO, 'Genero', 'gener_value'),
			'nacioValue' => array(self::BELONGS_TO, 'Nacionalidad', 'nacio_value'),
			'voceroses' => array(self::HAS_MANY, 'Voceros', 'perso_codig'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'perso_codig' => 'Perso Codig',
			'nacio_value' => 'Nacio Value',
			'perso_cedul' => 'Perso Cedul',
			'perso_pnomb' => 'Perso Pnomb',
			'perso_snomb' => 'Perso Snomb',
			'perso_papel' => 'Perso Papel',
			'perso_sapel' => 'Perso Sapel',
			'perso_fnaci' => 'Perso Fnaci',
			'gener_value' => 'Gener Value',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('perso_codig',$this->perso_codig);
		$criteria->compare('nacio_value',$this->nacio_value,true);
		$criteria->compare('perso_cedul',$this->perso_cedul);
		$criteria->compare('perso_pnomb',$this->perso_pnomb,true);
		$criteria->compare('perso_snomb',$this->perso_snomb,true);
		$criteria->compare('perso_papel',$this->perso_papel,true);
		$criteria->compare('perso_sapel',$this->perso_sapel,true);
		$criteria->compare('perso_fnaci',$this->perso_fnaci,true);
		$criteria->compare('gener_value',$this->gener_value,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Persona the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
