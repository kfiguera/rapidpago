<?php

/**
 * This is the model class for table "admin_tgenerica_dependiente".
 *
 * The followings are the available columns in table 'admin_tgenerica_dependiente':
 * @property integer $tdepe_codig
 * @property string $tdepe_descr
 * @property integer $tgene_depen
 * @property string $tdepe_tipod
 */
class TgenericaDependiente extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'admin_tgenerica_dependiente';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tdepe_descr, tgene_depen, tdepe_tipod', 'required'),
			array('tgene_depen', 'numerical', 'integerOnly'=>true),
			array('tdepe_descr', 'length', 'max'=>150),
			array('tdepe_tipod', 'length', 'max'=>3),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('tdepe_codig, tdepe_descr, tgene_depen, tdepe_tipod', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tdepe_codig' => 'Tdepe Codig',
			'tdepe_descr' => 'Tdepe Descr',
			'tgene_depen' => 'Tgene Depen',
			'tdepe_tipod' => 'Tdepe Tipod',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('tdepe_codig',$this->tdepe_codig);
		$criteria->compare('tdepe_descr',$this->tdepe_descr,true);
		$criteria->compare('tgene_depen',$this->tgene_depen);
		$criteria->compare('tdepe_tipod',$this->tdepe_tipod,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TgenericaDependiente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
