<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Despacho</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Despacho</a></li>
            <li><a>Preparar Entrega</a></li>
            <li class="active">Listado</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

<?php
//Mostrar mensajes del controlador
        foreach (Yii::app()->user->getFlashes() as $key => $message) {
          echo '<div class="alert alert-' . $key . ' alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              ' . $message  .' </div>';          
        }
$connection = Yii::app()->db;
if(Yii::app()->user->id['usuario']['permiso']==1){
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Criterio de Busqueda</h3>
    </div>
    <div class="panel-body" >
        <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'pedidos', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            

        ?>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Número de Solicitud</label>
                    <?php echo CHtml::textField('numero', '', array('class' => 'form-control', 'placeholder' => "Nro del Solicitud")); ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Estatus</label>
                    <?php 
                        
                        $sql="SELECT * FROM rd_preregistro_estatus";
                        $result=$connection->createCommand($sql)->queryAll();
                        $data=CHtml::listData($result,'epedi_codig','epedi_descr');
                        echo CHtml::dropDownList('epedi', '', $data,array('class' => 'form-control', 'placeholder' => "Descripción", 'prompt'=>'Seleccione...')); ?>
                </div>
            </div>
           
        </div>
        
        <div class="row">
            <div class="col-sm-6">
                <button type="button" id="buscar" class="btn btn-block btn-info">Buscar</button>
            </div>
            <div class="col-sm-6">
                <button type="reset" class="btn btn-block btn-default">Limpiar</button>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div> 
<?php
}
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-sm-8">
                <h3 class="panel-title">Listado</h3>
            </div>
        </div>
        
    </div>
    <div class="panel-body" >
        
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-pedido'>
                <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                #
                            </th>
                            <th>
                                Nro Solicitud
                            </th>
                            <th>
                                RIF del Comercio
                            </th>
                            <th>
                                Código de Afiliado
                            </th>
                            <th>
                                Banco
                            </th>
                            <th>
                                Cliente
                            </th>
                            <th>
                                Contacto
                            </th>
                            <th>
                                Total POS a Solicitar
                            </th>
                            <th>
                                Usuario de Registro
                            </th>
                            <th>
                                Fecha de Registro
                            </th>
                            <th width="5px">
                                Estatus
                            </th>
                            <th width="5%">
                                &nbsp;
                            </th>
                            <th width="5%">
                                &nbsp;
                            </th>
                            
                            <th width="5%">
                                &nbsp;
                            </th>        
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        /*$sql = "SELECT *
                                FROM solicitud a 
                                JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                                JOIN cliente c ON (a.clien_codig = c.clien_codig)
                                JOIN seguridad_usuarios d ON (a.usuar_codig = d.usuar_codig)
                                JOIN p_persona e ON (d.perso_codig = e.perso_codig)
                                WHERE a.estat_codig in (26)
                                AND solic_clvip<>'1'
                                ";*/
                        $sql = $this->funciones->SqlListadoSolicitudes('26');
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        $j=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                            $j++;
                            if($row['solic_clvip']<>'1'){
                                $sql="SELECT * 
                                      FROM solicitud_trayectoria 
                                      WHERE solic_codig = '".$row['solic_codig']."'
                                        AND estat_codig = '".$row['estat_codig']."'";

                                $trayectoria = $connection->createCommand($sql)->queryRow();

                                $inicio=$trayectoria['traye_finic'].' '.$trayectoria['traye_hinic'];
                                $fin=date('Y-m-d H:i:s');
                                $diff=$this->funciones->diferenciaHoras($inicio,$fin);
                                $horas=($diff->days * 24 )  + ( $diff->h );
                                $semaforo=$this->funciones->semaforo($connection,$row['estat_codig'],$horas);
                                
                                $sql = "SELECT sum(cequi_canti) cantidad
                                    FROM solicitud_equipos a 
                                    WHERE solic_codig='".$row['solic_codig'] ."'";
                                $cequipo = $connection->createCommand($sql)->queryRow();
                                
                                $sql = "SELECT *
                                    FROM p_banco 
                                    WHERE banco_codig='".$row['banco_codig'] ."'";
                                $banco = $connection->createCommand($sql)->queryRow();

                                $sql="SELECT * FROM solicitud_traslado a
                                      JOIN solicitud_movimiento_traslado b ON (a.trasl_codig = b.trasl_codig)
                                      WHERE b.solic_codig = '".$row['solic_codig']."'
                                        AND a.pesta_codig='1'";
                                
                                $traslado = $connection->createCommand($sql)->queryRow();
                            ?>
                            <tr>

                                <td><?php echo $i ?></td>
                                <td><?php echo $row['solic_numer'] ?></td>
                                <td><?php echo $row['clien_rifco'] ?></td>
                                <td><?php echo $row['solic_cafil'] ?></td>
                                <td><?php echo $banco['banco_descr'] ?></td>
                                <td><?php echo $row['clien_rsoci'] ?></td>
                                <td><?php echo $row['solic_celec'] ?></td>
                                <td><?php echo $cequipo['cantidad'] ?></td>
                                <td class="tabla"><?php echo $row['perso_pnomb'].' '.$row['perso_papel'] ?></td>
                                <td><?php echo $this->funciones->transformarFecha_v($row['solic_fcrea']).' '.$row['solic_hcrea'] ?></td>
                                <td><?php echo $semaforo.' '.$row['estat_descr'] ?></td>


                                <td>
                                    <a href="consultar?c=<?php echo $row['solic_codig'] ?>" class="btn btn-block btn-info"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Consultar">
                                        <i class="fa fa-search"></i>
                                    </a>
                                </td>
                                <td>
                                    <a href="entregar?c=<?php echo $row['solic_codig'] ?>" class="btn btn-block btn-warning"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Entregar">
                                        <i class="fa fa-check"></i>
                                    </a>
                                </td>
                                <td>
                                    <a href="rechazar?c=<?php echo $row['solic_codig'] ?>" class="btn btn-block btn-danger"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Rechazar">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php
                                }
                            }   
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
        </div>
    </div>
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#buscar').click(function () {
        var formData = new FormData($("#pedidos")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-pedido').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>
