<?php $conexion = Yii::app()->db; ?>
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Recepción de Dispositivos</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Despacho</a></li>
            <li><a href="#">Recepción de Dispositivos</a></li>
            <li class="active">Registrar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <div class="row line-steps">
                  <div class="col-md-6 column-step <?php echo $ubicacion[0]; ?>">
                    <a href="modificar?c=<?php echo $solicitud['solic_codig']?>&s=1">
                        <div class="step-number">1 </div>
                    <div class="step-title">Pre-Registro</div>
                    <div class="step-info">Detalles de la Solicitud</div>
                 </div>

                 <div class="col-md-6 column-step <?php echo $ubicacion[1]; ?> ">
                    <div class="step-number">2</div>
                    <div class="step-title">Documentos</div>
                    <div class="step-info">Carga de Documentos Digitales</div>
                 </div>
              </div>
        </div>
    </div>
</div>
<?php
    $sql="SELECT * FROM pedido_pedidos_detalle WHERE pedid_codig = '". $c."'"; 
    $materiales=$conexion->createCommand($sql)->queryAll();
    $material='';
    $a=0;
    foreach ($materiales as $key => $value) {
        if($a > 0){
            $material.=",";
        }
        $material.="'".$value['tmode_codig']."'";
    }
?>
<form id='login-form' name='login-form' method="post">

<div class="row">   
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Documentos Digitales</h3>
        </div>
        <div class="panel-body" >
            <input class="form-control" placeholder="Porcentaje" readonly="readonly" type="hidden" value="" name="bookindex" id="bookindex" /> 
                <!-- Plantilla -->
                <div class="form-group">
                    <div class="row">
                       <div class="col-xs-4">
                                <label>Tipo de Documento *</label>
                                <?php 
                                    echo CHtml::dropDownList('texto['.$a.']', $row['pimag_tbord'],'', array('class' => 'form-control', 'placeholder' => "Texto a Bordar",'prompt'=>'Seleccione...','id'=>'texto_'.$a.'')); ?>
                       </div> 
                       <div class="col-xs-7"> 
                            <div class="numero1_">
                                <label>Documento *</label>
                                <div  class="input-group image-preview">  
                                    <img id="dynamic">
                                    <!-- image-preview-filename input [CUT FROM HERE]-->
                                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-btn">
                                        <!-- image-preview-clear button -->
                                        <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                            <span class="fa fa-times"></span> Limpiar
                                        </button>
                                        <!-- image-preview-input -->
                                        <div class="btn btn-default image-preview-input">
                                            <span class="fa fa-folder-open"></span>
                                            <span class="numero1_ image-preview-input-title">Buscar</span>
                                            <input type="file" id="image_" name="image_"/> <!-- rename it -->
                                        </div>
                                    </span>
                               
                                </div> 
                            </div> 
                       </div> 
                       <div class="col-xs-1"> 
                            <label>&nbsp;</label>
                            <br>
                            <button type="button" class="btn btn-success btn-block addButton"><i class="fa fa-plus"></i></button>
                       </div> 
                    </div>
                    
                    <div class="row hide">


                        <div class="col-xs-4">        
                            <div class="numero1_">
                                <label>Planilla de Solicitud *</label>
                                <div  class="input-group image-preview">  
                                    <img id="dynamic">
                                    <!-- image-preview-filename input [CUT FROM HERE]-->
                                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-btn">
                                        <!-- image-preview-clear button -->
                                        <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                            <span class="fa fa-times"></span> Limpiar
                                        </button>
                                        <!-- image-preview-input -->
                                        <div class="btn btn-default image-preview-input">
                                            <span class="fa fa-folder-open"></span>
                                            <span class="numero1_ image-preview-input-title">Buscar</span>
                                            <input type="file" id="image_" name="image_"/> <!-- rename it -->
                                        </div>
                                    </span>
                               
                                </div> 
                            </div> 

                        </div>
                       
                        <div class="col-xs-4">        
                            <div class="numero1_">
                                <label>Documento Constitutivo</label>
                                <div  class="input-group image-preview">  
                                    <img id="dynamic">
                                    <!-- image-preview-filename input [CUT FROM HERE]-->
                                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-btn">
                                        <!-- image-preview-clear button -->
                                        <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                            <span class="fa fa-times"></span> Limpiar
                                        </button>
                                        <!-- image-preview-input -->
                                        <div class="btn btn-default image-preview-input">
                                            <span class="fa fa-folder-open"></span>
                                            <span class="numero1_ image-preview-input-title">Buscar</span>
                                            <input type="file" id="image_" name="image_"/> <!-- rename it -->
                                        </div>
                                    </span>
                               
                                </div> 
                            </div> 

                        </div>
                        <div class="col-xs-4">        
                            <div class="numero1_">
                                <label>Estado de Cuenta *</label>
                                <div  class="input-group image-preview">  
                                    <img id="dynamic">
                                    <!-- image-preview-filename input [CUT FROM HERE]-->
                                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-btn">
                                        <!-- image-preview-clear button -->
                                        <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                            <span class="fa fa-times"></span> Limpiar
                                        </button>
                                        <!-- image-preview-input -->
                                        <div class="btn btn-default image-preview-input">
                                            <span class="fa fa-folder-open"></span>
                                            <span class="numero1_ image-preview-input-title">Buscar</span>
                                            <input type="file" id="image_" name="image_"/> <!-- rename it -->
                                        </div>
                                    </span>
                               
                                </div> 
                            </div> 
                            <span class="help-block">
                                <small>
                                    Donde se evidencia los 20 digitos del número de cuenta asociado al codigo de afiliación.
                                </small>
                            </span>
                        </div>
                    </div>  
                </div>  
                <div class="form-group hide">

                    <div class="row">  
                        <div class="col-xs-4">        
                            <div class="numero1_">
                                <label>RIF del Comercio *</label>
                                <div  class="input-group image-preview">  
                                    <img id="dynamic">
                                    <!-- image-preview-filename input [CUT FROM HERE]-->
                                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-btn">
                                        <!-- image-preview-clear button -->
                                        <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                            <span class="fa fa-times"></span> Limpiar
                                        </button>
                                        <!-- image-preview-input -->
                                        <div class="btn btn-default image-preview-input">
                                            <span class="fa fa-folder-open"></span>
                                            <span class="numero1_ image-preview-input-title">Buscar</span>
                                            <input type="file" id="image_" name="image_"/> <!-- rename it -->
                                        </div>
                                    </span>
                               
                                </div> 
                            </div> 
                            <span class="help-block">
                                <small>
                                    Debe de estar vigente
                                </small>
                            </span>
                        </div>
                        
                        <div class="col-xs-4">        
                            <div class="numero1_">
                                <label>Cédula/Pasaporte </label>
                                <div  class="input-group image-preview">  
                                    <img id="dynamic">
                                    <!-- image-preview-filename input [CUT FROM HERE]-->
                                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-btn">
                                        <!-- image-preview-clear button -->
                                        <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                            <span class="fa fa-times"></span> Limpiar
                                        </button>
                                        <!-- image-preview-input -->
                                        <div class="btn btn-default image-preview-input">
                                            <span class="fa fa-folder-open"></span>
                                            <span class="numero1_ image-preview-input-title">Buscar</span>
                                            <input type="file" id="image_" name="image_"/> <!-- rename it -->
                                        </div>
                                    </span>
                               
                                </div> 
                            </div> 
                            <span class="help-block">
                                <small>
                                    De los representantes legales
                                </small>
                            </span>
                        </div>
                        <div class="col-xs-4">        
                            <div class="numero1_">
                                <label>RIF de los Representantes Legales*</label>
                                <div  class="input-group image-preview">  
                                    <img id="dynamic">
                                    <!-- image-preview-filename input [CUT FROM HERE]-->
                                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-btn">
                                        <!-- image-preview-clear button -->
                                        <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                            <span class="fa fa-times"></span> Limpiar
                                        </button>
                                        <!-- image-preview-input -->
                                        <div class="btn btn-default image-preview-input">
                                            <span class="fa fa-folder-open"></span>
                                            <span class="numero1_ image-preview-input-title">Buscar</span>
                                            <input type="file" id="image_" name="image_"/> <!-- rename it -->
                                        </div>
                                    </span>
                               
                                </div> 
                            </div> 
                            <span class="help-block">
                                <small>
                                    Debe de Estar Vigente
                                </small>
                            </span>
                        </div>
                    </div>  
                </div>
                <div class="form-group hide ">

                    <div class="row"> 
                        <div class="col-xs-4">        
                            <div class="numero1_">
                                <label>Contrato *</label>
                                <div  class="input-group image-preview">  
                                    <img id="dynamic">
                                    <!-- image-preview-filename input [CUT FROM HERE]-->
                                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-btn">
                                        <!-- image-preview-clear button -->
                                        <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                            <span class="fa fa-times"></span> Limpiar
                                        </button>
                                        <!-- image-preview-input -->
                                        <div class="btn btn-default image-preview-input">
                                            <span class="fa fa-folder-open"></span>
                                            <span class="numero1_ image-preview-input-title">Buscar</span>
                                            <input type="file" id="image_" name="image_"/> <!-- rename it -->
                                        </div>
                                    </span>
                               
                                </div> 
                            </div> 
                            <span class="help-block">
                                <small>
                                    Debe de estar firmado y sellado por los representantes legales.
                                </small>
                            </span>
                        </div>
                    </div>  
                </div>


                        
        </div>
    </div>
</div>

<!--div class="row hide">   
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Color Letras</h3>
        </div>
        <div class="panel-body" >
           
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color Curso *</label>
                            <div class="input-group">

                            <?php
                                $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                  FROM pedido_modelo_color a
                                  GROUP BY 1,2
                                  ORDER BY 1";

                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');

                                echo CHtml::dropDownList('ccurs', $pedidos['pedid_ccurs'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['1']['disabled'], 'disabled'=>'disabled')); ?>
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-8" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span> 
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color Nombre Personalizado *</label>
                            <div class="input-group">

                            <?php
                                $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                  FROM pedido_modelo_color a
                                  GROUP BY 1,2
                                  ORDER BY 1";

                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');

                                echo CHtml::dropDownList('cnper', $pedidos['pedid_cnper'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['1']['disabled'], 'disabled'=>'disabled')); ?>
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-9" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span> 
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color Apodo Espalda *</label>
                            <div class="input-group">

                            <?php
                                $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                  FROM pedido_modelo_color a
                                  GROUP BY 1,2
                                  ORDER BY 1";

                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');

                                echo CHtml::dropDownList('caesp', $pedidos['pedid_caesp'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['1']['disabled'], 'disabled'=>'disabled')); ?>
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-10" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Color Electivos *</label>
                            <div class="input-group">

                            <?php
                                $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                  FROM pedido_modelo_color a
                                  GROUP BY 1,2
                                  ORDER BY 1";

                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');

                                echo CHtml::dropDownList('celec', $pedidos['pedid_celec'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['1']['disabled'], 'disabled'=>'disabled')); ?>
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-11" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span> 
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Color Frase *</label>
                            <div class="input-group">

                            <?php
                                $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                  FROM pedido_modelo_color a
                                  GROUP BY 1,2
                                  ORDER BY 1";

                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');

                                echo CHtml::dropDownList('cfras', $pedidos['pedid_cfras'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['1']['disabled'], 'disabled'=>'disabled')); ?>
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-12" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span> 
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Otros</label>
                            <?php echo CHtml::textArea('otros', $pedidos['pedid_mensa'], array('class' => 'form-control', 'placeholder' => "Mensaje", 'disabled'=>'disabled')); ?>
                        </div>
                    </div>
                </div>
            </div><!-- form ->
    </div>  
</div-->
<div class="row">   
    <div class="panel panel-default">
        <div class="panel-body" >
                <!-- Button -->
                <div class="row controls">
                    <div class="col-sm-4 ">
                        <a href="../detalle/listado?p=<?php echo $c?>" type="reset" class="btn-block btn btn-default">Volver </a>
                    </div>
                    <div class="col-sm-4 ">
                        <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                    </div>
                    <div class="col-sm-4 ">
                        <button id="guardar" type="button" class="btn-block btn btn-info">Siguiente  </button>
                    </div>
                </div>


        </div><!-- form -->
    </div>  
</div>
</form>
<div class="hide" id="popover-img">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_ccuer']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive"></div>
    <div class="hide" id="popover-img-2">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_cbols']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive"></div>
    <div class="hide" id="popover-img-3">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_cmang']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive"></div>
    <div class="hide" id="popover-img-4">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_cegor']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive"></div>
    <div class="hide" id="popover-img-5">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_cigor']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive"></div>
    <div class="hide" id="popover-img-6">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_cppre']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive"></div>
    <div class="hide" id="popover-img-13">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_cpprl']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive"></div>
    <div class="hide" id="popover-img-14">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_cvivo']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive"></div>
    <div class="hide" id="popover-img-7">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_ccier']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive"></div>
    <div class="hide" id="popover-img-8">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_ccurs']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive"></div>
    <div class="hide" id="popover-img-9">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_cnper']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive"></div>
    <div class="hide" id="popover-img-10">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_caesp']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive"></div>
    <div class="hide" id="popover-img-11">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_celec']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive"></div>
    <div class="hide" id="popover-img-12">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_cfras']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive"></div>
<script type="text/javascript">
    var descr = {
            validators: {
                notEmpty: {
                    message: 'Estimado(a) Usuario(a) el campo "Nombre" es obligatorio',
                }
            }
        },
        image = {
            validators: {
                notEmpty: {
                    message: 'Estimado(a) Usuario(a) el campo "Imagen a Bordar" es obligatorio',
                }
            }
        },
        texto = {
            validators: {
                notEmpty: {
                    message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar" es obligatorio',
                }
            }
        },
        bookIndex = <?php echo $a; ?>,
        contador = 0;
        
</script>
<script>
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                ccuer: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color del Cuerpo" es obligatorio',
                        }
                    }
                },
                cbols: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color de los Bolsillos" es obligatorio',
                        }
                    }
                },
                cmang: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color de las Mangas" es obligatorio',
                        }
                    }
                },
                cegor: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color Exterior del Gorro" es obligatorio',
                        }
                    }
                },
                cigor: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color Interior del Gorro" es obligatorio',
                        }
                    }
                },
                cppre: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color Puños y Pretina Fondo" es obligatorio',
                        }
                    }
                },
                cpprl: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color Puños y Pretina Linea" es obligatorio',
                        }
                    }
                },
                cierr: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Cierre" es obligatorio',
                        }
                    }
                },
                ccier: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color Cierre" es obligatorio',
                        }
                    }
                },
                broch: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Broches" es obligatorio',
                        }
                    }
                },
                ccurs: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color Curso" es obligatorio',
                        }
                    }
                },
                cnper: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color Nombre Personalizado" es obligatorio',
                        }
                    }
                },
                caesp: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color Apodo Espalda" es obligatorio',
                        }
                    }
                },
                celec: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color Electivos" es obligatorio',
                        }
                    }
                },
                cfras: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color Frase" es obligatorio',
                        }
                    }
                },
                //'descr[0]': descr,
                //'image[0]': image,
                //'texto[0]': texto,
                
            }
        });
    }).on('click', '.addButton', function() {
        //if(contador<4){ 
            contador++;
            //alert(contador);
            bookIndex++;
         
            document.getElementById('bookindex').value = bookIndex;
            var $template = $('#bookTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .attr('data-book-index', bookIndex)
                                .insertBefore($template);

            // Update the name attributes
            $clone
                .find('[name="descr_"]').attr('name', 'descr[' + bookIndex + ']').end()
                .find('[name="image_"]').attr('name', 'image[' + bookIndex + ']').end()
                .find('[name="texto_"]').attr('name', 'texto[' + bookIndex + ']').end()
                
                .find('[id="descr_"]').attr('id', 'descr_' + bookIndex ).end()
                .find('[id="image_"]').attr('id', 'image_' + bookIndex ).end()
                .find('[id="texto_"]').attr('id', 'texto_' + bookIndex ).end()
                .find('[class="numero1_"]').attr('class', 'numero1_' + bookIndex ).end();

            // Add new fields
            // Note that we also pass the validator rules for new field as the third parameter
            //$('#login-form')
                //.formValidation('addField', 'descr[' + bookIndex + ']', descr);
                //.formValidation('addField', 'image[' + bookIndex + ']', image);
            

            //agregar ajax
            

            $('#login-form').append(
                '<script type="text/javascript">\n'+
                    '$(document).on("click", "#close-preview", function () {\n'+
                        '$(".numero1_'+bookIndex+' .image-preview").popover("hide");\n'+
                     
                        '$(".numero1_'+bookIndex+' .image-preview").hover(\n'+
                            'function () {\n'+
                                '$(".image-preview").popover("hide");\n'+
                            '},\n'+
                            'function () {\n'+
                                '$(".image-preview").popover("hide");\n'+
                            '}\n'+
                        ');\n'+
                    '});\n'+
                    '$(function () {\n'+
                       
                        'var closebtn = $("<button/>", {\n'+
                            'type: "button",\n'+
                            'text: "x",\n'+
                            'id: "close-preview",\n'+
                            'style: "font-size: initial;",\n'+
                        '});\n'+
                        'closebtn.attr("class","close pull-right");\n'+
                        '$(".numero1_'+bookIndex+' .image-preview").popover({\n'+
                            'trigger:"manual",\n'+
                            'html:true,\n'+
                            'title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,\n'+
                            'content: "No hay imagen",\n'+
                            'placement:"top"\n'+
                        '});\n'+
                       
                        '$(".numero1_'+bookIndex+' .image-preview-clear").click(function () {\n'+
                            '$(".numero1_'+bookIndex+' .image-preview").attr("data-content", "").popover("hide");\n'+
                            '$(".numero1_'+bookIndex+' .image-preview-filename").val("");\n'+
                            '$(".numero1_'+bookIndex+' .image-preview-clear").hide();\n'+
                            '$(".numero1_'+bookIndex+' .image-preview-input input:file").val("");\n'+
                            '$(".numero1_'+bookIndex+' .image-preview-input-title").text("Buscar");\n'+
                        '});\n'+
                       
                        '$(".numero1_'+bookIndex+' .image-preview-input input:file").change(function () {\n'+
                            'var img = $("<img/>", {\n'+
                                'id: "dynamic",\n'+
                                'width: 250,\n'+
                                'height: 200\n'+
                            '});\n'+
                            'var file = this.files[0];\n'+
                            'var reader = new FileReader();\n'+
                          
                            'reader.onload = function (e) {\n'+
                                '$(".numero1_'+bookIndex+' .image-preview-input-title").text("Cambiar");\n'+
                                '$(".numero1_'+bookIndex+' .image-preview-clear").show();\n'+
                                '$(".numero1_'+bookIndex+' .image-preview-filename").val(file.name);\n'+
                                'img.attr("src", e.target.result);\n'+
                                '$(".numero1_'+bookIndex+' .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");\n'+
                                '}\n'+
                            'reader.readAsDataURL(file);\n'+
                        '});\n'+
                    '});\n'+
                '<\/script>');
            
           

       
    }).on('click', '.removeButton', function() {// Remove button click handler
        //alert(contador);
        contador=contador-1;
        var $row  = $(this).parents('.form-group'),
            index = $row.attr('data-book-index');
        // Remove fields
        //$('#login-form')
            //.formValidation('removeField', $row.find('[name="descr[' + index + ']"]'));
            //.formValidation('removeField', $row.find('[name="image[' + index + ']"]'));
            //.formValidation('removeField', $row.find('[name="texto[' + index + ']"]'));
        // Remove element containing the fields
        $row.remove();
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        var data = new FormData(jQuery('form')[0]);
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                url: 'paso1',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        /*swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){*/
                            window.open('modificar?c=<?php echo $c; ?>&s=2', '_parent');
                        /*});*/
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>
<script type="text/javascript">
    $('#cierr').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '1':
                jQuery("#ccier").removeAttr('disabled');
                break;
            default:
                jQuery("#ccier").attr('disabled','true');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#iopci').change(function () {
        var iopci = $(this).val();
        $.ajax({
            'type':'POST',
            'data':{'tmode':tmode.value},
            'url':'<?php echo CController::createUrl('funciones/Recepción de DispositivosColorOpcional'); ?>',
            'cache':false,
            'success':function(html){
                switch(iopci) {
                    case '1':
                        
                        jQuery("#opcional-1").removeClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#iclin").html(html);
                        jQuery("#iccie").html('');
                        jQuery("#icviv").html('');
                        jQuery("#ideta").removeAttr('disabled');
                        jQuery("#iclin").removeAttr('disabled');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").attr('disabled','true');

                        break;
                    case '2':
                        jQuery("#opcional-2").removeClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#iclin").html('');
                        jQuery("#iccie").html(html);
                        jQuery("#icviv").html('');
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").removeAttr('disabled');
                        jQuery("#icviv").attr('disabled','true');
                        break;
                    case '3':
                        jQuery("#opcional-3").removeClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#iclin").html('');
                        jQuery("#iccie").html('');
                        jQuery("#icviv").html(html);
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").removeAttr('disabled');
                        break;
                    default:
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#iclin").html(html);
                        jQuery("#iccie").html(html);
                        jQuery("#icviv").html(html);
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").attr('disabled','true');
                        break;
                }
                
            }
        });
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    var id = ['ccuer', 'cbols', 'cmang', 'cegor', 'cigor', 'cppre', 'ccier', 'ccurs', 'cnper', 'caesp', 'celec', 'cfras', 'cpprl', 'cvivo' ];
    var con = 1;
    id.forEach( function(valor, indice, array) {
        var numero = indice+1;
        $('#img-'+numero).popover({
          html: true,
          content: function() {
            if(numero==1){
                return $('#popover-img').html();
            }else{
                return $('#popover-img-'+numero).html();    
            }
            
          },
          trigger: 'hover'
        });
        $('#'+valor).change(function () {
            var emoji = $(this).val();
            $.ajax({  
                url:"<?php echo CController::createUrl('funciones/Recepción de DispositivosVerColor'); ?>",  
                method:"POST",  
                async:false,  
                data:{id:emoji},  
                success:function(data){  
                    if(numero=='1'){
                        $('#popover-img').html(data);
                    }else{
                        $('#popover-img-'+numero).html(data);    
                    } 
                }  
            });
            $('[data-toggle=popover]').popover('hide');
        });
    });
});
</script>
<script type="text/javascript">
    $('#ideta').change(function () {
        var ideta = $(this).val();
        switch(ideta) {
            case '1':
                jQuery("#iclin").removeAttr('disabled');
                break;
            default:
                jQuery("#iclin").attr('disabled','true');
                break;
        }
                
            
    });
</script>

<!--script type="text/javascript">
$(document).ready(function(){
    $('#img-1').popover({
          html: true,
          content: function() {
            return $('#popover-img').html();
          },
          trigger: 'hover'
        });
});
$('#color').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/Recepción de DispositivosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-2').popover({
          html: true,
          content: function() {
            return $('#popover-img-2').html();
          },
          trigger: 'hover'
        });
});
$('#iclin').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/Recepción de DispositivosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-2").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-3').popover({
          html: true,
          content: function() {
            return $('#popover-img-3').html();
          },
          trigger: 'hover'
        });
});
$('#iccie').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/Recepción de DispositivosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-3").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-4').popover({
          html: true,
          content: function() {
            return $('#popover-img-4').html();
          },
          trigger: 'hover'
        });
});
$('#icviv').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/Recepción de DispositivosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-4").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script-->