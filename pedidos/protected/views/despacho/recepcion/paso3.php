<style>
    .image-preview-input {
        position: relative;
        overflow: hidden;
        margin: 0px;    
        color: #333;
        background-color: #fff;
        border-color: #ccc;    
    }
    .image-preview-input input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }
    .image-preview-input-title {
        margin-left:2px;
    }
</style> 
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Detalle del Pedido</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Recepción de Dispositivos</a></li>
            <li><a href="#">Detalle</a></li>
            <li class="active">Registrar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <div class="row line-steps">
                  <div class="col-md-4 column-step <?php echo $ubicacion[0]; ?>">
                    <a href="modificar?c=<?php echo $solicitud['solic_codig']?>&s=1">
                        <div class="step-number">1 </div>
                    <div class="step-title">Pedido</div>
                    <div class="step-info">Detalles del Pedido</div>
                 </div>

                 <div class="col-md-4 column-step <?php echo $ubicacion[1]; ?> ">
                    <div class="step-number">2</div>
                    <div class="step-title">Adicional</div>
                    <div class="step-info">Información Adicional a Bordar Parte 1</div>
                 </div>
                 <div class="col-md-4 column-step <?php echo $ubicacion[2]; ?> ">
                    <div class="step-number">3</div>
                    <div class="step-title">Adicional</div>
                    <div class="step-info">Información Adicional a Bordar Parte 2</div>
                 </div>
              </div>
        </div>
    </div>
</div>
<form id='login-form' name='login-form' method="post">

<div class="row">   
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Espalda</h3>
        </div>
        <div class="panel-body" >
           
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <div class="row hide">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Pedido *</label>
                            <?php 
                                echo CHtml::hiddenField('pedid', $p, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Código *</label>
                            <?php 
                                echo CHtml::hiddenField('codig', $c, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Paso *</label>
                            <?php 
                                echo CHtml::hiddenField('pasos', $pedidos['pedid_pasos'], array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color del Cuerpo *</label>
                            <?php 
                                echo CHtml::hiddenField('ccuer', $pedidos['pedid_ccuer'], array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color de las Mangas *</label>
                            <?php 
                                echo CHtml::hiddenField('cmang', $pedidos['pedid_cmang'], array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"> 
                        <label>Frase espalda alta *</label>
                    </div>
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Texto a Bordar *</label>
                            <?php 
                                
                                echo CHtml::textarea('eatbo', $pedidos['pedid_eatbo'], array('class' => 'form-control', 'placeholder' => "Texto a Bordar",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Fuente *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.fuent_codig, fuent_descr
                                      FROM pedido_fuente a

                                      GROUP BY 1,2
                                      ORDER BY 2";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                echo CHtml::dropDownList('eafue', $pedidos['pedid_eafue'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-3" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Color *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                      FROM pedido_modelo_color a
                                      WHERE tmode_codig='1'
                                        AND mcolo_numer<>'0'
                                      GROUP BY 1,2
                                      ORDER BY 2";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                                echo CHtml::dropDownList('eacol', $pedidos['pedid_eacol'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-4" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-sm-12"> 
                        <label>Frase espalda baja *</label>
                    </div>
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Texto a Bordar *</label>
                            <?php 
                                
                                echo CHtml::textArea('ebtbo', $pedidos['pedid_ebtbo'], array('class' => 'form-control', 'placeholder' => "Texto a Bordar",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Fuente *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.fuent_codig, fuent_descr
                                      FROM pedido_fuente a
                                      GROUP BY 1,2
                                      ORDER BY 2";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                echo CHtml::dropDownList('ebfue', $pedidos['pedid_ebfue'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-5" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Color *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                      FROM pedido_modelo_color a
                                      WHERE tmode_codig='1'
                                        AND mcolo_numer<>'0'

                                      GROUP BY 1,2
                                      ORDER BY 2";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                                echo CHtml::dropDownList('ebcol', $pedidos['pedid_ebcol'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-6" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-sm-12"> 
                        <label>Nombre o Apodo Espalda *</label>
                    </div>

                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Fuente *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.fuent_codig, fuent_descr
                                      FROM pedido_fuente a
                                      GROUP BY 1,2
                                      ORDER BY 2";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                echo CHtml::dropDownList('aefue', $pedidos['pedid_aefue'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-7" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Color *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                      FROM pedido_modelo_color a
                                      WHERE tmode_codig='1'
                                        AND mcolo_numer<>'0'
                                      GROUP BY 1,2
                                      ORDER BY 2";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                                echo CHtml::dropDownList('aecol', $pedidos['pedid_aecol'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-8" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                            <div class="numero4 form-group">
                                <label>Imagen Principal</label>
                                <div  class="input-group image-preview" data-placement="top" >  

                                    <img id="dynamic">
                                    <!-- image-preview-filename input [CUT FROM HERE]-->
                                    <?php 
                                        $sql="SELECT * FROM pedido_pedidos_imagen WHERE pedid_codig ='".$c."' and pimag_orden=7";
                                        $result=$conexion->createCommand($sql)->queryRow();
                                        $ruta=$result['pimag_ruta'];

                                        $nombre=explode('/', $ruta);
                                        $nombre=end($nombre);
                                        if($nombre){
                                            $opcional['4']['hide']='';
                                            $opcional['4']['show']='true';
                                        }else{
                                            $opcional['4']['hide']='hide';
                                            $opcional['4']['show']='false';
                                        }
                                    ?>
                                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="true" value="<?php echo $nombre ?>" > <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-btn">
                                        <!-- image-preview-clear button -->
                                        <button type="button" class="btn btn-default image-preview-clear <?php echo $opcional['4']['hide']; ?>" >
                                            <span class="fa fa-times"></span> Limpiar
                                        </button>
                                        <!-- image-preview-input -->
                                        <div class="btn btn-default image-preview-input">
                                            <span class="fa fa-folder-open"></span>
                                            <span class="numero4 image-preview-input-title">Buscar</span>
                                            <input type="file" id="iprut" name="iprut"/> <!-- rename it -->
                                        </div>
                                    </span>
                               
                                </div> 
                            </div> 
                            <div class="hide" id="popover-numero-4">
                                <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$ruta ?>" class="img-responsive">
                            </div>
                            <?php
                                if($opcional['4']['show']=='true') { 
                            ?>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    var closebtn = $('<button/>', {
                                        type: "button",
                                        text: 'x',
                                        id: 'close-preview',
                                        style: 'font-size: initial;',
                                    });
                                    closebtn.attr("class","close pull-right");
                                    $('.numero4 .image-preview').popover({
                                      html: true,
                                      title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
                                      content: function() {
                                        return $('#popover-numero-4').html();
                                      },
                                      trigger: 'manual'
                                    });

                                    $(".numero4 .image-preview").popover("show");
                                });
                            </script>
                            <?php
                                }
                            ?>
                        </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observaciones</label>
                            <?php 
                                echo CHtml::textArea('eobse', $pedidos['pedid_eobse'], array('class' => 'form-control', 'placeholder' => "Observaciones",'prompt'=>'Seleccione...')); ?>
                                
                               
                        </div>
                    </div>
                    
                </div>
                

            

        </div><!-- form -->
    </div>  
</div>
<div class="row">   
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Dibujo simple del poleron completo por ambos lados</h3>
        </div>
        <div class="panel-body" >
           
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
                <div class="row">
                    <div class="col-sm-6">        
                            <div class="numero5 form-group">
                                <label>Imagen Frontal</label>
                                <div  class="input-group image-preview" data-placement="top" >  

                                    <img id="dynamic">
                                    <!-- image-preview-filename input [CUT FROM HERE]-->
                                    <?php 
                                        $sql="SELECT * FROM pedido_pedidos_imagen WHERE pedid_codig ='".$c."' and pimag_orden=8";
                                        $result=$conexion->createCommand($sql)->queryRow();
                                        $ruta=$result['pimag_ruta'];

                                        $nombre=explode('/', $ruta);
                                        $nombre=end($nombre);
                                        if($nombre){
                                            $opcional['5']['hide']='';
                                            $opcional['5']['show']='true';
                                        }else{
                                            $opcional['5']['hide']='hide';
                                            $opcional['5']['show']='false';
                                        }
                                    ?>
                                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="true" value="<?php echo $nombre ?>" > <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-btn">
                                        <!-- image-preview-clear button -->
                                        <button type="button" class="btn btn-default image-preview-clear <?php echo $opcional['5']['hide']; ?>" >
                                            <span class="fa fa-times"></span> Limpiar
                                        </button>
                                        <!-- image-preview-input -->
                                        <div class="btn btn-default image-preview-input">
                                            <span class="fa fa-folder-open"></span>
                                            <span class="numero5  image-preview-input-title">Buscar</span>
                                            <input type="file" id="rifro" name="rifro"/> <!-- rename it -->
                                        </div>
                                    </span>
                               
                                </div> 
                            </div> 
                            <div class="hide" id="popover-numero-3">
                                <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$ruta ?>" class="img-responsive">
                            </div>
                            <?php
                                if($opcional['5']['show']=='true') { 
                            ?>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    var closebtn = $('<button/>', {
                                        type: "button",
                                        text: 'x',
                                        id: 'close-preview',
                                        style: 'font-size: initial;',
                                    });
                                    closebtn.attr("class","close pull-right");
                                    $('.numero5 .image-preview').popover({
                                      html: true,
                                      title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
                                      content: function() {
                                        return $('#popover-numero-5').html();
                                      },
                                      trigger: 'manual'
                                    });

                                    $(".numero5 .image-preview").popover("show");
                                });
                            </script>
                            <?php
                                }
                            ?>
                        </div>
                    <div class="col-sm-6">        
                            <div class="numero6 form-group">
                                <label>Imagen Espalda</label>
                                <div  class="input-group image-preview" data-placement="top" >  

                                    <img id="dynamic">
                                    <!-- image-preview-filename input [CUT FROM HERE]-->
                                    <?php 
                                        $sql="SELECT * FROM pedido_pedidos_imagen WHERE pedid_codig ='".$c."' and pimag_orden=9";
                                        $result=$conexion->createCommand($sql)->queryRow();
                                        $ruta=$result['pimag_ruta'];

                                        $nombre=explode('/', $ruta);
                                        $nombre=end($nombre);
                                        if($nombre){
                                            $opcional['6']['hide']='';
                                            $opcional['6']['show']='true';
                                        }else{
                                            $opcional['6']['hide']='hide';
                                            $opcional['6']['show']='false';
                                        }
                                    ?>
                                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="true" value="<?php echo $nombre ?>" > <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-btn">
                                        <!-- image-preview-clear button -->
                                        <button type="button" class="btn btn-default image-preview-clear <?php echo $opcional['6']['hide']; ?>" >
                                            <span class="fa fa-times"></span> Limpiar
                                        </button>
                                        <!-- image-preview-input -->
                                        <div class="btn btn-default image-preview-input">
                                            <span class="fa fa-folder-open"></span>
                                            <span class="numero4  image-preview-input-title">Buscar</span>
                                            <input type="file" id="riesp" name="riesp"/> <!-- rename it -->
                                        </div>
                                    </span>
                               
                                </div> 
                            </div> 
                            <div class="hide" id="popover-numero-6">
                                <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$ruta ?>" class="img-responsive">
                            </div>
                            <?php
                                if($opcional['6']['show']=='true') { 
                            ?>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    var closebtn = $('<button/>', {
                                        type: "button",
                                        text: 'x',
                                        id: 'close-preview',
                                        style: 'font-size: initial;',
                                    });
                                    closebtn.attr("class","close pull-right");
                                    $('.numero6 .image-preview').popover({
                                      html: true,
                                      title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
                                      content: function() {
                                        return $('#popover-numero-6').html();
                                      },
                                      trigger: 'manual'
                                    });

                                    $(".numero6 .image-preview").popover("show");
                                });
                            </script>
                            <?php
                                }
                            ?>
                        </div>
                </div>
        </div><!-- form -->
    </div>  
</div>
<div class="row">                    
    <div class="panel panel-default" >
        <div class="panel-body">
            <div class="row controls">
                <div class="col-sm-4 ">
                    <a href="modificar?p=<?php echo $p?>&c=<?php echo $c?>&s=2" type="reset" class="btn-block btn btn-default">Volver </a>
                </div>
                <div class="col-sm-4 ">
                        <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                    </div>
                <div class="col-sm-4 ">
                    <button id="guardar" type="button" class="btn-block btn btn-info">Finalizar  </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hide" id="popover-img">
    <?php
        $sql="SELECT * FROM pedido_fuente where fuent_codig = '".$pedidos['pedid_mfuen']."'";
        $fuente=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$fuente['fuent_ruta'] ?>" class="img-responsive"></div>
<div class="hide" id="popover-img-2">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_mcolo']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive">
</div>
<div class="hide" id="popover-img-3">
    <?php
        $sql="SELECT * FROM pedido_fuente where fuent_codig = '".$pedidos['pedid_eafue']."'";
        $fuente=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$fuente['fuent_ruta'] ?>" class="img-responsive"></div>
<div class="hide" id="popover-img-4">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_eacol']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive">
</div>
<div class="hide" id="popover-img-5">
    <?php
        $sql="SELECT * FROM pedido_fuente where fuent_codig = '".$pedidos['pedid_ebfue']."'";
        $fuente=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$fuente['fuent_ruta'] ?>" class="img-responsive"></div>
<div class="hide" id="popover-img-6">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_ebcol']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive">
</div>
<div class="hide" id="popover-img-7">
    <?php
        $sql="SELECT * FROM pedido_fuente where fuent_codig = '".$pedidos['pedid_aefue']."'";
        $fuente=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$fuente['fuent_ruta'] ?>" class="img-responsive"></div>
<div class="hide" id="popover-img-8">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_aecol']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive">
</div>
</form>
<script>
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {       
                /*mposi: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Posición" es obligatorio',
                        }
                    }
                },
                mtder: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar Derecha" es obligatorio',
                        }
                    }
                },
                mtizq: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar Izquierda" es obligatorio',
                        }
                    }
                },
                mfuen: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fuente" es obligatorio',
                        }
                    }
                },*/
                mtder: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar Derecha" es obligatorio',
                        }
                    }
                },
                mtizq: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar Izquierda" es obligatorio',
                        }
                    }
                },
                mfuen: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fuente" es obligatorio',
                        }
                    }
                },
                mcolo: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color" es obligatorio',
                        },
                        different: {
                            field: 'ccuer',
                            message: 'Estimado(a) Usuario(a) el campo "Color" Debe ser diferente al "Color del Cuerpo" debido a el texto no se va a notar'
                        }
                    }

                },
                mibiz: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Información a Bordar Izquierda" es obligatorio',
                        }
                    }

                },
                mtbiz: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar Izquierda" es obligatorio',
                        }
                    }

                },
                mtiiz: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Imagen a Bordar Izquierda',
                        }
                    }

                },
                /*mriiz: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Imagen a Bordar Izquierda" es obligatorio',
                        }
                    }

                },*/
                mibde: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Información a Bordar Derecha" es obligatorio',
                        }
                    }

                },
                mtbde: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar Derecha" es obligatorio',
                        }
                    }

                },
                mtide: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Imagen a Bordar Derecha" es obligatorio',
                        }
                    }

                },
                /*mride: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Imagen a Bordar Derecha" es obligatorio',
                        }
                    }

                },*//*
                limag: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Lleva Imagen Bordada" es obligatorio',
                        }
                    }
                },*/
                /*eatbo: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar" es obligatorio',
                        }
                    }
                },
                eafue: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fuente" es obligatorio',
                        }
                    }
                },*/
                eacol: {
                    validators: {
                        /*notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color" es obligatorio',
                        },*/
                        different: {
                            field: 'ccuer',
                            message: 'Estimado(a) Usuario(a) el campo "Color" Debe ser diferente al "Color del Cuerpo" debido a el texto no se va a notar'
                        }
                    }
                },/*
                ebtbo: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar" es obligatorio',
                        }
                    }
                },
                ebfue: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fuente" es obligatorio',
                        }
                    }
                },*/
                ebcol: {
                    validators: {
                        /*notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color" es obligatorio',
                        },
                        */different: {
                            field: 'ccuer',
                            message: 'Estimado(a) Usuario(a) el campo "Color" Debe ser diferente al "Color del Cuerpo" debido a el texto no se va a notar'
                        }
                    }
                },
                /*aetbo: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color" es obligatorio',
                        }
                    }
                },*/
                aefue: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fuente" es obligatorio',
                        }
                    }
                },
                aecol: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color" es obligatorio',
                        },
                        different: {
                            field: 'ccuer',
                            message: 'Estimado(a) Usuario(a) el campo "Color" Debe ser diferente al "Color del Cuerpo" debido a el texto no se va a notar'
                        }

                    }
                },
                /*mrima: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Imagen Libre" es obligatorio',
                        }
                    }
                },*/
                /*iprut: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Imagen Principal" es obligatorio',
                        }
                    }
                },
                rifro: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Imagen Frontal" es obligatorio',
                        }
                    }
                },
                riesp: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Imagen Espalda" es obligatorio',
                        }
                    }
                }*/
            }
        });
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        var data = new FormData(jQuery('form')[0]);
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                url: 'paso3',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });

                },
                success: function (response) {
                    $('#wrapper').unblock();
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({   
                            title: "Exito!",   
                            text: response['msg'],
                            type: "success",   
                            showCancelButton: true,  
                            confirmButtonText: "Enviar Pedido",   
                            cancelButtonText: "Cerrar",   
                            closeOnConfirm: false,   
                            closeOnCancel: false 
                        }, function(isConfirm){   
                            if (isConfirm) {     
                                $.ajax({
                                    dataType: "json",
                                    url: 'enviar',
                                    data: data,
                                    cache: false,
                                    contentType: false,
                                    processData: false,
                                    type: 'post',
                                    beforeSend: function () {
                                        $('#wrapper').block({
                                            message: '<h4>Espere unos momentos</h4>'
                                            , css: {
                                                border: '1px solid #fff'
                                            }
                                        });
                                    },
                                    success: function (response) {
                    $('#wrapper').unblock();
                                        $('#wrapper').unblock();
                                        if (response['success'] == 'true') {
                                            swal({ 
                                                title: "Exito!",
                                                text: response['msg'],
                                                type: "success",
                                                confirmButtonText: "Cerrar",
                                                confirmButtonClass: "btn-info"
                                            },function(){
                                                <?php

                                                if(Yii::app()->user->id['usuario']['permiso']==1){
                                                    ?>                  
                                                window.open('../../reportes/pedidos/pdf?c=<?php echo $c; ?>', '_blank');
                                                <?php
                                                }else{
                                                ?>
                                                window.open('listado', '_parent');
                                                <?php
                                                }
                                                ?>
                                            });
                                        } else {
                                            swal({ 
                                                title: "Error!",
                                                text: response['msg'],
                                                type: "error",
                                                confirmButtonText: "Cerrar",
                                                confirmButtonClass: "btn-danger"
                                            },function(){
                                                $("#guardar").removeAttr('disabled');
                                            });
                                        }

                                    },error:function (response) {
                    $('#wrapper').unblock();
                                        $('#wrapper').unblock();
                                        swal({ 
                                            title: "Error!",
                                            text: "Error el ejecutar la operación",
                                            type: "error",
                                            confirmButtonText: "Cerrar",
                                            confirmButtonClass: "btn-danger"

                                        },function(){
                                            $("#guardar").removeAttr('disabled');
                                        });
                                            
                                    }
                                });   
                            } else {     
                                window.open('listado?c=<?php echo $c; ?>', '_parent');   
                            } 
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>
<script>

    $(document).on('click', '#close-preview', function () {
        $('.numero1 .image-preview').popover('hide');
        // Hover befor close the preview
        $('.numero1 .image-preview').hover(
            function () {
                $('.image-preview').popover('hide');
            },
            function () {
                $('.image-preview').popover('hide');
            }
        );
    });
    $(function () {
        // Create the close button
        var closebtn = $('<button/>', {
            type: "button",
            text: 'x',
            id: 'close-preview',
            style: 'font-size: initial;',
        });
        closebtn.attr("class","close pull-right");
        $('.numero1 .image-preview').popover({
            trigger:'manual',
            html:true,
            title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
            content: "No hay imagen",
            placement:'top'
        });



        
        // Clear event
        $('.numero1 .image-preview-clear').click(function () {
            $('.numero1 .image-preview').attr("data-content", "").popover('hide');
            $('.numero1 .image-preview-filename').val("");
            $('.numero1 .image-preview-clear').hide();
            $('.numero1 .image-preview-input input:file').val("");
            $(".numero1 .image-preview-input-title").text("Buscar");
        });
        // Create the preview image
        $(".numero1 .image-preview-input input:file").change(function () {
            var img = $('<img/>', {
                id: 'dynamic',
                width: 250,
                height: 200
            });
            var file = this.files[0];
            var reader = new FileReader();
            // Set preview image into the popover data-content
            reader.onload = function (e) {
                $(".numero1 .image-preview-input-title").text("Cambiar");
                $(".numero1 .image-preview-clear").show();
                $(".numero1 .image-preview-filename").val(file.name);
                img.attr('src', e.target.result);
                $(".numero1 .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
                }
            reader.readAsDataURL(file);
        });
    });
</script>
<script>

    $(document).on('click', '#close-preview', function () {
        $('.numero2 .image-preview').popover('hide');
        // Hover befor close the preview
        $('.numero2 .image-preview').hover(
            function () {
                $('.image-preview').popover('hide');
            },
            function () {
                $('.image-preview').popover('hide');
            }
        );
    });
    $(function () {
        // Create the close button
        var closebtn = $('<button/>', {
            type: "button",
            text: 'x',
            id: 'close-preview',
            style: 'font-size: initial;',
        });
        closebtn.attr("class","close pull-right");
        $('.numero2 .image-preview').popover({
            trigger:'manual',
            html:true,
            title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
            content: "No hay imagen",
            placement:'top'
        });



        
        // Clear event
        $('.numero2 .image-preview-clear').click(function () {
            $('.numero2 .image-preview').attr("data-content", "").popover('hide');
            $('.numero2 .image-preview-filename').val("");
            $('.numero2 .image-preview-clear').hide();
            $('.numero2 .image-preview-input input:file').val("");
            $(".numero2 .image-preview-input-title").text("Buscar");
        });
        // Create the preview image
        $(".numero2 .image-preview-input input:file").change(function () {
            var img = $('<img/>', {
                id: 'dynamic',
                width: 250,
                height: 200
            });
            var file = this.files[0];
            var reader = new FileReader();
            // Set preview image into the popover data-content
            reader.onload = function (e) {
                $(".numero2 .image-preview-input-title").text("Cambiar");
                $(".numero2 .image-preview-clear").show();
                $(".numero2 .image-preview-filename").val(file.name);
                img.attr('src', e.target.result);
                $(".numero2 .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
                }
            reader.readAsDataURL(file);
        });
    });
</script>
<script>

    $(document).on('click', '#close-preview', function () {
        $('.numero3 .image-preview').popover('hide');
        // Hover befor close the preview
        $('.numero3 .image-preview').hover(
            function () {
                $('.image-preview').popover('hide');
            },
            function () {
                $('.image-preview').popover('hide');
            }
        );
    });
    $(function () {
        // Create the close button
        var closebtn = $('<button/>', {
            type: "button",
            text: 'x',
            id: 'close-preview',
            style: 'font-size: initial;',
        });
        closebtn.attr("class","close pull-right");
        $('.numero3 .image-preview').popover({
            trigger:'manual',
            html:true,
            title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
            content: "No hay imagen",
            placement:'top'
        });



        
        // Clear event
        $('.numero3 .image-preview-clear').click(function () {
            $('.numero3 .image-preview').attr("data-content", "").popover('hide');
            $('.numero3 .image-preview-filename').val("");
            $('.numero3 .image-preview-clear').hide();
            $('.numero3 .image-preview-input input:file').val("");
            $(".numero3 .image-preview-input-title").text("Buscar");
        });
        // Create the preview image
        $(".numero3 .image-preview-input input:file").change(function () {
            var img = $('<img/>', {
                id: 'dynamic',
                width: 250,
                height: 200
            });
            var file = this.files[0];
            var reader = new FileReader();
            // Set preview image into the popover data-content
            reader.onload = function (e) {
                $(".numero3 .image-preview-input-title").text("Cambiar");
                $(".numero3 .image-preview-clear").show();
                $(".numero3 .image-preview-filename").val(file.name);
                img.attr('src', e.target.result);
                $(".numero3 .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
                }
            reader.readAsDataURL(file);
        });
    });
</script>
<script>

    $(document).on('click', '#close-preview', function () {
        $('.numero4 .image-preview').popover('hide');
        // Hover befor close the preview
        $('.numero4 .image-preview').hover(
            function () {
                $('.image-preview').popover('hide');
            },
            function () {
                $('.image-preview').popover('hide');
            }
        );
    });
    $(function () {
        // Create the close button
        var closebtn = $('<button/>', {
            type: "button",
            text: 'x',
            id: 'close-preview',
            style: 'font-size: initial;',
        });
        closebtn.attr("class","close pull-right");
        $('.numero4 .image-preview').popover({
            trigger:'manual',
            html:true,
            title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
            content: "No hay imagen",
            placement:'top'
        });



        
        // Clear event
        $('.numero4 .image-preview-clear').click(function () {
            $('.numero4 .image-preview').attr("data-content", "").popover('hide');
            $('.numero4 .image-preview-filename').val("");
            $('.numero4 .image-preview-clear').hide();
            $('.numero4 .image-preview-input input:file').val("");
            $(".numero4 .image-preview-input-title").text("Buscar");
        });
        // Create the preview image
        $(".numero4 .image-preview-input input:file").change(function () {
            var img = $('<img/>', {
                id: 'dynamic',
                width: 250,
                height: 200
            });
            var file = this.files[0];
            var reader = new FileReader();
            // Set preview image into the popover data-content
            reader.onload = function (e) {
                $(".numero4 .image-preview-input-title").text("Cambiar");
                $(".numero4 .image-preview-clear").show();
                $(".numero4 .image-preview-filename").val(file.name);
                img.attr('src', e.target.result);
                $(".numero4 .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
                }
            reader.readAsDataURL(file);
        });
    });
</script>

<script>

    $(document).on('click', '#close-preview', function () {
        $('.numero5 .image-preview').popover('hide');
        // Hover befor close the preview
        $('.numero5 .image-preview').hover(
            function () {
                $('.image-preview').popover('hide');
            },
            function () {
                $('.image-preview').popover('hide');
            }
        );
    });
    $(function () {
        // Create the close button
        var closebtn = $('<button/>', {
            type: "button",
            text: 'x',
            id: 'close-preview',
            style: 'font-size: initial;',
        });
        closebtn.attr("class","close pull-right");
        $('.numero5 .image-preview').popover({
            trigger:'manual',
            html:true,
            title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
            content: "No hay imagen",
            placement:'top'
        });



        
        // Clear event
        $('.numero5 .image-preview-clear').click(function () {
            $('.numero5 .image-preview').attr("data-content", "").popover('hide');
            $('.numero5 .image-preview-filename').val("");
            $('.numero5 .image-preview-clear').hide();
            $('.numero5 .image-preview-input input:file').val("");
            $(".numero5 .image-preview-input-title").text("Buscar");
        });
        // Create the preview image
        $(".numero5 .image-preview-input input:file").change(function () {
            var img = $('<img/>', {
                id: 'dynamic',
                width: 250,
                height: 200
            });
            var file = this.files[0];
            var reader = new FileReader();
            // Set preview image into the popover data-content
            reader.onload = function (e) {
                $(".numero5 .image-preview-input-title").text("Cambiar");
                $(".numero5 .image-preview-clear").show();
                $(".numero5 .image-preview-filename").val(file.name);
                img.attr('src', e.target.result);
                $(".numero5 .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
                }
            reader.readAsDataURL(file);
        });
    });
</script>

<script>

    $(document).on('click', '#close-preview', function () {
        $('.numero6 .image-preview').popover('hide');
        // Hover befor close the preview
        $('.numero6 .image-preview').hover(
            function () {
                $('.image-preview').popover('hide');
            },
            function () {
                $('.image-preview').popover('hide');
            }
        );
    });
    $(function () {
        // Create the close button
        var closebtn = $('<button/>', {
            type: "button",
            text: 'x',
            id: 'close-preview',
            style: 'font-size: initial;',
        });
        closebtn.attr("class","close pull-right");
        $('.numero6 .image-preview').popover({
            trigger:'manual',
            html:true,
            title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
            content: "No hay imagen",
            placement:'top'
        });



        
        // Clear event
        $('.numero6 .image-preview-clear').click(function () {
            $('.numero6 .image-preview').attr("data-content", "").popover('hide');
            $('.numero6 .image-preview-filename').val("");
            $('.numero6 .image-preview-clear').hide();
            $('.numero6 .image-preview-input input:file').val("");
            $(".numero6 .image-preview-input-title").text("Buscar");
        });
        // Create the preview image
        $(".numero6 .image-preview-input input:file").change(function () {
            var img = $('<img/>', {
                id: 'dynamic',
                width: 250,
                height: 200
            });
            var file = this.files[0];
            var reader = new FileReader();
            // Set preview image into the popover data-content
            reader.onload = function (e) {
                $(".numero6 .image-preview-input-title").text("Cambiar");
                $(".numero6 .image-preview-clear").show();
                $(".numero6 .image-preview-filename").val(file.name);
                img.attr('src', e.target.result);
                $(".numero6 .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
                }
            reader.readAsDataURL(file);
        });
    });
</script>
<script type="text/javascript">
    $('#mposi').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '1':
                jQuery("#mtder").removeAttr('disabled');
                jQuery("#mtizq").attr('disabled','true');
                jQuery("#mtizq").val('');
                break;
            case '2':
                jQuery("#mtizq").removeAttr('disabled');
                jQuery("#mtder").attr('disabled','true');
                jQuery("#mtder").val('');
                break;
            case '3':
                jQuery("#mtder").removeAttr('disabled');
                jQuery("#mtizq").removeAttr('disabled');
                break;
            default:
                jQuery("#mtder").attr('disabled','true');
                jQuery("#mtizq").attr('disabled','true');
                jQuery("#mtder").val('');
                jQuery("#mtizq").val('');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#iopci').change(function () {
        var iopci = $(this).val();
        $.ajax({
            'type':'POST',
            'data':{'tmode':tmode.value},
            'url':'<?php echo CController::createUrl('funciones/Recepción de DispositivosColorOpcional'); ?>',
            'cache':false,
            'success':function(html){
                switch(iopci) {
                    case '1':
                        
                        jQuery("#opcional-1").removeClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#iclin").html(html);
                        jQuery("#iccie").html('');
                        jQuery("#icviv").html('');
                        jQuery("#ideta").removeAttr('disabled');
                        jQuery("#iclin").removeAttr('disabled');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").attr('disabled','true');

                        break;
                    case '2':
                        jQuery("#opcional-2").removeClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#iclin").html('');
                        jQuery("#iccie").html(html);
                        jQuery("#icviv").html('');
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").removeAttr('disabled');
                        jQuery("#icviv").attr('disabled','true');
                        break;
                    case '3':
                        jQuery("#opcional-3").removeClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#iclin").html('');
                        jQuery("#iccie").html('');
                        jQuery("#icviv").html(html);
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").removeAttr('disabled');
                        break;
                    default:
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#iclin").html(html);
                        jQuery("#iccie").html(html);
                        jQuery("#icviv").html(html);
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").attr('disabled','true');
                        break;
                }
                
            }
        });
    });
</script>
<script type="text/javascript">
    $('#limag').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '1':
                jQuery("#opcional-1").removeClass('hide');
                jQuery("#prima").removeAttr('disabled');
                jQuery("#opcional-2").addClass('hide');
                jQuery("#ptbor").attr('disabled','true');
                break;
            case '2':
                jQuery("#opcional-1").addClass('hide');
                jQuery("#prima").attr('disabled','true');
                jQuery("#opcional-2").removeClass('hide');
                jQuery("#ptbor").removeAttr('disabled');
                break;
            default:
                jQuery("#opcional-1").addClass('hide');
                jQuery("#prima").attr('disabled','true');
                jQuery("#opcional-2").addClass('hide');
                jQuery("#ptbor").attr('disabled','true');
                break;
        }
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-1').popover({
          html: true,
          content: function() {
            return $('#popover-img').html();
          },
          trigger: 'hover'
        });
});
$('#mfuen').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/Recepción de DispositivosVerFuente'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-2').popover({
          html: true,
          content: function() {
            return $('#popover-img-2').html();
          },
          trigger: 'hover'
        });
});
$('#mcolo').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/Recepción de DispositivosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-2").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-3').popover({
          html: true,
          content: function() {
            return $('#popover-img-3').html();
          },
          trigger: 'hover'
        });
});
$('#eafue').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/Recepción de DispositivosVerFuente'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-3").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-4').popover({
          html: true,
          content: function() {
            return $('#popover-img-4').html();
          },
          trigger: 'hover'
        });
});
$('#eacol').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/Recepción de DispositivosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-4").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-5').popover({
          html: true,
          content: function() {
            return $('#popover-img-5').html();
          },
          trigger: 'hover'
        });
});
$('#ebfue').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/Recepción de DispositivosVerFuente'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-5").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-6').popover({
          html: true,
          content: function() {
            return $('#popover-img-6').html();
          },
          trigger: 'hover'
        });
});
$('#ebcol').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/Recepción de DispositivosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-6").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-7').popover({
          html: true,
          content: function() {
            return $('#popover-img-7').html();
          },
          trigger: 'hover'
        });
});
$('#aefue').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/Recepción de DispositivosVerFuente'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-7").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-8').popover({
          html: true,
          content: function() {
            return $('#popover-img-8').html();
          },
          trigger: 'hover'
        });
});
$('#aecol').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/Recepción de DispositivosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-8").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>

<script type="text/javascript">
    function cambiarAtributo(id,campo,p_estados) {
        if(p_estados=='1') {
            jQuery(id).removeClass('hide');
            if(campo!=''){
                jQuery(campo).removeAttr('disabled');
                
            }
        }else{
            jQuery(id).addClass('hide');
            if(campo!=''){
                jQuery(campo).attr('disabled','true');
                jQuery(campo).val(''); 
            }
        }
    }
</script>
<script type="text/javascript">
    $('#mposi').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '1':
                cambiarAtributo('#m-d-infor','#mibde','1');
                cambiarAtributo('#m-i-infor','#mibiz','2');
                cambiarAtributo('#m-i-texto','#mtbiz','2');
                cambiarAtributo('#m-i-timag','#mtiiz','2');
                cambiarAtributo('#m-i-image','#mriiz','2');
                break;
            case '2':
                cambiarAtributo('#m-i-infor','#mibiz','1');
                cambiarAtributo('#m-d-infor','#mibde','2');
                cambiarAtributo('#m-d-texto','#mtbde','2');
                cambiarAtributo('#m-d-timag','#mtide','2');
                cambiarAtributo('#m-d-image','#mride','2');
                break;
            case '3':
                cambiarAtributo('#m-d-infor','#mibde','1');
                cambiarAtributo('#m-i-infor','#mibiz','1');
                break;
            default:
                cambiarAtributo('#m-i-infor','#mibiz','2');
                cambiarAtributo('#m-i-texto','#mtbiz','2');
                cambiarAtributo('#m-i-timag','#mtiiz','2');
                cambiarAtributo('#m-i-image','#mriiz','2');
                cambiarAtributo('#m-d-infor','#mibde','2');
                cambiarAtributo('#m-d-texto','#mtbde','2');
                cambiarAtributo('#m-d-timag','#mtide','2');
                cambiarAtributo('#m-d-image','#mride','2');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#mibde').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '1':
                cambiarAtributo('#m-d-timag','#mtide','1');
                cambiarAtributo('#m-d-texto','#mtbde','2');
                break;
            case '2':
                cambiarAtributo('#m-d-texto','#mtbde','1');
                cambiarAtributo('#m-d-timag','#mtide','2');
                cambiarAtributo('#m-d-image','#mride','2');
                break;
            case '3':
                cambiarAtributo('#m-d-texto','#mtbde','1');
                cambiarAtributo('#m-d-timag','#mtide','1');
                break;
            default:
                cambiarAtributo('#m-d-texto','#mtbde','2');
                cambiarAtributo('#m-d-timag','#mtide','2');
                cambiarAtributo('#m-d-image','#mride','2');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#mtide').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '5':
                cambiarAtributo('#m-d-image','#mride','1');
                break;
            default:
                cambiarAtributo('#m-d-image','#mride','2');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#mibiz').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '1':
                cambiarAtributo('#m-i-timag','#mtiiz','1');
                cambiarAtributo('#m-i-texto','#mtbiz','2');
                break;
            case '2':
                cambiarAtributo('#m-i-texto','#mtbiz','1');
                cambiarAtributo('#m-i-timag','#mtiiz','2');
                cambiarAtributo('#m-i-image','#mriiz','2');
                break;
            case '3':
                cambiarAtributo('#m-i-texto','#mtbiz','1');
                cambiarAtributo('#m-i-timag','#mtiiz','1');
                break;
            default:
                cambiarAtributo('#m-i-texto','#mtbiz','2');
                cambiarAtributo('#m-i-timag','#mtiiz','2');
                cambiarAtributo('#m-i-image','#mriiz','2');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#mtiiz').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '5':
                cambiarAtributo('#m-i-image','#mriiz','1');
                break;
            default:
                cambiarAtributo('#m-i-image','#mriiz','2');
                break;
        }
    });
</script>