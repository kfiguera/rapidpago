<?php $conexion = Yii::app()->db; ?>
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Solicitudes</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Registro y Documentación</a></li>
            <li><a href="#">Solicitudes</a></li>
            <li class="active">Registrar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

<?php $this->renderPartial('pasos', array('ubicacion' => $ubicacion,'solicitud'=>$solicitud));?>
<form id='login-form' name='login-form' method="post">

<div class="row">   
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Representantes Legales</h3>
        </div>
        <div class="panel-body" >
            <div class="row hide">
                <div class="col-sm-4">        
                    <div class="form-group">
                        <label>Pedido *</label>
                        <?php 
                            echo CHtml::hiddenField('pedid', $p, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                    </div>
                </div>
                <div class="col-sm-4">        
                    <div class="form-group">
                        <label>Código *</label>
                        <?php 
                            echo CHtml::hiddenField('codig', $c, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                    </div>
                </div>
                <div class="col-sm-4">        
                    <div class="form-group">
                        <label>Paso *</label>
                        <?php 
                            echo CHtml::hiddenField('pasos', $solicitud['prere_pasos'], array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                    </div>
                </div>
            </div>
            <input class="form-control" placeholder="Porcentaje" readonly="readonly" type="hidden" value="" name="bookindex" id="bookindex" /> 
                <!-- Plantilla -->
                <!-- OPCIONALES-->
                <?php
                    $sql="SELECT * FROM cliente WHERE clien_codig='".$solicitud['clien_codig']."'";
                    $cliente=$conexion->createCommand($sql)->queryRow();

                    $sql="SELECT * 
                          FROM cliente_representante
                          WHERE clien_codig='".$cliente['clien_codig']."'";
                    $rlegal=$conexion->createCommand($sql)->query();
                    
                    $row = $rlegal->read();
                    $i=0;
                    do{
                ?>
                    <div class="book">
                        <div class="form-group">
                        <div class="row">
                            <div class="col-xs-4">        
                            <label>RIF *</label>
                                <?php 
                                    echo CHtml::textField('rifrl['.$i.']', $row['rlega_rifrl'], array('class' => 'form-control rifrl', 'placeholder' => "RIF",'prompt'=>'Seleccione...','id'=>'rifrl_'.$i.'')); 
                                ?>
                                <script >
                                    $('#rifrl_<?php echo $i; ?>').change(function () {
                                        var formValidation = $("#login-form").data("formValidation");
                                        formValidation.revalidateField("rifrl[<?php echo $i; ?>]");
                                        
                                    });    
                                </script>
                            </div>

                            <div class="col-xs-4">        
                                <label>Nombre *</label>
                                <?php 
                                    echo CHtml::textField('nombr['.$i.']', $row['rlega_nombr'], array('class' => 'form-control', 'placeholder' => "Nombre",'prompt'=>'Seleccione...','id'=>'nombr_'.$i.'')); ?>
                            </div>
                            <div class="col-xs-4">        
                                <label>Apellido *</label>
                                <?php 
                                    echo CHtml::textField('apell['.$i.']', $row['rlega_apell'], array('class' => 'form-control', 'placeholder' => "Apellido",'prompt'=>'Seleccione...','id'=>'apell_'.$i.'')); ?>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-4">        
                                    <label>Tipo de Documento *</label>
                                    <?php 
                                        $sql="SELECT * 
                                              FROM p_documento_tipo a
                                              JOIN p_tipo_cliente_documento b ON (a.tdocu_codig = b.tdocu_codig)
                                              WHERE b.tclie_codig='1'
                                              ORDER BY 1";
                                        $tdocu=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($tdocu,'tdocu_codig','tdocu_descr');
                                        echo CHtml::dropDownList('tdocu['.$i.']', $row['tdocu_codig'], $data , array('class' => 'form-control', 'placeholder' => "Tipo de Documento",'prompt'=>'Seleccione...','id'=>'tdocu_'.$i.'')); ?>
                                </div>
                                <div class="col-xs-4">        
                                    <label>C.I/Pasaporte *</label>
                                    <?php 
                                        echo CHtml::textField('ndocu['.$i.']', $row['rlega_ndocu'], array('class' => 'form-control', 'placeholder' => "C.I/Pasaporte",'prompt'=>'Seleccione...','id'=>'ndocu_'.$i.'')); ?>
                                </div>
                                <div class="col-xs-4">        
                                    <label>Cargo *</label>
                                    <?php 
                                        echo CHtml::textField('cargo['.$i.']', $row['rlega_cargo'], array('class' => 'form-control', 'placeholder' => "Cargo",'prompt'=>'Seleccione...','id'=>'cargo_'.$i.'')); ?>
                                </div>
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-4">        
                                    <label>Telefono Movil *</label>
                                    <?php 
                                        echo CHtml::textField('tmovi['.$i.']', $row['rlega_tmovi'], array('class' => 'form-control tmovi', 'placeholder' => "Telefono Movil",'prompt'=>'Seleccione...','id'=>'tmovi_'.$i.'')); ?>
                                </div>
                                <div class="col-xs-4">        
                                    <label>Telefono Fijo</label>
                                    <?php 
                                        echo CHtml::textField('tfijo['.$i.']', $row['rlega_tfijo'], array('class' => 'form-control tfijo', 'placeholder' => "Telefono Fijo",'prompt'=>'Seleccione...','id'=>'tfijo_'.$i.'')); ?>
                                </div>
                                <div class="col-xs-3">        
                                    <label>Correo Electronico *</label>
                                    <?php 
                                        echo CHtml::textField('celec['.$i.']', $row['rlega_corre'], array('class' => 'form-control', 'placeholder' => "Correo Electronico",'prompt'=>'Seleccione...','id'=>'celec_'.$i.'')); ?>
                                </div>
                            <?php
                                if($i==0){
                            ?>
                                <div class="col-xs-1">
                                    <label>&nbsp;</label>
                                    <br>
                                    <button type="button" class="btn btn-success btn-block addButton"><i class="fa fa-plus"></i></button>
                                </div>
                            <?php
                                }else{
                            ?>
                                <div class="col-xs-1">
                                    <label>&nbsp;</label>
                                    <br>
                                    <div class="btn-group btn-group-justified">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-success addButton"><i class="fa fa-plus"></i></button>
                                        </div>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-danger removeButton"><i class="fa fa-minus"></i></button>
                                        </div>
                                    </div>
                                </div>
                            
                            <?php
                                }
                                $a++;
                                $i++;
                            ?>
                                
                            </div>
                        </div>
                    </div>
                <?php
                    }while(($row = $rlegal->read()) !== false);
                ?>
                        
                    
                <?php
                
                $a++;
                $i++;
                ?>
                        
                
                        <!-- FIN OPCIONALES-->
                    
                <div class="book hide" id="bookTemplate">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-4">        
                            <label>RIF *</label>
                                <?php 
                                    echo CHtml::textField('rifrl_', $row['rlega_rifrl'], array('class' => 'form-control rifrl', 'placeholder' => "RIF",'prompt'=>'Seleccione...','id'=>'rifrl_')); 
                                ?>
                            </div>
                            <div class="col-xs-4">        
                                <label>Nombre *</label>
                                <?php 
                                    echo CHtml::textField('nombr_', $row['rlega_nombr'], array('class' => 'form-control', 'placeholder' => "Nombre",'prompt'=>'Seleccione...','id'=>'nombr_')); ?>
                            </div>
                            <div class="col-xs-4">        
                                <label>Apellido *</label>
                                <?php 
                                    echo CHtml::textField('apell_', $row['rlega_apell'], array('class' => 'form-control', 'placeholder' => "Apellido",'prompt'=>'Seleccione...','id'=>'apell_')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-4">        
                                <label>Tipo de Documento *</label>
                                <?php 
                                    $sql="SELECT * 
                                          FROM p_documento_tipo a
                                          JOIN p_tipo_cliente_documento b ON (a.tdocu_codig = b.tdocu_codig)
                                          WHERE b.tclie_codig='1'
                                          ORDER BY 1";
                                    $tdocu=$conexion->createCommand($sql)->queryAll();
                                    $data=CHtml::listData($tdocu,'tdocu_codig','tdocu_descr');
                                    echo CHtml::dropDownList('tdocu_', $row['tdocu_codig'], $data , array('class' => 'form-control', 'placeholder' => "Tipo de Documento",'prompt'=>'Seleccione...','id'=>'tdocu_')); ?>
                            </div>
                            <div class="col-xs-4">        
                                <label>C.I/Pasaporte *</label>
                                <?php 
                                    echo CHtml::textField('ndocu_', $row['rlega_ndocu'], array('class' => 'form-control', 'placeholder' => "C.I/Pasaporte",'prompt'=>'Seleccione...','id'=>'ndocu_')); ?>
                            </div>
                            <div class="col-xs-4">        
                                <label>Cargo *</label>
                                <?php 
                                    echo CHtml::textField('cargo_', $row['rlega_cargo'], array('class' => 'form-control', 'placeholder' => "Cargo",'prompt'=>'Seleccione...','id'=>'cargo_')); ?>
                            </div>
                            
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-4">        
                                <label>Telefono Movil *</label>
                                <?php 
                                    echo CHtml::textField('tmovi_', $row['rlega_tmovi'], array('class' => 'form-control tmovi', 'placeholder' => "Telefono Movil",'prompt'=>'Seleccione...','id'=>'tmovi_')); ?>
                            </div>
                            <div class="col-xs-4">        
                                <label>Telefono Fijo</label>
                                <?php 
                                    echo CHtml::textField('tfijo_', $row['rlega_tfijo'], array('class' => 'form-control tfijo', 'placeholder' => "Telefono Fijo",'prompt'=>'Seleccione...','id'=>'tfijo_')); ?>
                            </div>
                            <div class="col-xs-3">        
                                <label>Correo Electronico *</label>
                                <?php 
                                    echo CHtml::textField('celec_', $row['rlega_corre'], array('class' => 'form-control', 'placeholder' => "Correo Electronico",'prompt'=>'Seleccione...','id'=>'celec_')); ?>
                            </div>
                            <div class="col-xs-1">
                                <label>&nbsp;</label>
                                <br>
                                <div class="btn-group btn-group-justified">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-success addButton"><i class="fa fa-plus"></i></button>
                                    </div>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-danger removeButton"><i class="fa fa-minus"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>    
        </div>
    </div>
</div>
<div class="row">   
    <div class="panel panel-default">
        <div class="panel-body" >
                <!-- Button -->
                <div class="row controls">
                    <div class="col-sm-4 ">
                        <a href="modificar?c=<?php echo $c?>&s=2" type="reset" class="btn-block btn btn-default">Volver </a>
                    </div>
                    <div class="col-sm-4 ">
                        <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                    </div>
                    <div class="col-sm-4 ">
                        <button id="guardar" type="button" class="btn-block btn btn-info">Siguiente  </button>
                    </div>
                </div>


        </div><!-- form -->
    </div>  
</div>
</form>
<script type="text/javascript">
    $('.rifrl').mask('Z000000000', {
        'translation': {
          'Z': {
            pattern: /[V|E|v|e]/
          }
        }
      });
    $('.tmovi').mask("0000-000.00.00");
    $('.tfijo').mask("0000-000.00.00");
</script>
<script type="text/javascript">
    var rifrl = {
            validators: {
                notEmpty: {
                    message: 'Estimado(a) Usuario(a) el campo "RIF" es obligatorio',
                },
                regexp: {
                    regexp: /^[V|E|v|e]\d{8}\d+$/,
                    message: 'Estimado(a) Usuario(a) el campo "RIF" debe poseer el siguiente formato: V000000000'
                }
            }
        },
        nombr = {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Nombre" es obligatorio',
                        },
                        regexp: {
                            regexp: /^[a-zA-ZñÑ]+$/i,
                            message: 'Estimado(a) Usuario(a) el campo "Nombre" solo debe poseer letras'
                        },
                    }
                },
        apell = {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Apellido" es obligatorio',
                        },
                        regexp: {
                            regexp: /^[a-zA-ZñÑ]+$/i,
                            message: 'Estimado(a) Usuario(a) el campo "Apellido" solo debe poseer letras'
                        }
                    }
                },
        tdocu = {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Documento" es obligatorio',
                        }
                    }
                },
        ndocu = {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "C.I/Pasaporte" es obligatorio',
                        },
                         numeric: {
                            message: 'Estimado(a) Usuario(a) el campo "C.I/Pasaporte" debe ser numérico',
                            // The default separators
                            //thousandsSeparator: '',
                            //decimalSeparator: '.'
                        },
                        stringLength: {
                            max: 10,
                            message: 'Estimado(a) Usuario(a) el campo "C.I/Pasaporte" permite hasta 10 caracteres'
                        }
                    }
                },
        cargo = {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Cargo" es obligatorio',
                        }
                    }
                },
        tmovi = {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Telefono Movil" es obligatorio',
                        }
                    }
                },
        tfijo = {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Telefono Fijo" es obligatorio',
                        }
                    }
                },
        celec = {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Correo Electronico" es obligatorio',
                        },
                        emailAddress: {
                            message: 'Estimado(a) Usuario(a) el campo "Correo Electronico"debe poseer el siguiente formato: ejemplo@rapidpago.com '
                        }
                    }
                },
        bookIndex = <?php echo $a; ?>,
        contador = 0;
        
</script>


<script>
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'rifrl[0]': rifrl,
                'nombr[0]': nombr,
                'apell[0]': apell,
                'tdocu[0]': tdocu,
                'ndocu[0]': ndocu,
                'cargo[0]': cargo,
                'tmovi[0]': tmovi,
                //'tfijo[0]': tfijo,
                'celec[0]': celec,
                
            }
        });
    }).on('click', '.addButton', function() {
        //if(contador<4){ 
            contador++;
            //alert(contador);
            bookIndex++;
         
            document.getElementById('bookindex').value = bookIndex;
            var $template = $('#bookTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .attr('data-book-index', bookIndex)
                                .insertBefore($template);

            // Update the name attributes
            $clone
                .find('[name="rifrl_"]').attr('name', 'rifrl[' + bookIndex + ']').end()
                .find('[name="nombr_"]').attr('name', 'nombr[' + bookIndex + ']').end()
                .find('[name="apell_"]').attr('name', 'apell[' + bookIndex + ']').end()
                .find('[name="tdocu_"]').attr('name', 'tdocu[' + bookIndex + ']').end()
                .find('[name="ndocu_"]').attr('name', 'ndocu[' + bookIndex + ']').end()
                .find('[name="cargo_"]').attr('name', 'cargo[' + bookIndex + ']').end()
                .find('[name="tmovi_"]').attr('name', 'tmovi[' + bookIndex + ']').end()
                .find('[name="tfijo_"]').attr('name', 'tfijo[' + bookIndex + ']').end()
                .find('[name="celec_"]').attr('name', 'celec[' + bookIndex + ']').end()


                .find('[id="rifrl_"]').attr('id', 'rifrl_' + bookIndex ).end()
                .find('[id="nombr_"]').attr('id', 'nombr_' + bookIndex ).end()
                .find('[id="apell_"]').attr('id', 'apell_' + bookIndex ).end()
                .find('[id="tdocu_"]').attr('id', 'tdocu_' + bookIndex ).end()
                .find('[id="ndocu_"]').attr('id', 'ndocu_' + bookIndex ).end()
                .find('[id="cargo_"]').attr('id', 'cargo_' + bookIndex ).end()
                .find('[id="tmovi_"]').attr('id', 'tmovi_' + bookIndex ).end()
                .find('[id="tfijo_"]').attr('id', 'tfijo_' + bookIndex ).end()
                .find('[id="celec_"]').attr('id', 'celec_' + bookIndex ).end();

            // Add new fields
            // Note that we also pass the validator rules for new field as the third parameter
            $('#login-form')
                .formValidation('addField', 'rifrl[' + bookIndex + ']', rifrl)
                .formValidation('addField', 'nombr[' + bookIndex + ']', nombr)
                .formValidation('addField', 'apell[' + bookIndex + ']', apell)
                .formValidation('addField', 'tdocu[' + bookIndex + ']', tdocu)
                .formValidation('addField', 'ndocu[' + bookIndex + ']', ndocu)
                .formValidation('addField', 'cargo[' + bookIndex + ']', cargo)
                .formValidation('addField', 'tmovi[' + bookIndex + ']', tmovi)
                .formValidation('addField', 'tfijo[' + bookIndex + ']', tfijo)
                .formValidation('addField', 'celec[' + bookIndex + ']', celec);
            

            //agregar ajax
            $('#login-form').append(
                '<script type="text/javascript">\n'+
                    '$("#rifrl_' + bookIndex + '").mask("Z000000000", {'+
                        '"translation": {'+
                          '"Z": {'+
                            'pattern: /[V|E|v|e]/'+
                          '}'+
                        '}'+
                      '});'+
                      '$("#rifrl_' + bookIndex + '").change(function () {'+
                        'var formValidation = $("#login-form").data("formValidation");'+
                        'formValidation.revalidateField("rifrl[" + bookIndex +"]");'+
                          
                        '});'+
                '<\/script>');

       
    }).on('click', '.removeButton', function() {// Remove button click handler
        //alert(contador);
        contador=contador-1;
        var $row  = $(this).parents('.book'),
            index = $row.attr('data-book-index');
        // Remove fields
        $('#login-form')
            .formValidation('removeField', $row.find('[name="rifrl[' + index + ']"]'))
            .formValidation('removeField', $row.find('[name="nombr[' + index + ']"]'))
            .formValidation('removeField', $row.find('[name="apell[' + index + ']"]'))
            .formValidation('removeField', $row.find('[name="tdocu[' + index + ']"]'))
            .formValidation('removeField', $row.find('[name="ndocu[' + index + ']"]'))
            .formValidation('removeField', $row.find('[name="cargo[' + index + ']"]'))
            .formValidation('removeField', $row.find('[name="tmovi[' + index + ']"]'))
            .formValidation('removeField', $row.find('[name="tfijo[' + index + ']"]'))
            .formValidation('removeField', $row.find('[name="celec[' + index + ']"]'));
        // Remove element containing the fields
        $row.remove();
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        var data = new FormData(jQuery('form')[0]);
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                url: 'paso3',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        /*swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){*/
                            window.open('modificar?c=<?php echo $c; ?>&s=4', '_parent');
                        /*});*/
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>
<script type="text/javascript">
    $('#cierr').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '1':
                jQuery("#ccier").removeAttr('disabled');
                break;
            default:
                jQuery("#ccier").attr('disabled','true');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#iopci').change(function () {
        var iopci = $(this).val();
        $.ajax({
            'type':'POST',
            'data':{'tmode':tmode.value},
            'url':'<?php echo CController::createUrl('funciones/SolicitudesColorOpcional'); ?>',
            'cache':false,
            'success':function(html){
                switch(iopci) {
                    case '1':
                        
                        jQuery("#opcional-1").removeClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#iclin").html(html);
                        jQuery("#iccie").html('');
                        jQuery("#icviv").html('');
                        jQuery("#ideta").removeAttr('disabled');
                        jQuery("#iclin").removeAttr('disabled');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").attr('disabled','true');

                        break;
                    case '2':
                        jQuery("#opcional-2").removeClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#iclin").html('');
                        jQuery("#iccie").html(html);
                        jQuery("#icviv").html('');
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").removeAttr('disabled');
                        jQuery("#icviv").attr('disabled','true');
                        break;
                    case '3':
                        jQuery("#opcional-3").removeClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#iclin").html('');
                        jQuery("#iccie").html('');
                        jQuery("#icviv").html(html);
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").removeAttr('disabled');
                        break;
                    default:
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#iclin").html(html);
                        jQuery("#iccie").html(html);
                        jQuery("#icviv").html(html);
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").attr('disabled','true');
                        break;
                }
                
            }
        });
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    var id = ['ccuer', 'cbols', 'cmang', 'cegor', 'cigor', 'cppre', 'ccier', 'ccurs', 'cnper', 'caesp', 'celec', 'cfras', 'cpprl', 'cvivo' ];
    var con = 1;
    id.forEach( function(valor, indice, array) {
        var numero = indice+1;
        $('#img-'+numero).popover({
          html: true,
          content: function() {
            if(numero==1){
                return $('#popover-img').html();
            }else{
                return $('#popover-img-'+numero).html();    
            }
            
          },
          trigger: 'hover'
        });
        $('#'+valor).change(function () {
            var emoji = $(this).val();
            $.ajax({  
                url:"<?php echo CController::createUrl('funciones/SolicitudesVerColor'); ?>",  
                method:"POST",  
                async:false,  
                data:{id:emoji},  
                success:function(data){  
                    if(numero=='1'){
                        $('#popover-img').html(data);
                    }else{
                        $('#popover-img-'+numero).html(data);    
                    } 
                }  
            });
            $('[data-toggle=popover]').popover('hide');
        });
    });
});
</script>
<script type="text/javascript">
    $('#ideta').change(function () {
        var ideta = $(this).val();
        switch(ideta) {
            case '1':
                jQuery("#iclin").removeAttr('disabled');
                break;
            default:
                jQuery("#iclin").attr('disabled','true');
                break;
        }
                
            
    });
</script>

<!--script type="text/javascript">
$(document).ready(function(){
    $('#img-1').popover({
          html: true,
          content: function() {
            return $('#popover-img').html();
          },
          trigger: 'hover'
        });
});
$('#color').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/SolicitudesVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-2').popover({
          html: true,
          content: function() {
            return $('#popover-img-2').html();
          },
          trigger: 'hover'
        });
});
$('#iclin').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/SolicitudesVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-2").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-3').popover({
          html: true,
          content: function() {
            return $('#popover-img-3').html();
          },
          trigger: 'hover'
        });
});
$('#iccie').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/SolicitudesVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-3").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-4').popover({
          html: true,
          content: function() {
            return $('#popover-img-4').html();
          },
          trigger: 'hover'
        });
});
$('#icviv').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/SolicitudesVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-4").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script-->