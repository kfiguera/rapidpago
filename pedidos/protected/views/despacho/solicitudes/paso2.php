<?php $conexion = Yii::app()->db; ?>
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Solicitudes</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Registro y Documentación</a></li>
            <li><a href="#">Solicitudes</a></li>
            <li class="active">Registrar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

<?php $this->renderPartial('pasos', array('ubicacion' => $ubicacion,'solicitud'=>$solicitud));?>
<form id='login-form' name='login-form' method="post">



<div class="row">   
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Despacho del Equipo</h3>
        </div>
        <div class="panel-body" >
            <div class="row hide">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Pedido *</label>
                            <?php 
                                echo CHtml::hiddenField('pedid', $p, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Código *</label>
                            <?php 
                                echo CHtml::hiddenField('codig', $c, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Paso *</label>
                            <?php 
                                echo CHtml::hiddenField('pasos', $solicitud['prere_pasos'], array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                </div>
            <input class="form-control" placeholder="Porcentaje" readonly="readonly" type="hidden" value="" name="bookindex" id="bookindex" /> 
                <!-- Plantilla -->
                    <!-- OPCIONALES-->
            <?php
                $sql="SELECT * 
                      FROM solicitud_equipos
                      WHERE solic_codig='".$c."'";
                $electivas=$conexion->createCommand($sql)->query();
                $result=$conexion->createCommand($sql)->queryAll();
                
                $row = $electivas->read();
                $i=0;
                do{
                    ?>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">        
                                <div class="form-group">
                                    <label>Operador Telefonico *</label>
                                    <?php
                                        $sql="SELECT *
                                          FROM p_operador_telefonico a
                                          ORDER BY 2";

                                        $result=$conexion->createCommand($sql)->queryAll();

                                        $data=CHtml::listData($result,'otele_codig','otele_descr');

                                        echo CHtml::dropDownList('otele['.$i.']', $row['otele_codig'], $data, array('class' => 'form-control', 'placeholder' => "Operador Telefonico",'prompt'=>'Seleccione...','id'=>'otele_'.$i.'')); ?>
                                       
                                </div>
                            </div>
                            <div class="col-xs-5">        
                                <label>Cantidad *</label>
                                <?php 
                                    echo CHtml::textField('canti['.$i.']', $row['cequi_canti'], array('class' => 'form-control canti', 'placeholder' => "Cantidad",'prompt'=>'Seleccione...','id'=>'canti_'.$i.'')); ?>
                            </div>
                        <?php
                            if($i==0){
                        ?>
                            <div class="col-xs-1">
                                <label>&nbsp;</label>
                                <br>
                                <button type="button" class="btn btn-success btn-block addButton"><i class="fa fa-plus"></i></button>
                            </div>
                        <?php
                            }else{
                        ?>
                            <div class="col-xs-1">
                                <label>&nbsp;</label>
                                <br>
                                <div class="btn-group btn-group-justified">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-success addButton"><i class="fa fa-plus"></i></button>
                                    </div>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-danger removeButton"><i class="fa fa-minus"></i></button>
                                    </div>
                                </div>
                            </div>
                
                        <?php
                            }
                            $a++;
                            $i++;
                        ?>
                        </div>
                    </div>
                    <?php
                }while(($row = $electivas->read()) !== false);
            ?>
            <!-- FIN OPCIONALES-->                   
            <div class="form-group hide" id="bookTemplate">
                <div class="row">
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Operador Telefonico *</label>
                            <?php
                                $sql="SELECT *
                                      FROM p_operador_telefonico a
                                      ORDER BY 2";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'otele_codig','otele_descr');
                                echo CHtml::dropDownList('otele_', '', $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','id'=>'otele_')); 
                            ?>   
                        </div>
                    </div>
                    <div class="col-xs-5">        
                        <label>Cantidad *</label>
                        <?php 
                            echo CHtml::textField('canti_', '', array('class' => 'form-control canti', 'placeholder' => "Texto a Bordar",'prompt'=>'Seleccione...','id'=>'canti_')); 
                        ?>
                    </div>
                    <div class="col-xs-1">
                        <label>&nbsp;</label>
                        <br>
                        <div class="btn-group btn-group-justified">
                            <div class="btn-group">
                                <button type="button" class="btn btn-success addButton"><i class="fa fa-plus"></i></button>
                            </div>
                            <div class="btn-group">
                                <button type="button" class="btn btn-danger removeButton"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    </div>
</div>

<div class="row">   
    <div class="panel panel-default">
        <div class="panel-body" >
                <!-- Button -->
                <div class="row controls">
                    <div class="col-sm-4 ">
                        <a href="modificar?c=<?php echo $c?>&s=1" type="reset" class="btn-block btn btn-default">Volver </a>
                    </div>
                    <div class="col-sm-4 ">
                        <button class="btn-block btn btn-default" onclick= "$(':input','#login-form')
                            .not(':button, :submit, :reset, :hidden')
                            .val('')
                            .removeAttr('checked')
                            .removeAttr('selected')">Limpiar  </button>
                    </div>
                    <div class="col-sm-4 ">
                        <button id="guardar" type="button" class="btn-block btn btn-info">Siguiente  </button>
                    </div>
                </div>


        </div><!-- form -->
    </div>  
</div>
</form>
<script type="text/javascript">
    $('.canti').mask('000');
</script>
<script type="text/javascript">
    var otele = {
            validators: {
                notEmpty: {
                    message: 'Estimado(a) Usuario(a) el campo "Operador Telefónico" es obligatorio',
                }
            }
        },
        canti = {
            validators: {
                notEmpty: {
                    message: 'Estimado(a) Usuario(a) el campo "Cantidad" es obligatorio',
                },
                numeric: {
                    message: 'Estimado(a) Usuario(a) el campo "Cantidad" es numérico',
                },
                stringLength: {
                    max: 20,
                    message: 'Estimado(a) Usuario(a) el campo "Cantidad" permite hasta 8 caracteres'
                }
            }
        },
        bookIndex = <?php echo $i; ?>,
        contador = 0;
        
</script>
<script>
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                
                'otele[0]': otele,
                'canti[0]': canti,
                //'texto[0]': texto,
                
            }
        });
    }).on('click', '.addButton', function() {
        //if(contador<4){ 
            contador++;
            //alert(contador);
            bookIndex++;
         
            document.getElementById('bookindex').value = bookIndex;
            var $template = $('#bookTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .attr('data-book-index', bookIndex)
                                .insertBefore($template);

            // Update the name attributes
            $clone
                .find('[name="otele_"]').attr('name', 'otele[' + bookIndex + ']').end()
                .find('[name="canti_"]').attr('name', 'canti[' + bookIndex + ']').end()
                .find('[id="otele_"]').attr('id', 'otele_' + bookIndex ).end()
                .find('[id="canti_"]').attr('id', 'canti_' + bookIndex ).end();

            // Add new fields
            // Note that we also pass the validator rules for new field as the third parameter
            $('#login-form')
                .formValidation('addField', 'otele[' + bookIndex + ']', otele)
                .formValidation('addField', 'canti[' + bookIndex + ']', canti);
        }).on('click', '.removeButton', function() {// Remove button click handler
        //alert(contador);
        contador=contador-1;
        var $row  = $(this).parents('.form-group'),
            index = $row.attr('data-book-index');
        // Remove fields
        $('#login-form')
            .formValidation('removeField', $row.find('[name="otele[' + index + ']"]'))
            .formValidation('removeField', $row.find('[name="canti[' + index + ']"]'));
            //.formValidation('removeField', $row.find('[name="texto[' + index + ']"]'));
        // Remove element containing the fields
        $row.remove();
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        var data = new FormData(jQuery('form')[0]);
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                url: 'paso2',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        /*swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){*/
                            window.open('modificar?c=<?php echo $c; ?>&s=3', '_parent');
                        /*});*/
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>
<script type="text/javascript">
    $('#cierr').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '1':
                jQuery("#ccier").removeAttr('disabled');
                break;
            default:
                jQuery("#ccier").attr('disabled','true');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#iopci').change(function () {
        var iopci = $(this).val();
        $.ajax({
            'type':'POST',
            'data':{'tmode':tmode.value},
            'url':'<?php echo CController::createUrl('funciones/SolicitudesColorOpcional'); ?>',
            'cache':false,
            'success':function(html){
                switch(iopci) {
                    case '1':
                        
                        jQuery("#opcional-1").removeClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#iclin").html(html);
                        jQuery("#iccie").html('');
                        jQuery("#icviv").html('');
                        jQuery("#ideta").removeAttr('disabled');
                        jQuery("#iclin").removeAttr('disabled');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").attr('disabled','true');

                        break;
                    case '2':
                        jQuery("#opcional-2").removeClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#iclin").html('');
                        jQuery("#iccie").html(html);
                        jQuery("#icviv").html('');
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").removeAttr('disabled');
                        jQuery("#icviv").attr('disabled','true');
                        break;
                    case '3':
                        jQuery("#opcional-3").removeClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#iclin").html('');
                        jQuery("#iccie").html('');
                        jQuery("#icviv").html(html);
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").removeAttr('disabled');
                        break;
                    default:
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#iclin").html(html);
                        jQuery("#iccie").html(html);
                        jQuery("#icviv").html(html);
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").attr('disabled','true');
                        break;
                }
                
            }
        });
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    var id = ['ccuer', 'cbols', 'cmang', 'cegor', 'cigor', 'cppre', 'ccier', 'ccurs', 'cnper', 'caesp', 'celec', 'cfras', 'cpprl', 'cvivo' ];
    var con = 1;
    id.forEach( function(valor, indice, array) {
        var numero = indice+1;
        $('#img-'+numero).popover({
          html: true,
          content: function() {
            if(numero==1){
                return $('#popover-img').html();
            }else{
                return $('#popover-img-'+numero).html();    
            }
            
          },
          trigger: 'hover'
        });
        $('#'+valor).change(function () {
            var emoji = $(this).val();
            $.ajax({  
                url:"<?php echo CController::createUrl('funciones/SolicitudesVerColor'); ?>",  
                method:"POST",  
                async:false,  
                data:{id:emoji},  
                success:function(data){  
                    if(numero=='1'){
                        $('#popover-img').html(data);
                    }else{
                        $('#popover-img-'+numero).html(data);    
                    } 
                }  
            });
            $('[data-toggle=popover]').popover('hide');
        });
    });
});
</script>


