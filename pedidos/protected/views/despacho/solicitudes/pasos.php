<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <div class="row line-steps">
                  <div class="col-md-3 column-step <?php echo $ubicacion[0]; ?>">
                    <a href="modificar?c=<?php echo $solicitud['solic_codig']?>&s=1">
                        <div class="step-number">1 </div>
                        <div class="step-title">Pre-Registro</div>
                        <div class="step-info">Datos del Comercio</div>
                    </a>
                 </div>

                 <div class="col-md-2 column-step <?php echo $ubicacion[1]; ?> ">
                     <a href="modificar?c=<?php echo $solicitud['solic_codig']?>&s=2">
                        <div class="step-number">2</div>
                        <div class="step-title">Pre-Registro</div>
                        <div class="step-info">Datos de alifiación y dispositivo</div>
                    </a>
                 </div>

                 <div class="col-md-2 column-step <?php echo $ubicacion[2]; ?> ">
                     <a href="modificar?c=<?php echo $solicitud['solic_codig']?>&s=3">
                        <div class="step-number">3</div>
                        <div class="step-title">Pre-Registro</div>
                        <div class="step-info">Datos de los Representantes Legales</div>
                    </a>
                 </div>

                 <div class="col-md-2 column-step <?php echo $ubicacion[3]; ?> ">
                     <a href="modificar?c=<?php echo $solicitud['solic_codig']?>&s=4">
                        <div class="step-number">4</div>
                        <div class="step-title">Pre-Registro</div>
                        <div class="step-info">Datos Adicionales</div>
                    </a>
                 </div>

                 <div class="col-md-3 column-step <?php echo $ubicacion[4]; ?> ">
                     <a href="modificar?c=<?php echo $solicitud['solic_codig']?>&s=5">
                        <div class="step-number">5</div>
                        <div class="step-title">Documentos</div>
                        <div class="step-info">Carga de Documentos Digitales</div>
                    </a>
                 </div>
              </div>
        </div>
    </div>
</div>