<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Despacho</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Despacho</a></li>
            <li><a href="#">Recepción</a></li>
            <li class="active">Verificar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<form id='login-form' name='login-form' method="post">
    <div class="row">                    
        <div class="panel panel-default" >
            <div class="panel-heading" >
                <h3 class="panel-title">Datos del Traslado
                <div class="panel-action">
                        <a href="#" data-perform="panel-collapse">
                            <i class="ti-plus"></i>
                        </a> 
                    </div>
                </h3>
            </div>
            <div class="panel-wrapper collapse in">
                <?php
                $conexion=yii::app()->db;
                    $sql="SELECT * FROM solicitud_traslado WHERE trasl_codig='".$c."'";
                    $traslado=$conexion->createCommand($sql)->queryRow();       
                    

                    $sql="SELECT * 
                          FROM solicitud_traslado a
                          JOIN solicitud_movimiento_traslado b ON (a.trasl_codig = b.trasl_codig)
                          JOIN inventario_movimiento c ON (b.movim_codig = c.movim_codig)
                          WHERE a.trasl_codig='".$c."' 
                            AND c.mesta_codig<>'3'";
                    $traslado=$conexion->createCommand($sql)->queryRow();

                    $sql="SELECT * 
                          FROM inventario_almacen a
                          WHERE a.almac_codig='".$traslado['almac_orige']."'";
                    $origen=$conexion->createCommand($sql)->queryRow();     
                    $sql="SELECT * 
                          FROM inventario_almacen a
                          WHERE a.almac_codig='".$traslado['almac_desti']."'";
                    $destino=$conexion->createCommand($sql)->queryRow();

                    $sql="SELECT * 
                          FROM p_inventario_movimiento_estatus a
                          WHERE a.mesta_codig='".$traslado['mesta_codig']."'";
                    $estatus=$conexion->createCommand($sql)->queryRow();
                ?>
                <div class="panel-body" >
                    <table class="table table-bordered table-hover " width="100%" style="border-collapse: collapse;" cellpadding="5">
                        <thead>
                            <tr>
                                <th width="16,66%">CÓDIGO DE MOVIMIENTO</th>
                                <th width="16,66%">TRASLADO NÚMERO</th>
                                <th width="16,66%">ALMACEN ORIGEN</th>
                                <th width="16,66%">ALMACEN DESTINO</th>
                                <th width="16,66%">ESTATUS</th>
                                <th width="16,66%">FECHA DE REGISTRO</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><?php echo $traslado['movim_numer']?></td>
                                <td><?php echo $traslado['trasl_codig']?></td>
                                <td><?php echo $origen['almac_numer'].' '.$origen['almac_descr']?></td>
                                <td><?php echo $destino['almac_numer'].' '.$destino['almac_descr']?></td>
                                <td><?php echo $estatus['mesta_descr']?></td>
                                <td><?php echo $this->funciones->transformarFecha_v($traslado['trasl_fcrea']) ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div><!-- form -->
            </div>  
        </div>
    </div>

    <div class="row">                    
        <div class="panel panel-default" >

            <div class="panel-heading" >
                <h3 class="panel-title">Articulos
                  <div class="panel-action">
                        <a href="#" data-perform="panel-collapse">
                            <i class="ti-plus"></i>
                        </a> 
                    </div>
            </h3>
                

            </div>
            <div class="panel-wrapper collapse in">

                <div class="panel-body" >
                    <div class="col-sm-12 table-responsive" id='listado-p_persona'>
                        <table class="table table-hover table-bordered" width="100%" style="border-collapse: collapse;" cellpadding="5">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="19%">SOLICITUD</th>
                                    <th width="19%">TIPO ARTICULO</th>
                                    <th width="19%">MODELO</th>
                                    <th width="19%">ARTICULO</th>
                                    <th width="19%">SERIAL</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php

                                 $sql="SELECT * 
                                        FROM inventario_movimiento_detalle a 
                                        JOIN inventario_seriales b ON (a.seria_codig = b.seria_codig)
                                        JOIN inventario c ON (b.inven_codig = c.inven_codig)
                                        JOIN p_inventario_tipo d ON (c.tinve_codig = d.tinve_codig)
                                        JOIN inventario_modelo e ON (c.model_codig = e.model_codig)
                                        JOIN solicitud_movimiento_traslado f ON (a.movim_codig = f.movim_codig)
                                        JOIN solicitud g ON (f.solic_codig = g.solic_codig)
                                        JOIN solicitud_asignacion h ON (g.solic_codig = h.solic_codig and a.seria_codig = h.asign_dispo)
                                        WHERE a.movim_codig = '".$traslado['movim_codig']."'
                                        UNION
                                        SELECT * 
                                        FROM inventario_movimiento_detalle a 
                                        JOIN inventario_seriales b ON (a.seria_codig = b.seria_codig)
                                        JOIN inventario c ON (b.inven_codig = c.inven_codig)
                                        JOIN p_inventario_tipo d ON (c.tinve_codig = d.tinve_codig)
                                        JOIN inventario_modelo e ON (c.model_codig = e.model_codig)
                                        JOIN solicitud_movimiento_traslado f ON (a.movim_codig = f.movim_codig)
                                        JOIN solicitud g ON (f.solic_codig = g.solic_codig)
                                        JOIN solicitud_asignacion h ON (g.solic_codig = h.solic_codig and a.seria_codig = h.asign_opera)
                                        WHERE a.movim_codig = '".$traslado['movim_codig']."'
                                        ORDER BY solic_numer";
                                $dispositivos = $conexion->createCommand($sql)->queryAll();
                                
                                
                                $i=0;
                                foreach ($dispositivos as $key => $value) {
                                    $i++;
                                    ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $value['solic_numer']; ?></td>
                                        <td><?php echo $value['tinve_descr']; ?></td>
                                        <td><?php echo $value['model_descr']; ?></td>
                                        <td><?php echo $value['inven_descr']; ?></td>
                                        <td><?php echo $value['seria_numer']; ?></td>
                                    </tr>
                                    <?php
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div><!-- form -->


            </div>
        </div>  
    </div>
    <div class="row">                    
        <div class="panel panel-default" >

            <div class="panel-heading" >
                <h3 class="panel-title">Verificar Traslado</h3>
            </div>
            <div class="panel-body" >
                <?php
                    $conexion=Yii::app()->db;
                ?>
                <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
                
                    <div class="row hide">
                        <div class="col-sm-12">        
                            <div class="form-group">
                                <label>Código</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                    <?php 

                                        echo CHtml::hiddenField('codigo', $c, array('class' => 'form-control', 'placeholder' => "Código")); 
                                    ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-12">        
                            <div class="form-group">
                                <label>Accion</label>
                                    <?php 
                                        $sql="SELECT *
                                              FROM p_accion a
                                              ORDER BY 2";

                                        $result=$conexion->createCommand($sql)->queryAll();

                                        $data=CHtml::listData($result,'accio_codig','accio_descr');
                                        echo CHtml::dropDownList('accio', '', $data, array('class' => 'form-control', 'prompt' => "Seleccione")); ?>

                            </div>
                        </div>
                    </div>
                    <div class="row hide" id='motivo'>
                        <div class="col-sm-12">        
                            <div class="form-group">
                                <label>Motivo</label>
                                    <?php 
                                        $sql="SELECT *
                                              FROM p_motivo a
                                              ORDER BY 2";

                                        $result=$conexion->createCommand($sql)->queryAll();

                                        $data=CHtml::listData($result,'motiv_codig','motiv_descr');
                                        echo CHtml::dropDownList('motiv', '', $data, array('class' => 'form-control', 'prompt' => "Seleccione")); ?>

                            </div>
                        </div>
                        <div class="col-sm-12">        
                            <div class="form-group">
                                <label>Observación</label>
                                    <?php echo CHtml::textArea('obser','', array('class' => 'form-control', 'placeholder' => "Observación")); ?>

                            </div>
                        </div>
                    </div>
                    <!-- Button -->
                        
            </div><!-- form -->
            <div class="panel-footer">
                <div class="row controls">
                    <div class="col-sm-4 ">
                        <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                    </div>
                    <div class="col-sm-4 ">
                        <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                    </div>
                    <div class="col-sm-4 ">
                        <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</form>
<script>
$('#punid').mask('#.##0,00',{reverse: true,maxlength:false});
$('#desde').mask('00/00/0000');
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                accio: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Acción" es obligatorio',
                        }
                    }
                },
                motiv: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Motivo" es obligatorio',
                        }
                    }
                },
                obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Observaciones" es obligatorio',
                        }
                    }
                },



            }
        });
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'verificar',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#accio").change(function () {
            var accion = $(this).val();
            if(accion=='2'){
                $("#motivo").removeClass("hide");
                $("#motiv").removeAttr("disabled");
            }else{
                $("#motivo").addClass("hide");
                $("#motiv").attr("disabled","true");
            }
            
        });
    });
</script>