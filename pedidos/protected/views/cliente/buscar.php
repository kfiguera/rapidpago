<table id='auditoria' class="table table-bordered table-hover dataTable">
    <thead>
        <tr>
            <th width="2%">
                #
            </th>
            <th>
                Nombre o Razón Social
            </th>
            <th>
                Tipo de Documento
            </th>
            <th>
                Número de Documento
            </th>
            <!--th>
                Número de Contrato
            </th-->
            <th width="5%">
                Consultar
            </th>
            <th width="5%">
                Modificar
            </th>
            <th width="5%">
                Eliminar
            </th>
        </tr>
    </thead>

    <tbody>
        <?php
        $sql = "SELECT * FROM cliente a
                JOIN p_documento_tipo b ON (a.tdocu_codig=b.tdocu_codig)  ".$condicion;
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $p_persona = $command->query();
        $i=0;
        while (($row = $p_persona->read()) !== false) {
            $i++;
        ?>
        <tr>
            <td class="tabla"><?php echo $i ?></td>
            <td class="tabla"><?php echo $row['clien_denom'] ?></td>
            <td class="tabla"><?php echo $row['tdocu_descr'] ?></td>
            <td class="tabla"><?php echo $row['clien_ndocu'] ?></td>
            <!--td class="tabla"><?php echo $row['clien_ccont'] ?></td-->
            <td class="tabla"><a href="consultar?c=<?php echo $row['clien_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-search"></i></a></td>
            <td class="tabla"><a href="modificar?c=<?php echo $row['clien_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-pencil"></i></a></td>
            <td class="tabla"><a href="eliminar?c=<?php echo $row['clien_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-close"></i></a></td>
        </tr>
        <?php
            }   
        ?>
    </tbody>
</table>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable(); 
    });
</script>
