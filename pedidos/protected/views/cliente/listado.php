<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Cliente</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Cliente</a></li>
            <li class="active">Listado</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Criterio de Busqueda</h3>
    </div>
    <div class="panel-body" >
        <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'p_personas', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            $connection = Yii::app()->db;

        ?>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Nombre o Razón Social</label>
                    <?php echo CHtml::textField('denom', '', array('class' => 'form-control', 'placeholder' => "Nombre o Razón Social")); ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Tipo de Documento</label>
                    <?php 
                        $sql="SELECT * FROM p_documento_tipo";
                        $result=$connection->createCommand($sql)->queryAll();
                        $data=CHtml::listData($result,'tdocu_codig','tdocu_descr');
                        echo CHtml::dropDownList('tdocu', '', $data,array('class' => 'form-control', 'placeholder' => "Descripción", 'prompt'=>'Seleccione...')); ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Número de Documento</label>
                    <?php echo CHtml::textField('ndocu', '', array('class' => 'form-control', 'placeholder' => "Número de Documento")); ?>
                </div>
            </div>
        </div>
        <div class="row">
            
            <div class="col-sm-6 hide">
                <div class="form-group">
                    <label>Número de Contrato</label>
                    <?php echo CHtml::textField('ccont', '', array('class' => 'form-control', 'placeholder' => "Número de Contrato")); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <button type="button" id="buscar" class="btn btn-block btn-info">Buscar</button>
            </div>
            <div class="col-sm-4">
                <button type="reset" class="btn btn-block btn-default">Limpiar</button>
            </div>
            <div class="col-sm-4">
                <a href="registrar" class="btn btn-block btn-info" >Nuevo</a>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div> 
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Listado</h3>
    </div>
    <div class="panel-body" >
        
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-p_persona'>
                <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                #
                            </th>
                            <th>
                                Nombre o Razón Social
                            </th>
                            <th>
                                Tipo de Documento
                            </th>
                            <th>
                                Número de Documento
                            </th>
                            <!--th>
                                Número de Contrato
                            </th-->
                            <th width="5%">
                                Consultar
                            </th>
                            <th width="5%">
                                Modificar
                            </th>
                            <th width="5%">
                                Eliminar
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "SELECT * FROM cliente a
                                JOIN p_documento_tipo b ON (a.tdocu_codig=b.tdocu_codig)";
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                        ?>
                        <tr>
                            <td class="tabla"><?php echo $i ?></td>
                            <td class="tabla"><?php echo $row['clien_denom'] ?></td>
                            <td class="tabla"><?php echo $row['tdocu_descr'] ?></td>
                            <td class="tabla"><?php echo $row['clien_ndocu'] ?></td>
                            <!--td class="tabla"><?php echo $row['clien_ccont'] ?></td-->
                            <td class="tabla"><a href="consultar?c=<?php echo $row['clien_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-search"></i></a></td>
                            <td class="tabla"><a href="modificar?c=<?php echo $row['clien_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-pencil"></i></a></td>
                            <td class="tabla"><a href="eliminar?c=<?php echo $row['clien_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-close"></i></a></td>
                        </tr>
                        <?php
                            }   
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>  
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#buscar').click(function () {
        var formData = new FormData($("#p_personas")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-p_persona').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>
