<?php $conexion = Yii::app()->db; ?>
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Solicitudes</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Registro y Documentación</a></li>
            <li><a href="#">Solicitudes</a></li>
            <li class="active">Verificar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Datos de la Solicitud</h3>
        </div>
        <div class="panel-body">
            <?php 
                $this->funciones->imprimirDatosSolicitud($solicitud);
            ?>
        </div>
    </div>  
</div>
<form id='login-form' name='login-form' method="post">

<div class="row">   
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Numero de Terminal</h3>
        </div>
        <div class="panel-body" >
            <input class="form-control" placeholder="Porcentaje" readonly="readonly" type="hidden" value="" name="bookindex" id="bookindex" /> 
                <!-- Plantilla -->
                <div class="row hide">
                <div class="col-sm-4">        
                    <div class="form-group">
                        <label>Pedido *</label>
                        <?php 
                            echo CHtml::hiddenField('pedid', $p, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                    </div>
                </div>
                <div class="col-sm-4">        
                    <div class="form-group">
                        <label>Código *</label>
                        <?php 
                            echo CHtml::hiddenField('codig', $c, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                    </div>
                </div>
                <div class="col-sm-4">        
                    <div class="form-group">
                        <label>Paso *</label>
                        <?php 
                            echo CHtml::hiddenField('pasos', $solicitud['prere_pasos'], array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">        
                    <div class="form-group">
                        <label>Código de Afiliado</label>
                        <?php 
                            echo CHtml::textField('cafil', $solicitud['solic_cafil'], array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                    </div>
                </div>
                <div class="col-sm-6">        
                    <div class="form-group">
                        <label>Número de Cuenta</label>
                        <?php 
                            echo CHtml::textField('ncuen', $solicitud['solic_ncuen'], array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                    </div>
                </div>
            </div>      
                <?php
                $sql="SELECT * 
                      FROM solicitud_terminal
                      WHERE solic_codig='".$c."'";
                $electivas=$conexion->createCommand($sql)->query();
                $terminal=$conexion->createCommand($sql)->queryAll();
                
                $row = $electivas->read();
                $i=0;
                do{
                    ?>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-11">
                                    <label>Número de Terminal *</label>
                                    <?php 
                                        
                                        echo CHtml::textField('numer['.$i.']', $row['termi_numer'], array('class' => 'form-control', 'placeholder' => "Número de Terminal",'prompt'=>'Seleccione...','id'=>'numer_'.$i.'')); ?>
                           </div> 
                       <?php
                            if($i==0){
                        ?>
                            <div class="col-xs-1">
                                <label>&nbsp;</label>
                                <br>
                                <button type="button" class="btn btn-success btn-block addButton"><i class="fa fa-plus"></i></button>
                            </div>
                        <?php
                            }else{
                        ?>
                            <div class="col-xs-1">
                                <label>&nbsp;</label>
                                <br>
                                <div class="btn-group btn-group-justified">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-success addButton"><i class="fa fa-plus"></i></button>
                                    </div>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-danger removeButton"><i class="fa fa-minus"></i></button>
                                    </div>
                                </div>
                            </div>
                        <?php
                            }
                            $a++;
                            $i++;
                        ?>
                        </div>                                  
                    </div>
                    <?php
                        }while(($row = $electivas->read()) !== false);
                    ?>    
                        
                <div class="form-group hide" id="bookTemplate">
                    <div class="row">
                        <div class="col-xs-11">
                            <label>Número de Terminal *</label>
                            <?php 
                                
                                echo CHtml::textField('numer_', '', array('class' => 'form-control', 'placeholder' => "Número de Terminal",'prompt'=>'Seleccione...','id'=>'numer_')); ?>
                        </div>
                        <div class="col-xs-1">
                            <label>&nbsp;</label>
                            <br>
                            <div class="btn-group btn-group-justified">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-success addButton"><i class="fa fa-plus"></i></button>
                                </div>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-danger removeButton"><i class="fa fa-minus"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    

                 <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Accion</label>
                                <?php echo CHtml::dropDownList('accio', '',array('1'=>'Aprobar', '2' =>'Rechazar'), array('class' => 'form-control', 'prompt' => "Seleccione")); ?>

                        </div>
                    </div>
                </div>
                <div class="row hide" id='motivo'>
                    <div class="col-sm-12 hide">        
                        <div class="form-group">
                            <label>Motivo</label>
                                <?php echo CHtml::dropDownList('motiv', '',array('1'=>'Motivo 1', '2' =>'Motivo 2'), array('class' => 'form-control', 'prompt' => "Seleccione")); ?>

                        </div>
                    </div>
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observación</label>
                                <?php echo CHtml::textArea('obser','', array('class' => 'form-control', 'placeholder' => "Observación")); ?>

                        </div>
                    </div>
                </div>
                        
        </div>
        <div class="panel-footer">
            <div class="row controls">
                <div class="col-sm-4 ">
                    <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                </div>
                <div class="col-sm-4 ">
                    <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                </div>
                <div class="col-sm-4 ">
                    <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
<script type="text/javascript">
    var numer = {
            validators: {
                notEmpty: {
                    message: 'Estimado(a) Usuario(a) el campo "Número de Terminal" es obligatorio',
                },
                numeric:{
                    message: 'Estimado(a) Usuario(a) el campo "Número de Terminal" debe ser numerico',  
                }
            }
        },
        bookIndex = <?php echo $a; ?>,
        contador = 0;
        
</script>

<script>
    $(document).ready(function () {
        $('#numer_0').mask('0000');
        $('#ncuen').mask("00000000000000000000");   
        $('#cafil').mask("00000000");
    });
</script>
<script>
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                accio: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Acción" es obligatorio',
                        }
                    }
                },
                motiv: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Motivo" es obligatorio',
                        }
                    }
                },
                obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Observaciones" es obligatorio',
                        }
                    }
                },cafil:{
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Código Afiliado" es obligatorio',
                        },
                        numeric: {
                            message: 'Estimado(a) Usuario(a) el campo "Código Afiliado" es numérico',
                        },
                        stringLength: {
                            min: 8,
                            max: 8,
                            message: 'Estimado(a) Usuario(a) el campo "Código Afiliado" debe poseer 8 caracteres'
                        }
                    }
                },
                ncuen:{
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Número de Cuenta" es obligatorio',
                        },numeric: {
                            message: 'Estimado(a) Usuario(a) el campo "Número de Cuenta" es numérico',
                        },
                        stringLength: {
                            min: 20,
                            max: 20,
                            message: 'Estimado(a) Usuario(a) el campo "Número de Cuenta" debe poseer 20 caracteres'
                        }
                    }
                },
                'numer[0]': numer,
                <?php
                $i=1;
                foreach ($terminal as $key => $value) {
                    echo "'numer[".$i."]': numer,".PHP_EOL;
                    $i++;
                }
                ?>
                //'image[0]': image,
                //'texto[0]': texto,
                
            }
        });
    }).on('click', '.addButton', function() {
        //if(contador<4){ 
            contador++;
            //alert(contador);
            bookIndex++;
         
            document.getElementById('bookindex').value = bookIndex;
            var $template = $('#bookTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .attr('data-book-index', bookIndex)
                                .insertBefore($template);

            // Update the name attributes
            $clone
                .find('[name="numer_"]').attr('name', 'numer[' + bookIndex + ']').end()
                
                .find('[id="numer_"]').attr('id', 'numer_' + bookIndex ).end();

            // Add new fields
            // Note that we also pass the validator rules for new field as the third parameter
            $('#login-form')
                .formValidation('addField', 'numer[' + bookIndex + ']', numer);
            
            $('#login-form').append(
                '<script type="text/javascript">\n'+
                    '$(document).ready(function () {\n'+
                        '$("#numer_' + bookIndex + '").mask("0000");\n'+
                    '});\n'+
                '<\/script>');
           

       
    }).on('click', '.removeButton', function() {// Remove button click handler
        //alert(contador);
        contador=contador-1;
        var $row  = $(this).parents('.form-group'),
            index = $row.attr('data-book-index');
        // Remove fields
        //$('#login-form')
            //.formValidation('removeField', $row.find('[name="descr[' + index + ']"]'));
            //.formValidation('removeField', $row.find('[name="image[' + index + ']"]'));
            //.formValidation('removeField', $row.find('[name="texto[' + index + ']"]'));
        // Remove element containing the fields
        $row.remove();
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        var data = new FormData(jQuery('form')[0]);
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                url: 'terminal',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>
<script type="text/javascript">
    $('#cierr').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '1':
                jQuery("#ccier").removeAttr('disabled');
                break;
            default:
                jQuery("#ccier").attr('disabled','true');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#iopci').change(function () {
        var iopci = $(this).val();
        $.ajax({
            'type':'POST',
            'data':{'tmode':tmode.value},
            'url':'<?php echo CController::createUrl('funciones/SolicitudesColorOpcional'); ?>',
            'cache':false,
            'success':function(html){
                switch(iopci) {
                    case '1':
                        
                        jQuery("#opcional-1").removeClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#iclin").html(html);
                        jQuery("#iccie").html('');
                        jQuery("#icviv").html('');
                        jQuery("#ideta").removeAttr('disabled');
                        jQuery("#iclin").removeAttr('disabled');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").attr('disabled','true');

                        break;
                    case '2':
                        jQuery("#opcional-2").removeClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#iclin").html('');
                        jQuery("#iccie").html(html);
                        jQuery("#icviv").html('');
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").removeAttr('disabled');
                        jQuery("#icviv").attr('disabled','true');
                        break;
                    case '3':
                        jQuery("#opcional-3").removeClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#iclin").html('');
                        jQuery("#iccie").html('');
                        jQuery("#icviv").html(html);
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").removeAttr('disabled');
                        break;
                    default:
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#iclin").html(html);
                        jQuery("#iccie").html(html);
                        jQuery("#icviv").html(html);
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").attr('disabled','true');
                        break;
                }
                
            }
        });
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    var id = ['ccuer', 'cbols', 'cmang', 'cegor', 'cigor', 'cppre', 'ccier', 'ccurs', 'cnper', 'caesp', 'celec', 'cfras', 'cpprl', 'cvivo' ];
    var con = 1;
    id.forEach( function(valor, indice, array) {
        var numero = indice+1;
        $('#img-'+numero).popover({
          html: true,
          content: function() {
            if(numero==1){
                return $('#popover-img').html();
            }else{
                return $('#popover-img-'+numero).html();    
            }
            
          },
          trigger: 'hover'
        });
        $('#'+valor).change(function () {
            var emoji = $(this).val();
            $.ajax({  
                url:"<?php echo CController::createUrl('funciones/SolicitudesVerColor'); ?>",  
                method:"POST",  
                async:false,  
                data:{id:emoji},  
                success:function(data){  
                    if(numero=='1'){
                        $('#popover-img').html(data);
                    }else{
                        $('#popover-img-'+numero).html(data);    
                    } 
                }  
            });
            $('[data-toggle=popover]').popover('hide');
        });
    });
});
</script>
<script type="text/javascript">
    $('#ideta').change(function () {
        var ideta = $(this).val();
        switch(ideta) {
            case '1':
                jQuery("#iclin").removeAttr('disabled');
                break;
            default:
                jQuery("#iclin").attr('disabled','true');
                break;
        }
                
            
    });
</script>

<!--script type="text/javascript">
$(document).ready(function(){
    $('#img-1').popover({
          html: true,
          content: function() {
            return $('#popover-img').html();
          },
          trigger: 'hover'
        });
});
$('#color').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/SolicitudesVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-2').popover({
          html: true,
          content: function() {
            return $('#popover-img-2').html();
          },
          trigger: 'hover'
        });
});
$('#iclin').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/SolicitudesVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-2").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-3').popover({
          html: true,
          content: function() {
            return $('#popover-img-3').html();
          },
          trigger: 'hover'
        });
});
$('#iccie').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/SolicitudesVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-3").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-4').popover({
          html: true,
          content: function() {
            return $('#popover-img-4').html();
          },
          trigger: 'hover'
        });
});
$('#icviv').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/SolicitudesVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-4").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script-->

<script type="text/javascript">
    $(document).ready(function () {
        $("#accio").change(function () {
            var accion = $(this).val();
            if(accion=='2'){
                $("#motivo").removeClass("hide");
                //$("#motiv").removeAttr("disabled");
            }else{
                $("#motivo").addClass("hide");
                $("#motiv").attr("disabled","true");
            }
            
        });
    });
</script>