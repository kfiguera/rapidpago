<?php $conexion = Yii::app()->db; ?>
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Solicitudes</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Registro y Documentación</a></li>
            <li><a href="#">Solicitudes</a></li>
            <li class="active">Registrar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <div class="row line-steps">
                  <div class="col-md-3 column-step <?php echo $ubicacion[0]; ?>">
                    <a href="modificar?c=<?php echo $solicitud['solic_codig']?>&s=1">
                        <div class="step-number">1 </div>
                        <div class="step-title">Pre-Registro</div>
                        <div class="step-info">Datos del Comercio</div>
                    </a>
                 </div>

                 <div class="col-md-2 column-step <?php echo $ubicacion[1]; ?> ">
                     <a href="modificar?c=<?php echo $solicitud['solic_codig']?>&s=2">
                        <div class="step-number">2</div>
                        <div class="step-title">Pre-Registro</div>
                        <div class="step-info">Datos de alifiación y dispositivo</div>
                    </a>
                 </div>

                 <div class="col-md-2 column-step <?php echo $ubicacion[2]; ?> ">
                     <a href="modificar?c=<?php echo $solicitud['solic_codig']?>&s=3">
                        <div class="step-number">3</div>
                        <div class="step-title">Pre-Registro</div>
                        <div class="step-info">Datos de los Representantes Legales</div>
                    </a>
                 </div>

                 <div class="col-md-2 column-step <?php echo $ubicacion[3]; ?> ">
                     <a href="modificar?c=<?php echo $solicitud['solic_codig']?>&s=4">
                        <div class="step-number">4</div>
                        <div class="step-title">Pre-Registro</div>
                        <div class="step-info">Datos Adicionales</div>
                    </a>
                 </div>

                 <div class="col-md-3 column-step <?php echo $ubicacion[4]; ?> ">
                     <a href="modificar?c=<?php echo $solicitud['solic_codig']?>&s=5">
                        <div class="step-number">5</div>
                        <div class="step-title">Documentos</div>
                        <div class="step-info">Carga de Documentos Digitales</div>
                    </a>
              </div>
        </div>
    </div>
</div>
<form id='login-form' name='login-form' method="post">

<div class="row">   
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Documentos Digitales</h3>
        </div>
        <div class="panel-body" >
            <input class="form-control" placeholder="Porcentaje" readonly="readonly" type="hidden" value="" name="bookindex" id="bookindex" /> 
                <!-- Plantilla -->
                <div class="row hide">
                <div class="col-sm-4">        
                    <div class="form-group">
                        <label>Pedido *</label>
                        <?php 
                            echo CHtml::hiddenField('pedid', $p, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                    </div>
                </div>
                <div class="col-sm-4">        
                    <div class="form-group">
                        <label>Código *</label>
                        <?php 
                            echo CHtml::hiddenField('codig', $c, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                    </div>
                </div>
                <div class="col-sm-4">        
                    <div class="form-group">
                        <label>Paso *</label>
                        <?php 
                            echo CHtml::hiddenField('pasos', $solicitud['prere_pasos'], array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                    </div>
                </div>
            </div>
                <?php
                $sql="SELECT * 
                      FROM solicitud_documento
                      WHERE solic_codig='".$c."'";
                $electivas=$conexion->createCommand($sql)->query();
                $result=$conexion->createCommand($sql)->queryAll();
                
                $row = $electivas->read();
                $i=0;
                do{
                    ?>
                    <div class="form-group">
                        <div class="row">
                           <div class="col-xs-4">
                                    <label>Tipo de Documento *</label>
                                    <?php 
                                        $sql="SELECT * FROM rd_preregistro_documento_digital_tipo";
                                        $dtipo=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($dtipo,'dtipo_codig','dtipo_descr');
                                        echo CHtml::dropDownList('dtipo['.$i.']', $row['dtipo_codig'], $data, array('class' => 'form-control', 'placeholder' => "Texto a Bordar",'prompt'=>'Seleccione...','id'=>'dtipo_'.$i.'')); ?>
                           </div> 
                           <div class="col-xs-7">
                                <div class="numero1_<?php echo $i; ?> form-group">
                                    <label>Documento</label>
                                    <div  class="input-group image-preview" data-placement="top" >  

                                        <img id="dynamic">
                                        <!-- image-preview-filename input [CUT FROM HERE]-->
                                        <?php 
                                            $ruta=$row['docum_ruta'];
                                            
                                            $nombre=explode('/', $ruta);
                                            $nombre=end($nombre);
                                            if($nombre){
                                                $opcional['1']['hide']='';
                                                $opcional['1']['show']='true';
                                                $opcional['1']['display']='';
                                            }else{
                                                $opcional['1']['hide']='hide';
                                                $opcional['1']['display']='style="display:none;"';

                                                $opcional['1']['show']='false';
                                            }
                                        ?>
                                        <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="true" value="<?php echo $nombre ?>" > <!-- don't give a name === doesn't send on POST/GET -->
                                        <span class="input-group-btn">
                                            <!-- image-preview-clear button -->
                                            <button type="button" class="btn btn-default image-preview-clear " <?php echo $opcional['1']['display'] ?>>
                                                <span class="fa fa-times"></span> Limpiar
                                            </button>
                                            <!-- image-preview-input -->
                                            <div class="btn btn-default image-preview-input">
                                                <span class="fa fa-folder-open"></span>
                                                <span class="numero1_<?php echo $i; ?> image-preview-input-title">Buscar</span>
                                                <input type="file" id="image_<?php echo $i; ?>" name="image[<?php echo $i; ?>]" /> <!-- rename it -->
                                            </div>
                                        </span>
                                   
                                    </div> 
                                </div> 
                                
                                     
                                <script>

                                $(function () {
                                    // Clear event
                                    $('.numero1_<?php echo $i; ?> .image-preview-clear').click(function () {
                                        $('.numero1_<?php echo $i; ?> .image-preview').attr("data-content", "").popover('hide');
                                        $('.numero1_<?php echo $i; ?> .image-preview-filename').val("");
                                        $('.numero1_<?php echo $i; ?> .image-preview-clear').hide();
                                        $('.numero1_<?php echo $i; ?> .image-preview-input input:file').val("");
                                        $(".numero1_<?php echo $i; ?> .image-preview-input-title").text("Buscar");
                                    });
                                    // Create the preview image
                                    $(".numero1_<?php echo $i; ?> .image-preview-input input:file").change(function () {
                                        var img = $('<img/>', {
                                            id: 'dynamic',
                                            width: 250,
                                            height: 200
                                        });
                                        var file = this.files[0];
                                        var reader = new FileReader();
                                        // Set preview image into the popover data-content
                                        reader.onload = function (e) {
                                            $(".numero1_<?php echo $i; ?> .image-preview-input-title").text("Cambiar");
                                            $(".numero1_<?php echo $i; ?> .image-preview-clear").show();
                                            $(".numero1_<?php echo $i; ?> .image-preview-filename").val(file.name);
                                            img.attr('src', e.target.result);
                                            //$(".numero1_<?php echo $a; ?> .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
                                            }
                                        reader.readAsDataURL(file);
                                    });
                                });
                                </script>

                            </div>
                       <?php
                            if($i==0){
                        ?>
                            <div class="col-xs-1">
                                <label>&nbsp;</label>
                                <br>
                                <button type="button" class="btn btn-success btn-block addButton"><i class="fa fa-plus"></i></button>
                            </div>
                        <?php
                            }else{
                        ?>
                            <div class="col-xs-1">
                                <label>&nbsp;</label>
                                <br>
                                <div class="btn-group btn-group-justified">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-success addButton"><i class="fa fa-plus"></i></button>
                                    </div>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-danger removeButton"><i class="fa fa-minus"></i></button>
                                    </div>
                                </div>
                            </div>
                        <?php
                            }
                            $a++;
                            $i++;
                        ?>
                        </div>                                  
                    </div>
                    <?php
                        }while(($row = $electivas->read()) !== false);
                    ?>    
                        
                <div class="form-group hide" id="bookTemplate">
                    <div class="row">
                        <div class="col-xs-4">
                            <label>Tipo de Documento *</label>
                            <?php 
                                $sql="SELECT * FROM rd_preregistro_documento_digital_tipo";
                                $dtipo=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($dtipo,'dtipo_codig','dtipo_descr');
                                echo CHtml::dropDownList('dtipo_', $row['pimag_tbord'], $data, array('class' => 'form-control', 'placeholder' => "Texto a Bordar",'prompt'=>'Seleccione...','id'=>'dtipo_')); 
                            ?>
                       </div> 
                        
                        <div class="col-xs-7">        
                            <div class="numero1_">
                                <label>Documento</label>
                                <div  class="input-group image-preview">  
                                    <img id="dynamic">
                                    <!-- image-preview-filename input [CUT FROM HERE]-->
                                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-btn">
                                        <!-- image-preview-clear button -->
                                        <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                            <span class="fa fa-times"></span> Limpiar
                                        </button>
                                        <!-- image-preview-input -->
                                        <div class="btn btn-default image-preview-input">
                                            <span class="fa fa-folder-open"></span>
                                            <span class="numero1_ image-preview-input-title">Buscar</span>
                                            <input type="file" id="image_" name="image_"/> <!-- rename it -->
                                        </div>
                                    </span>
                               
                                </div> 
                            </div> 

                        </div>
                        <div class="col-xs-1">
                            <label>&nbsp;</label>
                            <br>
                            <div class="btn-group btn-group-justified">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-success addButton"><i class="fa fa-plus"></i></button>
                                </div>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-danger removeButton"><i class="fa fa-minus"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    


                        
        </div>
    </div>
</div>
<div class="row">   
    <div class="panel panel-default">
        <div class="panel-body" >
                <!-- Button -->
                <div class="row controls">
                    <div class="col-sm-4 ">
                        <a href="modificar?c=<?php echo $c?>&s=4" type="reset" class="btn-block btn btn-default">Volver </a>
                    </div>
                    <div class="col-sm-4 ">
                        <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                    </div>
                    <div class="col-sm-4 ">
                        <button id="guardar" type="button" class="btn-block btn btn-info">Siguiente  </button>
                    </div>
                </div>


        </div><!-- form -->
    </div>  
</div>
</form>
<script type="text/javascript">
    var dtipo = {
            validators: {
                notEmpty: {
                    message: 'Estimado(a) Usuario(a) el campo "Tipo de Documento *" es obligatorio',
                }
            }
        },
        image = {
            validators: {
                notEmpty: {
                    message: 'Estimado(a) Usuario(a) el campo "Documento" es obligatorio',
                }
            }
        },
        texto = {
            validators: {
                notEmpty: {
                    message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar" es obligatorio',
                }
            }
        },
        bookIndex = <?php echo $a; ?>,
        contador = 0;
        
</script>
<script>
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                
                'dtipo[0]': dtipo,
                //'image[0]': image,
                //'texto[0]': texto,
                
            }
        });
    }).on('click', '.addButton', function() {
        //if(contador<4){ 
            contador++;
            //alert(contador);
            bookIndex++;
         
            document.getElementById('bookindex').value = bookIndex;
            var $template = $('#bookTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .attr('data-book-index', bookIndex)
                                .insertBefore($template);

            // Update the name attributes
            $clone
                .find('[name="dtipo_"]').attr('name', 'dtipo[' + bookIndex + ']').end()
                .find('[name="image_"]').attr('name', 'image[' + bookIndex + ']').end()
                .find('[name="texto_"]').attr('name', 'texto[' + bookIndex + ']').end()
                
                .find('[id="dtipo_"]').attr('id', 'dtipo_' + bookIndex ).end()
                .find('[id="image_"]').attr('id', 'image_' + bookIndex ).end()
                .find('[id="texto_"]').attr('id', 'texto_' + bookIndex ).end()
                .find('[class="numero1_"]').attr('class', 'numero1_' + bookIndex ).end();

            // Add new fields
            // Note that we also pass the validator rules for new field as the third parameter
            $('#login-form')
                .formValidation('addField', 'dtipo[' + bookIndex + ']', dtipo)
                .formValidation('addField', 'image[' + bookIndex + ']', image);
            

            //agregar ajax
            

            $('#login-form').append(
                '<script type="text/javascript">\n'+
                    '$(document).on("click", "#close-preview", function () {\n'+
                        '$(".numero1_'+bookIndex+' .image-preview").popover("hide");\n'+
                     
                        '$(".numero1_'+bookIndex+' .image-preview").hover(\n'+
                            'function () {\n'+
                                '$(".image-preview").popover("hide");\n'+
                            '},\n'+
                            'function () {\n'+
                                '$(".image-preview").popover("hide");\n'+
                            '}\n'+
                        ');\n'+
                    '});\n'+
                    '$(function () {\n'+
                       
                        'var closebtn = $("<button/>", {\n'+
                            'type: "button",\n'+
                            'text: "x",\n'+
                            'id: "close-preview",\n'+
                            'style: "font-size: initial;",\n'+
                        '});\n'+
                        'closebtn.attr("class","close pull-right");\n'+
                        '$(".numero1_'+bookIndex+' .image-preview").popover({\n'+
                            'trigger:"manual",\n'+
                            'html:true,\n'+
                            'title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,\n'+
                            'content: "No hay imagen",\n'+
                            'placement:"top"\n'+
                        '});\n'+
                       
                        '$(".numero1_'+bookIndex+' .image-preview-clear").click(function () {\n'+
                            '$(".numero1_'+bookIndex+' .image-preview").attr("data-content", "").popover("hide");\n'+
                            '$(".numero1_'+bookIndex+' .image-preview-filename").val("");\n'+
                            '$(".numero1_'+bookIndex+' .image-preview-clear").hide();\n'+
                            '$(".numero1_'+bookIndex+' .image-preview-input input:file").val("");\n'+
                            '$(".numero1_'+bookIndex+' .image-preview-input-title").text("Buscar");\n'+
                        '});\n'+
                       
                        '$(".numero1_'+bookIndex+' .image-preview-input input:file").change(function () {\n'+
                            'var img = $("<img/>", {\n'+
                                'id: "dynamic",\n'+
                                'width: 250,\n'+
                                'height: 200\n'+
                            '});\n'+
                            'var file = this.files[0];\n'+
                            'var reader = new FileReader();\n'+
                          
                            'reader.onload = function (e) {\n'+
                                '$(".numero1_'+bookIndex+' .image-preview-input-title").text("Cambiar");\n'+
                                '$(".numero1_'+bookIndex+' .image-preview-clear").show();\n'+
                                '$(".numero1_'+bookIndex+' .image-preview-filename").val(file.name);\n'+
                                'img.attr("src", e.target.result);\n'+
                                //'$(".numero1_'+bookIndex+' .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");\n'+
                                '}\n'+
                            'reader.readAsDataURL(file);\n'+
                        '});\n'+
                    '});\n'+
                '<\/script>');
            
           

       
    }).on('click', '.removeButton', function() {// Remove button click handler
        //alert(contador);
        contador=contador-1;
        var $row  = $(this).parents('.form-group'),
            index = $row.attr('data-book-index');
        // Remove fields
        //$('#login-form')
            //.formValidation('removeField', $row.find('[name="descr[' + index + ']"]'));
            //.formValidation('removeField', $row.find('[name="image[' + index + ']"]'));
            //.formValidation('removeField', $row.find('[name="texto[' + index + ']"]'));
        // Remove element containing the fields
        $row.remove();
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        var data = new FormData(jQuery('form')[0]);
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                url: 'paso5',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>
<script type="text/javascript">
    $('#cierr').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '1':
                jQuery("#ccier").removeAttr('disabled');
                break;
            default:
                jQuery("#ccier").attr('disabled','true');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#iopci').change(function () {
        var iopci = $(this).val();
        $.ajax({
            'type':'POST',
            'data':{'tmode':tmode.value},
            'url':'<?php echo CController::createUrl('funciones/SolicitudesColorOpcional'); ?>',
            'cache':false,
            'success':function(html){
                switch(iopci) {
                    case '1':
                        
                        jQuery("#opcional-1").removeClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#iclin").html(html);
                        jQuery("#iccie").html('');
                        jQuery("#icviv").html('');
                        jQuery("#ideta").removeAttr('disabled');
                        jQuery("#iclin").removeAttr('disabled');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").attr('disabled','true');

                        break;
                    case '2':
                        jQuery("#opcional-2").removeClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#iclin").html('');
                        jQuery("#iccie").html(html);
                        jQuery("#icviv").html('');
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").removeAttr('disabled');
                        jQuery("#icviv").attr('disabled','true');
                        break;
                    case '3':
                        jQuery("#opcional-3").removeClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#iclin").html('');
                        jQuery("#iccie").html('');
                        jQuery("#icviv").html(html);
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").removeAttr('disabled');
                        break;
                    default:
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#iclin").html(html);
                        jQuery("#iccie").html(html);
                        jQuery("#icviv").html(html);
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").attr('disabled','true');
                        break;
                }
                
            }
        });
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    var id = ['ccuer', 'cbols', 'cmang', 'cegor', 'cigor', 'cppre', 'ccier', 'ccurs', 'cnper', 'caesp', 'celec', 'cfras', 'cpprl', 'cvivo' ];
    var con = 1;
    id.forEach( function(valor, indice, array) {
        var numero = indice+1;
        $('#img-'+numero).popover({
          html: true,
          content: function() {
            if(numero==1){
                return $('#popover-img').html();
            }else{
                return $('#popover-img-'+numero).html();    
            }
            
          },
          trigger: 'hover'
        });
        $('#'+valor).change(function () {
            var emoji = $(this).val();
            $.ajax({  
                url:"<?php echo CController::createUrl('funciones/SolicitudesVerColor'); ?>",  
                method:"POST",  
                async:false,  
                data:{id:emoji},  
                success:function(data){  
                    if(numero=='1'){
                        $('#popover-img').html(data);
                    }else{
                        $('#popover-img-'+numero).html(data);    
                    } 
                }  
            });
            $('[data-toggle=popover]').popover('hide');
        });
    });
});
</script>
<script type="text/javascript">
    $('#ideta').change(function () {
        var ideta = $(this).val();
        switch(ideta) {
            case '1':
                jQuery("#iclin").removeAttr('disabled');
                break;
            default:
                jQuery("#iclin").attr('disabled','true');
                break;
        }
                
            
    });
</script>

<!--script type="text/javascript">
$(document).ready(function(){
    $('#img-1').popover({
          html: true,
          content: function() {
            return $('#popover-img').html();
          },
          trigger: 'hover'
        });
});
$('#color').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/SolicitudesVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-2').popover({
          html: true,
          content: function() {
            return $('#popover-img-2').html();
          },
          trigger: 'hover'
        });
});
$('#iclin').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/SolicitudesVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-2").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-3').popover({
          html: true,
          content: function() {
            return $('#popover-img-3').html();
          },
          trigger: 'hover'
        });
});
$('#iccie').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/SolicitudesVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-3").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-4').popover({
          html: true,
          content: function() {
            return $('#popover-img-4').html();
          },
          trigger: 'hover'
        });
});
$('#icviv').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/SolicitudesVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-4").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script-->