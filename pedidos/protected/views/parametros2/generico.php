<?php
//Mostrar Errores del controlador
foreach (Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="alert alert-' . $key . '">' . $message . "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> </div>\n";
}
?> 
<div id="loginbox" style="margin-top:50px;" class="mainbox col-md-12">           
    <style>
        .image-preview-input {
            position: relative;
            overflow: hidden;
            margin: 0px;    
            color: #333;
            background-color: #fff;
            border-color: #ccc;    
        }
        .image-preview-input input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            padding: 0;
            font-size: 20px;
            cursor: pointer;
            opacity: 0;
            filter: alpha(opacity=0);
        }
        .image-preview-input-title {
            margin-left:2px;
        }
    </style>  
    <h1 class="titulo"><?php echo $param['titulo'] ?></h1>
    <div class="panel panel-danger" >


        <div style="padding-top:30px" class="panel-body" >

            <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>


            <?php
            
            $form = $this->beginWidget('CActiveForm', array('id' => 'subir-p_personas', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            ?>
                <div class="row hide">
                    <div class="col-sm-6">
                        <div class="form-group">  
                        <!-- image-preview-filename input [CUT FROM HERE]-->

                        <label for="clvarea">Código</label>
                        <?php
                        
                          echo CHtml::hiddenField('codigo', $param['codigo'], array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                        ?>

                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">  
                        <!-- image-preview-filename input [CUT FROM HERE]-->

                        <label for="clvarea">Tipo</label>
                        <?php
                        
                          echo CHtml::hiddenField('tipo', $param['tipo'], array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                        ?>

                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">  
                        <!-- image-preview-filename input [CUT FROM HERE]-->

                        <label for="clvarea">Operacion</label>
                        <?php
                        
                          echo CHtml::hiddenField('operacion', $param['operacion'], array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                        ?>

                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">  
                        <!-- image-preview-filename input [CUT FROM HERE]-->

                        <label for="clvarea">Descripción</label>
                        <?php
                        
                          echo CHtml::textField('descripcion', $param['descripcion'], array('prompt' => 'Seleccione...', "class" => "form-control ","style" => "text-transform: uppercase;")) 
                        ?>

                        </div>
                    </div>
                    
                </div>
                

            
            <div class="row">
                <!-- Button -->
                <div class="controls">
                    <div class="col-sm-6">
                        <button id="subir" name="subir" value="subir" type="submit" class="btn-block btn btn-uven"><span class="fa fa-save"></span> Guardar  </button>

                    </div>
                    <div class="col-sm-6">
                        <button id="subir" name="limpiar" value="limpiar" type="reset" class="btn-block btn btn-default"><span class="fa fa-eraser"></span> Limpiar  </button>

                    </div>
                </div>
            </div>              

            <?php $this->endWidget(); ?>

        </div><!-- form -->
    </div>  
</div>
<script type="text/javascript">
$(function() {
    $('#descripcion').keyup(function() {
        this.value = this.value.toUpperCase();
    });
});
</script>
<script type="text/javascript">
    $('#telefono').mask('(9999)999.99.99');
    

    $(document).ready(function () {

        $('#subir-p_personas').bootstrapValidator({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                descripcion: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        regexp: {
                            regexp: /^[a-zA-ZñÑ\s]*$/,
                            message: 'Estimado(a) Usuario(a) el campo "País" Solo puede poseer letras.'
                        }

                    }
                },
            }
        });
    });
    $('#subir').click(function () {
        var formData = new FormData($("#subir-p_personas")[0]);
        $('#subir-p_personas').bootstrapValidator('validate'); //secondary validation using Bootstrap Validator      
        var bootstrapValidator = $('#subir-p_personas').data('bootstrapValidator');
        if (bootstrapValidator.isValid()) {
            $.ajax({
                'dataType': "json",
                'type': 'post',
                'data': formData,
                'cache': false,
                'contentType': false,
                'processData': false,
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    //alert(response['success']);
                    $("#body").removeClass("loading");
                    if (response['success'] == 'true') {
                        $("#body").removeClass("loading");
                        bootbox.dialog({
                            message: response['msg'],
                            title: "Exito!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                    callback: function(){
                                       window.open('lista?t=<?php echo $param['tipo'] ?>', '_parent');
                                    }
                                },
                            }
                        });
                            $('.close').click(function(){
                                 window.open('lista?t=<?php echo $param['tipo'] ?>', '_parent');
                             });
                    } else {
                        $("#body").removeClass("loading");
                        bootbox.dialog({
                            message: response['msg'],
                            title: "Información!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                },
                            }
                        });
                        
                        $("#subir").removeAttr('disabled');
                    }

                }
            });
        }
    });
</script>