<?php
//Mostrar Errores del controlador
foreach (Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="alert alert-' . $key . '">' . $message . "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> </div>\n";
}
?> 
<div id="loginbox" style="margin-top:50px;" class="mainbox col-md-12">           
    <style>
        

        .image-preview-input {
            position: relative;
            overflow: hidden;
            margin: 0px;    
            color: #333;
            background-color: #fff;
            border-color: #ccc;    
        }
        .image-preview-input input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            padding: 0;
            font-size: 20px;
            cursor: pointer;
            opacity: 0;
            filter: alpha(opacity=0);
        }
        .image-preview-input-title {
            margin-left:2px;
        }
    </style>  
    <h1 class="titulo">Tarifador</h1>
    <div class="panel panel-danger" >


        <div style="padding-top:30px" class="panel-body" >

            <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>


            <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'subir-p_personas', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            ?>
            <div class="col-sm-6 hide">
                    <div class="form-group">  
                    
                    <?php
                    
                      echo CHtml::hiddenField('tarif_codigs',  $param['tarif_codigs'], array('prompt' => 'Seleccione...', "class" => "form-control ")); 

                    echo CHtml::hiddenField('o', $_REQUEST['o'], array('prompt' => 'Seleccione...', "class" => "form-control "));

                    ?>

                    </div>
                </div>   
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">  
                    <!-- image-preview-filename input [CUT FROM HERE]-->

                    <label for="clvarea">Costo por Kilómetro</label>
                    <?php
                    
                      echo CHtml::textField('tarif_kiloms', $param['tarif_kiloms'], array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                    ?>

                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">  
                    <!-- image-preview-filename input [CUT FROM HERE]-->

                    <label for="clvarea">Estatus</label>
                    <?php
                    
                      echo CHtml::dropDownList('tgene_status',  $param['tgene_status'], array('1'=>'Activo','0'=>'Inactiva'),array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                    ?>

                    </div>
                </div>
               
            </div>
                
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">  
                    <!-- image-preview-filename input [CUT FROM HERE]-->

                    <label for="clvarea">Hora diurna desde</label>
                    <?php
                    
                      echo CHtml::textField('tarif_diudes',  $param['tarif_diudes'], array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                    ?>

                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">  
                    <!-- image-preview-filename input [CUT FROM HERE]-->

                    <label for="clvarea">Diurno</label>
                    <?php
                    
                      echo CHtml::textField('tarif_diurns', $param['tarif_diurns'], array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                    ?>

                    </div>
                </div>
               
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">  
                    <!-- image-preview-filename input [CUT FROM HERE]-->

                    <label for="clvarea">Hora nocturna desde</label>
                    <?php
                    
                      echo CHtml::textField('tarif_nocdes', $param['tarif_nocdes'], array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                    ?>

                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">  
                    <!-- image-preview-filename input [CUT FROM HERE]-->

                    <label for="clvarea">Nocturno</label>
                    <?php
                    
                      echo CHtml::textField('tarif_noctus', $param['tarif_noctus'], array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                    ?>

                    </div>
                </div>
               
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">  
                    <!-- image-preview-filename input [CUT FROM HERE]-->

                    <label for="clvarea">Fin de semana</label>
                    <?php
                    
                      echo CHtml::textField('tarif_finses', $param['tarif_finses'], array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                    ?>

                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">  
                    <!-- image-preview-filename input [CUT FROM HERE]-->

                    <label for="clvarea">Feriado</label>
                    <?php
                    
                      echo CHtml::textField('tarif_ferias', $param['tarif_ferias'], array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                    ?>

                    </div>
                </div>
               
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">  
                    <!-- image-preview-filename input [CUT FROM HERE]-->

                    <label for="clvarea">Multiparada</label>
                    <?php
                    
                      echo CHtml::textField('tarif_params', $param['tarif_params'], array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                    ?>

                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">  
                    <!-- image-preview-filename input [CUT FROM HERE]-->

                    <label for="clvarea">Multipasajero</label>
                    <?php
                    
                      echo CHtml::textField('tarif_pasams', $param['tarif_pasams'], array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                    ?>

                    </div>
                </div>
               
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">  
                    <!-- image-preview-filename input [CUT FROM HERE]-->

                    <label for="clvarea">Mascota</label>
                    <?php
                    
                      echo CHtml::textField('tarif_mascos',  $param['tarif_mascos'], array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                    ?>

                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">  
                    <!-- image-preview-filename input [CUT FROM HERE]-->

                    <label for="clvarea">Maleta extra</label>
                    <?php
                    
                      echo CHtml::textField('tarif_malets', $param['tarif_malets'], array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                    ?>

                    </div>
                </div>
               
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">  
                    <!-- image-preview-filename input [CUT FROM HERE]-->

                    <label for="clvarea">Tiempo espera</label>
                    <?php
                    
                      echo CHtml::textField('tarif_espers', $param['tarif_espers'], array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                    ?>

                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">  
                    <!-- image-preview-filename input [CUT FROM HERE]-->

                    <label for="clvarea">Encomienda</label>
                    <?php
                    
                      echo CHtml::textField('tarif_encoms', $param['tarif_encoms'], array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                    ?>

                    </div>
                </div>
               
            </div>


            <div class="row">
                <!-- Button -->
                <div class="controls">
                    <div class="col-sm-6">
                        <button id="subir" name="subir" value="subir" type="button" class="btn-block btn btn-uven"><span class="fa fa-upload"></span> Guardar  </button>

                    </div>
                    <div class="col-sm-6">
                        <button id="subir" name="limpiar" value="limpiar" type="reset" class="btn-block btn btn-default"><span class="fa fa-eraser"></span> Limpiar  </button>

                    </div>
                </div>
            </div>              

            <?php $this->endWidget(); ?>

        </div><!-- form -->
    </div>  
</div>
<script>$(document).on('click', '#close-preview', function () {
                        $('.numero1 .image-preview').popover('hide');
                        // Hover befor close the preview
                        $('.numero1 .image-preview').hover(
                                function () {
                                    $('.image-preview').popover('hide');
                                },
                                function () {
                                    $('.image-preview').popover('hide');
                                }
                        );
                    });
                    $(function () {
                        // Create the close button
                        var closebtn = $('<button/>', {
                            type: "button",
                            text: 'x',
                            id: 'close-preview',
                            style: 'font-size: initial;',
                        });

                        // Clear event
                        $('.numero1 .image-preview-clear').click(function () {
                            $('.numero1 .image-preview').attr("data-content", "").popover('hide');
                            $('.numero1 .image-preview-filename').val("");
                            $('.numero1 .image-preview-clear').hide();
                            $('.numero1 .image-preview-input input:file').val("");
                            $(".numero1 .image-preview-input-title").text("Buscar");
                        });
                        // Create the preview image
                        $(".numero1 .image-preview-input input:file").change(function () {
                            var img = $('<img/>', {
                                id: 'dynamic',
                                width: 250,
                                height: 200
                            });
                            var file = this.files[0];
                            var reader = new FileReader();
                            // Set preview image into the popover data-content
                            reader.onload = function (e) {
                                $(".numero1 .image-preview-input-title").text("Cambiar");
                                $(".numero1 .image-preview-clear").show();
                                $(".numero1 .image-preview-filename").val(file.name);
                                img.attr('src', e.target.result);
                            }
                            reader.readAsDataURL(file);
                        });
                    });
                    </script>
                    <script>$(document).on('click', '#close-preview', function () {
                        $('.numero2 .image-preview').popover('hide');
                        // Hover befor close the preview
                        $('.numero2 .image-preview').hover(
                                function () {
                                    $('.image-preview').popover('hide');
                                },
                                function () {
                                    $('.image-preview').popover('hide');
                                }
                        );
                    });
                    $(function () {
                        // Create the close button
                        var closebtn = $('<button/>', {
                            type: "button",
                            text: 'x',
                            id: 'close-preview',
                            style: 'font-size: initial;',
                        });

                        // Clear event
                        $('.numero2 .image-preview-clear').click(function () {
                            $('.numero2 .image-preview').attr("data-content", "").popover('hide');
                            $('.numero2 .image-preview-filename').val("");
                            $('.numero2 .image-preview-clear').hide();
                            $('.numero2 .image-preview-input input:file').val("");
                            $(".numero2 .image-preview-input-title").text("Buscar");
                        });
                        // Create the preview image
                        $(".numero2 .image-preview-input input:file").change(function () {
                            var img = $('<img/>', {
                                id: 'dynamic',
                                width: 250,
                                height: 200
                            });
                            var file = this.files[0];
                            var reader = new FileReader();
                            // Set preview image into the popover data-content
                            reader.onload = function (e) {
                                $(".numero2 .image-preview-input-title").text("Cambiar");
                                $(".numero2 .image-preview-clear").show();
                                $(".numero2 .image-preview-filename").val(file.name);
                                img.attr('src', e.target.result);
                            }
                            reader.readAsDataURL(file);
                        });
                    });</script>
<script type="text/javascript">
    //$('#tarif_kiloms').mask('999.999.99');
    

    $(document).ready(function () {

        $('#subir-p_personas').bootstrapValidator({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                tarif_kiloms: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        regexp: {
                            regexp: /^[0-9\.]+$/,
                             message: 'Escriba un monto de sólo dígitos'
                        }

                    }
                },
                tarif_diurns: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        regexp: {                                              
                            regexp: /^[0-9\.]+$/,
                            message: 'Escriba un monto de sólo dígitos'
                        }

                    }
                },
                tarif_diudes: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        regexp: {
                             regexp: /^[0-9\.]+$/,
                            message: 'Escriba un monto de sólo dígitos'
                        }

                    }
                },
                tarif_noctus: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        regexp: {
                             regexp: /^[0-9\.]+$/,
                            message: 'Escriba un monto de sólo dígitos'
                        }

                    }
                },
                tarif_nocdes: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        regexp: {
                             regexp: /^[0-9\.]+$/,
                            message: 'Escriba un monto de sólo dígitos'
                        }

                    }
                },
                tarif_finses: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        regexp: {
                             regexp: /^[0-9\.]+$/,
                            message: 'Escriba un monto de sólo dígitos'
                        }

                    }
                },
                tarif_ferias: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        regexp: {
                             regexp: /^[0-9\.]+$/,
                            message: 'Escriba un monto de sólo dígitos'
                        }

                    }
                },
                tarif_params: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        regexp: {
                             regexp: /^[0-9\.]+$/,
                            message: 'Escriba un monto de sólo dígitos'
                        }

                    }
                },
                tarif_pasams: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        regexp: {
                             regexp: /^[0-9\.]+$/,
                            message: 'Escriba un monto de sólo dígitos'
                        }

                    }
                },
                tarif_malets: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        regexp: {
                             regexp: /^[0-9\.]+$/,
                            message: 'Escriba un monto de sólo dígitos'
                        }

                    }
                },
                tarif_mascos: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        regexp: {
                             regexp: /^[0-9\.]+$/,
                            message: 'Escriba un monto de sólo dígitos'
                        }

                    }
                },
                tarif_espers: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        regexp: {
                             regexp: /^[0-9\.]+$/,
                            message: 'Escriba un monto de sólo dígitos'
                        }

                    }
                },
                tarif_encoms: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        regexp: {
                             regexp: /^[0-9\.]+$/,
                            message: 'Escriba un monto de sólo dígitos'
                        }

                    }
                },
            }
        });
    });
    $('#subir').click(function () {
        var formData = new FormData($("#subir-p_personas")[0]);
        $('#subir-p_personas').bootstrapValidator('validate'); //secondary validation using Bootstrap Validator      
        var bootstrapValidator = $('#subir-p_personas').data('bootstrapValidator');
        if (bootstrapValidator.isValid()) {
            $.ajax({
                'dataType': "json",
                'type': 'post',
                'url': 'lista',
                'data': formData,
                'cache': false,
                'contentType': false,
                'processData': false,
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    //alert(response['success']);
                    $("#body").removeClass("loading");
                    if (response['success'] == 'true') {
                        $("#body").removeClass("loading");
                        bootbox.dialog({
                            message: response['msg'],
                            title: "Exito!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                    callback: function(){
                                       window.open('lista', '_parent');
                                    }
                                },
                            }
                        });
                            $('.close').click(function(){
                                 window.open('lista', '_parent');
                             });
                    } else {
                        $("#body").removeClass("loading");
                        bootbox.dialog({
                            message: response['msg'],
                            title: "Información!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                },
                            }
                        });
                        
                        $("#subir").removeAttr('disabled');
                    }

                }
            });
        }
    });
</script>