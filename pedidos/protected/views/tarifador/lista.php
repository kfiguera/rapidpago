<?php
//Mostrar Errores del controlador
foreach (Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="alert alert-' . $key . '">' . $message . "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> </div>\n";
}
?> 
<div id="loginbox" style="margin-top:50px;" >
    <?php
    /* echo '<pre>';
      var_dump($model);
      echo '</pre>'; */
    ?>
    <div class="modal"></div>    
    <h1 class="titulo">Tarifador</h1> 
    <div class="panel panel-danger " >


        <div style="padding-top:30px" class="panel-body" >

            <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

           
            <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'subir-p_personas', 'htmlOptions' => array('method' => 'post', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')));
             $user = Yii::app()->user->id->tgene_permi;
            ?>
            <div class="row">
                <div class="col-md-2">
                    <a href="./lista?o=I" class="btn btn-uven btn-block">Nuevo</a>
                </div>
            </div>
            <br></br>
            <div class="table-responsive">
                <table id="auditoria" class="table table-bordered table-hover dataTable" style="width: 100%">
                <thead>
                    <tr>
                        <th width="2%">
                            #
                        </th>
                        <th>
                            Descripción
                        </th>
                       
                        <th width="10%">
                            Modificar
                        </th>
                        <th width="10%">
                            Eliminar
                        </th>
                       
                        
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $sql="SELECT * FROM admin_tarifador";
                    $conexion=yii::app()->db;
                    $result=$conexion->createCommand($sql)->query();
                    $i=0;
                    while (($row=$result->read())!=false) {
                       $i++;
                       echo "<tr>";
                       echo "<td>".$i."</td>";
                        switch ($row['tgene_statu']) {
                            case '1':
                                echo "<td>Activa</td>";
                                break;
                            case '0':
                                echo "<td>Inactiva</td>";
                                break;
                            default:
                                echo "<td>Verifique</td>";
                                break;
                        }

                       echo "<td><a href='./lista?titulo=Tarifador&c=".$row['tarif_codig']."&o=U ' class='btn btn-uven btn-block'><i class='fa fa-pencil'></i></a></td>";
                       echo "<td><a href='modal2?titulo=Tarifador&c=".$row['tarif_codig']."' data-remote='false' data-toggle='modal' data-target='#modal' class='btn btn-uven btn-block'><i class='fa fa-close'></i></a></td>";
                       echo "</tr>";
                    }
                    ?>
                </tbody>
            </table>
            </div>

            <?php $this->endWidget(); ?>

        </div><!-- form -->
    </div>  
</div>

<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
$('#auditoria').DataTable();
    $('#subir').click(function () {
        var formData = new FormData($("#subir-p_personas")[0]);

        $('#subir-p_personas').bootstrapValidator('validate'); //secondary validation using Bootstrap Validator      
        var bootstrapValidator = $('#subir-p_personas').data('bootstrapValidator');
        if (bootstrapValidator.isValid()) {
            $.ajax({
                'dataType': "json",
                'type': 'post',
                'data': formData,
                'cache': false,
                'contentType': false,
                'processData': false,
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    //alert(response['success']);
                    $("#body").removeClass("loading");
                    if (response['success'] == 'true') {
                        $("#body").removeClass("loading");
                        bootbox.dialog({
                            message: response['msg'],
                            title: "Exito!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                    callback: function(){
                                       window.open('tarifador', '_parent');
                                    }
                                },
                            }
                        });
                       $('.close').click( function(){
                            window.open('tarifador', '_parent');
                        });
                    } else {
                        $("#body").removeClass("loading");
                        bootbox.dialog({
                            message: response['msg'],
                            title: "Información!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                },
                            }
                        });
                        
                        $("#subir").removeAttr('disabled');
                    }

                }
            });
        }
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>
