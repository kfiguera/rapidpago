<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Notificaciones</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Notificaciones</a></li>
            <li class="active">Listado</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

<?php
//Mostrar mensajes del controlador
        foreach (Yii::app()->user->getFlashes() as $key => $message) {
          echo '<div class="alert alert-' . $key . ' alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              ' . $message  .' </div>';          
        }
$connection = Yii::app()->db;
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-sm-8">
                <h3 class="panel-title">Listado</h3>
            </div>
        </div>
        
    </div>
    <div class="panel-body" >
        
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-pedido'>
                <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                #
                            </th>
                            <th>
                                Nro Solicitud
                            </th>
                            <th>
                                Titulo
                            </th>
                            <th>
                                Descripción
                            </th>
                            <th>
                                Fecha
                            </th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        /*if(Yii::app()->user->id['usuario']['permiso']==1){
                            $sql = "SELECT *, 
                                    (SELECT COUNT(pdeta_codig) FROM pedido_pedidos_detalle e WHERE a.pedid_codig = e.pedid_codig) modelos, 
                                    (SELECT COUNT(pdlis_codig) FROM pedido_pedidos_detalle_listado f WHERE a.pedid_codig = f.pedid_codig) p_personas 
                                FROM pedido_pedidos a 
                                JOIN rd_preregistro_estatus b ON (a.epedi_codig = b.epedi_codig)
                                JOIN seguridad_usuarios c ON (a.usuar_codig = c.usuar_codig)
                                LEFT JOIN colegio d ON (c.coleg_codig = d.coleg_codig)";
                   
                        }else{
                            $sql = "SELECT *, 
                                    (SELECT COUNT(pdeta_codig) FROM pedido_pedidos_detalle e WHERE a.pedid_codig = e.pedid_codig) modelos, 
                                    (SELECT COUNT(pdlis_codig) FROM pedido_pedidos_detalle_listado f WHERE a.pedid_codig = f.pedid_codig) p_personas  FROM pedido_pedidos a 
                                    JOIN rd_preregistro_estatus b ON (a.epedi_codig = b.epedi_codig)
                                    JOIN seguridad_usuarios c ON (a.usuar_codig = c.usuar_codig)
                                    LEFT JOIN colegio d ON (c.coleg_codig = d.coleg_codig)
                                    WHERE a.usuar_codig = '".Yii::app()->user->id['usuario']['codigo']."'";
                             
                        }*/

                        $sql="SELECT * 
                              FROM solicitud_usuario_notificacion a
                              JOIN solicitud b ON (a.solic_codig = b.solic_codig) 
                              JOIN p_notificacion_estatus c ON (a.enoti_codig = c.enoti_codig) 
                              WHERE a.usuar_codig = '".$usuar."'
                                 OR sunot_leido <> '1'";

                        

                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        $j=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                            $j++;

                            $sql="SELECT * 
                                  FROM solicitud_trayectoria 
                                  WHERE solic_codig = '".$row['solic_codig']."'
                                    AND estat_codig = '".$row['estat_codig']."'";

                            $trayectoria = $connection->createCommand($sql)->queryRow();

                            $inicio=$trayectoria['traye_finic'].' '.$trayectoria['traye_hinic'];
                            $fin=date('Y-m-d H:i:s');
                            $diff=$this->funciones->diferenciaHoras($inicio,$fin);
                            $horas=($diff->days * 24 )  + ( $diff->h );
                            $semaforo=$this->funciones->semaforo($connection,$row['estat_codig'],$horas);
                            
                            $sql = "SELECT sum(cequi_canti) cantidad
                                FROM solicitud_equipos a 
                                WHERE solic_codig='".$row['solic_codig'] ."'";
                            $cequipo = $connection->createCommand($sql)->queryRow();
                            
                            $sql = "SELECT *
                                FROM p_banco 
                                WHERE banco_codig='".$row['banco_codig'] ."'";
                            $banco = $connection->createCommand($sql)->queryRow();
                            $sql = "SELECT *
                                FROM p_solicitud_origen
                                WHERE orige_codig='".$row['orige_codig'] ."'";
                            $origen = $connection->createCommand($sql)->queryRow();
                        ?>
                        <tr>
                            <td><?php echo $i ?></td>

                            <td><a href="consultar?c=<?php echo $row['solic_codig'] ?>"><?php echo $row['solic_numer'] ?></a></td>
                            <td><?php echo $row['enoti_titul'] ?></td>
                            <td><?php echo $row['enoti_descr'] ?></td>
                            <td><?php echo $this->funciones->TransformarFecha_v($row['sunot_fcrea']).' '.$row['sunot_hcrea']; ?></td>
                        </tr>
                        <?php
                            }   
                        ?>
                    </tbody>
                </table>
            </div>
            
            
        </div>
    </div>
</div>  
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel'
            ]
        });
        $('#buscar').click(function () {
        var formData = new FormData($("#pedidos")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-pedido').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>
