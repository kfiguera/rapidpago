<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Producto</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Ventas</a></li>            
            
            <li><a href="#">Producto</a></li>
            <li class="active">Consultar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Consultar Producto</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
                //var_dump($roles);
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                 <div class="row hide">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Código</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php echo CHtml::hiddenField('codig', $roles['fprod_codig'], array('class' => 'form-control', 'placeholder' => "Descripción", 'disabled'=>'true')); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row hide">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Factura Nro:</label>
                            <?php echo CHtml::textField('nfact', $roles['factu_codig'], array('class' => 'form-control', 'placeholder' => "Número de Producto", 'readonly'=>'true')); ?>
                        </div>
                    </div>
                    
                </div>
                <div class="row">    
                    <div class="col-sm-3">        
                        <div class="form-group">
                            <label>Producto</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-archive"></i></span>
                                <?php 
                                $sql="SELECT * FROM inventario WHERE inven_canti>0";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'inven_codig','inven_descr');
                                echo CHtml::dropDownList('produ',  $roles['inven_codig'], $data, array('class' => 'form-control', 'prompt' => "Seleccione", 'disabled'=>'true')); ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">        
                        <div class="form-group">
                            <label>Cantidad</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                <?php echo CHtml::textField('canti', $this->funciones->TransformarMonto_v($roles['fprod_canti'],0), array('class' => 'form-control', 'placeholder' => "Cantidad", 'disabled'=>'true')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">        
                        <div class="form-group">
                            <label>Precio Unitario</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                                <?php echo CHtml::textField('preci', $this->funciones->TransformarMonto_v($roles['fprod_preci'],2), array('class' => 'form-control', 'placeholder' => "Cantidad", 'disabled'=>'true')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">        
                        <div class="form-group">
                            <label>Monto</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                <?php echo CHtml::textField('monto', $this->funciones->TransformarMonto_v($roles['fprod_monto'],2), array('class' => 'form-control', 'placeholder' => "Cantidad", 'disabled'=>'true')); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row hide">    
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observaciones</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php echo CHtml::textArea('obser', $roles['fprod_obser'], array('class' => 'form-control', 'placeholder' => "Observaciones", 'disabled'=>'true')); ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Button -->
                <div class="row controls">
                    <div class="col-sm-12 ">
                        <a href="listado?f=<?php echo $f ?>" type="reset" class="btn-block btn btn-info">Volver </a>
                    </div>
                </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
<script>
$('#punid').mask('#.##0,00',{reverse: true,maxlength:false});
$('#desde').mask('00/00/0000');
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                descr: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Descripción" es obligatorio',
                        }
                    }
                },
                codig: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Código" es obligatorio',
                        }
                    }
                },
                punid: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Precio por Unidad" es obligatorio',
                        }
                    }
                },
                canti: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "cantidad" es obligatorio',
                        }
                    }
                },
                tunid: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Unidad" es obligatorio',
                        }
                    }
                },
                moned: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Moneda" es obligatorio',
                        }
                    }
                },
                /*obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Observaciones" es obligatorio',
                        }
                    }
                },*/



            }
        });
    });

</script>