<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Producto</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Ventas</a></li>            
            <li><a href="#">Producto</a></li>
            <li class="active">Modificar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Modificar Producto</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row hide">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Código</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php echo CHtml::hiddenField('codig', $roles['fprod_codig'], array('class' => 'form-control', 'placeholder' => "Descripción")); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row hide">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Factura Nro:</label>
                            <?php echo CHtml::textField('nfact', $roles['factu_codig'], array('class' => 'form-control', 'placeholder' => "Número de Producto", 'readonly'=>'true')); ?>
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Tipo Producto o Servicio</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-archive"></i></span>
                                <?php $sql="SELECT * FROM producto_tipo";
                                    $result=$conexion->createCommand($sql)->queryAll();
                                    $data=CHtml::listData($result,'tprod_codig','tprod_descr');
                                    echo CHtml::dropDownList('tprod[0]', $roles['tprod_codig'], $data,array('class' => 'form-control', 'placeholder' => "Banco", 'prompt'=>'Seleccione...','id'=>'tprod_0')); ?>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Producto o Servicio</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-archive"></i></span>
                                <?php $sql="SELECT * FROM inventario";
                                    $result=$conexion->createCommand($sql)->queryAll();
                                    $data=CHtml::listData($result,'inven_codig','inven_descr');
                                    echo CHtml::dropDownList('produ[0]', $roles['inven_codig'], $data,array('class' => 'form-control', 'placeholder' => "Banco", 'prompt'=>'Seleccione...','id'=>'produ_0')); ?>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Cantidad</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                <input class="form-control" placeholder="Cantidad" prompt="Seleccione" type="text" value="<?php echo $this->funciones->TransformarMonto_v($roles['fprod_canti'],0); ?>" name="cprod[0]" id="cprod_0" />  
                            </div>      
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Precio Unitario</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                                <input class="form-control" placeholder="Precio Unitario" prompt="Seleccione" type="text" value="<?php echo $this->funciones->TransformarMonto_v($roles['fprod_preci'],2); ?>" name="pprod[0]" id="pprod_0" />  
                            </div>      
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Monto</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                <input class="form-control" placeholder="Monto" readonly="readonly" prompt="Seleccione"  type="text" value="<?php echo $this->funciones->TransformarMonto_v($roles['fprod_monto'],2) ?>" name="mprod[0]" id="mprod_0" />
                            </div>        
                        </div>
                    </div>
                </div>
                <div class="row hide">    
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observaciones</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php echo CHtml::textArea('obser', $roles['fprod_obser'], array('class' => 'form-control', 'placeholder' => "Observaciones")); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-4 ">
                            <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <a href="../factura/modificar?c=<?php echo $f ?>" type="reset" class="btn-block btn btn-default">Volver </a>
                        </div>
                    </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
<script type="text/javascript">
    var produ = {
            row: '.col-xs-3',   // The title is placed inside a <div class="col-xs-4"> element
            validators: {
                notEmpty: {
                    message: 'Debe Seleccionar un Producto'
                }
            }
        },
        cprod = {
            row: '.col-xs-3',   // The title is placed inside a <div class="col-xs-4"> element
            validators: {
                notEmpty: {
                    message: 'Debe ingresar una Cantidad'
                }
            }
        },
        pprod = {
            row: '.col-xs-3',
            validators: {
                notEmpty: {
                    message: 'Debe Ingresar un Valor'
                },
            }
        },
        mprod = {
            row: '.col-xs-3',
            validators: {
                notEmpty: {
                    message: 'Debe Ingresar un Valor'
                },
            }
        },
        bookIndex = 0,
        contador = 0;
        
</script>
<script>
    $('#punid').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#canti').mask('#.##0',{reverse: true,maxlength:false});
$('#desde').mask('00/00/0000');

$('#cprod_0').mask('#.##0',{reverse: true,maxlength:false});
    $('#pprod_0').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#mprod_0').mask('#.##0,00',{reverse: true,maxlength:false});

    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                 nfact: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Número de Factura" es obligatorio',
                        }
                    }
                },
                produ: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Producto" es obligatorio',
                        }
                    }
                },
                canti: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Cantidad" es obligatorio',
                        }
                    }
                },
                'produ[0]': produ,
                'cprod[0]': cprod,
                'pprod[0]': pprod,
                'mprod[0]': mprod,
                
                /*obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Observaciones" es obligatorio',
                        }
                    }
                },*/



            }
        });
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'modificar',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('../factura/modificar?c=<?php echo $f?>', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>


<script>
    $("#produ_0").change(function () {
        $.ajax({
            "url": "<?php echo CController::createUrl('funciones/GenerarValorProd'); ?>", 
            "data": {
                "producto": produ_0.value,
                "tipo": tprod_0.value
            }, 
            "type": "POST",
            "dataType": "json",
            "success": function (response){
                if (response['success'] == 'true') {
                        document.getElementById("cprod_0").value = response["cantidad"];
                        document.getElementById("pprod_0").value = response["precio"];
                        document.getElementById("mprod_0").value = response["monto"];
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }
            },error:function (response) {
                    $('#wrapper').unblock();
                swal({ 
                    title: "Error!",
                    text: "Error el ejecutar la operación",
                    type: "error",
                    confirmButtonText: "Cerrar",
                    confirmButtonClass: "btn-danger"

                },function(){
                    $("#guardar").removeAttr('disabled');
                });
                    
            }
        });
    });
</script>
<script>
    $("#pprod_0").change(function () {
        CambiarValorProd_0();
    });
    $("#cprod_0").change(function () {
        CambiarValorProd_0();
    });
</script>
<script>
    function CambiarValorProd_0(){
        $.ajax({
            "url": "<?php echo CController::createUrl('funciones/CambiarValorProd'); ?>", 
            "data": {
                "cantidad": cprod_0.value,
                "precio": pprod_0.value
            }, 
            "type": "POST",
            "dataType": "json",
            "success": function (response){
                if (response['success'] == 'true') {
                        
                        document.getElementById("mprod_0").value = response["monto"];
                        
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }
            },error:function (response) {
                    $('#wrapper').unblock();
                swal({ 
                    title: "Error!",
                    text: "Error el ejecutar la operación",
                    type: "error",
                    confirmButtonText: "Cerrar",
                    confirmButtonClass: "btn-danger"

                },function(){
                    $("#guardar").removeAttr('disabled');
                });
                    
            }
        });
    }
</script>
<script>

    function montoTotal(){
         $.ajax({
            "url": "<?php echo CController::createUrl('funciones/GenerarMontoTotal'); ?>", 
            "data": $('#login-form').serialize(),
            "type": "POST",
            "dataType": "json",
            "success": function (response){
                if (response['success'] == 'true') {
                        $("#monto_total").text(response["monto"]);
                        document.getElementById("mtotal").value = response["monto"];
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }
            },error:function (response) {
                    $('#wrapper').unblock();
                swal({ 
                    title: "Error!",
                    text: "Error el ejecutar la operación",
                    type: "error",
                    confirmButtonText: "Cerrar",
                    confirmButtonClass: "btn-danger"

                },function(){
                    $("#guardar").removeAttr('disabled');
                });
                    
            }
        });
    }

</script>

<script type="text/javascript">
    $('#tprod_0').change(function () {
        $.ajax({
            'type':'POST',
            'data':{'tprod':this.value},
            'url':'<?php echo CController::createUrl('funciones/TipoProducto'); ?>',
            'cache':false,
            'success':function(html){
                jQuery("#produ_0").html(html)
            }
        });
    });
</script>