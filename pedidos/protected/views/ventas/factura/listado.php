<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Factura</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Ventas</a></li>            
            
            <li><a href="#">Factura</a></li>
            <li class="active">Listado</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Criterio de Busqueda</h3>
    </div>
    <div class="panel-body" >
        <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'p_personas', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            $connection = Yii::app()->db;

        ?>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Número de Factura</label>
                    <?php echo CHtml::textField('nfact', '', array('class' => 'form-control', 'placeholder' => "Número de Factura")); ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Banco</label>
                    <?php $sql="SELECT * FROM p_banco ";
                        $result=$connection->createCommand($sql)->queryAll();
                        $data=CHtml::listData($result,'banco_codig','banco_descr');
                        echo CHtml::dropDownList('banco', '', $data,array('class' => 'form-control', 'placeholder' => "Banco", 'prompt'=>'Seleccione...')); ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Estatus de la factura</label>
                    <?php $sql="SELECT * FROM factura_estatu";
                                    $result=$connection->createCommand($sql)->queryAll();
                                    $data=CHtml::listData($result,'efact_codig','efact_descr');
                                    echo CHtml::dropDownList('efact', '', $data,array('class' => 'form-control', 'placeholder' => "Estatus Factura", 'prompt'=>'Seleccione...')); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <button type="button" id="buscar" class="btn btn-block btn-info">Buscar</button>
            </div>
            <div class="col-sm-4">
                <button type="reset" class="btn btn-block btn-default">Limpiar</button>
            </div>
            <div class="col-sm-4">
                <a href="registrar" class="btn btn-block btn-info" >Nuevo</a>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div> 
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Listado</h3>
    </div>
    <div class="panel-body" >
        
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-p_persona'>
                <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                #
                            </th>
                            <th>
                                Número de Factura
                            </th>
                            <th>
                                Número de Control
                            </th>
                            <th>
                                Fecha Emision
                            </th>
                            <th>
                                Monto
                            </th>
                            <th>
                                Banco
                            </th>
                            <th>
                                Estatus
                            </th>
                            <!--th width="5%">
                                Productos
                            </th-->
                            <th width="5%">
                                Consultar
                            </th>
                            <th width="5%">
                                Modificar
                            </th>
                            <th width="5%">
                                Eliminar
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "SELECT * FROM factura a
                                JOIN factura_estatu b ON (a.efact_codig = b.efact_codig)
                                JOIN p_banco c ON (a.banco_codig = c.banco_codig)";
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                        ?>
                        <tr>
                            <td class="tabla"><?php echo $i ?></td>
                            <td class="tabla"><?php echo $row['factu_nfact'] ?></td>
                            <td class="tabla"><?php echo $row['factu_ncont'] ?></td>
                            <td class="tabla"><?php echo $this->funciones->TransformarFecha_v($row['factu_femis']) ?></td>

                            <td class="tabla"><?php echo $this->funciones->TransformarMonto_v($row['factu_monto'],2) ?></td>
                            <td class="tabla"><?php echo $row['banco_descr'] ?></td>
                            <td class="tabla"><?php echo $row['efact_descr'] ?></td>
                            <!--td class="tabla"><a href="../producto?f=<?php echo $row['factu_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-list"></i></a></td-->
                            <td class="tabla"><a href="consultar?c=<?php echo $row['factu_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-search"></i></a></td>
                            <?php 
                                if($row["efact_codig"]<>'1') {
                                    ?>
                                        <td class="tabla"><a href="#" class="btn btn-block btn-info disabled"><i class="fa fa-pencil"></i></a></td>
                                        <td class="tabla"><a href="#" class="btn btn-block btn-info disabled"><i class="fa fa-close"></i></a></td>        
                                    <?php
                                }else{
                                    ?>
                                        <td class="tabla"><a href="modificar?c=<?php echo $row['factu_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-pencil"></i></a></td>
                                        <td class="tabla"><a href="eliminar?c=<?php echo $row['factu_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-close"></i></a></td>        
                                    <?php
                                }
                            ?>
                        </tr>
                        <?php
                            }   
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <a href="AprobarFacturas" class="btn btn-block btn-info" >Aprobar</a>
                </div>
                <div class="col-sm-3">
                    <a href="ReversarAprobarFacturas" class="btn btn-block btn-info" >Reversar Aprobación</a>
                </div>
                <div class="col-sm-3">
                    <a href="AnularFacturas" class="btn btn-block btn-info" >Anulación</a>
                </div>
                <div class="col-sm-3">
                    <a href="ReversarAnularFacturas" class="btn btn-block btn-info" >Reversar Anulación</a>
                </div>
            </div>
        </div>
    </div>
</div>  
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#buscar').click(function () {
        var formData = new FormData($("#p_personas")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-p_persona').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>
