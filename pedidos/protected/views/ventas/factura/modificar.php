<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Factura</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Ventas</a></li>            
            <li><a href="#">Factura</a></li>
            <li class="active">Modificar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<form id='login-form' name='login-form' method="post">
    <div class="row">                    
        <div class="panel panel-default" >

            <div class="panel-heading" >
                <h3 class="panel-title">Cliente<div class="panel-action">
                        <a href="#" data-perform="panel-collapse">
                            <i class="ti-plus"></i>
                        </a> 
                    </div>
                </h3>
            </div>
        <div class="panel-wrapper collapse in">
            <div class="panel-body" >
                <?php
                    $conexion=Yii::app()->db;
                    
                ?>
                    <div class="row hide">
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Cliente</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <?php 
                                        echo CHtml::hiddenField('clien', $cliente['clien_codig'],array('class' => 'form-control', 'placeholder' => "Descripción", 'prompt'=>'Seleccione...', 'disabled'=>'true')); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Tipo de Cliente</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-list"></i></span>
                                    <?php 
                                        $sql="SELECT * FROM cliente_tipo";
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'tclie_codig','tclie_descr');
                                        echo CHtml::dropDownList('tclie', $cliente['tclie_codig'], $data,array(
                                            'ajax' => array(
                                                'type' => 'POST',
                                                'url' => CController::createUrl('funciones/TipoDocumento'), // Controlador que devuelve las p_ciudades relacionadas
                                                'update' => '#tdocu', // id del item que se actualizará
                                            ),'class' => 'form-control', 'placeholder' => "Descripción", 'prompt'=>'Seleccione...', 'disabled'=>'true')); ?>

                                </div>
                            </div>
                        </div>
                                        
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Documento de Identidad</label>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <?php 
                                        $sql="SELECT a.* FROM p_documento_tipo a 
                                        JOIN tipo_cliente_documento b ON (a.tdocu_codig=b.tdocu_codig) 
                                        WHERE b.tclie_codig='".$cliente['tclie_codig']."'";
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'tdocu_codig','tdocu_descr');
                                        echo CHtml::dropDownList('tdocu', $cliente['tdocu_codig'], $data,array('ajax' => array(
                                                'type' => 'POST',
                                                'url' => CController::createUrl('funciones/ListarCliente'),
                                                'dataType' => 'json',
                                                'success' => 'ListarCliente',
                                            ),'class' => 'form-control', 'placeholder' => "Descripción", 'prompt'=>'Seleccione...', 'disabled'=>'true')); ?>
                                    </div>
                                    <div class="col-sm-8">
                                        <?php echo CHtml::textField('ndocu', $cliente['clien_ndocu'], array('ajax' => array(
                                                'type' => 'POST',
                                                'url' => CController::createUrl('funciones/ListarCliente'),
                                                'dataType' => 'json',
                                                'success' => 'ListarCliente',
                                            ),'class' => 'form-control', 'placeholder' => "Número de Documento", 'disabled'=>'true')); ?>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Nombre o Razón Social</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                    <?php echo CHtml::textField('denom', $cliente['clien_denom'], array('class' => 'form-control', 'placeholder' => "Nombre o Razón Social", 'disabled'=>'true')); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">        
                        <div class="col-sm-6">        
                            <div class="form-group">
                                <label>Correo Electrónico</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    <?php 
                                        echo CHtml::textField('corre', $cliente['clien_corre'], array('class' => 'form-control', 'placeholder' => "Correo Electrónico", 'disabled'=>'true')); ?>
                                </div>
                            </div>
                        </div>
                    
                    
                        <div class="col-sm-6">        
                            <div class="form-group">
                                <label>Teléfono</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-archive"></i></span>
                                    <?php echo CHtml::textField('telef', $cliente['clien_telef'], array('class' => 'form-control', 'placeholder' => "Teléfono", 'disabled'=>'true')); ?>

                                </div>
                            </div>
                        </div>
                        
                    
                    </div>
                    <div class="row">
                        <div class="col-sm-12">        
                            <div class="form-group">
                                <label>Dirección</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-home"></i></span>
                                    <?php 

                                        echo CHtml::textArea('direc', $cliente['clien_direc'], array('class' => 'form-control', 'placeholder' => "Dirección", 'disabled'=>'true')); ?>

                                </div>
                            </div>
                        </div>
                        
                    </div>
            </div><!-- form -->
        </div> 
        </div>  
    </div>
    <div class="row">                    
        <div class="panel panel-default" >

            <div class="panel-heading" >
                <h3 class="panel-title">Factura
                <div class="panel-action">
                        <a href="#" data-perform="panel-collapse">
                            <i class="ti-plus"></i>
                        </a> 
                    </div>
                </h3>
            </div>
                    <div class="panel-wrapper collapse in">

            <div class="panel-body" >
                <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
                    <div class="row hide">    
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Código</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <?php 
                                       echo CHtml::hiddenField('codig', $factura['factu_codig'],array('class' => 'form-control', 'placeholder' => "Descripción", 'prompt'=>'Seleccione...')); 
                                    ?>
                                </div>
                            </div>
                        </div>  
                       
                    </div>
                     <div class="row">    
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Vendedor</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <?php 
                                        $sql="SELECT traba_codig, concat(perso_pnomb,' ',perso_papel) nombre FROM p_trabajador a 
                                              JOIN p_persona b ON (a.perso_codig=b.perso_codig)
                                              WHERE ttipo_codig=1";
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'traba_codig','nombre');
                                        echo CHtml::dropDownList('traba', $factura['traba_codig'], $data,array('class' => 'form-control', 'placeholder' => "Descripción", 'prompt'=>'Seleccione...')); 
                                    ?>
                                </div>
                            </div>
                        </div>  
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Número de Factura</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                    <?php echo CHtml::textField('nfact', $factura['factu_nfact'], array('class' => 'form-control', 'placeholder' => "Número de Factura")); ?>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Número de Control</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                    <?php echo CHtml::textField('ncont', $factura['factu_ncont'], array('class' => 'form-control', 'placeholder' => "Número de Control")); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                   
                    <div class="row">
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Tipo de Venta</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-list"></i></span>
                                    

                                    <?php $sql="SELECT * FROM venta_tipo";
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'tvent_codig','tvent_descr');
                                        echo CHtml::dropDownList('tvent', $factura['tvent_codig'], $data,array('class' => 'form-control', 'placeholder' => "Descripción", 'prompt'=>'Seleccione...')); ?>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Fecha de Emisión</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <?php echo CHtml::textField('femis', $this->funciones->TransformarFecha_v($factura['factu_femis']), array('class' => 'form-control', 'placeholder' => "Fecha de Emisión")); ?>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Puntos</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                    <?php echo CHtml::textField('descu', $this->funciones->TransformarMonto_v($factura['factu_descu'],2), array('class' => 'form-control', 'placeholder' => "Puntos")); ?>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">        
                                     
                        <!--div class="col-sm-4">        
                            <div class="form-group">
                                <label>Monto Factura</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                    <?php echo CHtml::textField('monto', '', array('class' => 'form-control', 'placeholder' => "Monto Factura")); ?>

                                </div>
                            </div>
                        </div-->
                        
                        
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Tipo de Pago</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-list"></i></span>
                                    <?php $sql="SELECT * FROM p_pago_tipo";
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'ptipo_codig','ptipo_descr');
                                        echo CHtml::dropDownList('ptipo', $factura['ptipo_codig'], $data,array('class' => 'form-control', 'placeholder' => "Descripción", 'prompt'=>'Seleccione...')); ?>

                                </div>
                            </div>
                        </div>       
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Banco</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-list"></i></span>
                                    

                                    <?php $sql="SELECT * FROM p_banco ";
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'banco_codig','banco_descr');
                                        echo CHtml::dropDownList('banco', $factura['banco_codig'], $data,array('class' => 'form-control', 'placeholder' => "Banco", 'prompt'=>'Seleccione...')); ?>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Nro de Refencia</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                                    <?php echo CHtml::textField('refer', $factura['factu_refer'], array('class' => 'form-control', 'placeholder' => "Nro de Refencia")); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">  
                        
                        <div class="col-sm-6">        
                            <div class="form-group">
                                <label>Estatus Pago</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-list"></i></span>                              
                                    <?php $sql="SELECT * FROM factura_estatu_pago";
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'efpag_codig','efpag_descr');
                                        echo CHtml::dropDownList('efpag', $factura['efpag_codig'], $data,array('class' => 'form-control', 'placeholder' => "Estatus Factura", 'prompt'=>'Seleccione...')); ?>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">        
                            <div class="form-group">
                                <label>Estatus Factura</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-list"></i></span>                              
                                    <?php $sql="SELECT * FROM factura_estatu";
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'efact_codig','efact_descr');
                                        echo CHtml::dropDownList('efact', $factura['efact_codig'], $data,array('class' => 'form-control', 'placeholder' => "Estatus Factura", 'prompt'=>'Seleccione...', 'disabled'=>'true')); ?>

                                </div>
                            </div>
                        </div>  
                        
                        
                    </div>
                    <div class="row">
                        <div class="col-sm-12">        
                            <div class="form-group">
                                <label>Observaciones</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                    <?php 

                                        echo CHtml::textArea('obser', $factura['factu_obser'], array('class' => 'form-control', 'placeholder' => "Observaciones")); ?>

                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div><!-- form -->
        </div>  
    </div>
    <div class="row">                    
        <div class="panel panel-default" >

            <div class="panel-heading" >
                <h3 class="panel-title">Productos y Servicios 
                    <div class="panel-action">
                        <a href="#" data-perform="panel-collapse">
                            <i class="ti-plus"></i>
                        </a> 
                    </div>
            </h3>
                

            </div>
            <div class="panel-wrapper collapse in">

                <div class="panel-body" >
                    <div class="col-sm-12 table-responsive" id='listado-p_persona'>
                        <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                            <thead>
                                <tr>
                                    <th width="2%">
                                        #
                                    </th>
                                    <th>
                                        Tipo
                                    </th>
                                    <th>
                                        Descripción
                                    </th>
                                    <th>
                                        Precio Unitario
                                    </th>
                                    <th>
                                        Cantidad
                                    </th>
                                    <th>
                                        Monto
                                    </th>
                                    <th width="5%">
                                        Modificar
                                    </th>
                                    <th width="5%">
                                        Eliminar
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                
                                foreach ($productos as $key => $producto) {
                                
                                    $total['canti']+=$producto['fprod_canti'];
                                    $total['preci']+=$producto['fprod_preci'];
                                    $total['monto']+=$producto['fprod_monto'];
                                    $i++;
                                ?>
                                <tr>
                                    <td class="tabla"><?php echo $i ?></td>
                                    <td class="tabla"><?php echo $producto['tprod_descr'] ?></td>
                                    <td class="tabla"><?php echo $producto['inven_descr'] ?></td>
                                    <td class="tabla"><?php echo $this->funciones->TransformarMonto_v($producto['fprod_preci'],2) ?></td>
                                    <td class="tabla"><?php echo $this->funciones->TransformarMonto_v($producto['fprod_canti'],0) ?></td>
                                    

                                    <td class="tabla"><?php echo $this->funciones->TransformarMonto_v($producto['fprod_monto'],2) ?></td>
                                    <td class="tabla"><a href="../producto/modificar?f=<?php echo $producto['factu_codig'] ?>&c=<?php echo $producto['fprod_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-pencil"></i></a></td>
                                    <td class="tabla"><a href="../producto/eliminar?f=<?php echo $producto['factu_codig'] ?>&c=<?php echo $producto['fprod_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-close"></i></a></td>
                                </tr>
                                <?php
                                    }   
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th class="tabla" colspan="3">Total</th>
                                    <th class="tabla"><?php echo $this->funciones->TransformarMonto_v($total['preci'],2); ?></th>
                                    <th class="tabla"><?php echo $this->funciones->TransformarMonto_v($total['canti'],0); ?></th>
                                    
                                    <th class="tabla"><?php echo $this->funciones->TransformarMonto_v($total['monto'],2); ?></th>
                                    <th colspan="2">
                                        <a href="../producto/registrar?f=<?php echo $factura['factu_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-plus"></i></a>
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div><!-- form -->
            </div>
        </div>  
    </div>
    <div class="row">                    
        <div class="panel panel-default" >
            <div class="panel-body" >
<!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-4 ">
                            <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</form>
<script>
    $('#monto').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#mpago').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#devol').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#flete').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#reten').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#descu').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#cprod_0').mask('#.##0',{reverse: true,maxlength:false});
    $('#pprod_0').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#mprod_0').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#femis').mask('99/99/9999');
    $('#fentr').mask('99/99/9999');
    $('#fvenc').mask('99/99/9999');
    $('#fpago').mask('99/99/9999');
    $('#freci').mask('99/99/9999');

    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                nfact: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Número de Factura" es obligatorio',
                        }
                    }
                },
                ncont: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Número de Control" es obligatorio',
                        }
                    }
                },
                traba: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Vendedor" es obligatorio',
                        }
                    }
                },
                clien: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Cliente" es obligatorio',
                        }
                    }
                },
                tvent: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Venta" es obligatorio',
                        }
                    }
                },
                femis: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fecha de Emisión" es obligatorio',
                        }
                    }
                },
                dcred: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Días de Credito" es obligatorio',
                        }
                    }
                },
                fentr: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fecha de Entrega" es obligatorio',
                        }
                    }
                },
                fvenc: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fecha de Vencimiento" es obligatorio',
                        }
                    }
                },
                fpago: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fecha de Pago" es obligatorio',
                        }
                    }
                },
                freci: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fecha Recibida" es obligatorio',
                        }
                    }
                },
                descu: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Descuento" es obligatorio',
                        }
                    }
                },
                pdesc: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Porcentaje de Descuento" es obligatorio',
                        }
                    }
                },
                monto: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Monto Factura" es obligatorio',
                        }
                    }
                },
                mpago: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Monto Pagado" es obligatorio',
                        }
                    }
                },
                flete: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Monto Flete" es obligatorio',
                        }
                    }
                },
                devol: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Monto Devolución" es obligatorio',
                        }
                    }
                },
                ptipo: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Pago" es obligatorio',
                        }
                    }
                },
                reten: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Retención" es obligatorio',
                        }
                    }
                },
                banco: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Pago" es obligatorio',
                        }
                    }
                },
                efpag: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Retención" es obligatorio',
                        }
                    }
                },
                /*obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Observaciones" es obligatorio',
                        }
                    }
                },*/



            }
        });
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'modificar',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>

<script>
// Date Picker
    
    jQuery('#femis').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'

      });
    jQuery('#fentr').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'

      });
    jQuery('#freci').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'

      });

    jQuery('#fvenc').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'

      });
    jQuery('#fpago').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'

      });
      
</script>