<?php 
$codigoPedido= $_GET['c'];
$numero=       $_GET['numero'];
$modelo=          $_GET['mod'];
$color=        $_GET['color'];

if ($modelo==NULL) {
    $modelo=$mod;
    # code..x.
}
 
$boton="guardar";
$conexion=Yii::app()->db;
   
   if ($_GET['m']!=NULL) {
       # code...
              $codigoDetalle = $_GET['m'];
              $sql="SELECT * 
              FROM pedido_detalle  a
              WHERE a.detal_codig ='".$_GET['m']."'";
              $roles=$conexion->createCommand($sql)->queryRow();
              echo $roles['detal_color'];
              $boton="modificar";


   }if ($_GET['e']!=NULL) {
              $boton="eliminar";
    }
            
  
 ?>

<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Pedidos</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Pedidos</a></li>
            <li class="active">Modelo</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Detalle del Modelo</h3> 
                  
       
            
        </div>
        <div class="panel-body" >
           
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Categoria</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                  <select placeholder="Categoria" id="categoria" class="form-control" name="categoria" onchange="if(document.images) document.images['cambio'].src='../imagenes/poleron'+this.options[this.selectedIndex].value+'.png';">
                                    <option value="">Seleccione una opci&oacute;n</option>

                                    <?php 
                                        $sql="SELECT * FROM pedido_categoria WHERE model_codig= ".$modelo." order by categ_descr asc";
                                        $result=$conexion->createCommand($sql)->queryAll();
                                          foreach($result as $result) {
                                            $selected='';
                                           if ($roles['categ_codig'] == $result["categ_codig"]) $selected = 'selected';
                                             echo '<option value="'.$result["categ_codig"].'" '.$selected.'>'.$result["categ_descr"].'</option>';
                                             }?>
                                 </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Sub-categoria</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <select placeholder="subcategoria" id="subcategoria" class="form-control" name="subcategoria">
                                    <option value="0">Seleccione una opci&oacute;n</option>
                                        <?php 
                                        $sql="SELECT * FROM pedido_subcategoria ";
                                        $result=$conexion->createCommand($sql)->queryAll();
                                          foreach($result as $result) {
                                            $selected='';
                                           if ($roles['scate_codig'] == $result["categ_codig"]) $selected = 'selected';
                                             echo '<option value="'.$result["categ_codig"].'" '.$selected.'>'.$result["categ_descr"].'</option>';
                                             }?>
                                    
                                 </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Variante</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                              <?php 
                                $sql="SELECT * FROM pedido_variante order by varia_descr asc";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'varia_codig','varia_descr');

                                echo CHtml::dropDownList('variante', $roles['varia_codig'], $data, array('class' => 'form-control', 'placeholder' => "Variante",'prompt'=>'Seleccione...')); ?>


                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Opcional</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php 
                                $sql="SELECT * FROM pedido_opcional WHERE opcio_codig not in (4,5) order by opcio_descr asc";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'opcio_codig','opcio_descr');

                                echo CHtml::dropDownList('opcional', $roles['opcio_codig'], $data, array('class' => 'form-control', 'placeholder' => "Opcional",'prompt'=>'Seleccione...')); ?>

                            </div>
                        </div>
                    </div>

                     <div class="col-sm-4" >        
                        <div class="form-group" id="pretina" style="display:none;">
                            <label>Indicar Detalles Puño y Pretina</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php 
                                $sql="SELECT * FROM pedido_opcional WHERE opcio_codig = 4 or opcio_codig= 5 order by opcio_descr asc";
                                $result=$conexion->createCommand($sql)->queryAll();
                                 $data=CHtml::listData($result,'opcio_codig','opcio_descr');

                                echo CHtml::dropDownList('linea', $roles['detal_pretin'], $data, array('class' => 'form-control', 'placeholder' => "Opcional",'prompt'=>'Seleccione...','disabled'=>'true')); ?>

                            </div>
                        </div>
   <!-- /.col-lg-12**********************PARA CAMBIOS DE PANTALLLA*********************************************************************************************************************************************************++++++ -->
                        <div class="form-group" id="cierre" style="display:none;">
                            <label>Posee Cierre</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                 <select placeholder="Mangas" id="cierres" class="form-control" name="cierres" >
                                  
                                           
                                            <option value="1">Si</option>
                                           
                                            
                                 </select>

                            </div>
                        </div>


                        <div class="form-group" id="vivo" style="display:none;">
                            <label>Posee Color Vivo Bolsillo</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                 <select placeholder="Mangas" id="vivos" class="form-control" name="vivos" >
                                  
                                           
                                            <option value="1">Si</option>
                                           
                                            
                                 </select>

                            </div>
                        </div>

                           <div class="form-group" id="detalle" style="display:visible;">
                            <label>Detalle Opcional</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                 <select placeholder="Mangas" id="opcionales" class="form-control" name="opcionales" >
                                  
                                           
                                           <option value="">Seleccione una opci&oacute;n</option>
                                           
                                            
                                 </select>

                            </div>
                        </div>
 <!-- /.col-lg-12*******************************************************************************************************************************************************************************++++++ -->


                    </div>

                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Indicar Color</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php 
                                $sql="SELECT * FROM pedido_color where color_codig not in (4,5) order by color_descr asc";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'color_codig','color_descr');

                                echo CHtml::dropDownList('lineaColor',$roles['detal_color'], $data, array('class' => 'form-control', 'placeholder' => "Color",'prompt'=>'Seleccione...')); ?>

                            </div>
                        </div>
                    </div>


                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color Mangas</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                        <?php 
                                $sql="SELECT * FROM pedido_color where color_codig in (4,5)  order by color_codig asc";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'color_codig','color_descr');

                                echo CHtml::dropDownList('mangas',$roles['detal_manga'], $data, array('class' => 'form-control', 'placeholder' => "Color",'prompt'=>'Seleccione...')); ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label id="etiqueta" >Indicar Color</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php 
                                $sql="SELECT * FROM pedido_color where color_codig not in (4,5) order by color_descr asc";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'color_codig','color_descr');

                                echo CHtml::dropDownList('mangaColor', $roles['detal_cmanga'], $data, array('class' => 'form-control', 'placeholder' => "Modelo",'prompt'=>'Seleccione...')); ?>

                            </div>
                        </div>

                    </div>
                    
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <div id="mostrarImg"> <img src="" name="cambio" id="cambio" alt="" height="100" width="100"></div>
                            
                          
                        </div>
                        
                    </div>
                   
                    
                    
                </div>
                
                
                    <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-4 ">
                            <button id="<?=$boton?>" name="<?=$boton?>" type="button" class="btn-block btn btn-info">Continuar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                        </div>
                    </div>
                <input type="hidden" name="hidColor" id="hidColor" value="">
                <input type="hidden" name="hidColorBd" id="hidColorBd" value="<?=$color?>">
                <input type="hidden" name="codigoPedido" id="codigoPedido" value="<?=$codigoPedido?>">
                <input type="hidden" name="numero" id="numero" value="<?=$numero?>">
                <input type="hidden" name="modelo" id="modelo" value="<?=$modelo?>">
                <input type="hidden" name="codigoDetalle" id="codigoDetalle" value="<?=$codigoDetalle?>">



            

        </div><!-- form -->
    </div>  
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Listado</h3>
    </div>
    <div class="panel-body" >
        
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-pedido'>
                <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                #
                            </th>
                            <th>
                                Nro Pedido
                            </th>
                            <th>
                                Modelo
                            </th>
                            <th>
                                Categoria
                            </th>
                            <th>
                                Sub-Categoria
                            </th>
                            <th>
                                variante
                            </th>
                            <th>
                                Opcional
                            </th>
                            <th>
                                Det.Opcional
                            </th>
                             <th>
                                Color.Opcional
                            </th>
                            <th>
                                Manga
                            </th>
                            <th>
                                Color.Manga
                            </th>
                            <!--th>
                                Número de Contrato
                            </th-->
                            
                            <th width="">
                                Modificar
                            </th>
                            <th width="">
                                Eliminar
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "SELECT * FROM v_detalle ";
                        $command = $conexion->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                        ?>
                        <tr>
                            <td class="tabla"><?php echo $i ?></td>
                            <td class="tabla"><?php echo $row['pedid_nume'] ?></td>
                            <td class="tabla"><?php echo $row['modelo'] ?></td>
                            <td class="tabla"><?php echo $row['categoria'] ?></td>
                            <td class="tabla"><?php if ($row['subcategoria']==NULL) { echo "NO TIENE";}else{ echo $row['subcategoria'];} ?></td>
                            <td class="tabla"><?php echo $row['variante'] ?></td>
                            <td class="tabla"><?php echo $row['opcional'] ?></td>

                            <? if ($row['opcio_codig']==1) {?>
                             <td class="tabla"><?php echo $row['opcional1'] ?></td>
                            <?} ?>
                            <? if ($row['opcio_codig']==2) {?>
                             <td class="tabla"><?php echo "POSEE CIERRE";?></td>
                            <?} ?>
                            <? if ($row['opcio_codig']==3) {?>
                             <td class="tabla"><?php echo "POSEE COLOR VIVO BOLSILLO";?></td>
                            <?} ?>
                            
                            <td class="tabla"><?php echo $row['color'] ?></td>
                            <td class="tabla"><?php echo $row['manga'] ?></td>
                            <td class="tabla"><?php echo $row['colormanga'] ?></td>
                            

                         
                            
                            <td class="tabla"><a href="detalle?m=<?php echo $row['detal_codig'] ?>&mod=<?=$modelo?>&numero=<?=$numero?>&color=<?=$color?>&c=<?=$codigoPedido?>" class="btn btn-block btn-info"><i class="fa fa-pencil"></i></a></td>
                            <td class="tabla"><a id="eliminarId" href="#" class="btn btn-block btn-info"><i class="fa fa-close"></i></a></td>
                        <input type="hidden" name="codigoEliminar" id="codigoEliminar" value="<?=$row['detal_codig']?>"
                        </tr>
                        <?php
                            }   
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div> 

</form>
<?if ($roles['opcio_codig']==3) {?>
   <script>   $("#pretina").hide();
                    $("#cierre").hide();
                    $("#detalle").hide();
                    $("#vivo").show();
                    $("#lineaColor").prop("disabled", false);
                    $("#linea").val('');
                    //$("#lineaColor").val('');
                </script> 
<?} ?>
<?if ($roles['opcio_codig']==2) {?>
   <script>    $("#pretina").hide();
                    $("#cierre").show();
                    $("#detalle").hide();
                    $("#vivo").hide();
                    $("#lineaColor").prop("disabled", false);
                    $("#linea").val('');
                    //$("#lineaColor").val('');
                </script> 
<?} ?>
<?if ($roles['opcio_codig']==1) {?>
   <script>    $("#pretina").show();
                    $("#cierre").hide();
                    $("#detalle").hide();
                    $("#vivo").hide();
                    $("#lineaColor").prop("disabled", false);
                    $("#linea").prop("disabled", false);
                    //$("#lineaColor").val('');

                </script> 
<?} ?>

<script>

   $("#subcategoria").prop("disabled", true);
/*$('#fnaci').mask('00/00/0000');
$('#desde').mask('00/00/0000');
$('#preci').mask('#.##0,00',{reverse: true,maxlength:false});*/
$("#categoria").change(event => {
  //alert('You like ' + event.target.value + ' ice cream.');
  //$.get('prueba/Municipios&id='+ event.target.value, function(data){
    $.getJSON('categoria?id='+ event.target.value,function(res, sta){
      $("#subcategoria").empty();
      $("#subcategoria").append('<option value="">Seleccione una opción</option>');
    
    //alert(res.msg);
    $.each(res, function(key, value){
        $("#subcategoria").prop("disabled", false);
      //alert(key.tmuni_codig);
      $("#subcategoria").append('<option value="' + value.categ_codig + '">' + value.categ_descr + '</option>');
    });
  });
});
        
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                descr: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Descripción" es obligatorio',
                        }
                    }
                },
                preci: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Precio" es obligatorio',
                        }
                    }
                },
                moned: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Moneda" es obligatorio',
                        }
                    }
                }



            }
        });
    });

$("#mangaColor").change(function(){
          
         if ($(this).val() != "") {
                    //$("#linea").prop("disabled", false)
                    //$("#mangaColor").prop("disabled", false);
                    var variable = $(this).val();
                      $("#hidColor").val(variable);
                   
                }else{
                    //$("#mangas").prop("disabled", true);
                       $("#mangas").val('');
                       $("#hidColor").val('');
                      

                     }
         
        });



$("#mangas").change(function(){
          
         if ($(this).val() == "5") {
                    //$("#linea").prop("disabled", false)
                    $("#mangaColor").prop("disabled", false);
                    
                      $("#hidColor").val('');
                   
                }else{
                    $("#mangaColor").prop("disabled", true);
                       $("#mangaColor").val('');
                      var variable = $("#hidColorBd").val();

                      $("#hidColor").val(variable);

                     }
         
        });

$("#linea").change(function(){
          
         if ($(this).val() == "5") {
                    //$("#linea").prop("disabled", false);
                    $("#lineaColor").prop("disabled", true);
                    $("#lineaColor").val('');
                    //$("#opcional").val('');
                }
                else
                    $("#lineaColor").prop("disabled", false);
         
        });

    
$("#opcional").change(function(){
          
    if ($(this).val() == "1") {
           
                    $("#pretina").show();
                    $("#cierre").hide();
                    $("#detalle").hide();
                    $("#vivo").hide();
                    $("#lineaColor").prop("disabled", true);
                    $("#linea").prop("disabled", false);
                    $("#lineaColor").val('');


                /*    $("#linea").prop("disabled", false);
                    
                    $("#pretina").show();
                    $("#cierre").hide();
                }else{
                    $("#linea").prop("disabled", true);
                   
                    $("#linea").val('');
                    $("#lineaColor").val('');
                     $("#pretina").hide();
                    $("#cierre").show();*/
                   }


    if ($(this).val() == "2") {
           
                    $("#pretina").hide();
                    $("#cierre").show();
                    $("#detalle").hide();
                    $("#vivo").hide();
                    $("#lineaColor").prop("disabled", false);
                    $("#linea").val('');
                    $("#lineaColor").val('');

                /*    $("#linea").prop("disabled", false);
                    
                    $("#pretina").show();
                    $("#cierre").hide();
                }else{
                    $("#linea").prop("disabled", true);
                   
                    $("#linea").val('');
                    $("#lineaColor").val('');
                     $("#pretina").hide();
                    $("#cierre").show();*/
                   }

                   if ($(this).val() == "3") {
           
                    $("#pretina").hide();
                    $("#cierre").hide();
                    $("#detalle").hide();
                    $("#vivo").show();
                    $("#lineaColor").prop("disabled", false);
                    $("#linea").val('');
                    $("#lineaColor").val('');

                /*    $("#linea").prop("disabled", false);
                    
                    $("#pretina").show();
                    $("#cierre").hide();
                }else{
                    $("#linea").prop("disabled", true);
                   
                    $("#linea").val('');
                    $("#lineaColor").val('');
                     $("#pretina").hide();
                    $("#cierre").show();*/
                   }
                   
        });



                   
      

        $("#categoria").change(function(){
          
        
       

             if ($(this).val() == "2") {
                    $("#variante").prop("disabled", false);
                }
                else
                    $("#variante").prop("disabled", true);
                    $("#variante").val('');
         
        });
        
        $(".boton-cerrar").click(function(){
        
            $("#miventana").fadeOut("300");
        
        });
    
 $('select[name="categoria"]').on('change',function(){
var  others = $(this).val();
    if(others == "CUE"){           
    $('#disabled_input').removeAttr('disabled');          
     }else{
     $('#disabled_input').attr('disabled','disabled'); 
    }  

  });
   
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'registrardetalle',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            //?c=3&mod=1&numero=Y1202&color=3
                             window.open('detalle?c='+$("#codigoPedido").val()+'&mod='+$("#modelo").val()+'&numero='+$("#numero").val()+'&color'+$("#hidColorBd").val(), '_parent');
                        });
                   }else{
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });


    
$('#modificar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'modificardetalle',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            //?c=3&mod=1&numero=Y1202&color=3
                             window.open('detalle?c='+$("#codigoPedido").val()+'&mod='+$("#modelo").val()+'&numero='+$("#numero").val()+'&color'+$("#hidColorBd").val(), '_parent');
                        });
                   }else{
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
  $('#eliminarId').click(function () {
    
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'eliminardetalle',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
  $(document).ready(function(){
   $('#mostrarImg').on('click',function(){
        var src = $('#cambio').attr('src');
        var img = '<img src="' + src + '" class="img-responsive"/>';
        $('#myModal').modal();
        $('#myModal').on('shown.bs.modal', function(){
            $('#myModal .modal-body').html(img);
        });
        $('#myModal').on('hidden.bs.modal', function(){
            $('#myModal .modal-body').html('');
        });
   });  
})
</script>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            
          </div>
          <div class="modal-body">
            <img src="" name="imagenModal"  id="imagenModal" alt="" height="600" width="550">
          </div>
          <div class="modal-footer">
            <button type="button" id="siCerrar" class="btn btn-default bg-blue" data-dismiss="modal" >Aceptar</button>         
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
