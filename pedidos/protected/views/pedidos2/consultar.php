<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Cliente</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Cliente</a></li>
            <li class="active">Consultar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Consultar Cliente</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
                //var_dump($roles);
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row hide">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Código</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php echo CHtml::hiddenField('codig', $roles['clien_codig'], array('class' => 'form-control', 'placeholder' => "Descripción", 'disabled'=>'true')); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">    
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Nombre o Razón Social</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php echo CHtml::textField('denom',$roles['clien_denom'], array('class' => 'form-control', 'placeholder' => "Nombre o Razón Social", 'disabled'=>'true')); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Tipo de Cliente</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-list"></i></span>
                                <?php 
                                    $sql="SELECT * FROM cliente_tipo";
                                    $result=$conexion->createCommand($sql)->queryAll();
                                    $data=CHtml::listData($result,'tclie_codig','tclie_descr');
                                    echo CHtml::dropDownList('tclie', $roles['tclie_codig'], $data,array('class' => 'form-control', 'placeholder' => "Descripción", 'prompt'=>'Seleccione...','disabled'=>'true')); ?>

                            </div>
                        </div>
                    </div>
                    
                
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Documento de Identidad</label>
                            <div class="row">
                                <div class="col-sm-4">
                                    <?php $sql="SELECT * FROM p_documento_tipo";
                                    $result=$conexion->createCommand($sql)->queryAll();
                                    $data=CHtml::listData($result,'tdocu_codig','tdocu_descr');
                                    echo CHtml::dropDownList('tdocu', $roles['tdocu_codig'], $data,array('class' => 'form-control', 'placeholder' => "Descripción", 'prompt'=>'Seleccione...', 'disabled'=>'true')); ?>
                                </div>
                                <div class="col-sm-8">
                                    <?php echo CHtml::textField('ndocu', $roles['clien_ndocu'], array('class' => 'form-control', 'placeholder' => "Número de Documento", 'disabled'=>'true')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row hide">
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Número de Contrato</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-archive"></i></span>
                                <?php echo CHtml::textField('ccont',  $roles['clien_ccont'], array('class' => 'form-control', 'placeholder' => "Número de Contrato",'disabled'=>'true')); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">        
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Correo Electrónico</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <?php 
                                    echo CHtml::textField('corre',  $roles['clien_corre'], array('class' => 'form-control', 'placeholder' => "Correo Electrónico",'disabled'=>'true')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Teléfono</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-archive"></i></span>
                                <?php echo CHtml::textField('telef',  $roles['clien_telef'], array('class' => 'form-control', 'placeholder' => "Teléfono",'disabled'=>'true')); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Dirección</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-home"></i></span>
                                <?php 

                                    echo CHtml::textArea('direc',  $roles['clien_direc'], array('class' => 'form-control', 'placeholder' => "Dirección",'disabled'=>'true')); ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Observaciones</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php 

                                    echo CHtml::textArea('obser',  $roles['clien_obser'], array('class' => 'form-control', 'placeholder' => "Observaciones",'disabled'=>'true')); ?>

                            </div>
                        </div>
                    </div>
                    
                </div>
                
                <!-- Button -->
                <div class="row controls">
                    <div class="col-sm-12 ">
                        <a href="listado" type="reset" class="btn-block btn btn-info">Volver </a>
                    </div>
                </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
<script>
$('#punid').mask('#.##0,00',{reverse: true,maxlength:false});
$('#desde').mask('00/00/0000');
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                descr: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Descripción" es obligatorio',
                        }
                    }
                },
                codig: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Código" es obligatorio',
                        }
                    }
                },
                punid: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Precio por Unidad" es obligatorio',
                        }
                    }
                },
                canti: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "cantidad" es obligatorio',
                        }
                    }
                },
                tunid: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Unidad" es obligatorio',
                        }
                    }
                },
                moned: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Moneda" es obligatorio',
                        }
                    }
                },
                /*obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Observaciones" es obligatorio',
                        }
                    }
                },*/



            }
        });
    });

</script>