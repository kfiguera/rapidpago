-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.17-log - MySQL Community Server (GPL)
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para bordados_pedidos
CREATE DATABASE IF NOT EXISTS `bordados_pedidos` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `bordados_pedidos`;

-- Volcando estructura para tabla bordados_pedidos.banco
CREATE TABLE IF NOT EXISTS `banco` (
  `banco_codig` int(11) NOT NULL AUTO_INCREMENT,
  `banco_descr` varchar(45) DEFAULT NULL,
  `banco_value` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`banco_codig`),
  UNIQUE KEY `banco_descr_UNIQUE` (`banco_descr`),
  UNIQUE KEY `banco_value_UNIQUE` (`banco_value`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.banco: ~32 rows (aproximadamente)
/*!40000 ALTER TABLE `banco` DISABLE KEYS */;
INSERT INTO `banco` (`banco_codig`, `banco_descr`, `banco_value`) VALUES
	(1, 'BANESCO BANCO UNIVERSAL S.A.C.A.', '0134'),
	(2, 'BANCO PROVINCIAL, S.A. BANCO UNIVERSAL.', '0108'),
	(35, 'BANCO CENTRAL DE VENEZUELA.', '0001'),
	(36, 'BANCO INDUSTRIAL DE VENEZUELA, C.A. BANCO UNI', '0003'),
	(37, 'BANCO DE VENEZUELA S.A.C.A. BANCO UNIVERSAL.', '0102'),
	(38, 'VENEZOLANO DE CRÉDITO, S.A. BANCO UNIVERSAL.', '0104'),
	(39, 'BANCO MERCANTIL, C.A S.A.C.A. BANCO UNIVERSAL', '0105'),
	(40, 'BANCARIBE C.A. BANCO UNIVERSAL.', '0114'),
	(41, 'BANCO EXTERIOR C.A. BANCO UNIVERSAL.', '0115'),
	(42, 'BANCO OCCIDENTAL DE DESCUENTO, BANCO UNIVERSA', '0116'),
	(43, 'BANCO CARONÍ C.A. BANCO UNIVERSAL.', '0128'),
	(44, 'BANCO SOFITASA BANCO UNIVERSAL', '0137'),
	(45, 'BANCO PLAZA BANCO UNIVERSAL', '0138'),
	(46, 'BANCO DE LA GENTE EMPRENDEDORA C.A.', '0146'),
	(47, 'BANCO DEL PUEBLO SOBERANO, C.A. BANCO DE DESA', '0149'),
	(48, 'BFC BANCO FONDO COMÚN C.A BANCO UNIVERSAL', '0151'),
	(49, '100% BANCO, BANCO UNIVERSAL C.A.', '0156'),
	(50, 'DELSUR BANCO UNIVERSAL, C.A.', '0157'),
	(51, 'BANCO DEL TESORO, C.A. BANCO UNIVERSAL', '0163'),
	(52, 'BANCO AGRÍCOLA DE VENEZUELA, C.A. BANCO UNIVE', '0166'),
	(53, 'BANCRECER, S.A. BANCO MICROFINANCIERO', '0168'),
	(54, 'MI BANCO BANCO MICROFINANCIERO C.A.', '0169'),
	(55, 'BANCO ACTIVO, C.A. BANCO UNIVERSAL', '0171'),
	(56, 'BANCAMIGA BANCO MICROFINANCIERO C.A.', '0172'),
	(57, 'BANCO INTERNACIONAL DE DESARROLLO, C.A. BANCO', '0173'),
	(58, 'BANPLUS BANCO UNIVERSAL, C.A.', '0174'),
	(59, 'BANCO BICENTENARIO BANCO UNIVERSAL C.A.', '0175'),
	(60, 'BANCO ESPIRITO SANTO, S.A. SUCURSAL VENEZUELA', '0176'),
	(61, 'BANCO DE LA FUERZA ARMADA NACIONAL BOLIVARIAN', '0177'),
	(62, 'CITIBANK N.A.', '0190'),
	(63, 'BANCO NACIONAL DE CRÉDITO, C.A. BANCO UNIVERS', '0191'),
	(64, 'INSTITUTO MUNICIPAL DE CRÉDITO POPULAR', '0601');
/*!40000 ALTER TABLE `banco` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.banco_tipo_cuenta
CREATE TABLE IF NOT EXISTS `banco_tipo_cuenta` (
  `tcuen_codig` int(11) NOT NULL AUTO_INCREMENT,
  `tcuen_descr` varchar(45) NOT NULL,
  PRIMARY KEY (`tcuen_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.banco_tipo_cuenta: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `banco_tipo_cuenta` DISABLE KEYS */;
INSERT INTO `banco_tipo_cuenta` (`tcuen_codig`, `tcuen_descr`) VALUES
	(1, 'CORRIENTE'),
	(2, 'AHORRO');
/*!40000 ALTER TABLE `banco_tipo_cuenta` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.cliente
CREATE TABLE IF NOT EXISTS `cliente` (
  `clien_codig` int(11) NOT NULL AUTO_INCREMENT,
  `tclie_codig` int(11) NOT NULL,
  `tdocu_codig` int(11) NOT NULL,
  `clien_ndocu` varchar(20) NOT NULL,
  `clien_denom` varchar(200) NOT NULL,
  `clien_ccont` varchar(20) NOT NULL,
  `clien_corre` varchar(50) NOT NULL,
  `clien_telef` varchar(15) NOT NULL,
  `clien_direc` text NOT NULL,
  `clien_obser` text NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `clien_fcrea` date NOT NULL,
  `clien_hcrea` time NOT NULL,
  PRIMARY KEY (`clien_codig`),
  KEY `tclie_codig` (`tclie_codig`),
  KEY `tdocu_codig` (`tdocu_codig`),
  KEY `usuar_codig` (`usuar_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.cliente: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` (`clien_codig`, `tclie_codig`, `tdocu_codig`, `clien_ndocu`, `clien_denom`, `clien_ccont`, `clien_corre`, `clien_telef`, `clien_direc`, `clien_obser`, `usuar_codig`, `clien_fcrea`, `clien_hcrea`) VALUES
	(3, 1, 1, '123456', 'KEVIN FIGUERA', '12345', 'A@A.A', '12345', '1234', '', 3, '2018-01-22', '08:13:57'),
	(4, 1, 1, '24900845', 'KEVIN FIGUERA', '', 'KEVINALEJANDRO3@GMAIL.COM', '4241941881', 'MONTALBAN', 'NINGUNO', 3, '2018-08-01', '19:36:54'),
	(5, 1, 1, '13882284', 'IVEY MONTOYA', '', 'IVEYMONTOYA@GMAIL.COM', '5555555555', 'CALLE NEGRIN CON GARCIA LOS ALAMOS ', 'OTRO BANCO', 3, '2018-08-07', '22:13:40'),
	(6, 1, 1, '24321991', 'KARLA NADALES', '', 'KARLANADALES12@GMAIL.COM', '04141111111', 'BARINAS', '2', 3, '2018-08-08', '05:24:10'),
	(9, 1, 1, '18390585', 'DANNY MARIN', '', 'DMARIN@GMAIL.COM', '02121112222', 'A', '1', 3, '2018-08-08', '05:27:45'),
	(12, 2, 4, '309386310', 'JVKA REDES Y SISTEMAS CA', '', 'KEVINALEJANDRO3@GMAIL.COM', '4241941881', 'MONTABAN', 'NINGUNA', 3, '2018-08-28', '05:23:26');
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.cliente_tipo
CREATE TABLE IF NOT EXISTS `cliente_tipo` (
  `tclie_codig` int(11) NOT NULL AUTO_INCREMENT,
  `tclie_descr` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`tclie_codig`),
  UNIQUE KEY `tclie_descr_UNIQUE` (`tclie_descr`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.cliente_tipo: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `cliente_tipo` DISABLE KEYS */;
INSERT INTO `cliente_tipo` (`tclie_codig`, `tclie_descr`) VALUES
	(2, 'JURIDICO'),
	(1, 'NATURAL');
/*!40000 ALTER TABLE `cliente_tipo` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.cobranza_tipo
CREATE TABLE IF NOT EXISTS `cobranza_tipo` (
  `ctipo_codig` int(11) NOT NULL AUTO_INCREMENT,
  `ctipo_descr` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ctipo_codig`),
  UNIQUE KEY `ctipo_descr_UNIQUE` (`ctipo_descr`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.cobranza_tipo: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `cobranza_tipo` DISABLE KEYS */;
INSERT INTO `cobranza_tipo` (`ctipo_codig`, `ctipo_descr`) VALUES
	(2, 'CONCEPTO 2');
/*!40000 ALTER TABLE `cobranza_tipo` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.colegio
CREATE TABLE IF NOT EXISTS `colegio` (
  `coleg_codig` int(11) NOT NULL AUTO_INCREMENT,
  `coleg_nombr` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `coleg_direc` text COLLATE utf8mb4_bin NOT NULL,
  `colec_telef` varchar(20) COLLATE utf8mb4_bin NOT NULL,
  `coleg_pgweb` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `coleg_rede1` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `coleg_rede2` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `coleg_rede3` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `coleg_obser` text COLLATE utf8mb4_bin NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `coleg_fcrea` date NOT NULL,
  `coleg_hcrea` time NOT NULL,
  PRIMARY KEY (`coleg_codig`),
  UNIQUE KEY `coleg_nombr` (`coleg_nombr`),
  KEY `usuar_codig` (`usuar_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Volcando datos para la tabla bordados_pedidos.colegio: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `colegio` DISABLE KEYS */;
INSERT INTO `colegio` (`coleg_codig`, `coleg_nombr`, `coleg_direc`, `colec_telef`, `coleg_pgweb`, `coleg_rede1`, `coleg_rede2`, `coleg_rede3`, `coleg_obser`, `usuar_codig`, `coleg_fcrea`, `coleg_hcrea`) VALUES
	(2, 'COLEGIO LAS ACACIAS', 'CARACS', '02125554433', '', '', '', '', '', 3, '2018-09-17', '21:15:59');
/*!40000 ALTER TABLE `colegio` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.colegio_contacto
CREATE TABLE IF NOT EXISTS `colegio_contacto` (
  `ccole_codig` int(11) NOT NULL AUTO_INCREMENT,
  `coleg_codig` int(11) NOT NULL,
  `perso_codig` int(11) NOT NULL,
  `ccole_tipo` int(11) NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `ccole_fcrea` date NOT NULL,
  `ccole_hcrea` time NOT NULL,
  PRIMARY KEY (`ccole_codig`),
  UNIQUE KEY `coleg_codig_2` (`coleg_codig`,`perso_codig`),
  KEY `coleg_codig` (`coleg_codig`),
  KEY `perso_codig` (`perso_codig`),
  KEY `usuar_codig` (`usuar_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Volcando datos para la tabla bordados_pedidos.colegio_contacto: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `colegio_contacto` DISABLE KEYS */;
INSERT INTO `colegio_contacto` (`ccole_codig`, `coleg_codig`, `perso_codig`, `ccole_tipo`, `usuar_codig`, `ccole_fcrea`, `ccole_hcrea`) VALUES
	(2, 2, 68, 1, 3, '2018-09-17', '21:15:59'),
	(3, 2, 33, 2, 3, '2018-09-17', '21:15:59');
/*!40000 ALTER TABLE `colegio_contacto` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.depositos
CREATE TABLE IF NOT EXISTS `depositos` (
  `depos_codig` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `depos_solic` int(11) NOT NULL,
  `depos_ndepo` varchar(20) NOT NULL,
  `depos_fdepo` date NOT NULL,
  `depos_monto` decimal(10,0) NOT NULL,
  `depos_fregi` date NOT NULL,
  `depos_statu` int(11) NOT NULL,
  `depos_obser` text,
  PRIMARY KEY (`depos_codig`),
  UNIQUE KEY `depos_codig` (`depos_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=208 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.depositos: ~173 rows (aproximadamente)
/*!40000 ALTER TABLE `depositos` DISABLE KEYS */;
INSERT INTO `depositos` (`depos_codig`, `depos_solic`, `depos_ndepo`, `depos_fdepo`, `depos_monto`, `depos_fregi`, `depos_statu`, `depos_obser`) VALUES
	(11, 100063, '35506842', '2017-03-09', 60000, '2017-03-09', 1, ''),
	(12, 100079, '00436932', '2017-03-09', 60000, '2017-03-09', 1, 'Muchas Gracias.'),
	(13, 100081, '33457107', '2107-03-09', 60000, '2017-03-09', 1, 'Muchas Gracias.'),
	(15, 100082, '52300482891', '2017-03-09', 10000, '2017-03-09', 1, 'confirmado'),
	(16, 100083, '52300483040', '2017-03-09', 10000, '2017-03-09', 1, 'confirmado'),
	(17, 100084, '52300483141', '2017-03-09', 10000, '2017-03-09', 2, '1'),
	(19, 100076, '52300711141', '2017-03-12', 60000, '2017-03-12', 1, 'Muchas Gracias.'),
	(20, 100069, '0839347437', '2017-03-13', 60000, '2017-03-13', 1, 'Muchas Gracias.'),
	(21, 100093, '0003130893', '2017-03-14', 60000, '2017-03-14', 1, 'Muchas Gracias.'),
	(22, 100109, '0840536777', '2017-03-14', 60000, '2017-03-14', 1, 'Muchas Gracias.'),
	(23, 100118, '0841142967', '2017-03-15', 60000, '2017-03-15', 1, 'Muchas Gracias.'),
	(36, 100071, '52300399128', '2017-03-16', 60000, '2017-03-16', 0, NULL),
	(41, 100145, '0003148346', '2017-03-17', 60000, '2017-03-17', 1, 'Muchas Gracias.'),
	(42, 100064, '0843479337', '2017-03-17', 60000, '2017-03-17', 1, 'Muchas Gracias.'),
	(43, 100137, '0843649432', '2017-03-17', 10000, '2017-03-17', 1, 'Muchas Gracias.'),
	(44, 100136, '0843640359', '2017-03-17', 10000, '2017-03-17', 1, 'Muchas Gracias.'),
	(45, 100117, '0844979695', '2017-03-19', 60000, '2017-03-19', 1, 'Muchas Gracias.'),
	(46, 100091, '0000101924', '2017-03-20', 60000, '2017-03-20', 1, 'Muchas Gracias.'),
	(47, 100147, '0000991596', '2017-03-21', 600, '2017-03-21', 1, 'Muchas Gracias.'),
	(48, 100156, '00846642936', '2016-03-21', 10000, '2017-03-21', 1, 'Muchas Gracias.'),
	(49, 100138, '11312880182', '2017-03-21', 5000, '2017-03-21', 1, 'Muchas gracias por su participación.'),
	(51, 100132, '0847848894', '2017-03-22', 60000, '2017-03-22', 1, 'Muchas Gracias por su participación.'),
	(55, 100150, '52300283107', '2017-03-23', 60000, '2017-03-23', 1, 'Muchas Gracias por su participación.'),
	(56, 100101, 'TE0004066569', '2017-03-23', 60000, '2017-03-23', 1, 'Muchas Gracias por su inscripción.'),
	(58, 100212, '0064142197', '2017-03-23', 150000, '2017-03-23', 1, 'Muchas gracias por su participación.'),
	(59, 100213, '0064142197', '2017-03-23', 150000, '2017-03-23', 1, 'Muchas gracias por su participación.'),
	(60, 100214, '0064142197', '2017-03-23', 150000, '2017-03-23', 1, 'Muchas gracias por su participación.'),
	(61, 100215, '0064142197', '2017-03-23', 150000, '2017-03-23', 1, 'Muchas gracias por su participación.'),
	(62, 100216, '0064142197', '2017-03-23', 150000, '2017-03-23', 1, 'Muchas gracias por su participación.'),
	(63, 100217, '0064142197', '2017-03-23', 150000, '2017-03-23', 1, 'Muchas gracias por su participación.'),
	(64, 100218, '0064142197', '2017-03-23', 150000, '2017-03-23', 1, 'Muchas gracias por su participación.'),
	(65, 100219, '0064142197', '2017-03-23', 150000, '2017-03-23', 1, 'Muchas gracias por su participación.'),
	(66, 100220, '0064142197', '2017-03-23', 150000, '2017-03-23', 1, 'Muchas Gracias'),
	(67, 100221, '0064142197', '2017-03-23', 150000, '2017-03-23', 1, 'Muchas gracias por su participación.'),
	(68, 100103, '0079309515', '2017-03-23', 25000, '2017-03-23', 0, NULL),
	(69, 100098, '0079307487', '2017-03-23', 25000, '2017-03-23', 0, NULL),
	(70, 100230, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por participar.'),
	(71, 100066, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por participar.'),
	(72, 100116, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por participar.'),
	(73, 100108, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por participar.'),
	(74, 100107, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por participar.'),
	(75, 100106, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por participar.'),
	(76, 100229, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por participar.'),
	(77, 100165, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por participar.'),
	(78, 100164, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por participar.'),
	(79, 100163, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(80, 100161, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(81, 100162, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(82, 100185, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(83, 100182, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(84, 100179, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(85, 100192, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(86, 100187, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(87, 100188, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(88, 100190, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(89, 100189, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(90, 100224, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(91, 100225, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(92, 100226, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(93, 100223, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(94, 100222, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(95, 100228, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(96, 100210, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(97, 100104, '0097313805', '2017-03-23', 10000, '2017-03-24', 0, NULL),
	(98, 100175, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(99, 100177, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(100, 100178, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(101, 100180, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(102, 100181, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(103, 100183, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(104, 100184, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(105, 100186, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(106, 100191, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(107, 100193, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(108, 100194, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(109, 100195, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(110, 100196, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(111, 100197, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(112, 100198, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(113, 100199, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(114, 100200, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(115, 100201, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(116, 100168, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(117, 100169, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(118, 100170, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(119, 100171, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(120, 100172, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(121, 100174, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(122, 100202, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(123, 100209, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(124, 100208, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(125, 100207, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(126, 100206, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(127, 100176, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(128, 100203, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(129, 100204, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(130, 100205, '0', '2017-03-24', 0, '2017-03-24', 3, 'Muchas gracias por su participación.'),
	(131, 100167, '0849885981', '2017-03-24', 10, '2017-03-24', 1, 'Muchas gracias por su participación.'),
	(132, 100166, '0849895153', '2017-03-24', 10, '2017-03-24', 1, 'Muchas gracias por su participación.'),
	(133, 100275, '0097618618', '2017-03-25', 10000, '2017-03-25', 0, NULL),
	(134, 100283, '0850317146', '2017-03-24', 10000, '2017-03-25', 1, 'Muchas gracias por su participación.'),
	(135, 100266, '0097530346', '2017-03-24', 10000, '2017-03-25', 0, NULL),
	(136, 100267, '0097530346', '2017-03-24', 10000, '2017-03-25', 0, NULL),
	(137, 100268, '0074020357', '2017-03-22', 10000, '2017-03-25', 0, NULL),
	(138, 100269, '0000065904', '2017-03-22', 10000, '2017-03-25', 0, NULL),
	(139, 100286, '1113395810', '2017-03-24', 10000, '2017-03-25', 1, 'Muchas gracias por su participación.'),
	(140, 100287, '1113395810', '2017-03-24', 10000, '2017-03-25', 1, 'Muchas gracias por su participación.'),
	(141, 100288, '1113395810', '2017-03-24', 10000, '2017-03-25', 1, 'Muchas gracias por su participación.'),
	(142, 100289, '1113395810', '2017-03-24', 10000, '2017-03-25', 1, 'Muchas gracias por su participación.'),
	(143, 100290, '1113395810', '2017-03-24', 10000, '2017-03-25', 1, 'Muchas gracias por su participación.'),
	(144, 100291, '1113395810', '2017-03-24', 10000, '2017-03-25', 1, 'Muchas gracias por su participación.'),
	(145, 100252, '0850168061', '2017-03-24', 10000, '2017-03-25', 1, 'Muchas gracias por su participación.'),
	(146, 100292, '1113395810', '2017-03-24', 10000, '2017-03-25', 1, 'Muchas gracias por su participación.'),
	(147, 100251, '0850168061', '2017-03-24', 10000, '2017-03-25', 1, 'Muchas gracias por su participación.'),
	(148, 100250, '0850168061', '2017-03-24', 10000, '2017-03-25', 1, 'Muchas gracias por su participación.'),
	(149, 100293, '1113395810', '2017-03-24', 10000, '2017-03-25', 1, 'Muchas gracias por su participación.'),
	(150, 100249, '0850168061', '2017-03-24', 10000, '2017-03-25', 1, 'Muchas gracias por su participación.'),
	(151, 100280, '0097433172', '2017-03-24', 10000, '2017-03-25', 0, NULL),
	(152, 100248, '0850168061', '2017-03-24', 10000, '2017-03-25', 1, 'Muchas gracias por su participación.'),
	(153, 100247, '0850168061', '2017-03-24', 10000, '2017-03-25', 1, 'Muchas gracias por su participación.'),
	(154, 100246, '0850168061', '2017-03-24', 10000, '2017-03-25', 1, 'Muchas gracias por su participación.'),
	(155, 100299, '00851000132', '2017-03-26', 10000, '2017-03-26', 1, 'Muchas Gracias'),
	(156, 100173, '1632342595', '2017-03-24', 10000, '2017-03-27', 1, 'Muchas gracias por su participación.'),
	(157, 100227, '0', '2017-03-27', 0, '2017-03-27', 3, 'Muchas gracias, la participacion sera cambiada para el clasificatorio.'),
	(158, 100092, '0', '2017-03-27', 0, '2017-03-27', 3, 'Muchas gracias por su participación.'),
	(159, 100160, '0', '2017-03-27', 0, '2017-03-27', 3, 'Muchas gracias por su participación'),
	(160, 100241, '0', '2017-03-27', 0, '2017-03-27', 3, 'Muchas gracias por su participación'),
	(161, 100244, '0', '2017-03-27', 0, '2017-03-27', 3, 'Muchas gracias por su participación'),
	(162, 100243, '0', '2017-03-27', 0, '2017-03-27', 3, 'Muchas gracias por su participación'),
	(163, 100253, '0', '2017-03-27', 0, '2017-03-27', 3, 'Muchas gracias por su participación'),
	(164, 100242, '0', '2017-03-27', 0, '2017-03-27', 3, 'Muchas gracias por su participación'),
	(165, 100254, '0', '2017-03-27', 0, '2017-03-27', 3, 'Muchas gracias por su participación'),
	(166, 100240, '0', '2017-03-27', 0, '2017-03-27', 3, 'Muchas gracias por su participación'),
	(167, 100234, '0', '2017-03-27', 0, '2017-03-27', 3, 'Muchas gracias por su participación'),
	(168, 100239, '0', '2017-03-27', 0, '2017-03-27', 3, 'Muchas gracias por su participación'),
	(169, 100238, '0', '2017-03-27', 0, '2017-03-27', 3, 'Muchas gracias por su participación'),
	(170, 100237, '0', '2017-03-27', 0, '2017-03-27', 3, 'Muchas gracias por su participación'),
	(171, 100236, '0', '2017-03-27', 0, '2017-03-27', 3, 'Muchas gracias por su participación'),
	(172, 100235, '0', '2017-03-27', 0, '2017-03-27', 3, 'Muchas gracias por su participación'),
	(173, 100298, '0', '2017-03-27', 0, '2017-03-27', 3, 'Muchas gracias por su participación'),
	(174, 100297, '0', '2017-03-27', 0, '2017-03-27', 3, 'Gracias por su participación.'),
	(175, 100296, '0', '2017-03-27', 0, '2017-03-27', 3, 'Gracias por su participación.'),
	(176, 100295, '0', '2017-03-27', 0, '2017-03-27', 3, 'Gracias por su participación.'),
	(177, 100294, '0', '2017-03-27', 0, '2017-03-27', 3, 'Gracias por su participación.'),
	(178, 100301, '0', '2017-03-27', 0, '2017-03-27', 3, 'Gracias por su participación.'),
	(179, 100159, '0', '2017-03-27', 0, '2017-03-27', 3, 'Gracias por su participación.'),
	(180, 100264, '0', '2017-03-27', 0, '2017-03-27', 3, 'Gracias por su participación.'),
	(181, 100265, '0', '2017-03-27', 0, '2017-03-27', 3, 'Gracias por su participación.'),
	(182, 100270, '0', '2017-03-27', 0, '2017-03-27', 3, 'Gracias por su participación.'),
	(183, 100272, '0', '2017-03-27', 0, '2017-03-27', 3, 'Gracias por su participación.'),
	(184, 100273, '0', '2017-03-27', 0, '2017-03-27', 3, 'Gracias por su participación.'),
	(185, 100300, '0', '2017-03-27', 0, '2017-03-27', 3, 'Gracias por su participación.'),
	(186, 100271, '0', '2017-03-27', 0, '2017-03-27', 3, 'Gracias por su participación.'),
	(187, 100263, '0', '2017-03-27', 0, '2017-03-27', 3, 'Gracias por su participación.'),
	(188, 100262, '0', '2017-03-27', 0, '2017-03-27', 3, 'Gracias por su participación.'),
	(189, 100261, '0', '2017-03-27', 0, '2017-03-27', 3, 'Gracias por su participación.'),
	(190, 100260, '0', '2017-03-27', 0, '2017-03-27', 3, 'Gracias por su participación.'),
	(191, 100259, '0', '2017-03-27', 0, '2017-03-27', 3, 'Gracias por su participación.'),
	(192, 100258, '0', '2017-03-27', 0, '2017-03-27', 3, 'Gracias por su participación.'),
	(193, 100257, '0', '2017-03-27', 0, '2017-03-27', 3, 'Gracias por su participación.'),
	(194, 100256, '0', '2017-03-27', 0, '2017-03-27', 3, 'Gracias por su participación.'),
	(195, 100255, '0', '2017-03-27', 0, '2017-03-27', 3, 'Gracias por su participación.'),
	(196, 100305, '0000739936', '2017-03-27', 60000, '2017-03-27', 0, NULL),
	(197, 100308, '52300691122', '2017-03-27', 60000, '2017-03-27', 0, NULL),
	(198, 100304, '0851806299', '2017-03-27', 60000, '2017-03-27', 0, NULL),
	(199, 100094, '52300486438', '2017-03-24', 600, '2017-03-27', 0, NULL),
	(200, 100096, '0', '2017-03-27', 0, '2017-03-27', 3, 'Gracias por su participacion'),
	(201, 100233, '0', '2017-03-27', 0, '2017-03-27', 3, 'Gracias por su participacion'),
	(202, 100303, '0097739795', '2017-03-27', 10000, '2017-03-29', 0, NULL),
	(203, 100302, '0097736370', '2017-03-27', 10000, '2017-03-29', 0, NULL),
	(205, 100084, '52300483141', '2017-03-09', 10000, '2017-03-29', 1, 'gracias'),
	(206, 123457, '0', '2017-03-30', 0, '2017-03-30', 3, 'muchas'),
	(207, 123456, '0', '2017-03-30', 0, '2017-03-30', 3, 'gracias');
/*!40000 ALTER TABLE `depositos` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.depositos_validar
CREATE TABLE IF NOT EXISTS `depositos_validar` (
  `valid_codig` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `depos_solic` int(11) NOT NULL,
  `estva_codig` int(11) NOT NULL,
  `valid_obser` text,
  PRIMARY KEY (`valid_codig`),
  UNIQUE KEY `valid_codig` (`valid_codig`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.depositos_validar: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `depositos_validar` DISABLE KEYS */;
/*!40000 ALTER TABLE `depositos_validar` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.p_documento_tipo
CREATE TABLE IF NOT EXISTS `p_documento_tipo` (
  `tdocu_codig` int(11) NOT NULL AUTO_INCREMENT,
  `tdocu_descr` varchar(50) NOT NULL,
  PRIMARY KEY (`tdocu_codig`),
  UNIQUE KEY `tdocu_descr` (`tdocu_descr`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.p_documento_tipo: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `p_documento_tipo` DISABLE KEYS */;
INSERT INTO `p_documento_tipo` (`tdocu_codig`, `tdocu_descr`) VALUES
	(2, 'E'),
	(5, 'G'),
	(4, 'J'),
	(3, 'P'),
	(1, 'V');
/*!40000 ALTER TABLE `p_documento_tipo` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.egresos
CREATE TABLE IF NOT EXISTS `egresos` (
  `egres_codig` int(11) NOT NULL AUTO_INCREMENT,
  `egres_motiv` varchar(20) NOT NULL,
  `banco_codig` int(11) NOT NULL,
  `egres_nfact` int(11) NOT NULL,
  `egres_refer` varchar(20) NOT NULL,
  `egres_monto` double NOT NULL,
  `egres_fegre` date NOT NULL,
  `egres_obser` text NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `egres_fcrea` date NOT NULL,
  `egres_hcrea` time NOT NULL,
  PRIMARY KEY (`egres_codig`),
  KEY `banco_codig` (`banco_codig`),
  KEY `usuar_codig` (`usuar_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.egresos: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `egresos` DISABLE KEYS */;
INSERT INTO `egresos` (`egres_codig`, `egres_motiv`, `banco_codig`, `egres_nfact`, `egres_refer`, `egres_monto`, `egres_fegre`, `egres_obser`, `usuar_codig`, `egres_fcrea`, `egres_hcrea`) VALUES
	(2, 'PAGO A PROVEEDOR', 1, 100, '0', 10000000, '2018-02-04', '', 3, '2018-02-04', '12:11:42'),
	(3, 'PAGO A EMPLEADOS', 1, 0, '100000', 1000000, '2018-02-22', '', 3, '2018-02-04', '12:12:09');
/*!40000 ALTER TABLE `egresos` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.empresa_tipo
CREATE TABLE IF NOT EXISTS `empresa_tipo` (
  `etipo_codig` int(11) NOT NULL AUTO_INCREMENT,
  `etipo_descr` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`etipo_codig`),
  UNIQUE KEY `etipo_value_UNIQUE` (`etipo_descr`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.empresa_tipo: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `empresa_tipo` DISABLE KEYS */;
INSERT INTO `empresa_tipo` (`etipo_codig`, `etipo_descr`) VALUES
	(2, 'TIPO EMPRESA 1'),
	(3, 'VENDEDOR');
/*!40000 ALTER TABLE `empresa_tipo` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.p_estados
CREATE TABLE IF NOT EXISTS `p_estados` (
  `testa_codig` int(11) NOT NULL,
  `testa_descr` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.p_estados: ~25 rows (aproximadamente)
/*!40000 ALTER TABLE `p_estados` DISABLE KEYS */;
INSERT INTO `p_estados` (`testa_codig`, `testa_descr`) VALUES
	(3, 'Apure'),
	(4, 'Aragua'),
	(5, 'Barinas'),
	(7, 'Carabobo'),
	(8, 'Cojedes'),
	(9, 'Delta Amacuro'),
	(12, 'Lara'),
	(14, 'Miranda'),
	(15, 'Monagas'),
	(16, 'Nueva Esparta'),
	(17, 'Portuguesa'),
	(18, 'Sucre'),
	(20, 'Trujillo'),
	(21, 'Vargas'),
	(22, 'Yaracuy'),
	(23, 'Zulia'),
	(24, 'Distrito Capital'),
	(25, 'Dependencias Federales'),
	(2, 'Anzoategui'),
	(10, 'Falcon'),
	(11, 'Guarico'),
	(13, 'Merida'),
	(19, 'Tachira'),
	(6, 'Bolivar'),
	(1, 'Amazonas');
/*!40000 ALTER TABLE `p_estados` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.p_estado_civil
CREATE TABLE IF NOT EXISTS `p_estado_civil` (
  `ecivi_codig` int(11) NOT NULL AUTO_INCREMENT,
  `ecivi_descr` varchar(45) DEFAULT NULL,
  `ecivi_value` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ecivi_codig`),
  UNIQUE KEY `ecivi_descr_UNIQUE` (`ecivi_descr`),
  UNIQUE KEY `ecivi_value_UNIQUE` (`ecivi_value`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.p_estado_civil: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `p_estado_civil` DISABLE KEYS */;
INSERT INTO `p_estado_civil` (`ecivi_codig`, `ecivi_descr`, `ecivi_value`) VALUES
	(1, 'SOLTERO', 'S'),
	(2, 'CASADO', 'C'),
	(3, 'VIUDO', 'V'),
	(4, 'DIVORCIADO', 'D'),
	(5, 'UNION ESTABLE DE HECHO', 'U');
/*!40000 ALTER TABLE `p_estado_civil` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.estatus_validar
CREATE TABLE IF NOT EXISTS `estatus_validar` (
  `estval_codig` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `estva_descr` text NOT NULL,
  PRIMARY KEY (`estval_codig`),
  UNIQUE KEY `estval_codig` (`estval_codig`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.estatus_validar: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `estatus_validar` DISABLE KEYS */;
/*!40000 ALTER TABLE `estatus_validar` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.factura
CREATE TABLE IF NOT EXISTS `factura` (
  `factu_codig` int(11) NOT NULL AUTO_INCREMENT,
  `factu_nfact` int(11) NOT NULL,
  `factu_ncont` varchar(255) NOT NULL,
  `traba_codig` int(11) NOT NULL,
  `clien_codig` int(11) NOT NULL,
  `tvent_codig` int(11) NOT NULL,
  `factu_femis` date NOT NULL,
  `factu_desc` double NOT NULL,
  `factu_monto` double NOT NULL,
  `factu_impue` double NOT NULL,
  `factu_comis` double NOT NULL,
  `iva_codig` int(11) NOT NULL,
  `ptipo_codig` int(11) NOT NULL,
  `efact_codig` int(11) NOT NULL,
  `efpag_codig` int(11) NOT NULL,
  `banco_codig` int(11) NOT NULL,
  `factu_refer` text NOT NULL,
  `factu_obser` text NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `factu_fcrea` date NOT NULL,
  `factu_hcrea` time NOT NULL,
  PRIMARY KEY (`factu_codig`),
  UNIQUE KEY `factu_nfact` (`factu_nfact`),
  KEY `traba_codig` (`traba_codig`),
  KEY `tvent_codig` (`tvent_codig`),
  KEY `ptipo_codig` (`ptipo_codig`),
  KEY `usuar_codig` (`usuar_codig`),
  KEY `clien_codig` (`clien_codig`),
  KEY `efact_codig` (`efact_codig`),
  KEY `banco_codig` (`banco_codig`),
  KEY `iva_codig` (`iva_codig`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.factura: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `factura` DISABLE KEYS */;
INSERT INTO `factura` (`factu_codig`, `factu_nfact`, `factu_ncont`, `traba_codig`, `clien_codig`, `tvent_codig`, `factu_femis`, `factu_desc`, `factu_monto`, `factu_impue`, `factu_comis`, `iva_codig`, `ptipo_codig`, `efact_codig`, `efpag_codig`, `banco_codig`, `factu_refer`, `factu_obser`, `usuar_codig`, `factu_fcrea`, `factu_hcrea`) VALUES
	(29, 1, '1-00', 4, 4, 1, '2018-08-27', 0, 48200, 7712, 24100, 0, 5, 1, 1, 1, '1', 'NINGUNA', 3, '2018-08-27', '23:38:08'),
	(30, 2, '2-00', 4, 4, 1, '2018-08-24', 1000, 15000, 2400, 7500, 2, 5, 1, 1, 35, '2', 'NINGUNO', 3, '2018-08-28', '04:13:07'),
	(33, 3, '3-00', 6, 12, 1, '2018-08-27', 0, 54100, 8656, 541, 0, 4, 1, 1, 36, '3', 'NINGUNA', 3, '2018-08-28', '05:23:26');
/*!40000 ALTER TABLE `factura` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.factura_estatu
CREATE TABLE IF NOT EXISTS `factura_estatu` (
  `efact_codig` int(11) NOT NULL AUTO_INCREMENT,
  `efact_descr` varchar(45) NOT NULL,
  PRIMARY KEY (`efact_codig`),
  UNIQUE KEY `efact_descr` (`efact_descr`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.factura_estatu: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `factura_estatu` DISABLE KEYS */;
INSERT INTO `factura_estatu` (`efact_codig`, `efact_descr`) VALUES
	(3, 'ANULADA'),
	(2, 'APROBADA'),
	(1, 'EMITIDA');
/*!40000 ALTER TABLE `factura_estatu` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.factura_estatu_pago
CREATE TABLE IF NOT EXISTS `factura_estatu_pago` (
  `efpag_codig` int(11) NOT NULL AUTO_INCREMENT,
  `efpag_descr` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`efpag_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla bordados_pedidos.factura_estatu_pago: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `factura_estatu_pago` DISABLE KEYS */;
INSERT INTO `factura_estatu_pago` (`efpag_codig`, `efpag_descr`) VALUES
	(1, 'PAGO'),
	(2, 'PENDIENTE');
/*!40000 ALTER TABLE `factura_estatu_pago` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.factura_producto
CREATE TABLE IF NOT EXISTS `factura_producto` (
  `fprod_codig` int(11) NOT NULL AUTO_INCREMENT,
  `factu_codig` int(11) NOT NULL,
  `tprod_codig` int(11) NOT NULL,
  `inven_codig` int(11) NOT NULL,
  `fprod_canti` int(11) NOT NULL,
  `fprod_preci` double NOT NULL,
  `fprod_monto` double NOT NULL,
  `fprod_obser` text NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `fprod_fcrea` date NOT NULL,
  `fprod_hcrea` time NOT NULL,
  PRIMARY KEY (`fprod_codig`),
  KEY `usuar_codig` (`usuar_codig`),
  KEY `inven_codig` (`inven_codig`),
  KEY `factu_codig` (`factu_codig`),
  KEY `tprod_codig` (`tprod_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.factura_producto: ~8 rows (aproximadamente)
/*!40000 ALTER TABLE `factura_producto` DISABLE KEYS */;
INSERT INTO `factura_producto` (`fprod_codig`, `factu_codig`, `tprod_codig`, `inven_codig`, `fprod_canti`, `fprod_preci`, `fprod_monto`, `fprod_obser`, `usuar_codig`, `fprod_fcrea`, `fprod_hcrea`) VALUES
	(33, 29, 1, 4, 1, 15000, 15000, '', 3, '2018-08-27', '23:38:08'),
	(34, 29, 1, 5, 1, 25000, 25000, '', 3, '2018-08-27', '23:38:08'),
	(35, 29, 1, 6, 2, 4100, 8200, '', 3, '2018-08-27', '23:38:08'),
	(36, 30, 1, 4, 1, 15000, 15000, '', 3, '2018-08-28', '04:13:07'),
	(37, 33, 1, 4, 1, 15000, 15000, '', 3, '2018-08-28', '05:23:26'),
	(38, 33, 1, 5, 1, 25000, 25000, '', 3, '2018-08-28', '05:23:26'),
	(39, 33, 1, 6, 1, 4100, 4100, '', 3, '2018-08-28', '05:23:26'),
	(40, 33, 2, 1, 1, 10000, 10000, '', 3, '2018-08-28', '05:23:26');
/*!40000 ALTER TABLE `factura_producto` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.p_genero
CREATE TABLE IF NOT EXISTS `p_genero` (
  `gener_codig` int(11) NOT NULL AUTO_INCREMENT,
  `gener_descr` varchar(20) NOT NULL,
  `gener_value` varchar(1) NOT NULL,
  PRIMARY KEY (`gener_codig`),
  UNIQUE KEY `gener_value` (`gener_value`),
  UNIQUE KEY `gener_descr` (`gener_descr`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.p_genero: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `p_genero` DISABLE KEYS */;
INSERT INTO `p_genero` (`gener_codig`, `gener_descr`, `gener_value`) VALUES
	(1, 'MASCULINO', 'M'),
	(2, 'FEMENINO', 'F');
/*!40000 ALTER TABLE `p_genero` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.impuesto_iva
CREATE TABLE IF NOT EXISTS `impuesto_iva` (
  `iva_codig` int(11) NOT NULL AUTO_INCREMENT,
  `iva_descr` varchar(255) COLLATE utf8_bin NOT NULL,
  `iva_value` double NOT NULL,
  PRIMARY KEY (`iva_codig`),
  UNIQUE KEY `iva_descr` (`iva_descr`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla bordados_pedidos.impuesto_iva: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `impuesto_iva` DISABLE KEYS */;
INSERT INTO `impuesto_iva` (`iva_codig`, `iva_descr`, `iva_value`) VALUES
	(1, 'IVA 12%', 12),
	(2, 'IVA 16%', 16);
/*!40000 ALTER TABLE `impuesto_iva` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.inventario
CREATE TABLE IF NOT EXISTS `inventario` (
  `inven_codig` int(11) NOT NULL AUTO_INCREMENT,
  `inven_cprod` varchar(45) DEFAULT NULL,
  `inven_descr` varchar(45) DEFAULT NULL,
  `inven_punid` varchar(45) DEFAULT NULL,
  `inven_canti` int(11) DEFAULT NULL,
  `moned_codig` int(11) NOT NULL,
  `tunid_codig` int(11) NOT NULL,
  `inven_obser` varchar(500) DEFAULT NULL,
  `usuar_codig` int(11) NOT NULL,
  `inven_fcrea` date NOT NULL,
  `inven_hcrea` time NOT NULL,
  PRIMARY KEY (`inven_codig`),
  KEY `tunid_codig` (`tunid_codig`),
  KEY `usuar_codig` (`usuar_codig`) USING BTREE,
  KEY `ivent_cprod_UNIQUE` (`inven_cprod`) USING BTREE,
  KEY `moned_codig` (`moned_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.inventario: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `inventario` DISABLE KEYS */;
INSERT INTO `inventario` (`inven_codig`, `inven_cprod`, `inven_descr`, `inven_punid`, `inven_canti`, `moned_codig`, `tunid_codig`, `inven_obser`, `usuar_codig`, `inven_fcrea`, `inven_hcrea`) VALUES
	(4, '000000000000000000001', 'PRODUCTO 1', '15000', 11, 1, 1, 'QWQW', 3, '2018-08-27', '19:31:02'),
	(5, '000000000000000000002', 'PRODUCTO2', '25000', 11, 1, 1, '', 3, '2018-08-27', '22:42:01'),
	(6, '88888888', 'TINTE ', '41', 11, 2, 1, '', 3, '2018-08-27', '23:37:33');
/*!40000 ALTER TABLE `inventario` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.inventario_entrada
CREATE TABLE IF NOT EXISTS `inventario_entrada` (
  `ientr_codig` int(11) NOT NULL AUTO_INCREMENT,
  `prove_codig` int(11) NOT NULL,
  `ientr_nfact` varchar(20) COLLATE utf8_bin NOT NULL,
  `ientr_ncont` varchar(20) COLLATE utf8_bin NOT NULL,
  `ientr_femis` date NOT NULL,
  `ientr_monto` double NOT NULL,
  `ientr_obser` text COLLATE utf8_bin NOT NULL,
  `ieest_codig` int(11) NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `ientr_fcrea` int(11) NOT NULL,
  `ientr_hcrea` int(11) NOT NULL,
  PRIMARY KEY (`ientr_codig`),
  KEY `prove_codig` (`prove_codig`),
  KEY `ieest_codig` (`ieest_codig`),
  CONSTRAINT `inventario_entrada_ibfk_1` FOREIGN KEY (`ieest_codig`) REFERENCES `inventario_entrada_estatus` (`ieest_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla bordados_pedidos.inventario_entrada: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `inventario_entrada` DISABLE KEYS */;
INSERT INTO `inventario_entrada` (`ientr_codig`, `prove_codig`, `ientr_nfact`, `ientr_ncont`, `ientr_femis`, `ientr_monto`, `ientr_obser`, `ieest_codig`, `usuar_codig`, `ientr_fcrea`, `ientr_hcrea`) VALUES
	(6, 19, '10', '10-00', '2018-08-27', 100000, 'A', 2, 3, 2018, 19),
	(7, 19, '12', '12-00', '2018-08-27', 550000, 'PRUEBA 2', 2, 3, 2018, 19);
/*!40000 ALTER TABLE `inventario_entrada` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.inventario_entrada_estatus
CREATE TABLE IF NOT EXISTS `inventario_entrada_estatus` (
  `ieest_codig` int(11) NOT NULL AUTO_INCREMENT,
  `ieest_descr` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`ieest_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla bordados_pedidos.inventario_entrada_estatus: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `inventario_entrada_estatus` DISABLE KEYS */;
INSERT INTO `inventario_entrada_estatus` (`ieest_codig`, `ieest_descr`) VALUES
	(1, 'EMITIDA'),
	(2, 'APROBADA'),
	(3, 'ANULADA');
/*!40000 ALTER TABLE `inventario_entrada_estatus` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.inventario_entrada_producto
CREATE TABLE IF NOT EXISTS `inventario_entrada_producto` (
  `iepro_codig` int(11) NOT NULL AUTO_INCREMENT,
  `ientr_codig` int(11) NOT NULL,
  `inven_codig` int(11) NOT NULL,
  `iepro_canti` int(11) NOT NULL,
  `iepro_preci` double NOT NULL,
  `iepro_monto` double NOT NULL,
  `iepro_obser` text NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `iepro_fcrea` date NOT NULL,
  `iepro_hcrea` time NOT NULL,
  PRIMARY KEY (`iepro_codig`),
  KEY `usuar_codig` (`usuar_codig`),
  KEY `inven_codig` (`inven_codig`),
  KEY `ientr_codig` (`ientr_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.inventario_entrada_producto: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `inventario_entrada_producto` DISABLE KEYS */;
INSERT INTO `inventario_entrada_producto` (`iepro_codig`, `ientr_codig`, `inven_codig`, `iepro_canti`, `iepro_preci`, `iepro_monto`, `iepro_obser`, `usuar_codig`, `iepro_fcrea`, `iepro_hcrea`) VALUES
	(34, 6, 4, 10, 10000, 100000, '', 3, '2018-08-27', '19:42:04'),
	(35, 7, 5, 10, 20000, 200000, '', 3, '2018-08-27', '19:59:37'),
	(36, 7, 6, 10, 35000, 350000, '', 3, '2018-08-27', '20:02:20');
/*!40000 ALTER TABLE `inventario_entrada_producto` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.inventario_salida
CREATE TABLE IF NOT EXISTS `inventario_salida` (
  `isali_codig` int(11) NOT NULL AUTO_INCREMENT,
  `traba_codig` int(11) NOT NULL,
  `isali_femis` date NOT NULL,
  `isali_monto` double NOT NULL,
  `isali_obser` text COLLATE utf8_bin NOT NULL,
  `isest_codig` int(11) NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `isali_fcrea` int(11) NOT NULL,
  `isali_hcrea` int(11) NOT NULL,
  PRIMARY KEY (`isali_codig`),
  KEY `prove_codig` (`traba_codig`),
  KEY `isest_codig` (`isest_codig`),
  CONSTRAINT `inventario_salida_ibfk_1` FOREIGN KEY (`isest_codig`) REFERENCES `inventario_salida_estatus` (`isest_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla bordados_pedidos.inventario_salida: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `inventario_salida` DISABLE KEYS */;
INSERT INTO `inventario_salida` (`isali_codig`, `traba_codig`, `isali_femis`, `isali_monto`, `isali_obser`, `isest_codig`, `usuar_codig`, `isali_fcrea`, `isali_hcrea`) VALUES
	(8, 4, '2018-08-27', 10000, 'ENTREGA DE MERCANCIA PARA TRABAJAR', 1, 3, 2018, 21),
	(9, 4, '2018-08-27', 10000, 'ENTREGA DE MERCANCIA PARA TRABAJAR', 1, 3, 2018, 21);
/*!40000 ALTER TABLE `inventario_salida` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.inventario_salida_estatus
CREATE TABLE IF NOT EXISTS `inventario_salida_estatus` (
  `isest_codig` int(11) NOT NULL AUTO_INCREMENT,
  `isest_descr` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`isest_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla bordados_pedidos.inventario_salida_estatus: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `inventario_salida_estatus` DISABLE KEYS */;
INSERT INTO `inventario_salida_estatus` (`isest_codig`, `isest_descr`) VALUES
	(1, 'EMITIDA'),
	(2, 'APROBADA'),
	(3, 'ANULADA');
/*!40000 ALTER TABLE `inventario_salida_estatus` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.inventario_salida_producto
CREATE TABLE IF NOT EXISTS `inventario_salida_producto` (
  `ispro_codig` int(11) NOT NULL AUTO_INCREMENT,
  `isali_codig` int(11) NOT NULL,
  `inven_codig` int(11) NOT NULL,
  `ispro_canti` int(11) NOT NULL,
  `ispro_preci` double NOT NULL,
  `ispro_monto` double NOT NULL,
  `ispro_obser` text NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `ispro_fcrea` date NOT NULL,
  `ispro_hcrea` time NOT NULL,
  PRIMARY KEY (`ispro_codig`),
  KEY `usuar_codig` (`usuar_codig`),
  KEY `inven_codig` (`inven_codig`),
  KEY `isali_codig` (`isali_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.inventario_salida_producto: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `inventario_salida_producto` DISABLE KEYS */;
INSERT INTO `inventario_salida_producto` (`ispro_codig`, `isali_codig`, `inven_codig`, `ispro_canti`, `ispro_preci`, `ispro_monto`, `ispro_obser`, `usuar_codig`, `ispro_fcrea`, `ispro_hcrea`) VALUES
	(39, 8, 4, 1, 10000, 10000, '', 3, '2018-08-27', '21:40:04'),
	(40, 9, 4, 1, 10000, 10000, '', 3, '2018-08-27', '21:40:11');
/*!40000 ALTER TABLE `inventario_salida_producto` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.meses
CREATE TABLE IF NOT EXISTS `meses` (
  `meses_codig` int(11) NOT NULL AUTO_INCREMENT,
  `meses_descr` varchar(50) NOT NULL,
  PRIMARY KEY (`meses_codig`),
  UNIQUE KEY `meses_descr` (`meses_descr`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.meses: ~12 rows (aproximadamente)
/*!40000 ALTER TABLE `meses` DISABLE KEYS */;
INSERT INTO `meses` (`meses_codig`, `meses_descr`) VALUES
	(4, 'ABRIL'),
	(8, 'AGOSTO'),
	(12, 'DICIEMBRE'),
	(1, 'ENERO'),
	(2, 'FEBRERO'),
	(7, 'JULIO'),
	(6, 'JUNIO'),
	(3, 'MARZO'),
	(5, 'MAYO'),
	(11, 'NOVIEMBRE'),
	(10, 'OCTUBRE'),
	(9, 'SEPTIEMBRE');
/*!40000 ALTER TABLE `meses` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.modulos
CREATE TABLE IF NOT EXISTS `modulos` (
  `modul_codig` int(10) NOT NULL AUTO_INCREMENT,
  `modul_padre` int(10) NOT NULL,
  `modul_descr` varchar(100) NOT NULL,
  `modul_ruta` varchar(400) NOT NULL,
  `modul_orden` int(11) NOT NULL,
  `modul_bborr` tinyint(1) NOT NULL,
  `modul_bpadr` tinyint(1) NOT NULL,
  `modul_obser` varchar(100) NOT NULL,
  `modul_icono` varchar(100) NOT NULL,
  PRIMARY KEY (`modul_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=902 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.modulos: ~36 rows (aproximadamente)
/*!40000 ALTER TABLE `modulos` DISABLE KEYS */;
INSERT INTO `modulos` (`modul_codig`, `modul_padre`, `modul_descr`, `modul_ruta`, `modul_orden`, `modul_bborr`, `modul_bpadr`, `modul_obser`, `modul_icono`) VALUES
	(1, 0, 'Inicio', '/site/inicio', 1, 0, 0, '', 'fa fa-home'),
	(2, 0, 'Registro', '/registro/listado', 2, 0, 0, '', 'fa fa-edit'),
	(100, 0, 'Parametros', 'Parametros', 1000, 0, 1, '', 'fa fa-gears'),
	(101, 100, 'Persona', '/parametros/p_persona/listado', 1001, 0, 0, '', 'fa fa-user-plus'),
	(102, 100, 'Usuario', '/parametros/usuario/listado', 1002, 0, 0, '', 'fa fa-user'),
	(103, 100, 'Tipo de Usuario', '/parametros/tipousuario/listado', 1003, 0, 0, '', 'fa fa-users'),
	(104, 100, 'Tipo de Empresa', '/parametros/tipoempresa/listado', 1004, 1, 0, '', 'fa fa-building'),
	(105, 100, 'Tipo Trabajador', '/parametros/tipop_trabajador/listado', 1005, 0, 0, '', 'fa fa-list'),
	(106, 100, 'Moneda', '/parametros/p_moneda/listado', 1006, 0, 0, ' ', 'fa fa-money'),
	(107, 100, 'Tipo de Pago', '/parametros/tipopago/listado', 1007, 0, 0, '', 'fa fa-list'),
	(108, 100, 'Banco', '/parametros/banco/listado', 1008, 0, 0, '', 'fa fa-bank'),
	(109, 100, 'Concepto de Cobranza', '/parametros/cobranza/listado', 1009, 0, 1, '', 'fa fa-list'),
	(110, 100, 'Tipo de Cliente', '/parametros/tipocliente/listado', 1010, 0, 0, '', 'fa fa-list'),
	(111, 100, 'Estado Civil', '/parametros/p_estadoscivil/listado', 1011, 0, 0, '', 'fa fa-list'),
	(112, 100, 'Tipo de Unidad', '/parametros/tipounidad/listado', 1012, 0, 0, '', 'fa fa-list'),
	(113, 100, 'Impuesto del Valor Agregado', '/parametros/iva/listado', 1013, 0, 0, '', 'fa fa-bank'),
	(114, 100, 'Modulos', '/parametros/modulos/listado', 1013, 0, 0, '', 'fa fa-gear'),
	(115, 100, 'Permisos', '/parametros/permisos', 1015, 0, 0, '', 'fa fa-users'),
	(116, 100, 'Prueba', '/parametros/prueba', 1015, 0, 0, 'MODULO DE PRUEBA', 'fa fa-circle-o'),
	(200, 0, 'Inventario', '/Inventarios/listado', 300, 0, 1, '', 'fa fa-list'),
	(201, 200, 'Entrada', '/Inventario/Entrada/listado', 201, 0, 0, '', 'fa fa-sign-in'),
	(202, 200, 'Salidas', '/Inventario/Salidas/listado', 202, 0, 0, '', 'fa fa-sign-out'),
	(203, 200, 'Articulos', '/Inventario/Articulos/listado', 203, 0, 0, '', 'fa fa-archive'),
	(300, 0, 'Trabajador', '/p_trabajador/listado', 500, 0, 0, '', 'fa fa-users'),
	(400, 0, 'Cliente', '/cliente/listado', 100, 0, 0, '', 'fa fa-users'),
	(500, 0, 'Servicios', '/servicios/listado', 400, 0, 0, '', 'fa fa-list'),
	(600, 0, 'Factura', '/ventas/factura/listado', 200, 0, 0, '', 'fa fa-dollar'),
	(700, 0, 'Reportes', 'Reportes', 700, 0, 1, '', 'fa fa-file-pdf-o'),
	(701, 700, 'Relacion de Ventas', '/reportes/ventas/listado', 701, 0, 0, '', ''),
	(702, 700, 'Reporte de Egresos', '/reportes/egresos/listado', 702, 0, 0, '', 'fa fa-file-pdf-o'),
	(703, 700, 'Relación de Ingresos - Egresos', '/reportes/relacion/listado', 703, 0, 0, '', 'fa fa-file-pdf-o'),
	(800, 0, 'Gastos', 'Gastos', 500, 0, 1, '', 'fa fa-money'),
	(801, 800, 'Egresos', '/gastos/egresos/listado', 501, 0, 0, '', 'fa fa-money'),
	(802, 800, 'Nomina', '/gastos/nomina/listado', 502, 0, 0, '', 'fa fa-users'),
	(900, 0, 'Cambiar Contraseña', '/parametros/usuario/cambiarclave', 9000, 0, 0, '', 'fa fa-lock'),
	(901, 0, 'Pedidos', '/pedidos/listado', 101, 0, 0, '', 'fa fa-list');
/*!40000 ALTER TABLE `modulos` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.modulos_estatus
CREATE TABLE IF NOT EXISTS `modulos_estatus` (
  `emodu_codig` int(11) NOT NULL AUTO_INCREMENT,
  `emodu_descr` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`emodu_codig`),
  UNIQUE KEY `emodu_descr` (`emodu_descr`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Volcando datos para la tabla bordados_pedidos.modulos_estatus: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `modulos_estatus` DISABLE KEYS */;
INSERT INTO `modulos_estatus` (`emodu_codig`, `emodu_descr`) VALUES
	(0, 'ACTIVO'),
	(1, 'INACTIVO');
/*!40000 ALTER TABLE `modulos_estatus` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.p_moneda
CREATE TABLE IF NOT EXISTS `p_moneda` (
  `moned_codig` int(11) NOT NULL AUTO_INCREMENT,
  `moned_descr` varchar(20) NOT NULL,
  `moned_value` varchar(10) NOT NULL,
  `moned_cambi` decimal(10,0) NOT NULL,
  PRIMARY KEY (`moned_codig`),
  UNIQUE KEY `moned_descr` (`moned_descr`),
  UNIQUE KEY `moned_value` (`moned_value`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.p_moneda: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `p_moneda` DISABLE KEYS */;
INSERT INTO `p_moneda` (`moned_codig`, `moned_descr`, `moned_value`, `moned_cambi`) VALUES
	(1, 'BOLIVARES', 'BS', 1),
	(2, 'DOLAR', 'USD', 100);
/*!40000 ALTER TABLE `p_moneda` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.p_nacionalidad
CREATE TABLE IF NOT EXISTS `p_nacionalidad` (
  `nacio_codig` int(11) NOT NULL AUTO_INCREMENT,
  `nacio_descr` varchar(20) NOT NULL,
  `nacio_value` varchar(1) NOT NULL,
  PRIMARY KEY (`nacio_codig`),
  UNIQUE KEY `nacio_descr` (`nacio_descr`),
  UNIQUE KEY `nacio_value` (`nacio_value`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.p_nacionalidad: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `p_nacionalidad` DISABLE KEYS */;
INSERT INTO `p_nacionalidad` (`nacio_codig`, `nacio_descr`, `nacio_value`) VALUES
	(1, 'VENEZOLANO(A)', 'V'),
	(2, 'EXTRANJERO(A)', 'E');
/*!40000 ALTER TABLE `p_nacionalidad` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.nomina
CREATE TABLE IF NOT EXISTS `nomina` (
  `nomin_codig` int(11) NOT NULL AUTO_INCREMENT,
  `nomin_descr` varchar(50) NOT NULL,
  `enomi_codig` int(11) NOT NULL,
  `tnomi_codig` int(11) NOT NULL,
  `pnomi_codig` int(11) NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `nomin_fcrea` date NOT NULL,
  `nomin_hcrea` time NOT NULL,
  PRIMARY KEY (`nomin_codig`),
  UNIQUE KEY `nomin_descr` (`nomin_descr`),
  KEY `enomi_codig` (`enomi_codig`),
  KEY `tnomi_codig` (`tnomi_codig`),
  KEY `pnomi_codig` (`pnomi_codig`),
  KEY `usuar_codig` (`usuar_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.nomina: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `nomina` DISABLE KEYS */;
INSERT INTO `nomina` (`nomin_codig`, `nomin_descr`, `enomi_codig`, `tnomi_codig`, `pnomi_codig`, `usuar_codig`, `nomin_fcrea`, `nomin_hcrea`) VALUES
	(1, 'EMPLEADOS', 1, 1, 1, 3, '2018-03-18', '22:47:00');
/*!40000 ALTER TABLE `nomina` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.nomina_asignacion
CREATE TABLE IF NOT EXISTS `nomina_asignacion` (
  `asign_codig` int(11) NOT NULL AUTO_INCREMENT,
  `nomin_codig` int(11) NOT NULL,
  `perio_codig` int(11) NOT NULL,
  `traba_codig` int(11) NOT NULL,
  `asing_dtrab` double NOT NULL,
  `asign_dferi` double NOT NULL,
  `asign_cdfer` double NOT NULL,
  `asign_hextr` double NOT NULL,
  `asign_chext` double NOT NULL,
  `asign_bonif` double NOT NULL,
  `asign_balim` double NOT NULL,
  `asign_ntrab` double NOT NULL,
  `asign_cntra` double NOT NULL,
  `asign_obser` text NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `asign_fcrea` date NOT NULL,
  `asign_hcrea` time NOT NULL,
  PRIMARY KEY (`asign_codig`),
  UNIQUE KEY `nomin_codig_2` (`nomin_codig`,`perio_codig`,`traba_codig`),
  KEY `nomin_codig` (`nomin_codig`),
  KEY `perio_codig` (`perio_codig`),
  KEY `traba_codig` (`traba_codig`),
  KEY `usuar_codig` (`usuar_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.nomina_asignacion: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `nomina_asignacion` DISABLE KEYS */;
INSERT INTO `nomina_asignacion` (`asign_codig`, `nomin_codig`, `perio_codig`, `traba_codig`, `asing_dtrab`, `asign_dferi`, `asign_cdfer`, `asign_hextr`, `asign_chext`, `asign_bonif`, `asign_balim`, `asign_ntrab`, `asign_cntra`, `asign_obser`, `usuar_codig`, `asign_fcrea`, `asign_hcrea`) VALUES
	(13, 1, 14, 4, 15, 1, 0, 1, 0, 1, 1, 1, 0, '', 3, '2018-05-21', '22:45:39'),
	(14, 1, 14, 5, 15, 1, 0, 1, 5, 1, 1, 1, 0, '', 3, '2018-05-21', '22:45:39'),
	(16, 1, 15, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 3, '2018-05-21', '23:06:26'),
	(17, 1, 15, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 3, '2018-05-21', '23:06:26'),
	(18, 1, 15, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 3, '2018-05-21', '23:06:26');
/*!40000 ALTER TABLE `nomina_asignacion` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.nomina_concepto
CREATE TABLE IF NOT EXISTS `nomina_concepto` (
  `conce_codig` int(11) NOT NULL AUTO_INCREMENT,
  `nomin_codig` int(11) NOT NULL,
  `conce_descr` text NOT NULL,
  `conce_value` text NOT NULL,
  `tconc_codig` int(11) NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `conce_fcrea` date NOT NULL,
  `conce_hcrea` time NOT NULL,
  PRIMARY KEY (`conce_codig`),
  KEY `nomin_codig` (`nomin_codig`),
  KEY `tconc_codig` (`tconc_codig`),
  KEY `usuar_cofig` (`usuar_codig`),
  CONSTRAINT `nomina_concepto_ibfk_1` FOREIGN KEY (`nomin_codig`) REFERENCES `nomina` (`nomin_codig`),
  CONSTRAINT `nomina_concepto_ibfk_2` FOREIGN KEY (`tconc_codig`) REFERENCES `nomina_concepto_tipo` (`tconc_codig`),
  CONSTRAINT `nomina_concepto_ibfk_3` FOREIGN KEY (`usuar_codig`) REFERENCES `seguridad_usuarios` (`usuar_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.nomina_concepto: ~9 rows (aproximadamente)
/*!40000 ALTER TABLE `nomina_concepto` DISABLE KEYS */;
INSERT INTO `nomina_concepto` (`conce_codig`, `nomin_codig`, `conce_descr`, `conce_value`, `tconc_codig`, `usuar_codig`, `conce_fcrea`, `conce_hcrea`) VALUES
	(1, 1, 'SALARIO BASE', '$traba_salar', 1, 3, '2018-04-10', '09:23:00'),
	(2, 1, 'HORAS EXTRAS', '((($traba_salar/30)/8)*1.5)*$hextr_canti', 1, 3, '2018-04-10', '09:27:16'),
	(3, 1, 'DIAS FERIADOS', '$traba_salar*2', 2, 3, '2018-04-10', '09:28:11'),
	(4, 1, 'BONIFICACIONES', '$bonif_canti', 1, 3, '2018-04-10', '09:41:35'),
	(5, 1, 'BONO ALIMENTICIO', '(61*500)*30', 1, 3, '2018-04-10', '09:42:00'),
	(6, 1, 'DIAS NO LABORADOS', '$ntrab_canti*501.7', 2, 3, '2018-04-10', '09:44:37'),
	(7, 1, 'SEGURO SOCIAL', '$total_asign*0.02', 2, 3, '2018-04-10', '09:45:40'),
	(8, 1, 'FAOV', '$total_asign*0.005', 2, 3, '2018-04-10', '09:46:59'),
	(9, 1, 'INCE', '$total_asign*0.005', 2, 3, '2018-04-10', '09:48:00');
/*!40000 ALTER TABLE `nomina_concepto` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.nomina_concepto_tipo
CREATE TABLE IF NOT EXISTS `nomina_concepto_tipo` (
  `tconc_codig` int(11) NOT NULL AUTO_INCREMENT,
  `tconc_descr` varchar(50) NOT NULL,
  PRIMARY KEY (`tconc_codig`),
  UNIQUE KEY `tconc_descr` (`tconc_descr`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.nomina_concepto_tipo: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `nomina_concepto_tipo` DISABLE KEYS */;
INSERT INTO `nomina_concepto_tipo` (`tconc_codig`, `tconc_descr`) VALUES
	(1, 'ASIGNACIÓN'),
	(2, 'DEDUCCIÓN');
/*!40000 ALTER TABLE `nomina_concepto_tipo` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.nomina_estatus
CREATE TABLE IF NOT EXISTS `nomina_estatus` (
  `enomi_codig` int(11) NOT NULL AUTO_INCREMENT,
  `enomi_descr` varchar(50) NOT NULL,
  PRIMARY KEY (`enomi_codig`),
  UNIQUE KEY `enomi_descr` (`enomi_descr`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.nomina_estatus: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `nomina_estatus` DISABLE KEYS */;
INSERT INTO `nomina_estatus` (`enomi_codig`, `enomi_descr`) VALUES
	(1, 'ACTIVA'),
	(2, 'CERRADA');
/*!40000 ALTER TABLE `nomina_estatus` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.nomina_periodo
CREATE TABLE IF NOT EXISTS `nomina_periodo` (
  `perio_codig` int(11) NOT NULL AUTO_INCREMENT,
  `nomin_codig` int(11) NOT NULL,
  `perio_ano` int(11) NOT NULL,
  `meses_codig` int(11) NOT NULL,
  `perio_finic` date NOT NULL,
  `perio_ffina` date NOT NULL,
  `perio_monto` double NOT NULL,
  `eperi_codig` int(11) NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `perio_fcrea` date NOT NULL,
  `perio_hcrea` time NOT NULL,
  PRIMARY KEY (`perio_codig`),
  KEY `usuar_codig` (`usuar_codig`),
  KEY `nomin_codig` (`nomin_codig`),
  KEY `perio_seria` (`perio_ano`),
  KEY `meses_codig` (`meses_codig`),
  KEY `eperi_codig` (`eperi_codig`),
  CONSTRAINT `nomina_periodo_ibfk_1` FOREIGN KEY (`eperi_codig`) REFERENCES `nomina_periodo_estatus` (`eperi_codig`),
  CONSTRAINT `nomina_periodo_ibfk_2` FOREIGN KEY (`meses_codig`) REFERENCES `meses` (`meses_codig`),
  CONSTRAINT `nomina_periodo_ibfk_3` FOREIGN KEY (`nomin_codig`) REFERENCES `nomina` (`nomin_codig`),
  CONSTRAINT `nomina_periodo_ibfk_4` FOREIGN KEY (`usuar_codig`) REFERENCES `seguridad_usuarios` (`usuar_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.nomina_periodo: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `nomina_periodo` DISABLE KEYS */;
INSERT INTO `nomina_periodo` (`perio_codig`, `nomin_codig`, `perio_ano`, `meses_codig`, `perio_finic`, `perio_ffina`, `perio_monto`, `eperi_codig`, `usuar_codig`, `perio_fcrea`, `perio_hcrea`) VALUES
	(14, 1, 0, 1, '0000-00-00', '0000-00-00', 3763281.25, 2, 3, '2018-05-21', '22:45:39'),
	(15, 1, 0, 2, '0000-02-01', '0000-00-00', 0, 2, 3, '2018-05-21', '23:06:26');
/*!40000 ALTER TABLE `nomina_periodo` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.nomina_periodo_estatus
CREATE TABLE IF NOT EXISTS `nomina_periodo_estatus` (
  `eperi_codig` int(11) NOT NULL AUTO_INCREMENT,
  `eperi_descr` varchar(50) NOT NULL,
  PRIMARY KEY (`eperi_codig`),
  UNIQUE KEY `eperi_descr` (`eperi_descr`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.nomina_periodo_estatus: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `nomina_periodo_estatus` DISABLE KEYS */;
INSERT INTO `nomina_periodo_estatus` (`eperi_codig`, `eperi_descr`) VALUES
	(1, 'ACTIVO'),
	(3, 'CALCULADO'),
	(2, 'CERRADO');
/*!40000 ALTER TABLE `nomina_periodo_estatus` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.nomina_resumen
CREATE TABLE IF NOT EXISTS `nomina_resumen` (
  `resum_codig` int(11) NOT NULL AUTO_INCREMENT,
  `nomin_codig` int(11) NOT NULL,
  `perio_codig` int(11) NOT NULL,
  `traba_codig` int(11) NOT NULL,
  `conce_codig` int(11) NOT NULL,
  `resum_monto` double NOT NULL,
  `tconc_codig` int(11) NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `resum_fcrea` date NOT NULL,
  `resum_hcrea` time NOT NULL,
  PRIMARY KEY (`resum_codig`),
  UNIQUE KEY `nomin_codig_2` (`nomin_codig`,`perio_codig`,`traba_codig`,`conce_codig`),
  KEY `nomin_codig` (`nomin_codig`),
  KEY `perio_codig` (`perio_codig`),
  KEY `traba_codig` (`traba_codig`),
  KEY `conce_codig` (`conce_codig`),
  KEY `tconc_codig` (`tconc_codig`),
  KEY `usuar_codig` (`usuar_codig`),
  CONSTRAINT `nomina_resumen_ibfk_1` FOREIGN KEY (`nomin_codig`) REFERENCES `nomina` (`nomin_codig`),
  CONSTRAINT `nomina_resumen_ibfk_2` FOREIGN KEY (`conce_codig`) REFERENCES `nomina_concepto` (`conce_codig`),
  CONSTRAINT `nomina_resumen_ibfk_3` FOREIGN KEY (`perio_codig`) REFERENCES `nomina_periodo` (`perio_codig`),
  CONSTRAINT `nomina_resumen_ibfk_4` FOREIGN KEY (`tconc_codig`) REFERENCES `nomina_concepto_tipo` (`tconc_codig`),
  CONSTRAINT `nomina_resumen_ibfk_5` FOREIGN KEY (`traba_codig`) REFERENCES `p_trabajador` (`traba_codig`),
  CONSTRAINT `nomina_resumen_ibfk_6` FOREIGN KEY (`usuar_codig`) REFERENCES `seguridad_usuarios` (`usuar_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.nomina_resumen: ~27 rows (aproximadamente)
/*!40000 ALTER TABLE `nomina_resumen` DISABLE KEYS */;
INSERT INTO `nomina_resumen` (`resum_codig`, `nomin_codig`, `perio_codig`, `traba_codig`, `conce_codig`, `resum_monto`, `tconc_codig`, `usuar_codig`, `resum_fcrea`, `resum_hcrea`) VALUES
	(55, 1, 14, 4, 1, 500000, 1, 3, '2018-05-21', '22:49:53'),
	(56, 1, 14, 4, 2, 0, 1, 3, '2018-05-21', '22:49:53'),
	(57, 1, 14, 4, 3, 500000, 1, 3, '2018-05-21', '22:49:53'),
	(58, 1, 14, 4, 4, 0, 1, 3, '2018-05-21', '22:49:53'),
	(59, 1, 14, 4, 5, 915000, 1, 3, '2018-05-21', '22:49:53'),
	(60, 1, 14, 4, 6, 0, 2, 3, '2018-05-21', '22:49:53'),
	(61, 1, 14, 4, 7, 38300, 2, 3, '2018-05-21', '22:49:53'),
	(62, 1, 14, 4, 8, 9575, 2, 3, '2018-05-21', '22:49:53'),
	(63, 1, 14, 4, 9, 9575, 2, 3, '2018-05-21', '22:49:53'),
	(64, 1, 14, 5, 1, 9000, 1, 3, '2018-05-21', '22:50:20'),
	(65, 1, 14, 5, 2, 281.25, 1, 3, '2018-05-21', '22:50:20'),
	(66, 1, 14, 5, 3, 9000, 1, 3, '2018-05-21', '22:50:20'),
	(67, 1, 14, 5, 4, 0, 1, 3, '2018-05-21', '22:50:20'),
	(68, 1, 14, 5, 5, 915000, 1, 3, '2018-05-21', '22:50:20'),
	(69, 1, 14, 5, 6, 0, 2, 3, '2018-05-21', '22:50:20'),
	(70, 1, 14, 5, 7, 18665.625, 2, 3, '2018-05-21', '22:50:20'),
	(71, 1, 14, 5, 8, 4666.40625, 2, 3, '2018-05-21', '22:50:20'),
	(72, 1, 14, 5, 9, 4666.40625, 2, 3, '2018-05-21', '22:50:20'),
	(73, 1, 14, 6, 1, 0, 1, 3, '2018-05-21', '22:58:44'),
	(74, 1, 14, 6, 2, 0, 1, 3, '2018-05-21', '22:58:44'),
	(75, 1, 14, 6, 3, 0, 1, 3, '2018-05-21', '22:58:44'),
	(76, 1, 14, 6, 4, 0, 1, 3, '2018-05-21', '22:58:44'),
	(77, 1, 14, 6, 5, 915000, 1, 3, '2018-05-21', '22:58:44'),
	(78, 1, 14, 6, 6, 0, 2, 3, '2018-05-21', '22:58:44'),
	(79, 1, 14, 6, 7, 18300, 2, 3, '2018-05-21', '22:58:44'),
	(80, 1, 14, 6, 8, 4575, 2, 3, '2018-05-21', '22:58:44'),
	(81, 1, 14, 6, 9, 4575, 2, 3, '2018-05-21', '22:58:44');
/*!40000 ALTER TABLE `nomina_resumen` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.nomina_tipo
CREATE TABLE IF NOT EXISTS `nomina_tipo` (
  `tnomi_codig` int(11) NOT NULL AUTO_INCREMENT,
  `tnomi_descr` varchar(50) NOT NULL,
  PRIMARY KEY (`tnomi_codig`),
  UNIQUE KEY `tnomi_descr` (`tnomi_descr`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.nomina_tipo: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `nomina_tipo` DISABLE KEYS */;
INSERT INTO `nomina_tipo` (`tnomi_codig`, `tnomi_descr`) VALUES
	(2, 'EXTRAORDINARIA'),
	(1, 'ORDINARIA');
/*!40000 ALTER TABLE `nomina_tipo` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.nomina_tipo_pago
CREATE TABLE IF NOT EXISTS `nomina_tipo_pago` (
  `pnomi_codig` int(11) NOT NULL AUTO_INCREMENT,
  `pnomi_descr` varchar(50) NOT NULL,
  PRIMARY KEY (`pnomi_codig`),
  UNIQUE KEY `pnomi_descr` (`pnomi_descr`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.nomina_tipo_pago: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `nomina_tipo_pago` DISABLE KEYS */;
INSERT INTO `nomina_tipo_pago` (`pnomi_codig`, `pnomi_descr`) VALUES
	(4, 'DIARIO'),
	(1, 'MENSUAL'),
	(2, 'QUINCENAL'),
	(3, 'SEMANAL');
/*!40000 ALTER TABLE `nomina_tipo_pago` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.p_pago_tipo
CREATE TABLE IF NOT EXISTS `p_pago_tipo` (
  `ptipo_codig` int(11) NOT NULL AUTO_INCREMENT,
  `ptipo_descr` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ptipo_codig`),
  UNIQUE KEY `ptipo_descr_UNIQUE` (`ptipo_descr`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.p_pago_tipo: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `p_pago_tipo` DISABLE KEYS */;
INSERT INTO `p_pago_tipo` (`ptipo_codig`, `ptipo_descr`) VALUES
	(4, 'CHEQUE'),
	(1, 'EFECTIVO'),
	(5, 'TRANSFERENCIA');
/*!40000 ALTER TABLE `p_pago_tipo` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.p_paises
CREATE TABLE IF NOT EXISTS `p_paises` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iso` char(2) DEFAULT NULL,
  `nombre` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=241 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.p_paises: ~240 rows (aproximadamente)
/*!40000 ALTER TABLE `p_paises` DISABLE KEYS */;
INSERT INTO `p_paises` (`id`, `iso`, `nombre`) VALUES
	(1, 'AF', 'Afganistán'),
	(2, 'AX', 'Islas Gland'),
	(3, 'AL', 'Albania'),
	(4, 'DE', 'Alemania'),
	(5, 'AD', 'Andorra'),
	(6, 'AO', 'Angola'),
	(7, 'AI', 'Anguilla'),
	(8, 'AQ', 'Antártida'),
	(9, 'AG', 'Antigua y Barbuda'),
	(10, 'AN', 'Antillas Holandesas'),
	(11, 'SA', 'Arabia Saudí'),
	(12, 'DZ', 'Argelia'),
	(13, 'AR', 'Argentina'),
	(14, 'AM', 'Armenia'),
	(15, 'AW', 'Aruba'),
	(16, 'AU', 'Australia'),
	(17, 'AT', 'Austria'),
	(18, 'AZ', 'Azerbaiyán'),
	(19, 'BS', 'Bahamas'),
	(20, 'BH', 'Bahréin'),
	(21, 'BD', 'Bangladesh'),
	(22, 'BB', 'Barbados'),
	(23, 'BY', 'Bielorrusia'),
	(24, 'BE', 'Bélgica'),
	(25, 'BZ', 'Belice'),
	(26, 'BJ', 'Benin'),
	(27, 'BM', 'Bermudas'),
	(28, 'BT', 'Bhután'),
	(29, 'BO', 'Bolivia'),
	(30, 'BA', 'Bosnia y Herzegovina'),
	(31, 'BW', 'Botsuana'),
	(32, 'BV', 'Isla Bouvet'),
	(33, 'BR', 'Brasil'),
	(34, 'BN', 'Brunéi'),
	(35, 'BG', 'Bulgaria'),
	(36, 'BF', 'Burkina Faso'),
	(37, 'BI', 'Burundi'),
	(38, 'CV', 'Cabo Verde'),
	(39, 'KY', 'Islas Caimán'),
	(40, 'KH', 'Camboya'),
	(41, 'CM', 'Camerún'),
	(42, 'CA', 'Canadá'),
	(43, 'CF', 'República Centroafricana'),
	(44, 'TD', 'Chad'),
	(45, 'CZ', 'República Checa'),
	(46, 'CL', 'Chile'),
	(47, 'CN', 'China'),
	(48, 'CY', 'Chipre'),
	(49, 'CX', 'Isla de Navidad'),
	(50, 'VA', 'Ciudad del Vaticano'),
	(51, 'CC', 'Islas Cocos'),
	(52, 'CO', 'Colombia'),
	(53, 'KM', 'Comoras'),
	(54, 'CD', 'República Democrática del Congo'),
	(55, 'CG', 'Congo'),
	(56, 'CK', 'Islas Cook'),
	(57, 'KP', 'Corea del Norte'),
	(58, 'KR', 'Corea del Sur'),
	(59, 'CI', 'Costa de Marfil'),
	(60, 'CR', 'Costa Rica'),
	(61, 'HR', 'Croacia'),
	(62, 'CU', 'Cuba'),
	(63, 'DK', 'Dinamarca'),
	(64, 'DM', 'Dominica'),
	(65, 'DO', 'República Dominicana'),
	(66, 'EC', 'Ecuador'),
	(67, 'EG', 'Egipto'),
	(68, 'SV', 'El Salvador'),
	(69, 'AE', 'Emiratos Árabes Unidos'),
	(70, 'ER', 'Eritrea'),
	(71, 'SK', 'Eslovaquia'),
	(72, 'SI', 'Eslovenia'),
	(73, 'ES', 'España'),
	(74, 'UM', 'Islas ultramarinas de Estados Unidos'),
	(75, 'US', 'Estados Unidos'),
	(76, 'EE', 'Estonia'),
	(77, 'ET', 'Etiopía'),
	(78, 'FO', 'Islas Feroe'),
	(79, 'PH', 'Filipinas'),
	(80, 'FI', 'Finlandia'),
	(81, 'FJ', 'Fiyi'),
	(82, 'FR', 'Francia'),
	(83, 'GA', 'Gabón'),
	(84, 'GM', 'Gambia'),
	(85, 'GE', 'Georgia'),
	(86, 'GS', 'Islas Georgias del Sur y Sandwich del Sur'),
	(87, 'GH', 'Ghana'),
	(88, 'GI', 'Gibraltar'),
	(89, 'GD', 'Granada'),
	(90, 'GR', 'Grecia'),
	(91, 'GL', 'Groenlandia'),
	(92, 'GP', 'Guadalupe'),
	(93, 'GU', 'Guam'),
	(94, 'GT', 'Guatemala'),
	(95, 'GF', 'Guayana Francesa'),
	(96, 'GN', 'Guinea'),
	(97, 'GQ', 'Guinea Ecuatorial'),
	(98, 'GW', 'Guinea-Bissau'),
	(99, 'GY', 'Guyana'),
	(100, 'HT', 'Haití'),
	(101, 'HM', 'Islas Heard y McDonald'),
	(102, 'HN', 'Honduras'),
	(103, 'HK', 'Hong Kong'),
	(104, 'HU', 'Hungría'),
	(105, 'IN', 'India'),
	(106, 'ID', 'Indonesia'),
	(107, 'IR', 'Irán'),
	(108, 'IQ', 'Iraq'),
	(109, 'IE', 'Irlanda'),
	(110, 'IS', 'Islandia'),
	(111, 'IL', 'Israel'),
	(112, 'IT', 'Italia'),
	(113, 'JM', 'Jamaica'),
	(114, 'JP', 'Japón'),
	(115, 'JO', 'Jordania'),
	(116, 'KZ', 'Kazajstán'),
	(117, 'KE', 'Kenia'),
	(118, 'KG', 'Kirguistán'),
	(119, 'KI', 'Kiribati'),
	(120, 'KW', 'Kuwait'),
	(121, 'LA', 'Laos'),
	(122, 'LS', 'Lesotho'),
	(123, 'LV', 'Letonia'),
	(124, 'LB', 'Líbano'),
	(125, 'LR', 'Liberia'),
	(126, 'LY', 'Libia'),
	(127, 'LI', 'Liechtenstein'),
	(128, 'LT', 'Lituania'),
	(129, 'LU', 'Luxemburgo'),
	(130, 'MO', 'Macao'),
	(131, 'MK', 'ARY Macedonia'),
	(132, 'MG', 'Madagascar'),
	(133, 'MY', 'Malasia'),
	(134, 'MW', 'Malawi'),
	(135, 'MV', 'Maldivas'),
	(136, 'ML', 'Malí'),
	(137, 'MT', 'Malta'),
	(138, 'FK', 'Islas Malvinas'),
	(139, 'MP', 'Islas Marianas del Norte'),
	(140, 'MA', 'Marruecos'),
	(141, 'MH', 'Islas Marshall'),
	(142, 'MQ', 'Martinica'),
	(143, 'MU', 'Mauricio'),
	(144, 'MR', 'Mauritania'),
	(145, 'YT', 'Mayotte'),
	(146, 'MX', 'México'),
	(147, 'FM', 'Micronesia'),
	(148, 'MD', 'Moldavia'),
	(149, 'MC', 'Mónaco'),
	(150, 'MN', 'Mongolia'),
	(151, 'MS', 'Montserrat'),
	(152, 'MZ', 'Mozambique'),
	(153, 'MM', 'Myanmar'),
	(154, 'NA', 'Namibia'),
	(155, 'NR', 'Nauru'),
	(156, 'NP', 'Nepal'),
	(157, 'NI', 'Nicaragua'),
	(158, 'NE', 'Níger'),
	(159, 'NG', 'Nigeria'),
	(160, 'NU', 'Niue'),
	(161, 'NF', 'Isla Norfolk'),
	(162, 'NO', 'Noruega'),
	(163, 'NC', 'Nueva Caledonia'),
	(164, 'NZ', 'Nueva Zelanda'),
	(165, 'OM', 'Omán'),
	(166, 'NL', 'Países Bajos'),
	(167, 'PK', 'Pakistán'),
	(168, 'PW', 'Palau'),
	(169, 'PS', 'Palestina'),
	(170, 'PA', 'Panamá'),
	(171, 'PG', 'Papúa Nueva Guinea'),
	(172, 'PY', 'Paraguay'),
	(173, 'PE', 'Perú'),
	(174, 'PN', 'Islas Pitcairn'),
	(175, 'PF', 'Polinesia Francesa'),
	(176, 'PL', 'Polonia'),
	(177, 'PT', 'Portugal'),
	(178, 'PR', 'Puerto Rico'),
	(179, 'QA', 'Qatar'),
	(180, 'GB', 'Reino Unido'),
	(181, 'RE', 'Reunión'),
	(182, 'RW', 'Ruanda'),
	(183, 'RO', 'Rumania'),
	(184, 'RU', 'Rusia'),
	(185, 'EH', 'Sahara Occidental'),
	(186, 'SB', 'Islas Salomón'),
	(187, 'WS', 'Samoa'),
	(188, 'AS', 'Samoa Americana'),
	(189, 'KN', 'San Cristóbal y Nevis'),
	(190, 'SM', 'San Marino'),
	(191, 'PM', 'San Pedro y Miquelón'),
	(192, 'VC', 'San Vicente y las Granadinas'),
	(193, 'SH', 'Santa Helena'),
	(194, 'LC', 'Santa Lucía'),
	(195, 'ST', 'Santo Tomé y Príncipe'),
	(196, 'SN', 'Senegal'),
	(197, 'CS', 'Serbia y Montenegro'),
	(198, 'SC', 'Seychelles'),
	(199, 'SL', 'Sierra Leona'),
	(200, 'SG', 'Singapur'),
	(201, 'SY', 'Siria'),
	(202, 'SO', 'Somalia'),
	(203, 'LK', 'Sri Lanka'),
	(204, 'SZ', 'Suazilandia'),
	(205, 'ZA', 'Sudáfrica'),
	(206, 'SD', 'Sudán'),
	(207, 'SE', 'Suecia'),
	(208, 'CH', 'Suiza'),
	(209, 'SR', 'Surinam'),
	(210, 'SJ', 'Svalbard y Jan Mayen'),
	(211, 'TH', 'Tailandia'),
	(212, 'TW', 'Taiwán'),
	(213, 'TZ', 'Tanzania'),
	(214, 'TJ', 'Tayikistán'),
	(215, 'IO', 'Territorio Británico del Océano Índico'),
	(216, 'TF', 'Territorios Australes Franceses'),
	(217, 'TL', 'Timor Oriental'),
	(218, 'TG', 'Togo'),
	(219, 'TK', 'Tokelau'),
	(220, 'TO', 'Tonga'),
	(221, 'TT', 'Trinidad y Tobago'),
	(222, 'TN', 'Túnez'),
	(223, 'TC', 'Islas Turcas y Caicos'),
	(224, 'TM', 'Turkmenistán'),
	(225, 'TR', 'Turquía'),
	(226, 'TV', 'Tuvalu'),
	(227, 'UA', 'Ucrania'),
	(228, 'UG', 'Uganda'),
	(229, 'UY', 'Uruguay'),
	(230, 'UZ', 'Uzbekistán'),
	(231, 'VU', 'Vanuatu'),
	(232, 'VE', 'Venezuela'),
	(233, 'VN', 'Vietnam'),
	(234, 'VG', 'Islas Vírgenes Británicas'),
	(235, 'VI', 'Islas Vírgenes de los Estados Unidos'),
	(236, 'WF', 'Wallis y Futuna'),
	(237, 'YE', 'Yemen'),
	(238, 'DJ', 'Yibuti'),
	(239, 'ZM', 'Zambia'),
	(240, 'ZW', 'Zimbabue');
/*!40000 ALTER TABLE `p_paises` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.pedido_categoria
CREATE TABLE IF NOT EXISTS `pedido_categoria` (
  `categ_codig` int(11) NOT NULL AUTO_INCREMENT,
  `Columna 2` int(11) NOT NULL DEFAULT '0',
  `model_codig` int(11) DEFAULT NULL,
  `categ_descr` varchar(50) DEFAULT NULL,
  `categ_fecha` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `categ_precio` int(11) DEFAULT NULL,
  `categ_obser` varchar(50) DEFAULT NULL,
  `usuar_codig` int(11) DEFAULT NULL,
  `categ_fcrea` date DEFAULT NULL,
  `categ_hcrea` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`categ_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bordados_pedidos.pedido_categoria: ~12 rows (aproximadamente)
/*!40000 ALTER TABLE `pedido_categoria` DISABLE KEYS */;
INSERT INTO `pedido_categoria` (`categ_codig`, `Columna 2`, `model_codig`, `categ_descr`, `categ_fecha`, `categ_precio`, `categ_obser`, `usuar_codig`, `categ_fcrea`, `categ_hcrea`) VALUES
	(1, 0, 1, 'CUELLO POLO', '2018-09-29 15:05:57', 18000, NULL, 0, NULL, NULL),
	(2, 0, 1, 'CUELLO CIERRE', '2018-09-29 15:06:58', 22500, NULL, 0, NULL, NULL),
	(3, 0, 1, 'CANGURO CERRADO', '2018-09-29 15:07:32', 21000, NULL, 0, NULL, NULL),
	(4, 0, 2, 'AMERICANO', '2018-09-30 17:29:44', 31000, NULL, NULL, NULL, NULL),
	(5, 0, 2, 'CANGURO Y CIERRE', '2018-09-30 17:44:48', 27500, NULL, NULL, NULL, NULL),
	(6, 0, 3, 'CUELLO ALTO CON BROCHE CORTE DIAGONAL', '2018-09-30 17:02:05', NULL, NULL, NULL, NULL, NULL),
	(7, 0, 3, 'DELANTERO', '2018-09-30 17:02:49', NULL, NULL, NULL, NULL, NULL),
	(8, 0, 3, 'CUELLO ALTO', '2018-09-30 17:03:17', NULL, NULL, NULL, NULL, NULL),
	(9, 0, 3, 'NORMAL', '2018-09-30 17:03:26', NULL, NULL, NULL, NULL, NULL),
	(11, 0, 1, 'AMERICANO CAPUCHA', '2018-09-30 17:30:03', NULL, NULL, NULL, NULL, NULL),
	(12, 0, 1, 'AMERICANO SIN CAPUCHA', '2018-09-30 17:31:09', NULL, NULL, NULL, NULL, NULL),
	(13, 0, 1, 'CANGURO  CIERRE ', '2018-09-30 17:44:43', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `pedido_categoria` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.pedido_color
CREATE TABLE IF NOT EXISTS `pedido_color` (
  `color_codig` int(11) NOT NULL AUTO_INCREMENT,
  `color_descr` varchar(50) DEFAULT NULL,
  `usuar_codig` int(11) NOT NULL DEFAULT '0',
  `color_fcrea` date NOT NULL DEFAULT '0000-00-00',
  `color_hcrea` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`color_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='colores de los articulos';

-- Volcando datos para la tabla bordados_pedidos.pedido_color: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `pedido_color` DISABLE KEYS */;
INSERT INTO `pedido_color` (`color_codig`, `color_descr`, `usuar_codig`, `color_fcrea`, `color_hcrea`) VALUES
	(1, 'A-35', 0, '0000-00-00', '0'),
	(2, 'A-27', 0, '0000-00-00', '0'),
	(3, 'A-50', 0, '0000-00-00', '0'),
	(4, 'Igual que color del Cuerpo', 0, '0000-00-00', '0'),
	(5, 'Otro', 0, '0000-00-00', '0');
/*!40000 ALTER TABLE `pedido_color` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.pedido_detalle
CREATE TABLE IF NOT EXISTS `pedido_detalle` (
  `detal_codig` int(11) NOT NULL AUTO_INCREMENT,
  `detal_model` int(11) NOT NULL DEFAULT '0',
  `pedig_codig` int(11) DEFAULT NULL,
  `pedid_nume` varchar(50) DEFAULT NULL,
  `categ_codig` int(11) DEFAULT NULL,
  `scate_codig` int(11) DEFAULT NULL,
  `varia_codig` int(11) DEFAULT NULL,
  `opcio_codig` int(11) DEFAULT NULL,
  `detal_pretin` int(11) DEFAULT NULL,
  `detal_cierre` int(11) DEFAULT NULL,
  `detal_bolsi` int(11) DEFAULT NULL,
  `detal_color` int(11) DEFAULT NULL,
  `detal_manga` int(11) DEFAULT NULL,
  `detal_cmanga` int(11) DEFAULT NULL,
  `detal_monto` int(11) DEFAULT NULL,
  `usuar_codig` int(11) DEFAULT NULL,
  `detal_fcrea` date DEFAULT NULL,
  `detal_hcrea` time DEFAULT NULL,
  PRIMARY KEY (`detal_codig`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bordados_pedidos.pedido_detalle: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `pedido_detalle` DISABLE KEYS */;
/*!40000 ALTER TABLE `pedido_detalle` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.pedido_emoji
CREATE TABLE IF NOT EXISTS `pedido_emoji` (
  `emoji_codig` int(11) NOT NULL AUTO_INCREMENT,
  `emoji_descr` varchar(50) DEFAULT NULL,
  `usuar_codig` int(11) DEFAULT NULL,
  `emoji_fcrea` date DEFAULT NULL,
  `emoji_hcrea` time DEFAULT NULL,
  PRIMARY KEY (`emoji_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bordados_pedidos.pedido_emoji: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `pedido_emoji` DISABLE KEYS */;
INSERT INTO `pedido_emoji` (`emoji_codig`, `emoji_descr`, `usuar_codig`, `emoji_fcrea`, `emoji_hcrea`) VALUES
	(1, 'emoji uno', NULL, NULL, NULL),
	(2, 'emoji dos', NULL, NULL, NULL),
	(3, 'emoji tres', NULL, NULL, NULL);
/*!40000 ALTER TABLE `pedido_emoji` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.pedido_lista
CREATE TABLE IF NOT EXISTS `pedido_lista` (
  `lista_codig` int(11) NOT NULL AUTO_INCREMENT,
  `detal_model` int(11) DEFAULT NULL,
  `pedig_codig` int(11) DEFAULT NULL,
  `pedid_nume` varchar(50) DEFAULT NULL,
  `categ_codig` int(11) DEFAULT NULL,
  `lista_nombr` varchar(50) DEFAULT NULL,
  `lista_ident` varchar(50) DEFAULT NULL,
  `lista_talla` varchar(50) DEFAULT NULL,
  `lista_npers` varchar(50) DEFAULT NULL,
  `lista_aespa` varchar(50) DEFAULT NULL,
  `lista_elect` varchar(50) DEFAULT NULL,
  `lista_emoji` int(11) DEFAULT NULL,
  `lista_imag` varchar(150) DEFAULT NULL,
  `lista_obser` varchar(50) DEFAULT NULL,
  `usuar_codig` int(11) DEFAULT NULL,
  `lista_fcrea` date DEFAULT NULL,
  `lista_hcrea` time DEFAULT NULL,
  PRIMARY KEY (`lista_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bordados_pedidos.pedido_lista: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `pedido_lista` DISABLE KEYS */;
INSERT INTO `pedido_lista` (`lista_codig`, `detal_model`, `pedig_codig`, `pedid_nume`, `categ_codig`, `lista_nombr`, `lista_ident`, `lista_talla`, `lista_npers`, `lista_aespa`, `lista_elect`, `lista_emoji`, `lista_imag`, `lista_obser`, `usuar_codig`, `lista_fcrea`, `lista_hcrea`) VALUES
	(12, 1, 3, 'Y1202', 2, 'ALDEMAR', '122', 'XL', 'EL PAPI', 'PAPIEADO', 'ELECTIVA', 0, 'C:/AppServ/www/files/20181016225609_WIN_20180725_214218 - copia.JPG', 'PRUEBA2', 3, '2018-10-16', '22:57:14');
/*!40000 ALTER TABLE `pedido_lista` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.pedido_modelo
CREATE TABLE IF NOT EXISTS `pedido_modelo` (
  `model_codig` int(11) NOT NULL AUTO_INCREMENT,
  `model_descr` varchar(50) DEFAULT NULL,
  `usuar_codig` int(11) DEFAULT NULL,
  `model_fcrea` date DEFAULT NULL,
  `model_hcrea` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`model_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bordados_pedidos.pedido_modelo: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `pedido_modelo` DISABLE KEYS */;
INSERT INTO `pedido_modelo` (`model_codig`, `model_descr`, `usuar_codig`, `model_fcrea`, `model_hcrea`) VALUES
	(1, 'POLERON', NULL, NULL, NULL),
	(2, 'CORTAVIENTO', NULL, NULL, NULL),
	(3, 'MODELO 3', NULL, NULL, NULL);
/*!40000 ALTER TABLE `pedido_modelo` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.pedido_opcional
CREATE TABLE IF NOT EXISTS `pedido_opcional` (
  `opcio_codig` int(11) NOT NULL AUTO_INCREMENT,
  `opcio_descr` varchar(50) DEFAULT NULL,
  `opcio_preci` varchar(50) DEFAULT NULL,
  `usuar_codig` int(11) DEFAULT NULL,
  `opcio_fcrea` date DEFAULT NULL,
  `opcio_hcrea` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`opcio_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bordados_pedidos.pedido_opcional: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `pedido_opcional` DISABLE KEYS */;
INSERT INTO `pedido_opcional` (`opcio_codig`, `opcio_descr`, `opcio_preci`, `usuar_codig`, `opcio_fcrea`, `opcio_hcrea`) VALUES
	(1, 'PRETINA Y PUÑO', '2500', NULL, NULL, NULL),
	(2, 'BROCHE Y CIERRE JUNTOS', '2000', NULL, NULL, NULL),
	(3, 'VIVO EN EL BOLSILLO', '2500', NULL, NULL, NULL),
	(4, 'CON LINEA', NULL, NULL, NULL, NULL),
	(5, 'SIN LINEA', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `pedido_opcional` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.pedido_pedidos
CREATE TABLE IF NOT EXISTS `pedido_pedidos` (
  `pedid_codig` int(11) NOT NULL AUTO_INCREMENT,
  `pedid_nume` varchar(50) NOT NULL DEFAULT '0',
  `tusua_codig` int(11) NOT NULL DEFAULT '0',
  `categ_codig` int(11) DEFAULT NULL,
  `pedid_canti` int(11) NOT NULL DEFAULT '0',
  `pedid_ctdmo` int(11) NOT NULL DEFAULT '0',
  `pedid_model` int(11) NOT NULL DEFAULT '0',
  `pedid_cuerpo` int(11) NOT NULL DEFAULT '0',
  `pedid_fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pedid_statu` int(11) DEFAULT '0',
  `pedid_statbl` int(11) DEFAULT '0',
  PRIMARY KEY (`pedid_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='articulos de ropa';

-- Volcando datos para la tabla bordados_pedidos.pedido_pedidos: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `pedido_pedidos` DISABLE KEYS */;
INSERT INTO `pedido_pedidos` (`pedid_codig`, `pedid_nume`, `tusua_codig`, `categ_codig`, `pedid_canti`, `pedid_ctdmo`, `pedid_model`, `pedid_cuerpo`, `pedid_fecha`, `pedid_statu`, `pedid_statbl`) VALUES
	(3, 'Y1202', 3, 2, 24, 12, 1, 3, '2018-10-13 17:16:55', 0, 0);
/*!40000 ALTER TABLE `pedido_pedidos` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.pedido_subcategoria
CREATE TABLE IF NOT EXISTS `pedido_subcategoria` (
  `categ_codig` int(11) NOT NULL AUTO_INCREMENT,
  `categ_descr` varchar(50) DEFAULT NULL,
  `categ_codigp` int(11) DEFAULT NULL,
  `usuar_codig` int(11) DEFAULT NULL,
  `categ_fcrea` date DEFAULT NULL,
  `categ_hcrea` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`categ_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bordados_pedidos.pedido_subcategoria: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `pedido_subcategoria` DISABLE KEYS */;
INSERT INTO `pedido_subcategoria` (`categ_codig`, `categ_descr`, `categ_codigp`, `usuar_codig`, `categ_fcrea`, `categ_hcrea`) VALUES
	(1, 'CUELLO ALTO CON BROCHE CORTE DIAGONAL', 3, NULL, NULL, NULL),
	(2, 'DELANTERO', 3, NULL, NULL, NULL),
	(3, 'CUELLO ALTO', 13, NULL, NULL, NULL),
	(4, 'NORMAL', 3, NULL, NULL, NULL),
	(5, 'NORMAL', 13, NULL, NULL, NULL);
/*!40000 ALTER TABLE `pedido_subcategoria` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.pedido_variante
CREATE TABLE IF NOT EXISTS `pedido_variante` (
  `varia_codig` int(11) NOT NULL AUTO_INCREMENT,
  `varia_descr` varchar(50) DEFAULT NULL,
  `varia_preci` varchar(50) DEFAULT NULL,
  `usuar_codig` int(11) DEFAULT NULL,
  `varia_fcrea` date DEFAULT NULL,
  `varia_hcrea` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`varia_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bordados_pedidos.pedido_variante: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `pedido_variante` DISABLE KEYS */;
INSERT INTO `pedido_variante` (`varia_codig`, `varia_descr`, `varia_preci`, `usuar_codig`, `varia_fcrea`, `varia_hcrea`) VALUES
	(1, 'BOLSILLO CASACA', '1000', NULL, NULL, NULL),
	(2, 'BOLSILLO CANGURO', '1000', NULL, NULL, NULL);
/*!40000 ALTER TABLE `pedido_variante` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.permisos
CREATE TABLE IF NOT EXISTS `permisos` (
  `permi_codig` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `modul_codig` int(10) NOT NULL,
  `urole_codig` int(11) NOT NULL,
  `permi_bperm` tinyint(1) NOT NULL,
  `permi_bborr` tinyint(1) NOT NULL,
  PRIMARY KEY (`permi_codig`),
  UNIQUE KEY `cvlpermisos` (`permi_codig`),
  KEY `clvrol` (`urole_codig`),
  KEY `modul_codig` (`modul_codig`),
  CONSTRAINT `permisos_ibfk_1` FOREIGN KEY (`urole_codig`) REFERENCES `seguridad_usuarios_roles` (`urole_codig`),
  CONSTRAINT `permisos_ibfk_2` FOREIGN KEY (`modul_codig`) REFERENCES `modulos` (`modul_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.permisos: ~38 rows (aproximadamente)
/*!40000 ALTER TABLE `permisos` DISABLE KEYS */;
INSERT INTO `permisos` (`permi_codig`, `modul_codig`, `urole_codig`, `permi_bperm`, `permi_bborr`) VALUES
	(8, 1, 1, 1, 0),
	(9, 100, 1, 1, 0),
	(10, 101, 1, 1, 0),
	(11, 102, 1, 1, 0),
	(12, 103, 1, 1, 0),
	(13, 104, 1, 1, 1),
	(14, 105, 1, 1, 0),
	(15, 106, 1, 1, 0),
	(16, 107, 1, 1, 0),
	(17, 108, 1, 1, 0),
	(18, 109, 1, 1, 1),
	(19, 110, 1, 1, 0),
	(20, 111, 1, 1, 0),
	(21, 900, 1, 1, 0),
	(22, 112, 1, 1, 0),
	(23, 200, 1, 1, 0),
	(24, 300, 1, 1, 0),
	(25, 400, 1, 1, 0),
	(26, 600, 1, 1, 0),
	(27, 700, 1, 1, 0),
	(28, 701, 1, 1, 0),
	(29, 800, 1, 0, 1),
	(30, 801, 1, 0, 1),
	(31, 702, 1, 0, 1),
	(32, 802, 1, 0, 1),
	(33, 703, 1, 0, 1),
	(34, 201, 1, 1, 0),
	(35, 202, 1, 1, 0),
	(36, 203, 1, 1, 0),
	(37, 500, 1, 1, 0),
	(38, 113, 1, 1, 0),
	(39, 2, 1, 1, 0),
	(40, 114, 1, 1, 0),
	(41, 115, 1, 1, 0),
	(42, 116, 1, 1, 0),
	(43, 1, 2, 1, 0),
	(44, 1, 3, 1, 0),
	(46, 901, 1, 1, 0);
/*!40000 ALTER TABLE `permisos` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.permisos_estatus
CREATE TABLE IF NOT EXISTS `permisos_estatus` (
  `eperm_codig` int(11) NOT NULL AUTO_INCREMENT,
  `eperm_descr` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`eperm_codig`),
  UNIQUE KEY `eperm_descr` (`eperm_descr`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Volcando datos para la tabla bordados_pedidos.permisos_estatus: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `permisos_estatus` DISABLE KEYS */;
INSERT INTO `permisos_estatus` (`eperm_codig`, `eperm_descr`) VALUES
	(1, 'ACTIVO'),
	(0, 'INACTIVO');
/*!40000 ALTER TABLE `permisos_estatus` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.p_persona
CREATE TABLE IF NOT EXISTS `p_persona` (
  `perso_codig` int(11) NOT NULL AUTO_INCREMENT,
  `nacio_value` varchar(1) NOT NULL,
  `perso_cedul` bigint(50) NOT NULL,
  `perso_pnomb` varchar(20) NOT NULL,
  `perso_snomb` varchar(20) DEFAULT NULL,
  `perso_papel` varchar(20) NOT NULL,
  `perso_sapel` varchar(20) DEFAULT NULL,
  `perso_fnaci` date NOT NULL,
  `gener_value` varchar(1) NOT NULL,
  `ecivi_value` varchar(45) NOT NULL,
  `perso_obser` text NOT NULL,
  PRIMARY KEY (`perso_codig`),
  UNIQUE KEY `perso_nacio` (`nacio_value`,`perso_cedul`),
  KEY `gener_value` (`gener_value`),
  KEY `ecivi_value` (`ecivi_value`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.p_persona: ~10 rows (aproximadamente)
/*!40000 ALTER TABLE `p_persona` DISABLE KEYS */;
INSERT INTO `p_persona` (`perso_codig`, `nacio_value`, `perso_cedul`, `perso_pnomb`, `perso_snomb`, `perso_papel`, `perso_sapel`, `perso_fnaci`, `gener_value`, `ecivi_value`, `perso_obser`) VALUES
	(1, 'V', 24900845, 'KEVIN', 'ALEJANDRO', 'FIGUERA', 'RODRIGUEZ', '1994-10-03', 'M', 'S', 'KEVIN'),
	(19, 'V', 13882284, 'IVEY', 'EMILIA', 'MONTOYA', 'DE MARQUEZ', '1977-05-01', 'F', 'C', 'NINGUNA'),
	(32, 'E', 24900845, 'KEVIN ', 'ALEJANDRO', 'FIGUERA', 'RODRIGUEZ', '1994-10-03', 'M', 'S', 'PRUEBA'),
	(33, 'V', 18751147, 'ADRIANO', 'JESUS', 'SILVA', 'RODRIGUEZ', '1986-10-06', '1', 'S', 'COLEGIO LAS ACACIAS'),
	(34, 'V', 23456789, 'JUAN', 'ANTONIO', 'RODRIGUEZ', 'PEREZ', '1990-05-04', 'M', 'S', ''),
	(35, 'V', 249008, 'KE1111', '11111', '11111', '1111', '2018-06-07', 'M', 'S', ''),
	(44, 'V', 4343434343434343, 'KEVIN', 'ETERTER', 'ETRTRETE', 'ERTRETRETERTE', '2018-07-26', 'M', 'S', '4343434'),
	(68, '', 24900848, 'KEVIN', 'ALEJANDRO', 'FIGUERA', 'RODRIGUEZ', '1994-10-03', '1', '', 'COLEGIO LAS ACACIAS'),
	(75, '', 24900849, 'KEVIN', 'ALEJANDRO', 'FIGUERA', 'RODRIGUEZ', '0000-00-00', '', '', 'PRUEBA CON CORREO'),
	(76, '', 24900850, 'KEVIN', 'KEVIN', 'FIGUERA', 'RODRIGUEZ', '0000-00-00', '', '', '123455');
/*!40000 ALTER TABLE `p_persona` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.producto_tipo
CREATE TABLE IF NOT EXISTS `producto_tipo` (
  `tprod_codig` int(11) NOT NULL AUTO_INCREMENT,
  `tprod_descr` varchar(50) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`tprod_codig`),
  UNIQUE KEY `tprod_descr` (`tprod_descr`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla bordados_pedidos.producto_tipo: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `producto_tipo` DISABLE KEYS */;
INSERT INTO `producto_tipo` (`tprod_codig`, `tprod_descr`) VALUES
	(1, 'PRODUCTOS'),
	(2, 'SERVICIOS');
/*!40000 ALTER TABLE `producto_tipo` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.proveedor
CREATE TABLE IF NOT EXISTS `proveedor` (
  `prove_codig` int(11) NOT NULL AUTO_INCREMENT,
  `tclie_codig` int(11) NOT NULL,
  `tdocu_codig` int(11) NOT NULL,
  `prove_ndocu` varchar(20) NOT NULL,
  `prove_denom` varchar(200) NOT NULL,
  `prove_ccont` varchar(20) NOT NULL,
  `prove_corre` varchar(50) NOT NULL,
  `prove_telef` varchar(15) NOT NULL,
  `prove_direc` text NOT NULL,
  `prove_obser` text NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `prove_fcrea` date NOT NULL,
  `prove_hcrea` time NOT NULL,
  PRIMARY KEY (`prove_codig`),
  KEY `tclie_codig` (`tclie_codig`),
  KEY `tdocu_codig` (`tdocu_codig`),
  KEY `usuar_codig` (`usuar_codig`),
  CONSTRAINT `proveedor_ibfk_1` FOREIGN KEY (`tclie_codig`) REFERENCES `proveedor_tipo` (`tprov_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.proveedor: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `proveedor` DISABLE KEYS */;
INSERT INTO `proveedor` (`prove_codig`, `tclie_codig`, `tdocu_codig`, `prove_ndocu`, `prove_denom`, `prove_ccont`, `prove_corre`, `prove_telef`, `prove_direc`, `prove_obser`, `usuar_codig`, `prove_fcrea`, `prove_hcrea`) VALUES
	(3, 1, 1, '123456', 'KEVIN FIGUERA', '12345', 'A@A.A', '12345', '1234', '', 3, '2018-01-22', '08:13:57'),
	(4, 1, 1, '24900845', 'KEVIN FIGUERA', '', 'KEVINALEJANDRO3@GMAIL.COM', '4241941881', 'MONTALBAN', '', 3, '2018-08-01', '19:36:54'),
	(5, 1, 1, '13882284', 'IVEY MONTOYA', '', 'IVEYMONTOYA@GMAIL.COM', '5555555555', 'CALLE NEGRIN CON GARCIA LOS ALAMOS ', 'OTRO BANCO', 3, '2018-08-07', '22:13:40'),
	(6, 1, 1, '24321991', 'KARLA NADALES', '', 'KARLANADALES12@GMAIL.COM', '04141111111', 'BARINAS', '2', 3, '2018-08-08', '05:24:10'),
	(9, 1, 1, '18390585', 'DANNY MARIN', '', 'DMARIN@GMAIL.COM', '02121112222', 'A', '1', 3, '2018-08-08', '05:27:45'),
	(19, 2, 4, '309386310', 'JVKA REDES Y SISTEMAS CA', '', 'JVKACA@GMAIL.COM', '0421941881', 'CARACAS VENEZUELA', 'PRUEBA 2', 3, '2018-08-14', '19:41:15');
/*!40000 ALTER TABLE `proveedor` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.proveedor_tipo
CREATE TABLE IF NOT EXISTS `proveedor_tipo` (
  `tprov_codig` int(11) NOT NULL AUTO_INCREMENT,
  `tprov_descr` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`tprov_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla bordados_pedidos.proveedor_tipo: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `proveedor_tipo` DISABLE KEYS */;
INSERT INTO `proveedor_tipo` (`tprov_codig`, `tprov_descr`) VALUES
	(1, 'NATURAL'),
	(2, 'JURIDICO');
/*!40000 ALTER TABLE `proveedor_tipo` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.rango_categoria
CREATE TABLE IF NOT EXISTS `rango_categoria` (
  `ranca_codig` int(11) NOT NULL AUTO_INCREMENT,
  `categ_codig` int(11) NOT NULL,
  `categ_desde` varchar(10) NOT NULL,
  `categ_hasta` varchar(10) NOT NULL,
  `parti_inici` int(11) NOT NULL,
  `parti_final` int(11) NOT NULL,
  `parti_actua` int(11) NOT NULL,
  PRIMARY KEY (`ranca_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.rango_categoria: ~13 rows (aproximadamente)
/*!40000 ALTER TABLE `rango_categoria` DISABLE KEYS */;
INSERT INTO `rango_categoria` (`ranca_codig`, `categ_codig`, `categ_desde`, `categ_hasta`, `parti_inici`, `parti_final`, `parti_actua`) VALUES
	(1, 1, '20', '23', 201, 250, 209),
	(2, 2, '15', '17', 101, 200, 149),
	(3, 3, '8', '11', 1, 100, 73),
	(4, 3, '12', '14', 1, 100, 73),
	(7, 4, '30', '34', 251, 99999, 272),
	(8, 4, '35', '39', 251, 99999, 272),
	(9, 4, '40', '44', 251, 99999, 272),
	(10, 4, '45', '49', 251, 99999, 272),
	(11, 4, '50', '54', 251, 99999, 272),
	(12, 4, '55', '59', 251, 99999, 272),
	(13, 4, '60', '999', 251, 99999, 272),
	(14, 5, '24', '999', 201, 250, 209),
	(15, 6, '18', '19', 201, 250, 209);
/*!40000 ALTER TABLE `rango_categoria` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.registro
CREATE TABLE IF NOT EXISTS `registro` (
  `regis_codig` int(11) NOT NULL AUTO_INCREMENT,
  `regis_nombr` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `regis_direc` text COLLATE utf8mb4_bin NOT NULL,
  `regis_telef` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `regis_pgweb` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `regis_rede1` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `regis_rede2` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `regis_rede3` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `cont1_ndocu` int(11) NOT NULL,
  `cont1_pnomb` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `cont1_snomb` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `cont1_papel` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `cont1_sapel` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `cont1_corre` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `cont1_tofic` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `cont1_tcelu` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `cont1_fnaci` date DEFAULT NULL,
  `cont1_gener` int(11) NOT NULL,
  `cont1_curso` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `cont2_ndocu` int(11) NOT NULL,
  `cont2_pnomb` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `cont2_snomb` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `cont2_papel` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `cont2_sapel` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `cont2_corre` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `cont2_tofic` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `cont2_tcelu` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `cont2_fnaci` date DEFAULT NULL,
  `cont2_gener` int(11) NOT NULL,
  `cont2_curso` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `eregi_codig` int(11) NOT NULL,
  `regis_obser` text COLLATE utf8mb4_bin NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `regis_fcrea` date NOT NULL,
  `regis_hcrea` time NOT NULL,
  PRIMARY KEY (`regis_codig`),
  KEY `usuar_codig` (`usuar_codig`),
  KEY `cont2_gener` (`cont2_gener`),
  KEY `cont1_gener` (`cont1_gener`),
  KEY `eregi_codig` (`eregi_codig`) USING BTREE,
  CONSTRAINT `registro_ibfk_1` FOREIGN KEY (`usuar_codig`) REFERENCES `seguridad_usuarios` (`usuar_codig`),
  CONSTRAINT `registro_ibfk_2` FOREIGN KEY (`eregi_codig`) REFERENCES `registro_estatu` (`eregi_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Volcando datos para la tabla bordados_pedidos.registro: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `registro` DISABLE KEYS */;
INSERT INTO `registro` (`regis_codig`, `regis_nombr`, `regis_direc`, `regis_telef`, `regis_pgweb`, `regis_rede1`, `regis_rede2`, `regis_rede3`, `cont1_ndocu`, `cont1_pnomb`, `cont1_snomb`, `cont1_papel`, `cont1_sapel`, `cont1_corre`, `cont1_tofic`, `cont1_tcelu`, `cont1_fnaci`, `cont1_gener`, `cont1_curso`, `cont2_ndocu`, `cont2_pnomb`, `cont2_snomb`, `cont2_papel`, `cont2_sapel`, `cont2_corre`, `cont2_tofic`, `cont2_tcelu`, `cont2_fnaci`, `cont2_gener`, `cont2_curso`, `eregi_codig`, `regis_obser`, `usuar_codig`, `regis_fcrea`, `regis_hcrea`) VALUES
	(2, 'COLEGIO LAS ACACIAS', 'CARACS', '02125554433', '', '', '', '', 24900848, 'KEVIN', 'ALEJANDRO', 'FIGUERA', 'RODRIGUEZ', 'kevinalejandro@gmail.com', '04241941881', '04241941881', '1994-10-03', 1, '5CSA', 18751147, 'ADRIANO', 'JESUS', 'SILVA', 'RODRIGUEZ', 'DRAGON69_69', '', '', '1986-10-06', 1, '5CSB', 2, 'NINGUNA', 11, '2018-09-02', '22:32:06'),
	(9, '', '', '', '', '', '', '', 24900849, 'KEVIN', 'ALEJANDRO', 'FIGUERA', 'RODRIGUEZ', 'kevin.alejandro3@gmail.com', '02121941881', '04241941881', NULL, 0, '', 0, '', '', '', '', '', '', '', NULL, 0, '', 1, 'PRUEBA CON CORREO', 18, '2018-09-02', '23:30:48'),
	(10, '', '', '', '', '', '', '', 24900850, 'KEVIN', 'KEVIN', 'FIGUERA', 'RODRIGUEZ', 'k.evinalejandro3@gmail.com', '04241941881', '04241941881', NULL, 0, '', 0, '', '', '', '', '', '', '', NULL, 0, '', 1, '123455', 19, '2018-09-03', '19:52:38');
/*!40000 ALTER TABLE `registro` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.registro_estatu
CREATE TABLE IF NOT EXISTS `registro_estatu` (
  `eregi_codig` int(11) NOT NULL AUTO_INCREMENT,
  `eregi_descr` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`eregi_codig`),
  UNIQUE KEY `eregi_descr` (`eregi_descr`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Volcando datos para la tabla bordados_pedidos.registro_estatu: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `registro_estatu` DISABLE KEYS */;
INSERT INTO `registro_estatu` (`eregi_codig`, `eregi_descr`) VALUES
	(3, 'APROBADO'),
	(2, 'ENVIADO'),
	(1, 'PENDIENTE'),
	(9, 'RECHAZADO');
/*!40000 ALTER TABLE `registro_estatu` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.registro_movimiento
CREATE TABLE IF NOT EXISTS `registro_movimiento` (
  `mregi_codig` int(11) NOT NULL AUTO_INCREMENT,
  `regis_codig` int(11) NOT NULL,
  `eregi_codig` int(11) NOT NULL,
  `mregi_motiv` text COLLATE utf8mb4_bin NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `mregi_fcrea` date NOT NULL,
  `mregi_hcrea` time NOT NULL,
  PRIMARY KEY (`mregi_codig`),
  KEY `regis_codig` (`regis_codig`),
  KEY `eregi_codig` (`eregi_codig`),
  KEY `usuar_codig` (`usuar_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- Volcando datos para la tabla bordados_pedidos.registro_movimiento: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `registro_movimiento` DISABLE KEYS */;
INSERT INTO `registro_movimiento` (`mregi_codig`, `regis_codig`, `eregi_codig`, `mregi_motiv`, `usuar_codig`, `mregi_fcrea`, `mregi_hcrea`) VALUES
	(5, 2, 9, 'WERWER', 3, '2018-09-17', '21:15:59'),
	(16, 2, 9, 'A', 3, '2018-09-17', '21:53:12'),
	(17, 2, 9, 'A', 3, '2018-09-17', '21:54:28'),
	(18, 2, 9, 'A', 3, '2018-09-17', '21:55:37'),
	(19, 2, 9, 'A', 3, '2018-09-17', '21:56:31');
/*!40000 ALTER TABLE `registro_movimiento` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.sequence_data
CREATE TABLE IF NOT EXISTS `sequence_data` (
  `sequence_name` varchar(100) NOT NULL,
  `sequence_increment` int(11) unsigned NOT NULL DEFAULT '1',
  `sequence_min_value` int(11) unsigned NOT NULL DEFAULT '1',
  `sequence_max_value` bigint(20) unsigned NOT NULL DEFAULT '18446744073709551615',
  `sequence_cur_value` bigint(20) unsigned DEFAULT '1',
  `sequence_cycle` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sequence_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bordados_pedidos.sequence_data: 0 rows
/*!40000 ALTER TABLE `sequence_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `sequence_data` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.servicios
CREATE TABLE IF NOT EXISTS `servicios` (
  `servi_codig` int(11) NOT NULL AUTO_INCREMENT,
  `servi_descr` varchar(50) COLLATE utf8_bin NOT NULL,
  `servi_preci` double NOT NULL,
  `moned_codig` int(11) NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `servi_fcrea` date NOT NULL,
  `servi_hcrea` time NOT NULL,
  PRIMARY KEY (`servi_codig`),
  UNIQUE KEY `servi_descr` (`servi_descr`),
  KEY `usuar_codig` (`usuar_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla bordados_pedidos.servicios: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `servicios` DISABLE KEYS */;
INSERT INTO `servicios` (`servi_codig`, `servi_descr`, `servi_preci`, `moned_codig`, `usuar_codig`, `servi_fcrea`, `servi_hcrea`) VALUES
	(1, 'CORTE DE CABELLO', 10000, 1, 3, '2018-08-20', '18:57:00');
/*!40000 ALTER TABLE `servicios` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.tipo_cliente_documento
CREATE TABLE IF NOT EXISTS `tipo_cliente_documento` (
  `tcdoc_codig` int(11) NOT NULL AUTO_INCREMENT,
  `tclie_codig` int(11) NOT NULL,
  `tdocu_codig` int(11) NOT NULL,
  PRIMARY KEY (`tcdoc_codig`),
  UNIQUE KEY `tclie_codig_2` (`tclie_codig`,`tdocu_codig`),
  KEY `tclie_codig` (`tclie_codig`),
  KEY `tdocu_codig` (`tdocu_codig`),
  CONSTRAINT `tipo_cliente_documento_ibfk_1` FOREIGN KEY (`tclie_codig`) REFERENCES `cliente_tipo` (`tclie_codig`),
  CONSTRAINT `tipo_cliente_documento_ibfk_2` FOREIGN KEY (`tdocu_codig`) REFERENCES `p_documento_tipo` (`tdocu_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla bordados_pedidos.tipo_cliente_documento: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `tipo_cliente_documento` DISABLE KEYS */;
INSERT INTO `tipo_cliente_documento` (`tcdoc_codig`, `tclie_codig`, `tdocu_codig`) VALUES
	(1, 1, 1),
	(2, 1, 2),
	(3, 1, 3),
	(4, 2, 4),
	(5, 2, 5);
/*!40000 ALTER TABLE `tipo_cliente_documento` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.p_trabajador
CREATE TABLE IF NOT EXISTS `p_trabajador` (
  `traba_codig` int(11) NOT NULL AUTO_INCREMENT,
  `perso_codig` int(11) NOT NULL,
  `ttipo_codig` int(11) NOT NULL,
  `etrab_codig` int(11) NOT NULL,
  `traba_corre` varchar(50) NOT NULL,
  `traba_telef` varchar(50) NOT NULL,
  `traba_direc` text NOT NULL,
  `traba_salar` double NOT NULL,
  `traba_comis` double NOT NULL DEFAULT '0',
  `banco_value` varchar(4) NOT NULL,
  `traba_ncuen` varchar(20) NOT NULL,
  `tcuen_codig` int(11) NOT NULL,
  `traba_ctrab` varchar(45) NOT NULL,
  `traba_obser` text NOT NULL,
  `usuar_codig` int(11) NOT NULL,
  `traba_fcrea` date NOT NULL,
  `traba_hcrea` time NOT NULL,
  PRIMARY KEY (`traba_codig`),
  KEY `perso_codig` (`perso_codig`),
  KEY `ttipo_codig` (`ttipo_codig`),
  KEY `banco_value` (`banco_value`),
  KEY `usuar_codig` (`usuar_codig`),
  KEY `tcuen_tipo` (`tcuen_codig`),
  KEY `etrab_codig` (`etrab_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.p_trabajador: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `p_trabajador` DISABLE KEYS */;
INSERT INTO `p_trabajador` (`traba_codig`, `perso_codig`, `ttipo_codig`, `etrab_codig`, `traba_corre`, `traba_telef`, `traba_direc`, `traba_salar`, `traba_comis`, `banco_value`, `traba_ncuen`, `tcuen_codig`, `traba_ctrab`, `traba_obser`, `usuar_codig`, `traba_fcrea`, `traba_hcrea`) VALUES
	(4, 1, 1, 1, 'KEVINALEJANDRO3@GMAIL.COM', '04241941881', 'KM 23 DEL JUNQUITO, CARACAS, VENEZUELA', 3501000, 50, '0134', '0134079162791006348', 1, '0024900845', 'NINGUNA ', 3, '2017-12-28', '19:22:15'),
	(5, 33, 1, 1, 'DRAGON69_69@GMAIL.COM', '0424-194.18.81', 'CARACAS', 0, 50, '0134', '01340000000000000000', 1, '0', '', 3, '2018-02-05', '16:59:15'),
	(6, 34, 1, 1, 'PJ@GMAIL.COM', '0412-345.67.95', 'DFDFDFDD', 730, 1, '0134', '01346578078988576488', 1, '0', '', 3, '2018-05-21', '22:57:16');
/*!40000 ALTER TABLE `p_trabajador` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.p_trabajador_estatus
CREATE TABLE IF NOT EXISTS `p_trabajador_estatus` (
  `etrab_codig` int(11) NOT NULL AUTO_INCREMENT,
  `etrab_descr` varchar(50) NOT NULL,
  PRIMARY KEY (`etrab_codig`),
  UNIQUE KEY `etrab_descr` (`etrab_descr`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.p_trabajador_estatus: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `p_trabajador_estatus` DISABLE KEYS */;
INSERT INTO `p_trabajador_estatus` (`etrab_codig`, `etrab_descr`) VALUES
	(1, 'ACTIVO'),
	(3, 'EGRESADO'),
	(2, 'INACTIVO');
/*!40000 ALTER TABLE `p_trabajador_estatus` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.p_trabajador_tipo
CREATE TABLE IF NOT EXISTS `p_trabajador_tipo` (
  `ttipo_codig` int(11) NOT NULL AUTO_INCREMENT,
  `ttipo_descr` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ttipo_codig`),
  UNIQUE KEY `ttipo_descr_UNIQUE` (`ttipo_descr`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.p_trabajador_tipo: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `p_trabajador_tipo` DISABLE KEYS */;
INSERT INTO `p_trabajador_tipo` (`ttipo_codig`, `ttipo_descr`) VALUES
	(4, 'GERENTE'),
	(1, 'VENDEDOR');
/*!40000 ALTER TABLE `p_trabajador_tipo` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.p_unidad_tipo
CREATE TABLE IF NOT EXISTS `p_unidad_tipo` (
  `tunid_codig` int(11) NOT NULL AUTO_INCREMENT,
  `tunid_descr` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`tunid_codig`),
  UNIQUE KEY `tunid_descr_UNIQUE` (`tunid_descr`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.p_unidad_tipo: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `p_unidad_tipo` DISABLE KEYS */;
INSERT INTO `p_unidad_tipo` (`tunid_codig`, `tunid_descr`) VALUES
	(1, 'CAJA'),
	(3, 'DESCRIPCION'),
	(2, 'LITRO');
/*!40000 ALTER TABLE `p_unidad_tipo` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla bordados_pedidos.users: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `email`, `password`) VALUES
	(0, 'lunafive@hotmail.com', '123456789');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.seguridad_usuarios
CREATE TABLE IF NOT EXISTS `seguridad_usuarios` (
  `usuar_codig` int(11) NOT NULL AUTO_INCREMENT,
  `perso_codig` int(11) NOT NULL,
  `usuar_login` varchar(50) NOT NULL,
  `usuar_passw` varchar(50) NOT NULL,
  `urole_codig` int(11) NOT NULL,
  `uesta_codig` int(11) NOT NULL,
  `usuar_obser` text NOT NULL,
  `usuar_fcrea` date NOT NULL,
  PRIMARY KEY (`usuar_codig`),
  UNIQUE KEY `perso_codig` (`perso_codig`),
  KEY `uesta_codig` (`uesta_codig`),
  KEY `urole_codig` (`urole_codig`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.seguridad_usuarios: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `seguridad_usuarios` DISABLE KEYS */;
INSERT INTO `seguridad_usuarios` (`usuar_codig`, `perso_codig`, `usuar_login`, `usuar_passw`, `urole_codig`, `uesta_codig`, `usuar_obser`, `usuar_fcrea`) VALUES
	(3, 1, 'kevinalejandro3@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 1, 1, 'NINGUNA', '0000-00-00'),
	(4, 19, 'iveymontoya@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 1, 1, 'IVEY MONTOYA', '0000-00-00'),
	(5, 32, 'kfiguera@smartwebtools.net', 'e10adc3949ba59abbe56e057f20f883e', 1, 1, 'PRUEBA', '2018-02-05'),
	(11, 68, 'kevinalejandro@gmail.com', '', 2, 2, 'NINGUNA', '2018-09-02'),
	(18, 75, 'kevin.alejandro3@gmail.com', '', 2, 2, 'PRUEBA CON CORREO', '2018-09-02'),
	(19, 76, 'k.evinalejandro3@gmail.com', '', 2, 2, '123455', '2018-09-03');
/*!40000 ALTER TABLE `seguridad_usuarios` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.seguridad_usuarios_estatus
CREATE TABLE IF NOT EXISTS `seguridad_usuarios_estatus` (
  `uesta_codig` int(11) NOT NULL AUTO_INCREMENT,
  `uesta_descr` varchar(20) NOT NULL,
  PRIMARY KEY (`uesta_codig`),
  UNIQUE KEY `uesta_descr` (`uesta_descr`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.seguridad_usuarios_estatus: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `seguridad_usuarios_estatus` DISABLE KEYS */;
INSERT INTO `seguridad_usuarios_estatus` (`uesta_codig`, `uesta_descr`) VALUES
	(1, 'ACTIVO'),
	(9, 'INACTIVO'),
	(2, 'VERIFICACION');
/*!40000 ALTER TABLE `seguridad_usuarios_estatus` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.seguridad_usuarios_roles
CREATE TABLE IF NOT EXISTS `seguridad_usuarios_roles` (
  `urole_codig` int(11) NOT NULL AUTO_INCREMENT,
  `urole_descr` varchar(20) NOT NULL,
  PRIMARY KEY (`urole_codig`),
  UNIQUE KEY `roles_descr` (`urole_descr`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.seguridad_usuarios_roles: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `seguridad_usuarios_roles` DISABLE KEYS */;
INSERT INTO `seguridad_usuarios_roles` (`urole_codig`, `urole_descr`) VALUES
	(1, 'ADMINISTRADOR'),
	(2, 'CLIENTE EN REGISTRO'),
	(3, 'CLIENTE REGISTRADO');
/*!40000 ALTER TABLE `seguridad_usuarios_roles` ENABLE KEYS */;

-- Volcando estructura para tabla bordados_pedidos.venta_tipo
CREATE TABLE IF NOT EXISTS `venta_tipo` (
  `tvent_codig` int(11) NOT NULL AUTO_INCREMENT,
  `tvent_descr` varchar(45) NOT NULL,
  PRIMARY KEY (`tvent_codig`),
  UNIQUE KEY `tvent_descr` (`tvent_descr`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla bordados_pedidos.venta_tipo: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `venta_tipo` DISABLE KEYS */;
INSERT INTO `venta_tipo` (`tvent_codig`, `tvent_descr`) VALUES
	(1, 'CONTADO'),
	(2, 'CREDITO');
/*!40000 ALTER TABLE `venta_tipo` ENABLE KEYS */;

-- Volcando estructura para vista bordados_pedidos.v_detalle
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `v_detalle` (
	`detal_codig` INT(11) NOT NULL,
	`detal_model` INT(11) NOT NULL,
	`pedig_codig` INT(11) NULL,
	`pedid_nume` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`modelo` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`categoria` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`variante` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`opcional` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`opcional1` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`color` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`manga` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`colormanga` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`subcategoria` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`categ_codig` INT(11) NULL,
	`scate_codig` INT(11) NULL,
	`varia_codig` INT(11) NULL,
	`opcio_codig` INT(11) NULL,
	`detal_pretin` INT(11) NULL,
	`detal_cierre` INT(11) NULL,
	`detal_bolsi` INT(11) NULL,
	`detal_color` INT(11) NULL,
	`detal_manga` INT(11) NULL,
	`detal_cmanga` INT(11) NULL,
	`detal_monto` INT(11) NULL,
	`usuar_codig` INT(11) NULL,
	`detal_fcrea` DATE NULL,
	`detal_hcrea` TIME NULL
) ENGINE=MyISAM;

-- Volcando estructura para vista bordados_pedidos.v_lista
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `v_lista` (
	`lista_nombr` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`lista_talla` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`lista_npers` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`lista_aespa` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`lista_emoji` INT(11) NULL,
	`lista_imag` VARCHAR(150) NULL COLLATE 'utf8_general_ci',
	`categ_descr` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`pedig_codig` INT(11) NULL,
	`pedid_nume` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`categ_codig` INT(11) NULL,
	`usuar_codig` INT(11) NULL
) ENGINE=MyISAM;

-- Volcando estructura para vista bordados_pedidos.v_detalle
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `v_detalle`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_detalle` AS select `det`.`detal_codig` AS `detal_codig`,`det`.`detal_model` AS `detal_model`,`det`.`pedig_codig` AS `pedig_codig`,`det`.`pedid_nume` AS `pedid_nume`,`mode`.`model_descr` AS `modelo`,`cat`.`categ_descr` AS `categoria`,`var`.`varia_descr` AS `variante`,`opc`.`opcio_descr` AS `opcional`,`opc1`.`opcio_descr` AS `opcional1`,`col`.`color_descr` AS `color`,`man`.`color_descr` AS `manga`,`colm`.`color_descr` AS `colormanga`,`scat`.`categ_descr` AS `subcategoria`,`det`.`categ_codig` AS `categ_codig`,`det`.`scate_codig` AS `scate_codig`,`det`.`varia_codig` AS `varia_codig`,`det`.`opcio_codig` AS `opcio_codig`,`det`.`detal_pretin` AS `detal_pretin`,`det`.`detal_cierre` AS `detal_cierre`,`det`.`detal_bolsi` AS `detal_bolsi`,`det`.`detal_color` AS `detal_color`,`det`.`detal_manga` AS `detal_manga`,`det`.`detal_cmanga` AS `detal_cmanga`,`det`.`detal_monto` AS `detal_monto`,`det`.`usuar_codig` AS `usuar_codig`,`det`.`detal_fcrea` AS `detal_fcrea`,`det`.`detal_hcrea` AS `detal_hcrea` from (((((((((`pedido_detalle` `det` join `pedido_opcional` `opc1` on((`det`.`detal_pretin` = `opc1`.`opcio_codig`))) join `pedido_opcional` `opc` on((`opc`.`opcio_codig` = `det`.`opcio_codig`))) join `pedido_categoria` `cat` on((`cat`.`categ_codig` = `det`.`categ_codig`))) left join `pedido_variante` `var` on((`var`.`varia_codig` = `det`.`varia_codig`))) left join `pedido_color` `col` on((`col`.`color_codig` = `det`.`detal_color`))) join `pedido_color` `man` on((`man`.`color_codig` = `det`.`detal_manga`))) join `pedido_color` `colm` on((`colm`.`color_codig` = `det`.`detal_cmanga`))) left join `pedido_subcategoria` `scat` on((`scat`.`categ_codig` = `det`.`scate_codig`))) join `pedido_modelo` `mode` on((`mode`.`model_codig` = `det`.`detal_model`)));

-- Volcando estructura para vista bordados_pedidos.v_lista
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `v_lista`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_lista` AS select `pedido_lista`.`lista_nombr` AS `lista_nombr`,`pedido_lista`.`lista_talla` AS `lista_talla`,`pedido_lista`.`lista_npers` AS `lista_npers`,`pedido_lista`.`lista_aespa` AS `lista_aespa`,`pedido_lista`.`lista_emoji` AS `lista_emoji`,`pedido_lista`.`lista_imag` AS `lista_imag`,`pedido_categoria`.`categ_descr` AS `categ_descr`,`pedido_lista`.`pedig_codig` AS `pedig_codig`,`pedido_lista`.`pedid_nume` AS `pedid_nume`,`pedido_lista`.`categ_codig` AS `categ_codig`,`pedido_lista`.`usuar_codig` AS `usuar_codig` from (`pedido_lista` join `pedido_categoria` on((`pedido_lista`.`categ_codig` = `pedido_categoria`.`categ_codig`)));

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
