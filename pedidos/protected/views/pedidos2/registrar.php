<?php 
$conexion=Yii::app()->db;
  $sql="SELECT max(pedid_codig) as codigo from pedido_pedidos where tusua_codig=".Yii::app()->user->id['usuario']['codigo']." and pedid_statbl=1";
  

  $pedidos=$conexion->createCommand($sql)->queryRow();
                if($pedidos['codigo']==NULL){

                $sql="SELECT max(pedid_codig) as codigo,pedid_nume,pedid_canti from pedido_pedidos where tusua_codig=".Yii::app()->user->id['usuario']['codigo']." and pedid_statbl=0";
                        $pedidos=$conexion->createCommand($sql)->queryRow();
                         //$nroPedi= $pedidos['pedid_nume'];
                   if ($pedidos['codigo']!=NULL) {
                         $nroPedi= $pedidos['pedid_nume'];
                         $cantidad= $pedidos['pedid_canti'];
                         $codigoPedido=$nroPedi;
                         $disabled="disabled";
                        # code...
                    }else{

                         $letraAleatoria = chr(rand(ord('A'), ord('Z'))); 

                         $variable=mt_rand(0,9999);

                         $codigoPedido= $letraAleatoria.$variable;

                         }
                }
 ?>

<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Pedidos</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Pedidos</a></li>
            <li class="active">Registrar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Registrar Pedidos <? if ($nroPedi!=NULL){ echo " Al numero de orden ".$nroPedi;}?></h3>
        </div>
        <div class="panel-body" >
           
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Cantidad a Solicitar</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php echo CHtml::textField('cantidad', $cantidad, array('class' => 'form-control', 'placeholder' => "Cantidad",'disabled'=> $disabled)); ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Cantidad del modelo</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php echo CHtml::textField('cantidadModelo', '', array('class' => 'form-control', 'placeholder' => "Cantidad del Modelo")); ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Modelo</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php 
                                $sql="SELECT * FROM pedido_modelo order by model_descr asc";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'model_codig','model_descr');

                                echo CHtml::dropDownList('modelo', '', $data, array('class' => 'form-control', 'placeholder' => "Modelo",'prompt'=>'Seleccione...')); ?>

                            </div>
                        </div>
                    </div>

                     <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color del Cuerpo</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php 
                                $sql="SELECT * FROM pedido_color order by color_descr asc";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'color_codig','color_descr');

                                echo CHtml::dropDownList('color', '', $data, array('class' => 'form-control', 'placeholder' => "Modelo",'prompt'=>'Seleccione...')); ?>

                            </div>
                        </div>
                    </div>
                    
                </div>
                
                
                    <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-4 ">
                            <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                        </div>
                    </div>
                <input type="hidden" name="codigoPedido" value="<?=$codigoPedido?>">

            </form>

        </div><!-- form -->
    </div>  
</div>
<script>
/*$('#fnaci').mask('00/00/0000');
$('#desde').mask('00/00/0000');
$('#preci').mask('#.##0,00',{reverse: true,maxlength:false});*/

    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                descr: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Descripción" es obligatorio',
                        }
                    }
                },
                preci: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Precio" es obligatorio',
                        }
                    }
                },
                moned: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Moneda" es obligatorio',
                        }
                    }
                }



            }
        });
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'registrar',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>