<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Solicitudes por Area</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Reportes</a></li>
            <li><a href="#">Estadisticas</a></li>
            <li class="active">Solicitudes por Módulo</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php
$connection = Yii::app()->db;
if(Yii::app()->user->id['usuario']['permiso']==1){
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Criterio de Busqueda</h3>
    </div>
    <div class="panel-body" >
        <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'pedidos', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            $connection = Yii::app()->db;

        ?>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Número de Solicitud</label>
                    <?php echo CHtml::textField('numero', '', array('class' => 'form-control', 'placeholder' => "Nro del Solicitud")); ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Estatus</label>
                    <?php 
                        
                        $sql="SELECT * FROM solicitud_estatus_pago";
                        $result=$connection->createCommand($sql)->queryAll();
                        $data=CHtml::listData($result,'epago_codig','epago_descr');
                        echo CHtml::dropDownList('epago', '', $data,array('class' => 'form-control', 'placeholder' => "Descripción", 'prompt'=>'Seleccione...')); ?>
                </div>
            </div>
           
        </div>
        
        <div class="row">
            <div class="col-sm-6">
                <button type="button" id="buscar" class="btn btn-block btn-info">Buscar</button>
            </div>
            <div class="col-sm-6">
                <button type="reset" class="btn btn-block btn-default">Limpiar</button>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div>
<?php
}
?> 
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-sm-8">
                <h3 class="panel-title">Listado</h3>
            </div>
            <div class="col-sm-4">
                <a href="pdf" class="btn btn-info btn-block">Imprimir</a>
            </div>
        </div>
    </div>
    <div class="panel-body" >
         
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-pedido'>
                <table  id='auditoria'  class="display nowrap table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                #
                            </th>
                            <th>
                                Módulo
                            </th>
                            <th>
                                Cantidad Solicitudes
                            </th>
                            
                            <th>
                                Solicitudes con Retraso
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        //if(Yii::app()->user->id['usuario']['permiso']==1){


                        $sql="SELECT count(prere_codig) cantidad, c.smodu_codig, c.smodu_descr, c.smodu_icono, c.smodu_color
                              FROM rd_preregistro a 
                              JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig and a.estat_codig not in (0,4))
                              JOIN solicitud_modulo c ON (b.smodu_codig = c.smodu_codig)
                              WHERE prere_fcrea < '2019-09-01'
                              UNION
                              SELECT count(solic_codig) cantidad,  c.smodu_codig, c.smodu_descr, c.smodu_icono, c.smodu_color
                              FROM solicitud a 
                              JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig and a.estat_codig not in (0))
                              RIGHT JOIN solicitud_modulo c ON (b.smodu_codig = c.smodu_codig)
                              WHERE c.smodu_codig <> 1
                              AND solic_fcrea < '2019-09-01'
                              GROUP BY 2,3,4,5
                              ORDER BY 2";
                   
                        
                        
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                            $j++;
                            $total+=$row['cantidad'];
                            $retraso=$this->funciones->solicitudesRetraso($connection,$row['smodu_codig']);
                            $rtotal+=$retraso;
                        ?>
                        <tr>

                            <td class="tabla"><?php echo $i ?></td>
                            <td class="tabla"><?php echo $row['smodu_descr']; ?></td>
                            <td class="tabla"><?php echo $row['cantidad']; ?></td>
                            <td class="tabla"><?php echo $retraso; ?></td>
                            
                        </tr>
                        <?php
                            }   
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="2">Total</th>
                            <th ><?php echo $total; ?></th>
                            <th ><?php echo $rtotal; ?></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>  
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel'
            ]
        });

        $('#buscar').click(function () {
        var formData = new FormData($("#pedidos")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-pedido').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>