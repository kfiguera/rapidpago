<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Promedio por Estatus</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Reportes</a></li>
            <li><a href="#">Estadisticas</a></li>
            <li class="active">Promedio por Estatus</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php
$conexion = Yii::app()->db;
if(Yii::app()->user->id['usuario']['permiso']==1){
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Criterio de Busqueda</h3>
    </div>
    <div class="panel-body" >
        <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'pedidos', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            $connection = Yii::app()->db;

        ?>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Número de Solicitud</label>
                    <?php echo CHtml::textField('numero', '', array('class' => 'form-control', 'placeholder' => "Nro del Solicitud")); ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Estatus</label>
                    <?php 
                        
                        $sql="SELECT * FROM solicitud_estatus_pago";
                        $result=$connection->createCommand($sql)->queryAll();
                        $data=CHtml::listData($result,'epago_codig','epago_descr');
                        echo CHtml::dropDownList('epago', '', $data,array('class' => 'form-control', 'placeholder' => "Descripción", 'prompt'=>'Seleccione...')); ?>
                </div>
            </div>
           
        </div>
        
        <div class="row">
            <div class="col-sm-6">
                <button type="button" id="buscar" class="btn btn-block btn-info">Buscar</button>
            </div>
            <div class="col-sm-6">
                <button type="reset" class="btn btn-block btn-default">Limpiar</button>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div>
<?php
}
?> 
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-sm-8">
                <h3 class="panel-title">Listado</h3>
            </div>
            <div class="col-sm-4">
                <a href="pdf" class="btn btn-info btn-block">Imprimir</a>
            </div>
        </div>
    </div>
    <div class="panel-body" >
         
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-pedido'>
                <table id="auditoria" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th width="5%">#</th>
                        <th width="18%">Solicitud</th>
                        <th width="18%">Estatus</th>
                        <th width="18%">Fecha de Inicio</th>
                        <th width="18%">Fecha de Finalización </th>
                        <th width="18%">Tiempo de Diferencia </th>
                        <th width="18%">Usuario</th>
                        <th width="5%">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 

                        $sql="SELECT prere_codig, '' solic_codig, prere_numer solic_numer
                              FROM rd_preregistro a 
                              WHERE prere_codig not in (
                                SELECT prere_codig 
                                FROM solicitud b WHERE b.prere_codig=a.prere_codig)
                               UNION 
                               SELECT prere_codig, solic_codig, solic_numer 
                               FROM solicitud
                               ORDER BY 1";
                        $solicitudes=$conexion->createCommand($sql)->queryAll();
$i=0;
                        foreach ($solicitudes as $keys => $solicitud) {
                            
                            $a=0;
                            if($solicitud['prere_codig']){
                                $a++;
                                $add="a.prere_codig = '".$solicitud['prere_codig']."' ";
                            }
                            if($solicitud['solic_codig']){
                                $a++;
                                if($a>1){
                                    $add.="OR ";
                                }
                                $add.="a.solic_codig ='".$solicitud['prere_codig']."' ";
                            }
                            $add="WHERE ".$add;
                            $sql="SELECT prere_codig, solic_codig, traye_codig, estat_descr, a.estat_codig, traye_finic, traye_hinic, traye_ffina, traye_hfina, perso_pnomb, perso_papel
                                  FROM solicitud_trayectoria a 
                                  JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                                  JOIN seguridad_usuarios c ON (a.usuar_codig = c.usuar_codig)
                                  JOIN p_persona d ON (c.perso_codig = d.perso_codig)
                                  ".$add."
                                  
                                  GROUP BY traye_codig";
                            
                            $trayectorias=$conexion->createCommand($sql)->queryAll();

                            
                            foreach ($trayectorias as $key => $trayectoria) {
                                $i++;

                                $inicio=$trayectoria['traye_finic'].' '.$trayectoria['traye_hinic'];
                                $final=$trayectoria['traye_ffina'].' '.$trayectoria['traye_hfina'];
                                if($final == '0000-00-00 00:00:00'){
                                    $final=date('Y-m-d H:i:s');
                                }
                                $diff=$this->funciones->diferenciaHoras($inicio,$final);
                                $horas=($diff->days * 24 )  + ( $diff->h );
                                $letras=$this->funciones->diferenciaEnLetras($diff);
                                $semaforo=$this->funciones->semaforo($conexion,$trayectoria['estat_codig'],$horas);
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $solicitud['solic_numer']; ?></td>
                                    <td><?php echo $trayectoria['estat_descr']; ?></td>
                                    <td><?php echo $this->funciones->transformarFecha_v($trayectoria['traye_finic']).' '.$trayectoria['traye_hinic']; ?></td>
                                    <td><?php echo $this->funciones->transformarFecha_v($trayectoria['traye_ffina']).' '.$trayectoria['traye_hfina']; ?></td>
                                    <td><?php echo $letras; ?></td>
                                    <td><?php echo $trayectoria['perso_pnomb'].' '.$trayectoria['perso_papel']; ?></td>
                                    <td><?php echo $semaforo; ?></td>
                                </tr>
                                <?php
                            }
                        }
                    ?>    
                </tbody>
            </table> 
            <?php//exit;?>
            </div>
        </div>
    </div>
</div>  
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel'
            ]
        });

        $('#buscar').click(function () {
        var formData = new FormData($("#pedidos")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-pedido').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>