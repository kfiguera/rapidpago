<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

		<style>

			body {
				font-family: ipamp;
		 		font-size: 10pt;
		 	}
		 	
		 	td { 
		 		vertical-align: middle; 
		 	}
		 	p{
		 		text-align: justify;
		 	}
		 	.items td {
		 		/*border-left: 0.1mm solid #000000;
		 		border-right: 0.1mm solid #000000;*/
		 		border: 0.1mm solid #000000;
		 		font-size: 8pt;
		 		text-align: left;
		 		padding: 2pt;
		 	}
		 	.items td img {
		 		max-height: 300px;
		 		max-width: 300px
		 		width:100%;
		 		height: auto;
		 	}
		 	table thead td, table thead th, table tfoot td, table tfoot th { 
		 		background-color: #EEEEEE;
		 		text-align: center;
		 		border: 0.1mm solid #000000;
		 		font-size: 10pt;
		 		padding: 2pt;
		 	}
		 	.items td.blanktotal {
		 		background-color: #FFFFFF;
		 		border: 0mm none #000000;
		 		border-top: 0.1mm solid #000000;
		 	}
		 	.items td.totals {
		 		text-align: right;
		 		border: 0.1mm solid #000000;
		 	}
		 	.text-center{
		 		text-align: center;
		 	}
		 	.text-left{
		 		text-align: left;
		 	}
		 	.text-right{
		 		text-align: right;
		 	}
		 	.img-responsive{
		 		width: 100%;
		 		height: auto;
		 		max-width: 50px;
		 	}
		</style>
	</head>
	<body>
		<?php
			$connection = $conexion=yii::app()->db;

			$sql = "SELECT * FROM solicitud a
                                JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                                JOIN cliente c ON (a.clien_codig = c.clien_codig)
                                JOIN seguridad_usuarios d ON (a.usuar_codig = d.usuar_codig)
                                JOIN p_banco f ON (a.banco_codig = f.banco_codig)
                                JOIN p_persona e ON (d.perso_codig = e.perso_codig)
                              WHERE a.estat_codig in ('27')";
            $solicitudes = $conexion->createCommand($sql)->queryRow();


        ?>
 		<table class="items" width="100%" style="font-size: 8pt; border-collapse: collapse;" cellpadding="5">            
 			<thead>
                <tr>
                            <th width="2%">
                                #
                            </th>
                            <th>
                                Fecha de Entrega
                            </th>
                            <th>
                                Banco
                            </th>
                            <th>
                                Razón Social
                            </th>
                            <th>
                                Serial
                            </th> 
                            <th>
                                Afiliado
                            </th>
                            <th>
                                RIF
                            </th>
                            <th>
                                Terminal
                            </th>
                            <th>
                                Telefono
                            </th>

                            <th>
                                Estatus
                            </th>
                            
                        </tr>
            </thead>
            <tbody>
            	<?php

                        //if(Yii::app()->user->id['usuario']['permiso']==1){

                        $sql="SELECT a.solic_codig, b.entre_fentr, c.banco_descr, d.clien_rsoci, g.seria_numer, a.solic_cafil, d.clien_rifco, h.termi_numer, d.clien_tmovi, e.estat_descr, b.tentr_codig   FROM solicitud a
                            JOIN solicitud_entrega b ON (a.solic_codig = b.solic_codig)
                            JOIN p_banco c ON (a.banco_codig = c.banco_codig)
                            JOIN cliente d ON (a.clien_codig = d.clien_codig)
                            JOIN rd_preregistro_estatus e ON (a.estat_codig = e.estat_codig)
                            JOIN solicitud_asignacion f ON (a.solic_codig = f.solic_codig)
                            JOIN inventario_seriales g ON (f.asign_dispo = g.seria_codig)
                            JOIN solicitud_terminal h ON (f.termi_codig = h.termi_codig)
                              WHERE a.estat_codig in ('27')
                        ";
                   
                        /*}else{
                            $sql = "SELECT * FROM solicitud_pagos a 
                                    JOIN solicitud_estatus_pago b ON (a.epago_codig = b.epago_codig)
                                    JOIN solicitud c ON (a.solic_codig = c.solic_codig)
                                    JOIN seguridad_usuarios d ON (c.usuar_codig = d.usuar_codig)
                                    WHERE a.usuar_codig = '".Yii::app()->user->id['usuario']['codigo']."'
                                    AND c.estat_codig='10'";
                             
                        }*/
                        
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                            $j++;
                            $sql="SELECT * FROM cliente where clien_codig='".$row['clien_codig']."'";
                            $cliente=$connection->createCommand($sql)->queryRow();
                            $vip=array('1'=>'SI', '2'=>'NO');
                            $sql="SELECT * 
                                  FROM solicitud_trayectoria 
                                  WHERE solic_codig = '".$row['solic_codig']."'
                                    AND estat_codig = '".$row['estat_codig']."'";

                            $trayectoria = $connection->createCommand($sql)->queryRow();

                            $inicio=$trayectoria['traye_finic'].' '.$trayectoria['traye_hinic'];
                            $fin=date('Y-m-d H:i:s');
                            $diff=$this->funciones->diferenciaHoras($inicio,$fin);
                            $horas=($diff->days * 24 )  + ( $diff->h );
                            $semaforo=$this->funciones->semaforo($connection,$row['estat_codig'],$horas);
                        ?>
                        <tr>
                            
                            <td class="tabla"><?php echo $i ?></td>
                            <td><?php echo $this->funciones->transformarFecha_v($row['entre_fentr']); ?></td>
                            <td class="tabla"><?php echo $row['banco_descr']; ?></td>
                            <td class="tabla"><?php echo $row['clien_rsoci']; ?></td>
                            <td class="tabla"><?php echo $row['seria_numer']; ?></td>
                            <td class="tabla"><?php echo $row['solic_cafil']; ?></td>
                            <td class="tabla"><?php echo $row['clien_rifco']; ?></td>
                            <td class="tabla"><?php echo $row['termi_numer']; ?></td>
                            <td class="tabla"><?php echo $row['clien_tmovi']; ?></td>
                            <td class="tabla"><?php echo $row['estat_descr']; ?></td>
                            
                        </tr>
                        <?php
                            }   
                        ?>
            </tbody>
        </table>
    </body>
 </html>
 <?php  ?>