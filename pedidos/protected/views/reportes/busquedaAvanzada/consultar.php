<?php $conexion = Yii::app()->db; ?>
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Reportes</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Historico de Solicitudes</a></li>
            <li class="active">Consultar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>               
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Datos de la Solicitud</h3>
        </div>
        <div class="panel-body">
            <?php 
                $this->funciones->imprimirDatosSolicitud($solicitud);
            ?>
        </div>
    </div>  
                 
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Historico de la Solicitud</h3>
        </div>
        <div class="panel-body">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th width="5%">#</th>
                        <th width="18%">Solicitud</th>
                        <th width="18%">Estatus</th>
                        <th width="18%">Fecha de Inicio</th>
                        <th width="18%">Fecha de Finalización </th>
                        <th width="18%">Usuario</th>
                        <th width="5%">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $sql="SELECT * 
                              FROM solicitud_trayectoria a 
                              JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                              JOIN seguridad_usuarios c ON (a.usuar_codig = c.usuar_codig)
                              JOIN p_persona d ON (c.perso_codig = d.perso_codig)
                              WHERE a.prere_codig = '".$solicitud['prere_codig']."'
                                 OR a.solic_codig = '".$solicitud['solic_codig']."'
                              ORDER BY traye_codig";
                      
                        $trayectorias=$conexion->createCommand($sql)->queryAll();

                        
                        $i=0;
                        foreach ($trayectorias as $key => $trayectoria) {
                            $i++;

                            $inicio=$trayectoria['traye_finic'].' '.$trayectoria['traye_hinic'];
                            $final=$trayectoria['traye_ffina'].' '.$trayectoria['traye_hfina'];
                            if($final == '0000-00-00 00:00:00'){
                                $final=date('Y-m-d H:i:s');
                            }
                            $diff=$this->funciones->diferenciaHoras($inicio,$final);
                            $horas=($diff->days * 24 )  + ( $diff->h );

                            $semaforo=$this->funciones->semaforo($conexion,$trayectoria['estat_codig'],$horas);
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $solicitud['solic_numer']; ?></td>
                                <td><?php echo $trayectoria['estat_descr']; ?></td>
                                <td><?php echo $this->funciones->transformarFecha_v($trayectoria['traye_finic']).' '.$trayectoria['traye_hinic']; ?></td>
                                <td><?php echo $this->funciones->transformarFecha_v($trayectoria['traye_ffina']).' '.$trayectoria['traye_hfina']; ?></td>
                                <td><?php echo $trayectoria['perso_pnomb'].' '.$trayectoria['perso_papel']; ?></td>
                                <td><?php echo $semaforo; ?></td>
                            </tr>
                            <?php
                        }
                    ?>    
                </tbody>
            </table>    
        </div>
        
      <div class="panel-footer">
            <div class="row controls">
                <div class="col-sm-12">
                    <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                </div>
            </div>
        </div>
    </div> 
<script type="text/javascript">
$(document).ready(function(){
    var id = ['ccuer', 'cbols', 'cmang', 'cegor', 'cigor', 'cppre', 'ccier', 'ccurs', 'cnper', 'caesp', 'celec', 'cfras' ];
    var con = 1;
    id.forEach( function(valor, indice, array) {
        var numero = indice+1;
        $('#img-'+numero).popover({
          html: true,
          content: function() {
            if(numero==1){
                return $('#popover-img').html();
            }else{
                return $('#popover-img-'+numero).html();    
            }
            
          },
          trigger: 'hover'
        });
        $('#'+valor).change(function () {
            var emoji = $(this).val();
            $.ajax({  
                url:"<?php echo CController::createUrl('funciones/SolicitudesVerColor'); ?>",  
                method:"POST",  
                async:false,  
                data:{id:emoji},  
                success:function(data){  
                    if(numero=='1'){
                        $('#popover-img').html(data);
                    }else{
                        $('#popover-img-'+numero).html(data);    
                    } 
                }  
            });
            $('[data-toggle=popover]').popover('hide');
        });
    });
});
</script>


<script type="text/javascript">
$(document).ready(function(){
    $('#img-13').popover({
          html: true,
          content: function() {
            return $('#popover-img-13').html();
          },
          trigger: 'hover'
        });
});
$('#pfuen').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/SolicitudesVerFuente'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-13").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-14').popover({
          html: true,
          content: function() {
            return $('#popover-img-14').html();
          },
          trigger: 'hover'
        });
});
$('#pcolo').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/SolicitudesVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-14").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-15').popover({
          html: true,
          content: function() {
            return $('#popover-img-15').html();
          },
          trigger: 'hover'
        });
});
$('#gfuen').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/SolicitudesVerFuente'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-15").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-16').popover({
          html: true,
          content: function() {
            return $('#popover-img-16').html();
          },
          trigger: 'hover'
        });
});
$('#gcolo').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/SolicitudesVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-16").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
    $('#img-numero-1').popover({
          html: true,
          content: function() {
            return $('#popover-numero-1').html();
          },
          trigger: 'hover'
        });
});
</script>
<script type="text/javascript">
    $(document).ready(function(){
    $('#img-numero-2').popover({
          html: true,
          content: function() {
            return $('#popover-numero-2').html();
          },
          trigger: 'hover'
        });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-17').popover({
          html: true,
          content: function() {
            return $('#popover-img-17').html();
          },
          trigger: 'hover'
        });
});
$('#mfuen').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/SolicitudesVerFuente'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-17").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-18').popover({
          html: true,
          content: function() {
            return $('#popover-img-18').html();
          },
          trigger: 'hover'
        });
});
$('#mcolo').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/SolicitudesVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-18").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-19').popover({
          html: true,
          content: function() {
            return $('#popover-img-19').html();
          },
          trigger: 'hover'
        });
});
$('#eafue').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/SolicitudesVerFuente'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-19").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-20').popover({
          html: true,
          content: function() {
            return $('#popover-img-20').html();
          },
          trigger: 'hover'
        });
});
$('#eacol').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/SolicitudesVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-20").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-21').popover({
          html: true,
          content: function() {
            return $('#popover-img-21').html();
          },
          trigger: 'hover'
        });
});
$('#ebfue').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/SolicitudesVerFuente'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-21").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-22').popover({
          html: true,
          content: function() {
            return $('#popover-img-22').html();
          },
          trigger: 'hover'
        });
});
$('#ebcol').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/SolicitudesVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-22").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-23').popover({
          html: true,
          content: function() {
            return $('#popover-img-23').html();
          },
          trigger: 'hover'
        });
});
$('#aefue').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/SolicitudesVerFuente'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-23").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-24').popover({
          html: true,
          content: function() {
            return $('#popover-img-24').html();
          },
          trigger: 'hover'
        });
});
$('#aecol').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/SolicitudesVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-24").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
    $('#img-numero-3').popover({
          html: true,
          content: function() {
            return $('#popover-numero-3').html();
          },
          trigger: 'hover'
        });
});
</script>
<script type="text/javascript">
    $(document).ready(function(){
    $('#img-numero-4').popover({
          html: true,
          content: function() {
            return $('#popover-numero-4').html();
          },
          trigger: 'hover'
        });
});
</script>
<script type="text/javascript">
    $(document).ready(function(){
    $('#img-numero-6').popover({
          html: true,
          content: function() {
            return $('#popover-numero-6').html();
          },
          trigger: 'hover'
        });
});
</script>
<script type="text/javascript">
    $(document).ready(function(){
    $('#img-numero-5').popover({
          html: true,
          content: function() {
            return $('#popover-numero-5').html();
          },
          trigger: 'hover'
        });
});
</script>