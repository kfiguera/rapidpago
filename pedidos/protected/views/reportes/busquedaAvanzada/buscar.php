<table id='auditoria' class="table table-bordered table-hover dataTable">
    <thead>
        <tr>
            <th width="2%">
                #
            </th>
            <th>
                Nro Solicitud
            </th>
            <th>
                RIF del Comercio
            </th>
            <th>
                Código de Afiliado
            </th>
            <th>
                Banco
            </th>
            <th>
                Cliente
            </th>
            <th>
                Contacto
            </th>
            <th>
                Total POS a Solicitar
            </th>
            <th>
                Origen
            </th>
            <th>
                Usuario de Registro
            </th>
            <th>
                Fecha de Registro
            </th>
            <th width="5px">
                Estatus
            </th>
            <th width="5%">
                &nbsp;
            </th>
        </tr>
    </thead>

    <tbody>
        <?php
        $sql = "SELECT *
                FROM solicitud a 
                JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                JOIN cliente c ON (a.clien_codig = c.clien_codig)
                JOIN seguridad_usuarios d ON (a.usuar_codig = d.usuar_codig)
                JOIN p_persona e ON (d.perso_codig = e.perso_codig)
                ".$condicion;
   
        //echo $sql;
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $p_persona = $command->query();
        $i=0;
        while (($row = $p_persona->read()) !== false) {
            $i++;
                            $j++;

                            $sql="SELECT * 
                                  FROM solicitud_trayectoria 
                                  WHERE solic_codig = '".$row['solic_codig']."'
                                    AND estat_codig = '".$row['estat_codig']."'";

                            $trayectoria = $connection->createCommand($sql)->queryRow();

                            $inicio=$trayectoria['traye_finic'].' '.$trayectoria['traye_hinic'];
                            $fin=date('Y-m-d H:i:s');
                            $diff=$this->funciones->diferenciaHoras($inicio,$fin);
                            $horas=($diff->days * 24 )  + ( $diff->h );
                            $semaforo=$this->funciones->semaforo($connection,$row['estat_codig'],$horas);
                            
                            $sql = "SELECT sum(cequi_canti) cantidad
                                FROM solicitud_equipos a 
                                WHERE solic_codig='".$row['solic_codig'] ."'";
                            $cequipo = $connection->createCommand($sql)->queryRow();
                            
                            $sql = "SELECT *
                                FROM p_banco 
                                WHERE banco_codig='".$row['banco_codig'] ."'";
                            $banco = $connection->createCommand($sql)->queryRow();
                            $sql = "SELECT *
                                FROM p_solicitud_origen
                                WHERE orige_codig='".$row['orige_codig'] ."'";
                            $origen = $connection->createCommand($sql)->queryRow();
                        ?>
                        <tr>
                            <td><?php echo $i ?></td>

                            <td><?php echo $row['solic_numer'] ?></td>
                            <td><?php echo $row['clien_rifco'] ?></td>
                            <td><?php echo $row['solic_cafil'] ?></td>
                            <td><?php echo $banco['banco_descr'] ?></td>
                            <td><?php echo $row['clien_rsoci'] ?></td>
                            <td><?php echo $row['solic_celec'] ?></td>
                            <td><?php echo $cequipo['cantidad'] ?></td>
                            <td><?php echo $origen['orige_descr'] ?></td>
                            <td class="tabla"><?php echo $row['perso_pnomb'].' '.$row['perso_papel'] ?></td>
                            <td><?php echo $this->funciones->transformarFecha_v($row['solic_fcrea']).' '.$row['solic_hcrea'] ?></td>
                            <td><?php echo $semaforo.' '.$row['estat_descr'] ?></td>

                            <!--td><a href="../detalle/listado?p=<?php echo $row['prere_codig'] ?>" class="btn btn-block btn-info"><i class="glyphicon glyphicon-list-alt"></i></a></td-->
                            <td>
                                <a href="consultar?c=<?php echo $row['solic_codig'] ?>" class="btn btn-block btn-info"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Consultar" >
                                    <i class="fa fa-search"></i>
                                </a>
                            </td>
                            <!--td>
                                <a href="modificar?c=<?php echo $row['solic_codig'] ?>&s=1" class="btn btn-block btn-info"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Modificar" >
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </td>

                            <!--td>
                                <a href="verificar?c=<?php echo $row['solic_codig'] ?>" class="btn btn-block btn-warning"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Verificar">
                                    <i class="fa fa-check-square-o"></i>
                                </a>
                            </td-->

                            <!--td>
                                <a href="anular?c=<?php echo $row['prere_codig'] ?>" target="_blank" class="btn btn-block btn-danger">
                                    <i class="fa fa-close"></i>
                                </a>
                            </td-->
                        </tr>
        <?php
            }   
        ?>
    </tbody>
</table>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable(); 
    });
</script>
