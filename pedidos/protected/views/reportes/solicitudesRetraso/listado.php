<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Solicitudes Pagadas con Retraso</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Reportes</a></li>
            <li><a href="#">Solicitudes Pagadas con Retraso</a></li>
            <li class="active">Listado</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php
$connection = Yii::app()->db;
if(Yii::app()->user->id['usuario']['permiso']==1){
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Criterio de Busqueda</h3>
    </div>
    <div class="panel-body" >
        <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'pedidos', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            $connection = Yii::app()->db;

        ?>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Número de Solicitud</label>
                    <?php echo CHtml::textField('numero', '', array('class' => 'form-control', 'placeholder' => "Nro del Solicitud")); ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Estatus</label>
                    <?php 
                        
                        $sql="SELECT * FROM solicitud_estatus_pago";
                        $result=$connection->createCommand($sql)->queryAll();
                        $data=CHtml::listData($result,'epago_codig','epago_descr');
                        echo CHtml::dropDownList('epago', '', $data,array('class' => 'form-control', 'placeholder' => "Descripción", 'prompt'=>'Seleccione...')); ?>
                </div>
            </div>
           
        </div>
        
        <div class="row">
            <div class="col-sm-6">
                <button type="button" id="buscar" class="btn btn-block btn-info">Buscar</button>
            </div>
            <div class="col-sm-6">
                <button type="reset" class="btn btn-block btn-default">Limpiar</button>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div>
<?php
}
?> 
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-sm-8">
                <h3 class="panel-title">Listado</h3>
            </div>
            <div class="col-sm-4">
                <a href="pdf" class="btn btn-info btn-block">Imprimir</a>
            </div>
        </div>
    </div>
    <div class="panel-body" >
         
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-pedido'>
                <table  id='auditoria'  class="display nowrap table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                #
                            </th>
                            <th>Solicitud</th>
                            <th>
                                Razón Social
                            </th>
                            <th>Estatus</th>
                            <th>Fecha de Pago</th>
                            <th>Tiempo en Curso</th>
                            <th>Plazo</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        //if(Yii::app()->user->id['usuario']['permiso']==1){


                        /*$sql="SELECT a.solic_codig, b.entre_fentr, c.banco_descr, d.clien_rsoci, g.seria_numer, a.solic_cafil, d.clien_rifco, h.termi_numer, d.clien_tmovi, e.estat_descr, b.tentr_codig   FROM solicitud a
                            JOIN solicitud_entrega b ON (a.solic_codig = b.solic_codig)
                            JOIN p_banco c ON (a.banco_codig = c.banco_codig)
                            JOIN cliente d ON (a.clien_codig = d.clien_codig)
                            JOIN rd_preregistro_estatus e ON (a.estat_codig = e.estat_codig)
                            JOIN solicitud_asignacion f ON (a.solic_codig = f.solic_codig)
                            JOIN inventario_seriales g ON (f.asign_dispo = g.seria_codig)
                            JOIN solicitud_terminal h ON (f.termi_codig = h.termi_codig)
                              WHERE a.estat_codig in ('27')
                        ";*/
                        $sql="SELECT *
                            FROM solicitud a
                            JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                            JOIN cliente c ON (a.clien_codig = c.clien_codig)
                            WHERE a.estat_codig NOT IN (0,27)
                              AND a.estat_codig >= '11'
                            GROUP BY solic_codig;";
                        /*}else{
                            $sql = "SELECT * FROM solicitud_pagos a 
                                    JOIN solicitud_estatus_pago b ON (a.epago_codig = b.epago_codig)
                                    JOIN solicitud c ON (a.solic_codig = c.solic_codig)
                                    JOIN seguridad_usuarios d ON (c.usuar_codig = d.usuar_codig)
                                    WHERE a.usuar_codig = '".Yii::app()->user->id['usuario']['codigo']."'
                                    AND c.estat_codig='10'";
                             
                        }*/
                        
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                            $j++;
                            $sql="SELECT *
                              FROM solicitud_trayectoria a
                              WHERE estat_codig ='11'
                                AND solic_codig='".$row['solic_codig']."'";
                                $traye=$connection->createCommand($sql)->queryRow();
                                $inicio=$traye["traye_finic"].' '.$traye["traye_hinic"];
                                $fin=date('Y-m-d H:i:s');
                                $diff=$this->funciones->diferenciaHoras($inicio, $fin);
                                $diferenciaHoras=$this->funciones->diferenciaEnLetras($diff);
                                $horas=($diff->days * 24 )  + ( $diff->h );

                                $sql="SELECT *
                                      FROM p_dias_entrega a";
                                $entrega=$connection->createCommand($sql)->queryRow();
                                if($horas > ($entrega['dentr_value'] * 24)){
                                    $retraso++;
                                    $marca='RETRASO';
                                }else{
                                    $atiempo++;
                                    $marca='A TIEMPO';
                                }
                        ?>
                        <tr>

                            <td class="tabla"><?php echo $i ?></td>
                            <td class="tabla"><a  href="consultar?c=<?php echo $row['solic_codig'] ?>"><?php echo $row['solic_numer'] ?></a></td>


                            <td><?php echo $row["clien_rsoci"]; ?></td>

                            <td><?php echo $row["estat_descr"]; ?></td>
                            <td><?php echo $this->funciones->TransformarFecha_v($traye["traye_finic"]).' '.$traye["traye_hinic"]; ?></td>
                            <td><?php echo $diferenciaHoras; ?></td>

                            <td><?php echo $marca; ?></td>
                            
                        </tr>
                        <?php
                            }   
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>  
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel'
            ]
        });

        $('#buscar').click(function () {
        var formData = new FormData($("#pedidos")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-pedido').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>