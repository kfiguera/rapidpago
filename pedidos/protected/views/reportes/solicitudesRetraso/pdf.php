<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

		<style>

			body {
				font-family: ipamp;
		 		font-size: 10pt;
		 	}
		 	
		 	td { 
		 		vertical-align: middle; 
		 	}
		 	p{
		 		text-align: justify;
		 	}
		 	.items td {
		 		/*border-left: 0.1mm solid #000000;
		 		border-right: 0.1mm solid #000000;*/
		 		border: 0.1mm solid #000000;
		 		font-size: 8pt;
		 		text-align: left;
		 		padding: 2pt;
		 	}
		 	.items td img {
		 		max-height: 300px;
		 		max-width: 300px
		 		width:100%;
		 		height: auto;
		 	}
		 	table thead td, table thead th, table tfoot td, table tfoot th { 
		 		background-color: #EEEEEE;
		 		text-align: center;
		 		border: 0.1mm solid #000000;
		 		font-size: 10pt;
		 		padding: 2pt;
		 	}
		 	.items td.blanktotal {
		 		background-color: #FFFFFF;
		 		border: 0mm none #000000;
		 		border-top: 0.1mm solid #000000;
		 	}
		 	.items td.totals {
		 		text-align: right;
		 		border: 0.1mm solid #000000;
		 	}
		 	.text-center{
		 		text-align: center;
		 	}
		 	.text-left{
		 		text-align: left;
		 	}
		 	.text-right{
		 		text-align: right;
		 	}
		 	.img-responsive{
		 		width: 100%;
		 		height: auto;
		 		max-width: 50px;
		 	}
		</style>
	</head>
	<body>
		<?php
			$connection = $conexion=yii::app()->db;

			$sql = "SELECT * FROM solicitud a
                                JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                                JOIN cliente c ON (a.clien_codig = c.clien_codig)
                                JOIN seguridad_usuarios d ON (a.usuar_codig = d.usuar_codig)
                                JOIN p_banco f ON (a.banco_codig = f.banco_codig)
                                JOIN p_persona e ON (d.perso_codig = e.perso_codig)
                              WHERE a.estat_codig in ('27')";
            $solicitudes = $conexion->createCommand($sql)->queryRow();


        ?>
 		<table class="items" width="100%" style="font-size: 8pt; border-collapse: collapse;" cellpadding="5">            
 			<thead>
                <tr>
                    <th width="2%">
                        #
                    </th>
                    <th>Solicitud</th>
                    <th>
                        Razón Social
                    </th>
                    <th>Estatus</th>
                    <th>Fecha de Pago</th>
                    <th>Tiempo en Curso</th>
                    <th>Plazo</th>
                    
                </tr>
            </thead>
            <tbody>
            	<?php

                        //if(Yii::app()->user->id['usuario']['permiso']==1){
                        
                        $sql="SELECT *
                            FROM solicitud a
                            JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                            JOIN cliente c ON (a.clien_codig = c.clien_codig)
                            WHERE a.estat_codig NOT IN (0,27)
                              AND a.estat_codig >= '11'
                            GROUP BY solic_codig;";
                   
                        /*}else{
                            $sql = "SELECT * FROM solicitud_pagos a 
                                    JOIN solicitud_estatus_pago b ON (a.epago_codig = b.epago_codig)
                                    JOIN solicitud c ON (a.solic_codig = c.solic_codig)
                                    JOIN seguridad_usuarios d ON (c.usuar_codig = d.usuar_codig)
                                    WHERE a.usuar_codig = '".Yii::app()->user->id['usuario']['codigo']."'
                                    AND c.estat_codig='10'";
                             
                        }*/
                        
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                            $j++;
                            $sql="SELECT *
                              FROM solicitud_trayectoria a
                              WHERE estat_codig ='11'
                                AND solic_codig='".$row['solic_codig']."'";
                                $traye=$connection->createCommand($sql)->queryRow();
                                $inicio=$traye["traye_finic"].' '.$traye["traye_hinic"];
                                $fin=date('Y-m-d H:i:s');
                                $diff=$this->funciones->diferenciaHoras($inicio, $fin);
                                $diferenciaHoras=$this->funciones->diferenciaEnLetras($diff);
                                $horas=($diff->days * 24 )  + ( $diff->h );

                                $sql="SELECT *
                                      FROM p_dias_entrega a";
                                $entrega=$connection->createCommand($sql)->queryRow();
                                if($horas > ($entrega['dentr_value'] * 24)){
                                    $retraso++;
                                    $marca='RETRASO';
                                }else{
                                    $atiempo++;
                                    $marca='A TIEMPO';
                                }
                        ?>
                        <tr>

                            <td class="tabla"><?php echo $i ?></td>
                            <td class="tabla"><a  href="consultar?c=<?php echo $row['solic_codig'] ?>"><?php echo $row['solic_numer'] ?></a></td>


                            <td><?php echo $row["clien_rsoci"]; ?></td>

                            <td><?php echo $row["estat_descr"]; ?></td>
                            <td><?php echo $this->funciones->TransformarFecha_v($traye["traye_finic"]).' '.$traye["traye_hinic"]; ?></td>
                            <td><?php echo $diferenciaHoras; ?></td>

                            <td><?php echo $marca; ?></td>
                            
                        </tr>
                        <?php
                            }   
                        ?>
            </tbody>
        </table>
    </body>
 </html>
 <?php  ?>