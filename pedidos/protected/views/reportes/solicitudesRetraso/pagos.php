<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

		<style>

			body {
				font-family: ipamp;
		 		font-size: 10pt;
		 	}
		 	p { 
		 		margin: 0pt;
		 	}
		 	td { 
		 		vertical-align: middle; 
		 	}
		 	.items td {
		 		/*border-left: 0.1mm solid #000000;
		 		border-right: 0.1mm solid #000000;*/
		 		border: 0.1mm solid #000000;
		 		font-size: 10pt;
		 		text-align: left;
		 		padding: 2pt;
		 	}
		 	.items td img {
		 		max-height: 300px;
		 		max-width: 300px
		 		width:100%;
		 		height: auto;
		 	}
		 	table thead td, table thead th, table tfoot td, table tfoot th { 
		 		background-color: #EEEEEE;
		 		text-align: center;
		 		border: 0.1mm solid #000000;
		 		font-size: 10pt;
		 		padding: 2pt;
		 	}
		 	.items td.blanktotal {
		 		background-color: #FFFFFF;
		 		border: 0mm none #000000;
		 		border-top: 0.1mm solid #000000;
		 	}
		 	.items td.totals {
		 		text-align: right;
		 		border: 0.1mm solid #000000;
		 	}
		 	.text-center{
		 		text-align: center;
		 	}
		 	.text-left{
		 		text-align: left;
		 	}
		 	.text-right{
		 		text-align: right;
		 	}
		 	.img-responsive{
		 		width: 100%;
		 		height: auto;
		 		max-width: 50px;
		 	}
		</style>
	</head>
	<body>
		<?php
			$conexion=yii::app()->db;

			$sql = "SELECT * FROM solicitud_pagos WHERE pagos_codig = '".$c."'";
            $pagos = $conexion->createCommand($sql)->queryRow();

            $sql = "SELECT * FROM solicitud WHERE solic_codig = '".$pagos['solic_codig']."'";
            $pedidos = $conexion->createCommand($sql)->queryRow();

            $sql = "SELECT * FROM p_pago_tipo WHERE ptipo_codig = '".$pagos['ptipo_codig']."'";
            $fpago = $conexion->createCommand($sql)->queryRow();

            $sql = "SELECT * FROM seguridad_usuarios WHERE usuar_codig = '".$pedidos['usuar_codig']."'";
            $usuario = $conexion->createCommand($sql)->queryRow();

            $sql = "SELECT * FROM p_persona WHERE perso_codig = '".$usuario['perso_codig']."'";
            $p_persona = $conexion->createCommand($sql)->queryRow();

			$sql = "SELECT * 
					FROM colegio a 
					WHERE a.coleg_codig='".$usuario['coleg_codig']."'";
			
            $colegio = $conexion->createCommand($sql)->queryRow();
            $sql = "SELECT * 
					FROM colegio_contacto a 
					JOIN colegio b ON (a.coleg_codig = b.coleg_codig)
					JOIN p_persona c ON (a.perso_codig = c.perso_codig)
					WHERE a.usuar_codig='".$pedidos['usuar_codig']."'";

            $contacto = $conexion->createCommand($sql)->queryAll();
            
        ?>
        <table class="items" width="100%" style="font-size: 8pt; border-collapse: collapse;" cellpadding="5">
        	<tr>
        		<td width="70%" colspan="2" align="left"><b>Polerones Tiempo</td>
        		<td width="15%" align="left"><b>NRO DE PAGO:</b></td>
        		<td width="15%" align="left"><?php echo $pagos['pagos_numer'];?></td>
        	</tr>
        	<tr>
        		<td width="15%"><b>DIRECCIÓN:</b></td>
        		<td colspan="3">NONATO COO 4217, PUENTE ALTO, REGIÓN METROPOLITANA, CHILE</td>
        	</tr>
        	<tr>
        		<td align="left"><b>TELÉFONO:</b></td>
        		<td align="left">+56963408065</td>
        		<td align="left"><b>FECHA:</b></td> 
        		<td align="left"><?php echo $this->funciones->transformarFecha_v($pagos['pagos_fcrea']);?></td>
        	</tr>
        </table>
        <table class="items" width="100%" style="font-size: 8pt; border-collapse: collapse;" cellpadding="5">
        	<tr>
        		<td width="25%" align="left"><b>NOMBRE DEL CLIENTE:</b></td>
        		<td width="25%" align="left"><?php echo $p_persona['perso_pnomb'].' '.$p_persona['perso_papel'] ?></td>
        		<td width="25%" align="left"><b>COLEGIO:</b></td> 
        		<td width="25%" align="left"><?php echo $colegio['coleg_nombr'];?></td>
        	</tr>
        	<tr>
        		<td align="left"><b>NUMERO DE PEDIDO:</b></td>
        		<td align="left"><?php echo $pedidos['solic_numer'] ?></td>
        		<td align="left"><b>MONTO DEL PEDIDO:</b></td> 
        		<td align="left"><?php echo $this->funciones->transformarMonto_v($pedidos['solic_monto']);?></td>
        	</tr>
        </table>
		<table class="items" width="100%" style="font-size: 10pt; border-collapse: collapse;" cellpadding="5">
			<thead>
				<tr>
					
					<td width="25%"><b>REFERENCIA:</b></td>
					<td width="25%"><b>FORMA DE PAGO:</b></td>
					<td width="25%"><b>CONCEPTO:</b></td>
					<td width="25%"><b>MONTO:</b></td>
				</tr>
			</thead>
			<tbody>
				
				<tr>
					<td><?php echo $pagos['pagos_refer'] ?></td>
					<td><?php echo $fpago['ptipo_descr'] ?></td>
					<td><?php echo $pagos['pagos_obser'] ?></td>
					<td align="right"><?php echo $this->funciones->transformarMonto_v($pagos['pagos_monto'],0) ?></td>
				</tr>
				
		 	</tbody>
		 	<tfoot>
				<tr>
					<th colspan="3">TOTAL</th>
					<th align="right"><?php echo $this->funciones->transformarMonto_v($pagos['pagos_monto'],0) ?></th>
				</tr>
		 	</tfoot>	 
	    </table>
	    
    </body>
 </html>