<html>
	<head>
		<style>
			body {
				font-family: sans-serif;
		 		font-size: 10pt;
		 	}
		 	p { 
		 		margin: 0pt;
		 	}
		 	td { 
		 		vertical-align: bottom; 
		 	}
		 	.items td {
		 		/*border-left: 0.1mm solid #000000;
		 		border-right: 0.1mm solid #000000;*/
		 		border: 0.1mm solid #000000;
		 		font-size: 4pt;
		 		text-align: center;
		 		padding: 1pt;
		 	}
		 	table thead td, table thead th { 
		 		background-color: #EEEEEE;
		 		text-align: center;
		 		border: 0.1mm solid #000000;
		 		font-size: 4pt;
		 		padding: 2pt;
		 	}
		 	.items td.blanktotal {
		 		background-color: #FFFFFF;
		 		border: 0mm none #000000;
		 		border-top: 0.1mm solid #000000;
		 	}
		 	.items td.totals {
		 		text-align: right;
		 		border: 0.1mm solid #000000;
		 	}
		</style>
	</head>
	<body>
		<!--div style="text-align: right"><b>Fecha: </b><?php echo date("d/m/Y"); ?> </div>
		<b>Total Registros:</b> 12-->
		 
		 <table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse;" cellpadding="5">
		 <thead>
		 <tr>
		 	<th width="1%">#</th>
            <th width="15,66%">FECHA EGRESO</th>
            <th width="16,66%">MONTO EGRESO</th>
            <th width="16,66%">NRO FACTURA</th>
            <th width="16,66%">NRO REFERENCIA</th>
            <th width="16,66%">MOTIVO</th>
            <th width="16,66%">BANCO</th>

		 </tr>
		 </thead>
		 <tbody>
		 <?php
            $sql = "SELECT *
                    FROM egresos a
                    JOIN p_banco b ON (a.banco_codig = b.banco_codig)
                ".$condicion;
            $connection=yii::app()->db;
            $command = $connection->createCommand($sql);
            $p_persona = $command->query();
            $i=0;
            while (($row = $p_persona->read()) !== false) {
                $i++;
            ?>
            <tr>
                <td><?php echo $i ?></td>
                            <td><?php echo $this->funciones->TransformarFecha_v($row['egres_fegre']) ?></td>
                            <td><?php echo $this->funciones->TransformarMonto_v($row['egres_monto'],2) ?></td>
                            <td><?php echo $this->funciones->TransformarMonto_v($row['egres_nfact'],0) ?></td>
                            <td><?php echo $row['egres_refer'] ?></td>
                            <td><?php echo $row['egres_motiv'] ?></td>
                            <td><?php echo $row['banco_descr'] ?></td>
            </tr>
            <?php
            $total+=$row['egres_monto'];
                }   
            ?>
		 <!-- FIN ITEMS -->
		 <tr>
		 <td class="blanktotal" colspan="7"></td>
		 </tr>
		 </tbody>
		 <tfoot>
		 	<tr>
		 		<td colspan="6">Total Egresos</td>
		 		<td><?php echo $this->funciones->TransformarMonto_v($total,2) ?></td>
		 	</tr>
		 </tfoot>
		 </table>

	 </body>
 </html>
