<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Formato Vportal</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Configuración</a></li>
            <li><a href="#">Formato Vportal</a></li>
            <li class="active">Listado</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php
$connection = Yii::app()->db;
if(Yii::app()->user->id['usuario']['permiso']==1){
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Criterio de Busqueda</h3>
    </div>
    <div class="panel-body" >
        <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'pedidos', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            $connection = Yii::app()->db;

        ?>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Número de Solicitud</label>
                    <?php echo CHtml::textField('numero', '', array('class' => 'form-control', 'placeholder' => "Nro del Solicitud")); ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Estatus</label>
                    <?php 
                        
                        $sql="SELECT * FROM solicitud_estatus_pago";
                        $result=$connection->createCommand($sql)->queryAll();
                        $data=CHtml::listData($result,'epago_codig','epago_descr');
                        echo CHtml::dropDownList('epago', '', $data,array('class' => 'form-control', 'placeholder' => "Descripción", 'prompt'=>'Seleccione...')); ?>
                </div>
            </div>
           
        </div>
        
        <div class="row">
            <div class="col-sm-6">
                <button type="button" id="buscar" class="btn btn-block btn-info">Buscar</button>
            </div>
            <div class="col-sm-6">
                <button type="reset" class="btn btn-block btn-default">Limpiar</button>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div>
<?php
}
?> 
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-sm-8">
                <h3 class="panel-title">Listado</h3>
            </div>
            <div class="col-sm-4">
                <a href="pdf" class="btn btn-info btn-block">Imprimir</a>
            </div>
        </div>
    </div>
    <div class="panel-body" >
         
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-pedido'>
                <table  id='auditoria'  class="display nowrap table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                #
                            </th>
                            <th>Nº SOLICITUD</th>
                            <th>RIF</th>
                            <th>RAZON SOCIAL</th>
                            <th>NOMBRE DE FANTASIA</th>
                            <th>ACTIVIDAD COMERCIAL</th>
                            <th>ESTADO</th>
                            <th>TELEFONOS</th>
                            <th>PERSONA DE CONTACTO</th>
                            <th>CORREO</th>
                            <th>BANCO</th>
                            <th>Nº AFILIADO</th>
                            <th>Nº DE TERMINAL</th>
                            <th>OPERADORA</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        //if(Yii::app()->user->id['usuario']['permiso']==1){

                        $sql="SELECT a.solic_codig, b.clien_codig, a.solic_numer, b.clien_rifco, b.clien_rsoci, b.clien_nfant, c.acome_descr, e.estad_descr, b.clien_tmovi, a.solic_celec,  f.banco_descr, a.solic_cafil
                            FROM solicitud a
                            JOIN cliente b ON (a.clien_codig = b.clien_codig)
                            JOIN p_actividad_comercial c ON (b.acome_codig = c.acome_codig)
                            JOIN cliente_direccion d ON (d.clien_codig = b.clien_codig)
                            JOIN p_estados e ON (d.estad_codig = e.estad_codig)
                            JOIN p_banco f ON (a.banco_codig = f.banco_codig)
                            WHERE a.estat_codig in ('11')
                            
                            ORDER BY 1
                        ";
                        
                        
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                            $j++;
                            $sql="SELECT * FROM cliente where clien_codig='".$row['clien_codig']."'";
                            $cliente=$connection->createCommand($sql)->queryRow();
                            $vip=array('1'=>'SI', '2'=>'NO');
                            $sql="SELECT * 
                                  FROM solicitud_trayectoria 
                                  WHERE solic_codig = '".$row['solic_codig']."'
                                    AND estat_codig = '".$row['estat_codig']."'";

                            $trayectoria = $connection->createCommand($sql)->queryRow();

                            $inicio=$trayectoria['traye_finic'].' '.$trayectoria['traye_hinic'];
                            $fin=date('Y-m-d H:i:s');
                            $diff=$this->funciones->diferenciaHoras($inicio,$fin);
                            $horas=($diff->days * 24 )  + ( $diff->h );
                            $semaforo=$this->funciones->semaforo($connection,$row['estat_codig'],$horas);

                            $sql="SELECT * 
                                  FROM cliente_representante 
                                  WHERE clien_codig = '".$row['clien_codig']."'";

                            $cliente = $connection->createCommand($sql)->queryRow();

                            $termi=$this->funciones->verTerminalesSolicitudes($connection,$row['solic_codig']);
                            $opera=$this->funciones->verOperadoresSolicitudes($connection,$row['solic_codig']);
                        ?>
                        <tr>

                            <td class="tabla"><?php echo $i ?></td>
                            <td class="tabla"><a  href="consultar?c=<?php echo $row['solic_codig'] ?>"><?php echo $row['solic_numer'] ?></a></td>
                            <td class="tabla"><?php echo $row['clien_rifco']; ?></td>
                            <td class="tabla"><?php echo $row['clien_rsoci']; ?></td>
                            <td class="tabla"><?php echo $row['clien_nfant']; ?></td>
                            <td class="tabla"><?php echo $row['acome_descr']; ?></td>
                            <td class="tabla"><?php echo $row['estad_descr']; ?></td>
                            <td class="tabla"><?php echo $row['clien_tmovi']; ?></td>
                            <td class="tabla"><?php echo $cliente['rlega_nombr'].' '.$cliente['rlega_apell']; ?></td>
                            <td class="tabla"><?php echo $row['solic_celec']; ?></td>
                            <td class="tabla"><?php echo $row['banco_descr']; ?></td>
                            <td class="tabla"><?php echo $row['solic_cafil']; ?></td>
                            <td class="tabla"><?php echo $termi ?></td>
                            <td class="tabla"><?php echo $opera; ?></td>
                            
                        </tr>
                        <?php
                            }   
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>  
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel'
            ]
        });

        $('#buscar').click(function () {
        var formData = new FormData($("#pedidos")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-pedido').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>