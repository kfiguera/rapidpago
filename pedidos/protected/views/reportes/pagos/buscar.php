<table id='auditoria' class="table table-bordered table-hover dataTable">
    <thead>
        <tr>
            <th width="2%">
                #
            </th>
            <th>
                Nro Pedido
            </th>
            <th>
                Usuario
            </th>
            <th>
                Colegio
            </th>
            <th>
                Nro Pago
            </th>
            <th>
                Monto
            </th>
            <th>
                Referencia
            </th>
            
            <th>
                Estatus
            </th>
            <th width="5%">
                Comprobante
            </th>
            <th width="5%">
                Imprimir
            </th>
            <?php
                if(Yii::app()->user->id['usuario']['permiso']==1){
            ?>
            <th width="5%">
                Aprobar
            </th>
            <th width="5%">
                Rechazar
            </th>
            <?php } ?>
        </tr>
    </thead>

    <tbody>
        <?php
        $sql = "SELECT * FROM solicitud_pagos a 
                JOIN solicitud_estatus_pago b ON (a.epago_codig = b.epago_codig)
                JOIN solicitud c ON (a.solic_codig = c.solic_codig)
                JOIN seguridad_usuarios d ON (c.usuar_codig = d.usuar_codig)
                LEFT JOIN colegio e ON (d.coleg_codig = e.coleg_codig)  
                ".$condicion;
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $p_persona = $command->query();
        $i=0;
        while (($row = $p_persona->read()) !== false) {
            $i++;
        ?>
        <tr>
            <td class="tabla"><?php echo $i ?></td>

            <td class="tabla"><?php echo $row['solic_numer'] ?></td>
            <td class="tabla"><?php echo $row['usuar_login'] ?></td>
            <td class="tabla"><?php echo $row['coleg_nombr'] ?></td>
            <td class="tabla"><?php echo $row['pagos_numer'] ?></td>
            <td class="tabla"><?php echo $this->funciones->transformarMonto_v($row['pagos_monto'],2) ?></td>
            <td class="tabla"><?php echo $row['pagos_refer'] ?></td>
            <td class="tabla"><?php echo $row['epago_descr'] ?></td>

            <!--td class="tabla"><a href="../detalle/listado?p=<?php echo $row['solic_codig'] ?>" class="btn btn-block btn-info"><i class="glyphicon glyphicon-list-alt"></i></a></td-->
            <td class="tabla"><a href="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$row['pagos_rimag'] ?>" target="_blank" class="btn btn-block btn-info"><i class="fa fa-search"></i></a></td>
            <td class="tabla"><a href="../../reportes/pedidos/pagos?c=<?php echo $row['pagos_codig'] ?>" target="_blank" class="btn btn-block btn-info"><i class="fa fa-file-pdf-o"></i></a></td>
            <?php
                if(Yii::app()->user->id['usuario']['permiso']==1){
            ?>
                <td class="tabla"><a href="aprobar?c=<?php echo $row['pagos_codig'] ?>" target="_blank" class="btn btn-block btn-success"><i class="fa fa-check"></i></a></td>
                <td class="tabla"><a href="rechazar?c=<?php echo $row['pagos_codig'] ?>" target="_blank" class="btn btn-block btn-danger"><i class="fa fa-close"></i></a></td>
            <?php } ?>
        </tr>
        <?php
            }   
        ?>
    </tbody>
</table>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable(); 
    });
</script>
