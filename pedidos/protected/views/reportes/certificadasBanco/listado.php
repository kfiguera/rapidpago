<div class="row bg-title">
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Solicitudes Certificadas por el Banco</h4>
    </div>
    <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Tramitación</a></li>
            <li><a href="#">Solicitudes Certificadas por el Banco</a></li>
            <li class="active">Listado</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php
$connection = Yii::app()->db;
if(Yii::app()->user->id['usuario']['permiso']==1){
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Criterio de Busqueda</h3>
    </div>
    <div class="panel-body" >
        <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'pedidos', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            $connection = Yii::app()->db;

        ?>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Número de Solicitud</label>
                    <?php echo CHtml::textField('numero', '', array('class' => 'form-control', 'placeholder' => "Nro del Solicitud")); ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Estatus</label>
                    <?php 
                        
                        $sql="SELECT * FROM solicitud_estatus_pago";
                        $result=$connection->createCommand($sql)->queryAll();
                        $data=CHtml::listData($result,'epago_codig','epago_descr');
                        echo CHtml::dropDownList('epago', '', $data,array('class' => 'form-control', 'placeholder' => "Descripción", 'prompt'=>'Seleccione...')); ?>
                </div>
            </div>
           
        </div>
        
        <div class="row">
            <div class="col-sm-6">
                <button type="button" id="buscar" class="btn btn-block btn-info">Buscar</button>
            </div>
            <div class="col-sm-6">
                <button type="reset" class="btn btn-block btn-default">Limpiar</button>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div>
<?php
}
?> 
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-sm-8">
                <h3 class="panel-title">Listado</h3>
            </div>
            <div class="col-sm-4">
                <a href="pdf" class="btn btn-info btn-block">Imprimir</a>
            </div>
        </div>
    </div>
    <div class="panel-body" >
         
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-pedido'>
                <table  id='auditoria'  class="display nowrap table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                #
                            </th>
                            <th>
                                Nro Solicitud
                            </th>
                            <th>
                                Razón Social
                            </th>
                            <th>
                                Nombre Fantasia
                            </th> 
                            <th>
                                Banco
                            </th>
                            <th>
                                Código de Afiliado
                            </th>
                            <th>
                                Acción
                            </th>
                            <th>
                                Observación
                            </th>
                            <th>
                                Usuario de Certificación
                            </th>
                            <th>
                                Fecha de Certificación
                            </th>
                            <th width="5px">
                                Estatus
                            </th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        //if(Yii::app()->user->id['usuario']['permiso']==1){

                        $sql="SELECT * FROM solicitud a
                                JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                                JOIN cliente c ON (a.clien_codig = c.clien_codig)
                                JOIN solicitud_certificacion_banco g ON (a.solic_codig = g.solic_codig)
                                JOIN p_accion h ON (g.accio_codig = h.accio_codig)
                                JOIN seguridad_usuarios d ON (g.usuar_codig = d.usuar_codig)
                                JOIN p_banco f ON (a.banco_codig = f.banco_codig)
                                JOIN p_persona e ON (d.perso_codig = e.perso_codig)
                                
                        ";
                   
                        /*}else{
                            $sql = "SELECT * FROM solicitud_pagos a 
                                    JOIN solicitud_estatus_pago b ON (a.epago_codig = b.epago_codig)
                                    JOIN solicitud c ON (a.solic_codig = c.solic_codig)
                                    JOIN seguridad_usuarios d ON (c.usuar_codig = d.usuar_codig)
                                    WHERE a.usuar_codig = '".Yii::app()->user->id['usuario']['codigo']."'
                                    AND c.estat_codig='10'";
                             
                        }*/
                        
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                            $j++;
                            $sql="SELECT * FROM cliente where clien_codig='".$row['clien_codig']."'";
                            $cliente=$connection->createCommand($sql)->queryRow();

                            $sql="SELECT * FROM cliente_representante where clien_codig='".$row['clien_codig']."'";
                            $representante=$connection->createCommand($sql)->queryRow();


                            $vip=array('1'=>'SI', '2'=>'NO');
                            $sql="SELECT * 
                                  FROM solicitud_trayectoria 
                                  WHERE solic_codig = '".$row['solic_codig']."'
                                    AND estat_codig = '".$row['estat_codig']."'";

                            $trayectoria = $connection->createCommand($sql)->queryRow();


                            $inicio=$trayectoria['traye_finic'].' '.$trayectoria['traye_hinic'];
                            $fin=date('Y-m-d H:i:s');
                            $diff=$this->funciones->diferenciaHoras($inicio,$fin);
                            $horas=($diff->days * 24 )  + ( $diff->h );
                            $semaforo=$this->funciones->semaforo($connection,$row['estat_codig'],$horas);
                        ?>
                        <tr>
                            <td class="tabla"><?php echo $i ?></td>

                            <td class="tabla"><a  href="consultar?c=<?php echo $row['solic_codig'] ?>"><?php echo $row['solic_numer'] ?></a></td>
                             <td class="tabla"><?php echo $cliente['clien_rsoci'];//$row['coleg_nombr'] ?></td>
                            <td class="tabla"><?php echo $cliente['clien_nfant']//$row['usuar_login'] ?></td>
                            <td class="tabla"><?php echo $row['banco_descr']//$row['usuar_login'] ?></td>
                            <td class="tabla"><?php echo $row['cbanc_cafil']//$row['usuar_login'] ?></td>
                            <td class="tabla"><?php echo $row['accio_descr'] ?></td>
                            <td class="tabla"><?php echo $row['cbanc_obser'] ?></td>
                            
                            <td class="tabla"><?php echo $row['perso_pnomb'].' '.$row['perso_papel'] ?></td>
                            <td><?php echo $this->funciones->transformarFecha_v($row['solic_fcrea']).' '.$row['solic_hcrea'] ?></td>
                            <td><?php echo $semaforo.' '.$row['estat_descr'] ?></td>

                            <!--td class="tabla"><a href="../detalle/listado?p=<?php echo $row['solic_codig'] ?>" class="btn btn-block btn-info"><i class="glyphicon glyphicon-list-alt"></i></a></td-->
                            
                            
                        </tr>
                        <?php
                            }   
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>  
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel'
            ]
        });

        $('#buscar').click(function () {
        var formData = new FormData($("#pedidos")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-pedido').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>