<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

		<style>

			body {
				font-family: ipamp;
		 		font-size: 10pt;
		 	}
		 	
		 	td { 
		 		vertical-align: middle; 
		 	}
		 	p{
		 		text-align: justify;
		 	}
		 	.items td {
		 		/*border-left: 0.1mm solid #000000;
		 		border-right: 0.1mm solid #000000;*/
		 		border: 0.1mm solid #000000;
		 		font-size: 8pt;
		 		text-align: left;
		 		padding: 2pt;
		 	}
		 	.items td img {
		 		max-height: 300px;
		 		max-width: 300px
		 		width:100%;
		 		height: auto;
		 	}
		 	table thead td, table thead th, table tfoot td, table tfoot th { 
		 		background-color: #EEEEEE;
		 		text-align: center;
		 		border: 0.1mm solid #000000;
		 		font-size: 10pt;
		 		padding: 2pt;
		 	}
		 	.items td.blanktotal {
		 		background-color: #FFFFFF;
		 		border: 0mm none #000000;
		 		border-top: 0.1mm solid #000000;
		 	}
		 	.items td.totals {
		 		text-align: right;
		 		border: 0.1mm solid #000000;
		 	}
		 	.text-center{
		 		text-align: center;
		 	}
		 	.text-left{
		 		text-align: left;
		 	}
		 	.text-right{
		 		text-align: right;
		 	}
		 	.img-responsive{
		 		width: 100%;
		 		height: auto;
		 		max-width: 50px;
		 	}
		</style>
	</head>
	<body>
		<?php
			$connection = $conexion=yii::app()->db;

			


        ?>
 		<table class="items" width="100%" style="font-size: 8pt; border-collapse: collapse;" cellpadding="5">            
 			<thead>
                <tr>
                    <th width="4%">
                                #
                            </th>
                            <th width="24%">
                                Modelo
                            </th>
                            <th width="24%">
                                Articulo
                            </th>
                            <th width="24%">
                                Serial
                            </th> 
                            <th width="24%">
                                Almacen
                            </th>
                </tr>
            </thead>
            <tbody>
            	<?php

                        //if(Yii::app()->user->id['usuario']['permiso']==1){

                        $sql="SELECT * 
                              FROM inventario_seriales a 
                              JOIN inventario b ON (a.inven_codig = b.inven_codig)
                              JOIN inventario_modelo c ON (a.model_codig = c.model_codig)
                              JOIN inventario_almacen d ON (a.almac_codig = d.almac_codig)";
                        /*}else{
                            $sql = "SELECT * FROM solicitud_pagos a 
                                    JOIN solicitud_estatus_pago b ON (a.epago_codig = b.epago_codig)
                                    JOIN solicitud c ON (a.solic_codig = c.solic_codig)
                                    JOIN seguridad_usuarios d ON (c.usuar_codig = d.usuar_codig)
                                    WHERE a.usuar_codig = '".Yii::app()->user->id['usuario']['codigo']."'
                                    AND c.estat_codig='10'";
                             
                        }*/
                        
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                            $j++;
                            $sql="SELECT * FROM cliente where clien_codig='".$row['clien_codig']."'";
                            $cliente=$connection->createCommand($sql)->queryRow();
                            $vip=array('1'=>'SI', '2'=>'NO');
                            $sql="SELECT * 
                                  FROM solicitud_trayectoria 
                                  WHERE solic_codig = '".$row['solic_codig']."'
                                    AND estat_codig = '".$row['estat_codig']."'";

                            $trayectoria = $connection->createCommand($sql)->queryRow();

                            $inicio=$trayectoria['traye_finic'].' '.$trayectoria['traye_hinic'];
                            $fin=date('Y-m-d H:i:s');
                            $diff=$this->funciones->diferenciaHoras($inicio,$fin);
                            $horas=($diff->days * 24 )  + ( $diff->h );
                            $semaforo=$this->funciones->semaforo($connection,$row['estat_codig'],$horas);
                        ?>
                        <tr>
                             <td class="tabla"><?php echo $i ?></td>

                            <td class="tabla"><?php echo $row['solic_numer'] ?></td>
                             <td class="tabla"><?php echo $cliente['clien_rsoci'];//$row['coleg_nombr'] ?></td>
                            <td class="tabla"><?php echo $cliente['clien_nfant']//$row['usuar_login'] ?></td>
                            <td class="tabla"><?php echo $row['banco_descr']//$row['usuar_login'] ?></td>
                           <td class="tabla"><?php echo $vip[$row['solic_clvip']] ?></td>
                            <td class="tabla"><?php echo $row['perso_pnomb'].' '.$row['perso_papel'] ?></td>
                            <td><?php echo $this->funciones->transformarFecha_v($row['solic_fcrea']).' '.$row['solic_hcrea'] ?></td>
                            <td><?php echo $semaforo.' '.$row['estat_descr'] ?></td>

                            

                            <!--td class="tabla"><a href="../detalle/listado?p=<?php echo $row['solic_codig'] ?>" class="btn btn-block btn-info"><i class="glyphicon glyphicon-list-alt"></i></a></td-->
                            
                            
                        </tr>
                        <?php
                            }   
                        ?>
            </tbody>
        </table>
    </body>
 </html>
 <?php  ?>