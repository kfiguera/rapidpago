<?php $conexion = Yii::app()->db; ?>
<style type="text/css">
    td, th {
      white-space: normal !important; 
      word-wrap: break-word;  
    }
    table {
        table-layout: fixed;
        word-wrap: break-word;
    }
</style>
<div class="sttabs tabs-style-linebox">
    <nav>
      <ul>
        <li class="tab-current">
            <a href="#section-linebox-1">
                <span>DATOS DEL COMERCIO</span>
            </a>
        </li>
        <li class="">
            <a href="#section-linebox-2">
                <span>DATOS DE AFILIACIÓN</span>
            </a>
        </li>
        <!--li class="">
            <a href="#section-linebox-3">
                <span>CONFIGURACIÓN DEL EQUIPO</span>
            </a>
        </li-->

        <li class="">
            <a href="#section-linebox-4">
                <span>REPRESENTANTES LEGALES</span>
            </a>
        </li>

        <li class="">
            <a href="#section-linebox-5">
                <span>DATOS ADICIONALES</span>
            </a>
        </li>
        <li class="">
            <a href="#section-linebox-6">
                <span>DOCUMENTOS DIGITALES</span>
            </a>
        </li>
        
      </ul>
    </nav>
    <?php
    $sql="SELECT * FROM cliente
            WHERE clien_codig='".$solicitud['clien_codig']."'";
    $cliente=$conexion->createCommand($sql)->queryRow();
    $sql="SELECT * FROM cliente_direccion
            WHERE clien_codig='".$solicitud['clien_codig']."'";
    $direccion=$conexion->createCommand($sql)->queryRow();
    $sql="SELECT * FROM p_actividad_comercial
            WHERE acome_codig='".$cliente['acome_codig']."'";
    $acomercial=$conexion->createCommand($sql)->queryRow();
    $sql="SELECT * FROM p_estados
            WHERE estad_codig='".$direccion['estad_codig']."'";
    $p_estados=$conexion->createCommand($sql)->queryRow();
    $sql="SELECT * FROM p_municipio
            WHERE munic_codig='".$direccion['munic_codig']."'";
    $p_municipio=$conexion->createCommand($sql)->queryRow();
    $sql="SELECT * FROM p_parroquia
            WHERE parro_codig='".$direccion['parro_codig']."'";
    $p_parroquia=$conexion->createCommand($sql)->queryRow();
    $sql="SELECT * FROM p_ciudades
            WHERE ciuda_codig='".$direccion['ciuda_codig']."'";
    $ciudad=$conexion->createCommand($sql)->queryRow();
    ?>
    <div class="content-wrap text-center">
        <section id="section-linebox-1" class="content-current">
            <table class="table table-bordered table-hover">
                <tr>
                    <th class="col-sm-2">RIF del Comercio * </th>
                    <td class="col-sm-2"><?php echo $cliente['clien_rifco']?></td>
                    <th class="col-sm-2">Razon Social* </th>
                    <td class="col-sm-2"><?php echo $cliente['clien_rsoci']?></td>
                    <th class="col-sm-2">Nombre Fantasia o Comercial *</th>
                    <td class="col-sm-2"><?php echo $cliente['clien_nfant']?></td>
                </tr>
                <tr>
                    <th>Actividad Comercial * </th>
                    <td><?php echo $acomercial['acome_descr']?></td>
                    <th>Teléfono Movil * </th>
                    <td><?php echo $cliente['clien_tmovi']?></td>
                    <th>Teléfono Fijo</th>
                    <td><?php echo $cliente['clien_tfijo']?></td>
                </tr>
                <tr>
                    <th>Estado *  </th>
                    <td><?php echo $p_estados['estad_descr']; ?></td>
                    <th>Municipio * </th>
                    <td><?php echo $p_municipio['munic_descr']; ?></td>
                    <th>Parroquia *</th>
                    <td><?php echo $p_parroquia['parro_descr']; ?></td>
                </tr>
                <tr>
                    <th>Ciudad *  </th>
                    <td><?php echo $ciudad['ciuda_descr']; ?></td>
                    <th>Correo Electronico * </th>
                    <td><?php echo $solicitud['solic_celec']; ?></td>
                    <th>Dirección Fiscal  *</th>
                    <td><?php echo $cliente['clien_dfisc']; ?></td>
                </tr>
            </table>
        </section>
        <section id="section-linebox-2" class="">
            <div class="row">
                <div class="col-sm-6">  
                    <h4>Datos de la afiliación</h4>

                    <?php
                    $sql="SELECT * FROM p_banco 
                          WHERE banco_codig='".$solicitud['banco_codig']."'";
                    $banco=$conexion->createCommand($sql)->queryRow();
                    ?>

                    <table class="table table-bordered table-hover">
                        <tr>
                            <th>Banco Afiliado * </th>
                            <td><?php echo $banco['banco_descr']; ?></td>
                        </tr>
                        <tr>
                            <th>Código Afiliado * </th>
                            <td><?php echo $solicitud['solic_cafil']; ?> </td>
                        </tr>
                        <tr>
                            <th>Numero de Cuenta *</th>
                            <td><?php echo $solicitud['solic_ncuen']; ?></td>
                        </tr>
                    </table>
                </div>
                <div class="col-sm-6">  
                    <?php
                    $sql="SELECT * FROM solicitud_equipos a
                          JOIN p_operador_telefonico b ON (a.otele_codig = b.otele_codig)
                          WHERE solic_codig='".$solicitud['solic_codig']."'";
                    $cequipo=$conexion->createCommand($sql)->queryAll();
                    ?>
                    <h4>Cantidad de equipos</h4>
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Operador Telefonico</th>
                                <th>Cantidad </th>
                            </tr>    
                        </thead>
                        <tbody>
                            <?php
                                foreach ($cequipo as $key => $value) {
                                   ?>
                                   <tr> 
                                        <td><?php echo $value['otele_descr']; ?></td>
                                        <td><?php echo $value['cequi_canti']; ?></td>
                                    </tr> 
                                   <?php 
                                   $total+=$value['cequi_canti'];
                                }
                            ?>
                             
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>TOTAL</th>
                                <th><?php echo $total?></th>
                            </tr>  
                        </tfoot>
                    </table>   
                </div>
            </div>
            
        <!--/section>
        <section id="section-linebox-3" class=""-->
                        
        </section>
        <section id="section-linebox-4" class="">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>RIF</th>
                        <th>Nombre </th>
                        <th>Tipo de Documento</th>
                        <th>Número Documento</th>
                        <th>Cargo</th>
                        <th>Teléfono Movil</th>
                        <th>Teléfono Fijo</th>
                        <th>Correo Electrónico</th>
                    </tr>    
                </thead>
                <tbody>
                     <?php
                        $sql="SELECT * FROM cliente_representante a
                              JOIN p_documento_tipo b ON (a.tdocu_codig = b.tdocu_codig)
                              WHERE clien_codig='".$cliente['clien_codig']."'";

                        $rlegal=$conexion->createCommand($sql)->queryAll();
                        ?>
                    <?php
                        foreach ($rlegal as $key => $value) {
                           ?>
                           <tr> 
                                <td><?php echo $value['rlega_rifrl']; ?></td>
                                <td><?php echo $value['rlega_nombr'].' '.$value['rlega_apell']; ?></td>
                                <td><?php echo $value['tdocu_descr']; ?></td>
                                <td><?php echo $value['rlega_ndocu']; ?></td>
                                <td><?php echo $value['rlega_cargo']; ?></td>
                                <td><?php echo $value['rlega_tmovi']; ?></td>
                                <td><?php echo $value['rlega_tfijo']; ?></td>
                                <td><?php echo $value['rlega_corre']; ?></td>

                            </tr> 
                           <?php 
                           $total+=$value['cequi_canti'];
                        }
                    ?>
                       
                </tbody>
            </table>               
        </section>
        <section id="section-linebox-5" class="">
             <?php
            $sql="SELECT * FROM p_solicitud_origen
                  WHERE orige_codig='".$solicitud['orige_codig']."'";
            $origen=$conexion->createCommand($sql)->queryRow();
            $vip= array('1' => 'SI', '2' => 'NO');
            ?>
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Fecha de Soliciud</th>
                        <th>Origen </th>
                        <th>Cliente VIP</th>
                        <th>Observaciones</th>
                    </tr>    
                </thead>
                <tbody>
                    <tr>
                        <td><?php echo $this->funciones->transformarFecha_v($solicitud['solic_fsoli'])?></td>
                        <td><?php echo $origen['orige_descr']?></td>
                        <td><?php echo $vip[$solicitud['solic_clvip']]?></td>
                        <td><?php echo $solicitud['solic_obser']?></td>
                    </tr>    
                </tbody>
            </table>               
        </section>
        <section id="section-linebox-6" class="">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>DOCUMENTO</th>
                        <th>VER</th>
                    </tr>    
                </thead>
                <tbody>
                    <?php
                        $sql="SELECT * FROM solicitud_documento a
                              JOIN rd_preregistro_documento_digital_tipo b ON (a.dtipo_codig = b.dtipo_codig)
                              WHERE solic_codig='".$solicitud['solic_codig']."'";
                        $rlegal=$conexion->createCommand($sql)->queryAll();
                        ?>
                    <?php
                        foreach ($rlegal as $key => $value) {
                           ?>
                           <tr> 
                                <td><?php echo $value['dtipo_descr']; ?></td>
                                <td><a class="btn btn-info btn-block" href="/<?php echo $value['docum_ruta']; ?>" target="_blank"><i class="fa fa-search"></i></td>

                            </tr> 
                           <?php 
                           $total+=$value['cequi_canti'];
                        }
                    ?>    
                </tbody>
            </table>               
        </section>
    </div><!-- /content -->
</div>