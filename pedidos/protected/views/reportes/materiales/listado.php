<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Materiales por Pedidos</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Reportes</a></li>            
            
            <li><a href="#">Materiales por Pedidos</a></li>
            <li class="active">Listado</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Criterio de Busqueda</h3>
    </div>
    <div class="panel-body" >
        <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'form', 'action'=>'pdf', 'htmlOptions' => array('target'=>'_blank','id'=>'form','method' => 'post', 'enctype' => 'multipart/form-data')));
            $connection = Yii::app()->db;

        ?>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Pedidos</label>
                    <?php 
                    $sql="SELECT * FROM pedido_pedidos";
                    $result=$connection->createCommand($sql)->queryAll();
                    $data=CHtml::listData($result,'pedid_codig','pedid_numer');
                    echo CHtml::DropDownList('pedid', '', $data,array('class' => 'form-control select2', 'placeholder' => "Pedidos", 'prompt'=>'Seleccione')); ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Desde</label>
                    <?php echo CHtml::textField('desde', '', array('class' => 'form-control', 'placeholder' => "DD/MM/AAAA")); ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Hasta</label>
                    <?php echo CHtml::textField('hasta', '', array('class' => 'form-control', 'placeholder' => "DD/MM/AAAA")); ?>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-6">
                <button type="button" id="buscar" class="btn btn-block btn-info">Buscar</button>
            </div>
            <div class="col-sm-6">
                <button type="reset" class="btn btn-block btn-default">Limpiar</button>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div> 
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-sm-8">
                <h3 class="panel-title">Listado</h3>
            </div>
            <div class="col-sm-4">
                <button type="button" id="imprimir" class="btn btn-block btn-info">Imprimir</button>
            </div>
        </div>
    </div>
    <div class="panel-body" >
        
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-p_persona'>
                <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th colspan="2" width="20%">DATOS DEL PEDIDO</th>
                            <th colspan="2"  width="30%">TIPO DE TELA</th>
                            <th colspan="4" width="50%">COLORES</th>
                        </tr>
                        <tr>
                            <th rowspan="2" width="5%">#</th>
                            <th rowspan="2" width="15%">NRO. PEDIDO</th>
                            <th rowspan="2" width="15%">POLERON</th>
                            <th rowspan="2" width="15%">CORTAVIENTO</th>
                            <th rowspan="2" width="16.66%">CUERPO</th>
                            <th rowspan="2" width="16.66%">MANGAS</th>
                            <th colspan="2" width="16.66%">GORRO</th>
                            
                        </tr>
                        <tr>
                            <th width="8.33%">EXTERIOR</th>
                            <th width="8.33%">INTERIOR</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <?php
                        $sql = "SELECT *, 
                                    (SELECT count(pdlis_codig) 
                                     FROM pedido_pedidos_detalle b
                                     JOIN pedido_pedidos_detalle_listado c ON (b.pdeta_codig = c.pdeta_codig)
                                     WHERE b.pedid_codig=a.pedid_codig
                                     AND b.tmode_codig='1' 
                                     ) polerones, 
                                    (SELECT count(pdlis_codig) 
                                     FROM pedido_pedidos_detalle b
                                     JOIN pedido_pedidos_detalle_listado c ON (b.pdeta_codig = c.pdeta_codig)
                                     WHERE b.pedid_codig=a.pedid_codig
                                     AND b.tmode_codig='2' 
                                     ) cortaviento
                            FROM pedido_pedidos a
                            WHERE epedi_codig='3'";
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                            $sql="SELECT *
                                  FROM pedido_modelo_color a
                                  WHERE a.mcolo_codig='".$row['pedid_ccuer']." '";
                            $cuerpo=$connection->createCommand($sql)->queryRow();
                            $sql="SELECT *
                                  FROM pedido_modelo_color a
                                  WHERE a.mcolo_codig='".$row['pedid_cmang']." '";
                            $mangas=$connection->createCommand($sql)->queryRow();
                            $sql="SELECT *
                                  FROM pedido_modelo_color a
                                  WHERE a.mcolo_codig='".$row['pedid_cegor']." '";
                            $gorroe=$connection->createCommand($sql)->queryRow();
                            $sql="SELECT *
                                  FROM pedido_modelo_color a
                                  WHERE a.mcolo_codig='".$row['pedid_cigor']." '";
                            $gorroi=$connection->createCommand($sql)->queryRow();
                        ?>
                        <tr>
                            <td><?php echo $i ?></td>
                            <td><?php echo $row['pedid_numer']; ?></td>
                            <td><?php echo $this->funciones->TransformarMonto_v($row['polerones'],0) ?></td>
                            <td><?php echo $this->funciones->TransformarMonto_v($row['cortaviento'],0) ?></td>
                            <td><?php echo $cuerpo['mcolo_numer'].' - '.$cuerpo['mcolo_descr'] ?></td>
                            <td><?php echo $mangas['mcolo_numer'].' - '.$mangas['mcolo_descr'] ?></td>
                            <td><?php echo $gorroe['mcolo_numer'].' - '.$gorroe['mcolo_descr'] ?></td>
                            <td><?php echo $gorroi['mcolo_numer'].' - '.$gorroi['mcolo_descr'] ?></td>
                        </tr>
                        <?php
                            }   
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>  
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
    $('#desde').mask('99/99/9999');
    $('#hasta').mask('99/99/9999');
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#buscar').click(function () {
        var formData = new FormData($("#form")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-p_persona').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
    $('#imprimir').click(function(){
        var formData = new FormData($("#form")[0]);
        $("#form").submit();
    });
    jQuery('#desde').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'

      });
    jQuery('#hasta').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'

      });
</script>
