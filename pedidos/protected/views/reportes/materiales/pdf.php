<html>
	<head>
		<style>
			body {
				font-family: ipamp;
		 		font-size: 10pt;
		 	}
		 	p { 
		 		margin: 0pt;
		 	}
		 	td { 
		 		vertical-align: middle; 
		 	}
		 	.items td {
		 		/*border-left: 0.1mm solid #000000;
		 		border-right: 0.1mm solid #000000;*/
		 		border: 0.1mm solid #000000;
		 		font-size: 10pt;
		 		text-align: center;
		 		padding: 2pt;
		 	}
		 	.items td img {
		 		max-height: 300px;
		 		max-width: 300px
		 		width:100%;
		 		height: auto;
		 	}
		 	table thead td, table thead th, table tfoot td, table tfoot th { 
		 		background-color: #EEEEEE;
		 		text-align: center;
		 		border: 0.1mm solid #000000;
		 		font-size: 10pt;
		 		padding: 2pt;
		 	}
		 	.items td.blanktotal {
		 		background-color: #FFFFFF;
		 		border: 0mm none #000000;
		 		border-top: 0.1mm solid #000000;
		 	}
		 	.items td.totals {
		 		text-align: right;
		 		border: 0.1mm solid #000000;
		 	}
		 	.text-center{
		 		text-align: center;
		 	}
		 	.text-left{
		 		text-align: left;
		 	}
		 	.text-right{
		 		text-align: right;
		 	}
		 	.img-responsive{
		 		width: 100%;
		 		height: auto;
		 		max-width: 50px;
		 	}
		</style>
	</head>
	<body>
		<!--div style="text-align: right"><b>Fecha: </b><?php echo date("d/m/Y"); ?> </div>
		<b>Total Registros:</b> 12-->
		 
		 
		 <?php
            $sql =  "SELECT *, 
                                    (SELECT count(pdlis_codig) 
                                     FROM pedido_pedidos_detalle b
                                     JOIN pedido_pedidos_detalle_listado c ON (b.pdeta_codig = c.pdeta_codig)
                                     WHERE b.pedid_codig=a.pedid_codig
                                     AND b.tmode_codig='1' 
                                     ) polerones, 
                                    (SELECT count(pdlis_codig) 
                                     FROM pedido_pedidos_detalle b
                                     JOIN pedido_pedidos_detalle_listado c ON (b.pdeta_codig = c.pdeta_codig)
                                     WHERE b.pedid_codig=a.pedid_codig
                                     AND b.tmode_codig='2' 
                                     ) cortaviento
                            FROM pedido_pedidos a
                            WHERE epedi_codig<>'0'
                ".$condicion;
            $connection=yii::app()->db;
            $command = $connection->createCommand($sql);
            $p_persona = $command->query();
            $i=0;
            while (($row = $p_persona->read()) !== false) {
                $i++;

	            $sql = "SELECT * FROM seguridad_usuarios WHERE usuar_codig = '".$row['usuar_codig']."'";
	            $usuario = $connection->createCommand($sql)->queryRow();

				$sql = "SELECT * 
						FROM colegio a 
						WHERE a.coleg_codig='".$usuario['coleg_codig']."'";
				
	            $colegio = $connection->createCommand($sql)->queryRow();

	            $sql="SELECT *
	                  FROM pedido_modelo_color a
	                  WHERE a.mcolo_codig='".$row['pedid_ccuer']." '";
	            $cuerpo=$connection->createCommand($sql)->queryRow();
	            $sql="SELECT *
	                  FROM pedido_modelo_color a
	                  WHERE a.mcolo_codig='".$row['pedid_cmang']." '";
	            $mangas=$connection->createCommand($sql)->queryRow();
	            $sql="SELECT *
	                  FROM pedido_modelo_color a
	                  WHERE a.mcolo_codig='".$row['pedid_cegor']." '";
	            $gorroe=$connection->createCommand($sql)->queryRow();
	            $sql="SELECT *
	                  FROM pedido_modelo_color a
	                  WHERE a.mcolo_codig='".$row['pedid_cigor']." '";
	            $gorroi=$connection->createCommand($sql)->queryRow();
	        ?>
	        <table class="items" width="100%" style=" border-collapse: collapse;" cellpadding="5">
				 <thead>
				 	<tr>
					    <th colspan="2" width="20%">COLEGIO</th>
					    <th colspan="6" width="80%"><?php echo $colegio['coleg_nombr'];?></th>
					</tr>
					<tr>
					    <th colspan="2" width="20%">DATOS DEL PEDIDO</th>
					    <th colspan="2"  width="20%">TIPO DE TELA</th>
					    <th colspan="4" width="60%">COLORES</th>
					</tr>
					<tr>
		                <th rowspan="2" width="5%">#</th>
		                <th rowspan="2" width="15%">NRO. PEDIDO</th>
		                <th rowspan="2" width="10%">POLERON</th>
		                <th rowspan="2" width="10%">CORTAVIENTO</th>
		                <th rowspan="2" width="20%">CUERPO</th>
		                <th rowspan="2" width="20%">MANGAS</th>
		                <th colspan="2" width="20%">GORRO</th>
		                
		            </tr>
		            <tr>
		                <th width="10%">EXTERIOR</th>
		                <th width="10%">INTERIOR</th>
		            </tr>
				 </thead>
				 <tbody>
			        <tr>
			            <td><?php echo $i ?></td>
			            <td><?php echo $row['pedid_numer']; ?></td>
			            <td><?php echo $this->funciones->TransformarMonto_v($row['polerones'],0) ?></td>
			            <td><?php echo $this->funciones->TransformarMonto_v($row['cortaviento'],0) ?></td>
			            <td><?php echo $cuerpo['mcolo_numer'].' - '.$cuerpo['mcolo_descr'] ?></td>
			            <td><?php echo $mangas['mcolo_numer'].' - '.$mangas['mcolo_descr'] ?></td>
			            <td><?php echo $gorroe['mcolo_numer'].' - '.$gorroe['mcolo_descr'] ?></td>
			            <td><?php echo $gorroi['mcolo_numer'].' - '.$gorroi['mcolo_descr'] ?></td>
			        </tr>
			      </tbody>
		 		</table>
            <?php
            	//Adicionales
            	$sql="SELECT * FROM pedido_pedidos_detalle WHERE pedid_codig ='".$row['pedid_codig']."' order by 1";
            	$detalles=$connection->createCommand($sql)->queryAll();
            	

            	//var_dump($detalles);
            	$j=0;
            	$adicional= array();
            	$incluyen= array();
            	foreach ($detalles as $key => $detalle) {
            		$j++;
            		
            		$incluyen=array_unique($this->funciones->AdicionalPersonas($row['pedid_codig'],$detalle['pdeta_codig']));
            		
            		$sql="SELECT pdeta_codig, count(pdeta_codig) cantidad FROM pedido_pedidos_detalle_listado WHERE pedid_codig ='".$detalle['pedid_codig']."' AND pdeta_codig ='".$detalle['pdeta_codig']."'";
            		
            		$cantidad=$connection->createCommand($sql)->queryRow();
            		
            		foreach ($incluyen as $key => $value) {
            			$adicional[$value]['color']=$this->funciones->AdicionalColor($row['pedid_codig'],$value);
            			$adicional[$value]['canti']+=$cantidad['cantidad'];
            			$adicional[$value]['codig']=$value;
            			$totales[$value]['codig']=$value;
            			$totales[$value]['canti']+=$cantidad['cantidad'];
            		}
            		
            	} 
            	
            	
            	?>

            	<table class="items" width="100%" style=" border-collapse: collapse;" cellpadding="5">
				 <thead>
					<tr>
					    <th colspan="4" width="100%">ADICIONALES</th>
					</tr>
					<tr>
		                <th width="5%">#</th>
		                <th width="15%">DESCRIPCIÓN</th>
		                <th width="60%">COLOR</th>
		                <th width="20%">CANTIDAD</th>		                
		            </tr>
				 </thead>
				 <tbody>
				 	<?php
				 		$j=0;
				 		foreach ($adicional as $key => $value) {
				 			$j++;
				 			$sql="SELECT * FROM pedido_incluye WHERE inclu_codig='".$value['codig']."'";
				 			$opcional=$connection->createCommand($sql)->queryRow();
				 			?>
						        <tr>
						            <td><?php echo $j ?></td>
						            <td><?php echo $opcional['inclu_descr']; ?></td>
						            <td><?php echo $value['color'] ?></td>
						            <td><?php echo $this->funciones->TransformarMonto_v($value['canti'],0) ?></td>
						        </tr>
						    <?php
				 		}
				 	?>
				 	<tr>
					 <td class="blanktotal" colspan="4"></td>
					 </tr>
			      </tbody>
		 		</table>
            	<?php
                }   
            ?>

		 <!-- FIN ITEMS -->
		 <table class="items" width="100%" style=" border-collapse: collapse;" cellpadding="5">
		 	<thead>
		 		<tr>
		 			<th colspan="3">TOTALES</th>
		 		</tr>
		 		<tr>
		 			<th width="5%">#</th>
		 			<th width="75%">DESCRICIÓN</th>
		 			<th width="20%">CANTIDAD</th>
		 		</tr>
		 	</thead>
			 <tbody>
			 	<?php
			 	$k=0;
			 	foreach ($totales as $key => $value) {
			 		$k++;
				 	$sql="SELECT * FROM pedido_incluye WHERE inclu_codig='".$value['codig']."'";
				 	$opcional=$connection->createCommand($sql)->queryRow();
				 	?>
			        <tr>
			            <td><?php echo $k ?></td>
			            <td><?php echo $opcional['inclu_descr']; ?></td>
			            <td><?php echo $this->funciones->TransformarMonto_v($value['canti'],0) ?></td>
			        </tr>
			    <?php
			 	}
			 	?>
			 </tbody>
		 </table>

	 </body>
 </html>
