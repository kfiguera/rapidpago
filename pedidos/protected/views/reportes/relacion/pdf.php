<html>
	<head>
		<style>
			body {
				font-family: sans-serif;
		 		font-size: 10pt;
		 	}
		 	p { 
		 		margin: 0pt;
		 	}
		 	td { 
		 		vertical-align: bottom; 
		 	}
		 	.items td {
		 		/*border-left: 0.1mm solid #000000;
		 		border-right: 0.1mm solid #000000;*/
		 		border: 0.1mm solid #000000;
		 		font-size: 4pt;
		 		text-align: center;
		 		padding: 1pt;
		 	}
		 	table thead td, table thead th, table tfoot td, table tfoot th { 
		 		background-color: #EEEEEE;
		 		text-align: center;
		 		border: 0.1mm solid #000000;
		 		font-size: 4pt;
		 		padding: 2pt;
		 	}
		 	.items td.blanktotal {
		 		background-color: #FFFFFF;
		 		border: 0mm none #000000;
		 		border-top: 0.1mm solid #000000;
		 	}
		 	.items td.totals {
		 		text-align: right;
		 		border: 0.1mm solid #000000;
		 	}
		</style>
	</head>
	<body>
		<!--div style="text-align: right"><b>Fecha: </b><?php echo date("d/m/Y"); ?> </div>
		<b>Total Registros:</b> 12-->
		 
		 <table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse;" cellpadding="5">
		 <thead>
		 <tr>
		 	<th width="1%">#</th>
            <th width="15,66%">DESCRIPCIÓN</th>
            <th width="16,66%">FACTURA</th>
            <th width="16,66%">FECHA</th>
            <th width="16,66%">INGRESO</th>
            <th width="16,66%">EGRESO</th>
            <th width="16,66%">SALDO</th>

		 </tr>
		 </thead>
		 <tbody>
		 <?php
            $sql = "SELECT clien_denom, factu_nfact, factu_mpago, '' egres_monto, factu_femis 
                            FROM factura a
                            JOIN cliente b ON (a.clien_codig = b.clien_codig)
                            Union
                            SELECT egres_motiv, egres_nfact, '' ,egres_monto, egres_fcrea
                            FROM egresos
                ".$condicion;
            $connection=yii::app()->db;
            $command = $connection->createCommand($sql);
            $p_persona = $command->query();
            $i=0;
            $total['I']=0;
            $total['E']=0;
            $total['G']=0;
            while (($row = $p_persona->read()) !== false) {
                $i++;
                $total['I']+=$row['factu_mpago'];
                $total['E']+=$row['egres_monto'];
                $total['G']+=$row['factu_mpago']-$row['egres_monto'];

            ?>
            <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $row['clien_denom'] ?></td>
                <td><?php echo $row['factu_nfact'] ?></td>
                <td><?php echo $this->funciones->TransformarFecha_v($row['factu_femis']) ?></td>
                <td><?php echo $this->funciones->TransformarMonto_v($row['factu_mpago'],2) ?></td>
                <td><?php echo $this->funciones->TransformarMonto_v($row['egres_monto'],2) ?></td>
                <td><?php echo $this->funciones->TransformarMonto_v($total['G'],2) ?></td>
            </tr>
            <?php
                }   
            ?>
			 <!-- FIN ITEMS -->
			 <tr>
			 </tr>
			 </tbody>
			 <tfoot>
	            <tr>
	                <th colspan="4">TOTAL</th>
	                <th><?php echo $this->funciones->TransformarMonto_v($total['I'],2)?></th>
	                <th><?php echo $this->funciones->TransformarMonto_v($total['E'],2)?></th>
	                <th><?php echo $this->funciones->TransformarMonto_v($total['G'],2)?></th>
	            </tr>
	        </tfoot>		 
	    </table>
    </body>
 </html>
