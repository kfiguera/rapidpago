<table  id='auditoria'  class="table table-bordered table-hover dataTable">
    <thead>
        <tr>
            <th width="2%">
                #
            </th>
            <th>
                Usuario
            </th>
            <th>
                Primer de Inicio de Sesión
            </th>
            <th>
                Total Tiempo de Sesión
            </th>
        </tr>
    </thead>
    <tbody>
        <?php

        $sql="SELECT * 
        FROM seguridad_sesiones a 
        JOIN seguridad_usuarios b ON (a.usuar_codig=b.usuar_codig)
        JOIN p_persona c ON (b.perso_codig = c.perso_codig)
        ".$condicion."
        GROUP BY a.usuar_codig, sesio_finic 
        ORDER BY sesio_finic, sesio_hinic
        ";
        $connection= Yii::app()->db;
        $command = $connection->createCommand($sql);
        $p_persona = $command->query();
        $i=0;
        $j=0;
        $total = new DateTime(date('Y-m-d'));
        $inicio = clone $total;
        while (($row = $p_persona->read()) !== false) {
            $i++;
            $j++;

            //$total+=$row['cantidad'];
            $promedio=$this->funciones->promedioSesion($connection,$row['usuar_codig'],$row['sesio_finic']);
            
            $letras=$this->funciones->diferenciaEnLetras2($promedio);

            $total->add($promedio);
        ?>
        <tr>

            <td class="tabla"><?php echo $i ?></td>
            <td class="tabla"><?php echo $row['perso_pnomb'].' '.$row['perso_papel']; ?></td>
            <td class="tabla"><?php echo $this->funciones->TransformarFecha_v($row['sesio_finic']).' '.$row['sesio_hinic']; ?></td>
            <td class="tabla"><?php echo $letras; ?></td>    
        </tr>
        <?php
            }
            $total2=$inicio->diff($total);   
        ?>
    </tbody>
    <tfoot>
        <tr>
            <th colspan="3">Total</th>
            <th><?php echo $this->funciones->diferenciaEnLetras2($total2)?></th>
        </tr>
    </tfoot>
</table>
<script type="text/javascript">
    $('#auditoria').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel'
            ]
        });
</script>
