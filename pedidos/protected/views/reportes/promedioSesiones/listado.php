<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Reportes</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Reportes</a></li>
            <li>Promedio de Sesiones</a></li>
            <li class="active">Listado</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

<?php
//Mostrar mensajes del controlador
        foreach (Yii::app()->user->getFlashes() as $key => $message) {
          echo '<div class="alert alert-' . $key . ' alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              ' . $message  .' </div>';          
        }
$connection = Yii::app()->db;
if(Yii::app()->user->id['usuario']['permiso']==1){
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Criterio de Busqueda</h3>
    </div>
    <div class="panel-body" >
        <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'pedidos', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            

        ?>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Usuario</label>
                    <?php 
                        $sql="SELECT a.usuar_codig, CONCAT(b.perso_pnomb, ' ', b.perso_papel) nombre 
                              FROM seguridad_usuarios a
                              JOIN p_persona b ON (a.perso_codig = b.perso_codig)
                              JOIN seguridad_sesiones c ON  (a.usuar_codig = c.usuar_codig) ";
                        $result=$connection->createCommand($sql)->queryAll();
                        $data=CHtml::listData($result,'usuar_codig', 'nombre');
                        echo CHtml::dropDownList('usuario', '', $data, array('class' => 'form-control select2', 'prompt' => "Seleccione...")); ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Desde</label>
                    <?php echo CHtml::textField('desde', '', array('class' => 'form-control', 'placeholder' => "Desde")); ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Hasta</label>
                    <?php echo CHtml::textField('hasta', '', array('class' => 'form-control', 'placeholder' => "Hasta")); ?>
                </div>
            </div>
           
        </div>
        
        <div class="row">
            <div class="col-sm-6">
                <button type="button" id="buscar" class="btn btn-block btn-info">Buscar</button>
            </div>
            <div class="col-sm-6">
                <button type="reset" class="btn btn-block btn-default">Limpiar</button>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div> 
<?php
}
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-sm-8">
                <h3 class="panel-title">Listado</h3>
            </div>
            
        </div>
        
    </div>
    <div class="panel-body" >
        
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-pedido'>
                <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                #
                            </th>
                            <th>
                                Usuario
                            </th>
                            <th>
                                Primer de Inicio de Sesión
                            </th>
                            <th>
                                Total Tiempo de Sesión
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        $sql="SELECT * 
                        FROM seguridad_sesiones a 
                        JOIN seguridad_usuarios b ON (a.usuar_codig=b.usuar_codig)
                        JOIN p_persona c ON (b.perso_codig = c.perso_codig)
                        GROUP BY a.usuar_codig, sesio_finic 
                        ORDER BY sesio_finic, sesio_hinic
                        ";

                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        $j=0;
                        $total = new DateTime(date('Y-m-d'));
                        $inicio = clone $total;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                            $j++;

                            //$total+=$row['cantidad'];
                            $promedio=$this->funciones->promedioSesion($connection,$row['usuar_codig'],$row['sesio_finic']);
                            
                            $letras=$this->funciones->diferenciaEnLetras2($promedio);

                            $total->add($promedio);
                        ?>
                        <tr>

                            <td class="tabla"><?php echo $i ?></td>
                            <td class="tabla"><?php echo $row['perso_pnomb'].' '.$row['perso_papel']; ?></td>
                            <td class="tabla"><?php echo $this->funciones->TransformarFecha_v($row['sesio_finic']).' '.$row['sesio_hinic']; ?></td>
                            <td class="tabla"><?php echo $letras; ?></td>    
                        </tr>
                        <?php
                            }
                            $total2=$inicio->diff($total);   
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="3">Total</th>
                            <th><?php echo $this->funciones->diferenciaEnLetras2($total2)?></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            
            
        </div>
    </div>
</div>  
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel'
            ]
        });
        $('#buscar').click(function () {
        var formData = new FormData($("#pedidos")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-pedido').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        jQuery('#desde').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd/mm/yyyy'

          });
        jQuery('#hasta').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd/mm/yyyy'

          });
    });
</script>