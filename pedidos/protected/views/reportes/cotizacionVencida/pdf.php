<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

		<style>

			body {
				font-family: ipamp;
		 		font-size: 10pt;
		 	}
		 	
		 	td { 
		 		vertical-align: middle; 
		 	}
		 	p{
		 		text-align: justify;
		 	}
		 	.items td {
		 		/*border-left: 0.1mm solid #000000;
		 		border-right: 0.1mm solid #000000;*/
		 		border: 0.1mm solid #000000;
		 		font-size: 8pt;
		 		text-align: left;
		 		padding: 2pt;
		 	}
		 	.items td img {
		 		max-height: 300px;
		 		max-width: 300px
		 		width:100%;
		 		height: auto;
		 	}
		 	table thead td, table thead th, table tfoot td, table tfoot th { 
		 		background-color: #EEEEEE;
		 		text-align: center;
		 		border: 0.1mm solid #000000;
		 		font-size: 10pt;
		 		padding: 2pt;
		 	}
		 	.items td.blanktotal {
		 		background-color: #FFFFFF;
		 		border: 0mm none #000000;
		 		border-top: 0.1mm solid #000000;
		 	}
		 	.items td.totals {
		 		text-align: right;
		 		border: 0.1mm solid #000000;
		 	}
		 	.text-center{
		 		text-align: center;
		 	}
		 	.text-left{
		 		text-align: left;
		 	}
		 	.text-right{
		 		text-align: right;
		 	}
		 	.img-responsive{
		 		width: 100%;
		 		height: auto;
		 		max-width: 50px;
		 	}
		</style>
	</head>
	<body>
		<?php
			$connection = $conexion=yii::app()->db;

			$sql = "SELECT * FROM solicitud_pagos a 
                                JOIN solicitud_estatus_pago b ON (a.epago_codig = b.epago_codig)
                                JOIN solicitud c ON (a.solic_codig = c.solic_codig)
                                JOIN seguridad_usuarios d ON (c.usuar_codig = d.usuar_codig)
                                JOIN p_persona e ON (d.perso_codig = e.perso_codig)

                                JOIN p_pago_tipo f ON (a.ptipo_codig = f.ptipo_codig)
                                AND c.estat_codig in ('10','29')";
            $solicitudes = $conexion->createCommand($sql)->queryRow();


        ?>
 		<table class="items" width="100%" style="font-size: 8pt; border-collapse: collapse;" cellpadding="5">            
 			<thead>
                <tr>
                    <th width="2%">
                        #
                    </th>
                    <th>
                        Nro Solicitud
                    </th>
                    <th>
                        Nro Cotización
                    </th>
                    <th>
                        Razón Social
                    </th>
                    <th>
                        Nombre Fantasia
                    </th> 
                    <th>
                        Banco
                    </th>

                    <th>
                        Monto Solicitud
                    </th>
                    <th>
                        Monto Cotización
                    </th>
                    <th>
                        Responsable
                    </th>
                    <th>
                        Teléfono
                    </th>
                    <th>
                        Ciente VIP
                    </th>
                    <th>
                        Usuario de Registro
                    </th>
                    <th>
                        Fecha de Registro
                    </th>

                    <th>
                        Fecha de Cotización
                    </th>
                    <th>
                        Dias de Retraso
                    </th>
                    <th width="5px">
                        Estatus
                    </th>
                </tr>
            </thead>
            <tbody>
            	<?php

                        //if(Yii::app()->user->id['usuario']['permiso']==1){

                        $sql="SELECT * FROM solicitud a
                                JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                                JOIN cliente c ON (a.clien_codig = c.clien_codig)
                                JOIN seguridad_usuarios d ON (a.usuar_codig = d.usuar_codig)
                                JOIN p_banco f ON (a.banco_codig = f.banco_codig)
                                JOIN p_persona e ON (d.perso_codig = e.perso_codig)
                                JOIN solicitud_cotizacion g ON (a.solic_codig = g.solic_codig AND g.cotiz_estat='1')
                              WHERE a.estat_codig in ('10','29')
                              AND a.solic_codig not in (SELECT solic_codig FROM solicitud_pagos a  GROUP BY solic_codig)
                        ";
                   
                        /*}else{
                            $sql = "SELECT * FROM solicitud_pagos a 
                                    JOIN solicitud_estatus_pago b ON (a.epago_codig = b.epago_codig)
                                    JOIN solicitud c ON (a.solic_codig = c.solic_codig)
                                    JOIN seguridad_usuarios d ON (c.usuar_codig = d.usuar_codig)
                                    WHERE a.usuar_codig = '".Yii::app()->user->id['usuario']['codigo']."'
                                    AND c.estat_codig='10'";
                             
                        }*/
                        
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                            $j++;
                            $sql="SELECT * FROM cliente where clien_codig='".$row['clien_codig']."'";
                            $cliente=$connection->createCommand($sql)->queryRow();

                            $sql="SELECT * FROM cliente_representante where clien_codig='".$row['clien_codig']."'";
                            $representante=$connection->createCommand($sql)->queryRow();


                            $vip=array('1'=>'SI', '2'=>'NO');
                            $sql="SELECT * 
                                  FROM solicitud_trayectoria 
                                  WHERE solic_codig = '".$row['solic_codig']."'
                                    AND estat_codig = '".$row['estat_codig']."'";

                            $trayectoria = $connection->createCommand($sql)->queryRow();

                            $inicio=$trayectoria['traye_finic'].' '.$trayectoria['traye_hinic'];
                            $fin=date('Y-m-d H:i:s');
                            $diff=$this->funciones->diferenciaHoras($inicio,$fin);
                            $horas=($diff->days * 24 )  + ( $diff->h );
                            $semaforo=$this->funciones->semaforo($connection,$row['estat_codig'],$horas);
                            $cinic=$row['cotiz_fcrea'].' '.$row['cotiz_hcrea'];
                            $cfin=date('Y-m-d H:i:s');
                            $cdiff=$this->funciones->diferenciaHoras($cinic, $cfin);
                            
                            $choras=($cdiff->days * 24 )  + ( $cdiff->h );

                            $sql="SELECT * 
                                  FROM p_tiempo_cotizacion 
                                  WHERE pesta_codig = '1'";

                            $tcoti = $connection->createCommand($sql)->queryRow();
                            if($tcoti['tcoti_value']<$choras){
                        ?>
                        <tr>
                            <td class="tabla"><?php echo $i ?></td>

                            <td class="tabla"><?php echo $row['solic_numer'] ?></td>
                            <td class="tabla"><?php echo $row['cotiz_codig'] ?></td>
                             <td class="tabla"><?php echo $cliente['clien_rsoci'];//$row['coleg_nombr'] ?></td>
                            <td class="tabla"><?php echo $cliente['clien_nfant']//$row['usuar_login'] ?></td>
                            <td class="tabla"><?php echo $row['banco_descr']//$row['usuar_login'] ?></td>
                            <td class="tabla"><?php echo $this->funciones->transformarMonto_v($row['solic_monto'],2) ?></td>
                            <td class="tabla"><?php echo $this->funciones->transformarMonto_v($row['cotiz_total'],2) ?></td>
                            
                            <td class="tabla"><?php echo $representante['rlega_nombr'].' '.$row['rlega_apell'] ?></td>
                            <td class="tabla"><?php echo $representante['rlega_tmovi'] ?></td>
                            <td class="tabla"><?php echo $vip[$row['solic_clvip']] ?></td>
                            
                            <td class="tabla"><?php echo $row['perso_pnomb'].' '.$row['perso_papel'] ?></td>
                            <td><?php echo $this->funciones->transformarFecha_v($row['solic_fcrea']).' '.$row['solic_hcrea'] ?></td>

                            <td><?php echo $this->funciones->transformarFecha_v($row['cotiz_fcrea']).' '.$row['cotiz_hcrea'] ?></td>
                            <td><?php echo $this->funciones->diferenciaEnLetras($cdiff);?></td>
                            <td><?php echo $semaforo.' '.$row['estat_descr'] ?></td>

                            <!--td class="tabla"><a href="../detalle/listado?p=<?php echo $row['solic_codig'] ?>" class="btn btn-block btn-info"><i class="glyphicon glyphicon-list-alt"></i></a></td-->
                            
                            
                        </tr>
                        <?php
                            }   
                        }
                        ?>
            </tbody>
        </table>
    </body>
 </html>
 <?php  ?>