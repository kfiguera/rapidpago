<!DOCTYPE html>
<html lang="en">

<head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="language" content="es">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <link rel="shortcut icon" href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/img/logito.ico">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>

        <!-- Bootstrap -->
        <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/css/bootstrap.css" rel="stylesheet">

        <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/css/bootstrapValidator.css" rel="stylesheet">
        <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/css/font-awesome.min.css" rel="stylesheet" >
        <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/css/dataTables.bootstrap.min.css" rel="stylesheet" >
        <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/formvalidation/formValidation.min.css" rel="stylesheet" >
        <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/formvalidation/demo.css" rel="stylesheet">
        <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
        <!-- Timeline CSS -->
        <!-- Custom CSS -->
        <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/dist/css/sb-admin-2.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <!-- Custom Fonts -->
        <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script type="text/javascript" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/jquery.mask.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script type="text/javascript" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/bootstrapValidator.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/bootbox.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/dataTables.bootstrap.min.js"></script>
        <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/css/bootstrap.LineaTiempo.css" rel="stylesheet">
        <script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/formvalidation/formValidation.min.js"></script>
        <script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/formvalidation/bootstrap.min.js"></script>
        <script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/formvalidation/reCaptcha2.min.js"></script>
        <!-- include summernote css/js-->
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/css/summernote/summernote.css" rel="stylesheet">
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/summernote/summernote.js"></script>
        <!-- jQuery -->

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/bower_components/raphael/raphael-min.js"></script>


    <!-- Custom Theme JavaScript -->
    <script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/dist/js/sb-admin-2.js"></script>
        

    <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/css/UVEN.css" rel="stylesheet">
    <script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/bootstrap-waitingfor.js"></script>

</head>

<body>

    <div id="wrapper">
        <div  id="banner">
            <!--img class="img-responsive" height="50%" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/img/banner.jpg"-->

        </div>
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-static-top" role="navigation" id="topnavbar">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="../../../uven2"><img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/img/uven-logo-2-1.png"></a>
                </div>
                <div id="navbar" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="../../../uven2">Inicio</a>
                        </li>
                        <?php if (Yii::app()->user->isGuest) { ?>
                        <li ><a href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/site/login">Inicio Sesión</a>
                        </li>
                        <?php } ?>
                        <?php if (Yii::app()->user->id['usuario']->usuar_tipou != '2') { ?>
                        <li>
                            <a href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/uven/pedirtaxi2">Solicitar Taxi</a>
                        </li>
                        <li>
                            <a href="../../../uven2/registrarse">Registrarse</a>
                        </li>
                        <li>
                            <a href="../../../uven2/unete-al-equipo">Únete al Equipo</a>
                        </li>
                        <?php } ?>

                    </ul>
                    
                </div><!--/.nav-collapse -->
            </div>
        </nav>
            <div class="container-fluid">
                <?php echo $content; ?>

            <footer class="footer">
                <div class="container" style="padding-top: 20px;">
                    <p class="text-muted text-center"><b>UVEN <?php echo date('Y')?> &copy; Todos los derechos reservados</b>
                    </p>
                </div>
            </footer>
            </div>
        
            

    </div>
    <!-- /#wrapper -->

    

</body>

</html>
