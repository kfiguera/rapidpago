<table id='auditoria' class="table table-bordered table-hover dataTable">
    <thead>
        <tr>
            <th width="2%">
                #
            </th>
            <th>
                Tipo
            </th>
            <th>
                Modelo
            </th>
            <th>
                Categoria
            </th>
            <!--th>
                Variante
            </th-->
            <th>
                Opcional
            </th>
            <th>
                ¿Que incluye?
            </th>
            <th>
                Precio
            </th>
            <th width="5%">
                Consultar
            </th>
            <th width="5%">
                Modificar
            </th>
            <th width="5%">
                Eliminar
            </th>
        </tr>
    </thead>

    <tbody>
        <?php
        $sql = "SELECT * 
                FROM pedido_modelo_conjunto a
                JOIN pedido_modelo_tipo b ON (a.tmode_codig = b.tmode_codig)
                JOIN pedido_modelo c ON (a.model_codig = c.model_codig)
                JOIN pedido_modelo_categoria d ON (a.mcate_codig = d.mcate_codig)
                JOIN pedido_modelo_variante e ON (a.mvari_codig = e.mvari_codig)
                JOIN pedido_modelo_opcional f ON (a.mopci_codig = f.mopci_codig)
                ".$condicion."
                ORDER BY 2,3,4,5,6";
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $p_persona = $command->query();
        $i=0;
        while (($row = $p_persona->read()) !== false) {
            $i++;
        ?>
        <tr>
            <td class="tabla"><?php echo $i ?></td>
            <td class="tabla"><?php echo $row['tmode_descr'] ?></td>
            <td class="tabla"><?php echo $row['model_descr'] ?></td>
            <td class="tabla"><?php echo $row['mcate_descr'] ?></td>
            <!--td class="tabla"><?php echo $row['mvari_descr'] ?></td-->
            <td class="tabla"><?php echo $row['mopci_descr'] ?></td>
            <td class="tabla"><?php echo $this->funciones->VerOpcionales(json_decode($row['mopci_codig'])); ?></td>
            <td class="tabla"><?php echo $this->funciones->VerQueIncluye(json_decode($row['mconj_inclu'])); ?></td>
            <td class="tabla"><?php echo $this->funciones->TransformarMonto_v($row['mconj_preci'],2) ?></td>
            <td class="tabla"><a href="consultar?c=<?php echo $row['mconj_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-search"></i></a></td>
            <td class="tabla"><a href="modificar?c=<?php echo $row['mconj_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-pencil"></i></a></td>
            <td class="tabla"><a href="eliminar?c=<?php echo $row['mconj_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-close"></i></a></td>

        </tr>
        <?php
            }   
        ?>
    </tbody>
</table>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable(); 
    });
</script>
