<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Conjunto</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Modelos</a></li>
            <li><a href="#">Conjunto</a></li>
            <li class="active">Consultar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Consultar Conjunto</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row hide">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Código</label>
                            
                                <?php 
                                echo CHtml::textField('codig', $conjunto['mconj_codig'], array('class' => 'form-control', 'placeholder' => "Código", 'disabled'=>'true')); ?>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Imagen</label>
                            <br>
                            <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$conjunto['mconj_ruta'] ?>" class='img-responsive img-thumbnail'>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-sm-4">        
                                <div class="form-group">
                                    <label>Tipo *</label>
                                    <?php 
                                        $conexion = Yii::app()->db;
                                        $sql="SELECT tmode_codig, tmode_descr
                                              FROM pedido_modelo_tipo 
                                              ORDER BY 1";
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'tmode_codig','tmode_descr');
                                        echo CHtml::dropDownList('tmode', $conjunto['tmode_codig'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...', 'disabled'=>'true')); ?>
                                </div>
                            </div>
                            <div class="col-sm-4">        
                                <div class="form-group">
                                    <label>Modelo *</label>
                                    <?php 
                                        $sql="SELECT model_codig, model_descr
                                              FROM pedido_modelo
                                              ORDER BY 1";
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'model_codig','model_descr');
                                        echo CHtml::dropDownList('model', $conjunto['model_codig'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...', 'disabled'=>'true')); ?>
                                </div>
                            </div>
                            <div class="col-sm-4">        
                                <div class="form-group">
                                    <label>Categoria *</label>
                                    <?php 
                                        $sql="SELECT mcate_codig, mcate_descr
                                              FROM pedido_modelo_categoria
                                              ORDER BY 1";
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'mcate_codig','mcate_descr');
                                        echo CHtml::dropDownList('categ', $conjunto['mcate_codig'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...', 'disabled'=>'true')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 hide">        
                                <div class="form-group">
                                    <label>Variante *</label>
                                    <?php 
                                        $sql="SELECT mvari_codig, mvari_descr
                                              FROM pedido_modelo_variante
                                              ORDER BY 1";
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'mvari_codig','mvari_descr');
                                        echo CHtml::dropDownList('varia', $conjunto['mvari_codig'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...', 'disabled'=>'true')); ?>
                                </div>
                            </div>
                            <div class="col-sm-4">        
                                <div class="form-group">
                                    <label>Adicional *</label>
                                    <?php 
                                        $sql="SELECT mopci_codig, mopci_descr
                                              FROM pedido_modelo_opcional
                                              ORDER BY 1";
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'mopci_codig','mopci_descr');
                                        echo CHtml::dropDownList('opcio', json_decode($conjunto['mopci_codig']), $data, array('class' => 'select2', 'placeholder' => "Seleccione..",'prompt'=>'Seleccione...','multiple'=>'multiple', 'disabled'=>'true')); ?>
                                </div>
                            </div>
                            <div class="col-sm-4">        
                                <div class="form-group">
                                    <label>Adicional 2 *</label>
                                    <?php 
                                        $sql="SELECT mopci_codig, mopci_descr
                                              FROM pedido_modelo_opcional
                                              ORDER BY 1";
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'mopci_codig','mopci_descr');
                                        echo CHtml::dropDownList('opci2', json_decode($conjunto['mopci_codi2']), $data, array('class' => 'select2', 'placeholder' => "Seleccione..",'prompt'=>'Seleccione...','multiple'=>'multiple', 'disabled'=>'true')); ?>
                                </div>
                            </div>
                            <div class="col-sm-4">        
                                <div class="form-group">
                                    <label>¿Que incluye?</label>
                                        <?php 
                                        $sql="SELECT inclu_codig, inclu_descr
                                              FROM pedido_incluye
                                              ORDER BY 1";
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'inclu_codig','inclu_descr');
                                            echo CHtml::dropDownList('inclu', json_decode($conjunto['mconj_inclu']), $data ,array('class' => 'select2 select2-multiple', 'placeholder' => "Descripción", 'multiple'=>'multiple', 'disabled'=>'true')); 
                                        ?>
                                </div>
                            </div>

                            <div class="col-sm-12">        
                                <div class="form-group">
                                    <label>Precio</label>
                                        <?php 
                                            echo CHtml::textField('preci', $this->funciones->TransformarMonto_v($conjunto['mconj_preci'],2), array('class' => 'form-control', 'placeholder' => "Precio", 'disabled'=>'true')); 
                                        ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">        
                                <div class="form-group">
                                    <label>Observación</label>
                                        <?php 
                                            echo CHtml::textArea('obser', $conjunto['mconj_obser'], array('class' => 'form-control', 'placeholder' => "Observación", 'disabled'=>'true')); 
                                        ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                    <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-12">
                            <a href="listado" type="reset" class="btn-block btn btn-info">Volver </a>
                        </div>
                    </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
