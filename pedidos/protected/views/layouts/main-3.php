<!DOCTYPE html>
<html lang="en">

<head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="language" content="es">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <link rel="shortcut icon" href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/img/logito.ico">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>

        <!-- Bootstrap -->
        <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/css/bootstrap.css" rel="stylesheet">

        <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/css/bootstrapValidator.css" rel="stylesheet">
        <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/css/font-awesome.min.css" rel="stylesheet" >
        <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/css/dataTables.bootstrap.min.css" rel="stylesheet" >
        <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/formvalidation/formValidation.min.css" rel="stylesheet" >
        <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/formvalidation/demo.css" rel="stylesheet">
        <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
        <!-- Timeline CSS -->
        <!-- Custom CSS -->
        <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/dist/css/sb-admin-2.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <!-- Custom Fonts -->
        <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script type="text/javascript" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/jquery.mask.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script type="text/javascript" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/bootstrapValidator.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/bootbox.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/dataTables.bootstrap.min.js"></script>
        <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/css/bootstrap.LineaTiempo.css" rel="stylesheet">
        <script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/formvalidation/formValidation.min.js"></script>
        <script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/formvalidation/bootstrap.min.js"></script>
        <script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/formvalidation/reCaptcha2.min.js"></script>
        <!-- include summernote css/js-->
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/css/summernote/summernote.css" rel="stylesheet">
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/summernote/summernote.js"></script>
        <!-- jQuery -->

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/bower_components/raphael/raphael-min.js"></script>


    <!-- Custom Theme JavaScript -->
    <script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/dist/js/sb-admin-2.js"></script>
        <style>
            /* Sticky footer styles
-------------------------------------------------- */
            html {
                position: relative;
                min-height: 100%;
            }
            body {
                font-family: 'Arial';
            }
            /*#page-wrapper .container-fluid,/*,body*/ /* {
                /* Margin bottom by footer height */
                /*margin-bottom: 60px;
            }*/
            .footer {
                position: absolute;
                bottom: 0;
                left:250px;
                width: 100%;
                /* Set the fixed height of the footer here */
                height: 60px;

            }
            .navbar{
                margin-bottom: 0px !important;
            }
            .navbar-inverse {
                border-color: #000000 !important;
                background-color: #222222 !important;
            }
            .navbar-inverse .navbar-top-links > li > a {
                color: #FFBF1C ;
            }
            .navbar-inverse .navbar-brand {
                color: #fff !important;
            }
            /* Custom page CSS
            -------------------------------------------------- */
            /* Not required for template or sticky footer method. */

            /*body > .container {
                padding: 60px 15px 0;
            }
            body > .container-fluid {
                padding: 60px 15px 0;
            }*/
            .container .text-muted {
                margin: 20px 0;
            }

            .footer > .container {
                padding-right: 15px;
                padding-left: 15px;
            }

            code {
                font-size: 80%;
            }
            .col-sm-2, .col-sm-3 {
                text-align: right !important;
            }

            #page-wrapper/*,body*/ {
                //background: -webkit-linear-gradient(90deg, #16222A 10%, #3A6073 90%); /* Chrome 10+, Saf5.1+ */
                //background:    -moz-linear-gradient(90deg, #16222A 10%, #3A6073 90%); /* FF3.6+ */
                ///background:     -ms-linear-gradient(90deg, #16222A 10%, #3A6073 90%); /* IE10 */
                //background:      -o-linear-gradient(90deg, #16222A 10%, #3A6073 90%); /* Opera 11.10+ */
                //background:         linear-gradient(90deg, #16222A 10%, #3A6073 90%); /* W3C */
                background-image: url(<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/img/crossword.png);
                background-repeat: repeat;
            }
            .panel-body{
                color:#000 !important;
            }
            #banner > .img-responsive {
                min-height: 50px;
                width: 100%;
                max-height: 75px;
            }
            #topnavbar {
                margin: 0;
            }
            #topnavbar.affix {
                position: fixed;
                top: 0;
                width: 100%;

            }
            .well{
                background-color: #ffffff;
            }
            .navbar-inverse .navbar-top-links > li > a:focus, .navbar-inverse .navbar-top-links > li > a:hover {
                color: #FFF;
                background-color: #222222;
            }
            #side-menu > li > a,.nav-second-level > li > a,.nav-third-level > li > a{
                color: #000000;
            }
            #side-menu > li > a:hover,#side-menu > .active > a,
            #side-menu > li > .active,
            .nav-second-level > li > a:hover,
            .nav-second-level > .active > a,
            .nav-second-level > li > .active,
            .nav-third-level > li > a:hover,
            .nav-third-level > .active > a,
            .nav-third-level > li > .active{
                color: #000000;
                font-weight: bold;
            }
            .pagination > .active > a, .pagination > .active > a:focus, .pagination > .active > a:hover, .pagination > .active > span, .pagination > .active > span:focus, .pagination > .active > span:hover {
                background-color: #FFCD00;
                color: #000000;
                border-color: #CCC;
            }
            .btn-info {
                background-color: #299AAE;
            }
        </style> 
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/css/UVEN.css" rel="stylesheet">




</head>

<body>

    <div id="wrapper">
        <!--div  id="banner">
            <img class="img-responsive" height="50%" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/img/banner.jpg">

        </div-->
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="topnavbar">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="../../../uven2"><img src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/img/uven-logo-2-1.png"></a>
                </div>
                <ul class="nav  navbar-top-links  navbar-nav  navbar-right">
                        <li >
                            <a href="../../../uven2">Inicio</a>
                        </li>
                        <?php if (Yii::app()->user->isGuest) { ?>
                        <li> <a href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/site/login">Inicio Sesión</a>
                        </li>
                        <li>
                            <a href="../../../uven2/registrarse">Registrarse</a>
                        </li>
                        <?php } ?>
                        <?php if (Yii::app()->user->id['usuario']->usuar_tipou != '2' and Yii::app()->user->id['usuario']->usuar_tipou != '4') { ?>
                        <li>
                            <a href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/uven/pedirtaxi2">Solicitar Taxi</a>
                        </li>
                        <?php } ?>

                    </ul>
                    
                

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                            <?php 

                             $sql="select A.clvmodulo, A.clvmodulopadre, A.stretiqueta, A.strdisparador, A.intorden, A.blnpadre, B.blnborrado, B.blnpermitido from admin_tblmodulos A, admin_tblpermisos B where A.clvmodulo = B.clvmodulo AND A.clvmodulopadre='0' AND B.clvrol='".Yii::app()->user->id['usuario']->usuar_tipou."'
                                GROUP BY A.clvmodulo, B.blnborrado, B.blnpermitido
                                ORDER by intorden asc";
                                
                                $html='';
                                        $connection = Yii::app()->db;
                                        $menu = $connection->createCommand($sql)->queryAll();
                                        //var_dump($sql);
                                        
                                       
                                        foreach ($menu as $key => $value) {
                                            if($value['blnborrado']!=true and $value['blnpermitido']==true ){
                                             if($value['blnpadre']){

                                                $html.= '<li>
                                                    <a href="#" >'.$value['stretiqueta'].'<span class="caret"></span></a>
                                                    <ul class="nav nav-second-level">';
                                                    $sql="select A.clvmodulo, A.clvmodulopadre, A.stretiqueta, A.strdisparador, A.intorden, A.blnpadre , B.blnborrado, B.blnpermitido from admin_tblmodulos A, admin_tblpermisos B where A.clvmodulo = B.clvmodulo AND  A.clvmodulopadre='".$value['clvmodulo']."'
                                                    AND B.clvrol='4'
                                GROUP BY A.clvmodulo, B.blnborrado, B.blnpermitido
                                ORDER by intorden asc";
                                //var_dump($sql);
                                            $submenu = $connection->createCommand($sql)->queryAll();
                                            foreach ($submenu as $key => $valor) {
                                                
                                                if($valor['blnborrado']!=true and $valor['blnpermitido']==true ){    
                                                    if($valor['blnpadre']){
                                                        $html.= '<li>
                                                                    <a href="#" >'.$valor['stretiqueta'].'<span class="caret"></span></a>
                                                                    <ul class="nav nav-third-level">';
                                                        $sql="select A.clvmodulo, A.clvmodulopadre, A.stretiqueta, A.strdisparador, A.intorden, A.blnpadre , B.blnborrado, B.blnpermitido 
                                                            from admin_tblmodulos A, admin_tblpermisos B 
                                                            where A.clvmodulo = B.clvmodulo 
                                                            AND  A.clvmodulopadre='".$valor['clvmodulo']."'
                                                            AND B.clvrol='4'
                                                            GROUP BY A.clvmodulo, B.blnborrado, B.blnpermitido
                                                            ORDER by intorden asc";
                                                            //var_dump($sql);
                                                        $tercero = $connection->createCommand($sql)->queryAll();
                                                        foreach ($tercero as $key => $tercer) {
                                                            if($tercer['blnborrado']!=true and $tercer['blnpermitido']==true ){  
                                                                $html.= '<li><a href="'.Yii::app()->request->getBaseUrl(true).$tercer['strdisparador'].'">'. $tercer['stretiqueta'].'</a></li>';
                                                            }
                                                        }
                                                        $html.= '</ul></li>';
                                                    }else{

                                                     $html.= '<li><a href="'.Yii::app()->request->getBaseUrl(true).$valor['strdisparador'].'">'. $valor['stretiqueta'].'</a></li>';
                                                    }
                                                 }
                                            }
                                            $html.= '</ul></li>';

                                             }else{
                                                 $html.= '<li><a href="'.Yii::app()->request->getBaseUrl(true).$value['strdisparador'].'">'. $value['stretiqueta'].'</a></li>';
                                             }
                                            
                                          } 
                                      }


                                        echo $html;
                                      ?>

                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="container-fluid" >
                <div class="nombre">
                <?php 
                if(!Yii::app()->user->isGuest){
                    echo 'Usuario: '.Yii::app()->user->id['usuario']->usuar_nombr.' '.Yii::app()->user->id['usuario']->usuar_apell;
                }
                ?>
                </div>
                <?php



//echo Yii::app()->user->id['usuario']->usuar_tipo;
                echo $content; ?>
            
            <!--footer class="footer">
                <div class="container">
                    <p class="text-muted text-center"><b>Tesorería de Seguridad Social. <?php echo date('Y')?> &copy; Todos los derechos reservados</b>
                    </p>
                </div>
            </footer-->
            </div>
        </div>
        
            

    </div>
    <!-- /#wrapper -->

    

</body>

</html>
