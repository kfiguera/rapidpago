<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="language" content="es">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <link rel="shortcut icon" href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/img/logito.ico">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>

        <!-- Bootstrap -->
        <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/css/bootstrap.min.css" rel="stylesheet">

        <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/css/bootstrapValidator.css" rel="stylesheet">
        <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/css/font-awesome.min.css" rel="stylesheet" >
        <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/css/dataTables.bootstrap.min.css" rel="stylesheet" >
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script type="text/javascript" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/jquery.mask.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script type="text/javascript" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/bootstrapValidator.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/bootbox.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/dataTables.bootstrap.min.js"></script>
        <link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/css/bootstrap.LineaTiempo.css" rel="stylesheet">
        <script src="http://formvalidation.io/vendor/formvalidation/js/formValidation.min.js"></script>
        <script src="http://formvalidation.io/vendor/formvalidation/js/framework/bootstrap.min.js"></script>
        <script src="http://formvalidation.io/vendor/formvalidation/js/addons/reCaptcha2.min.js"></script>

        <style>
            /* Sticky footer styles
-------------------------------------------------- */
            html {
                position: relative;
                min-height: 100%;
            }
            body {
                /* Margin bottom by footer height */
                margin-bottom: 60px;
            }
            .footer {
                position: absolute;
                bottom: 0;
                width: 100%;
                /* Set the fixed height of the footer here */
                height: 60px;

            }
            .navbar-inverse {
                border-color: #721206 !important;
                background-color: #ad1400 !important;
            }
            .navbar-inverse .navbar-nav > li > a {
                color: #fff !important;
            }
            .navbar-inverse .navbar-brand {
                color: #fff !important;
            }
            /* Custom page CSS
            -------------------------------------------------- */
            /* Not required for template or sticky footer method. */

            /*body > .container {
                padding: 60px 15px 0;
            }
            body > .container-fluid {
                padding: 60px 15px 0;
            }*/
            .container .text-muted {
                margin: 20px 0;
            }

            .footer > .container {
                padding-right: 15px;
                padding-left: 15px;
            }

            code {
                font-size: 80%;
            }
            .col-sm-2, .col-sm-3 {
                text-align: right !important;
            }
            body {
                //background: -webkit-linear-gradient(90deg, #16222A 10%, #3A6073 90%); /* Chrome 10+, Saf5.1+ */
                //background:    -moz-linear-gradient(90deg, #16222A 10%, #3A6073 90%); /* FF3.6+ */
                ///background:     -ms-linear-gradient(90deg, #16222A 10%, #3A6073 90%); /* IE10 */
                //background:      -o-linear-gradient(90deg, #16222A 10%, #3A6073 90%); /* Opera 11.10+ */
                //background:         linear-gradient(90deg, #16222A 10%, #3A6073 90%); /* W3C */
                background-image: url(<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/img/crossword.png);
                background-repeat: repeat;
            }
            .panel-body{
                color:#000 !important;
            }
            #banner > .img-responsive {
                min-height: 50px;
                width: 100%;
                max-height: 75px;
            }
            #topnavbar {
                margin: 0;
            }
            #topnavbar.affix {
                position: fixed;
                top: 0;
                width: 100%;

            }
            .well{
                background-color: #ffffff;
            }
            .navbar-inverse .navbar-nav > .open > a, .navbar-inverse .navbar-nav > .open > a:focus, .navbar-inverse .navbar-nav > .open > a:hover {
                color: #FFF;
                background-color: #787878;
            }
        </style> 

    </head>

    <body id="body">
        <!-- banner -->
        <div  id="banner">
            <img class="img-responsive" src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/img/banner.jpg">

        </div>
        <!-- Fixed navbar -->
        <nav class="navbar navbar-inverse navbar-static-top" role="navigation" id="topnavbar">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><?php echo CHtml::encode(Yii::app()->name); ?></a>
                </div>
                <div id="navbar" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/calcular/index">Calcular Jubilación</a></li>
                        <li><a href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/calcular/pension">Calcular Pensión por Discapacidad</a></li>

                    </ul>
                    
                </div><!--/.nav-collapse -->
            </div>
        </nav>

        <!-- Begin page content -->
        <div class="container-fluid">

<?php echo $content; ?>
        </div>

        <footer class="footer">
            <div class="container">
                <p class="text-muted text-center"><b>Tesorería de Seguridad Social. <?php echo date('Y')?> &copy; Todos los derechos reservados</b></p>
            </div>
        </footer>
        <script>
            $('#topnavbar').affix({
                offset: {
                    top: $('#banner').height()
                }
            });
        </script>


    </body>
</html>
