<!DOCTYPE html>  
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/img/favicon.png">
<title><?php echo CHtml::encode($this->pageTitle); ?></title>
<!-- Bootstrap Core CSS -->
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/css/colors/default-dark.css" id="theme"  rel="stylesheet">
<!-- Form Validation CSS -->
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/form-validation/formValidation.min.css" id="theme"  rel="stylesheet">
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/form-validation/demo.css" id="theme"  rel="stylesheet">
<!--alerts CSS -->
<link href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-122642309-8"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-122642309-8');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-148147142-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-148147142-1');
</script>
<style type="text/css">
	html, {
    background: url(../assets/img/footer_lodyas.png) top left repeat !important;
    height: 100%;
  }
  .login-register {
    background: url(../assets/img/footer_lodyas.png) top left repeat !important;
    height: 100vh;
    padding-top: 5vh;

  }
</style>
</head>
<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="login-register">

  <?php echo $content; ?>
</section>
<!-- jQuery -->


<!-- Bootstrap Core JavaScript -->
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

<!--slimscroll JavaScript -->
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/js/custom.min.js"></script>
<!--Style Switcher -->
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
<!--Form Validation -->
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/form-validation/formValidation.min.js"></script>
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/form-validation/bootstrap.min.js"></script>
<!-- Sweet-Alert  -->
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="<?php echo Yii::app()->request->getBaseUrl(true); ?>/assets/plugins/bower_components/blockUI/jquery.blockUI.js"></script>
</body>
</html>
