<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Inventario</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Inventario</a></li>
            <li><a href="#">Articulos</a></li>
            <li class="active">Registrar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Consultar Articulo</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
                //var_dump($roles);
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                
                <div class="row hide">

                  <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Código</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                <?php echo CHtml::textField('codigo',$roles['inven_codig'], array('class' => 'form-control', 'placeholder' => "Código", 'disabled'=>'true')); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Código</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                <?php echo CHtml::textField('codig',$roles['inven_cprod'], array('class' => 'form-control', 'placeholder' => "Código", 'disabled' => 'disabled')); ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Descripción</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php echo CHtml::textField('descr', $roles['inven_descr'], array('class' => 'form-control', 'placeholder' => "Descripción", 'disabled' => 'disabled')); ?>

                            </div>
                        </div>
                    </div>
                    
                

                </div>
                <div class="row">   


                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Modelo</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php 
                                    $sql="SELECT * FROM inventario_modelo";
                                    $result=$conexion->createCommand($sql)->queryAll();
                                    $data=CHtml::listData($result,'model_codig','model_descr');
                                    echo CHtml::dropDownList('model', $roles['model_codig'], $data,  array('class' => 'form-control', 'prompt' => "Seleccione...", 'disabled'=>'true')); ?>

                            </div>
                        </div>
                    </div>
                
                 
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Precio por Unidad</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                <?php echo CHtml::textField('punid', $this->funciones->TransformarMonto_v($roles['inven_punid'],2), array('class' => 'form-control', 'placeholder' => "Precio por Unidad", 'disabled' => 'disabled')); ?>

                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="row">    
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Tipo de Unidad</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-list"></i></span>
                                <?php 
                                $conexion=yii::app()->db;
                                    $sql="SELECT * FROM p_unidad_tipo";
                                    $result=$conexion->createCommand($sql)->queryAll();
                                    $data=CHtml::listData($result,'tunid_codig','tunid_descr');
                                echo CHtml::dropDownList('tunid', $roles['tunid_codig'],$data , array('class' => 'form-control', 'placeholder' => "Tipo de Unidad","prompt"=>'Seleccione', 'disabled' => 'disabled')); ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Moneda</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                <?php 
                                $conexion=yii::app()->db;
                                    $sql="SELECT * FROM p_moneda";
                                    $result=$conexion->createCommand($sql)->queryAll();
                                    $data=CHtml::listData($result,'moned_codig','moned_descr');
                                echo CHtml::dropDownList('moned', $roles['moned_codig'],$data , array('class' => 'form-control', 'placeholder' => "Moneda","prompt"=>'Seleccione', 'disabled' => 'disabled')); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observaciones</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <?php 

                                    echo CHtml::textArea('obser', $roles['inven_obser'], array('class' => 'form-control', 'placeholder' => "Observaciones", 'disabled' => 'disabled')); ?>

                            </div>
                        </div>
                    </div>
                    
                </div>
                
                <!-- Button -->
                <div class="row controls">
                    <div class="col-sm-12 ">
                        <a href="listado" type="reset" class="btn-block btn btn-info">Volver </a>
                    </div>
                </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
<script>
$('#punid').mask('#.##0,00',{reverse: true,maxlength:false});
$('#desde').mask('00/00/0000');
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                descr: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Descripción" es obligatorio',
                        }
                    }
                },
                codig: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Código" es obligatorio',
                        }
                    }
                },
                punid: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Precio por Unidad" es obligatorio',
                        }
                    }
                },
                canti: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "cantidad" es obligatorio',
                        }
                    }
                },
                tunid: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Unidad" es obligatorio',
                        }
                    }
                },
                moned: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Moneda" es obligatorio',
                        }
                    }
                },
                /*obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Observaciones" es obligatorio',
                        }
                    }
                },*/



            }
        });
    });

</script>