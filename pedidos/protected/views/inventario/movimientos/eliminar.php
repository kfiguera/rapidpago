<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Inventario</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Inventario</a></li>
            <li><a href="#">Movimiento</a></li>
            <li class="active">Eliminar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Eliminar Movimientos</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
               <div class="row hide">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Código</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php echo CHtml::hiddenField('codigo', $roles['almac_numbe'], array('class' => 'form-control', 'placeholder' => "Código")); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">            
                
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Código</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                <?php echo CHtml::textField('codig',$roles['almac_numbe'], array('class' => 'form-control', 'placeholder' => "Código", 'disabled'=>'true')); ?>

                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Descripción</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php echo CHtml::textField('descr', $roles['almac_descr'], array('class' => 'form-control', 'placeholder' => "Descripción", 'disabled'=>'true')); ?>

                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">    
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Ubicación</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php echo CHtml::textField('descr', '', array('class' => 'form-control', 'placeholder' => "Descripción")); ?>

                            </div>
                        </div>
                    </div>                    
                
  
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Responsable</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php echo CHtml::textField('descr', '', array('class' => 'form-control', 'placeholder' => "Descripción")); ?>

                            </div>
                        </div>
                    </div>                    
                
                </div>

                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observaciones</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <?php 

                                    echo CHtml::textArea('obser', $roles['inven_obser'], array('class' => 'form-control', 'placeholder' => "Observaciones", 'disabled'=>'true')); ?>

                            </div>
                        </div>
                    </div>
                    
                </div>
                
            </form>  
              </div>
          </div>



<div class="row">                    
        <div class="panel panel-default" >

            <div class="panel-heading" >
                <h3 class="panel-title">Productos y Servicios 
                    <div class="panel-action">
                        <a href="#" data-perform="panel-collapse">
                            <i class="ti-plus"></i>
                        </a> 
                    </div>
            </h3>
                

            </div>
            <div class="panel-wrapper collapse in">

                <div class="panel-body" >
                    <div class="col-sm-12 table-responsive" id='listado-p_persona'>
                        <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                            <thead>
                                <tr>
                                    <th width="2%">
                                        #
                                    </th>
                                    <th>
                                        Modelo
                                    </th>
                                    <th>
                                        Serial
                                    </th>
                                    <th>
                                        Cantidad
                                    </th>
                                    <th>
                                        Precio Unitario
                                    </th>
                                    <th>
                                        Monto
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                
                                foreach ($productos as $key => $producto) {
                                
                                    $total['canti']+=$producto['iepro_canti'];
                                    $total['preci']+=$producto['iepro_preci'];
                                    $total['monto']+=$producto['iepro_monto'];
                                    $i++;
                                ?>
                                <tr>
                                    <td class="tabla"><?php echo $i ?></td>
                                    <td class="tabla"><?php echo $producto['inven_descr'] ?></td>
                                    <td class="tabla"><?php echo $this->funciones->TransformarMonto_v($producto['iepro_preci'],2) ?></td>
                                    <td class="tabla"><?php echo $this->funciones->TransformarMonto_v($producto['iepro_canti'],0) ?></td>
                                    

                                    <td class="tabla"><?php echo $this->funciones->TransformarMonto_v($producto['iepro_monto'],2) ?></td>
                                </tr>
                                <?php
                                    }   
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th class="tabla" colspan="2">Total</th>
                                    <th class="tabla"><?php echo $this->funciones->TransformarMonto_v($total['preci'],2); ?></th>
                                    <th class="tabla"><?php echo $this->funciones->TransformarMonto_v($total['canti'],0); ?></th>
                                    
                                    <th class="tabla"><?php echo $this->funciones->TransformarMonto_v($total['monto'],2); ?></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div><!-- form -->
            </div>
        </div>  
    </div>
    <div class="row">                    
        <div class="panel panel-default" >
            <div class="panel-body" >
                <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-6 ">
                            <button id="guardar" type="button" class="btn-block btn btn-info">Eliminar  </button>
                        </div>
                        <div class="col-sm-6">
                            <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</form>
<script>
$('#fnaci').mask('00/00/0000');
$('#desde').mask('00/00/0000');
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                corre: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Correo" es obligatorio',
                        },
                        regexp: {
                            regexp: /^[A-Za-z0-9._%+-]+\@[a-zA-Z0-9.-]+\.[A-Za-z]{2,4}$/,
                            message: 'Estimado(a) Usuario(a) el campo "Correo" debe poseer el siguiente formato: ejemplo@gmail.com'
                        }, identical: {
                            field: 'ccorre',
                            message: 'Estimado(a) Usuario(a) el campo "Correo" y su confirmacion no son iguales'
                        }
                    }
                },ccorre: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Confirmar Correo" es obligatorio',
                        },
                        regexp: {
                            regexp: /^[A-Za-z0-9._%+-]+\@[a-zA-Z0-9.-]+\.[A-Za-z]{2,4}$/,
                            message: 'Estimado(a) Usuario(a) el campo "Confirmar Correo"  debe poseer el siguiente formato: ejemplo@gmail.com'
                        }, identical: {
                            field: 'corre',
                            message: 'Estimado(a) Usuario(a) el campo "Correo" y su confirmacion no son iguales'
                        }
                    }
                },contra: {
                    validators: {
                        /*notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Contraseña" es obligatorio',
                        },*/
                        identical: {
                            field: 'ccontra',
                            message: 'Estimado(a) Usuario(a) el campo "Contraseña" y su confirmacion no son iguales'
                        }
                    }
                },ccontra: {
                    validators: {
                        /*notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Confirmar Contraseña" es obligatorio',
                        },*/
                        identical: {
                            field: 'contra',
                            message: 'Estimado(a) Usuario(a) el campo "Contraseña" y su confirmacion no son iguales'
                        }
                    }
                },perso: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Persona" es obligatorio',
                        }
                    }
                },
                nivel: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Nivel" es obligatorio',
                        }
                    }
                },
                /*obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Observaciones" es obligatorio',
                        }
                    }
                },*/



            }
        });
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'eliminar',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>