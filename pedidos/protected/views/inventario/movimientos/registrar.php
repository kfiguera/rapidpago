<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Inventario</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Inventario</a></li>
            <li><a href="#">Movimientos</a></li>
            <li class="active">Registrar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
            <form id='login-form' name='login-form' method="post">

    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Registrar Movimientos</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
                
                <div class="row">          
                   <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Código</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                <?php echo CHtml::textField('codig', '', array('class' => 'form-control', 'placeholder' => "Código")); ?>

                            </div>
                        </div>
                    </div>


             <div class="col-sm-4">
                <div class="form-group">
                    <label>Fecha</label>
                    <input type="text" class="form-control" name="desde" id="desde" placeholder="Fecha">
                </div>
             </div>

                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Almacén Origen</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php echo CHtml::textField('descr', '', array('class' => 'form-control', 'placeholder' => "Descripción")); ?>

                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">    
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Almacén Destino</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php echo CHtml::textField('descr', '', array('class' => 'form-control', 'placeholder' => "Descripción")); ?>

                            </div>
                        </div>
                    </div>                    
                
  
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Estatus</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php echo CHtml::textField('descr', '', array('class' => 'form-control', 'placeholder' => "Descripción")); ?>

                            </div>
                        </div>
                    </div>                    
                
                </div>

                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observaciones</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <?php 

                                    echo CHtml::textArea('obser', '', array('class' => 'form-control', 'placeholder' => "Observaciones")); ?>

                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
                
        <div class="panel panel-default" >

            <div class="panel-heading" >
                <h3 class="panel-title">Registrar Productos
                    <div class="panel-action">
                        <a href="#" data-perform="panel-collapse">
                            <i class="ti-plus"></i>
                        </a> 
                    </div>
            </h3>
                

            </div>
            <div class="panel-wrapper collapse in">

                <div class="panel-body" >
                    <input class="form-control" placeholder="Porcentaje" readonly="readonly" type="hidden" value="" name="bookindex" id="bookindex" /> 
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-11">
                                <div class="row">
                                    
                                    <div class="col-xs-4">
                                        <label>Modelo</label>
                                        <?php 
                                            $data=array();
                                            $sql="SELECT * FROM inventario";
                                            $result=$conexion->createCommand($sql)->queryAll();
                                            $data['Productos']=CHtml::listData($result,'inven_codig','inven_descr');
                                            echo CHtml::dropDownList('produ[0]', '', $data,array('class' => 'form-control select2', 'placeholder' => "Banco", 'prompt'=>'Seleccione...','id'=>'produ_0')); ?>
                                        
                                    </div>
                                    <div class="col-xs-4">
                                        <label>Serial</label>
                                        <?php 
                                            $data=array();
                                            $sql="SELECT * FROM inventario";
                                            $result=$conexion->createCommand($sql)->queryAll();
                                            $data['Productos']=CHtml::listData($result,'inven_codig','inven_descr');
                                            echo CHtml::dropDownList('produ[0]', '', $data,array('class' => 'form-control select2', 'placeholder' => "Banco", 'prompt'=>'Seleccione...','id'=>'produ_0')); ?>
                                        
                                    </div>
                                    <div class="col-xs-4">
                                        <label>Cantidad</label>
                                        <input class="form-control" placeholder="Cantidad" prompt="Seleccione" type="text" value="" name="cprod[0]" id="cprod_0" />        
                                    </div>
                            </div>
                            </div>
                            <div class="col-xs-1">
                                <label>&nbsp;</label>
                                <br>
                                <button type="button" class="btn btn-success btn-block addButton"><i class="fa fa-plus"></i></button>
                            </div>   
                        </div>
                    </div>
                    <!-- Plantilla -->
                    <div class="form-group hide" id="bookTemplate">
                        <div class="row">
                            <div class="col-xs-11">
                                <div class="row">
                                    <div class="col-xs-3 ">
                                        <label>Producto o Servicio</label>
                                        <?php echo CHtml::dropDownList('produ_', '', $data,array('class' => 'form-control', 'placeholder' => "Producto o Servicio", 'prompt'=>'Seleccione...','id'=>'produ_')); ?>
                                    </div>
                                    <div class="col-xs-3">
                                        <label>Modelo</label>
                                        <input class="form-control" placeholder="Cantidad" type="text" value="" name="cprod_" id="cprod_" />        
                                    </div>
                                    <div class="col-xs-3">
                                        <label>Serial</label>
                                        <input class="form-control" placeholder="Precio Unitario" type="text" value="" name="pprod_" id="pprod_" />        
                                    </div>
                                    <div class="col-xs-3">
                                        <label>Cantidad</label>
                                        <input class="form-control" placeholder="Monto" readonly="readonly" type="text" value="" name="mprod_" id="mprod_" />        
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-1">
                                <label>&nbsp;</label>
                                <br>
                                <div class="btn-group btn-group-justified">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-success addButton"><i class="fa fa-plus"></i></button>
                                    </div>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-danger removeButton"><i class="fa fa-minus"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- form -->
                <div class="panel-footer text-center">
                     <input class="form-control" placeholder="Monto Total" readonly="readonly" type="hidden" value="0,00" name="mtotal" id="mtotal" />
                    <b>Monto Total de Entrada: <span id="monto_total">0,00<span></b>
                </div>
            </div>
        </div>  
    </div>

                <!-- Button -->
                <div class="row controls">
                    <div class="col-sm-4 ">
                        <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                    </div>
                    <div class="col-sm-4 ">
                        <button type="reset" class="btn-block btn btn-default">Limpiar  </button>
                    </div>
                    <div class="col-sm-4 ">
                        <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                    </div>
                </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
<script>
$('#punid').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#canti').mask('#.##0',{reverse: true,maxlength:false});
        $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                descr: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Descripción" es obligatorio',
                        }
                    }
                },
                codig: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Código" es obligatorio',
                        }
                    }
                },
                punid: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Precio por Unidad" es obligatorio',
                        }
                    }
                },
                canti: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "cantidad" es obligatorio',
                        }
                    }
                },
                tunid: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Unidad" es obligatorio',
                        }
                    }
                },
                moned: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Moneda" es obligatorio',
                        }
                    }
                },
                /*obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Observaciones" es obligatorio',
                        }
                    }
                },*/



            }
        });
    });

    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'registrar',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>