<?php $conexion=Yii::app()->db; ?>
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Inventario</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Inventario</a></li>
            <li><a href="#">Almacén</a></li>
            <li><a href="#">Asignar Dispositivos</a></li>
            <li class="active">Modificar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<form id='login-form' name='login-form' method="post">
            
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Datos de la Solicitud</h3>
        </div>
        <div class="panel-body">
            <?php 
                $this->funciones->imprimirDatosSolicitud($solicitud);
            ?>
        </div>
    </div>  
</div>
<?php
$sql="SELECT * FROM solicitud_asignacion";
?>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Registrar Productos 
                    <div class="panel-action">
                        <a href="#" data-perform="panel-collapse">
                            <i class="ti-plus"></i>
                        </a> 
                    </div>
            </h3>
        </div>
        <div class="panel-wrapper collapse in">

            <div class="panel-body" >
                <input class="form-control" placeholder="Porcentaje" readonly="readonly" type="hidden" value="" name="bookindex" id="bookindex" /> 
                <div class="row hide">
                    <div class="col-xs-6">
                        <label>Solicitud</label>
                        <?php 
                            echo CHtml::hiddenField('solic', $solicitud['solic_codig'], $data,array('class' => 'form-control ', 'placeholder' => "Solicitud", 'prompt'=>'Seleccione...','id'=>'model_0')); ?>       
                    </div>
                    <div class="col-xs-6">
                        <label>Código</label>
                        <?php 
                            echo CHtml::hiddenField('codig', $solicitud['solic_codig'], $data,array('class' => 'form-control ', 'placeholder' => "Solicitud", 'prompt'=>'Seleccione...','id'=>'model_0')); ?>       
                    </div>
                </div>
                <?php
                    $sql="SELECT * 
                          FROM solicitud_asignacion
                          WHERE solic_codig='".$solicitud['solic_codig']."'";
                    $electivas=$conexion->createCommand($sql)->query();
                    $result=$conexion->createCommand($sql)->queryAll();
                    
                    $row = $electivas->read();
                    $i=0;
                    do{
                        
                        $sql="SELECT * 
                          FROM inventario_seriales
                          WHERE seria_codig='".$row['asign_dispo']."'";
                        
                        $dispo=$conexion->createCommand($sql)->queryRow();

                        $sql="SELECT * 
                          FROM inventario_seriales
                          WHERE seria_codig='".$row['asign_opera']."'";
                        
                        $opera=$conexion->createCommand($sql)->queryRow();
                        
                        ?>
                        <div class="form-group">
                           <div class="row">
                                <div class="col-xs-11">
                                    <div class="row">
                                        <div class="col-xs-2">
                                            <label>Modelo</label>
                                            <?php 
                                                $data=array();
                                                $sql="SELECT a.* 
                                                      FROM inventario_modelo a 
                                                      JOIN inventario b ON (a.model_codig = b.model_codig)
                                                      WHERE b.tinve_codig='1'
                                                      GROUP BY a.model_codig
                                                    ";
                                                $result=$conexion->createCommand($sql)->queryAll();
                                                $data=CHtml::listData($result,'model_codig','model_descr');
                                                echo CHtml::dropDownList('model['.$i.']', $dispo['model_codig'], $data,array('class' => 'form-control ', 'placeholder' => "Banco", 'prompt'=>'Seleccione...','id'=>'model_'.$i.'')); ?>       
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label>Producto </label>
                                                    <?php $sql="SELECT * FROM inventario
                                                    WHERE tinve_codig='1'
                                                      AND inven_codig='".$dispo['inven_codig']."'";
                                                        $result=$conexion->createCommand($sql)->queryAll();
                                                        $data=CHtml::listData($result,'inven_codig','inven_descr');
                                                        echo CHtml::dropDownList('produ['.$i.']', $dispo['inven_codig'], $data,array('class' => 'form-control', 'placeholder' => "Banco", 'prompt'=>'Seleccione...','id'=>'produ_'.$i.'')); ?>
                                                    
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Serial </label>
                                                    <?php 
                                                        $sql="SELECT * FROM inventario_seriales
                                                        WHERE seria_codig='".$dispo['seria_codig']."'";
                                                        $result=$conexion->createCommand($sql)->queryAll();
                                                        $data=CHtml::listData($result,'seria_codig','seria_numer');
                                                        echo CHtml::dropDownList('seria['.$i.']', $dispo['seria_codig'], $data,array('class' => ' form-control', 'placeholder' => "Banco", 'prompt'=>'Seleccione...','id'=>'seria_'.$i.'')); ?>
                                                    
                                            </div>
                                        </div>
                                        <div class="col-xs-2">
                                            <label>Operador Telefónico</label>

                                            <?php 
                                                $data=array();
                                                $sql="SELECT a.* 
                                                      FROM inventario_modelo a 
                                                      JOIN inventario b ON (a.model_codig = b.model_codig)
                                                      WHERE b.tinve_codig='2'
                                                      GROUP BY a.model_codig
                                                    ";
                                                $result=$conexion->createCommand($sql)->queryAll();
                                                $data=CHtml::listData($result,'model_codig','model_descr');
                                                echo CHtml::dropDownList('opera['.$i.']', $opera['model_codig'], $data,array('class' => 'form-control ', 'placeholder' => "Banco", 'prompt'=>'Seleccione...','id'=>'opera_'.$i.'')); ?>        
                                        </div>
                                        <div class="col-xs-3">
                                            <label>Serial Tarjeta SIM</label>
                                            <?php 
                                                $sql="SELECT * FROM inventario_seriales
                                                    WHERE seria_codig='".$opera['seria_codig']."'";
                                                $result=$conexion->createCommand($sql)->queryAll();
                                                $data=CHtml::listData($result,'seria_codig','seria_numer');
                                                echo CHtml::dropDownList('srsim['.$i.']', $opera['seria_codig'], $data,array('class' => 'form-control ', 'placeholder' => "Banco", 'prompt'=>'Seleccione...','id'=>'srsim_'.$i.'')); ?>          
                                        </div>
                                        <script>
                                            $(document).ready(function () {
                                        $("#seria_<?php echo $i; ?>").select2({
                                          minimumInputLength: 2,
                                            allowClear: true
                                            });
                                        $("#srsim_<?php echo $i; ?>").select2({
                                        minimumInputLength: 2,
                                            allowClear: true
                                            });
                                        });
                                    </script>
                                    </div>
                                </div>

                                <?php
                                    if($i==0){
                                ?>
                                <div class="col-xs-1">
                                    <label>&nbsp;</label>
                                    <br>
                                    <button type="button" class="btn btn-success btn-block addButton"><i class="fa fa-plus"></i></button>
                                </div>
                                <?php
                                    }else{
                                ?>
                                <div class="col-xs-1">
                                    <label>&nbsp;</label>
                                    <br>
                                    <div class="btn-group btn-group-justified">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-success addButton"><i class="fa fa-plus"></i></button>
                                        </div>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-danger removeButton"><i class="fa fa-minus"></i></button>
                                        </div>
                                    </div>
                                </div>
                    
                                <?php
                                    }
                                    $a++;
                                    $i++;
                                ?>   
                            </div>
                            
                        </div>
                        <?php
                    }while(($row = $electivas->read()) !== false);
                ?>
                
                <div class="form-group hide" id="bookTemplate">
                    

                    <div class="row">
                        <div class="col-xs-11">
                            <div class="row">
                                <div class="col-xs-2">
                                    <label>Modelo</label>
                                    <?php 
                                        $data=array();
                                        $sql="SELECT a.* 
                                              FROM inventario_modelo a 
                                              JOIN inventario b ON (a.model_codig = b.model_codig)
                                              WHERE b.tinve_codig='1'
                                              GROUP BY a.model_codig
                                            ";
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'model_codig','model_descr');
                                        echo CHtml::dropDownList('model_', $roles['model_codig'], $data,array('class' => 'form-control ', 'placeholder' => "Banco", 'prompt'=>'Seleccione...','id'=>'model_')); ?>       
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label>Producto </label>
                                            <?php $sql="SELECT * FROM inventario
                                            WHERE tinve_codig='1'";
                                                $result=$conexion->createCommand($sql)->queryAll();
                                                $data=CHtml::listData($result,'inven_codig','inven_descr');
                                                echo CHtml::dropDownList('produ_', $roles['inven_codig'], $data,array('class' => 'form-control', 'placeholder' => "Banco", 'prompt'=>'Seleccione...','id'=>'produ_')); ?>
                                            
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Serial </label>
                                            <?php 
                                                $data=array();
                                                echo CHtml::dropDownList('seria_', $roles['inven_codig'], $data,array('class' => 'form-control', 'placeholder' => "Banco", 'prompt'=>'Seleccione...','id'=>'seria_')); ?>
                                            
                                    </div>
                                </div>
                                <div class="col-xs-2">
                                    <label>Operador Telefónico</label>

                                    <?php 
                                        $data=array();
                                        $sql="SELECT a.* 
                                              FROM inventario_modelo a 
                                              JOIN inventario b ON (a.model_codig = b.model_codig)
                                              WHERE b.tinve_codig='2'
                                              GROUP BY a.model_codig
                                            ";
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'model_codig','model_descr');
                                        echo CHtml::dropDownList('opera_', $roles['model_codig'], $data,array('class' => 'form-control ', 'placeholder' => "Banco", 'prompt'=>'Seleccione...','id'=>'opera_')); ?>        
                                </div>
                                <div class="col-xs-3">
                                    <label>Serial Tarjeta SIM</label>
                                    <?php 
                                        $data=array();
                                        
                                        echo CHtml::dropDownList('srsim_', $roles['model_codig'], $data,array('class' => 'form-control', 'placeholder' => "Banco", 'prompt'=>'Seleccione...','id'=>'srsim_')); ?>          
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-xs-1">
                            <label>&nbsp;</label>
                            <br>
                            <div class="btn-group btn-group-justified">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-success addButton"><i class="fa fa-plus"></i></button>
                                </div>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-danger removeButton"><i class="fa fa-minus"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
                <!-- Button -->
                <div class="row controls">
                    <div class="col-sm-4 ">
                        <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                    </div>
                    <div class="col-sm-4 ">
                        <button type="reset" class="btn-block btn btn-default">Limpiar  </button>
                    </div>
                    <div class="col-sm-4 ">
                        <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                    </div>
                </div>
            </div>
        </div><!-- form -->
    </div>  
</div>
</form>
<script>
    $('#punid').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#canti').mask('#.##0',{reverse: true,maxlength:false});
$('#desde').mask('00/00/0000');

    
</script>




<script type="text/javascript">
    var model = {
            row: '.col-xs-3',   // The title is placed inside a <div class="col-xs-4"> element
            validators: {
                notEmpty: {
                    message: 'Debe Seleccionar un Modelo'
                }
            }
        },
        produ = {
            row: '.col-sm-3',   // The title is placed inside a <div class="col-xs-4"> element
            validators: {
                notEmpty: {
                    message: 'Debe Seleccionar un Producto'
                }
            }
        },
        cprod = {
            row: '.col-ms-2',   // The title is placed inside a <div class="col-xs-4"> element
            validators: {
                notEmpty: {
                    message: 'Debe ingresar una Cantidad'
                }
            }
        },
        pprod = {
            row: '.col-sm-2',
            validators: {
                notEmpty: {
                    message: 'Debe Ingresar un Valor'
                },
            }
        },
        mprod = {
            row: '.col-sm-2',
            validators: {
                notEmpty: {
                    message: 'Debe Ingresar un Valor'
                },
            }
        },
        /*opera = {
            row: '.col-xs-2',
            validators: {
                notEmpty: {
                    message: 'Debe Ingresar un Valor'
                },
            }
        },
        srsim = {
            row: '.col-xs-2',
            validators: {
                notEmpty: {
                    message: 'Debe Ingresar un Valor'
                },
            }
        },*/

        seria = {
            row: '.col-sm-2',
            validators: {
                notEmpty: {
                    message: 'Debe Ingresar un Valor'
                },
            }
        },
        bookIndex = 0,
        contador = 0;
        
   </script>
<script>
    $('#monto').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#mpago').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#devol').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#flete').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#reten').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#descu').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#cprod_0').mask('#.##0',{reverse: true,maxlength:false});
    $('#pprod_0').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#mprod_0').mask('#.##0,00',{reverse: true,maxlength:false});
    //$('#nfact').mask('#.##0',{reverse: true,maxlength:false});
    //$('#ncont').mask('#.##0',{reverse: true,maxlength:false});
    $('#femis').mask('99/99/9999');
    $('#fentr').mask('99/99/9999');
    $('#fvenc').mask('99/99/9999');
    $('#fpago').mask('99/99/9999');
    $('#freci').mask('99/99/9999');

    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                
                'model[0]': model,
                'produ[0]': produ,
                'cprod[0]': cprod,
                'pprod[0]': pprod,
                'mprod[0]': mprod,
                /*'opera[0]': opera,
                'srsim[0]': srsim,*/
                'seria[0]': seria,

                /*obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Observaciones" es obligatorio',
                        }
                    }
                },*/



            }
        });
    }).on('click', '.addButton', function() {
        //if(contador<4){ 
            contador++;
            //alert(contador);
            bookIndex++;
            document.getElementById('bookindex').value = bookIndex;
            var $template = $('#bookTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .attr('data-book-index', bookIndex)
                                .insertBefore($template);

            // Update the name attributes
            $clone
                .find('[name="model_"]').attr('name', 'model[' + bookIndex + ']').end()
                .find('[name="produ_"]').attr('name', 'produ[' + bookIndex + ']').end()
                .find('[name="seria_"]').attr('name', 'seria[' + bookIndex + ']').end()
                .find('[name="cprod_"]').attr('name', 'cprod[' + bookIndex + ']').end()
                .find('[name="pprod_"]').attr('name', 'pprod[' + bookIndex + ']').end()
                .find('[name="mprod_"]').attr('name', 'mprod[' + bookIndex + ']').end()
                .find('[name="opera_"]').attr('name', 'opera[' + bookIndex + ']').end()
                .find('[name="srsim_"]').attr('name', 'srsim[' + bookIndex + ']').end()
                .find('[id="model_"]').attr('id', 'model_' + bookIndex ).end()
                .find('[id="produ_"]').attr('id', 'produ_' + bookIndex ).end()
                .find('[id="seria_"]').attr('id', 'seria_' + bookIndex ).end()
                .find('[id="cprod_"]').attr('id', 'cprod_' + bookIndex ).end()
                .find('[id="pprod_"]').attr('id', 'pprod_' + bookIndex ).end()
                .find('[id="mprod_"]').attr('id', 'mprod_' + bookIndex ).end()
                .find('[id="opera_"]').attr('id', 'opera_' + bookIndex ).end()
                .find('[id="srsim_"]').attr('id', 'srsim_' + bookIndex ).end()


            // Add new fields
            // Note that we also pass the validator rules for new field as the third parameter
            $('#login-form')
                .formValidation('addField', 'model[' + bookIndex + ']', model)
                .formValidation('addField', 'produ[' + bookIndex + ']', produ)
                .formValidation('addField', 'cprod[' + bookIndex + ']', cprod)
                .formValidation('addField', 'pprod[' + bookIndex + ']', pprod)
                .formValidation('addField', 'mprod[' + bookIndex + ']', mprod)
                .formValidation('addField', 'seria[' + bookIndex + ']', seria);
                //.formValidation('addField', 'opera[' + bookIndex + ']', opera)
                //.formValidation('addField', 'srsim[' + bookIndex + ']', srsim);
            $('#cprod_' + bookIndex).mask('#.##0',{reverse: true,maxlength:false});
            $('#pprod_' + bookIndex).mask('#.##0,00',{reverse: true,maxlength:false});
            $('#mprod_' + bookIndex).mask('#.##0,00',{reverse: true,maxlength:false});
            //agresgar ajax
            $('#login-form').append('<script>'+
                '$("#model_'+bookIndex+'").change(function () {'+
                    '$.ajax({'+
                        '"url": "<?php echo CController::createUrl("funciones/ComboProductosAsignar"); ?>", '+
                        '"data": {'+
                            '"modelo": model_'+bookIndex+'.value'+
                        '}, '+
                        '"type": "POST",'+
                        '"cache":false,'+
                        '"success": function (html){'+
                            'jQuery("#produ_'+bookIndex+'").html(html)'+
                        '}'+
                    '});'+
                '});'+
            '<\/script>');
            $('#login-form').append('<script>'+
                '$("#produ_'+bookIndex+'").change(function () {'+
                    '$.ajax({'+
                        '"url": "<?php echo CController::createUrl("funciones/ComboSerial"); ?>", '+
                        '"data": {'+
                            '"modelo": model_'+bookIndex+'.value,'+
                            '"producto": produ_'+bookIndex+'.value,'+
                            '"tipo": 1'+
                        '}, '+
                        '"type": "POST",'+
                        '"success": function (html){'+
                            'jQuery("#seria_'+bookIndex+'").html(html)'+
                        '}'+
                    '});'+
                '});'+
            '<\/script>');
            $('#login-form').append('<script>'+
                '$("#opera_'+bookIndex+'").change(function () {'+
                    '$.ajax({'+
                        '"url": "<?php echo CController::createUrl("funciones/ComboSerialSim"); ?>", '+
                        '"data": {'+
                            '"modelo": opera_'+bookIndex+'.value,'+
                            '"producto": produ_'+bookIndex+'.value,'+
                            '"tipo": 1'+
                        '}, '+
                        '"type": "POST",'+
                        '"success": function (html){'+
                            'jQuery("#srsim_'+bookIndex+'").html(html)'+
                        '}'+
                    '});'+
                '});'+
            '<\/script>');
            $('#login-form').append('<script>'+
                '$("#seria_'+bookIndex+'").select2({'+
                  'minimumInputLength: 2,'+
                    'allowClear: true'+
                    '});'+
                '$("#srsim_'+bookIndex+'").select2({'+
                'minimumInputLength: 2,'+
                    'allowClear: true'+
                    '});'+ 
            '<\/script>');
            /*$('#login-form').append('<script>'+
                '$("#produ_'+bookIndex+'").select2();'+
            '<\/script>');*/
           

        /*}else{
            bootbox.alert('No se pueden agregar más de 5 Producctos');
        }*/
    }).on('click', '.removeButton', function() {// Remove button click handler
        //alert(contador);
        contador=contador-1;
        var $row  = $(this).parents('.form-group'),
            index = $row.attr('data-book-index');
        // Remove fields
        $('#login-form')
            .formValidation('removeField', $row.find('[name="accsesorios[' + index + ']"]'))
            .formValidation('removeField', $row.find('[name="valoracc[' + index + ']"]'))
            .formValidation('removeField', $row.find('[name="valoraccmin[' + index + ']"]'));
        // Remove element containing the fields
        $row.remove();
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'modificar',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>
<script>
    $("#model_0").change(function () {
        $.ajax({
            "url": "<?php echo CController::createUrl('funciones/ComboProductosAsignar'); ?>", 
            "data": {
                "modelo": model_0.value
            }, 
            "type": "POST",

            'cache':false,
            "success": function (html){
                jQuery("#produ_0").html(html)
            }
        });
    });
    $("#produ_0").change(function () {
        $.ajax({
            "url": "<?php echo CController::createUrl('funciones/ComboSerial'); ?>", 
            "data": {
                "modelo": model_0.value,
                "producto": produ_0.value,
                "tipo": 1
            }, 
            "type": "POST",
            "success": function (html){
                jQuery("#seria_0").html(html)
            }
        });
    });
    $("#opera_0").change(function () {
        $.ajax({
            "url": "<?php echo CController::createUrl('funciones/ComboSerialSim'); ?>", 
            "data": {
                "modelo": opera_0.value,
                "producto": produ_0.value,
                "tipo": 1
            }, 
            "type": "POST",
            "success": function (html){
                jQuery("#srsim_0").html(html)
            }
        });
    });
    
</script>