<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Inventario</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Inventario</a></li>
            <li><a href="#">Asignar Dispositivo</a></li>
            <li class="active">Listado</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php
//Mostrar mensajes del controlador
        foreach (Yii::app()->user->getFlashes() as $key => $message) {
          echo '<div class="alert alert-' . $key . ' alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              ' . $message  .' </div>';          
        }
$connection = Yii::app()->db;
if(Yii::app()->user->id['usuario']['permiso']==1){
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Criterio de Busqueda</h3>
    </div>
    <div class="panel-body" >
        <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'p_personas', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            $connection = Yii::app()->db;

        ?>
        <div class="row">
                
                <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Desde</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <?php echo CHtml::textField('femis', '', array('class' => 'form-control', 'placeholder' => "Fecha Desde")); ?>

                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Hasta</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <?php echo CHtml::textField('femis', '', array('class' => 'form-control', 'placeholder' => "Fecha Hasta")); ?>

                            </div>
                        </div>
                    </div>


                <div class="col-sm-4">
                <div class="form-group">
                    <label>N° Solicitud</label>
                    <input type="text" class="form-control" name="codigo" id="codigo" placeholder="Usuario">
                </div>
            </div>

        <div class="row">
            <div class="col-sm-4">
                        <div class="form-group">
                            <label>Modelo</label>
                            <select class="form-control" name="estatu" id="estatu">
                                <option value="">Seleccione...</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Serial</label>
                            <select class="form-control" name="estatu" id="estatu">
                                <option value="">Seleccione...</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Serial de Trajeta SIM</label>
                            <select class="form-control" name="estatu" id="estatu">
                                <option value="">Seleccione...</option>
                            </select>
                        </div>
                    </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                        <div class="form-group">
                            <label>Operador de Telefonia</label>
                            <select class="form-control" name="estatu" id="estatu">
                                <option value="">Seleccione...</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Estatus</label>
                            <select class="form-control" name="estatu" id="estatu">
                                <option value="">Seleccione...</option>
                            </select>
                        </div>
                    </div>
                    </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <button type="button" id="buscar" class="btn btn-block btn-info">Buscar</button>
            </div>
            <div class="col-sm-6">
                <button type="reset" class="btn btn-block btn-default">Limpiar</button>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div> 
<?php } ?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Listado</h3>
    </div>
    <div class="panel-body" >
        
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-p_persona'>
                <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                #
                            </th>
                            <th>
                                N° Solicitud
                            </th>
                            <th>
                                Cantidad de Dispositivos
                            </th>
                            <th>
                                Ciente VIP
                            </th>
                            <th>
                                Usuario de Registro
                            </th>
                            <th>
                                Fecha de Registro
                            </th>
                            <th>
                                Estatus
                            </th>
                    
                            <th width="5%">
                                &nbsp;
                            </th>
                            <th width="5%">
                                &nbsp;
                            </th>

                            <!--th width="5%">
                                &nbsp;
                            </th>
                            <th width="5%">
                                &nbsp;
                            </th-->
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        
                        $sql = $this->funciones->SqlListadoSolicitudes('11');
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        $j=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                            $j++;

                            $sql="SELECT * 
                                  FROM solicitud_trayectoria 
                                  WHERE solic_codig = '".$row['solic_codig']."'
                                    AND estat_codig = '".$row['estat_codig']."'";

                            $trayectoria = $connection->createCommand($sql)->queryRow();

                            $inicio=$trayectoria['traye_finic'].' '.$trayectoria['traye_hinic'];
                            $fin=date('Y-m-d H:i:s');
                            $diff=$this->funciones->diferenciaHoras($inicio,$fin);
                            $horas=($diff->days * 24 )  + ( $diff->h );
                            $semaforo=$this->funciones->semaforo($connection,$row['estat_codig'],$horas);
                            
                            $sql = "SELECT count(asign_codig) cantidad
                                FROM solicitud_asignacion a 
                                WHERE solic_codig='".$row['solic_codig'] ."'";
                            $asignar = $connection->createCommand($sql)->queryRow();
                           
                            $sql = "SELECT *
                                FROM p_banco 
                                WHERE banco_codig='".$row['banco_codig'] ."'";
                            $banco = $connection->createCommand($sql)->queryRow();
                            $vip=array('1'=>'SI', '2'=>'NO');
                        ?>
                        <tr>
                            <td><?php echo $i ?></td>

                            <td><?php echo $row['solic_numer'] ?></td>
                            <td><?php echo $this->funciones->transformarMonto_v($asignar['cantidad'],0) ?></td>
                           <td class="tabla"><?php echo $vip[$row['solic_clvip']] ?></td>
                            <td class="tabla"><?php echo $row['perso_pnomb'].' '.$row['perso_papel'] ?></td>
                            <td><?php echo $this->funciones->transformarFecha_v($row['solic_fcrea']).' '.$row['solic_hcrea'] ?></td>
                            <td>
                                
                                <?php echo $semaforo.' '.$row['estat_descr'] ?></td>
                            <td>
                                <a href="consultar?c=<?php echo $row['solic_codig'] ?>" class="btn btn-block btn-info"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Consultar" >
                                    <i class="fa fa-search"></i>
                                </a>
                            </td>
                            <td>
                                <a href="modificar?c=<?php echo $row['solic_codig'] ?>" class="btn btn-block btn-info"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Asignar" >
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </td>  
                            <!--td>
                                <a href="aprobar?c=<?php echo $row['solic_codig'] ?>" class="btn btn-block btn-success"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Aprobar" >
                                    <i class="fa fa-check"></i>
                                </a>
                            </td> 
                            <td>
                                <a href="reversar?c=<?php echo $row['solic_codig'] ?>" class="btn btn-block btn-danger"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Reversar" >
                                    <i class="fa fa-close"></i>
                                </a>
                            </td-->   
                        </tr>
                        <?php
                            }   
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <div class="row">
            <div class="col-sm-6">
                <a href="AprobarSolicitud" class="btn btn-block btn-info" >Aprobar Asignacion</a>
            </div>
            <div class="col-sm-6">
                <a href="ReversarSolicitud" class="btn btn-block btn-info" >Reversar Aprobación</a>
            </div>
        </div>
    </div>
</div>  
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#buscar').click(function () {
        var formData = new FormData($("#p_personas")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-p_persona').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>
<script>
$('#cantidad').mask('#.##0',{reverse: true,maxlength:false});
$('#punidad').mask('#.##0,00',{reverse: true,maxlength:false});

</script>
