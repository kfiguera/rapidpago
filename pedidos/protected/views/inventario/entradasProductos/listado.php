<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Producto</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Ventas</a></li>            
            
            <li><a href="#">Producto</a></li>
            <li class="active">Listado</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Criterio de Busqueda</h3>
    </div>
    <div class="panel-body" >
        <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'p_personas', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            $connection = Yii::app()->db;

        ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Factura Nro:</label>
                    <?php echo CHtml::textField('nfact', $f, array('class' => 'form-control', 'placeholder' => "Número de Producto", 'readonly'=>'true')); ?>
                </div>
            </div>
            
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Descripción</label>
                    <?php echo CHtml::textField('descr', '', array('class' => 'form-control', 'placeholder' => "Descripción")); ?>
                </div>
            </div>
            
        </div>
        
        <div class="row">
            <div class="col-sm-4">
                <button type="button" id="buscar" class="btn btn-block btn-info">Buscar</button>
            </div>
            <div class="col-sm-4">
                <button type="reset" class="btn btn-block btn-default">Limpiar</button>
            </div>
            <div class="col-sm-4">
                <a href="../factura" class="btn btn-block btn-default" >Volver</a>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div> 
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Listado</h3>
    </div>
    <div class="panel-body" >
        
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-p_persona'>
                <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                #
                            </th>
                            <th>
                                Descripción
                            </th>
                            <th>
                                Precio Unitario
                            </th>
                            <th>
                                Cantidad
                            </th>
                            <th>
                                Monto
                            </th>
                            <th width="5%">
                                Consultar
                            </th>
                            <th width="5%">
                                Modificar
                            </th>
                            <th width="5%">
                                Eliminar
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "SELECT * FROM factura_producto a 
                                JOIN inventario b ON (a.inven_codig=b.inven_codig)
                                WHERE factu_codig='".$f."'";
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        while (($row = $p_persona->read()) !== false) {
                            $total['canti']+=$row['fprod_canti'];
                            $total['preci']+=$row['fprod_preci'];
                            $total['monto']+=$row['fprod_monto'];
                            $i++;
                        ?>
                        <tr>
                            <td class="tabla"><?php echo $i ?></td>
                            <td class="tabla"><?php echo $row['inven_descr'] ?></td>
                            <td class="tabla"><?php echo $this->funciones->TransformarMonto_v($row['fprod_preci'],2) ?></td>
                            <td class="tabla"><?php echo $this->funciones->TransformarMonto_v($row['fprod_canti'],0) ?></td>
                            <td class="tabla"><?php echo $this->funciones->TransformarMonto_v($row['fprod_monto'],2) ?></td>

                            <td class="tabla"><a href="consultar?f=<?php echo $f ?>&c=<?php echo $row['fprod_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-search"></i></a></td>
                            <td class="tabla"><a href="modificar?f=<?php echo $f ?>&c=<?php echo $row['fprod_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-pencil"></i></a></td>
                            <td class="tabla"><a href="eliminar?f=<?php echo $f ?>&c=<?php echo $row['fprod_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-close"></i></a></td>
                        </tr>
                        <?php
                            }   
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="tabla" colspan="2">Total</th>
                            <th class="tabla"><?php echo $this->funciones->TransformarMonto_v($total['preci'],2); ?></th>
                            <th class="tabla"><?php echo $this->funciones->TransformarMonto_v($total['canti'],0); ?></th>
                            
                            <th class="tabla"><?php echo $this->funciones->TransformarMonto_v($total['monto'],2); ?></th>
                            <th class="tabla" colspan="3"></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <a href="registrar?f=<?php echo $f ?>" class="btn btn-block btn-info" >Nuevo</a>
            </div>
        </div>
    </div>
</div>  
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#buscar').click(function () {
        var formData = new FormData($("#p_personas")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-p_persona').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>
