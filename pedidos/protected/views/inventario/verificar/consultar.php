<?php $conexion = Yii::app()->db; ?>
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Inventario</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Inventario</a></li>
            <li><a href="#">Almacén</a></li>
            <li><a href="#">Solicitudes Pendientes por Verificar</a></li>
            <li class="active">Consultar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Datos de la Solicitud</h3>
        </div>
        <div class="panel-body">
            <?php 
                $this->funciones->imprimirDatosSolicitud($solicitud);
            ?>
        </div>
    </div>  
</div>
<div class="row">                    
    <div class="panel panel-default" >
        <div class="panel-heading" >
            <h3 class="panel-title"> Consultar Verificación
                <div class="panel-action">
                    <a href="#" data-perform="panel-collapse">
                        <i class="ti-plus"></i>
                    </a> 
                </div>
            </h3>
        </div>
        <div class="panel-wrapper collapse in">
            <div class="panel-body" >
                <div class="row">
                    <div class="col-sm-12">  
                        <?php
                        $sql="SELECT * FROM solicitud_asignacion a
                              WHERE solic_codig='".$solicitud['solic_codig']."'";
                        $asignacion=$conexion->createCommand($sql)->queryAll();
                        if($asignacion){
                            ?>
                        <table id='auditoria'  class="table table-bordered table-hover dataTable">
                            <thead>
                                <tr>
                                    <th width="5%"># </th>
                                    <th width="19%">Dispositivo </th>
                                    <th width="19%">Modelo</th>
                                    <th width="19%">Serial</th> 
                                    <th width="19%">Operador Telefónico</th>    
                                    <th width="19%">Serial SIM</th>    
                                </tr>    
                            </thead>
                            <tbody>
                            <?php
                            $i=0;
                            foreach ($asignacion as $key => $value) {
                                $i++;
                                $sql="SELECT * 
                                      FROM inventario_seriales a
                                      JOIN inventario_modelo b ON (a.model_codig = b.model_codig)
                                      JOIN inventario c ON (a.inven_codig = c.inven_codig)
                                      WHERE a.seria_codig='".$value['asign_dispo']."'";
                                $dispo=$conexion->createCommand($sql)->queryRow();

                                $sql="SELECT * 
                                      FROM inventario_seriales a
                                      JOIN inventario_modelo b ON (a.model_codig = b.model_codig)
                                      JOIN inventario c ON (a.inven_codig = c.inven_codig)
                                      WHERE a.seria_codig='".$value['asign_opera']."'";
                                $opera=$conexion->createCommand($sql)->queryRow();
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $dispo['inven_descr']; ?></td>
                                    <td><?php echo $dispo['model_descr']; ?></td>
                                    <td><?php echo $dispo['seria_numer']; ?></td>
                                    <td><?php echo $opera['model_descr']; ?></td>
                                    <td><?php echo $opera['seria_numer']; ?></td>
                                </tr>
                                <?php
                            }
                        }
                        
                        ?>
                            </tbody>
                        </table>                           
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="row controls">
                    <div class="col-sm-12 ">
                        <a href="listado" type="reset" class="btn-block btn btn-info">Volver </a>
                    </div>
                </div>
            </div>
            <!-- Button -->
        </div><!-- form -->
    </div>  
</div>
<script>
$('#punid').mask('#.##0,00',{reverse: true,maxlength:false});
$('#desde').mask('00/00/0000');
    $(document).ready(function () {

        $('#auditoria').DataTable();
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                descr: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Descripción" es obligatorio',
                        }
                    }
                },
                codig: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Código" es obligatorio',
                        }
                    }
                },
                punid: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Precio por Unidad" es obligatorio',
                        }
                    }
                },
                canti: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "cantidad" es obligatorio',
                        }
                    }
                },
                tunid: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Unidad" es obligatorio',
                        }
                    }
                },
                moned: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Moneda" es obligatorio',
                        }
                    }
                },
                /*obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Observaciones" es obligatorio',
                        }
                    }
                },*/



            }
        });
    });

</script>