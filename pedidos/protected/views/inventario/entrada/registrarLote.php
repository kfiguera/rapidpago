<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Inventario</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Inventario</a></li>
            <li>Entrada</a></li>
            <li class="active">Registrar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<form id='login-form' name='login-form' method="post">
    <div class="row">                    
        <div class="panel panel-default" >

            <div class="panel-heading" >
                <h3 class="panel-title">Registrar Proovedor<div class="panel-action">
                        <a href="#" data-perform="panel-collapse">
                            <i class="ti-plus"></i>
                        </a> 
                    </div>
                </h3>
            </div>
        <div class="panel-wrapper collapse in">
            <div class="panel-body" >
                <?php
                    $conexion=Yii::app()->db;
                ?>
                    <div class="row hide">
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Proveedor</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <?php 
                                        echo CHtml::hiddenField('clien', '',array('class' => 'form-control', 'placeholder' => "Descripción", 'prompt'=>'Seleccione...')); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Tipo de Proveedor</label>
                                <?php 
                                        $sql="SELECT * FROM p_cliente_tipo";
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'tclie_codig','tclie_descr');
                                        echo CHtml::dropDownList('tclie', '', $data,array(
                                            'ajax' => array(
                                                'type' => 'POST',
                                                'url' => CController::createUrl('funciones/TipoDocumento'), // Controlador que devuelve las p_ciudades relacionadas
                                                'update' => '#tdocu', // id del item que se actualizará
                                            ),'class' => 'form-control ', 'placeholder' => "Descripción", 'prompt'=>'Seleccione...')); ?>

                                
                            </div>
                        </div>
                                        
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Documento de Identidad</label>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <?php 
                                        echo CHtml::dropDownList('tdocu', '', '',array('ajax' => array(
                                                'type' => 'POST',
                                                'url' => CController::createUrl('funciones/ListarProveedor'),
                                                'dataType' => 'json',
                                                'success' => 'ListarProveedor',
                                            ),'class' => 'form-control ', 'placeholder' => "Descripción", 'prompt'=>'Seleccione...')); ?>
                                    </div>
                                    <div class="col-sm-8">
                                        <?php echo CHtml::textField('ndocu', '', array('ajax' => array(
                                                'type' => 'POST',
                                                'url' => CController::createUrl('funciones/ListarProveedor'),
                                                'dataType' => 'json',
                                                'success' => 'ListarProveedor',
                                            ),'class' => 'form-control', 'placeholder' => "Número de Documento")); ?>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Nombre o Razón Social</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                    <?php echo CHtml::textField('denom', '', array('class' => 'form-control', 'placeholder' => "Nombre o Razón Social")); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row hide">
                        <div class="col-sm-6">        
                            <div class="form-group">
                                <label>Número de Contrato</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-archive"></i></span>
                                    <?php echo CHtml::textField('ccont', '', array('class' => 'form-control', 'placeholder' => "Número de Contrato")); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">        
                        <div class="col-sm-6">        
                            <div class="form-group">
                                <label>Correo Electrónico</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    <?php 
                                        echo CHtml::textField('corre', '', array('class' => 'form-control', 'placeholder' => "Correo Electrónico")); ?>
                                </div>
                            </div>
                        </div>
                    
                    
                        <div class="col-sm-6">        
                            <div class="form-group">
                                <label>Teléfono</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-archive"></i></span>
                                    <?php echo CHtml::textField('telef', '', array('class' => 'form-control', 'placeholder' => "Teléfono")); ?>

                                </div>
                            </div>
                        </div>
                        
                    
                    </div>
                    <div class="row">
                        <div class="col-sm-12">        
                            <div class="form-group">
                                <label>Dirección</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-home"></i></span>
                                    <?php 

                                        echo CHtml::textArea('direc', '', array('class' => 'form-control', 'placeholder' => "Dirección")); ?>

                                </div>
                            </div>
                        </div>
                        
                    </div>
            </div><!-- form -->
        </div> 
        </div>  
    </div>
    <div class="row">                    
        <div class="panel panel-default" >

            <div class="panel-heading" >
                <h3 class="panel-title">Registrar Factura
                <div class="panel-action">
                        <a href="#" data-perform="panel-collapse">
                            <i class="ti-plus"></i>
                        </a> 
                    </div>
                </h3>
            </div>
                    <div class="panel-wrapper collapse in">

            <div class="panel-body" >
                
                <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
                     <div class="row">    
                         
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Número de Factura</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                    <?php echo CHtml::textField('nfact', '', array('class' => 'form-control', 'placeholder' => "Número de Factura")); ?>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Número de Control</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                    <?php echo CHtml::textField('ncont', '', array('class' => 'form-control', 'placeholder' => "Número de Control")); ?>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Fecha de Emisión</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <?php echo CHtml::textField('femis', '', array('class' => 'form-control', 'placeholder' => "Fecha de Emisión")); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">        
                            <div class="form-group">
                                <label>Observaciones</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                    <?php 

                                        echo CHtml::textArea('obser', '', array('class' => 'form-control', 'placeholder' => "Observaciones")); ?>

                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div><!-- form -->
        </div>  
    </div>

<div class="row">                    
        <div class="panel panel-default" >

            <div class="panel-heading" >
                <h3 class="panel-title">Almacén
                <div class="panel-action">
                        <a href="#" data-perform="panel-collapse">
                            <i class="ti-plus"></i>
                        </a> 
                    </div>
                </h3>
            </div>
                    <div class="panel-wrapper collapse in">

            <div class="panel-body" >
                <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
                    <div class="row">    
                        <div class="col-sm-12">        
                            <div class="form-group">
                                <label>Código</label>
                                <?php 
                                    $sql="SELECT a.almac_codig, a.almac_descr
                                          FROM inventario_almacen a";
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'almac_codig','almac_descr');
                                        echo CHtml::dropDownList('almac', '', $data,array('class' => 'form-control ', 'placeholder' => "Descripción", 'prompt'=>'Seleccione...')); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- form -->
        </div>  
    </div>

    <div class="row">                    
        <div class="panel panel-default" >

            <div class="panel-heading" >
                <h3 class="panel-title">Registrar Productos
                    <div class="panel-action">
                        <a href="#" data-perform="panel-collapse">
                            <i class="ti-plus"></i>
                        </a> 
                    </div>
            </h3>
                

            </div>
            <div class="panel-wrapper collapse in">

                <div class="panel-body" >
                    <input class="form-control" placeholder="Porcentaje" readonly="readonly" type="hidden" value="" name="bookindex" id="bookindex" /> 
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-11">
                                <div class="row">
                                    
                                    <div class="col-xs-3">
                                        <label>Modelo</label>
                                        <?php 
                                            $data=array();
                                            $sql="SELECT * FROM inventario_modelo";
                                            $result=$conexion->createCommand($sql)->queryAll();
                                            $data=CHtml::listData($result,'model_codig','model_descr');
                                            echo CHtml::dropDownList('model[0]', '', $data,array('class' => 'form-control ', 'placeholder' => "Banco", 'prompt'=>'Seleccione...','id'=>'model_0')); ?>
                                        
                                    </div>
                                    <div class="col-xs-3">
                                        <label>Producto</label>
                                        <?php 
                                            $data=array();
                                            
                                            echo CHtml::dropDownList('produ[0]', '', $data,array('class' => 'form-control ', 'placeholder' => "Banco", 'prompt'=>'Seleccione...','id'=>'produ_0')); ?>
                                        
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="numero1_0 form-group">
                                            <label>Archivo CSV</label>
                                            <div  class="input-group image-preview" data-placement="top" >  

                                                <img id="dynamic">
                                                <!-- image-preview-filename input [CUT FROM HERE]-->
                                                
                                                <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="true"  > <!-- don't give a name === doesn't send on POST/GET -->
                                                <span class="input-group-btn">
                                                    <!-- image-preview-clear button -->
                                                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;" >
                                                        <span class="fa fa-times"></span> Limpiar
                                                    </button>
                                                    <!-- image-preview-input -->
                                                    <div class="btn btn-default image-preview-input">
                                                        <span class="fa fa-folder-open"></span>
                                                        <span class="numero1 image-preview-input-title">Buscar</span>
                                                        <input type="file" id="image_0" name="image[0]" /> <!-- rename it -->
                                                    </div>
                                                </span>
                                           
                                            </div> 
                                        </div>   

                                    </div>
                                    <div class="col-xs-2 hide">
                                        <label>Cantidad</label>
                                        <input class="form-control" placeholder="Cantidad" prompt="Seleccione" type="text" value="" name="cprod[0]" id="cprod_0" />        
                                    </div>
                                    <div class="col-xs-2">
                                        <label>Precio Unitario</label>
                                        <input class="form-control" placeholder="Precio Unitario" prompt="Seleccione" type="text" value="" name="pprod[0]" id="pprod_0" />        
                                    </div>
                                    <div class="col-xs-2 hide">
                                        <label>Monto</label>
                                        <input class="form-control" placeholder="Monto" readonly="readonly" prompt="Seleccione"  type="text" value="" name="mprod[0]" id="mprod_0" />        
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-1">
                                <label>&nbsp;</label>
                                <br>
                                <button type="button" class="btn btn-success btn-block addButton"><i class="fa fa-plus"></i></button>
                            </div>   
                        </div>
                    </div>
                    <!-- Plantilla -->
                    <div class="form-group hide" id="bookTemplate">
                        <div class="row">
                            <div class="col-xs-11">
                                <div class="row">
                                    <div class="col-xs-3 ">
                                        <label>Modelo</label>
                                        <?php 
                                            $data=array();
                                            $sql="SELECT * FROM inventario_modelo";
                                            $result=$conexion->createCommand($sql)->queryAll();
                                            $data=CHtml::listData($result,'model_codig','model_descr');
                                            echo CHtml::dropDownList('model_', '', $data,array('class' => 'form-control', 'placeholder' => "Producto o Servicio", 'prompt'=>'Seleccione...','id'=>'model_')); ?>
                                    </div>
                                    <div class="col-xs-3 ">
                                        <label>Producto</label>
                                        <?php 
                                            $data=array();
                                            
                                            echo CHtml::dropDownList('produ_', '', $data,array('class' => 'form-control', 'placeholder' => "Producto o Servicio", 'prompt'=>'Seleccione...','id'=>'produ_')); ?>
                                    </div>
                                    <div class="col-xs-4">        
                                        <div class="numero1_">
                                            <label>Archivo CSV</label>
                                            <div  class="input-group image-preview">  
                                                <img id="dynamic">
                                                <!-- image-preview-filename input [CUT FROM HERE]-->
                                                <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                                <span class="input-group-btn">
                                                    <!-- image-preview-clear button -->
                                                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                                        <span class="fa fa-times"></span> Limpiar
                                                    </button>
                                                    <!-- image-preview-input -->
                                                    <div class="btn btn-default image-preview-input">
                                                        <span class="fa fa-folder-open"></span>
                                                        <span class="numero1_ image-preview-input-title">Buscar</span>
                                                        <input type="file" id="image_" name="image_"/> <!-- rename it -->
                                                    </div>
                                                </span>
                                           
                                            </div> 
                                        </div> 

                                    </div>
                                    <div class="col-xs-2 hide">
                                        <label>Cantidad</label>
                                        <input class="form-control" placeholder="Cantidad" type="text" value="" name="cprod_" id="cprod_" />        
                                    </div>
                                    <div class="col-xs-2">
                                        <label>Precio Unitario</label>
                                        <input class="form-control" placeholder="Precio Unitario" type="text" value="" name="pprod_" id="pprod_" />        
                                    </div>
                                    <div class="col-xs-2 hide">
                                        <label>Monto</label>
                                        <input class="form-control" placeholder="Monto" readonly="readonly" type="text" value="" name="mprod_" id="mprod_" />        
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-1">
                                <label>&nbsp;</label>
                                <br>
                                <div class="btn-group btn-group-justified">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-success addButton"><i class="fa fa-plus"></i></button>
                                    </div>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-danger removeButton"><i class="fa fa-minus"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- form -->
                <div class="panel-footer text-center">
                     <input class="form-control" placeholder="Monto Total" readonly="readonly" type="hidden" value="0,00" name="mtotal" id="mtotal" />
                    <b>Monto Total de Entrada: <span id="monto_total">0,00<span></b>
                </div>
            </div>
        </div>  
    </div>
    <div class="row">                    
        <div class="panel panel-default" >
            <div class="panel-body" >
                <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-4 ">
                            <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <button type="reset" class="btn-block btn btn-default">Limpiar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">
    var model = {
            row: '.col-xs-3',   // The title is placed inside a <div class="col-xs-4"> element
            validators: {
                notEmpty: {
                    message: 'Debe Seleccionar un Modelo'
                }
            }
        },
        seria = {
            row: '.col-xs-2',   // The title is placed inside a <div class="col-xs-4"> element
            validators: {
                notEmpty: {
                    message: 'Debe ingresar un Serial'
                }
            }
        },
        produ = {
            row: '.col-xs-3',   // The title is placed inside a <div class="col-xs-4"> element
            validators: {
                notEmpty: {
                    message: 'Debe Seleccionar un Producto'
                }
            }
        },
        cprod = {
            row: '.col-xs-3',   // The title is placed inside a <div class="col-xs-4"> element
            validators: {
                notEmpty: {
                    message: 'Debe ingresar una Cantidad'
                }
            }
        },
        pprod = {
            row: '.col-xs-2',
            validators: {
                notEmpty: {
                    message: 'Debe Ingresar un Valor'
                },
            }
        },
        mprod = {
            row: '.col-xs-2',
            validators: {
                notEmpty: {
                    message: 'Debe Ingresar un Valor'
                },
            }
        },
        image = {
            validators: {
                notEmpty: {
                    message: 'Estiamdo(a) Usuario(a) el campo "Archivo CSV" es obligatorio',
                }
            }
        },
        bookIndex = 0,
        contador = 0;
        
   </script>
<script>
    $('#monto').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#mpago').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#devol').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#flete').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#reten').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#descu').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#cprod_0').mask('#.##0',{reverse: true,maxlength:false});
    $('#pprod_0').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#mprod_0').mask('#.##0,00',{reverse: true,maxlength:false});
    //$('#nfact').mask('#.##0',{reverse: true,maxlength:false});
    //$('#ncont').mask('#.##0',{reverse: true,maxlength:false});
    $('#femis').mask('99/99/9999');
    $('#fentr').mask('99/99/9999');
    $('#fvenc').mask('99/99/9999');
    $('#fpago').mask('99/99/9999');
    $('#freci').mask('99/99/9999');

    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                denom: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Nombre o Razón Social" es obligatorio',
                        }
                    }
                },
                tclie: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Cliente" es obligatorio',
                        }
                    }
                },
                tdocu: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Documento" es obligatorio',
                        }
                    }
                },
                ndocu: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Número de Documento" es obligatorio',
                        }
                    }
                },
                ccont: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Número de Contrato" es obligatorio',
                        }
                    }
                },
                corre: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Correo Electrónico" es obligatorio',
                        },
                        regexp: {
                            regexp: /^[^@\s]+@([^@\s]+\.)+[^@\s]+$/i,
                            message: 'Estimado(a) Usuario(a) el campo "Correo Electrónico" debe posee el sguiente formato: ejemplo@smartwebtools.net'
                        }
                    }
                },
                telef: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Teléfono" es obligatorio',
                        },
                        numeric: {
                            message: 'Estimado(a) Usuario(a) el campo "Teléfono" es numerico',
                        }
                    }
                },
                direc: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Dirección" es obligatorio',
                        }
                    }
                },nfact: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Número de Factura" es obligatorio',
                        }
                    }
                },
                ncont: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Número de Control" es obligatorio',
                        }
                    }
                },
                traba: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Vendedor" es obligatorio',
                        }
                    }
                },
                clien: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Cliente" es obligatorio',
                        }
                    }
                },
                tvent: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Venta" es obligatorio',
                        }
                    }
                },
                femis: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fecha de Emisión" es obligatorio',
                        }
                    }
                },
                dcred: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Días de Credito" es obligatorio',
                        }
                    }
                },
                fentr: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fecha de Entrega" es obligatorio',
                        }
                    }
                },
                fvenc: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fecha de Vencimiento" es obligatorio',
                        }
                    }
                },
                fpago: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fecha de Pago" es obligatorio',
                        }
                    }
                },
                freci: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fecha Recibida" es obligatorio',
                        }
                    }
                },
                descu: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Descuento" es obligatorio',
                        }
                    }
                },
                pdesc: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Porcentaje de Descuento" es obligatorio',
                        }
                    }
                },
                monto: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Monto Factura" es obligatorio',
                        }
                    }
                },
                mpago: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Monto Pagado" es obligatorio',
                        }
                    }
                },
                flete: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Monto Flete" es obligatorio',
                        }
                    }
                },
                devol: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Monto Devolución" es obligatorio',
                        }
                    }
                },
                ptipo: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Pago" es obligatorio',
                        }
                    }
                },
                reten: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Retención" es obligatorio',
                        }
                    }
                },
                /*banco: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Pago" es obligatorio',
                        }
                    }
                },*/
                efact: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Retención" es obligatorio',
                        }
                    }
                },
                'model[0]': model,

                'seria[0]': seria,
                'image[0]': image,
                'produ[0]': produ,
                'cprod[0]': cprod,
                'pprod[0]': pprod,
                'mprod[0]': mprod,
                /*obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Observaciones" es obligatorio',
                        }
                    }
                },*/



            }
        });
    }).on('click', '.addButton', function() {
        //if(contador<4){ 
            contador++;
            //alert(contador);
            bookIndex++;
            document.getElementById('bookindex').value = bookIndex;
            var $template = $('#bookTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .attr('data-book-index', bookIndex)
                                .insertBefore($template);

            // Update the name attributes
            $clone
                .find('[name="model_"]').attr('name', 'model[' + bookIndex + ']').end()
                .find('[name="produ_"]').attr('name', 'produ[' + bookIndex + ']').end()
                .find('[name="cprod_"]').attr('name', 'cprod[' + bookIndex + ']').end()
                .find('[name="pprod_"]').attr('name', 'pprod[' + bookIndex + ']').end()
                .find('[name="mprod_"]').attr('name', 'mprod[' + bookIndex + ']').end()
                .find('[name="seria_"]').attr('name', 'seria[' + bookIndex + ']').end()
                .find('[name="image_"]').attr('name', 'image[' + bookIndex + ']').end()
                .find('[id="image_"]').attr('id', 'image_' + bookIndex ).end()
                .find('[class="numero1_"]').attr('class', 'numero1_' + bookIndex ).end()
                .find('[id="model_"]').attr('id', 'model_' + bookIndex ).end()
                .find('[id="produ_"]').attr('id', 'produ_' + bookIndex ).end()
                .find('[id="cprod_"]').attr('id', 'cprod_' + bookIndex ).end()
                .find('[id="pprod_"]').attr('id', 'pprod_' + bookIndex ).end()
                .find('[id="mprod_"]').attr('id', 'mprod_' + bookIndex ).end()
                .find('[id="seria_"]').attr('id', 'seria_' + bookIndex ).end()

            // Add new fields
            // Note that we also pass the validator rules for new field as the third parameter
            $('#login-form')
                .formValidation('addField', 'model[' + bookIndex + ']', model)
                .formValidation('addField', 'seria[' + bookIndex + ']', seria)
                .formValidation('addField', 'produ[' + bookIndex + ']', produ)
                .formValidation('addField', 'cprod[' + bookIndex + ']', cprod)
                .formValidation('addField', 'pprod[' + bookIndex + ']', pprod)
                .formValidation('addField', 'mprod[' + bookIndex + ']', mprod);
            $('#cprod_' + bookIndex).mask('#.##0',{reverse: true,maxlength:false});
            $('#pprod_' + bookIndex).mask('#.##0,00',{reverse: true,maxlength:false});
            $('#mprod_' + bookIndex).mask('#.##0,00',{reverse: true,maxlength:false});
            //agresgar ajax
            $('#login-form').append('<script>'+
                '$("#model_'+bookIndex+'").change(function () {'+
                    '$.ajax({'+
                        '"url": "<?php echo CController::createUrl("funciones/ComboProductos"); ?>", '+
                        '"data": {'+
                            '"modelo": model_'+bookIndex+'.value'+
                            '}, '+
                        '"type": "POST",'+
                        '"cache":false,'+
                        '"success": function (html){'+
                            'jQuery("#produ_'+bookIndex+'").html(html)'+
                        '}'+
                    '});'+
                '});'+
            '<\/script>');
            $('#login-form').append('<script>'+
                '$("#produ_'+bookIndex+'").change(function () {'+
                    '$.ajax({'+
                        '"url": "<?php echo CController::createUrl('funciones/GenerarValorProd'); ?>", '+
                            '"data": {'+
                                '"producto": produ_'+bookIndex+'.value'+
                                ',"tipo": 1'+
                            '}, '+
                            '"type": "POST",'+
                            '"dataType": "json",'+
                            '"success": function (response){'+
                                'if (response["success"] == "true") {'+
                                    'document.getElementById("cprod_'+bookIndex+'").value = response["cantidad"];'+
                                    'document.getElementById("pprod_'+bookIndex+'").value = response["precio"];'+
                                    'document.getElementById("mprod_'+bookIndex+'").value = response["monto"];'+
                                    'montoTotal();'+
                                '} else {'+
                                    'swal({ '+
                                        'title: "Error!",'+
                                        'text: response["msg"],'+
                                        'type: "error",'+
                                        'confirmButtonText: "Cerrar",'+
                                        'confirmButtonClass: "btn-danger"'+
                                    '},function(){'+
                                        '$("#guardar").removeAttr("disabled");'+
                                    '});'+
                                '}'+
                            '},error:function (response) {'+
                                '$("#wrapper").unblock();'+
                                'swal({ '+
                                    'title: "Error!",'+
                                    'text: "Error el ejecutar la operación",'+
                                    'type: "error",'+
                                    'confirmButtonText: "Cerrar",'+
                                    'confirmButtonClass: "btn-danger"'+
                                '},function(){'+
                                    '$("#guardar").removeAttr("disabled");'+
                                '});'+
                            '}'+
                        '});'+
                    '});'+'<\/script>');
            $('#login-form').append(
                '<script type="text/javascript">\n'+
                    '$(document).on("click", "#close-preview", function () {\n'+
                        '$(".numero1_'+bookIndex+' .image-preview").popover("hide");\n'+
                     
                        '$(".numero1_'+bookIndex+' .image-preview").hover(\n'+
                            'function () {\n'+
                                '$(".image-preview").popover("hide");\n'+
                            '},\n'+
                            'function () {\n'+
                                '$(".image-preview").popover("hide");\n'+
                            '}\n'+
                        ');\n'+
                    '});\n'+
                    '$(function () {\n'+
                       
                        'var closebtn = $("<button/>", {\n'+
                            'type: "button",\n'+
                            'text: "x",\n'+
                            'id: "close-preview",\n'+
                            'style: "font-size: initial;",\n'+
                        '});\n'+
                        'closebtn.attr("class","close pull-right");\n'+
                        '$(".numero1_'+bookIndex+' .image-preview").popover({\n'+
                            'trigger:"manual",\n'+
                            'html:true,\n'+
                            'title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,\n'+
                            'content: "No hay imagen",\n'+
                            'placement:"top"\n'+
                        '});\n'+
                       
                        '$(".numero1_'+bookIndex+' .image-preview-clear").click(function () {\n'+
                            '$(".numero1_'+bookIndex+' .image-preview").attr("data-content", "").popover("hide");\n'+
                            '$(".numero1_'+bookIndex+' .image-preview-filename").val("");\n'+
                            '$(".numero1_'+bookIndex+' .image-preview-clear").hide();\n'+
                            '$(".numero1_'+bookIndex+' .image-preview-input input:file").val("");\n'+
                            '$(".numero1_'+bookIndex+' .image-preview-input-title").text("Buscar");\n'+
                        '});\n'+
                       
                        '$(".numero1_'+bookIndex+' .image-preview-input input:file").change(function () {\n'+
                            'var img = $("<img/>", {\n'+
                                'id: "dynamic",\n'+
                                'width: 250,\n'+
                                'height: 200\n'+
                            '});\n'+
                            'var file = this.files[0];\n'+
                            'var reader = new FileReader();\n'+
                          
                            'reader.onload = function (e) {\n'+
                                '$(".numero1_'+bookIndex+' .image-preview-input-title").text("Cambiar");\n'+
                                '$(".numero1_'+bookIndex+' .image-preview-clear").show();\n'+
                                '$(".numero1_'+bookIndex+' .image-preview-filename").val(file.name);\n'+
                                'img.attr("src", e.target.result);\n'+
                                '$(".numero1_'+bookIndex+' .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");\n'+
                                '}\n'+
                            'reader.readAsDataURL(file);\n'+
                        '});\n'+
                    '});\n'+
                '<\/script>');
            /*$('#login-form').append('<script>'+
                '$("#cprod_'+bookIndex+'").change(function () {'+
                    '$.ajax({'+
                        '"url": "CambiarValorProd", '+
                        '"data": {'+
                            '"cantidad": cprod_'+bookIndex+'.value,'+
                            '"precio": pprod_'+bookIndex+'.value'+
                        '}, '+
                        '"type": "POST",'+
                        '"dataType": "json",'+
                        '"success": function (response){'+
                            'if (response["success"] == "true") {'+
                                    ''+
                                    'document.getElementById("mprod_'+bookIndex+'").value = response["monto"];'+
                                    'montoTotal();'+
                                '} else {'+
                                    'swal({ '+
                                        'title: "Error!",'+
                                        'text: response["msg"],'+
                                        'type: "error",'+
                                        'confirmButtonText: "Cerrar",'+
                                        'confirmButtonClass: "btn-danger"'+
                                    '},function(){'+
                                        '$("#guardar").removeAttr("disabled");'+
                                    '});'+
                                '}'+
                        '},error:function (response) {
                    $('#wrapper').unblock();'+
                            'swal({ '+
                                'title: "Error!",'+
                                'text: "Error el ejecutar la operación",'+
                                'type: "error",'+
                                'confirmButtonText: "Cerrar",'+
                                'confirmButtonClass: "btn-danger"'+
                            '},function(){'+
                                '$("#guardar").removeAttr("disabled");'+
                            '});'+
                                ''+
                        '}'+
                    '});'+
                '});'+
            '<\/script>');
            */
            $('#login-form').append('<script>'+
                '$("#pprod_'+bookIndex+'").change(function () {'+
                    'CambiarValorProd_'+bookIndex+'();'+
                '});'+
                '$("#cprod_'+bookIndex+'").change(function () {'+
                    'CambiarValorProd_'+bookIndex+'();'+
                '});'+
            '<\/script>');

            $('#login-form').append('<script>'+
                'function CambiarValorProd_'+bookIndex+'(){'+
                    '$.ajax({'+
                        '"url": "<?php echo CController::createUrl('funciones/CambiarValorProd'); ?>", '+
                        '"data": {'+
                            '"cantidad": cprod_'+bookIndex+'.value,'+
                            '"precio": pprod_'+bookIndex+'.value'+
                        '}, '+
                        '"type": "POST",'+
                        '"dataType": "json",'+
                        '"success": function (response){'+
                            'if (response["success"] == "true") {'+
                                    ''+
                                    'document.getElementById("mprod_'+bookIndex+'").value = response["monto"];'+
                                    'montoTotal();'+
                                '} else {'+
                                    'swal({ '+
                                        'title: "Error!",'+
                                        'text: response["msg"],'+
                                        'type: "error",'+
                                        'confirmButtonText: "Cerrar",'+
                                        'confirmButtonClass: "btn-danger"'+
                                    '},function(){'+
                                        '$("#guardar").removeAttr("disabled");'+
                                    '});'+
                                '}'+
                        '},error:function (response) {'+
                            '$("#wrapper").unblock();'+
                            'swal({ '+
                                'title: "Error!",'+
                                'text: "Error el ejecutar la operación",'+
                                'type: "error",'+
                                'confirmButtonText: "Cerrar",'+
                                'confirmButtonClass: "btn-danger"'+
                            '},function(){'+
                                '$("#guardar").removeAttr("disabled");'+
                            '});'+
                                ''+
                        '}'+
                    '});'+
                '}'+
            '<\/script>');
            /*$('#login-form').append('<script>'+
                '$("#produ_'+bookIndex+'").select2();'+
            '<\/script>');*/
           

        /*}else{
            bootbox.alert('No se pueden agregar más de 5 Producctos');
        }*/
    }).on('click', '.removeButton', function() {// Remove button click handler
        //alert(contador);
        contador=contador-1;
        var $row  = $(this).parents('.form-group'),
            index = $row.attr('data-book-index');
        // Remove fields
        $('#login-form')
            .formValidation('removeField', $row.find('[name="accsesorios[' + index + ']"]'))
            .formValidation('removeField', $row.find('[name="valoracc[' + index + ']"]'))
            .formValidation('removeField', $row.find('[name="valoraccmin[' + index + ']"]'));
        // Remove element containing the fields
        $row.remove();
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        var data = new FormData(jQuery('form')[0]);
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                url: 'registrarLote',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>
<script>
// Date Picker
    
    jQuery('#femis').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'

      });
    jQuery('#fentr').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'

      });
    jQuery('#freci').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'

      });

    jQuery('#fvenc').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'

      });
    jQuery('#fpago').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'

      });
      
</script>
<script>

    
    $(function () {
       



        
        // Clear event
        $('.numero1_0 .image-preview-clear').click(function () {
            $('.numero1_0 .image-preview').attr("data-content", "").popover('hide');
            $('.numero1_0 .image-preview-filename').val("");
            $('.numero1_0 .image-preview-clear').hide();
            $('.numero1_0 .image-preview-input input:file').val("");
            $(".numero1_0 .image-preview-input-title").text("Buscar");
        });
        // Create the preview image
        $(".numero1_0 .image-preview-input input:file").change(function () {
            var img = $('<img/>', {
                id: 'dynamic',
                width: 250,
                height: 200
            });
            var file = this.files[0];
            var reader = new FileReader();
            // Set preview image into the popover data-content
            reader.onload = function (e) {
                $(".numero1_0 .image-preview-input-title").text("Cambiar");
                $(".numero1_0 .image-preview-clear").show();
                $(".numero1_0 .image-preview-filename").val(file.name);
                img.attr('src', e.target.result);
                }
            reader.readAsDataURL(file);
        });
    });
    </script>
<script>
    $("#model_0").change(function () {
        $.ajax({
            "url": "<?php echo CController::createUrl('funciones/ComboProductos'); ?>", 
            "data": {
                "modelo": model_0.value
            }, 
            "type": "POST",

            'cache':false,
            "success": function (html){
                jQuery("#produ_0").html(html)
            }
        });
    });
    $("#produ_0").change(function () {
        $.ajax({
            "url": "<?php echo CController::createUrl('funciones/GenerarValorProd'); ?>", 
            "data": {
                "producto": produ_0.value,
                "tipo": 1
            }, 
            "type": "POST",
            "dataType": "json",
            "success": function (response){
                if (response['success'] == 'true') {
                        document.getElementById("cprod_0").value = response["cantidad"];
                        document.getElementById("pprod_0").value = response["precio"];
                        document.getElementById("mprod_0").value = response["monto"];
                        montoTotal();
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }
            },error:function (response) {
                    $('#wrapper').unblock();
                swal({ 
                    title: "Error!",
                    text: "Error el ejecutar la operación",
                    type: "error",
                    confirmButtonText: "Cerrar",
                    confirmButtonClass: "btn-danger"

                },function(){
                    $("#guardar").removeAttr('disabled');
                });
                    
            }
        });
    });
</script>
<script>
    $("#pprod_0").change(function () {
        CambiarValorProd_0();
    });
    $("#cprod_0").change(function () {
        CambiarValorProd_0();
    });
</script>
<script>
    function CambiarValorProd_0(){
        $.ajax({
            "url": "<?php echo CController::createUrl('funciones/CambiarValorProd'); ?>", 
            "data": {
                "cantidad": cprod_0.value,
                "precio": pprod_0.value
            }, 
            "type": "POST",
            "dataType": "json",
            "success": function (response){
                if (response['success'] == 'true') {
                        
                        document.getElementById("mprod_0").value = response["monto"];
                        montoTotal();
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }
            },error:function (response) {
                    $('#wrapper').unblock();
                swal({ 
                    title: "Error!",
                    text: "Error el ejecutar la operación",
                    type: "error",
                    confirmButtonText: "Cerrar",
                    confirmButtonClass: "btn-danger"

                },function(){
                    $("#guardar").removeAttr('disabled');
                });
                    
            }
        });
    }
</script>
<script>

    function montoTotal(){
         $.ajax({
            "url": "<?php echo CController::createUrl('funciones/GenerarMontoTotal'); ?>", 
            "data": $('#login-form').serialize(),
            "type": "POST",
            "dataType": "json",
            "success": function (response){
                if (response['success'] == 'true') {
                        $("#monto_total").text(response["monto"]);
                        document.getElementById("mtotal").value = response["monto"];
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }
            },error:function (response) {
                    $('#wrapper').unblock();
                swal({ 
                    title: "Error!",
                    text: "Error el ejecutar la operación",
                    type: "error",
                    confirmButtonText: "Cerrar",
                    confirmButtonClass: "btn-danger"

                },function(){
                    $("#guardar").removeAttr('disabled');
                });
                    
            }
        });
    }

</script>
<script type="text/javascript">
    function ListarProveedor(response){
        if (response['success'] == 'true') {
            document.getElementById("denom").value = response["denom"];
            document.getElementById("corre").value = response["corre"];
            document.getElementById("telef").value = response["telef"];
            document.getElementById("direc").value = response["direc"];
            document.getElementById("clien").value = response["clien"];


        } else {
            swal({ 
                title: "Error!",
                text: response['msg'],
                type: "error",
                confirmButtonText: "Cerrar",
                confirmButtonClass: "btn-danger"
            },function(){
                $("#guardar").removeAttr('disabled');
            });
        }
    }
</script>