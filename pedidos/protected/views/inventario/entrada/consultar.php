<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Inventario</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Inventario</a></li>
            <li>Entrada</a></li>
            <li class="active">Consultar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<form id='login-form' name='login-form' method="post">
    <div class="row">                    
        <div class="panel panel-default" >

            <div class="panel-heading" >
                <h3 class="panel-title">Proveedor<div class="panel-action">
                        <a href="#" data-perform="panel-collapse">
                            <i class="ti-plus"></i>
                        </a> 
                    </div>
                </h3>
            </div>
        <div class="panel-wrapper collapse in">
            <div class="panel-body" >
                <?php
                    $conexion=Yii::app()->db;
                    
                ?>
                    <div class="row hide">
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Proveedor</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <?php 
                                        echo CHtml::hiddenField('prove', $inventario_proveedor['prove_codig'],array('class' => 'form-control', 'placeholder' => "Descripción", 'prompt'=>'Seleccione...', 'disabled'=>'true')); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Tipo de Proveedor</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-list"></i></span>
                                    <?php 
                                        $sql="SELECT * FROM p_cliente_tipo";
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'tclie_codig','tclie_descr');
                                        echo CHtml::dropDownList('tclie', $inventario_proveedor['tclie_codig'], $data,array(
                                            'ajax' => array(
                                                'type' => 'POST',
                                                'url' => CController::createUrl('funciones/TipoDocumento'), // Controlador que devuelve las p_ciudades relacionadas
                                                'update' => '#tdocu', // id del item que se actualizará
                                            ),'class' => 'form-control', 'placeholder' => "Descripción", 'prompt'=>'Seleccione...', 'disabled'=>'true')); ?>

                                </div>
                            </div>
                        </div>
                                        
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Documento de Identidad</label>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <?php 
                                        $sql="SELECT a.* FROM p_documento_tipo a 
                                        JOIN p_tipo_cliente_documento b ON (a.tdocu_codig=b.tdocu_codig) 
                                        WHERE b.tclie_codig='".$inventario_proveedor['tclie_codig']."'";
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'tdocu_codig','tdocu_descr');
                                        echo CHtml::dropDownList('tdocu', $inventario_proveedor['tdocu_codig'], $data,array('ajax' => array(
                                                'type' => 'POST',
                                                'url' => CController::createUrl('funciones/ListarProveedor'),
                                                'dataType' => 'json',
                                                'success' => 'ListarProveedor',
                                            ),'class' => 'form-control', 'placeholder' => "Descripción", 'prompt'=>'Seleccione...', 'disabled'=>'true')); ?>
                                    </div>
                                    <div class="col-sm-8">
                                        <?php echo CHtml::textField('ndocu', $inventario_proveedor['prove_ndocu'], array('ajax' => array(
                                                'type' => 'POST',
                                                'url' => CController::createUrl('funciones/ListarProveedor'),
                                                'dataType' => 'json',
                                                'success' => 'ListarProveedor',
                                            ),'class' => 'form-control', 'placeholder' => "Número de Documento", 'disabled'=>'true')); ?>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Nombre o Razón Social</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                    <?php echo CHtml::textField('denom', $inventario_proveedor['prove_denom'], array('class' => 'form-control', 'placeholder' => "Nombre o Razón Social", 'disabled'=>'true')); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">        
                        <div class="col-sm-6">        
                            <div class="form-group">
                                <label>Correo Electrónico</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    <?php 
                                        echo CHtml::textField('corre', $inventario_proveedor['prove_corre'], array('class' => 'form-control', 'placeholder' => "Correo Electrónico", 'disabled'=>'true')); ?>
                                </div>
                            </div>
                        </div>
                    
                    
                        <div class="col-sm-6">        
                            <div class="form-group">
                                <label>Teléfono</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-archive"></i></span>
                                    <?php echo CHtml::textField('telef', $inventario_proveedor['prove_telef'], array('class' => 'form-control', 'placeholder' => "Teléfono", 'disabled'=>'true')); ?>

                                </div>
                            </div>
                        </div>
                        
                    
                    </div>
                    <div class="row">
                        <div class="col-sm-12">        
                            <div class="form-group">
                                <label>Dirección</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-home"></i></span>
                                    <?php 
                                        echo CHtml::textArea('direc', $inventario_proveedor['prove_direc'], array('class' => 'form-control', 'placeholder' => "Dirección", 'disabled'=>'true')); ?>

                                </div>
                            </div>
                        </div>
                        
                    </div>
            </div><!-- form -->
        </div> 
        </div>  
    </div>
    <div class="row">                    
        <div class="panel panel-default" >

            <div class="panel-heading" >
                <h3 class="panel-title">Factura
                <div class="panel-action">
                        <a href="#" data-perform="panel-collapse">
                            <i class="ti-plus"></i>
                        </a> 
                    </div>
                </h3>
            </div>
                    <div class="panel-wrapper collapse in">

            <div class="panel-body" >
                <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
                    <div class="row hide">    
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Código</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <?php 
                                       echo CHtml::hiddenField('codig', $entrada['ientr_codig'],array('class' => 'form-control', 'placeholder' => "Descripción", 'prompt'=>'Seleccione...', 'disabled'=>'true')); 
                                    ?>
                                </div>
                            </div>
                        </div>  
                       
                    </div>
                     <div class="row">    
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Número de Factura</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                    <?php echo CHtml::textField('nfact', $entrada['ientr_nfact'], array('class' => 'form-control', 'placeholder' => "Número de Factura", 'disabled'=>'true')); ?>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Número de Control</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                    <?php echo CHtml::textField('ncont', $entrada['ientr_ncont'], array('class' => 'form-control', 'placeholder' => "Número de Control", 'disabled'=>'true')); ?>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Fecha de Emisión</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <?php echo CHtml::textField('femis', $this->funciones->TransformarFecha_v($entrada['ientr_femis']), array('class' => 'form-control', 'placeholder' => "Fecha de Emisión", 'disabled'=>'true')); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                   
                    <div class="row">  
                        
                        <div class="col-sm-12">        
                            <div class="form-group">
                                <label>Estatus Entrada</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-list"></i></span>                              
                                    <?php $sql="SELECT * FROM p_inventario_entrada_estatus";
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'ieest_codig','ieest_descr');
                                        echo CHtml::dropDownList('ieent', $entrada['ieest_codig'], $data,array('class' => 'form-control', 'placeholder' => "Estatus Factura", 'prompt'=>'Seleccione...', 'disabled'=>'true')); ?>

                                </div>
                            </div>
                        </div>
                          
                        
                        
                    </div>
                    <div class="row">
                        <div class="col-sm-12">        
                            <div class="form-group">
                                <label>Observaciones</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                    <?php 

                                        echo CHtml::textArea('obser', $entrada['ientr_obser'], array('class' => 'form-control', 'placeholder' => "Observaciones", 'disabled'=>'true')); ?>

                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div><!-- form -->
        </div>  
    </div>
    <div class="row">                    
        <div class="panel panel-default" >

            <div class="panel-heading" >
                <h3 class="panel-title">Almacén
                <div class="panel-action">
                        <a href="#" data-perform="panel-collapse">
                            <i class="ti-plus"></i>
                        </a> 
                    </div>
                </h3>
            </div>
                    <div class="panel-wrapper collapse in">

            <div class="panel-body" >
                <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
                    <div class="row">    
                        <div class="col-sm-12">        
                            <div class="form-group">
                                <label>Código</label>
                                <?php 
                                    $sql="SELECT a.almac_codig, a.almac_descr
                                          FROM inventario_almacen a";
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'almac_codig','almac_descr');
                                        echo CHtml::dropDownList('almac', $entrada['almac_codig'], $data,array('class' => 'form-control ', 'placeholder' => "Descripción", 'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- form -->
        </div>  
    </div>
    
    <div class="row">                    
        <div class="panel panel-default" >

            <div class="panel-heading" >
                <h3 class="panel-title">Productos y Servicios 
                    <div class="panel-action">
                        <a href="#" data-perform="panel-collapse">
                            <i class="ti-plus"></i>
                        </a> 
                    </div>
            </h3>
                

            </div>
            <div class="panel-wrapper collapse in">

                <div class="panel-body" >
                    <div class="col-sm-12 table-responsive" id='listado-p_persona'>
                        <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                            <thead>
                                <tr>
                                    <th width="2%">
                                        #
                                    </th>
                                    <th>
                                        Modelo
                                    </th>
                                    <th>
                                        Producto
                                    </th>
                                    <th>
                                        Serial
                                    </th>
                                    <th>
                                        Precio Unitario
                                    </th>
                                    <th>
                                        Cantidad
                                    </th>
                                    <th>
                                        Monto
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                
                                foreach ($productos as $key => $producto) {
                                
                                    $total['canti']+=$producto['iepro_canti'];
                                    $total['preci']+=$producto['iepro_preci'];
                                    $total['monto']+=$producto['iepro_monto'];
                                    $i++;
                                ?>
                                <tr>
                                    <td class="tabla"><?php echo $i ?></td>
                                    <td class="tabla"><?php echo $producto['model_descr'] ?></td>
                                    <td class="tabla"><?php echo $producto['inven_descr'] ?></td>
                                    <td class="tabla"><?php echo $producto['inven_seria'] ?></td>
                                    <td class="tabla"><?php echo $this->funciones->TransformarMonto_v($producto['iepro_preci'],2) ?></td>
                                    <td class="tabla"><?php echo $this->funciones->TransformarMonto_v($producto['iepro_canti'],0) ?></td>
                                    

                                    <td class="tabla"><?php echo $this->funciones->TransformarMonto_v($producto['iepro_monto'],2) ?></td>
                                </tr>
                                <?php
                                    }   
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th class="tabla" colspan="4">Total</th>
                                    <th class="tabla"><?php echo $this->funciones->TransformarMonto_v($total['preci'],2); ?></th>
                                    <th class="tabla"><?php echo $this->funciones->TransformarMonto_v($total['canti'],0); ?></th>
                                    
                                    <th class="tabla"><?php echo $this->funciones->TransformarMonto_v($total['monto'],2); ?></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div><!-- form -->
            </div>
        </div>  
    </div>
    <div class="row">                    
        <div class="panel panel-default" >
            <div class="panel-body" >
                <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-12 ">
                            <a href="listado" type="reset" class="btn-block btn btn-info">Volver </a>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</form>

<script>
    
    $(document).ready(function () {

        $('#auditoria').DataTable();
    });
</script>
<script>
$('#punid').mask('#.##0,00',{reverse: true,maxlength:false});
$('#desde').mask('00/00/0000');
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                descr: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Descripción" es obligatorio',
                        }
                    }
                },
                codig: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Código" es obligatorio',
                        }
                    }
                },
                punid: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Precio por Unidad" es obligatorio',
                        }
                    }
                },
                canti: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "cantidad" es obligatorio',
                        }
                    }
                },
                tunid: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Unidad" es obligatorio',
                        }
                    }
                },
                moned: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Moneda" es obligatorio',
                        }
                    }
                },
                /*obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Observaciones" es obligatorio',
                        }
                    }
                },*/



            }
        });
    });

</script>