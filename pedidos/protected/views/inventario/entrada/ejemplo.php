<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Inventario</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Inventario</a></li>
            <li>Entrada</a></li>
            <li class="active">Ejemplo</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Criterio de Busqueda</h3>
    </div>
    <div class="panel-body" >
        <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'p_personas', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            $connection = Yii::app()->db;

        ?>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Desde</label>
                    <input type="text" class="form-control" name="desde" id="desde" placeholder="Desde">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Hasta</label>
                    <input type="text" class="form-control" name="hasta" id="hasta" placeholder="Hasta">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Estatus</label>
                    <select class="form-control" name="estatu" id="estatu">
                        <option value="">Seleccione...</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Proveedor</label>
                    <select class="form-control" name="estatu" id="estatu">
                        <option value="">Seleccione...</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Factura</label>
                    <input type="text" class="form-control" name="factura" id="factura" placeholder="Factura">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Código</label>
                    <input type="text" class="form-control" name="codigo" id="codigo" placeholder="Código">
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-sm-4">
                <button type="button" id="buscar" class="btn btn-block btn-info">Buscar</button>
            </div>
            <div class="col-sm-4">
                <button type="reset" class="btn btn-block btn-default">Limpiar</button>
            </div>
            <div class="col-sm-4">
                <a href="registrar" class="btn btn-block btn-info" >Nuevo</a>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div> 

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Listado</h3>
    </div>
    <div class="panel-body" >
        
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-p_persona'>
                <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                #
                            </th>
                            <th width="2%">
                                Código
                            </th>
                            <th>
                                Factura
                            </th>
                            <th>
                                Proveedor
                            </th>
                            <th>
                                Cantidad
                            </th>
                            <th>
                                Monto
                            </th>
                            <th>
                                Fecha
                            </th>
                            
                            <th>
                                Estatus
                            </th>
                            <th width="5%">
                                Consultar
                            </th>
                            <th width="5%">
                                Modificar
                            </th>
                            <th width="5%">
                                Eliminar
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "SELECT *,
                                (SELECT count(*) 
                                    FROM inventario_entrada_producto c
                                    WHERE c.ientr_codig=a.ientr_codig) cantidad 
                                FROM inventario_entrada a
                                JOIN inventario_proveedor b on (a.prove_codig = b.prove_codig)
                                JOIN p_inventario_entrada_estatus d ON (a.ieest_codig=d.ieest_codig)";
                        
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                            $disabled='';
                            if($row["ieest_codig"]<>'1'){
                                $disabled="disabled='true'";
                            }
                        ?>
                        <tr>
                            <td class="tabla"><?php echo $i ?></td>
                            <td class="tabla"><?php echo $row['ientr_codig'] ?></td>
                            <td class="tabla"><?php echo $row['ientr_nfact'] ?></td>
                            <td class="tabla"><?php echo $row['prove_denom'] ?></td>
                            <td class="tabla"><?php echo $this->TransformarMonto_v($row['cantidad'],0) ?></td>
                            <td class="tabla"><?php echo $this->TransformarMonto_v($row['ientr_monto'],2) ?></td>
                            <td class="tabla"><?php echo $this->TransformarFecha_v($row['ientr_femis']) ?></td>
                            <td class="tabla"><?php echo $row['ieest_descr'] ?></td>
                            <td class="tabla"><a href="consultar?c=<?php echo $row['ientr_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-search"></i></a></td>
                            <?php 
                                if($row["ieest_codig"]<>'1') {
                                    ?>
                                        <td class="tabla"><a href="#" class="btn btn-block btn-info disabled"><i class="fa fa-pencil"></i></a></td>
                                        <td class="tabla"><a href="#" class="btn btn-block btn-info disabled"><i class="fa fa-close"></i></a></td>        
                                    <?php
                                }else{
                                    ?>
                                        <td class="tabla"><a href="modificar?c=<?php echo $row['ientr_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-pencil"></i></a></td>
                                        <td class="tabla"><a href="eliminar?c=<?php echo $row['ientr_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-close"></i></a></td>        
                                    <?php
                                }
                            ?>
                        </tr>
                        <?php
                            }   
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <a href="AprobarEntradas" class="btn btn-block btn-info" >Aprobar</a>
                </div>
                <div class="col-sm-3">
                    <a href="ReversarAprobarEntradas" class="btn btn-block btn-info" >Reversar Aprobación</a>
                </div>
                <div class="col-sm-3">
                    <a href="AnularEntradas" class="btn btn-block btn-info" >Anulación</a>
                </div>
                <div class="col-sm-3">
                    <a href="ReversarAnularEntradas" class="btn btn-block btn-info" >Reversar Anulación</a>
                </div>
            </div>
        </div>
    </div>
</div>  

<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#buscar').click(function () {
        var formData = new FormData($("#p_personas")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-p_persona').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>
<script>
    $(document).ready(function () {
        $('#cantidad').mask('#.##0',{reverse: true,maxlength:false});
        $('#punidad').mask('#.##0,00',{reverse: true,maxlength:false});
    });
</script>

<script>
// Date Picker
    $(document).ready(function () {
        $('#desde').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd/mm/yyyy'
        });
        $('#hasta').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd/mm/yyyy'
        });
    }); 
</script>