<?php
    $conexion=Yii::app()->db;
?><div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Inventario</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Inventario</a></li>
            <li><a href="#">Almacén</a></li>
            <li class="active">Registrar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Registrar Almacén</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                
                <div class="row">          
                   <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Código</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                <?php echo CHtml::textField('numer', '', array('class' => 'form-control', 'placeholder' => "Código")); ?>

                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Descripción</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php echo CHtml::textField('descr', '', array('class' => 'form-control', 'placeholder' => "Descripción")); ?>

                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">    
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Ubicación</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-map"></i></span>
                                <?php echo CHtml::textField('ubica', '', array('class' => 'form-control', 'placeholder' => "Ubicación")); ?>

                            </div>
                        </div>
                    </div>                    
                
  
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Responsable</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php 
                                    $sql="SELECT usuar_codig, concat(perso_pnomb,' ',perso_papel) as nombre
                                          FROM seguridad_usuarios a
                                          JOIN p_persona b ON (a.perso_codig = b.perso_codig)";
                                    $result=$conexion->createCommand($sql)->queryAll();
                                    $data=CHtml::listData($result,'usuar_codig','nombre');
                                echo CHtml::dropDownList('respo', '', $data, array('class' => 'form-control', 'placeholder' => "Descripción")); ?>

                            </div>
                        </div>
                    </div>                    
                
                </div>

                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observaciones</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <?php 

                                    echo CHtml::textArea('obser', '', array('class' => 'form-control', 'placeholder' => "Observaciones")); ?>

                            </div>
                        </div>
                    </div>
                    
                </div>
                
                <!-- Button -->
                <div class="row controls">
                    <div class="col-sm-4 ">
                        <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                    </div>
                    <div class="col-sm-4 ">
                        <button type="reset" class="btn-block btn btn-default">Limpiar  </button>
                    </div>
                    <div class="col-sm-4 ">
                        <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                    </div>
                </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
<script>
$('#punid').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#canti').mask('#.##0',{reverse: true,maxlength:false});
        $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                descr: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Descripción" es obligatorio',
                        }
                    }
                },
                numer: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Código" es obligatorio',
                        }
                    }
                },
                ubica: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Ubicación" es obligatorio',
                        }
                    }
                },
                respo: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Responsable" es obligatorio',
                        }
                    }
                },
                /*obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Observaciones" es obligatorio',
                        }
                    }
                },*/



            }
        });
    });

    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'registrar',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>