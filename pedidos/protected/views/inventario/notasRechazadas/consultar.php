<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Inventario</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Inventario</a></li>
            <li><a href="#">Almacén</a></li>
            <li><a href="#">Nota de Entrega Rechazadas</a></li>
            <li class="active">Consultar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<form id='login-form' name='login-form' method="post">
    <div class="row">                    
        <div class="panel panel-default" >

            <div class="panel-heading" >
                <h3 class="panel-title">Notas de Entrega Rechazadas
                <div class="panel-action">
                        <a href="#" data-perform="panel-collapse">
                            <i class="ti-plus"></i>
                        </a> 
                    </div>
                </h3>
            </div>
                    <div class="panel-wrapper collapse in">
            <?php
                    $conexion=Yii::app()->db;
                ?>
            <div class="panel-body" >
                <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
                    
<div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>N° Nota de Entrega</label>
                            <input type="text" class="form-control" name="codigo" id="codigo" placeholder="Código">
                        </div>
                    </div>
                    
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Fecha de Emisión</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <?php echo CHtml::textField('femis', '', array('class' => 'form-control', 'placeholder' => "Fecha de Emisión")); ?>

                            </div>
                        </div>
                    </div>
                      <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Fecha de Entrega</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <?php echo CHtml::textField('femis', '', array('class' => 'form-control', 'placeholder' => "Fecha de Emisión")); ?>

                            </div>
                        </div>
                    </div>
                </div>
                    <div class="row">
                        <div class="col-sm-12">        
                            <div class="col-sm-4">
                        <div class="form-group">
                            <label>Cliente</label>
                            <input type="text" class="form-control" name="codigo" id="codigo" placeholder="Código">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>R.I.F</label>
                            <input type="text" class="form-control" name="codigo" id="codigo" placeholder="Código">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>N.I.T</label>
                            <input type="text" class="form-control" name="codigo" id="codigo" placeholder="Código">
                        </div>
                    </div>
                        </div>
                        
                    </div>

                                        <div class="row">
                        <div class="col-sm-12">        
                            <div class="col-sm-4">
                        <div class="form-group">
                            <label>Dirección</label>
                            <input type="text" class="form-control" name="codigo" id="codigo" placeholder="Código">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Teléfono</label>
                            <input type="text" class="form-control" name="codigo" id="codigo" placeholder="Código">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Fax</label>
                            <input type="text" class="form-control" name="codigo" id="codigo" placeholder="Código">
                        </div>
                    </div>
                        </div>
                        
                    </div>
                                        <div class="row">
                        <div class="col-sm-12">        
                            <div class="col-sm-4">
                        <div class="form-group">
                            <label>Transporte</label>
                            <input type="text" class="form-control" name="codigo" id="codigo" placeholder="Código">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Condición de Pago</label>
                            <input type="text" class="form-control" name="codigo" id="codigo" placeholder="Código">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Vendedor</label>
                            <input type="text" class="form-control" name="codigo" id="codigo" placeholder="Código">
                        </div>
                    </div>
                        </div>
                        
                    </div>
                </div>
            </div><!-- form -->
        </div>  
    </div>

    <div class="row">                    
        <div class="panel panel-default" >

            <div class="panel-heading" >
                <h3 class="panel-title">Productos y Servicios
                  <div class="panel-action">
                        <a href="#" data-perform="panel-collapse">
                            <i class="ti-plus"></i>
                        </a> 
                    </div>
            </h3>
                

            </div>
            <div class="panel-wrapper collapse in">

                <div class="panel-body" >
                    <div class="col-sm-12 table-responsive" id='listado-p_persona'>
                        <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                            <thead>
                                <tr>
                                    <th width="2%">
                                        #
                                    </th>
                                    <th>
                                        Modelo del POS
                                    </th>
                                    <th>
                                        Serial del POS
                                    </th>
                                    <th>
                                        Doc. Origen
                                    </th>
                                    <th>
                                        N° Doc.
                                    </th>
                                    <th>
                                        Almacén
                                    </th>
                                    <th>
                                        Cantidad
                                    </th>
                                    <th>
                                        Unidad
                                    </th>
                                    <th>
                                        neto
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                
                                foreach ($productos as $key => $producto) {
                                
                                    $total['canti']+=$producto['ispro_canti'];
                                    $total['preci']+=$producto['ispro_preci'];
                                    $total['monto']+=$producto['ispro_monto'];
                                    $i++;
                                ?>
                                <tr>
                                    <td class="tabla"><?php echo $i ?></td>
                                    <td class="tabla"><?php echo $producto['inven_descr'] ?></td>
                                    <td class="tabla"><?php echo $this->funciones->TransformarMonto_v($producto['ispro_preci'],2) ?></td>
                                    <td class="tabla"><?php echo $this->funciones->TransformarMonto_v($producto['ispro_canti'],0) ?></td>
                                    

                                    <td class="tabla"><?php echo $this->funciones->TransformarMonto_v($producto['ispro_monto'],2) ?></td>
                                </tr>
                                <?php
                                    }   
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th class="tabla" colspan="2">Total</th>
                                    <th class="tabla"><?php echo $this->funciones->TransformarMonto_v($total['preci'],2); ?></th>
                                    <th class="tabla"><?php echo $this->funciones->TransformarMonto_v($total['canti'],0); ?></th>
                                    
                                    <th class="tabla"><?php echo $this->funciones->TransformarMonto_v($total['monto'],2); ?></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div><!-- form -->


            </div>
        </div>  
    </div>
    <div class="row">                    
        <div class="panel panel-default" >
              <h3 class="panel-title">Detalle del Rechazo</h3>
            <div class="panel-body" >

                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Motivo</label>
                                <?php 
                                    $sql="SELECT *
                                          FROM p_motivo a
                                          ORDER BY 2";

                                    $result=$conexion->createCommand($sql)->queryAll();

                                    $data=CHtml::listData($result,'motiv_codig','motiv_descr');
                                    echo CHtml::dropDownList('motiv', '', $data, array('class' => 'form-control', 'prompt' => "Seleccione")); ?>

                        </div>
                    </div>
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observación</label>
                                <?php echo CHtml::textArea('obser','', array('class' => 'form-control', 'placeholder' => "Observación")); ?>

                        </div>
                    </div>
                

                <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-12 ">
                            <a href="listado" type="reset" class="btn-block btn btn-info">Volver </a>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</form>
<script>
$('#punid').mask('#.##0,00',{reverse: true,maxlength:false});
$('#desde').mask('00/00/0000');
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                descr: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Descripción" es obligatorio',
                        }
                    }
                },
                codig: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Código" es obligatorio',
                        }
                    }
                },
                punid: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Precio por Unidad" es obligatorio',
                        }
                    }
                },
                canti: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "cantidad" es obligatorio',
                        }
                    }
                },
                tunid: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Unidad" es obligatorio',
                        }
                    }
                },
                moned: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Moneda" es obligatorio',
                        }
                    }
                },
                /*obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Observaciones" es obligatorio',
                        }
                    }
                },*/



            }
        });
    });

</script>