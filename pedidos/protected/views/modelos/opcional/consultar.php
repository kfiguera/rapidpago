<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Opcional</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Modelos</a></li>
            <li><a href="#">Opcional</a></li>
            <li class="active">Consultar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Consultar Opcional</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row hide">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Código</label>
                            
                                <?php 
                                echo CHtml::textField('codig', $opcional['mopci_codig'], array('class' => 'form-control', 'placeholder' => "Código", 'disabled'=>'true')); ?>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Descripción</label>
                                <?php 
                                    echo CHtml::textField('descr', $opcional['mopci_descr'], array('class' => 'form-control', 'placeholder' => "Descripción", 'disabled'=>'true')); 
                                ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Precio</label>
                                <?php 
                                    echo CHtml::textField('preci', $this->funciones->TransformarMonto_v($opcional['mopci_preci'],2), array('class' => 'form-control', 'placeholder' => "Precio", 'disabled'=>'true')); 
                                ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>¿Que incluye?</label>
                                <?php 
                                $sql="SELECT inclu_codig, inclu_descr
                                      FROM pedido_incluye
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'inclu_codig','inclu_descr');
                                    echo CHtml::dropDownList('inclu', json_decode($opcional['mopci_inclu']), $data ,array('class' => 'select2 select2-multiple', 'placeholder' => "Descripción", 'multiple'=>'multiple', 'disabled'=>'true')); 
                                ?>
                        </div>
                    </div>
                </div>
                
                
                    <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-12">
                            <a href="listado" type="reset" class="btn-block btn btn-info">Volver </a>
                        </div>
                    </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
