<style>
    .image-preview-input {
        position: relative;
        overflow: hidden;
        margin: 0px;    
        color: #333;
        background-color: #fff;
        border-color: #ccc;    
    }
    .image-preview-input input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }
    .image-preview-input-title {
        margin-left:2px;
    }
</style> 
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Parametros</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Parametros</a></li>
            <li><a href="#">Emoji</a></li>
            <li class="active">Modificar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Registrar Emoji</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row hide">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Código</label>
                            
                                <?php 
                                echo CHtml::textField('codig', $emoji['emoji_codig'], array('class' => 'form-control', 'placeholder' => "Código")); ?>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Imagen</label>
                            <br>
                            <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$emoji['emoji_ruta'] ?>" class='img-responsive img-thumbnail'>
                        </div>
                    </div>
                    <div class="col-sm-8">        
                        <div class="row">
                            <div class="col-sm-12"> 
                                <div class="form-group">
                                    <label>Descripción</label>
                                        <?php 
                                            echo CHtml::textField('descr', $emoji['emoji_descr'], array('class' => 'form-control', 'placeholder' => "Descripción")); 
                                        ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">        
                                <div class="numero1 form-group">
                                    <label>Imagen Libre</label>
                                    <div  class="input-group image-preview">  
                                        <img id="dynamic">
                                        <!-- image-preview-filename input [CUT FROM HERE]-->
                                        <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                        <span class="input-group-btn">
                                            <!-- image-preview-clear button -->
                                            <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                                <span class="fa fa-times"></span> Limpiar
                                            </button>
                                            <!-- image-preview-input -->
                                            <div class="btn btn-default image-preview-input">
                                                <span class="fa fa-folder-open"></span>
                                                <span class="numero1 image-preview-input-title">Buscar</span>
                                                <input type="file" id="image" name="image"/> <!-- rename it -->
                                            </div>
                                        </span>
                                   
                                    </div> 
                                </div> 

                            </div>
                        </div>
                    </div>
                </div>
                    <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-4 ">
                            <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                        </div>
                    </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
<script>
    $(document).ready(function () { 
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                
                urole: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Rol de Usuario" es obligatorio',
                        }
                    }
                },modul: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Modulos" es obligatorio',
                        }
                    }
                }
                ,estat: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Estatus" es obligatorio',
                        }
                    }
                }


            }
        });
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        var data = new FormData(jQuery('form')[0]);

        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                url: 'modificar',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>

<script type="text/javascript">
    $('#urole').change(function () {
        $.ajax({
            'type':'POST',
            'data':{'urole':this.value},
            'url':'<?php echo CController::createUrl('funciones/modulos'); ?>',
            'cache':false,
            'success':function(html){
                jQuery("#modul").html(html)
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#preci').mask('#.##0,00',{reverse: true,maxlength:false});
    });
</script>
<script>

$(document).on('click', '#close-preview', function () {
    $('.numero1 .image-preview').popover('hide');
    // Hover befor close the preview
    $('.numero1 .image-preview').hover(
        function () {
            $('.image-preview').popover('hide');
        },
        function () {
            $('.image-preview').popover('hide');
        }
    );
});
$(function () {
    // Create the close button
    var closebtn = $('<button/>', {
        type: "button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    $('.numero1 .image-preview').popover({
        trigger:'manual',
        html:true,
        title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
        content: "No hay imagen",
        placement:'top'
    });



    
    // Clear event
    $('.numero1 .image-preview-clear').click(function () {
        $('.numero1 .image-preview').attr("data-content", "").popover('hide');
        $('.numero1 .image-preview-filename').val("");
        $('.numero1 .image-preview-clear').hide();
        $('.numero1 .image-preview-input input:file').val("");
        $(".numero1 .image-preview-input-title").text("Buscar");
    });
    // Create the preview image
    $(".numero1 .image-preview-input input:file").change(function () {
        var img = $('<img/>', {
            id: 'dynamic',
            width: 250,
            height: 200
        });
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".numero1 .image-preview-input-title").text("Cambiar");
            $(".numero1 .image-preview-clear").show();
            $(".numero1 .image-preview-filename").val(file.name);
            img.attr('src', e.target.result);
            $(".numero1 .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
            }
        reader.readAsDataURL(file);
    });
});
</script>