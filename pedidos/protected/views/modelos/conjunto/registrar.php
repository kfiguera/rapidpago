<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Modelos</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Modelos</a></li>
            <li><a href="#">Conjunto</a></li>
            <li class="active">Registrar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Registrar Conjunto</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Tipo *</label>
                            <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT tmode_codig, tmode_descr
                                      FROM pedido_modelo_tipo 
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'tmode_codig','tmode_descr');
                                echo CHtml::dropDownList('tmode', '', $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Modelo *</label>
                            <?php 
                                $sql="SELECT model_codig, model_descr
                                      FROM pedido_modelo
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'model_codig','model_descr');
                                echo CHtml::dropDownList('model', '', $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Categoria *</label>
                            <?php 
                                $sql="SELECT mcate_codig, mcate_descr
                                      FROM pedido_modelo_categoria
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'mcate_codig','mcate_descr');
                                echo CHtml::dropDownList('categ', '', $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 hide">        
                        <div class="form-group">
                            <label>Variante *</label>
                            <?php 
                                $sql="SELECT mvari_codig, mvari_descr
                                      FROM pedido_modelo_variante
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'mvari_codig','mvari_descr');
                                echo CHtml::dropDownList('varia', 1, $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Adicional *</label>
                            <?php 
                                $sql="SELECT mopci_codig, mopci_descr
                                      FROM pedido_modelo_opcional
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'mopci_codig','mopci_descr');
                                echo CHtml::dropDownList('opcio', '', $data, array('class' => 'select2', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','multiple'=>'multiple')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Adicional 2 *</label>
                            <?php 
                                $sql="SELECT mopci_codig, mopci_descr
                                      FROM pedido_modelo_opcional
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'mopci_codig','mopci_descr');
                                echo CHtml::dropDownList('opci2', '', $data, array('class' => 'select2', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','multiple'=>'multiple')); ?>
                        </div>
                    </div>
                    
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>¿Que incluye?</label>
                                <?php 
                                $sql="SELECT inclu_codig, inclu_descr
                                      FROM pedido_incluye
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'inclu_codig','inclu_descr');
                                    echo CHtml::dropDownList('inclu', $opcional['mopci_inclu'], $data ,array('class' => 'select2 select2-multiple', 'placeholder' => "Descripción", 'multiple'=>'multiple')); 
                                ?>
                        </div>
                    </div>    
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Precio</label>
                                <?php 
                                    echo CHtml::textField('preci', '', array('class' => 'form-control', 'placeholder' => "Precio")); 
                                ?>
                        </div>
                    </div>
                    <div class="col-sm-8">        
                        <div class="numero1 form-group">
                            <label>Imagen Libre</label>
                            <div  class="input-group image-preview">  
                                <img id="dynamic">
                                <!-- image-preview-filename input [CUT FROM HERE]-->
                                <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                <span class="input-group-btn">
                                    <!-- image-preview-clear button -->
                                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                        <span class="fa fa-times"></span> Limpiar
                                    </button>
                                    <!-- image-preview-input -->
                                    <div class="btn btn-default image-preview-input">
                                        <span class="fa fa-folder-open"></span>
                                        <span class="numero1 image-preview-input-title">Buscar</span>
                                        <input type="file" id="image" name="image"/> <!-- rename it -->
                                    </div>
                                </span>
                           
                            </div> 
                        </div> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observación</label>
                                <?php 
                                    echo CHtml::textArea('obser', $conjunto['mconj_obser'], array('class' => 'form-control', 'placeholder' => "Observación")); 
                                ?>
                        </div>
                    </div>
                </div>
                
                
                
                    <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-4 ">
                            <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                        </div>
                    </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
<script>
    $(document).ready(function () {   
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                urole: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Rol de Usuario" es obligatorio',
                        }
                    }
                },modul: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Modulos" es obligatorio',
                        }
                    }
                }
                ,estat: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Estatus" es obligatorio',
                        }
                    }
                }




            }
        });
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        var data = new FormData(jQuery('form')[0]);
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: data,
                url: 'registrar',
                cache: false,
                contentType: false,
                processData: false,
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>
<script type="text/javascript">
    $('#urole').change(function () {
        $.ajax({
            'type':'POST',
            'data':{'urole':this.value},
            'url':'<?php echo CController::createUrl('funciones/modulos'); ?>',
            'cache':false,
            'success':function(html){
                jQuery("#modul").html(html)
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#preci').mask('#.##0,00',{reverse: true,maxlength:false});
    });
</script>
<script>
    $(document).on('click', '#close-preview', function () {
        $('.numero1 .image-preview').popover('hide');
        // Hover befor close the preview
        $('.numero1 .image-preview').hover(
            function () {
                $('.image-preview').popover('hide');
            },
            function () {
                $('.image-preview').popover('hide');
            }
        );
    });
    $(function () {
        // Create the close button
        var closebtn = $('<button/>', {
            type: "button",
            text: 'x',
            id: 'close-preview',
            style: 'font-size: initial;',
        });
        closebtn.attr("class","close pull-right");
        $('.numero1 .image-preview').popover({
            trigger:'manual',
            html:true,
            title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
            content: "No hay imagen",
            placement:'top'
        });



        
        // Clear event
        $('.numero1 .image-preview-clear').click(function () {
            $('.numero1 .image-preview').attr("data-content", "").popover('hide');
            $('.numero1 .image-preview-filename').val("");
            $('.numero1 .image-preview-clear').hide();
            $('.numero1 .image-preview-input input:file').val("");
            $(".numero1 .image-preview-input-title").text("Buscar");
        });
        // Create the preview image
        $(".numero1 .image-preview-input input:file").change(function () {
            var img = $('<img/>', {
                id: 'dynamic',
                width: 250,
                height: 200
            });
            var file = this.files[0];
            var reader = new FileReader();
            // Set preview image into the popover data-content
            reader.onload = function (e) {
                $(".numero1 .image-preview-input-title").text("Cambiar");
                $(".numero1 .image-preview-clear").show();
                $(".numero1 .image-preview-filename").val(file.name);
                img.attr('src', e.target.result);
                $(".numero1 .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
                }
            reader.readAsDataURL(file);
        });
    });
</script>