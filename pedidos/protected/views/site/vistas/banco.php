<style type="text/css">
    .col-xs-15,
    .col-sm-15,
    .col-md-15,
    .col-lg-15 {
        position: relative;
        min-height: 1px;
        padding-right: 10px;
        padding-left: 10px;
    }
    .col-xs-15 {
        width: 20%;
        float: left;
    }

    @media (min-width: 768px) {
    .col-sm-15 {
            width: 20%;
            float: left;
        }
    }

    @media (min-width: 992px) {
        .col-md-15 {
            width: 20%;
            float: left;
        }
    }

    @media (min-width: 1200px) {
        .col-lg-15 {
            width: 20%;
            float: left;
        }
    }
</style>
<?php $conexion=Yii::app()->db;?>
<div class="row bg-title">
	<!-- .page title -->
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
    	<h4 class="page-title">SOLICITUDES EN PROCESO 
        <?php
            if(Yii::app()->user->id['usuario']['permiso']==16){
                $sql="SELECT * FROM seguridad_roles WHERE srole_codig='".Yii::app()->user->id['usuario']['permiso']."'" ;
                $result=$conexion->createCommand($sql)->queryRow();
                echo $result['srole_descr'];
            }else{
                $result['srole_descr']='BANCO';
                echo 'BANCO';
            }

        ?>
        </h4>
    </div>
    <!-- /.page title -->
	<!-- .breadcrumb -->
    <div class="col-lg-8 col-sm-8 col-md-8 col-xs-8">
  		<ol class="breadcrumb">
        	<li class="active"><img src='/files/banco/banco_del_sur.png' class='img-responsive center-block' style="max-height: 25px;"></li>
        </ol>
    </div>
    
    <!-- /.breadcrumb -->
</div>
    <div class="white-box hide">
        <div class="row">
           
            <div class="col-md-4 col-md-offset-4">
                <div class="row">
                    <div class="col-md-12 m-b-15">
                            <img src='/files/banco/banco_del_sur.png' class='img-responsive center-block'>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
<div class="row">
    <?php

    ?> 
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?php echo $result['srole_descr'];?>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 m-b-15">
                        <img src='/files/banco/banco_del_sur.png' class='img-responsive center-block'>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div>
    <div class="col-md-4 col-lg-4 col-sm-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                Certificción de Datos
            </div>
                <div class="row row-in">
                    <?php
                        $i=0;
                        $sql="SELECT count(solic_codig) cantidad, 'fa fa-list' smodu_icono, 'PENDIENTES' smodu_descr, 'info' smodu_color
                        FROM solicitud a 
                        JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                        JOIN cliente c ON (a.clien_codig = c.clien_codig)
                        JOIN seguridad_usuarios d ON (a.usuar_codig = d.usuar_codig)
                        JOIN p_persona e ON (d.perso_codig = e.perso_codig)
                        JOIN seguridad_roles_banco f ON (a.banco_codig = f.banco_codig)
                        WHERE a.estat_codig in (6)
                        AND f.srole_codig = '".Yii::app()->user->id['usuario']['permiso']."'
                        ORDER BY solic_clvip, prere_codig";
                        $cantidad = $conexion->createCommand($sql)->queryRow();
                        /*foreach ($cantidades as $key => $cantidad) {
                            $porcentaje=($cantidad['cantidad']*100)/$total;
                            if($i<0){
                                $cabecera='row-in-br';
                            }else{
                                $cabecera='b-0';
                            }*/
                    ?>
                        <div class="col-sm-6 row-in-br">
                            <div class="col-in row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <i class="<?php echo $cantidad['smodu_icono']; ?>"></i>
                                    <h5 class="text-muted vb"><?php echo $cantidad['smodu_descr']; ?></h5>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <h3 class="counter text-right m-t-15 text-<?php echo $cantidad['smodu_color']; ?>"><?php echo $cantidad['cantidad'] ?></h3>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-<?php echo $cantidad['smodu_color']; ?>" role="progressbar" aria-valuenow="<?php echo $cantidad['cantidad'] ?> " aria-valuemin="0" aria-valuemax="<?php echo $cantidad['cantidad']; ?>" style="width: 100%"> <span class="sr-only"><?php echo $porcentaje ?>% Complete (success)</span> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php

                            $i++;
                       // }

                    ?>
                    <?php
                        $i=0;
                        $sql="SELECT count(a.solic_codig) cantidad, 'fa fa-check' smodu_icono, 'VERIFICADOS' smodu_descr, 'success' smodu_color 
                            FROM solicitud a 
                            JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig) 
                            JOIN cliente c ON (a.clien_codig = c.clien_codig) 
                            JOIN seguridad_usuarios d ON (a.usuar_codig = d.usuar_codig) 
                            JOIN p_persona e ON (d.perso_codig = e.perso_codig) 
                            JOIN seguridad_roles_banco f ON (a.banco_codig = f.banco_codig) 
                            JOIN solicitud_certificacion_banco g ON (a.solic_codig = g.solic_codig) 
                            WHERE f.srole_codig = '".Yii::app()->user->id['usuario']['permiso']."'
                            ORDER BY solic_clvip, prere_codig";
                        
                        $cantidad = $conexion->createCommand($sql)->queryRow();
                        /*foreach ($cantidades as $key => $cantidad) {
                            $porcentaje=($cantidad['cantidad']*100)/$total;
                            if($i<0){
                                $cabecera='row-in-br';
                            }else{
                                $cabecera='b-0';
                            }*/
                    ?>
                        <div class="col-sm-6 b-0">
                            <div class="col-in row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <i class="<?php echo $cantidad['smodu_icono']; ?>"></i>
                                    <h5 class="text-muted vb"><?php echo $cantidad['smodu_descr']; ?></h5>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <h3 class="counter text-right m-t-15 text-<?php echo $cantidad['smodu_color']; ?>"><?php echo $cantidad['cantidad'] ?></h3>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-<?php echo $cantidad['smodu_color']; ?>" role="progressbar" aria-valuenow="<?php echo $cantidad['cantidad'] ?> " aria-valuemin="0" aria-valuemax="<?php echo $cantidad['cantidad']; ?>" style="width: 100%"> <span class="sr-only"><?php echo $porcentaje ?>% Complete (success)</span> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                        $i++;
                    ?>
                </div>
            
            
        </div>
    </div>

    <div class="col-md-4 col-lg-4 col-sm-4">
        <div class="panel panel-success">
            <div class="panel-heading">
                Carga de Parametros
            </div>
                <div class="row row-in">
                    <?php
                        $i=0;
                        $sql="SELECT count(solic_codig) cantidad, 'fa fa-list' smodu_icono, 'PENDIENTES' smodu_descr, 'info' smodu_color
                        FROM solicitud a 
                        JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                        JOIN cliente c ON (a.clien_codig = c.clien_codig)
                        JOIN seguridad_usuarios d ON (a.usuar_codig = d.usuar_codig)
                        JOIN p_persona e ON (d.perso_codig = e.perso_codig)
                        JOIN seguridad_roles_banco f ON (a.banco_codig = f.banco_codig)
                        WHERE a.estat_codig in (12,13)
                        AND f.srole_codig = '".Yii::app()->user->id['usuario']['permiso']."'
                        ORDER BY solic_clvip, prere_codig";
                        $cantidad = $conexion->createCommand($sql)->queryRow();
                        /*foreach ($cantidades as $key => $cantidad) {
                            $porcentaje=($cantidad['cantidad']*100)/$total;
                            if($i<0){
                                $cabecera='row-in-br';
                            }else{
                                $cabecera='b-0';
                            }*/
                    ?>
                        <div class="col-sm-6 row-in-br">
                            <div class="col-in row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <i class="<?php echo $cantidad['smodu_icono']; ?>"></i>
                                    <h5 class="text-muted vb"><?php echo $cantidad['smodu_descr']; ?></h5>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <h3 class="counter text-right m-t-15 text-<?php echo $cantidad['smodu_color']; ?>"><?php echo $cantidad['cantidad'] ?></h3>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-<?php echo $cantidad['smodu_color']; ?>" role="progressbar" aria-valuenow="<?php echo $cantidad['cantidad'] ?> " aria-valuemin="0" aria-valuemax="<?php echo $cantidad['cantidad']; ?>" style="width: 100%"> <span class="sr-only"><?php echo $porcentaje ?>% Complete (success)</span> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php

                            $i++;
                       // }

                    ?>
                    <?php
                        $i=0;
                        $sql="SELECT count(a.solic_codig) cantidad, 'fa fa-check' smodu_icono, 'VERIFICADOS' smodu_descr, 'success' smodu_color 
                            FROM solicitud a 
                            JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig) 
                            JOIN cliente c ON (a.clien_codig = c.clien_codig) 
                            JOIN seguridad_usuarios d ON (a.usuar_codig = d.usuar_codig) 
                            JOIN p_persona e ON (d.perso_codig = e.perso_codig) 
                            JOIN seguridad_roles_banco f ON (a.banco_codig = f.banco_codig) 
                            JOIN solicitud_confirmacion_banco g ON (a.solic_codig = g.solic_codig) 
                            WHERE f.srole_codig = '".Yii::app()->user->id['usuario']['permiso']."'
                            ORDER BY solic_clvip, prere_codig";
                        
                        $cantidad = $conexion->createCommand($sql)->queryRow();
                        /*foreach ($cantidades as $key => $cantidad) {
                            $porcentaje=($cantidad['cantidad']*100)/$total;
                            if($i<0){
                                $cabecera='row-in-br';
                            }else{
                                $cabecera='b-0';
                            }*/
                    ?>
                        <div class="col-sm-6 b-0">
                            <div class="col-in row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <i class="<?php echo $cantidad['smodu_icono']; ?>"></i>
                                    <h5 class="text-muted vb"><?php echo $cantidad['smodu_descr']; ?></h5>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <h3 class="counter text-right m-t-15 text-<?php echo $cantidad['smodu_color']; ?>"><?php echo $cantidad['cantidad'] ?></h3>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-<?php echo $cantidad['smodu_color']; ?>" role="progressbar" aria-valuenow="<?php echo $cantidad['cantidad'] ?> " aria-valuemin="0" aria-valuemax="<?php echo $cantidad['cantidad']; ?>" style="width: 100%"> <span class="sr-only"><?php echo $porcentaje ?>% Complete (success)</span> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                        $i++;
                    ?>
                </div>
            
            
        </div>
    </div>

</div>
