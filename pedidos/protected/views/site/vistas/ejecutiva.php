<style type="text/css">
    .col-xs-15,
    .col-sm-15,
    .col-md-15,
    .col-lg-15 {
        position: relative;
        min-height: 1px;
        padding-right: 10px;
        padding-left: 10px;
    }
    .col-xs-15 {
        width: 20%;
        float: left;
    }

    @media (min-width: 768px) {
    .col-sm-15 {
            width: 20%;
            float: left;
        }
    }

    @media (min-width: 992px) {
        .col-md-15 {
            width: 20%;
            float: left;
        }
    }

    @media (min-width: 1200px) {
        .col-lg-15 {
            width: 20%;
            float: left;
        }
    }
</style>
<?php $conexion=Yii::app()->db;?>
<div class="row bg-title">
	<!-- .page title -->
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
    	<h4 class="page-title">Resumen Operativo</h4>
    </div>
    <!-- /.page title -->
	<!-- .breadcrumb -->
    <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
  		<ol class="breadcrumb">
        	<li class="active">Inicio</li>
        </ol>
    </div>
    <!-- /.breadcrumb -->
</div>
<div class="row">
    <?php
        $sql="SELECT count(solic_codig) cantidad FROM solicitud WHERE estat_codig NOT IN (0)";
        $solicitud = $conexion->createCommand($sql)->queryRow();

        $sql="SELECT count(prere_codig) cantidad FROM rd_preregistro WHERE estat_codig NOT IN (0,4)";
        $preregistro = $conexion->createCommand($sql)->queryRow();

        $total=$solicitud['cantidad']+$preregistro['cantidad'];

        $sql="SELECT count(prere_codig) cantidad, c.smodu_codig, c.smodu_descr, c.smodu_icono, c.smodu_color
              FROM rd_preregistro a 
              JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig and a.estat_codig not in (0,4))
              JOIN solicitud_modulo c ON (b.smodu_codig = c.smodu_codig)
              UNION
              SELECT count(solic_codig) cantidad,  c.smodu_codig, c.smodu_descr, c.smodu_icono, c.smodu_color
              FROM solicitud a 
              JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig and a.estat_codig not in (0,27))
              RIGHT JOIN solicitud_modulo c ON (b.smodu_codig = c.smodu_codig)
              WHERE c.smodu_codig <> 1
                AND c.smodu_codig = 2

                AND a.estat_codig < 11
              GROUP BY 2,3,4,5
              ORDER BY 2";
        $cantidades = $conexion->createCommand($sql)->queryAll();
    ?> 
    <div class="col-md-12 col-lg-4 col-sm-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                Pre-Venta
            </div>
                <div class="row row-in">
                    <?php
                        $i=0;
                        foreach ($cantidades as $key => $cantidad) {
                            $porcentaje=($cantidad['cantidad']*100)/$total;
                            if($i<0){
                                $cabecera='row-in-br';
                            }else{
                                $cabecera='b-0';
                            }
                    ?>
                        <div class="col-sm-12 <?php echo $cabecera; ?>">
                            <div class="col-in row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <i class="<?php echo $cantidad['smodu_icono']; ?>"></i>
                                    <h5 class="text-muted vb"><?php echo $cantidad['smodu_descr']; ?></h5>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <h3 class="counter text-right m-t-15 text-<?php echo $cantidad['smodu_color']; ?>"><?php echo $cantidad['cantidad'] ?></h3>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-<?php echo $cantidad['smodu_color']; ?>" role="progressbar" aria-valuenow="<?php echo $porcentaje ?> " aria-valuemin="0" aria-valuemax="<?php echo $total; ?>" style="width: <?php echo $porcentaje ?>%"> <span class="sr-only"><?php echo $porcentaje ?>% Complete (success)</span> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                            $i++;
                        }

                    ?>
                </div>
            <div class="panel-heading panel-footer text-center">
                Días promedio en Pre-Venta: <b><?php echo $this->funciones->promedioPreVenta($conexion); ?></b>
            </div>
            
        </div>
    </div>
    <?php
        $sql="SELECT count(solic_codig) cantidad FROM solicitud WHERE estat_codig NOT IN (0)";
        $solicitud = $conexion->createCommand($sql)->queryRow();

        $sql="SELECT count(prere_codig) cantidad FROM rd_preregistro WHERE estat_codig NOT IN (0,4)";
        $preregistro = $conexion->createCommand($sql)->queryRow();

        $total=$solicitud['cantidad']+$preregistro['cantidad'];

        $sql="SELECT count(solic_codig) cantidad,  c.smodu_codig, c.smodu_descr, c.smodu_icono, c.smodu_color
              FROM solicitud a 
              JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig and a.estat_codig not in (0,27))
              RIGHT JOIN solicitud_modulo c ON (b.smodu_codig = c.smodu_codig)
              WHERE c.smodu_codig <> 1
                AND c.smodu_codig in (2,3,4,5)
                AND a.estat_codig >= 11
              GROUP BY 2,3,4,5
              ORDER BY 2";
        $cantidades = $conexion->createCommand($sql)->queryAll();
    ?> 
    <div class="col-md-12 col-lg-8 col-sm-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                Venta
            </div>
                <div class="row row-in">
                    <?php
                        $i=0;
                        foreach ($cantidades as $key => $cantidad) {
                            $porcentaje=($cantidad['cantidad']*100)/$total;
                            if($i==0 or $i==2){
                                $cabecera='row-in-br';

                            }else{
                                $cabecera='b-0';
                                
                            }
                            if($i==2){
                                $i=0;
                            }
                    ?>
                        <div class="col-sm-6 <?php echo $cabecera; ?>">
                            <div class="col-in row">
                                <div class="col-md-6 col-sm-6 col-xs-6"> <i class="<?php echo $cantidad['smodu_icono']; ?>"></i>
                                    <h5 class="text-muted vb"><?php echo $cantidad['smodu_descr']; ?></h5>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <h3 class="counter text-right m-t-15 text-<?php echo $cantidad['smodu_color']; ?>"><?php echo $cantidad['cantidad'] ?></h3>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-<?php echo $cantidad['smodu_color']; ?>" role="progressbar" aria-valuenow="<?php echo $porcentaje ?> " aria-valuemin="0" aria-valuemax="<?php echo $total; ?>" style="width: <?php echo $porcentaje ?>%"> <span class="sr-only"><?php echo $porcentaje ?>% Complete (success)</span> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                            $i++;
                        }

                    ?>
                </div>
            <div class="panel-heading panel-footer text-center">
                Días promedio en Venta: <b><?php echo $this->funciones->promedioPostVenta($conexion); ?></b>
            </div>
            
        </div>
    </div>
</div>
<?php
if(Yii::app()->user->id['usuario']['permiso']==13 or Yii::app()->user->id['usuario']['permiso']==12){
    ?>
    <div class="white-box m-t-30 hide">
        <div class="row">
           
            <div class="col-md-4 col-md-offset-4">
                <div class="row">
                    <div class="col-md-12 m-b-15">
                            <img src='https://autogestion.rapidpago.com/assets/img/logo.png' class='img-responsive center-block'>
                    </div>
                </div>
                
            </div>
        </div>
    </div>



        <?php

        $post['usuar']=Yii::app()->user->id['usuario']['usuario'];
        $sql="SELECT * FROM seguridad_usuarios WHERE usuar_login = '".$_POST['usuar']."'";
        $usuario=$conexion->createCommand($sql)->queryRow();
        Yii::app()->user->id['usuario']['codigo']=$usuar=$usuario['usuar_codig'];

        if($usuario['urole_codig']==13 or $usuario['urole_codig']==12){
            $sql = "SELECT solic_numer, a.estat_codig, estat_descr
                FROM solicitud a 
                JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                JOIN cliente c ON (a.clien_codig = c.clien_codig)
                JOIN seguridad_usuarios d ON (a.usuar_codig = d.usuar_codig)
                JOIN p_persona e ON (d.perso_codig = e.perso_codig)
                WHERE a.estat_codig between 6 and 30
                AND a.usuar_codig = '".$usuar."'
                UNION 
                SELECT prere_numer, a.estat_codig, estat_descr
                FROM rd_preregistro a 
                JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                JOIN seguridad_usuarios d ON (a.usuar_codig = d.usuar_codig)
                JOIN p_persona e ON (d.perso_codig = e.perso_codig)
                WHERE a.estat_codig in(1,2,3,5)
                AND a.usuar_codig = '".$usuar."'
                ";
        }else{
            $sql = "SELECT *
                FROM solicitud a 
                JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                JOIN cliente c ON (a.clien_codig = c.clien_codig)
                JOIN seguridad_usuarios d ON (a.usuar_codig = d.usuar_codig)
                JOIN p_persona e ON (d.perso_codig = e.perso_codig)
                WHERE a.estat_codig between 6 and 30
                ORDER BY solic_clvip, prere_codig
                ";    
        }
        foreach ($datos as $key => $dato) {
            $ubicacion[0]='start ';
            $ubicacion[4]='finish ';
            $sql="SELECT a.*, b.estat_codig, b.estat_descr 
                  FROM solicitud_modulo a 
                  JOIN rd_preregistro_estatus b ON (a.smodu_codig = b.smodu_codig and b.estat_codig ='".$dato['estat_codig']."')";
            $pasos=$conexion->createCommand($sql)->queryRow();
            $paso=$pasos['smodu_codig'];
            for ($i=0; $i < 5 ; $i++) { 
                $icono[$i]=$i+1;
                if($i<($paso-1)){
                    $ubicacion[$i].='upcoming ';
                    $icono[$i]="<i class='fa fa-check'></i>";
                }
                if ($i==($paso-1)) {
                    $ubicacion[$i].='active ';
                }
            }
            $sql="SELECT * 
                  FROM solicitud_modulo a ";
            $modulos=$conexion->createCommand($sql)->queryAll();
            $i=0;


            ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">

                        
                        <div class="col-md-8">
                            <h3 class="panel-title"><b>Solicitud:</b> <a href=""><?php echo $datos[$key]['solic_numer']; ?></h3>
                        </div>
                        <div class="col-md-4 text-right">
                            <h3 class="panel-title"><b>Estatus Actual:</b> <?php echo $datos[$key]['estat_descr']; ?></h3>
                        </div>
                    </div>
                    
                </div>
                <div class="panel-body" >
                    <div class="row line-steps">
                        <?php
                        $i=0;
                        foreach ($modulos as $key => $modulo) {                        ?>
                        <div class="col-md-15 column-step <?php echo $ubicacion[$i]; ?>">
                            <div class="step-number"><?php echo $icono[$i]; ?> </div>
                            <div class="step-title"><?php echo $modulo['smodu_descr']; ?></div>
                        </div>
                        <?php
                        $i++;
                    }
                    ?>
                      
                </div>
            </div>
        </div>
            
            <?php
        }
        ?> 
       
<?php }else { ?>
<div class="white-box hide">
    <div class="row">
       
        <div class="col-md-4 col-md-offset-4">
            <div class="row">
                <div class="col-md-12 m-b-15">
                        <img src='https://autogestion.rapidpago.com/assets/img/logo.png' class='img-responsive center-block'>
                </div>
            </div>
            
        </div>
    </div>
</div>
<?php
$sql="SELECT count(solic_codig) cantidad FROM solicitud WHERE estat_codig NOT IN (0)";
$solicitud = $conexion->createCommand($sql)->queryRow();

$sql="SELECT count(prere_codig) cantidad FROM rd_preregistro WHERE estat_codig NOT IN (0,4)";
$preregistro = $conexion->createCommand($sql)->queryRow();

$total=$solicitud['cantidad']+$preregistro['cantidad'];

$sql="SELECT count(prere_codig) cantidad, c.smodu_codig, c.smodu_descr, c.smodu_icono, c.smodu_color
      FROM rd_preregistro a 
      JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig and a.estat_codig not in (0,4))
      JOIN solicitud_modulo c ON (b.smodu_codig = c.smodu_codig)
      UNION
      SELECT count(solic_codig) cantidad,  c.smodu_codig, c.smodu_descr, c.smodu_icono, c.smodu_color
      FROM solicitud a 
      JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig and a.estat_codig not in (0,27))
      RIGHT JOIN solicitud_modulo c ON (b.smodu_codig = c.smodu_codig)
      WHERE c.smodu_codig <> 1
      GROUP BY 2,3,4,5
      ORDER BY 2";
$cantidades = $conexion->createCommand($sql)->queryAll();
?>
<div class="row hide">
    <div class="col-md-12 col-lg-12 col-sm-12">
        <div class="white-box">
            <div class="row row-in">
            <?php
            $i=0;
            foreach ($cantidades as $key => $cantidad) {
                $porcentaje=($cantidad['cantidad']*100)/$total;
                if($i<4){
                    $cabecera='row-in-br';
                }else{
                    $cabecera='b-0';
                }
                ?>
                <div class="col-lg-15 col-sm-6 <?php echo $cabecera; ?>">
                    <div class="col-in row">
                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="<?php echo $cantidad['smodu_icono']; ?>"></i>
                            <h5 class="text-muted vb"><?php echo $cantidad['smodu_descr']; ?></h5>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <h3 class="counter text-right m-t-15 text-<?php echo $cantidad['smodu_color']; ?>"><?php echo $cantidad['cantidad'] ?></h3>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="progress">
                                <div class="progress-bar progress-bar-<?php echo $cantidad['smodu_color']; ?>" role="progressbar" aria-valuenow="<?php echo $porcentaje ?> " aria-valuemin="0" aria-valuemax="<?php echo $total; ?>" style="width: <?php echo $porcentaje ?>%"> <span class="sr-only"><?php echo $porcentaje ?>% Complete (success)</span> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                $i++;
            }

            ?>
            </div>
          </div>
        </div>
      </div>

     
<!-- .row -->
<div class="row">
    <div class="col-md-12 hide">
            
        <div class="row">
            <div class="col-md-15">
                <div class="white-box">
                    <h3 class="box-title">REGISTRO </h3>
                    <ul class="list-inline two-part">
                        <li><i class="fa fa-edit text-info"></i></li>
                        <li class="text-right"><span class="counter">23</span></li>
                    </ul>
                  </div>
            </div>
            <div class="col-md-15">
                <div class="white-box">
                    <h3 class="box-title">TRAMTACIÓN</h3>
                    <ul class="list-inline two-part">
                        <li><i class="fa fa-bank text-success"></i></li>
                        <li class="text-right"><span class="counter">23</span></li>
                    </ul>
                  </div>
            </div>
            <div class="col-md-15">
                <div class="white-box">
                    <h3 class="box-title">ALMACEN</h3>
                    <ul class="list-inline two-part">
                        <li><i class="fa fa-archive text-warning"></i></li>
                        <li class="text-right"><span class="counter">23</span></li>
                    </ul>
                  </div>
            </div>
            <div class="col-md-15">
                <div class="white-box">
                    <h3 class="box-title">CONFIGURACION</h3>
                    <ul class="list-inline two-part">
                        <li><i class="fa fa-sliders text-primary"></i></li>
                        <li class="text-right"><span class="counter">23</span></li>
                    </ul>
                  </div>
            </div>
            <div class="col-md-15">
                <div class="white-box">
                    <h3 class="box-title">DESPACHO</h3>
                    <ul class="list-inline two-part">
                        <li><i class="fa fa-truck text-danger"></i></li>
                        <li class="text-right"><span class="counter">23</span></li>
                    </ul>
                  </div>
            </div>
        </div>
        
    </div>
<div class="col-md-4">
<div class="row">
   <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Ventas con Retraso (15 dias despues de pago)</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <canvas id="chart7" height="239" width="479" style="width: 533px; height: 266px;"> </canvas>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-md-10">
                        <span class="text-muted">Nota: El tiempo de retraso es a partir de los 15 días de Pago</span>
                    </div>
                    <div class="col-md-2">
                        <a class="btn btn-info btn-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="Reporte" href="<?php echo Yii::app()->request->getBaseUrl(true); ?>/reportes/solicitudesRetraso/listado"> <i class="fa fa-file-pdf-o"></i>
                        </a>
                    </div>
                </div>
            </div>  
            <hr class="m-t-0 m-b-0">  
            <div class="panel-body">
                <h3 class="panel-title">Detalle:
                <div class="pull-right">
                    <a href="#" data-perform="panel-collapse" class="btn btn-info btn-circle btn-outline">
                        <i class="ti-plus"></i>
                    </a> 
                
                </div>
                </h3>

            </div>
            <div class="panel-wrapper collapse table-responsive" aria-expanded="true">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Descripcion</th>
                        <th>Cantidad</th>
                    </tr>
                </thead>
                <tbody>
                
                <?php
                  $sql="SELECT *
                    FROM solicitud a
                    JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                    WHERE a.estat_codig NOT IN (0,27)
                      AND a.estat_codig >= '11'
                    GROUP BY solic_codig;";
                $datos=$conexion->createCommand($sql)->queryAll();
                $total=0;
                $i=1;
                $retraso=0;
                $atiempo=0;

                foreach ($datos as $key => $dato) {
                        $sql="SELECT *
                              FROM solicitud_trayectoria a
                              WHERE estat_codig ='11'
                                AND solic_codig='".$dato['solic_codig']."'";
                        $traye=$conexion->createCommand($sql)->queryRow();
                        $inicio=$traye["traye_finic"].' '.$traye["traye_hinic"];
                        $fin=date('Y-m-d H:i:s');
                        $diff=$this->funciones->diferenciaHoras($inicio, $fin);
                        $diferenciaHoras=$this->funciones->diferenciaEnLetras($diff);
                        $horas=($diff->days * 24 )  + ( $diff->h );

                        $sql="SELECT *
                              FROM p_dias_entrega a";
                        $entrega=$conexion->createCommand($sql)->queryRow();
                        if($horas > ($entrega['dentr_value'] * 24)){
                            $retraso++;
                            $marca='SI';
                        }else{
                            $atiempo++;
                            $marca='NO';
                        }
                    $total+=$dato["cantidad"];
                    $i++;
                }
                $relacion[0]['descr']='A TIEMPO';
                $relacion[0]['canti']=$atiempo;
                $relacion[0]['color']='#00c292';
                $relacion[1]['descr']='RETRASO';
                $relacion[1]['canti']=$retraso;
                $relacion[1]['color']='#ed4040';
                $i=0;
                foreach ($relacion as $key => $value) {
                    $i++;
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $value['descr']; ?></td>
                        <td><?php echo $value['canti']; ?></td>
                    </tr>
                    <?php
                    $total+= $value['canti'];
                }
                ?>
                  
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2">Total de Solicitudes</td>
                        <td><?php echo $total; ?></td>
                    </tr>
                </tfoot>
            </table>
        </div>
        </div>
    </div> 
</div>    
<div class="row">    
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Plazo de Entrega</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <canvas id="chart8" height="239" width="479" style="width: 533px; height: 266px;"> </canvas>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <h3 class="panel-title">Detalle:
                <div class="pull-right">
                    <a href="#" data-perform="panel-collapse" class="btn btn-info btn-circle btn-outline">
                        <i class="ti-plus"></i>
                    </a> 
                
                </div>
                </h3>

            </div>
            <div class="panel-wrapper collapse table-responsive" aria-expanded="true">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Descripcion</th>
                        <th>Cantidad</th>
                    </tr>
                </thead>
                <tbody>
                
                <?php
                  $sql="SELECT *
                    FROM solicitud a
                    JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                    WHERE a.estat_codig IN (27)
                    GROUP BY solic_codig;";
                $datos=$conexion->createCommand($sql)->queryAll();
                $total=0;
                $i=1;
                $retraso=0;
                $atiempo=0;

                foreach ($datos as $key => $dato) {
                        $sql="SELECT *
                              FROM solicitud_trayectoria a
                              WHERE estat_codig ='11'
                                AND solic_codig='".$dato['solic_codig']."'";
                        $tinicio=$conexion->createCommand($sql)->queryRow();
                        $sql="SELECT *
                              FROM solicitud_trayectoria a
                              WHERE estat_codig ='27'
                                AND solic_codig='".$dato['solic_codig']."'";
                        $tfin=$conexion->createCommand($sql)->queryRow();
                        
                        $inicio=$tinicio["traye_finic"].' '.$tinicio["traye_hinic"];
                        $fin=$tfin["traye_finic"].' '.$tfin["traye_hinic"];
                        $diff=$this->funciones->diferenciaHoras($inicio, $fin);
                        $diferenciaHoras=$this->funciones->diferenciaEnLetras($diff);
                        $horas=($diff->days * 24 )  + ( $diff->h );

                        $sql="SELECT *
                              FROM p_dias_entrega a";
                        $entrega=$conexion->createCommand($sql)->queryRow();
                        if($horas > ($entrega['dentr_value'] * 24)){
                            $retraso++;
                            $marca='SI';
                        }else{
                            $atiempo++;
                            $marca='NO';
                        }
                    $total+=$dato["cantidad"];
                    $i++;
                }
                $pentrega[0]['descr']='A TIEMPO';
                $pentrega[0]['canti']=$atiempo;
                $pentrega[0]['color']='#00c292';
                $pentrega[1]['descr']='RETRASO';
                $pentrega[1]['canti']=$retraso;
                $pentrega[1]['color']='#ed4040';
                $i=0;
                foreach ($pentrega as $key => $value) {
                    $i++;
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $value['descr']; ?></td>
                        <td><?php echo $value['canti']; ?></td>
                    </tr>
                    <?php
                    $total+= $value['canti'];
                }
                ?>
                  
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2">Total de Solicitudes</td>
                        <td><?php echo $total; ?></td>
                    </tr>
                </tfoot>
            </table>
        </div>
        </div>
    </div>
</div>

<div class="row">   
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Solicitudes por Origen</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <canvas id="chart5" height="239" width="479" style="width: 533px; height: 266px;"> </canvas>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <h3 class="panel-title">Detalle:
                <div class="pull-right">
                    <a href="#" data-perform="panel-collapse" class="btn btn-info btn-circle btn-outline">
                        <i class="ti-plus"></i>
                    </a> 
                
                </div>
                </h3>

            </div>
            <div class="panel-wrapper collapse table-responsive" aria-expanded="true">
            <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Descrición</th>
                                <th>Cantidad</th>
                            </tr>
                        </thead>
                        <tbody>
                        
                        <?php
                          $sql="
                              SELECT count(a.orige_codig) cantidad,b.orige_codig, b.orige_descr
                              FROM rapidpago.solicitud a
                              JOIN rapidpago.p_solicitud_origen b ON (a.orige_codig = b.orige_codig)
                              WHERE a.estat_codig <> 0
                              GROUP BY orige_codig
                              ORDER BY orige_codig;";
                        $datos=$conexion->createCommand($sql)->queryAll();
                        $total=0;
                        $i=1;
                        foreach ($datos as $key => $dato) {
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $dato["orige_descr"]; ?></td>
                                <td><?php echo $dato["cantidad"]; ?></td>
                            </tr>
                            <?php
                            $total+=$dato["cantidad"];
                            $i++;
                        }

                        ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2">Total de Solicitudes</td>
                                <td><?php echo $total; ?></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
        </div>
    </div>
    <div class="col-md-4 hide">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Solicitudes por Estatus</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <canvas id="chart4" height="239" width="479" style="width: 533px; height: 266px;"> </canvas>
                    </div>
                </div>
                
            </div>
            <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Modulo</th>
                                <th>Descrición</th>
                                <th>Cantidad</th>
                            </tr>
                        </thead>
                        <tbody>
                        
                        <?php
                          $sql="SELECT b.smodu_codig, count(a.estat_codig) cantidad,b.estat_codig, b.estat_descr, c.smodu_descr
                              FROM rapidpago.rd_preregistro a
                              JOIN rapidpago.rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                              JOIN rapidpago.solicitud_modulo c ON (b.smodu_codig = c.smodu_codig)
                              WHERE a.estat_codig not in (0,4)

                              GROUP BY estat_codig
                              UNION
                              SELECT b.smodu_codig, count(a.estat_codig) cantidad,b.estat_codig, b.estat_descr, c.smodu_descr
                              FROM rapidpago.solicitud a
                              JOIN rapidpago.rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                              JOIN rapidpago.solicitud_modulo c ON (b.smodu_codig = c.smodu_codig)
                              WHERE a.estat_codig <> 0
                              GROUP BY estat_codig
                              ORDER BY smodu_codig,estat_codig;";
                        $datos=$conexion->createCommand($sql)->queryAll();
                        $total=0;
                        $i=1;
                        foreach ($datos as $key => $dato) {
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $dato["smodu_descr"]; ?></td>
                                <td><?php echo $dato["estat_descr"]; ?></td>
                                <td><?php echo $dato["cantidad"]; ?></td>
                            </tr>
                            <?php
                            $total+=$dato["cantidad"];
                            $i++;
                        }

                        ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="3">Total de Solicitudes</td>
                                <td><?php echo $total; ?></td>
                            </tr>
                        </tfoot>
                    </table>
        </div>
    </div>

    
    <div class="col-md-4 hide">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Solicitudes de Acceso</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <canvas id="chart6" height="239" width="479" style="width: 533px; height: 266px;"> </canvas>
                    </div>
                </div>
            </div>
            <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Descrición</th>
                                <th>Cantidad</th>
                            </tr>
                        </thead>
                        <tbody>
                        
                        <?php
                          $sql="SELECT count(b.uesta_codig) cantidad, b.uesta_codig
                            FROM rd_acceso a 
                            JOIN seguridad_usuarios b ON (a.acces_corre = b.usuar_login)
                            WHERE uesta_codig in (1,2)
                            GROUP BY uesta_codig;";
                        $datos=$conexion->createCommand($sql)->queryAll();
                        $total=0;
                        $i=1;
                        foreach ($datos as $key => $dato) {
                            if($dato['uesta_codig']=='2'){
                                $estatus='PENDIENTE';
                            }else{
                                $estatus='APROBADO';
                            }
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $estatus; ?></td>
                                <td><?php echo $dato["cantidad"]; ?></td>
                            </tr>
                            <?php
                            $total+=$dato["cantidad"];
                            $i++;
                        }

                        ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2">Total de Solicitudes</td>
                                <td><?php echo $total; ?></td>
                            </tr>
                        </tfoot>
                    </table>
        </div>
    </div>
</div>
</div>
<div class="col-md-8">
    
    <div class="row"> 
            <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Promedio Solicitudes por Estatus</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <canvas id="chart1" style="width: 100%; height: 100%; max-width: 100%; min-height: 400px"> </canvas>
                    </div>
                </div>
                
            </div>
            
            <div class="panel-body">
                <h3 class="panel-title">Detalle:
                <div class="pull-right">
                    <a href="#" data-perform="panel-collapse" class="btn btn-info btn-circle btn-outline">
                        <i class="ti-plus"></i>
                    </a> 
                
                </div>
                </h3>

            </div>
            <div class="panel-wrapper collapse table-responsive" aria-expanded="false">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Estatus</th>
                            <th>Iniciado</th>
                            <th>A Tiempo</th>
                            <th>Demorado</th>
                            <th>Critico</th>
                            <th>Total</th>
                            <th>Promedio</th>
                        </tr>
                    </thead>
                    <tbody >
                    
                    <?php
                      $sql="SELECT a.estat_codig, a.estat_descr, COUNT(b.prere_codig) cantidad
                            FROM rapidpago.rd_preregistro_estatus a
                            JOIN rapidpago.rd_preregistro b ON (a.estat_codig = b.estat_codig)
                            WHERE a.estat_codig NOT IN (0,4)
                            GROUP BY a.estat_codig
                            UNION
                            SELECT a.estat_codig, a.estat_descr, COUNT(b.solic_codig) cantidad
                            FROM rapidpago.rd_preregistro_estatus a
                            JOIN rapidpago.solicitud b ON (a.estat_codig = b.estat_codig)
                            WHERE a.estat_codig NOT IN (0,27)
                            GROUP BY a.estat_codig";
                    $datos=$conexion->createCommand($sql)->queryAll();
                    $total=0;
                    $totales[0]=0;
                    $totales[1]=0;
                    $totales[2]=0;
                    $totales[3]=0;
                    $tprome=0;
                    $i=1;
                    foreach ($datos as $key => $dato) {
                        $grafico1['label'][]=$dato["estat_descr"];
                        $testatu=$this->funciones->promedioEstatus($conexion,$dato["estat_codig"]);
                        $promedio=$this->funciones->transformarMonto_v($testatu['promedio'],0);
                        $promedio=$this->funciones->transformarMonto_bd($promedio);

                        $grafico1['0'][]=$testatu[1];
                        $grafico1['1'][]=$testatu[2];
                        $grafico1['2'][]=$testatu[3];
                        $grafico1['3'][]=$testatu[4];
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $dato["estat_descr"]; ?></td>
                            <td><?php echo $testatu[1]; ?></td>

                            <td><?php echo $testatu[2]; ?></td>
                            <td><?php echo $testatu[3]; ?></td>
                            <td><?php echo $testatu[4]; ?></td>
                            <td><?php echo $testatu[0]; ?></td>

                            <td><?php echo $this->funciones->secondsToTime($promedio); ?></td>
                        </tr>
                        <?php
                        $total+=$testatu[0];
                        $totales[0]+=$testatu[1];
                        $totales[1]+=$testatu[2];
                        $totales[2]+=$testatu[3];
                        $totales[3]+=$testatu[4];
                        $tprome+=$promedio;
                        $i++;
                    }
                    $tprome=$tprome/$i;
                    $tprome=$this->funciones->transformarMonto_v($tprome,0);
                        $tprome=$this->funciones->transformarMonto_bd($tprome);
                        
                    ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="2">Total de Solicitudes</th>
                            <th><?php echo $totales[0]; ?></th>
                            <th><?php echo $totales[1]; ?></th>
                            <th><?php echo $totales[2]; ?></th>
                            <th><?php echo $totales[3]; ?></th>

                            <th><?php echo $total; ?></th>
                            <th><?php echo $this->funciones->secondsToTime($tprome); ?></th>

                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    </div>
    <div class="row"> 
        <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Promedio Solicitudes por Proceso</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <canvas id="chart2" style="width: 100%; height: 100%; max-width: 100%; min-height: 400px"> </canvas>
                    </div>
                </div>
                
            </div>
            
            <div class="panel-body">
                <h3 class="panel-title">Detalle:
                <div class="pull-right">
                    <a href="#" data-perform="panel-collapse" class="btn btn-info btn-circle btn-outline">
                        <i class="ti-plus"></i>
                    </a> 
                
                </div>
                </h3>

            </div>
            <div class="panel-wrapper collapse table-responsive" aria-expanded="true">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Proceso</th>
                            <th>Iniciado</th>
                            <th>A Tiempo</th>
                            <th>Demorado</th>
                            <th>Critico</th>
                            <th>Total</th>
                            <th>Promedio</th>
                        </tr>
                    </thead>
                    <tbody >
                    
                    <?php
                      $sql="SELECT c.smodu_codig, c.smodu_descr, COUNT(b.prere_codig) cantidad
                            FROM rapidpago.solicitud_modulo c
                            JOIN rapidpago.rd_preregistro_estatus a ON (c.smodu_codig = a.smodu_codig)
                            JOIN rapidpago.rd_preregistro b ON (a.estat_codig = b.estat_codig)
                            WHERE a.estat_codig NOT IN (0,4)
                            GROUP BY c.smodu_codig
                            UNION
                            SELECT c.smodu_codig, c.smodu_descr, COUNT(b.solic_codig) cantidad
                            FROM rapidpago.solicitud_modulo c
                            JOIN rapidpago.rd_preregistro_estatus a ON (c.smodu_codig = a.smodu_codig)
                            JOIN rapidpago.solicitud b ON (a.estat_codig = b.estat_codig)
                            WHERE a.estat_codig NOT IN (0,27)
                            GROUP BY c.smodu_codig";
                    $datos=$conexion->createCommand($sql)->queryAll();
                    $total=0;
                    $totales[0]=0;
                    $totales[1]=0;
                    $totales[2]=0;
                    $totales[3]=0;
                    $tprome=0;
                    $i=1;
                    foreach ($datos as $key => $dato) {
                        $grafico2['label'][]=$dato["smodu_descr"];
                        $testatu=$this->funciones->promedioProceso($conexion,$dato["smodu_codig"]);
                        $promedio=$this->funciones->transformarMonto_v($testatu['promedio'],0);
                        $promedio=$this->funciones->transformarMonto_bd($promedio);

                        $grafico2['0'][]=$testatu[1];
                        $grafico2['1'][]=$testatu[2];
                        $grafico2['2'][]=$testatu[3];
                        $grafico2['3'][]=$testatu[4];
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $dato["smodu_descr"]; ?></td>
                            <td><?php echo $testatu[1]; ?></td>

                            <td><?php echo $testatu[2]; ?></td>
                            <td><?php echo $testatu[3]; ?></td>
                            <td><?php echo $testatu[4]; ?></td>
                            <td><?php echo $testatu[0]; ?></td>

                            <td><?php echo $this->funciones->secondsToTime($promedio); ?></td>
                        </tr>
                        <?php
                        $total+=$testatu[0];
                        $totales[0]+=$testatu[1];
                        $totales[1]+=$testatu[2];
                        $totales[2]+=$testatu[3];
                        $totales[3]+=$testatu[4];
                        $tprome+=$promedio;
                        $i++;
                    }
                    $tprome=$tprome/$i;
                    $tprome=$this->funciones->transformarMonto_v($tprome,0);
                        $tprome=$this->funciones->transformarMonto_bd($tprome);
                        
                    ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="2">Total de Solicitudes</th>
                            <th><?php echo $totales[0]; ?></th>
                            <th><?php echo $totales[1]; ?></th>
                            <th><?php echo $totales[2]; ?></th>
                            <th><?php echo $totales[3]; ?></th>

                            <th><?php echo $total; ?></th>
                            <th><?php echo $this->funciones->secondsToTime($tprome); ?></th>

                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    </div>
</div>
    
    
</div>

<script type="text/javascript">
    $(document).ready(function () {

        $(".counter").counterUp({
            delay: 100,
            time: 1200
        });

    });
</script>
<?php
    $sql="SELECT b.smodu_codig, count(a.estat_codig) cantidad,b.estat_codig, b.estat_descr, c.smodu_codig, c.smodu_descr
          FROM rapidpago.rd_preregistro a
          JOIN rapidpago.rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
          JOIN rapidpago.solicitud_modulo c ON (b.smodu_codig = c.smodu_codig)
          WHERE a.estat_codig not in (0,4)

          GROUP BY estat_codig
          UNION
          SELECT b.smodu_codig, count(a.estat_codig) cantidad,b.estat_codig, b.estat_descr, c.smodu_codig, c.smodu_descr
          FROM rapidpago.solicitud a
          JOIN rapidpago.rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
          JOIN rapidpago.solicitud_modulo c ON (b.smodu_codig = c.smodu_codig)
          WHERE a.estat_codig not in (0,27)
          GROUP BY estat_codig
          ORDER BY 1 desc,estat_codig desc";
    $datos=$conexion->createCommand($sql)->queryAll();
    
    $i=21;
    foreach ($datos as $key => $dato) {
        $grafico['value'][]=$dato['cantidad'];
        $grafico['label'][]=$dato['estat_descr'];
    }
   


?>

<script type="text/javascript">

    $(document).ready(function () {
         var ctx1 = document.getElementById("chart1").getContext("2d");
            var data1 = {
                labels: <?php echo json_encode($grafico1['label']); ?>,
                datasets: [
                    {
                        label: "Iniciado",
                        fillColor: "rgba(84,117,237,0)",
                        strokeColor: "rgba(84,117,237,1)",
                        pointColor: "rgba(84,117,237,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(243,245,246,0.9)",
                        data: <?php echo json_encode($grafico1['0']); ?>
                    },
                    {
                        label: "A Tiempo",
                        fillColor: "rgba(0,194,146,0)",
                        strokeColor: "rgba(0,194,146,0.9)",
                        pointColor: "rgba(0,194,146,0.9)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(152,235,239,1)",
                        data:<?php echo json_encode($grafico1['1']); ?>
                    },
                    {
                        label: "Demorado",
                        fillColor: "rgba(241,196,31,0)",
                        strokeColor: "rgba(241,196,31,0.8)",
                        pointColor: "rgba(241,196,31,0.8)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(243,245,246,0.9)",
                        data: <?php echo json_encode($grafico1['2']); ?>
                    },
                    {
                        label: "Critico",
                        fillColor: "rgba(237,64,64,0)",
                        strokeColor: "rgba(237,64,64,0.7)",
                        pointColor: "rgba(237,64,64,7)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(152,235,239,1)",
                        data: <?php echo json_encode($grafico1['3']); ?>
                    }
                    
                ]
            };
            
            var chart1 = new Chart(ctx1).Line(data1, {
                scaleShowGridLines : true,
                scaleGridLineColor : "rgba(0,0,0,.005)",
                scaleGridLineWidth : 0,
                scaleShowHorizontalLines: true,
                scaleShowVerticalLines: true,
                bezierCurve : true,
                bezierCurveTension : 0.4,
                pointDot : true,
                pointDotRadius : 4,
                pointDotStrokeWidth : 1,
                pointHitDetectionRadius : 2,
                datasetStroke : true,
                tooltipCornerRadius: 2,
                datasetStrokeWidth : 2,
                datasetFill : true,
                legendTemplate : "<ul class=\"<\x25=name.toLowerCase()\x25>-legend\"><\x25 for (var i=0; i<datasets.length; i++){\x25><li><span style=\"background-color:<\x25=datasets[i].strokeColor\x25>\"></span><\x25if(datasets[i].label){\x25><\x25=datasets[i].label\x25><\x25}\x25></li><\x25}\x25></ul>",
                responsive: true
            });
        });
</script>   
<script type="text/javascript">

    $(document).ready(function () {
         var ctx1 = document.getElementById("chart2").getContext("2d");
            var data1 = {
                labels: <?php echo json_encode($grafico2['label']); ?>,
                datasets: [
                    {
                        label: "Iniciado",
                        fillColor: "rgba(84,117,237,0)",
                        strokeColor: "rgba(84,117,237,1)",
                        pointColor: "rgba(84,117,237,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(243,245,246,0.9)",
                        data: <?php echo json_encode($grafico2['0']); ?>
                    },
                    {
                        label: "A Tiempo",
                        fillColor: "rgba(0,194,146,0)",
                        strokeColor: "rgba(0,194,146,0.9)",
                        pointColor: "rgba(0,194,146,0.9)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(152,235,239,1)",
                        data:<?php echo json_encode($grafico2['1']); ?>
                    },
                    {
                        label: "Demorado",
                        fillColor: "rgba(241,196,31,0)",
                        strokeColor: "rgba(241,196,31,0.8)",
                        pointColor: "rgba(241,196,31,0.8)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(243,245,246,0.9)",
                        data: <?php echo json_encode($grafico2['2']); ?>
                    },
                    {
                        label: "Critico",
                        fillColor: "rgba(237,64,64,0)",
                        strokeColor: "rgba(237,64,64,0.7)",
                        pointColor: "rgba(237,64,64,7)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(152,235,239,1)",
                        data: <?php echo json_encode($grafico2['3']); ?>
                    }
                    
                ]
            };
            
            var chart1 = new Chart(ctx1).Line(data1, {
                scaleShowGridLines : true,
                scaleGridLineColor : "rgba(0,0,0,.005)",
                scaleGridLineWidth : 0,
                scaleShowHorizontalLines: true,
                scaleShowVerticalLines: true,
                bezierCurve : true,
                bezierCurveTension : 0.4,
                pointDot : true,
                pointDotRadius : 4,
                pointDotStrokeWidth : 1,
                pointHitDetectionRadius : 2,
                datasetStroke : true,
                tooltipCornerRadius: 2,
                datasetStrokeWidth : 2,
                datasetFill : true,
                legendTemplate : "<ul class=\"<\x25=name.toLowerCase()\x25>-legend\"><\x25 for (var i=0; i<datasets.length; i++){\x25><li><span style=\"background-color:<\x25=datasets[i].strokeColor\x25>\"></span><\x25if(datasets[i].label){\x25><\x25=datasets[i].label\x25><\x25}\x25></li><\x25}\x25></ul>",
                responsive: true
            });
        });
</script>   

<script type="text/javascript">
    $( document ).ready(function() {
    
   
    var ctx4 = document.getElementById("chart4").getContext("2d");
    var data4 = [
        <?php
            $sql="SELECT count(a.estat_codig) cantidad, b.estat_codig, b.estat_descr
                  FROM rapidpago.rd_preregistro a
                  JOIN rapidpago.rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                  WHERE a.estat_codig not in (0,4)

                  GROUP BY estat_codig
                  UNION
                  SELECT count(a.estat_codig) cantidad, b.estat_codig, b.estat_descr
                  FROM rapidpago.solicitud a
                  JOIN rapidpago.rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                  WHERE a.estat_codig <> 0
                  GROUP BY a.estat_codig";
            $datos=$conexion->createCommand($sql)->queryAll();
            
            $i=21;
            foreach ($datos as $key => $dato) {
                
                $color = '#'.substr(md5($i), 0, 6);
                $grafico['value']=$dato['cantidad'];
                $grafico['color']=$color;
                $grafico['highlight']=$color;
                $grafico['label']=$dato['estat_descr'];
                if($i>21){
                    echo ',';    
                }
                echo json_encode($grafico);
                $i++;
            }

        ?>
    ];
    
    var myDoughnutChart = new Chart(ctx4).Doughnut(data4,{
        segmentShowStroke : true,
        segmentStrokeColor : "#fff",
        segmentStrokeWidth : 0,
        animationSteps : 100,
        tooltipCornerRadius: 2,
        animationEasing : "easeOutBounce",
        animateRotate : true,
        animateScale : false,
        legendTemplate : "<ul class=\"<\x25=name.toLowerCase()\x25>-legend\"><\x25 for (var i=0; i<segments.length; i++){\x25><li><span style=\"background-color:<\x25=segments[i].fillColor\x25>\"></span><\x25if(segments[i].label){\x25><\x25=segments[i].label\x25><\x25}\x25></li><\x25}\x25></ul>",
        responsive: true,
        onAnimationComplete: function () {
            this.showTooltip(this.segments, true);
        },
    });
    $("#chart4").on('mouseleave', function (){
        myDoughnutChart.showTooltip(myDoughnutChart.segments, true);
    });
    
});
</script>

<script type="text/javascript">
    $( document ).ready(function() {
    
   
    var ctx4 = document.getElementById("chart5").getContext("2d");
    var data4 = [
        <?php
            $sql="
                  SELECT count(a.orige_codig) cantidad,b.orige_codig, b.orige_descr
                  FROM rapidpago.solicitud a
                  JOIN rapidpago.p_solicitud_origen b ON (a.orige_codig = b.orige_codig)
                  WHERE a.estat_codig <> 0
                  GROUP BY orige_codig
                  ORDER BY orige_codig;";
            $datos=$conexion->createCommand($sql)->queryAll();
            
            $i=21;
            foreach ($datos as $key => $dato) {
                
                $color = '#'.substr(md5($i), 0, 6);
                $grafico['value']=$dato['cantidad'];
                $grafico['color']=$color;
                $grafico['highlight']=$color;
                $grafico['label']=$dato['orige_descr'];
                if($i>21){
                    echo ',';    
                }
                echo json_encode($grafico);
                $i++;
            }

        ?>
    ];
    
    var myDoughnutChart = new Chart(ctx4).Doughnut(data4,{
        segmentShowStroke : true,
        segmentStrokeColor : "#fff",
        segmentStrokeWidth : 0,
        animationSteps : 100,
        tooltipCornerRadius: 2,
        animationEasing : "easeOutBounce",
        animateRotate : true,
        animateScale : false,
        legendTemplate : "<ul class=\"<\x25=name.toLowerCase()\x25>-legend\"><\x25 for (var i=0; i<segments.length; i++){\x25><li><span style=\"background-color:<\x25=segments[i].fillColor\x25>\"></span><\x25if(segments[i].label){\x25><\x25=segments[i].label\x25><\x25}\x25></li><\x25}\x25></ul>",
        responsive: true,

        onAnimationComplete: function () {
            this.showTooltip(this.segments, true);
        },
    });
    
    
    $("#chart5").on('mouseleave', function (){
        myDoughnutChart.showTooltip(myDoughnutChart.segments, true);
    });
});
</script>

<script type="text/javascript">
    $( document ).ready(function() {
    
   
    var ctx7 = document.getElementById("chart7").getContext("2d");
    var data7 = [
        <?php
            $i=21;
            foreach ($relacion as $key => $dato) {
                
                $color = $dato['color'];
                $grafico['value']=$dato['canti'];
                $grafico['color']=$color;
                $grafico['highlight']=$color;
                $grafico['label']=$dato['descr'];
                if($i>21){
                    echo ',';    
                }
                echo json_encode($grafico);
                $i++;
            }

        ?>
    ];
    
    var myDoughnutChart = new Chart(ctx7).Doughnut(data7,{
        segmentShowStroke : true,
        segmentStrokeColor : "#fff",
        segmentStrokeWidth : 0,
        animationSteps : 100,
        tooltipCornerRadius: 2,
        animationEasing : "easeOutBounce",
        animateRotate : true,
        animateScale : false,
        legendTemplate : "<ul class=\"<\x25=name.toLowerCase()\x25>-legend\"><\x25 for (var i=0; i<segments.length; i++){\x25><li><span style=\"background-color:<\x25=segments[i].fillColor\x25>\"></span><\x25if(segments[i].label){\x25><\x25=segments[i].label\x25><\x25}\x25></li><\x25}\x25></ul>",
        responsive: true,

        onAnimationComplete: function () {
            this.showTooltip(this.segments, true);
        },
    });
    
    
    $("#chart7").on('mouseleave', function (){
        myDoughnutChart.showTooltip(myDoughnutChart.segments, true);
    });
});
</script>
<script type="text/javascript">
    $( document ).ready(function() {
    
   
    var ctx7 = document.getElementById("chart8").getContext("2d");
    var data7 = [
        <?php
            $i=21;
            foreach ($pentrega as $key => $dato) {
                
                $color = $dato['color'];
                $grafico['value']=$dato['canti'];
                $grafico['color']=$color;
                $grafico['highlight']=$color;
                $grafico['label']=$dato['descr'];
                if($i>21){
                    echo ',';    
                }
                echo json_encode($grafico);
                $i++;
            }

        ?>
    ];
    
    var myDoughnutChart = new Chart(ctx7).Doughnut(data7,{
        segmentShowStroke : true,
        segmentStrokeColor : "#fff",
        segmentStrokeWidth : 0,
        animationSteps : 100,
        tooltipCornerRadius: 2,
        animationEasing : "easeOutBounce",
        animateRotate : true,
        animateScale : false,
        legendTemplate : "<ul class=\"<\x25=name.toLowerCase()\x25>-legend\"><\x25 for (var i=0; i<segments.length; i++){\x25><li><span style=\"background-color:<\x25=segments[i].fillColor\x25>\"></span><\x25if(segments[i].label){\x25><\x25=segments[i].label\x25><\x25}\x25></li><\x25}\x25></ul>",
        responsive: true,

        onAnimationComplete: function () {
            this.showTooltip(this.segments, true);
        },
    });
    
    
    $("#chart8").on('mouseleave', function (){
        myDoughnutChart.showTooltip(myDoughnutChart.segments, true);
    });
});
</script>
<script type="text/javascript">
    $( document ).ready(function() {
    
   
        var ctx4 = document.getElementById("chart6").getContext("2d");
        var data4 = [
            <?php
                $sql="SELECT count(b.uesta_codig) cantidad, b.uesta_codig
                            FROM rd_acceso a 
                            JOIN seguridad_usuarios b ON (a.acces_corre = b.usuar_login)
                            WHERE uesta_codig in (1,2)
                            GROUP BY uesta_codig;";
                $datos=$conexion->createCommand($sql)->queryAll();
                
                $i=21;
                foreach ($datos as $key => $dato) {
                    if($dato['uesta_codig']=='2'){
                        $estatus='PENDIENTE';
                    }else{
                        $estatus='APROBADO';
                    }
                    $color = '#'.substr(md5($i), 0, 6);
                    $grafico['value']=$dato['cantidad'];
                    $grafico['color']=$color;
                    $grafico['highlight']=$color;
                    $grafico['label']=$estatus;
                    if($i>21){
                        echo ',';    
                    }
                    echo json_encode($grafico);
                    $i++;
                }

            ?>
        ];
        
        var myDoughnutChart = new Chart(ctx4).Doughnut(data4,{
            segmentShowStroke : true,
            segmentStrokeColor : "#fff",
            segmentStrokeWidth : 0,
            animationSteps : 100,
            tooltipCornerRadius: 2,
            animationEasing : "easeOutBounce",
            animateRotate : true,
            animateScale : false,
            legendTemplate : "<ul class=\"<\x25=name.toLowerCase()\x25>-legend\"><\x25 for (var i=0; i<segments.length; i++){\x25><li><span style=\"background-color:<\x25=segments[i].fillColor\x25>\"></span><\x25if(segments[i].label){\x25><\x25=segments[i].label\x25><\x25}\x25></li><\x25}\x25></ul>",
            responsive: true,

            onAnimationComplete: function () {
                this.showTooltip(this.segments, true);
            },
        });
        
        
        $("#chart6").on('mouseleave', function (){
            myDoughnutChart.showTooltip(myDoughnutChart.segments, true);
        });
    });
</script>
 <?php
        }
        ?> 