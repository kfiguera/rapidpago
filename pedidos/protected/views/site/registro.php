<?php
    $this->pageTitle = 'Registro | ' . Yii::app()->name ;
    $this->breadcrumbs = array(
        'Registro',
    );
?>
    <style type="text/css">
  .login-box{
    margin: 7vh auto;
  }
</style>
<div class="login-box" style="width: 80vw;">
    <div class="white-box">
        <form class="form-material" id="login-form" method="post">
            <h3 class="box-title m-b-20">Registro de Cliente </h3>
            <div class="row">
                <div class="col-sm-4 hide">
                    <div class="form-group ">
                        <input class="form-control" id="ndocu" name="ndocu" type="text" required="" placeholder="RUT">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group ">
                        <input class="form-control" id="pnomb" name="pnomb" type="text" required="" placeholder="Primer Nombre">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group ">
                        <input class="form-control" id="snomb" name="snomb" type="text" placeholder="Segundo Nombre">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group ">
                        <input class="form-control" id="papel" name="papel" type="text" required="" placeholder="Primer Apellido">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        <input class="form-control" id="sapel" name="sapel" type="text" placeholder="Segundo Apellido">
                    </div>
                </div>
                <div class="col-sm-4 hide">
                    <div class="form-group ">
                        <input class="form-control" id="tloca" name="tloca" type="text" required="" placeholder="Teléfono Local">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                        <input class="form-control" id="tcelu" name="tcelu" type="text" required="" placeholder="Teléfono Celular">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        <input class="form-control" id="corre" name="corre" type="text" required="" placeholder="Correo Electrónico">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                        <input class="form-control" id="ccorr" name="ccorr" type="text" required="" placeholder="Confirmar Correo Electrónico">
                    </div>
                </div>
                
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <textarea class="form-control" id="obser" name="obser" placeholder="Nombre de colegio o Agrupación"></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group text-center m-t-20">
                        <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" id="guardar" type="button">Registrarse</button>
                    </div>
                </div>
            </div>
            <div class="form-group m-b-0">
                <div class="col-sm-12 text-center">
                    <p>¿Ya Tienes Cuenta? <a href="login" class="text-primary m-l-5"><b>Inicia Sesión</b></a></p>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                /*ndocu: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "RUT" es obligatorio',
                        }
                    }
                },*/
                pnomb: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Primer Nombre" es obligatorio',
                        }
                    }
                },
                papel: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Primer Apellido" es obligatorio',
                        }
                    }
                },
                tloca: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Teléfono Local" es obligatorio',
                        },
                        numeric: {
                            message: 'Estimado(a) Usuario(a) el campo "Teléfono Local" debe ser un Número',
                        }
                    }
                },
                corre: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Correo Electrónico" es obligatorio',
                        },emailAddress: {
                            message: 'Estimado(a) Usuario(a) debe ingresar un correo electronico valido: ejemplo@smartwebtools.net'
                        }
                    }
                },
                ccorr: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Confirmar Correo Electrónico" es obligatorio',
                        },emailAddress: {
                            message: 'Estimado(a) Usuario(a) debe ingresar un correo electronico valido: ejemplo@smartwebtools.net'
                        },
                        identical: {
                            field: 'corre',
                            message: 'Estimado(a) Usuario(a) el campo "Correo Electrónico" y su confirmacion no son iguales'
                        }
                    }
                },
                tcelu: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Teléfono Celular" es obligatorio',
                        }
                    },
                        numeric: {
                            message: 'Estimado(a) Usuario(a) el campo "Teléfono Celular" debe ser un Número',
                        }
                }/*,
                obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Observaciones" es obligatorio',
                        }
                    }
                }*/
            }
        });
    });
</script>
<script type="text/javascript">
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
        $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'registro',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                    message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                              '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                });
                },
                success: function (response) {
                $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('login', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
    $('#tcelu').mask('+56ZZZZZZZZZZZ', {translation:  {'Z': {pattern: /[0-9]/, optional: true}}});
    });
</script>