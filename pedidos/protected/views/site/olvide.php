<?php

/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = Yii::app()->name . ' - Login';
$this->breadcrumbs = array(
    'Login',
);
?>
<?php
//Mostrar Errores del controlador
foreach (Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="alert alert-' . $key . '">' . $message . "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> </div>\n";
}
?> 

<div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
    <h1 class="titulo">¿Olvidaste tu contraseña?</h1>
    <div class="panel panel-danger" >


        <div style="padding-top:30px" class="panel-body" >

            <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'login-form',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                    'class' => 'form-horizontal',
                ),
            ));
            ?>

            <div style="margin-bottom: 25px" class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <?php echo CHtml::textField('username', '', array('class' => 'form-control', 'placeholder' => "Correo")); ?>
            </div

            <div style="margin-top:10px" class="form-group">
                <!-- Button -->
                <div class="row controls">
                    <div class="col-xs-12">
                        <div class="btn-group btn-group-justified" role="group" aria-label="...">

                            <div class="btn-group" role="group">
                                <button id="btn-login" type="button" class="btn-block btn btn-uven">Continuar  </button>

                    </div>
                            <div class="btn-group" role="group">
                                
                        <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>

                    </div>
                </div>
            </div>
            </div>

            <?php $this->endWidget(); ?>

        </div><!-- form -->
    </div>  
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#btn-login').click(function (e) {
            var formData = new FormData($("#login-form")[0]);
            e.preventDefault();
            $.ajax({
                dataType: "json",
                url: 'olvide',
                type: 'POST',
                cache: false,
                data: formData,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    //alert(response['success']);
                    if (response['success'] == 'true') {
                        bootbox.dialog({
                            message: response['msg'],
                            title: "Exito!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                    callback: function(){
                                       window.open('seguridad_usuarios', '_parent');
                                    }
                                },
                            }
                        });
                        $('.close').click( function(){
                            window.open('seguridad_usuarios?t=' + t, '_parent');
                        });
                    } else {

                        bootbox.dialog({
                            message: response['msg'],
                            title: "Información!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                },
                            }
                        });
                        setTimeout(function () {
                            $('.close').click();
                        }, 1000);
                        //alert('error');
                        // $("#subir").removeAttr('disabled');
                    }

                }
            });

        });
        

    });
</script>
