<?php
$this->pageTitle = Yii::app()->name . ' - Restaurar Contraseña';
$this->breadcrumbs = array(
    'Restaurar Contraseña',
);
?>
<div class="login-box">
    <div class="white-box">
      <form class="form-horizontal form-material" id="loginform" action="restaurar" method="post">
        <div class="form-group ">
          <div class="col-xs-12">
            <h3>Restaurar Contraseña</h3>
            <p class="text-muted">Ingresa tú nueva contraseña </p>
          </div>
        </div>
        <div class="form-group hide">
          <div class="col-xs-12">
            <input class="form-control" type="hidden" required="" name="c" value="<?php echo $usu['usuar_codig']?>" placeholder="Contraseña">
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" type="password" required="" name="contra" placeholder="Contraseña">
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" type="password" required="" name="ccontra" placeholder="Confirmar Contraseña">
          </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Continuar</button>
          </div>
        </div>
      </form>
    </div>
  </div>

<script>
    $(document).ready(function () {
        $('#loginform').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                
                contra: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Contraseña" es obligatorio',
                        },
                        identical: {
                            field: 'ccontra',
                            message: 'Estimado(a) Usuario(a) el campo "Contraseña" y su confirmacion no son iguales'
                        }
                    }
                },ccontra: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Confirmar Contraseña" es obligatorio',
                        },
                        identical: {
                            field: 'contra',
                            message: 'Estimado(a) Usuario(a) el campo "Contraseña" y su confirmacion no son iguales'
                        }
                    }
                }



            }
        });
    });
    $('#guardar').click(function () {
        $('#loginform').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            document.getElementById("login-form").submit();

        }
    });
</script>