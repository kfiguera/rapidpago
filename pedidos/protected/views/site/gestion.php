<?php
//Mostrar Errores del controlador
foreach (Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="alert alert-' . $key . '">' . $message . "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> </div>\n";
}
?> 
<div id="loginbox" style="margin-top:50px;" >
    <?php
    /* echo '<pre>';
      var_dump($model);
      echo '</pre>'; */
    ?>
    <div class="modal"></div>    
    <h1 class="titulo">Gestión de analistas</h1> 
    <div class="panel panel-danger " >


        <div style="padding-top:30px" class="panel-body" >

            <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>


            <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'subir-p_personas', 'htmlOptions' => array('method' => 'post', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')));
            ?>

            <table id="gestion" class="table table-bordered table-hover dataTable" style="width: 100%">
                <thead>
                    <tr>
                        <th width="2%">
                            #
                        </th>
                        <th>
                            Cédula
                        </th>
                        <th>
                            Nombre
                        </th>
                        <th >
                            Estatus
                        </th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>

                    <?php
                    $sql = "select *";
                    $sql.="FROM tusuario";
                    $sql.=" WHERE tgene_permi =' 3' AND organ_codig = '988' ";
                    $sql.="ORDER BY tpers_cedul ";
                    /* echo $sql;
                      exit(); */
                    $connection = Yii::app()->db;
                    $command = $connection->createCommand($sql);
                    $dataReader = $command->query();
                    while (($row = $dataReader->read()) !== false) {
                        $sql2 = "select * from tsaime where tpers_cedul ='" . $row['tpers_cedul'] . "'";
                        $result = Yii::app()->saime->createCommand($sql2)->query();
                        $p_persona = $result->read();
                        //var_dump($p_persona);
                        /* $nombre;
                          $i = 0;
                          while ($model[$i]) { */
                        //$datos = Tsaime::model()->find('tpers_cedul = ' . $model[$i]);
                        //$porcentaje = Tafiliacion::model()->find('tpers_cedul = \'' . $datos['tpers_cedul'] . '\'');
                        ?><tr>
                            <td ><?php echo $i + 1 ?></td>
                            <td ><?php echo $p_persona['tpers_nacio'] . '-' . $p_persona['tpers_cedul'] ?></td>
                            <td ><?php echo $p_persona['tpers_pnomb'] . ' ' . $p_persona['tpers_papel'] ?></h4></td>
                            <td ><?php
                                if ($row['tusua_statu'] == '1') {
                                    echo 'Activo';
                                } else {
                                    echo 'Inactivo';
                                }
                                ?>
                            </td>
                            <td>
                                <?php
                                if ($row['tusua_statu'] == '1') {
                                    ?>
                                <button aprobar="true" rd_preregistro_estatus="2" cedula='<?php echo $p_persona['tpers_cedul']; ?>' class="btn-block btn btn-danger" type="button"><i class="fa fa-close"></i>
                                    Desactivar</button>
                                        <?php
                                } else {
                                    ?>
                                     <button aprobar="true" rd_preregistro_estatus="1" class="btn btn-block btn-success" type="button" cedula='<?php echo $p_persona['tpers_cedul']; ?>' ><i class="fa fa-check"></i>
                                    Activar</button>
                                    <?php
                                }
                                ?>
                                
                                
                            </td>
                            <!--td> 
                                <a href="modal?cedula=<?php echo $row['tpers_cedul']; ?>" data-remote="false" data-toggle="modal" data-target="#modal" class="btn btn-info btn-block">

                                    <i class="glyphicon glyphicon-info-sign"></i> Información</a></td>

                            <td> <button aprobar="true" rd_preregistro_estatus="2" class="btn btn-block btn-success" type="button" archivo='<?php echo $nombre ?>' cedula='<?php echo $row['tpers_cedul']; ?>' ><i class="fa fa-check"></i>
                                    Aprobar</button></td>
                            <td><button aprobar="true" rd_preregistro_estatus="3"  archivo='<?php echo $nombre ?>' cedula='<?php echo $row['tpers_cedul']; ?>' class="btn-block btn btn-danger" type="button"><i class="fa fa-close"></i>
                                    Rechazar</button></td-->
                        </tr><?php
                        $i++;
                    }
                    ?>
                </tbody>
            </table>
            <?php $this->endWidget(); ?>

        </div><!-- form -->
    </div>  
</div>

<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function () {
        $('#gestion').DataTable();
    });
</script>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('button[aprobar="true"]').click(function (e) {
            e.preventDefault();
            aux = $(this);
            
            var cedula = $(this).attr('cedula');
            var rd_preregistro_estatus = $(this).attr('estatus');
            $.ajax({
                dataType: "json",
                url: 'actualizarUsuario?cedula=' + cedula + '&estatus=' + rd_preregistro_estatus,
                type: 'get',
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    //alert(response['success']);
                    if (response['success'] == 'true') {
                        alert(response['msg']);/*
                        setTimeout(function () {
                            $('.close').click();
                        }, 1000);*/


                       
                        window.open('gestion', '_parent');
                    } else {

                        bootbox.dialog({
                            message: response['msg'],
                            title: "Información!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                },
                            }
                        });
                        setTimeout(function () {
                            $('.close').click();
                        }, 1000);
                        //alert('error');
                        // $("#subir").removeAttr('disabled');
                    }

                }
            });

        });

    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>