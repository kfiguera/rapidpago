<div class="modal fade" id="verificar_telefono" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Código Teléfono</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="form-group">
                                <input type="text" name="correo-telefono"  id="correo-telefono" class="form-control text-center" placeholder="0-0-0-0">
                        </div>
                        <div class="form-group">
                                <input type="text" name="codigo-telefono"  id="codigo-telefono" class="form-control text-center" placeholder="0-0-0-0">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            <button  id='si' type="button" class="btn btn-uven" data-dismiss="modal" >Aceptar</button>
            </div>
        </div>
    </div>
</div>
<script>
$('#codigo-telefono').mask('9-9-9-9');
</script>