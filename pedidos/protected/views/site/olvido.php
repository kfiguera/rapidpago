<?php
$v=$row['usuar_codig'].$row['usuar_login'].$row['usuar_fcrea'];
$c=$row['usuar_codig'];
$v=md5($v);
$ruta=Yii::app()->getBaseUrl(true).'/site/completar?c='.$c.'&v='.$v;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Polerones Tiempo | Restablecer Contraseña</title>
</head>
<body style="margin:0px; background: #f8f8f8; ">
<div width="100%" style="background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;">
  <div style="max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px">
      <tbody>
        <tr>
          <td style="vertical-align: top; padding-bottom:30px;" align="center">
            <img src="https://autogestion.rapidpago.com/assets/img/logo.png" width="350px" style="border:none"></td>
        </tr>
      </tbody>
    </table>
    <div style="padding: 40px; background: #fff;">
      <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tbody>
          
          <tr>
            <td style="border-bottom:1px solid #f6f6f6;"><h1 style="font-size:14px; font-family:arial; margin:0px; font-weight:bold;">Estimado(a) <?php echo $row['perso_pnomb'].' '.$row['perso_papel'];?>,</h1>
              <p style="margin-top:0px; color:#bbbbbb; text-align: justify">Aquí están las instrucciones para restablecer tu contraseña.</p></td>
          </tr>
          <tr>
            <td style="padding:10px 0 30px 0;"><p style="text-align: justify">Se ha realizado una solicitud para restablecer su contraseña. Si no hizo esta solicitud, simplemente ignore este correo electrónico. Si realizó esta solicitud, restablezca su contraseña:</p>
              <center>
                <a href="<?php echo $ruta; ?>" style="display: inline-block; padding: 11px 30px; margin: 20px 0px 30px; font-size: 15px; color: #fff; background: #5475ed; border-radius: 60px; text-decoration:none;">Restablecer la contraseña</a>
              </center>
               </td>
          </tr>
          <tr>
            <td  style="border-top:1px solid #f6f6f6; padding-top:20px; color:#777">
				Si el botón de arriba no funciona, intente copiar y pegar la URL en su navegador. Si continúa teniendo problemas, no dude en contactarnos a ventas@rapidpago.com</td>
          </tr>
        </tbody>
      </table>
    </div>
    <div style="text-align: center; font-size: 12px; color: #b2b2b5; margin-top: 20px">
      <p> <a href="https://Innova Technology.net">Elaborado por Innova Technology</a> <p>
    </div>
  </div>
</div>
</body>
</html>