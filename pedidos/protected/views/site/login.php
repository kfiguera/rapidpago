<?php
$this->pageTitle = Yii::app()->name . ' - Login';
$this->breadcrumbs = array(
    'Login',
);
?>
<style type="text/css">
  .login-box{
    margin: 0 auto;

  }
</style>
    <div class="login-box">
      <div class="white-box">
        <div class="m-b-20 ">
            <img src='https://autogestion.rapidpago.com/assets/img/logo.png' class='img-responsive center-block' style="max-width: 200px">
        </div>
      <?php
        $form = $this->beginWidget('CActiveForm', array('id' => 'loginform', 'htmlOptions'=>array(
                          'class'=>'form-horizontal form-material',
                        ),'enableClientValidation' => true, 'clientOptions' => array('validateOnSubmit' => true)));


        
        
        if(strlen($form->error($model,'mensaje'))!='80'){

          echo '<div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              '.$form->error($model,'mensaje').' </div>';
        }
        //Mostrar mensajes del controlador
        foreach (Yii::app()->user->getFlashes() as $key => $message) {
          echo '<div class="alert alert-' . $key . ' alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              ' . $message  .' </div>';          
        }

        
      ?>
        <h3 class="box-title m-b-20">Iniciar Sesión </h3>
        <div class="form-group ">
          <div class="col-xs-12">
            <?php echo $form->textField($model, 'username', array('class' => 'form-control', 'placeholder' => "Usuario")); ?>
            <?php //echo $form->error($model, 'username', array('class' => 'help-block' )); ?>

          </div>
        </div>
        <div class="form-group">
          <div class="col-xs-12">
            <?php echo $form->passwordField($model, 'password', array('class' => 'form-control', 'placeholder' => "Contraseña")); ?>
            <?php //echo $form->error($model, 'password', array('class' => 'help-block' )); ?>

          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <div class="checkbox checkbox-primary pull-left p-t-0">
              <input id="checkbox-signup" type="checkbox">
              <label for="checkbox-signup"> Recuerdame </label>
            </div>
            <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> ¿Olvidaste tu Contraseña?</a>
          </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Ingresar</button>
          </div>
        </div>
       <div class="form-group m-b-0 hide">
          <div class="col-sm-12 text-center">
            <p>¿No tienes cuenta? <a href="registro" class="text-primary m-l-5"><b>Registrate</b></a></p>
          </div>
        </div> 
      <?php $this->endWidget(); ?>
      <form class="form-horizontal" id="recoverform" action="olvide" method="post">
        <div class="form-group ">
          <div class="col-xs-12">
            <h3>Recuperar Contraseña</h3>
            <p class="text-muted">Ingresa tu Correo y te enviaremos instrucciones! </p>
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" type="text" required="" name="correo" placeholder="Correo">
          </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Recuperar</button>
          </div>
        </div>
      </form>
    </div>
  </div>
