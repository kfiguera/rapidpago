<?php $conexion = Yii::app()->db; ?>
<style type="text/css">
    td, th {
      white-space: normal !important; 
      word-wrap: break-word;  
    }
    table {
        table-layout: fixed;
        word-wrap: break-word;
    }
</style>
<div class="sttabs tabs-style-linebox">
    <nav>
      <ul>
        <li class="tab-current">
            <a href="#section-linebox-1">
                <span>DATOS DEL COMERCIO</span>
            </a>
        </li>
        <li class="">
            <a href="#section-linebox-2">
                <span>DATOS DE AFILIACIÓN</span>
            </a>
        </li>
        <!--li class="">
            <a href="#section-linebox-3">
                <span>CONFIGURACIÓN DEL EQUIPO</span>
            </a>
        </li-->

        <li class="">
            <a href="#section-linebox-4">
                <span>REPRESENTANTES LEGALES</span>
            </a>
        </li>

        <li class="">
            <a href="#section-linebox-5">
                <span>DATOS ADICIONALES</span>
            </a>
        </li>
        <li class="">
            <a href="#section-linebox-6">
                <span>DOCUMENTOS DIGITALES</span>
            </a>
        </li>
        
      </ul>
    </nav>
    <?php
    $sql="SELECT * FROM cliente
            WHERE clien_codig='".$solicitud['clien_codig']."'";
    $cliente=$conexion->createCommand($sql)->queryRow();
    $sql="SELECT * FROM cliente_direccion
            WHERE clien_codig='".$solicitud['clien_codig']."'";
    $direccion=$conexion->createCommand($sql)->queryRow();
    $sql="SELECT * FROM p_actividad_comercial
            WHERE acome_codig='".$cliente['acome_codig']."'";
    $acomercial=$conexion->createCommand($sql)->queryRow();
    $sql="SELECT * FROM p_estados
            WHERE estad_codig='".$direccion['estad_codig']."'";
    $p_estados=$conexion->createCommand($sql)->queryRow();
    $sql="SELECT * FROM p_municipio
            WHERE munic_codig='".$direccion['munic_codig']."'";
    $p_municipio=$conexion->createCommand($sql)->queryRow();
    $sql="SELECT * FROM p_parroquia
            WHERE parro_codig='".$direccion['parro_codig']."'";
    $p_parroquia=$conexion->createCommand($sql)->queryRow();
    $sql="SELECT * FROM p_ciudades
            WHERE ciuda_codig='".$direccion['ciuda_codig']."'";
    $ciudad=$conexion->createCommand($sql)->queryRow();
    $sql="SELECT * FROM solicitud_entrega WHERE solic_codig='".$solicitud['solic_codig']."'";
    $entrega=$conexion->createCommand($sql)->queryRow();
    $sql="SELECT * FROM p_courier WHERE couri_codig='".$entrega['couri_codig']."'";
    
    $courier=$conexion->createCommand($sql)->queryRow();


    $sql="SELECT * FROM solicitud_eliminacion WHERE solic_codig='".$solicitud['solic_codig']."'";
    
    $eliminacion=$conexion->createCommand($sql)->queryRow();
    ?>
    <div class="content-wrap text-center">
        <section id="section-linebox-1" class="content-current">
            <table class="table table-bordered table-hover">
                <tr>
                    <th class="col-sm-2">RIF del Comercio * </th>
                    <td class="col-sm-2"><?php echo $cliente['clien_rifco']?></td>
                    <th class="col-sm-2">Razon Social* </th>
                    <td class="col-sm-2"><?php echo $cliente['clien_rsoci']?></td>
                    <th class="col-sm-2">Nombre Fantasia o Comercial *</th>
                    <td class="col-sm-2"><?php echo $cliente['clien_nfant']?></td>
                </tr>
                <tr>
                    <th>Actividad Comercial * </th>
                    <td><?php echo $acomercial['acome_descr']?></td>
                    <th>Teléfono Movil * </th>
                    <td><?php echo $cliente['clien_tmovi']?></td>
                    <th>Teléfono Fijo</th>
                    <td><?php echo $cliente['clien_tfijo']?></td>
                </tr>
                <tr>
                    <th>Estado *  </th>
                    <td><?php echo $p_estados['estad_descr']; ?></td>
                    <th>Municipio * </th>
                    <td><?php echo $p_municipio['munic_descr']; ?></td>
                    <th>Parroquia *</th>
                    <td><?php echo $p_parroquia['parro_descr']; ?></td>
                </tr>
                <tr>
                    <th>Ciudad *  </th>
                    <td><?php echo $ciudad['ciuda_descr']; ?></td>
                    <th>Zona Postal *  </th>
                    <td><?php echo $direccion['direc_zpost']; ?></td>
                    <th>Correo Electronico * </th>
                    <td><?php echo $solicitud['solic_celec']; ?></td>
                </tr>
                <tr>
                    <th>Dirección Fiscal  *</th>
                    <td colspan="5"><?php echo $direccion['direc_dfisc']; ?></td>
                </tr>
            </table>
            <div class="row">
                <?php
                    if($entrega['tentr_codig']=='2'){
                ?>
                    <div class="col-sm-12">
                        <h4 class="text-center">DATOS DEL ENVIO</h4>
                        <table class="table table-bordered table-hover" width="100%" style="border-collapse: collapse;" cellpadding="5">
                            <thead>
                                <tr>
                                    <th width="5%"># </th>
                                    <th width="19%">Courier </th>
                                    <th width="19%">Agencia</th>
                                    <th width="19%">Direción</th> 
                                    <th width="19%">Guia</th>    
                                </tr>    
                            </thead>
                            <tbody>
                            <?php
                            $i=1;
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $courier['couri_descr']; ?></td>
                                    <td><?php echo $entrega['entre_agenc']; ?></td>
                                    <td><?php echo $entrega['entre_direc']; ?></td>
                                    <td><?php echo $entrega['entre_nguia']; ?></td>
                                </tr>
                                <?php                ?>
                            </tbody>
                        </table>                           
                    </div>
                <?php
                    }
                ?>
            </div>
            <div class="row">
                <?php
                    if($eliminacion){
                ?>
                    <div class="col-sm-12">
                        <h4 class="text-center">Motivo de Eliminacion</h4>
                        <table class="table table-bordered table-hover" width="100%" style="border-collapse: collapse;" cellpadding="5">
                            <thead>
                                <tr>
                                    <th width="5%"># </th>
                                    <th width="80%">Motivo </th>
                                    <th width="15%">Fecha</th>
                                </tr>    
                            </thead>
                            <tbody>
                            <?php
                            $i=1;
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $eliminacion['selim_obser']; ?></td>
                                    <td><?php echo $this->transformarFecha_v($eliminacion['selim_fcrea']); ?></td>
                                    
                                </tr>
                                <?php                ?>
                            </tbody>
                        </table>                           
                    </div>
                <?php
                    }
                ?>
            </div>
        </section>
        <section id="section-linebox-2" class="">
            <div class="row">
                <div class="col-sm-12">  
                    <h4>Datos de la afiliación</h4>

                    <?php
                    $sql="SELECT * FROM p_banco 
                          WHERE banco_codig='".$solicitud['banco_codig']."'";
                    $banco=$conexion->createCommand($sql)->queryRow();
                    ?>

                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Banco Afiliado * </th>
                                <th>Código Afiliado * </th>
                                <th>Numero de Cuenta *</th>
                            </tr>                                
                        </thead>
                        <tbody>
                            <tr>
                                <td><?php echo $banco['banco_descr']; ?></td>
                                <td><?php echo $solicitud['solic_cafil']; ?> </td>
                                <td><?php echo $solicitud['solic_ncuen']; ?></td>
                            </tr>    
                        </tbody>
                    </table>
                </div>
                
            </div>
            <div class="row">
                <div class="col-sm-6">  
                    <?php
                    $sql="SELECT * FROM solicitud_equipos a
                          JOIN p_operador_telefonico b ON (a.otele_codig = b.otele_codig)
                          WHERE solic_codig='".$solicitud['solic_codig']."'";
                    $cequipo=$conexion->createCommand($sql)->queryAll();
                    ?>
                    <h4>Cantidad de equipos</h4>
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Operador Telefonico</th>
                                <th>Cantidad </th>
                            </tr>    
                        </thead>
                        <tbody>
                            <?php
                                foreach ($cequipo as $key => $value) {
                                   ?>
                                   <tr> 
                                        <td><?php echo $value['otele_descr']; ?></td>
                                        <td><?php echo $value['cequi_canti']; ?></td>
                                    </tr> 
                                   <?php 
                                   $total+=$value['cequi_canti'];
                                }
                            ?>
                             
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>TOTAL</th>
                                <th><?php echo $total?></th>
                            </tr>  
                        </tfoot>
                    </table>   
                </div>
            <?php
                $sql="SELECT * FROM solicitud_terminal a
                      JOIN solicitud b ON (a.solic_codig = b.solic_codig)
                      WHERE a.solic_codig='".$solicitud['solic_codig']."'";
                $terminal=$conexion->createCommand($sql)->queryAll();
                if($terminal){
            ?>
                <div class="col-sm-6">
                    <h4>Número de Terminal</h4>
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th width="5%"># </th>
                                <th width="47.5%">Número de Solicitud </th> 
                                <th width="47.5%">Número de Terminal </th> 
                            </tr>    
                        </thead>
                        <tbody>
                        <?php
                        $i=0;
                        foreach ($terminal as $key => $value) {
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $value['solic_numer']; ?></td>
                                <td><?php echo $value['termi_numer']; ?></td>
                            </tr>
                            <?php
                        }
                    ?>
                        </tbody>
                    </table>                           
                </div>
            
            <?php
                }
            ?>

            </div>
            <div class="row">

            <?php
                $sql="SELECT * FROM solicitud_asignacion a
                      WHERE solic_codig='".$solicitud['solic_codig']."'";
                $asignacion=$conexion->createCommand($sql)->queryAll();
                if($asignacion){
            ?>
                <div class="col-sm-12">
                    <h4>Equipos Asignados</h4>
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th width="5%"># </th>
                                <th width="19%">Dispositivo </th>
                                <th width="19%">Modelo</th>
                                <th width="19%">Serial</th> 
                                <th width="19%">Operador Telefónico</th>    
                                <th width="19%">Serial SIM</th>    
                            </tr>    
                        </thead>
                        <tbody>
                        <?php
                        $i=0;
                        foreach ($asignacion as $key => $value) {
                            $i++;
                            $sql="SELECT * 
                                  FROM inventario_seriales a
                                  JOIN inventario_modelo b ON (a.model_codig = b.model_codig)
                                  JOIN inventario c ON (a.inven_codig = c.inven_codig)
                                  WHERE a.seria_codig='".$value['asign_dispo']."'";
                            $dispo=$conexion->createCommand($sql)->queryRow();

                            $sql="SELECT * 
                                  FROM inventario_seriales a
                                  JOIN inventario_modelo b ON (a.model_codig = b.model_codig)
                                  JOIN inventario c ON (a.inven_codig = c.inven_codig)
                                  WHERE a.seria_codig='".$value['asign_opera']."'";
                            $opera=$conexion->createCommand($sql)->queryRow();
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $dispo['inven_descr']; ?></td>
                                <td><?php echo $dispo['model_descr']; ?></td>
                                <td><?php echo $dispo['seria_numer']; ?></td>
                                <td><?php echo $opera['model_descr']; ?></td>
                                <td><?php echo $opera['seria_numer']; ?></td>
                            </tr>
                            <?php
                        }
                    ?>
                        </tbody>
                    </table>                           
                </div>
            <?php
                }
            ?>
            </div>            
           
            
        <!--/section>
        <section id="section-linebox-3" class=""-->
                        
        </section>
        <section id="section-linebox-4" class="">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>RIF</th>
                        <th>Nombre </th>
                        <th>Tipo de Documento</th>
                        <th>Número Documento</th>
                        <th>Cargo</th>
                        <th>Teléfono Movil</th>
                        <th>Teléfono Fijo</th>
                        <th>Correo Electrónico</th>
                    </tr>    
                </thead>
                <tbody>
                     <?php
                        $sql="SELECT * FROM cliente_representante a
                              JOIN p_documento_tipo b ON (a.tdocu_codig = b.tdocu_codig)
                              WHERE clien_codig='".$cliente['clien_codig']."'";

                        $rlegal=$conexion->createCommand($sql)->queryAll();
                        ?>
                    <?php
                        foreach ($rlegal as $key => $value) {
                           ?>
                           <tr> 
                                <td><?php echo $value['rlega_rifrl']; ?></td>
                                <td><?php echo $value['rlega_nombr'].' '.$value['rlega_apell']; ?></td>
                                <td><?php echo $value['tdocu_descr']; ?></td>
                                <td><?php echo $value['rlega_ndocu']; ?></td>
                                <td><?php echo $value['rlega_cargo']; ?></td>
                                <td><?php echo $value['rlega_tmovi']; ?></td>
                                <td><?php echo $value['rlega_tfijo']; ?></td>
                                <td><?php echo $value['rlega_corre']; ?></td>

                            </tr> 
                           <?php 
                           $total+=$value['cequi_canti'];
                        }
                    ?>
                       
                </tbody>
            </table>               
        </section>
        <section id="section-linebox-5" class="">
             <?php
            $sql="SELECT * FROM p_solicitud_origen
                  WHERE orige_codig='".$solicitud['orige_codig']."'";
            $origen=$conexion->createCommand($sql)->queryRow();
            $vip= array('1' => 'SI', '2' => 'NO');
            ?>
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Fecha de Soliciud</th>
                        <th>Origen </th>
                        <th>Cliente VIP</th>
                        <th>Observaciones</th>
                    </tr>    
                </thead>
                <tbody>
                    <tr>
                        <td><?php echo $this->transformarFecha_v($solicitud['solic_fsoli'])?></td>
                        <td><?php echo $origen['orige_descr']?></td>
                        <td><?php echo $vip[$solicitud['solic_clvip']]?></td>
                        <td><?php echo $solicitud['solic_obser']?></td>
                    </tr>    
                </tbody>
            </table>

            <?php
            $sql="SELECT * FROM solicitud_factura
                  WHERE solic_codig='".$solicitud['solic_codig']."'";
            $factura=$conexion->createCommand($sql)->queryRow();
            if($factura){
            ?>
            <h4>Factura</h4>
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Número de Factura</th>
                        <th>Número de Control</th>
                        <th>Fecha</th>
                        <th>Observaciones</th>
                    </tr>    
                </thead>
                <tbody>
                    <tr>
                        
                        <td><?php echo $factura['factu_nfact']?></td>
                        <td><?php echo $factura['factu_ncont']?></td>
                        <td><?php echo $this->transformarFecha_v($factura['factu_fsoli'])?></td>
                        <td><?php echo $factura['factu_obser']?></td>
                    </tr>    
                </tbody>
            </table>     
            <?php } ?>          
        </section>
        <section id="section-linebox-6" class="">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>DOCUMENTO</th>
                        <th>VERIFICADO</th>
                        <th>VER</th>
                    </tr>    
                </thead>
                <tbody>
                    <?php

                        if($solicitud['orige_codig']==4){
                            $conexion=Yii::app()->db2;
                            $ruta='https://autogestion.rapidpago.com';
                            $sql="SELECT * FROM rd_preregistro_documento_digital a
                              JOIN rd_preregistro_documento_digital_tipo b ON (a.dtipo_codig = b.dtipo_codig)
                              WHERE prere_codig='".$solicitud['prere_codig']."'";
                            
                        }else{
                            $ruta=Yii::app()->request->getBaseUrl(true);
                            $sql="SELECT * FROM solicitud_documento a
                              JOIN rd_preregistro_documento_digital_tipo b ON (a.dtipo_codig = b.dtipo_codig)
                              WHERE solic_codig='".$solicitud['solic_codig']."'";
                        }
                        
                        $rlegal=$conexion->createCommand($sql)->queryAll();
                        ?>
                    <?php
                    $i=0;
                        foreach ($rlegal as $key => $value) {
                            $i++;
                           ?>
                           <tr> 
                                <td><?php echo $i; ?>
                                <td><?php echo $value['dtipo_descr']; ?>
                                <td><?php echo $vip[$value['docum_verif']]; ?>
                                <td><?php

                                    if($solicitud['orige_codig']==4){
                                        $file_exists=file_exists('../autogestion/'.$value['docum_ruta']);
                                    }else{
                                        $file_exists=file_exists($value['docum_ruta']);
                                    }
                                    if($file_exists and $value['docum_ruta']!='')
                                    {
                                    ?> 
                                        <a class="btn btn-info btn-block" href="<?php echo $ruta ?>/<?php echo $value['docum_ruta']; ?>" target="_blank"><i class="fa fa-search"></i></td>
                                    <?php
                                    }else{
                                    ?> 
                                        <a class="btn btn-info btn-block disabled" href="#" ><i class="fa fa-search"></i></td>
                                    <?php
                              
                                    }     
                                ?>
                                </td>
                            </tr> 
                           <?php 
                           $total+=$value['cequi_canti'];
                        }
                    ?>    
                </tbody>
            </table>               
        </section>
    </div><!-- /content -->
</div>