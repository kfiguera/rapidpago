<?php
    $conexion=Yii::app()->db;
?>
<style>
    .image-preview-input {
        position: relative;
        overflow: hidden;
        margin: 0px;    
        color: #333;
        background-color: #fff;
        border-color: #ccc;    
    }
    .image-preview-input input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }
    .image-preview-input-title {
        margin-left:2px;
    }
    .sweet-alert {
        padding-top: 25px;
        padding-bottom: : 25px;
        padding-right: 0px;
        padding-left: 0px;
    }
</style>  
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Costos Adicionales</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Recepción de Dispositivos</a></li>
            <li><a href="#">Costos Adicionales</a></li>
            <li class="active">Registrar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Datos del Recepción de Dispositivos</h3>
        </div>
        <div class="panel-body" >
           
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
                <div class="row hide">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Código *</label>
                            <?php echo CHtml::textField('codig', $c, array('class' => 'form-control', 'placeholder' => "Código")); ?>
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Pedido *</label>
                            <?php echo CHtml::textField('pedid', $pedidos['pedid_numer'], array('class' => 'form-control', 'placeholder' => "Pedido",'disabled'=>'true')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Cantidad de modelos *</label>
                            <?php echo CHtml::textField('mcant', $this->funciones->transformarMonto_v($totales['modelos'],0), array('class' => 'form-control', 'placeholder' => "Cantidad del Modelo",'readonly'=>'true')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Total a Bordar *</label>
                            <?php echo CHtml::textField('total', $this->funciones->transformarMonto_v($totales['p_personas'],0), array('class' => 'form-control', 'placeholder' => "Total a Bordar",'readonly'=>'true')); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Monto del Pedido</label>
                            <?php echo CHtml::textField('montop',  $this->funciones->transformarMonto_v($totales['monto'],0), array('class' => 'form-control', 'placeholder' => "Monto del Pedido",'readonly'=>'true')); ?>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Ver Pedido</label>
                            <a href="../../reportes/pedidos/pdf?c=<?php echo $pedidos['pedid_codig'] ?>" target="_blank" class="btn btn-block btn-info"><i class="fa fa-file-pdf-o"></i></a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                    </div>
                </div>
        </div><!-- form -->
    </div>  
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Costos Adicionales</h3>
        </div>
        <div class="panel-body" >
           
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row hide">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Código *</label>
                            <?php echo CHtml::textField('codig', $c, array('class' => 'form-control', 'placeholder' => "Código")); ?>
                        </div>
                    </div>
                    
                </div>
                
                <div class="row">
                    <div class="col-sm-8">        
                        <div class="form-group">
                            <label>Descripción *</label>
                            <?php 
                            $sql="SELECT * FROM pedido_costo ";
                            $result=$conexion->createCommand($sql)->queryAll();
                            $data=CHtml::listData($result,'costo_codig','costo_descr');

                                echo CHtml::dropDownList('costo', '', $data, array('class' => 'form-control', 'placeholder' => "Descripción",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Monto *</label>
                            <?php 
                                echo CHtml::textField('monto', '', array('class' => 'form-control', 'placeholder' => "Monto",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    
                </div>  
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observación *</label>
                            <?php 
                            
                            echo CHtml::textArea('obser', '', array('class' => 'form-control', 'placeholder' => "Observación",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    
                </div>  
                
                <!-- Button -->
                <div class="row controls">
                   
                    <div class="col-sm-6 ">
                        <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                    </div>
                    <div class="col-sm-6 ">
                        <button id="guardar" type="button" class="btn-block btn btn-info">Guardar  </button>
                    </div>
                </div>

            </form>

        </div><!-- form -->
    </div>  
</div>

<div class="row">   
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Listado</h3>
        </div>
        <div class="panel-body" >
            
            <div class="row">
                <div class="col-sm-12 table-responsive" id='listado-pedido'>
                    <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                        <thead>
                            <tr>
                                <th width="2%">#</th>
                                <th>Descripción</th> 
                                <th>Monto</th>
                                <th>Observación</th> 
                                <th width="5%">Eliminar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sql = "SELECT *
                                    FROM pedido_pedidos_adicional a
                                    JOIN pedido_costo b ON (a.costo_codig = b.costo_codig)
                                    WHERE a.pedid_codig='".$c."'";


                            $command = $conexion->createCommand($sql);
                            $p_persona = $command->query();
                            $i=0;
                            while (($row = $p_persona->read()) !== false) {
                                $i++;
                                $ajustes=$this->funciones->VerAjusteTalla(json_decode($row['pdlis_tajus']));
                            ?>
                            <tr>
                                <td class="tabla"><?php echo $i ?></td>
                                <td class="tabla"><?php echo $row['costo_descr']; ?></td>
                                <td class="tabla"><?php echo $this->funciones->transformarMonto_v($row['adici_monto'],0); ?></td>
                                <td class="tabla"><?php echo $row['adici_obser']; ?></td>
                                <td class="tabla"><a href="CostosEliminar?p=<?php echo $row['pedid_codig'] ?>&c=<?php echo $row['adici_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-close"></i></a></td>
                            </tr>
                            <?php
                                }   
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div> 
</div>
 <script type="text/javascript">
    $(document).ready(function () {
        $('#monto').mask('#.##0',{reverse: true,maxlength:false});
    });
</script>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                costo: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Descripción" es obligatorio',
                        }
                    }
                },
                monto: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Monto" es obligatorio',
                        }
                    }
                },
            }
        });
    });
</script>
<script type="text/javascript">
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        var data = new FormData(jQuery('form')[0]);
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                url: 'costos',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('costos?c=<?php echo $c; ?>', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>
<script type="text/javascript">
    function Paso2Listado() {
        $.ajax({
            url: 'Paso2Listado?p=<?php echo $p ?>&c=<?php echo $c ?>',
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            beforeSend: function () {
                $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
            },
            success: function (response) {
                    $('#wrapper').unblock();
                $("#listado-pedido").html(response);
            }
        });
    }
    function Siguiente(){

        var data = new FormData(jQuery('form')[0]);
        $.ajax({
            dataType: "json",
            url: 'paso2',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            beforeSend: function () {
                $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
            },
            success: function (response) {
                    $('#wrapper').unblock();
                if (response['success'] == 'true') {
                    $("#guardar").removeAttr('disabled');
                    /*window.open('modificar?p=<?php echo $p; ?>&s=3&c=<?php echo $c; ?>', '_parent');*/
                    /*swal({ 
                        title: "Exito!",
                        text: response['msg'],
                        type: "success",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-info"
                    },function(){
                        window.open('listado?p=<?php echo $p; ?>', '_parent');
                    });*/
                    swal({   
                        title: "Exito!",   
                        text: response['msg'],
                        type: "success",   
                        showCancelButton: true,   
                        confirmButtonText: "Finalizar Pedido",   
                        cancelButtonText: "Agregar otro Modelo",   
                        closeOnConfirm: false,   
                        closeOnCancel: false 
                    }, function(isConfirm){   
                        if (isConfirm) {     
                            window.open('../pedidos/modificar?c=<?php echo $p; ?>&s=1', '_parent');   
                        } else {     
                            window.open('registrar?p=<?php echo $p; ?>', '_parent');   
                        } 
                    });
                } else {
                    swal({ 
                        title: "Error!",
                        text: response['msg'],
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"
                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                }

            },error:function (response) {
                    $('#wrapper').unblock();
                swal({ 
                    title: "Error!",
                    text: "Error el ejecutar la operación",
                    type: "error",
                    confirmButtonText: "Cerrar",
                    confirmButtonClass: "btn-danger"

                },function(){
                    $("#guardar").removeAttr('disabled');
                });
                    
            }
        });
        
    }
</script>
<script type="text/javascript">
    $('#siguiente').click(function () { 
        var data = new FormData(jQuery('form')[0]);
        $.ajax({
            dataType: "json",
            url: 'paso2',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            beforeSend: function () {
                $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
            },
            success: function (response) {
                    $('#wrapper').unblock();
                if (response['success'] == 'true') {
                    $("#guardar").removeAttr('disabled');
                    /*window.open('modificar?p=<?php echo $p; ?>&s=3&c=<?php echo $c; ?>', '_parent');*/
                    /*swal({ 
                        title: "Exito!",
                        text: response['msg'],
                        type: "success",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-info"
                    },function(){
                        window.open('listado?p=<?php echo $p; ?>', '_parent');
                    });*/
                    swal({   
                        title: "Exito!",   
                        text: response['msg'],
                        type: "success",   
                        showCancelButton: true,   
                        confirmButtonText: "Finalizar Pedido",   
                        cancelButtonText: "Agregar otro Modelo",   
                        closeOnConfirm: false,   
                        closeOnCancel: false 
                    }, function(isConfirm){   
                        if (isConfirm) {     
                            window.open('../pedidos/modificar?c=<?php echo $p; ?>&s=1', '_parent');   
                        } else {     
                            window.open('registrar?p=<?php echo $p; ?>', '_parent');   
                        } 
                    });
                } else {
                    swal({ 
                        title: "Error!",
                        text: response['msg'],
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"
                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                }

            },error:function (response) {
                    $('#wrapper').unblock();
                swal({ 
                    title: "Error!",
                    text: "Error el ejecutar la operación",
                    type: "error",
                    confirmButtonText: "Cerrar",
                    confirmButtonClass: "btn-danger"

                },function(){
                    $("#guardar").removeAttr('disabled');
                });
                    
            }
        });
        Siguiente();
    });
</script>
<script type="text/javascript">
    $('#timag_0').change(function () {
        var timag = $(this).val();
        if(timag==null || timag=='' || timag=='4'){
            jQuery("#opcional-3_0").addClass('hide');
            jQuery("#opcional-2_0").addClass('hide');
            jQuery("#opcional-1_0").addClass('hide');
            jQuery("#image_0").attr('disabled','true');
            jQuery("#emoji_0").attr('disabled','true');    
            jQuery("#simbo_0").attr('disabled','true');    
        }else if(timag=='1'){
            jQuery("#opcional-1_0").removeClass('hide');
            jQuery("#opcional-2_0").addClass('hide');
            jQuery("#opcional-3_0").addClass('hide');
            jQuery("#emoji_0").removeAttr('disabled');
            jQuery("#image_0").attr('disabled','true');    
            jQuery("#simbo_0").attr('disabled','true'); 
        }else if(timag=='3'){
            jQuery("#opcional-3_0").removeClass('hide');
            jQuery("#opcional-2_0").addClass('hide');
            jQuery("#opcional-1_0").addClass('hide');
            jQuery("#simbo_0").removeAttr('disabled');
            jQuery("#image_0").attr('disabled','true');    
            jQuery("#emoji_0").attr('disabled','true'); 
        }else {
            jQuery("#opcional-2_0").removeClass('hide');
            jQuery("#opcional-1_0").addClass('hide');
            jQuery("#opcional-3_0").addClass('hide');
            jQuery("#image_0").removeAttr('disabled');
            jQuery("#emoji_0").attr('disabled','true');
            jQuery("#simbo_0").attr('disabled','true'); 
        }
               
    });
</script>
<script type="text/javascript">
    $('#ideta').change(function () {
        var ideta = $(this).val();
        switch(ideta) {
            case '1':
                jQuery("#iclin").removeAttr('disabled');
                break;
            default:
                jQuery("#iclin").attr('disabled','true');
                break;
        }
                
            
    });
</script>
<script>

$(document).on('click', '#close-preview', function () {
    $('.numero1_0 .image-preview').popover('hide');
    // Hover befor close the preview
    $('.numero1_0 .image-preview').hover(
        function () {
            $('.image-preview').popover('hide');
        },
        function () {
            $('.image-preview').popover('hide');
        }
    );
});
$(function () {
    // Create the close button
    var closebtn = $('<button/>', {
        type: "button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    $('.numero1_0 .image-preview').popover({
        trigger:'manual',
        html:true,
        title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
        content: "No hay imagen",
        placement:'top'
    });



    
    // Clear event
    $('.numero1_0 .image-preview-clear').click(function () {
        $('.numero1_0 .image-preview').attr("data-content", "").popover('hide');
        $('.numero1_0 .image-preview-filename').val("");
        $('.numero1_0 .image-preview-clear').hide();
        $('.numero1_0 .image-preview-input input:file').val("");
        $(".numero1_0 .image-preview-input-title").text("Buscar");
    });
    // Create the preview image
    $(".numero1_0 .image-preview-input input:file").change(function () {
        var img = $('<img/>', {
            id: 'dynamic',
            width: 250,
            height: 200
        });
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".numero1_0 .image-preview-input-title").text("Cambiar");
            $(".numero1_0 .image-preview-clear").show();
            $(".numero1_0 .image-preview-filename").val(file.name);
            img.attr('src', e.target.result);
            $(".numero1_0 .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
            }
        reader.readAsDataURL(file);
    });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#login_0').popover({
          html: true,
          content: function() {
            return $('#popover-img_0').html();
          },
          trigger: 'hover'
        });
});
$('#emoji_0').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/Recepción de DispositivosEmoji'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img_0").html(data);  
            }  
        });
        $('#login_0').popover('hide');
    });
$(document).ready(function(){
    $('#login-2_0').popover({
          html: true,
          content: function() {
            return $('#popover-img-2_0').html();
          },
          trigger: 'hover'
        });
});
$('#simbo_0').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/Recepción de DispositivosSimbolo'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-2_0").html(data);  
            }  
        });
        $('#login-2_0').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#sa-params').click(function(){
        swal({   
            title: "Are you sure?",   
            text: "You will not be able to recover this imaginary file!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                swal("Deleted!", "Your imaginary file has been deleted.", "success");   
            } else {     
                swal("Cancelled", "Your imaginary file is safe :)", "error");   
            } 
        });
    });
 });
</script>

<!--script type="text/javascript">
  <script type="text/javascript">
    jQuery(document).ready(function(){
    jQuery('#emoji').popover({
    title: popoverContent,
    html: true,
    placement: 'right',
    trigger: 'hover'
    });
    });
</script>  

$('#emoji').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/Recepción de DispositivosEmoji'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:id},  
            success:function(data){  
                fetch_data = data;  
            }  
        });
        $('#emoji').popover({  
                title:fetch_data,  
                html:true,  
                trigger: 'focus', 
                placement:'right'  
           });       
    });
</script>
 <script>  
      $(document).ready(function(){  
           $('.emoji').popover({  
                title:fetchData,  
                html:true,  
                trigger: 'focus', 
                placement:'right'  
           });  
           function fetchData(){  
                var fetch_data = '';  
                var element = $(this);  
                var id = $(this).val();  
                $.ajax({  
                     url:"<?php echo CController::createUrl('funciones/Recepción de DispositivosEmoji'); ?>",  
                     method:"POST",  
                     async:false,  
                     data:{id:id},  
                     success:function(data){  
                          fetch_data = data;  
                     }  
                });  
                return fetch_data;  
           }  
      });  
 </script--> 
