<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Configuración</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Configuración</a></li>
            <li><a href="#"> Generar Notas de Entreha</a></li>
            <li class="active">Reversar Traslado</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
<div class="panel panel-default">
    
        <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'p_personas', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            $connection = Yii::app()->db;

        ?>
        
        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div> 
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Reversar Traslado</h3>
    </div>
    <div class="panel-body" >
        
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-p_persona'>
                <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                <input type="checkbox" name="select_all" value="1" id="example-select-all">
                            </th>
                            <th>
                                N° Traslado
                            </th>
                            <th>
                                Fecha
                            </th>
                            <th>
                                Almacen Origen
                            </th>
                            <th>
                                Destino
                            </th>
                            <th>
                                Motivo del Traslado
                            </th>
                            
                    
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "SELECT * FROM inventario_almacen";
                        
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                        ?>
                        
                        <tr>
                            

                            <td class="tabla"><input type="checkbox" name="id[<?php echo $i ?>]" value="<?php echo $row['ientr_codig'] ?>" id="id_<?php echo $i ?>"></td>
                            <td class="tabla"><?php echo $row['pedid_numer'] ?></td>
                            <td class="tabla"><?php echo "CLIENTE N° $i"//$row['coleg_nombr'] ?></td>
                            <td class="tabla"><?php echo "cliente_$i@rapidpago.com"//$row['usuar_login'] ?></td>
                            <td class="tabla"><?php echo $row['p_personas'] ?></td>
                            <td class="tabla"><?php echo $row['pedid_fcrea'] ?></td>
                            
                        </tr>
                        <?php
                            }  
                        ?>

                    </tbody>
                </table>
             </div>
             </div>
             </div>
             </div>   
                   
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Detalles del Reverso</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            
                <div class="row hide">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Código</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php 

                                    echo CHtml::hiddenField('codigo', $solicitud['prere_codig'], array('class' => 'form-control', 'placeholder' => "Código")); 
                                ?>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row " id='motivo'>
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Motivo</label>
                                <?php 
                                    $sql="SELECT *
                                          FROM p_motivo a
                                          ORDER BY 2";

                                    $result=$conexion->createCommand($sql)->queryAll();

                                    $data=CHtml::listData($result,'motiv_codig','motiv_descr');
                                    echo CHtml::dropDownList('motiv', '', $data, array('class' => 'form-control', 'prompt' => "Seleccione")); ?>

                        </div>
                    </div>
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observación</label>
                                <?php echo CHtml::textArea('obser','', array('class' => 'form-control', 'placeholder' => "Observación")); ?>

                        </div>
                    </div>
                </div>
                <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-4 ">
                            <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <button type="reset" class="btn-block btn btn-default">Limpiar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                        </div>
                    </div>
        </div><!-- form -->
    </div>  
</div>

<!-- Modal -->

<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#buscar').click(function () {
        var formData = new FormData($("#p_personas")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-p_persona').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>
<script>
$('#cantidad').mask('#.##0',{reverse: true,maxlength:false});
$('#punidad').mask('#.##0,00',{reverse: true,maxlength:false});

</script>
