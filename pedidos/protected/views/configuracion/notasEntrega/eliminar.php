<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Configuración</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <<li><a href="#">Configuración</a></li>
            <li><a href="#"> Generar Notas de Entreha</a></li>
            <li class="active">Eliminar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Datos de la Solicitud</h3>
        </div>
        <div class="panel-body">
            <?php 
                $this->renderPartial('../../registro/solicitudes/datos_solicitud',array('solicitud' => $solicitud));
            ?>
        </div>
    </div>  
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Datos de  Artículos 
                    <div class="panel-action">
                        <a href="#" data-perform="panel-collapse">
                            <i class="ti-plus"></i>
                        </a> 
                    </div>
            </h3>
                

            </div>
            <div class="panel-wrapper collapse in">

                <div class="panel-body" >
                    <input class="form-control" placeholder="Porcentaje" readonly="readonly" type="hidden" value="" name="bookindex" id="bookindex" /> 
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label>Modelo</label>
                                            <select class="form-control" name="serial" id="serial">
                                                <option value="">Seleccione...</option>
                                             </select>        
                                    </div>
                                    <div class="col-xs-3">
                                        <label>Serial</label>
                                            <select class="form-control" name="serial" id="serial">
                                                <option value="">Seleccione...</option>
                                             </select>        
                                    </div>
                                    <div class="col-xs-3">
                                        <label>Operados de Telefonia</label>
                                        <input class="form-control" placeholder="Cantidad" prompt="Seleccione" type="text" value="" name="cprod[0]" id="cprod_0" />        
                                    </div>
                                    <div class="col-xs-3">
                                        <label>Serial Tarjeta SIM</label>
                                        <input class="form-control" placeholder="Precio Unitario" prompt="Seleccione" type="text" value="" name="pprod[0]" id="pprod_0" />        
                                    </div>
                                    
                                </div>
                            </div>
  
                        </div>
                    </div>

             </div><!-- form -->
    </div>  
</div>
</div>

<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Verificar Rechazo</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            
                <div class="row hide">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Código</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php 

                                    echo CHtml::hiddenField('codigo', $solicitud['prere_codig'], array('class' => 'form-control', 'placeholder' => "Código")); 
                                ?>

                            </div>
                        </div>
                    </div>
                </div>
                

                <div class="row " id='motivo'>
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Motivo</label>
                                <?php 
                                    $sql="SELECT *
                                          FROM p_motivo a
                                          ORDER BY 2";

                                    $result=$conexion->createCommand($sql)->queryAll();

                                    $data=CHtml::listData($result,'motiv_codig','motiv_descr');
                                    echo CHtml::dropDownList('motiv', '', $data, array('class' => 'form-control', 'prompt' => "Seleccione")); ?>

                        </div>
                    </div>
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observación</label>
                                <?php echo CHtml::textArea('obser','', array('class' => 'form-control', 'placeholder' => "Observación")); ?>

                        </div>
                    </div>
                </div>
                <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-4 ">
                            <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <button type="reset" class="btn-block btn btn-default">Limpiar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                        </div>
                    </div>
        </div><!-- form -->
    </div>  
</div>
<script>
$('#fnaci').mask('00/00/0000');
$('#desde').mask('00/00/0000');
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                corre: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Correo" es obligatorio',
                        },
                        regexp: {
                            regexp: /^[A-Za-z0-9._%+-]+\@[a-zA-Z0-9.-]+\.[A-Za-z]{2,4}$/,
                            message: 'Estimado(a) Usuario(a) el campo "Correo" debe poseer el siguiente formato: ejemplo@gmail.com'
                        }, identical: {
                            field: 'ccorre',
                            message: 'Estimado(a) Usuario(a) el campo "Correo" y su confirmacion no son iguales'
                        }
                    }
                },ccorre: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Confirmar Correo" es obligatorio',
                        },
                        regexp: {
                            regexp: /^[A-Za-z0-9._%+-]+\@[a-zA-Z0-9.-]+\.[A-Za-z]{2,4}$/,
                            message: 'Estimado(a) Usuario(a) el campo "Confirmar Correo"  debe poseer el siguiente formato: ejemplo@gmail.com'
                        }, identical: {
                            field: 'corre',
                            message: 'Estimado(a) Usuario(a) el campo "Correo" y su confirmacion no son iguales'
                        }
                    }
                },contra: {
                    validators: {
                        /*notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Contraseña" es obligatorio',
                        },*/
                        identical: {
                            field: 'ccontra',
                            message: 'Estimado(a) Usuario(a) el campo "Contraseña" y su confirmacion no son iguales'
                        }
                    }
                },ccontra: {
                    validators: {
                        /*notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Confirmar Contraseña" es obligatorio',
                        },*/
                        identical: {
                            field: 'contra',
                            message: 'Estimado(a) Usuario(a) el campo "Contraseña" y su confirmacion no son iguales'
                        }
                    }
                },perso: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Persona" es obligatorio',
                        }
                    }
                },
                nivel: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Nivel" es obligatorio',
                        }
                    }
                },
                /*obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Observaciones" es obligatorio',
                        }
                    }
                },*/



            }
        });
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'eliminar',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>