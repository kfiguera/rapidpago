<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Configuración</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Configuración</a></li>
            <li><a href="#"> Generar Notas de Entreha</a></li>
            <li class="active">Generar Traslado</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
<div class="panel panel-default">
    
        <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'p_personas', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            $connection = Yii::app()->db;

        ?>
        
        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div> 
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Generar Traslados</h3>
    </div>
    <div class="panel-body" >
        
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-p_persona'>
                <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                <input type="checkbox" name="select_all" value="1" id="example-select-all">
                            </th>
                            <th>
                                N° Nota Entrega
                            </th>
                            <th>
                                Modelo del POS
                            </th>
                            <th>
                                Serial del POS
                            </th>
                            <th>
                                Doc. Origen
                            </th>
                            <th>
                                N° Doc.
                            </th>
                            <th>
                                Almacén
                            </th>
                            <th>
                                Cantidad
                            </th>
                            <th>
                                Unidad
                            </th>
                            <th>
                                neto
                            </th>
                    
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "SELECT * FROM inventario_almacen";
                        
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                        ?>
                        
                        <tr>
                            

                            <td class="tabla"><input type="checkbox" name="id[<?php echo $i ?>]" value="<?php echo $row['ientr_codig'] ?>" id="id_<?php echo $i ?>"></td>
                            <td class="tabla"><?php echo $row['pedid_numer'] ?></td>
                            <td class="tabla"><?php echo "CLIENTE N° $i"//$row['coleg_nombr'] ?></td>
                            <td class="tabla"><?php echo "cliente_$i@rapidpago.com"//$row['usuar_login'] ?></td>
                            <td class="tabla"><?php echo "cliente_$i@rapidpago.com"//$row['usuar_login'] ?></td>
                            <td class="tabla"><?php echo "cliente_$i@rapidpago.com"//$row['usuar_login'] ?></td>
                            <td class="tabla"><?php echo "cliente_$i@rapidpago.com"//$row['usuar_login'] ?></td>
                            <td class="tabla"><?php echo "cliente_$i@rapidpago.com"//$row['usuar_login'] ?></td>
                            <td class="tabla"><?php echo $row['p_personas'] ?></td>
                            
                            <!--td class="tabla"><a href="../detalle/listado?p=<?php echo $row['pedid_codig'] ?>" class="btn btn-block btn-info"><i class="glyphicon glyphicon-list-alt"></i></a></td-->
                            <td class="tabla"><?php echo "cliente_$i@rapidpago.com"//$row['usuar_login'] ?></td>
                        </tr>
                        <?php
                            }  
                        ?>

                    </tbody>
                </table>
            <div class="row">
            <div class="col-sm-6">
                <button type="button" id="buscar" class="btn btn-block btn-info">Continuar</button>
            </div>
             <div class="col-sm-6">
                <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
            </div>
        </div>
            </div>
        </div>
    </div>
</div>  

<!-- Modal -->

<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#buscar').click(function () {
        var formData = new FormData($("#p_personas")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-p_persona').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>
<script>
$('#cantidad').mask('#.##0',{reverse: true,maxlength:false});
$('#punidad').mask('#.##0,00',{reverse: true,maxlength:false});

</script>
