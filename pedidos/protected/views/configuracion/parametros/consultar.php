<?php $conexion = Yii::app()->db; ?>
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Configuración</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Carga de Parametros</a></li>
            <li class="active">Consultar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Consultar Carga de Parametros</h3>
        </div>
        <div class="panel-body">
            <?php 
                $this->funciones->imprimirDatosSolicitud($solicitud);
            ?>
        </div>
        <div class="panel-footer">
            <div class="row controls">
                <div class="col-sm-12">
                    <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                </div>
            </div>
        </div>
    </div>  
</div>

<script type="text/javascript">
$(document).ready(function(){
    var id = ['ccuer', 'cbols', 'cmang', 'cegor', 'cigor', 'cppre', 'ccier', 'ccurs', 'cnper', 'caesp', 'celec', 'cfras' ];
    var con = 1;
    id.forEach( function(valor, indice, array) {
        var numero = indice+1;
        $('#img-'+numero).popover({
          html: true,
          content: function() {
            if(numero==1){
                return $('#popover-img').html();
            }else{
                return $('#popover-img-'+numero).html();    
            }
            
          },
          trigger: 'hover'
        });
        $('#'+valor).change(function () {
            var emoji = $(this).val();
            $.ajax({  
                url:"<?php echo CController::createUrl('funciones/Carga de ParametrosVerColor'); ?>",  
                method:"POST",  
                async:false,  
                data:{id:emoji},  
                success:function(data){  
                    if(numero=='1'){
                        $('#popover-img').html(data);
                    }else{
                        $('#popover-img-'+numero).html(data);    
                    } 
                }  
            });
            $('[data-toggle=popover]').popover('hide');
        });
    });
});
</script>


<script type="text/javascript">
$(document).ready(function(){
    $('#img-13').popover({
          html: true,
          content: function() {
            return $('#popover-img-13').html();
          },
          trigger: 'hover'
        });
});
$('#pfuen').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/Carga de ParametrosVerFuente'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-13").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-14').popover({
          html: true,
          content: function() {
            return $('#popover-img-14').html();
          },
          trigger: 'hover'
        });
});
$('#pcolo').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/Carga de ParametrosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-14").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-15').popover({
          html: true,
          content: function() {
            return $('#popover-img-15').html();
          },
          trigger: 'hover'
        });
});
$('#gfuen').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/Carga de ParametrosVerFuente'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-15").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-16').popover({
          html: true,
          content: function() {
            return $('#popover-img-16').html();
          },
          trigger: 'hover'
        });
});
$('#gcolo').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/Carga de ParametrosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-16").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
    $('#img-numero-1').popover({
          html: true,
          content: function() {
            return $('#popover-numero-1').html();
          },
          trigger: 'hover'
        });
});
</script>
<script type="text/javascript">
    $(document).ready(function(){
    $('#img-numero-2').popover({
          html: true,
          content: function() {
            return $('#popover-numero-2').html();
          },
          trigger: 'hover'
        });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-17').popover({
          html: true,
          content: function() {
            return $('#popover-img-17').html();
          },
          trigger: 'hover'
        });
});
$('#mfuen').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/Carga de ParametrosVerFuente'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-17").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-18').popover({
          html: true,
          content: function() {
            return $('#popover-img-18').html();
          },
          trigger: 'hover'
        });
});
$('#mcolo').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/Carga de ParametrosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-18").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-19').popover({
          html: true,
          content: function() {
            return $('#popover-img-19').html();
          },
          trigger: 'hover'
        });
});
$('#eafue').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/Carga de ParametrosVerFuente'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-19").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-20').popover({
          html: true,
          content: function() {
            return $('#popover-img-20').html();
          },
          trigger: 'hover'
        });
});
$('#eacol').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/Carga de ParametrosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-20").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-21').popover({
          html: true,
          content: function() {
            return $('#popover-img-21').html();
          },
          trigger: 'hover'
        });
});
$('#ebfue').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/Carga de ParametrosVerFuente'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-21").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-22').popover({
          html: true,
          content: function() {
            return $('#popover-img-22').html();
          },
          trigger: 'hover'
        });
});
$('#ebcol').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/Carga de ParametrosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-22").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-23').popover({
          html: true,
          content: function() {
            return $('#popover-img-23').html();
          },
          trigger: 'hover'
        });
});
$('#aefue').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/Carga de ParametrosVerFuente'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-23").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-24').popover({
          html: true,
          content: function() {
            return $('#popover-img-24').html();
          },
          trigger: 'hover'
        });
});
$('#aecol').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/Carga de ParametrosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-24").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
    $('#img-numero-3').popover({
          html: true,
          content: function() {
            return $('#popover-numero-3').html();
          },
          trigger: 'hover'
        });
});
</script>
<script type="text/javascript">
    $(document).ready(function(){
    $('#img-numero-4').popover({
          html: true,
          content: function() {
            return $('#popover-numero-4').html();
          },
          trigger: 'hover'
        });
});
</script>
<script type="text/javascript">
    $(document).ready(function(){
    $('#img-numero-6').popover({
          html: true,
          content: function() {
            return $('#popover-numero-6').html();
          },
          trigger: 'hover'
        });
});
</script>
<script type="text/javascript">
    $(document).ready(function(){
    $('#img-numero-5').popover({
          html: true,
          content: function() {
            return $('#popover-numero-5').html();
          },
          trigger: 'hover'
        });
});
</script>