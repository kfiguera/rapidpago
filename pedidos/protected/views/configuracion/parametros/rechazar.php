<?php $conexion = Yii::app()->db; ?>
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Configuracion</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Configuracion</a></li>
            <li><a href="#">Carga de Parametros</a></li>
            <li class="active">Rechazar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Datos de la Solicitud</h3>
        </div>
        <div class="panel-body">
            <?php 
                $this->funciones->imprimirDatosSolicitud($solicitud);
            ?>
        </div>
    </div>  
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Datos de Artículos 
                    <div class="panel-action">
                        <a href="#" data-perform="panel-collapse">
                            <i class="ti-plus"></i>
                        </a> 
                    </div>
            </h3>
                

            </div>
            <div class="panel-wrapper collapse in">

                <div class="panel-body" >
                <div class="row">
                    <div class="col-sm-12">  
                        <?php
                        $sql="SELECT * FROM solicitud_asignacion a
                              WHERE solic_codig='".$solicitud['solic_codig']."'";
                        $asignacion=$conexion->createCommand($sql)->queryAll();
                        if($asignacion){
                            ?>
                        <table id='auditoria'  class="table table-bordered table-hover dataTable">
                            <thead>
                                <tr>
                                    <th width="5%"># </th>
                                    <th width="19%">Dispositivo </th>
                                    <th width="19%">Modelo</th>
                                    <th width="19%">Serial</th> 
                                    <th width="19%">Operador Telefónico</th>    
                                    <th width="19%">Serial SIM</th>    
                                </tr>    
                            </thead>
                            <tbody>
                            <?php
                            $i=0;
                            foreach ($asignacion as $key => $value) {
                                $i++;
                                $sql="SELECT * 
                                      FROM inventario_seriales a
                                      JOIN inventario_modelo b ON (a.model_codig = b.model_codig)
                                      JOIN inventario c ON (a.inven_codig = c.inven_codig)
                                      WHERE a.seria_codig='".$value['asign_dispo']."'";
                                $dispo=$conexion->createCommand($sql)->queryRow();

                                $sql="SELECT * 
                                      FROM inventario_seriales a
                                      JOIN inventario_modelo b ON (a.model_codig = b.model_codig)
                                      JOIN inventario c ON (a.inven_codig = c.inven_codig)
                                      WHERE a.seria_codig='".$value['asign_opera']."'";
                                $opera=$conexion->createCommand($sql)->queryRow();
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $dispo['inven_descr']; ?></td>
                                    <td><?php echo $dispo['model_descr']; ?></td>
                                    <td><?php echo $dispo['seria_numer']; ?></td>
                                    <td><?php echo $opera['model_descr']; ?></td>
                                    <td><?php echo $opera['seria_numer']; ?></td>
                                </tr>
                                <?php
                            }
                        }
                        
                        ?>
                            </tbody>
                        </table>                           
                    </div>
                </div>
            </div>
 

        </div><!-- form -->
    </div>  
</div>
</div>
<form id='login-form' name='login-form' method="post">

<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Rechazar Solicitud</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            
                <div class="row hide">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Código</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php 

                                    echo CHtml::hiddenField('codigo', $solicitud['solic_codig'], array('class' => 'form-control', 'placeholder' => "Código")); 
                                ?>

                            </div>
                        </div>
                    </div>
                </div>
                
                
                <div class="row" id='motivo'>
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Motivo</label>
                                <?php 
                                    $sql="SELECT *
                                          FROM p_motivo a
                                          ORDER BY 2";

                                    $result=$conexion->createCommand($sql)->queryAll();

                                    $data=CHtml::listData($result,'motiv_codig','motiv_descr');
                                    echo CHtml::dropDownList('motiv', '', $data, array('class' => 'form-control', 'prompt' => "Seleccione")); ?>

                        </div>
                    </div>
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observación</label>
                                <?php echo CHtml::textArea('obser','', array('class' => 'form-control', 'placeholder' => "Observación")); ?>

                        </div>
                    </div>
                </div>
                <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-4 ">
                            <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                        </div>
                    </div>
        </div><!-- form -->
    </div>  
</div>
</form>
<script>
$('#punid').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#canti').mask('#.##0',{reverse: true,maxlength:false});
        $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                accio: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Acción" es obligatorio',
                        }
                    }
                },
                motiv: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Motivo" es obligatorio',
                        }
                    }
                },
                obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Observaciones" es obligatorio',
                        }
                    }
                },



            }
        });
    });

    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'rechazar',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#accio").change(function () {
            var accion = $(this).val();
            if(accion=='2'){
                $("#motivo").removeClass("hide");
                $("#motiv").removeAttr("disabled");
            }else{
                $("#motivo").addClass("hide");
                $("#motiv").attr("disabled","true");
            }
            
        });
    });
</script>