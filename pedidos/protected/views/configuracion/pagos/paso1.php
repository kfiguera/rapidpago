<?php $conexion = Yii::app()->db; ?>
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Detalle del Pedido</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Pedidos</a></li>
            <li><a href="#">Detalle</a></li>
            <li class="active">Registrar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <div class="row line-steps">
                  <div class="col-md-4 column-step <?php echo $ubicacion[0]; ?>">
                    <a href="modificar?c=<?php echo $solicitud['solic_codig']?>&s=1">
                        <div class="step-number">1 </div>
                    <div class="step-title">Pedido</div>
                    <div class="step-info">Detalles del Pedido</div>
                 </div>

                 <div class="col-md-4 column-step <?php echo $ubicacion[1]; ?> ">
                    <div class="step-number">2</div>
                    <div class="step-title">Adicional</div>
                    <div class="step-info">Información Adicional a Bordar Parte 1</div>
                 </div>
                 <div class="col-md-4 column-step <?php echo $ubicacion[2]; ?> ">
                    <div class="step-number">3</div>
                    <div class="step-title">Adicional</div>
                    <div class="step-info">Información Adicional a Bordar Parte 2</div>
                 </div>
              </div>
        </div>
    </div>
</div>
<form id='login-form' name='login-form' method="post">
<div class="row">   
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Detalles del Pedido</h3>
        </div>
        <div class="panel-body" >
           
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
                <div class="row hide">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Pedido *</label>
                            <?php 
                                echo CHtml::hiddenField('pedid', $p, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Código *</label>
                            <?php 
                                echo CHtml::hiddenField('codig', $c, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Paso *</label>
                            <?php 
                                echo CHtml::hiddenField('pasos', $pedidos['pedid_pasos'], array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color del Cuerpo *</label>
                            <div class="input-group">

                            <?php
                                $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                  FROM pedido_modelo_color a
                                  GROUP BY 1,2
                                  ORDER BY 1";

                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');

                                echo CHtml::dropDownList('ccuer', $pedidos['pedid_ccuer'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['1']['disabled'])); ?>
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-1" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span> 
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color de los Bolsillos *</label>
                            <div class="input-group">

                            <?php
                                $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                  FROM pedido_modelo_color a
                                  GROUP BY 1,2
                                  ORDER BY 1";

                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');

                                echo CHtml::dropDownList('cbols', $pedidos['pedid_cbols'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['1']['disabled'])); ?>
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-2" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span> 
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color de las Mangas *</label>
                            <div class="input-group">

                            <?php
                                $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                  FROM pedido_modelo_color a
                                  GROUP BY 1,2
                                  ORDER BY 1";

                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');

                                echo CHtml::dropDownList('cmang', $pedidos['pedid_cmang'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['1']['disabled'])); ?>
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-3" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span> 
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                if($pedidos['gorro']=='1'){
                    $gorro='';
                }else{
                    $gorro='true';
                }
                if($pedidos['preti']=='1'){
                    $preti='';
                }else{
                    $preti='true';
                }
                ?>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color Exterior del Gorro *</label>
                            <div class="input-group">

                            <?php
                                $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                  FROM pedido_modelo_color a
                                  GROUP BY 1,2
                                  ORDER BY 1";

                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');

                                echo CHtml::dropDownList('cegor', $pedidos['pedid_cegor'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['1']['disabled'],'disabled'=>$gorro)); ?>
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-4" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span> 
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color Interior del Gorro *</label>
                            <div class="input-group">

                            <?php
                                $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                  FROM pedido_modelo_color a
                                  GROUP BY 1,2
                                  ORDER BY 1";

                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');

                                echo CHtml::dropDownList('cigor', $pedidos['pedid_cigor'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['1']['disabled'],'disabled'=>$gorro)); ?>
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-5" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span> 
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color Puños y Pretina *</label>
                            <div class="input-group">

                            <?php
                                $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                  FROM pedido_modelo_color a
                                  GROUP BY 1,2
                                  ORDER BY 1";

                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');

                                echo CHtml::dropDownList('cppre', $pedidos['pedid_cppre'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['1']['disabled'],'disabled'=>$preti)); ?>
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-6" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span> 
                            </div>
                        </div>
                        
                    
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Cierre *</label>
                            <?php 
                                $data=array('1'=>'SI','2'=>'NO');
                                echo CHtml::dropDownList('cierr', $pedidos['pedid_cierr'], $data, array('ajax' => array(
                                            'type' => 'POST',
                                            'url' => CController::createUrl('funciones/PedidosOpcionales'), // Controlador que devuelve las p_ciudades relacionadas
                                            'update' => '#iopci', // id del item que se actualizará
                                        ),'class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color Cierre *</label>
                            <div class="input-group">

                            <?php
                                $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                  FROM pedido_modelo_color a
                                  GROUP BY 1,2
                                  ORDER BY 1";

                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');

                                echo CHtml::dropDownList('ccier', $pedidos['pedid_ccier'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['1']['disabled'])); ?>
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-7" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span> 
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Broches *</label>
                            <?php 

                                $data=array('1'=>'SIN BROCHE','2'=>'CRUDO','3'=>'GRIS','4'=>'BLANCO');
                                echo CHtml::dropDownList('broch', $pedidos['pedid_broch'], $data, array('ajax' => array(
                                            'type' => 'POST',
                                            'url' => CController::createUrl('funciones/PedidosOpcionales'), // Controlador que devuelve las p_ciudades relacionadas
                                            'update' => '#iopci', // id del item que se actualizará
                                        ),'class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                </div>
            </div><!-- form -->
    </div>  
</div>
<div class="row">   
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Color Letras</h3>
        </div>
        <div class="panel-body" >
           
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color Curso *</label>
                            <div class="input-group">

                            <?php
                                $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                  FROM pedido_modelo_color a
                                  GROUP BY 1,2
                                  ORDER BY 1";

                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');

                                echo CHtml::dropDownList('ccurs', $pedidos['pedid_ccurs'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['1']['disabled'])); ?>
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-8" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span> 
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color Nombre Personalizado *</label>
                            <div class="input-group">

                            <?php
                                $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                  FROM pedido_modelo_color a
                                  GROUP BY 1,2
                                  ORDER BY 1";

                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');

                                echo CHtml::dropDownList('cnper', $pedidos['pedid_cnper'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['1']['disabled'])); ?>
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-9" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span> 
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color Apodo Espalda *</label>
                            <div class="input-group">

                            <?php
                                $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                  FROM pedido_modelo_color a
                                  GROUP BY 1,2
                                  ORDER BY 1";

                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');

                                echo CHtml::dropDownList('caesp', $pedidos['pedid_caesp'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['1']['disabled'])); ?>
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-10" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Color Electivos *</label>
                            <div class="input-group">

                            <?php
                                $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                  FROM pedido_modelo_color a
                                  GROUP BY 1,2
                                  ORDER BY 1";

                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');

                                echo CHtml::dropDownList('celec', $pedidos['pedid_celec'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['1']['disabled'])); ?>
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-11" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span> 
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Color Frase *</label>
                            <div class="input-group">

                            <?php
                                $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                  FROM pedido_modelo_color a
                                  GROUP BY 1,2
                                  ORDER BY 1";

                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');

                                echo CHtml::dropDownList('cfras', $pedidos['pedid_cfras'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['1']['disabled'])); ?>
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-12" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span> 
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Otros</label>
                            <?php echo CHtml::textArea('otros', $pedidos['pedid_mensa'], array('class' => 'form-control', 'placeholder' => "Mensaje")); ?>
                        </div>
                    </div>
                </div>
            </div><!-- form -->
    </div>  
</div>
<div class="row">   
    <div class="panel panel-default">
        <div class="panel-body" >
                <!-- Button -->
                <div class="row controls">
                    <div class="col-sm-4 ">
                        <a href="../detalle/listado?p=<?php echo $c?>" type="reset" class="btn-block btn btn-default">Volver </a>
                    </div>
                    <div class="col-sm-4 ">
                        <button type="reset" class="btn-block btn btn-default">Limpiar  </button>
                    </div>
                    <div class="col-sm-4 ">
                        <button id="guardar" type="button" class="btn-block btn btn-info">Siguiente  </button>
                    </div>
                </div>


        </div><!-- form -->
    </div>  
</div>
</form>
<div class="hide" id="popover-img">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_ccuer']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive"></div>
    <div class="hide" id="popover-img-2">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_cbols']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive"></div>
    <div class="hide" id="popover-img-3">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_cmang']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive"></div>
    <div class="hide" id="popover-img-4">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_cegor']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive"></div>
    <div class="hide" id="popover-img-5">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_cigor']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive"></div>
    <div class="hide" id="popover-img-6">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_cppre']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive"></div>
    <div class="hide" id="popover-img-7">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_ccier']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive"></div>
    <div class="hide" id="popover-img-8">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_ccurs']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive"></div>
    <div class="hide" id="popover-img-9">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_cnper']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive"></div>
    <div class="hide" id="popover-img-10">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_caesp']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive"></div>
    <div class="hide" id="popover-img-11">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_celec']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive"></div>
    <div class="hide" id="popover-img-12">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_cfras']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive"></div>

<script>
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                ccuer: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color del Cuerpo" es obligatorio',
                        }
                    }
                },
                cbols: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color de los Bolsillos" es obligatorio',
                        }
                    }
                },
                cmang: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color de las Mangas" es obligatorio',
                        }
                    }
                },
                cegor: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color Exterior del Gorro" es obligatorio',
                        }
                    }
                },
                cigor: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color Interior del Gorro" es obligatorio',
                        }
                    }
                },
                cppre: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color Puños y Pretina" es obligatorio',
                        }
                    }
                },
                cierr: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Cierre" es obligatorio',
                        }
                    }
                },
                ccier: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color Cierre" es obligatorio',
                        }
                    }
                },
                broch: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Broches" es obligatorio',
                        }
                    }
                },
                ccurs: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color Curso" es obligatorio',
                        }
                    }
                },
                cnper: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color Nombre Personalizado" es obligatorio',
                        }
                    }
                },
                caesp: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color Apodo Espalda" es obligatorio',
                        }
                    }
                },
                celec: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color Electivos" es obligatorio',
                        }
                    }
                },
                cfras: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color Frase" es obligatorio',
                        }
                    }
                },
                
            }
        });
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'paso1',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        /*swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){*/
                            window.open('modificar?c=<?php echo $c; ?>&s=2', '_parent');
                        /*});*/
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>
<script type="text/javascript">
    $('#opcio').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '1':
                jQuery("#iopci").removeAttr('disabled');
                break;
            default:
                jQuery("#opcional-3").addClass('hide');
                jQuery("#opcional-2").addClass('hide');
                jQuery("#opcional-1").addClass('hide');
                jQuery("#iopci").html('');
                jQuery("#iclin").html('');
                jQuery("#iccie").html('');
                jQuery("#icviv").html('');
                jQuery("#iopci").attr('disabled','true');
                jQuery("#ideta").attr('disabled','true');
                jQuery("#iclin").attr('disabled','true');
                jQuery("#iccie").attr('disabled','true');
                jQuery("#icviv").attr('disabled','true');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#iopci').change(function () {
        var iopci = $(this).val();
        $.ajax({
            'type':'POST',
            'data':{'tmode':tmode.value},
            'url':'<?php echo CController::createUrl('funciones/PedidosColorOpcional'); ?>',
            'cache':false,
            'success':function(html){
                switch(iopci) {
                    case '1':
                        
                        jQuery("#opcional-1").removeClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#iclin").html(html);
                        jQuery("#iccie").html('');
                        jQuery("#icviv").html('');
                        jQuery("#ideta").removeAttr('disabled');
                        jQuery("#iclin").removeAttr('disabled');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").attr('disabled','true');

                        break;
                    case '2':
                        jQuery("#opcional-2").removeClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#iclin").html('');
                        jQuery("#iccie").html(html);
                        jQuery("#icviv").html('');
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").removeAttr('disabled');
                        jQuery("#icviv").attr('disabled','true');
                        break;
                    case '3':
                        jQuery("#opcional-3").removeClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#iclin").html('');
                        jQuery("#iccie").html('');
                        jQuery("#icviv").html(html);
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").removeAttr('disabled');
                        break;
                    default:
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#iclin").html(html);
                        jQuery("#iccie").html(html);
                        jQuery("#icviv").html(html);
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").attr('disabled','true');
                        break;
                }
                
            }
        });
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    var id = ['ccuer', 'cbols', 'cmang', 'cegor', 'cigor', 'cppre', 'ccier', 'ccurs', 'cnper', 'caesp', 'celec', 'cfras' ];
    var con = 1;
    id.forEach( function(valor, indice, array) {
        var numero = indice+1;
        $('#img-'+numero).popover({
          html: true,
          content: function() {
            if(numero==1){
                return $('#popover-img').html();
            }else{
                return $('#popover-img-'+numero).html();    
            }
            
          },
          trigger: 'hover'
        });
        $('#'+valor).change(function () {
            var emoji = $(this).val();
            $.ajax({  
                url:"<?php echo CController::createUrl('funciones/PedidosVerColor'); ?>",  
                method:"POST",  
                async:false,  
                data:{id:emoji},  
                success:function(data){  
                    if(numero=='1'){
                        $('#popover-img').html(data);
                    }else{
                        $('#popover-img-'+numero).html(data);    
                    } 
                }  
            });
            $('[data-toggle=popover]').popover('hide');
        });
    });
});
</script>
<script type="text/javascript">
    $('#ideta').change(function () {
        var ideta = $(this).val();
        switch(ideta) {
            case '1':
                jQuery("#iclin").removeAttr('disabled');
                break;
            default:
                jQuery("#iclin").attr('disabled','true');
                break;
        }
                
            
    });
</script>
<!--script type="text/javascript">
$(document).ready(function(){
    $('#img-1').popover({
          html: true,
          content: function() {
            return $('#popover-img').html();
          },
          trigger: 'hover'
        });
});
$('#color').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-2').popover({
          html: true,
          content: function() {
            return $('#popover-img-2').html();
          },
          trigger: 'hover'
        });
});
$('#iclin').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-2").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-3').popover({
          html: true,
          content: function() {
            return $('#popover-img-3').html();
          },
          trigger: 'hover'
        });
});
$('#iccie').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-3").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-4').popover({
          html: true,
          content: function() {
            return $('#popover-img-4').html();
          },
          trigger: 'hover'
        });
});
$('#icviv').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-4").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script-->