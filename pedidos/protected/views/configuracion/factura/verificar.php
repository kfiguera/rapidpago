<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Factura</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Configuración</a></li>
            <li><a href="#">Factura</a></li>
            <li class="active">Verificar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php
                $conexion=Yii::app()->db;
            ?>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Datos de la Solicitud</h3>
        </div>
        <div class="panel-body">
            <div class="sttabs tabs-style-linebox">
            <nav>
              <ul>
                <li class="tab-current">
                    <a href="#section-linebox-1">
                        <span>DATOS DEL COMERCIO</span>
                    </a>
                </li>
                <li class="">
                    <a href="#section-linebox-2">
                        <span>DATOS DE AFILIACIÓN</span>
                    </a>
                </li>
                <li class="">
                    <a href="#section-linebox-3">
                        <span>CONFIGURACIÓN DEL EQUIPO</span>
                    </a>
                </li>

                <li class="">
                    <a href="#section-linebox-4">
                        <span>REPRESENTANTES LEGALES</span>
                    </a>
                </li>

                <li class="">
                    <a href="#section-linebox-5">
                        <span>DATOS ADICIONALES</span>
                    </a>
                </li>
                <li class="">
                    <a href="#section-linebox-6">
                        <span>DOCUMENTOS DIGITALES</span>
                    </a>
                </li>
                
              </ul>
            </nav>
            <div class="content-wrap text-center">
                <section id="section-linebox-1" class="content-current">
                    <table class="table table-bordered table-hover">
                        <tr>
                            <th>RIF del Comercio * </th>
                            <td>J-00000000-0</td>
                            <th>Razon Social* </th>
                            <td>RAZON SOCIAL </td>
                            <th>Nombre Fantasia o Comercial *</th>
                            <td>NOMBRE DE FANTASIA</td>
                        </tr>
                        <tr>
                            <th>Actividad Comercial * </th>
                            <td>0001 - ACTIVIDAD COMERCIAL</td>
                            <th>Teléfono Movil * </th>
                            <td>0400 000.00.00 </td>
                            <th>Teléfono Fijo</th>
                            <td>0200 000.00.00</td>
                        </tr>
                        <tr>
                            <th>Estado *  </th>
                            <td>DISTRITO CAPITAL</td>
                            <th>Municipio * </th>
                            <td>LIBERTADOR</td>
                            <th>Parroquia *</th>
                            <td>EL RECREO</td>
                        </tr>
                        <tr>
                            <th>Ciudad *  </th>
                            <td>CARACAS</td>
                            <th>Correo Electronico * </th>
                            <td>EJEMPLO@RAPIDPAGO.COM</td>
                            <th>Dirección Fiscal  *</th>
                            <td>LA URBINA, CARCAS, VENEZUELA</td>
                        </tr>
                    </table>
                </section>
                <section id="section-linebox-2" class="">
                    <table class="table table-bordered table-hover">
                        <tr>
                            <th>Banco Afiliado * </th>
                            <td>BANCO DE VENEZUELA</td>
                        </tr>
                        <tr>
                            <th>Código Afiliado * </th>
                            <td>A123456789 </td>
                        </tr>
                        <tr>
                            <th>Numero de Cuenta *</th>
                            <td>01234567890123456789</td>
                        </tr>
                    </table>
                </section>
                <section id="section-linebox-3" class="">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Operador Telefonico</th>
                                <th>Cantidad </th>
                            </tr>    
                        </thead>
                        <tbody>
                            <tr>
                                <td>MOVISTAR</td>
                                <td>10</td>
                            </tr>
                            <tr>
                                <td>DIGITEL</td>
                                <td>10</td>
                            </tr>    
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>TOTAL</th>
                                <th>20</th>
                            </tr>  
                        </tfoot>
                    </table>               
                </section>
                <section id="section-linebox-4" class="">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>RIF</th>
                                <th>Nombre </th>
                                <th>Tipo de Documento</th>
                                <th>Número Documento</th>
                                <th>Cargo</th>
                                <th>Teléfono Movil</th>
                                <th>Teléfono Fijo</th>
                                <th>Correo Electrónico</th>
                            </tr>    
                        </thead>
                        <tbody>
                            <tr>
                                <td>V-24900845-8</td>
                                <td>KEVIN FIGUERA</td>
                                <td>CEDULA</td>
                                <td>24900845</td>
                                <td>PRESIDENTE</td>
                                <td>0200 000.00.00</td>
                                <td>0400 000.00.00</td>
                                <td>EJEMPLO@RAPIDPAGO.COM</td>
                            </tr> 
                            <tr>
                                <td>V-24900846-8</td>
                                <td>ALEJANDRO RODRIGUEZ</td>
                                <td>CEDULA</td>
                                <td>24900846</td>
                                <td>VICEPRESIDENTE</td>
                                <td>0200 000.00.00</td>
                                <td>0400 000.00.00</td>
                                <td>EJEMPLO@RAPIDPAGO.COM</td>
                            </tr>    
                        </tbody>
                    </table>               
                </section>
                <section id="section-linebox-5" class="">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Fecha de Soliciud</th>
                                <th>Origen </th>
                                <th>Cliente VIP</th>
                                <th>Observaciones</th>
                            </tr>    
                        </thead>
                        <tbody>
                            <tr>
                                <td>01/01/2019</td>
                                <td>CORREO ELECTRONICO</td>
                                <td>SI</td>
                                <td>NINGUN TIPO DE OBSERVCIÓN</td>
                            </tr>    
                        </tbody>
                    </table>               
                </section>
                <section id="section-linebox-6" class="">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>DOCUMENTO</th>
                                <th>VER</th>
                            </tr>    
                        </thead>
                        <tbody>
                            <tr>
                                <td>PLANILLA DE SOLICITUD</td>
                                <td>
                                    <a href="#" class="btn btn-block btn-info">
                                        <i class="fa fa-search"></i>
                                    </a>
                                </td>
                            </tr> 
                            <tr>
                                <td>DOCUMENTO CONSTITUTIVO</td>
                                <td>
                                    <a href="#" class="btn btn-block btn-info">
                                        <i class="fa fa-search"></i>
                                    </a>
                                </td>
                            </tr> 
                            <tr>
                                <td>ESTADO DE CUENTA / CHEQUE</td>
                                <td>
                                    <a href="#" class="btn btn-block btn-info">
                                        <i class="fa fa-search"></i>
                                    </a>
                                </td>
                            </tr> 
                            <tr>
                                <td>PLANILLA DE SOLICITUD</td>
                                <td>
                                    <a href="#" class="btn btn-block btn-info">
                                        <i class="fa fa-search"></i>
                                    </a>
                                </td>
                            </tr> 
                            <tr>
                                <td>RIF DEL COMERCIO</td>
                                <td>
                                    <a href="#" class="btn btn-block btn-info">
                                        <i class="fa fa-search"></i>
                                    </a>
                                </td>
                            </tr> 
                            <tr>
                                <td>DOCUMENTO IDENTIDAD REPRESENTANTE 1</td>
                                <td>
                                    <a href="#" class="btn btn-block btn-info">
                                        <i class="fa fa-search"></i>
                                    </a>
                                </td>
                            </tr> 
                            <tr>
                                <td>DOCUMENTO IDENTIDAD REPRESENTANTE 2</td>
                                <td>
                                    <a href="#" class="btn btn-block btn-info">
                                        <i class="fa fa-search"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>RIF REPRESENTANTE 1</td>
                                <td>
                                    <a href="#" class="btn btn-block btn-info">
                                        <i class="fa fa-search"></i>
                                    </a>
                                </td>
                            </tr> 
                            <tr>
                                <td>RIF REPRESENTANTE 2</td>
                                <td>
                                    <a href="#" class="btn btn-block btn-info">
                                        <i class="fa fa-search"></i>
                                    </a>
                                </td>
                            </tr> 
                            <tr>
                                <td>CONTRATO</td>
                                <td>
                                    <a href="#" class="btn btn-block btn-info">
                                        <i class="fa fa-search"></i>
                                    </a>
                                </td>
                            </tr>    
                        </tbody>
                    </table>               
                </section>
            </div><!-- /content -->
        </div>
    </div>
    </div>  
</div>
<form id='login-form' name='login-form' method="post">

<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Verificar Solicitud</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            
                <div class="row hide">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Código</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php echo CHtml::hiddenField('codigo', $roles['usuar_codig'], array('class' => 'form-control', 'placeholder' => "Código")); ?>

                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Accion</label>
                                <?php echo CHtml::dropDownList('accio', '',array('1'=>'Aprobar', '2' =>'Rechazar'), array('class' => 'form-control', 'prompt' => "Seleccione")); ?>

                        </div>
                    </div>
                </div>
                <div class="row hide" id='motivo'>
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Motivo</label>
                                <?php echo CHtml::dropDownList('motiv', '',array('1'=>'Motivo 1', '2' =>'Motivo 2'), array('class' => 'form-control', 'prompt' => "Seleccione")); ?>

                        </div>
                    </div>
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observación</label>
                                <?php echo CHtml::textArea('obser','', array('class' => 'form-control', 'placeholder' => "Motivo")); ?>

                        </div>
                    </div>
                </div>
                <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-4 ">
                            <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                        </div>
                    </div>
        </div><!-- form -->
    </div>  
</div>
</form>
<script type="text/javascript">
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                accio: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Acción" es obligatorio',
                        }
                    }
                },
                motiv: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Motivo" es obligatorio',
                        }
                    }
                },
                obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Observaciones" es obligatorio',
                        }
                    }
                },

            }
        });
    })
</script>
<script type="text/javascript">
    
    $(document).ready(function () {
        $('#guardar').click(function () {
            $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
            var formValidation = $('#login-form').data('formValidation');
            if (formValidation.isValid()) {
                $.ajax({
                    dataType: "json",
                    data: $('#login-form').serialize(),
                    url: 'verificar',
                    type: 'post',
                    beforeSend: function () {
                        $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                    },
                    success: function (response) {
                    $('#wrapper').unblock();
                        if (response['success'] == 'true') {
                            swal({ 
                                title: "Exito!",
                                text: response['msg'],
                                type: "success",
                                confirmButtonText: "Cerrar",
                                confirmButtonClass: "btn-info"
                            },function(){
                                window.open('listado', '_parent');
                            });
                        } else {
                            swal({ 
                                title: "Error!",
                                text: response['msg'],
                                type: "error",
                                confirmButtonText: "Cerrar",
                                confirmButtonClass: "btn-danger"
                            },function(){
                                $("#guardar").removeAttr('disabled');
                            });
                        }

                    },error:function (response) {
                    $('#wrapper').unblock();
                        swal({ 
                            title: "Error!",
                            text: "Error el ejecutar la operación",
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"

                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                            
                    }
                });
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#accio").change(function () {
            var accion = $(this).val();
            if(accion=='2'){
                $("#motivo").removeClass("hide");
                $("#motiv").removeAttr("disabled");
            }else{
                $("#motivo").addClass("hide");
                $("#motiv").attr("disabled","true");
            }
            
        });
    });
</script>