<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Factura</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Configuración</a></li>
            <li><a href="#">Factura</a></li>
            <li class="active">Verificar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php
                $conexion=Yii::app()->db;
            ?>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Datos de la Solicitud</h3>
        </div>
        <div class="panel-body">
            <?php 
                $this->funciones->imprimirDatosSolicitud($solicitud);
            ?>
        </div>
    </div>
    </div>  
</div>
<form id='login-form' name='login-form' method="post">

<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Modificar Factura</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            
                <div class="row hide">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Código</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php echo CHtml::hiddenField('codigo', $solicitud['solic_codig'], array('class' => 'form-control', 'placeholder' => "Código")); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <?php 
                $sql="SELECT * FROM solicitud_factura WHERE solic_codig ='".$solicitud['solic_codig']."'";
                $factura=$conexion->createCommand($sql)->queryRow();
                ?>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Número de Factura</label>
                                <?php echo CHtml::textField('nfact', $factura['factu_nfact'], array('class' => 'form-control', 'placeholder' => "Número de Factura")); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Número de Control</label>
                                <?php echo CHtml::textField('ncont', $factura['factu_ncont'], array('class' => 'form-control', 'placeholder' => "Número de Control")); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Fecha de Factura </label>
                            <?php

                                echo CHtml::textField('fsoli', $this->funciones->transformarFecha_v($factura['factu_fsoli']), array('class' => 'form-control', 'placeholder' => "Fecha de solicitud",'prompt'=>'Seleccione...')); ?>
                               
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observaciones</label>
                                <?php echo CHtml::textArea('obser', $factura['factu_obser'], array('class' => 'form-control', 'placeholder' => "Observaciones")); ?>

                        </div>
                    </div>
                </div>
                <!-- Button -->
                    
        </div><!-- form -->
        <div class="panel-footer">
            <div class="row controls">
                <div class="col-sm-4 ">
                    <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                </div>
                <div class="col-sm-4 ">
                    <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                </div>
                <div class="col-sm-4 ">
                    <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                </div>
            </div>
        </div>
    </div>  
</div>
</form>

<script type="text/javascript">
    $(document).ready(function () {
        jQuery('#fsoli').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        endDate: '0d',
      });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                nfact: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Número de Factura" es obligatorio',
                        },numeric: {
                            message: 'Estimado(a) Usuario(a) el campo "Número de Factura" es numérico',
                        }
                    }
                },
                ncont: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Número de Control" es obligatorio',
                        },numeric: {
                            message: 'Estimado(a) Usuario(a) el campo "Número de Control" es numérico',
                        }
                    }
                },
                fsoli: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fecha de Solicitud" es obligatorio',
                        },date: {
                            format: 'DD/MM/YYYY',
                            message: 'Estimado(a) Usuario(a) el campo "Fecha de Solicitud" debe poseer el siguiente formato DD/MM/YYYY',
                        }
                    }
                },
                /*obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Observaciones" es obligatorio',
                        }
                    }
                },*/

            }
        });
    })
</script>
<script type="text/javascript">
    
    $(document).ready(function () {
        $('#guardar').click(function () {
            $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
            var formValidation = $('#login-form').data('formValidation');
            if (formValidation.isValid()) {
                $.ajax({
                    dataType: "json",
                    data: $('#login-form').serialize(),
                    url: 'modificar',
                    type: 'post',
                    beforeSend: function () {
                        $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                    },
                    success: function (response) {
                    $('#wrapper').unblock();
                        if (response['success'] == 'true') {
                            swal({ 
                                title: "Exito!",
                                text: response['msg'],
                                type: "success",
                                confirmButtonText: "Cerrar",
                                confirmButtonClass: "btn-info"
                            },function(){
                                window.open('listado', '_parent');
                            });
                        } else {
                            swal({ 
                                title: "Error!",
                                text: response['msg'],
                                type: "error",
                                confirmButtonText: "Cerrar",
                                confirmButtonClass: "btn-danger"
                            },function(){
                                $("#guardar").removeAttr('disabled');
                            });
                        }

                    },error:function (response) {
                    $('#wrapper').unblock();
                        swal({ 
                            title: "Error!",
                            text: "Error el ejecutar la operación",
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"

                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                            
                    }
                });
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#accio").change(function () {
            var accion = $(this).val();
            if(accion=='2'){
                $("#motivo").removeClass("hide");
                $("#motiv").removeAttr("disabled");
            }else{
                $("#motivo").addClass("hide");
                $("#motiv").attr("disabled","true");
            }
            
        });
    });
</script>