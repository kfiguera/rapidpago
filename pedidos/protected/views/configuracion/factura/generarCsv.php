<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Factura</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Configuración</a></li>
            <li>Facturación</a></li>
            <li>Aprobar</a></li>
            <li class="active">Listado</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="panel panel-default hide">
    <div class="panel-heading">
        <h3 class="panel-title">Criterio de Busqueda</h3>
    </div>
    <div class="panel-body" >
        <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'p_personas', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            $connection = Yii::app()->db;

        ?>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Desde</label>
                    <input type="text" class="form-control" name="desde" id="desde" placeholder="Desde">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Hasta</label>
                    <input type="text" class="form-control" name="hasta" id="hasta" placeholder="Hasta">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Estatus</label>
                    <select class="form-control" name="estatu" id="estatu">
                        <option value="">Seleccione...</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Proveedor</label>
                    <select class="form-control" name="estatu" id="estatu">
                        <option value="">Seleccione...</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Factura</label>
                    <input type="text" class="form-control" name="factura" id="factura" placeholder="Factura">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Código</label>
                    <input type="text" class="form-control" name="codigo" id="codigo" placeholder="Código">
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-sm-4">
                <button type="button" id="buscar" class="btn btn-block btn-info">Buscar</button>
            </div>
            <div class="col-sm-4">
                <button type="reset" class="btn btn-block btn-default">Limpiar</button>
            </div>
            <div class="col-sm-4">
                <a href="registrar" class="btn btn-block btn-info" >Nuevo</a>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div> 
<form id='login-form' name='login-form' method="post">

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Aprobar Facturación </h3>
    </div>
    <div class="panel-body" >
        
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-p_persona'>
                <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                <input type="checkbox" name="select_all" value="1" id="example-select-all">
                            </th>
                            <th>
                                Nro Solicitud
                            </th>
                            <th>
                                Cliente
                            </th>
                            <th>
                                Usuario
                            </th>
                            <th>
                                Total a Solicitar
                            </th>
                            <th>
                                Fecha de Registro
                            </th>
                            <th>
                                Estatus
                            </th>
                            <th width="5%">
                                Imprimir
                            </th>
                            <th width="5%">
                                Consultar
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(Yii::app()->user->id['usuario']['permiso']==1){
                            $sql = "SELECT *, 
                                    (SELECT COUNT(pdeta_codig) FROM pedido_pedidos_detalle e WHERE a.pedid_codig = e.pedid_codig) modelos, 
                                    (SELECT COUNT(pdlis_codig) FROM pedido_pedidos_detalle_listado f WHERE a.pedid_codig = f.pedid_codig) p_personas 
                                FROM pedido_pedidos a 
                                JOIN rd_preregistro_estatus b ON (a.epedi_codig = b.epedi_codig)
                                JOIN seguridad_usuarios c ON (a.usuar_codig = c.usuar_codig)
                                LEFT JOIN colegio d ON (c.coleg_codig = d.coleg_codig)";
                   
                        }else{
                            $sql = "SELECT *, 
                                    (SELECT COUNT(pdeta_codig) FROM pedido_pedidos_detalle e WHERE a.pedid_codig = e.pedid_codig) modelos, 
                                    (SELECT COUNT(pdlis_codig) FROM pedido_pedidos_detalle_listado f WHERE a.pedid_codig = f.pedid_codig) p_personas  FROM pedido_pedidos a 
                                    JOIN rd_preregistro_estatus b ON (a.epedi_codig = b.epedi_codig)
                                    JOIN seguridad_usuarios c ON (a.usuar_codig = c.usuar_codig)
                                    LEFT JOIN colegio d ON (c.coleg_codig = d.coleg_codig)
                                    WHERE a.usuar_codig = '".Yii::app()->user->id['usuario']['codigo']."'";
                             
                        }
                        
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                            $j++;
                            switch ($j) {
                                case '1':
                                    $etiqueta="label-danger label";
                                    break;
                                case '2':
                                    $etiqueta="label-info label";
                                    break;
                                case '3':
                                    $etiqueta="label-success label";
                                    break;    
                                default:
                                    $etiqueta="label-warning label";
                                    $j=0;
                                    break;
                            }
                        ?>
                        <tr>
                            

                            <td class="tabla"><input type="checkbox" name="id[<?php echo $i ?>]" value="<?php echo $row['ientr_codig'] ?>" id="id_<?php echo $i ?>"></td>
                            <td class="tabla"><?php echo $row['pedid_numer'] ?></td>
                            <td class="tabla"><?php echo "CLIENTE N° $i"//$row['coleg_nombr'] ?></td>
                            <td class="tabla"><?php echo "cliente_$i@rapidpago.com"//$row['usuar_login'] ?></td>
                            <td class="tabla"><?php echo $row['p_personas'] ?></td>
                            <td class="tabla"><?php echo $row['pedid_fcrea'] ?></td>
                            <td class="tabla"><span class="<?php echo $etiqueta; ?>"><?php echo $row['epedi_descr'] ?></span></td>

                            <!--td class="tabla"><a href="../detalle/listado?p=<?php echo $row['pedid_codig'] ?>" class="btn btn-block btn-info"><i class="glyphicon glyphicon-list-alt"></i></a></td-->
                            <td class="tabla"><a href="consultar?c=<?php echo $row['pedid_codig'] ?>" class="btn btn-block btn-success"><i class="fa fa-print"></i></a></td>
                            <td class="tabla"><a href="consultar?c=<?php echo $row['pedid_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-search"></i></a></td>
                        </tr>
                        <?php
                            }   
                        ?>
                    </tbody>
                </table>
            </div>
            
        </div>
        <div class="row">
                <div class="col-sm-4 ">
                    <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                </div>
                <div class="col-sm-4 ">
                    <button type="reset" class="btn-block btn btn-default">Limpiar  </button>
                </div>
                <div class="col-sm-4 ">
                    <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                </div>
            </div>
    </div>
</div>
</form>  
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function (){
   var table = $('#auditoria').DataTable({
      
      'columnDefs': [{
         'targets': 0,
         'searchable': false,
         'orderable': false,
         'className': 'dt-body-center',
         
      }],
      'order': [[1, 'asc']]
   });

   // Handle click on "Select all" control
   $('#example-select-all').on('click', function(){
      // Get all rows with search applied
      var rows = table.rows({ 'search': 'applied' }).nodes();
      // Check/uncheck checkboxes for all rows in the table
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
   });

   // Handle click on checkbox to set state of "Select all" control
   $('#auditoria tbody').on('change', 'input[type="checkbox"]', function(){
      // If checkbox is not checked
      if(!this.checked){
         var el = $('#example-select-all').get(0);
         // If "Select all" control is checked and has 'indeterminate' property
         if(el && el.checked && ('indeterminate' in el)){
            // Set visual state of "Select all" control
            // as 'indeterminate'
            el.indeterminate = true;
         }
      }
   });

   // Handle form submission event
   $('#frm-example').on('submit', function(e){
      var form = this;

      // Iterate over all checkboxes in the table
      table.$('input[type="checkbox"]').each(function(){
         // If checkbox doesn't exist in DOM
         if(!$.contains(document, this)){
            // If checkbox is checked
            if(this.checked){
               // Create a hidden element
               $(form).append(
                  $('<input>')
                     .attr('type', 'hidden')
                     .attr('name', this.name)
                     .val(this.value)
               );
            }
         }
      });
   });

});
</script>
<script>
    $(document).ready(function () {
       
        $('#buscar').click(function () {
        var formData = new FormData($("#p_personas")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-p_persona').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>
<script>
    $(document).ready(function () {
        $('#cantidad').mask('#.##0',{reverse: true,maxlength:false});
        $('#punidad').mask('#.##0,00',{reverse: true,maxlength:false});
    });
</script>

<script>
// Date Picker
    $(document).ready(function () {
        $('#desde').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd/mm/yyyy'
        });
        $('#hasta').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd/mm/yyyy'
        });
    }); 
</script>

<script type="text/javascript">
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'AprobarEntradas',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>