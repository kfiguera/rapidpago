<?php $conexion = Yii::app()->db; ?>
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Factura</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Registro y Documentación</a></li>
            <li><a href="#">Factura</a></li>
            <li class="active">Registrar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php $this->renderPartial('pasos', array('ubicacion' => $ubicacion,'solicitud'=>$solicitud));?>
<form id='login-form' name='login-form' method="post">

<div class="row">   
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Datos del Comercio</h3>
        </div>
        <div class="panel-body" >
           
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
                <div class="row hide">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Pedido *</label>
                            <?php 
                                echo CHtml::hiddenField('pedid', $p, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Código *</label>
                            <?php 
                                echo CHtml::hiddenField('codig', $c, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Paso *</label>
                            <?php 
                                echo CHtml::hiddenField('pasos', $solicitud['prere_pasos'], array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                </div>
                <?php
                    $sql="SELECT * FROM cliente WHERE clien_codig='".$solicitud['clien_codig']."'";
                    $cliente=$conexion->createCommand($sql)->queryRow();
                ?>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>RIF del Comercio *</label>

                            <?php
                                echo CHtml::textField('rifco', $cliente['clien_rifco'], array('class' => 'form-control', 'placeholder' => "RIF del Comercio",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Razon Social*</label>

                            <?php
                                echo CHtml::textField('rsoci', $cliente['clien_rsoci'], array('class' => 'form-control', 'placeholder' => "Razon Social",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Nombre Fantasia o Comercial *</label>

                            <?php
                                echo CHtml::textField('nfant', $cliente['clien_nfant'], array('class' => 'form-control', 'placeholder' => "Nombre Fantasia o Comercial",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Actividad Comercial *</label>
                            <?php
                                $sql="SELECT *
                                  FROM p_actividad_comercial
                                  ORDER BY 1";

                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'acome_codig','acome_descr');

                                echo CHtml::dropDownList('acome', $cliente['acome_codig'], $data, array('class' => 'form-control select2', 'placeholder' => "Actividad Comercial",'prompt'=>'Seleccione...')); ?>
                               
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Teléfono Movil *</label>

                            <?php
                                echo CHtml::textField('tmovi', $cliente['clien_tmovi'], array('class' => 'form-control', 'placeholder' => "Teléfono Movil",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Teléfono Fijo </label>

                            <?php
                                echo CHtml::textField('tfijo', $cliente['clien_tfijo'], array('class' => 'form-control', 'placeholder' => "Teléfono Fijo",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                </div>
                <?php
                    $sql="SELECT * FROM cliente_direccion WHERE clien_codig='".$cliente['clien_codig']."'";
                    $direccion=$conexion->createCommand($sql)->queryRow();
                ?>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Estado *</label>
                            <?php
                                $sql="SELECT *
                                  FROM p_estados
                                  ORDER BY 1";

                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'estad_codig','estad_descr');

                                echo CHtml::dropDownList('estad', $direccion['estad_codig'], $data, array('ajax' => array(
                                            'type' => 'POST',
                                            'url' => CController::createUrl('funciones/ComboMunicipios'), // Controlador que devuelve las p_ciudades relacionadas
                                            'update' => '#munic', // id del item que se actualizará
                                        ),'class' => 'form-control', 'placeholder' => "Estado",'prompt'=>'Seleccione...')); ?>
                               
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Municipio *</label>
                            <?php
                                $sql="SELECT *
                                  FROM p_municipio
                                  ORDER BY 1";

                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'munic_codig','munic_descr');

                                echo CHtml::dropDownList('munic', $direccion['munic_codig'], $data, array('ajax' => array(
                                            'type' => 'POST',
                                            'url' => CController::createUrl('funciones/ComboParroquias'), // Controlador que devuelve las p_ciudades relacionadas
                                            'update' => '#parro', // id del item que se actualizará
                                        ),'class' => 'form-control', 'placeholder' => "Municipio",'prompt'=>'Seleccione...')); ?>
                               
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Parroquia *</label>
                            <?php
                                $sql="SELECT *
                                      FROM p_parroquia
                                      ORDER BY 1";

                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'parro_codig','parro_descr');

                                echo CHtml::dropDownList('parro', $direccion['parro_codig'], $data, array('ajax' => array(
                                            'type' => 'POST',
                                            'url' => CController::createUrl('funciones/ComboCiudades'), // Controlador que devuelve las p_ciudades relacionadas
                                            'update' => '#ciuda', // id del item que se actualizará
                                        ),'class' => 'form-control', 'placeholder' => "Parroquia",'prompt'=>'Seleccione...')); ?>
                               
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Ciudad *</label>
                            <?php
                                $sql="SELECT *
                                      FROM p_ciudades
                                      ORDER BY 1";

                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'ciuda_codig','ciuda_descr');

                                echo CHtml::dropDownList('ciuda', $direccion['ciuda_codig'], $data, array('class' => 'form-control', 'placeholder' => "Ciudad",'prompt'=>'Seleccione...')); ?>
                               
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Correo Electronico *</label>

                            <?php
                                echo CHtml::textField('celec', $solicitud['solic_celec'], array('class' => 'form-control', 'placeholder' => "Correo Electronico",'prompt'=>'Seleccione...')); ?>
                            <span class="help-block">
                                <small>
                                    Contacto Principal.
                                </small>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Dirección Fiscal * </label>

                            <?php
                                echo CHtml::textArea('dfisc', $direccion['direc_dfisc'], array('class' => 'form-control', 'placeholder' => "Dirección Fiscal",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                </div>
            </div><!-- form -->
    </div>  
</div>
<div class="row">   
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Datos de afiliación</h3>
        </div>
        <div class="panel-body" >
           
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
                
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Banco Afiliado *</label>
                            <?php
                                $sql="SELECT *
                                  FROM p_banco a
                                  WHERE banco_estat='1'
                                  ORDER BY 2";

                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'banco_codig','banco_descr');

                                echo CHtml::dropDownList('banco', $solicitud['banco_codig'], $data, array('class' => 'select2 form-control', 'placeholder' => "Banco Afiliado",'prompt'=>'Seleccione...','disabled'=>$gorro)); ?>
                               
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Código Afiliado *</label>

                            <?php
                                echo CHtml::textField('cafil', $solicitud['solic_cafil'], array('class' => 'form-control', 'placeholder' => "Código Afiliado",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Numero de Cuenta * </label>

                            <?php
                                echo CHtml::textField('ncuen', $solicitud['solic_ncuen'], array('class' => 'form-control', 'placeholder' => "Numero de Cuenta",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                </div>
                
            </div><!-- form -->
    </div>  
</div>

<div class="row">   
    <div class="panel panel-default">
        <div class="panel-body" >
                <!-- Button -->
                <div class="row controls">
                    <div class="col-sm-4 ">
                        <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                    </div>
                    <div class="col-sm-4 ">
                        <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                    </div>
                    <div class="col-sm-4 ">
                        <button id="guardar" type="button" class="btn-block btn btn-info">Siguiente  </button>
                    </div>
                </div>


        </div><!-- form -->
    </div>  
</div>
</form>
<script type="text/javascript">
    //$('#rifco').mask("V-00000000-0");
    $('#rifco').mask('Z000000000',  {
        'translation': {
          'Z': {
            pattern: /[V|E|J|G|C|v|e|j|g|c]/
          }
        }
      });
    $('#tmovi').mask("0000-000.00.00");
    $('#tfijo').mask("0000-000.00.00");
    $('#ncuen').mask("00000000000000000000");   
    $('#cafil').mask("00000000");
    //$('#ncuen').mask("0000-0000-00-0000000000");
        
</script>

<script type="text/javascript">
    var descr = {
            validators: {
                notEmpty: {
                    message: 'Estimado(a) Usuario(a) el campo "Nombre" es obligatorio',
                }
            }
        },
        image = {
            validators: {
                notEmpty: {
                    message: 'Estimado(a) Usuario(a) el campo "Imagen a Bordar" es obligatorio',
                }
            }
        },
        texto = {
            validators: {
                notEmpty: {
                    message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar" es obligatorio',
                }
            }
        },
        bookIndex = <?php echo $i; ?>,
        contador = 0;
        
</script>
<script>
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                rifco:{
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "RIF del Comercio" es obligatorio',
                        },
                        regexp: {
                            regexp: /^[V|E|J|G|C|v|e|j|g|c]\d{8}\d+$/,
                            message: 'Estimado(a) Usuario(a) el campo "RIF" debe poseer el siguiente formato: J000000000'
                        }
                    }
                },
                rsoci:{
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Razon Social" es obligatorio',
                        }
                    }
                },
                nfant:{
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Nombre Fantasia o Comercial" es obligatorio',
                        }
                    }
                },
                acome:{
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Actividad Comercial" es obligatorio',
                        }
                    }
                },
                tmovi:{
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Teléfono Movil" es obligatorio',
                        }
                    }
                },
                /*tfijo:{
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Teléfono Fijo" es obligatorio',
                        }
                    }
                },*/
                estad:{
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Estado" es obligatorio',
                        }
                    }
                },
                munic:{
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Municipio" es obligatorio',
                        }
                    }
                },
                parro:{
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Parroquia" es obligatorio',
                        }
                    }
                },
                ciuda:{
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Ciudad" es obligatorio',
                        }
                    }
                },
                celec:{
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Correo Electronico" es obligatorio',
                        },
                        emailAddress: {
                            message: 'Estimado(a) Usuario(a) el campo "Correo Electronico"debe poseer el siguiente formato: ejemplo@rapidpago.com '
                        }
                    }
                },
                dfisc:{
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Dirección Fiscal" es obligatorio',
                        }
                    }
                },
                banco:{
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Banco Afiliado" es obligatorio',
                        }
                    }
                },
                cafil:{
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Código Afiliado" es obligatorio',
                        },
                        numeric: {
                            message: 'Estimado(a) Usuario(a) el campo "Código Afiliado" es numérico',
                        },
                        stringLength: {
                            min: 8,
                            max: 8,
                            message: 'Estimado(a) Usuario(a) el campo "Código Afiliado" debe poseer 8 caracteres'
                        }
                    }
                },
                ncuen:{
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Número de Cuenta" es obligatorio',
                        },numeric: {
                            message: 'Estimado(a) Usuario(a) el campo "Número de Cuenta" es numérico',
                        },
                        stringLength: {
                            min: 20,
                            max: 20,
                            message: 'Estimado(a) Usuario(a) el campo "Número de Cuenta" debe poseer 20 caracteres'
                        }
                    }
                },
                
            }
        });
    
    });
    $('#rifco').change(function () {
        $('#login-form').formValidation('resetForm');
        $('#login-form').formValidation('validate');
    });

    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        var data = new FormData(jQuery('form')[0]);
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                url: 'paso1',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        /*swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){*/
                            window.open('modificar?c=<?php echo $c; ?>&s=2', '_parent');
                        /*});*/
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>
<script type="text/javascript">
    $('#cierr').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '1':
                jQuery("#ccier").removeAttr('disabled');
                break;
            default:
                jQuery("#ccier").attr('disabled','true');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#iopci').change(function () {
        var iopci = $(this).val();
        $.ajax({
            'type':'POST',
            'data':{'tmode':tmode.value},
            'url':'<?php echo CController::createUrl('funciones/FacturaColorOpcional'); ?>',
            'cache':false,
            'success':function(html){
                switch(iopci) {
                    case '1':
                        
                        jQuery("#opcional-1").removeClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#iclin").html(html);
                        jQuery("#iccie").html('');
                        jQuery("#icviv").html('');
                        jQuery("#ideta").removeAttr('disabled');
                        jQuery("#iclin").removeAttr('disabled');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").attr('disabled','true');

                        break;
                    case '2':
                        jQuery("#opcional-2").removeClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#iclin").html('');
                        jQuery("#iccie").html(html);
                        jQuery("#icviv").html('');
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").removeAttr('disabled');
                        jQuery("#icviv").attr('disabled','true');
                        break;
                    case '3':
                        jQuery("#opcional-3").removeClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#iclin").html('');
                        jQuery("#iccie").html('');
                        jQuery("#icviv").html(html);
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").removeAttr('disabled');
                        break;
                    default:
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#iclin").html(html);
                        jQuery("#iccie").html(html);
                        jQuery("#icviv").html(html);
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").attr('disabled','true');
                        break;
                }
                
            }
        });
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    var id = ['ccuer', 'cbols', 'cmang', 'cegor', 'cigor', 'cppre', 'ccier', 'ccurs', 'cnper', 'caesp', 'celec', 'cfras', 'cpprl', 'cvivo' ];
    var con = 1;
    id.forEach( function(valor, indice, array) {
        var numero = indice+1;
        $('#img-'+numero).popover({
          html: true,
          content: function() {
            if(numero==1){
                return $('#popover-img').html();
            }else{
                return $('#popover-img-'+numero).html();    
            }
            
          },
          trigger: 'hover'
        });
        $('#'+valor).change(function () {
            var emoji = $(this).val();
            $.ajax({  
                url:"<?php echo CController::createUrl('funciones/FacturaVerColor'); ?>",  
                method:"POST",  
                async:false,  
                data:{id:emoji},  
                success:function(data){  
                    if(numero=='1'){
                        $('#popover-img').html(data);
                    }else{
                        $('#popover-img-'+numero).html(data);    
                    } 
                }  
            });
            $('[data-toggle=popover]').popover('hide');
        });
    });
});
</script>
<script type="text/javascript">
    $('#ideta').change(function () {
        var ideta = $(this).val();
        switch(ideta) {
            case '1':
                jQuery("#iclin").removeAttr('disabled');
                break;
            default:
                jQuery("#iclin").attr('disabled','true');
                break;
        }
                
            
    });
</script>

<!--script type="text/javascript">
$(document).ready(function(){
    $('#img-1').popover({
          html: true,
          content: function() {
            return $('#popover-img').html();
          },
          trigger: 'hover'
        });
});
$('#color').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/FacturaVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-2').popover({
          html: true,
          content: function() {
            return $('#popover-img-2').html();
          },
          trigger: 'hover'
        });
});
$('#iclin').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/FacturaVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-2").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-3').popover({
          html: true,
          content: function() {
            return $('#popover-img-3').html();
          },
          trigger: 'hover'
        });
});
$('#iccie').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/FacturaVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-3").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-4').popover({
          html: true,
          content: function() {
            return $('#popover-img-4').html();
          },
          trigger: 'hover'
        });
});
$('#icviv').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/FacturaVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-4").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script-->