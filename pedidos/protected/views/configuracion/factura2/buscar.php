<table  id='auditoria'  class="table table-bordered table-hover dataTable">
    <thead>
        <tr>
            <th width="2%">
                #
            </th>
            <th>
                Número de Factura
            </th>
            <th>
                Número de Control
            </th>
            <th>
                Fecha Emision
            </th>
            <th>
                Monto
            </th>
            <th>
                Banco
            </th>
            <th>
                Estatus
            </th>
            <th width="5%">
                Consultar
            </th>
            <th width="5%">
                Modificar
            </th>
            <th width="5%">
                Eliminar
            </th>
        </tr>
    </thead>
    <tbody>
        <?php
        $sql = "SELECT * FROM factura a 
                JOIN p_factura_estatu b ON (a.efact_codig = b.efact_codig) 
                JOIN banco c ON (a.banco_codig = c.banco_codig)".$condicion;
        $connection= yii::app()->db;
        $command = $connection->createCommand($sql);
        $persona = $command->query();
        $i=0;
        while (($row = $persona->read()) !== false) {
            $i++;
        ?>
        <tr>
            <td class="tabla"><?php echo $i ?></td>
            <td class="tabla"><?php echo $this->funciones->TransformarMonto_v($row['factu_nfact'],0) ?></td>
            <td class="tabla"><?php echo $this->funciones->TransformarMonto_v($row['factu_ncont'],0) ?></td>
            <td class="tabla"><?php echo $this->funciones->TransformarFecha_v($row['factu_femis']) ?></td>
            <td class="tabla"><?php echo $this->funciones->TransformarMonto_v($row['factu_monto'],2) ?></td>
            <td class="tabla"><?php echo $row['banco_descr'] ?></td>
            <td class="tabla"><?php echo $row['efact_descr'] ?></td>
            <td class="tabla"><a href="consultar?c=<?php echo $row['factu_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-search"></i></a></td>
            <?php 
                if($row["ieest_codig"]<>'1') {
                    ?>
                        <td class="tabla"><a href="#" class="btn btn-block btn-info disabled"><i class="fa fa-pencil"></i></a></td>
                        <td class="tabla"><a href="#" class="btn btn-block btn-info disabled"><i class="fa fa-close"></i></a></td>        
                    <?php
                }else{
                    ?>
                        <td class="tabla"><a href="modificar?c=<?php echo $row['factu_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-pencil"></i></a></td>
                        <td class="tabla"><a href="eliminar?c=<?php echo $row['factu_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-close"></i></a></td>        
                    <?php
                }
            ?>
        </tr>
        <?php
            }   
        ?>
    </tbody>
</table>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable(); 
    });
</script>
