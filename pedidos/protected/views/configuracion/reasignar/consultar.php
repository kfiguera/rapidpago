<?php $conexion=Yii::app()->db; ?>
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Configuración</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Configuración</a></li>
            <li><a href="#">Almacén</a></li>
            <li><a href="#">Asignar Dispositivos</a></li>
            <li class="active">Consultar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Datos de la Solicitud</h3>
        </div>
        <div class="panel-body">
            <?php 
                $this->funciones->imprimirDatosSolicitud($solicitud);
            ?>
        </div>
    </div>  
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title"> Consultar Asignación
                    <div class="panel-action">
                        <a href="#" data-perform="panel-collapse">
                            <i class="ti-plus"></i>
                        </a> 
                    </div>
            </h3>
                

            </div>
            <div class="panel-wrapper collapse in">

                <div class="panel-body" >
                    <input class="form-control" placeholder="Porcentaje" readonly="readonly" type="hidden" value="" name="bookindex" id="bookindex" /> 
                    <table class="table table-bordered table-hover  ">
                        <thead>
                            <tr>
                                <th>Motivo de Rechazo</th>
                                <th>Observación</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <?php
                                        $sql="SELECT a.* 
                                          FROM p_motivo a 
                                          WHERE motiv_codig='".$solicitud['motiv_codig']."'
                                          ORDER BY 1";
                                        $result=$conexion->createCommand($sql)->queryRow();
                                        echo $result['motiv_descr'];
                                    ?>
                                </td>
                                <td><?php echo $solicitud['solic_robse']; ?></td>
                            </tr>
                        </tbody>
                    </table>
                    <?php
                        $sql="SELECT * 
                              FROM solicitud_asignacion
                              WHERE solic_codig='".$solicitud['solic_codig']."'";
                        $electivas=$conexion->createCommand($sql)->query();
                        $result=$conexion->createCommand($sql)->queryAll();
                    ?>
                    <div class="row">
                        <div class="col-xs-12 table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th width="13.5%">Modelo</th>
                                        <th width="13.5%">Producto</th>
                                        <th width="13.5%">Serial Dispositivo</th>
                                        <th width="13.5%">Operador Telefónico</th>
                                        <th width="13.5%">Serial Tarjeta SIM</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   <?php
                                        $a=0;
                                        foreach ($result as $key => $asignacion) {
                                            $a++;
                                            $sql="SELECT * 
                                                  FROM inventario_seriales a
                                                  JOIN inventario b ON (a.model_codig = b.model_codig)
                                                  JOIN inventario_modelo c ON (b.model_codig = c.model_codig)
                                                  WHERE seria_codig='".$asignacion['asign_dispo']."'";    
                                            $dispo=$conexion->createCommand($sql)->queryRow();
                                            $sql="SELECT * 
                                                  FROM inventario_seriales a 
                                                  JOIN inventario b ON (a.model_codig = b.model_codig)
                                                  JOIN inventario_modelo c ON (b.model_codig = c.model_codig)
                                                  WHERE seria_codig='".$asignacion['asign_opera']."'";
                                                
                                            $opera=$conexion->createCommand($sql)->queryRow();
                                            $sql="SELECT a.seria_codig, a.seria_numer
                                                  FROM inventario_seriales a
                                                  JOIN inventario b ON (a.inven_codig =b.inven_codig)
                                                  JOIN inventario_modelo c ON (a.model_codig =c.model_codig)
                                                  WHERE a.model_codig ='".$dispo['model_codig']."'
                                                  AND a.inven_codig ='".$dispo['inven_codig']."'
                                                  AND a.seria_codig not in (
                                                    SELECT b.asign_dispo
                                                    FROM solicitud_asignacion b
                                                    UNION 
                                                    SELECT c.asign_sdisp
                                                    FROM solicitud_asignacion c)
                                                  AND a.seria_codig <> '".$dispo['seria_codig']."'";

                                            $seriales=$conexion->createCommand($sql)->queryAll();
                                            $sql="SELECT a.seria_codig, a.seria_numer
                                                  FROM inventario_seriales a
                                                  JOIN inventario b ON (a.inven_codig =b.inven_codig)
                                                  JOIN inventario_modelo c ON (a.model_codig =c.model_codig)
                                                  WHERE a.model_codig ='".$opera['model_codig']."'
                                                  AND a.seria_codig not in (
                                                    SELECT b.asign_opera
                                                    FROM solicitud_asignacion b
                                                    UNION 
                                                    SELECT c.asign_soper
                                                    FROM solicitud_asignacion c)
                                                  AND a.seria_codig <> '".$opera['seria_codig']."'";
                                            $tsim=$conexion->createCommand($sql)->queryAll();
                                            ?>
                                            <tr>
                                                <th >
                                                    <?php 
                                                        echo $a; 
                                                        echo CHtml::hiddenField('asign['.$a.']', $asignacion['asign_codig'], $data,array('class' => 'form-control ', 'placeholder' => "Banco", 'prompt'=>'Seleccione...','id'=>'sdisp_'.$a.'')); 
                                                    ?>       
                                                </th>
                                                <th ><?php echo $dispo['model_descr']; ?></th>
                                                <th ><?php echo $dispo['inven_descr']; ?></th>
                                                <th ><?php echo $dispo['seria_numer']; ?></th>
                                                <th ><?php echo $opera['model_descr']; ?></th>
                                                <th ><?php echo $opera['seria_numer']; ?></th>
                                               
                                            </tr>
                                            <?php
                                        }
                                   ?> 
                                </tbody>
                            </table>
                        </div>
                    </div>
                
                <!-- Button -->
                <div class="row controls">
                    <div class="col-sm-12 ">
                        <a href="listado" type="reset" class="btn-block btn btn-info">Volver </a>
                    </div>
                </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
<script>
$('#punid').mask('#.##0,00',{reverse: true,maxlength:false});
$('#desde').mask('00/00/0000');
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                descr: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Descripción" es obligatorio',
                        }
                    }
                },
                codig: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Código" es obligatorio',
                        }
                    }
                },
                punid: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Precio por Unidad" es obligatorio',
                        }
                    }
                },
                canti: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "cantidad" es obligatorio',
                        }
                    }
                },
                tunid: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Unidad" es obligatorio',
                        }
                    }
                },
                moned: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Moneda" es obligatorio',
                        }
                    }
                },
                /*obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Observaciones" es obligatorio',
                        }
                    }
                },*/



            }
        });
    });

</script>