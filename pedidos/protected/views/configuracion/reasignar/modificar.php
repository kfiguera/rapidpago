<?php $conexion=Yii::app()->db; ?>
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Configuración</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Configuración</a></li>
            <li><a href="#">Almacén</a></li>
            <li><a href="#">Reasignar Dispositivos</a></li>
            <li class="active">Modificar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<form id='login-form' name='login-form' method="post">
            
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Datos de la Solicitud</h3>
        </div>
        <div class="panel-body">
            <?php 
                $this->funciones->imprimirDatosSolicitud($solicitud);
            ?>
        </div>
    </div>  
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Registrar Productos 
                    <div class="panel-action">
                        <a href="#" data-perform="panel-collapse">
                            <i class="ti-plus"></i>
                        </a> 
                    </div>
            </h3>
                

            </div>
            <div class="panel-wrapper collapse in">

                <div class="panel-body" >
                    <input class="form-control" placeholder="Porcentaje" readonly="readonly" type="hidden" value="" name="bookindex" id="bookindex" /> 

                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label>Motivo</label>
                            <?php 
                                $data=array();
                                $sql="SELECT a.* 
                                      FROM p_motivo a 
                                      WHERE motiv_codig='".$solicitud['motiv_codig']."'
                                      ORDER BY 1
                                    ";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'motiv_codig','motiv_descr');
                                echo CHtml::dropDownList('motiv', $solicitud['motiv_codig'], $data,array('class' => 'form-control ', 'placeholder' => "Banco", 'prompt'=>'Seleccione...','id'=>'motiv', 'disabled'=>'true')); 
                            ?>       
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label>Observacion de Rechazo</label>
                        <?php 
                            
                            $result=$conexion->createCommand($sql)->queryAll();
                            $data=CHtml::listData($result,'model_codig','model_descr');
                            echo CHtml::textField('obser', $solicitud['solic_robse'],array('class' => 'form-control ', 'placeholder' => "Observacion de Rechazo", 'prompt'=>'Seleccione...','id'=>'motiv', 'disabled'=>'true')); ?>
                        </div>
                    </div>
                </div>
                <div class="row hide">
                    <div class="col-xs-6">
                        <label>Solicitud</label>
                        <?php 
                            echo CHtml::hiddenField('solic', $solicitud['solic_codig'], $data,array('class' => 'form-control ', 'placeholder' => "Solicitud", 'prompt'=>'Seleccione...','id'=>'model_0')); ?>       
                    </div>
                    <div class="col-xs-6">
                        <label>Código</label>
                        <?php 
                            echo CHtml::hiddenField('codig', $solicitud['solic_codig'], $data,array('class' => 'form-control ', 'placeholder' => "Solicitud", 'prompt'=>'Seleccione...','id'=>'model_0')); ?>       
                    </div>
                </div>
                <?php
                    $sql="SELECT * 
                          FROM solicitud_asignacion
                          WHERE solic_codig='".$solicitud['solic_codig']."'";
                    $electivas=$conexion->createCommand($sql)->query();
                    $result=$conexion->createCommand($sql)->queryAll();
                ?>
                <div class="row">
                    <div class="col-xs-12 table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="7.5%">Modelo</th>
                                    <th width="12.5%">Producto</th>
                                    <th width="12.5%">Serial Dispositivo</th>
                                    <th width="7.5%">Operador Telefónico</th>
                                    <th width="15%">Serial Tarjeta SIM</th>
                                    <th width="20%">Nuevo POS</th>
                                    <th width="20%">Nueva SIM</th>
                                </tr>
                            </thead>
                            <tbody>
                               <?php
                                    $a=0;
                                    foreach ($result as $key => $asignacion) {
                                        $a++;
                                        $sql="SELECT * 
                                              FROM inventario_seriales a
                                              JOIN inventario b ON (a.model_codig = b.model_codig)
                                              JOIN inventario_modelo c ON (b.model_codig = c.model_codig)
                                              WHERE seria_codig='".$asignacion['asign_dispo']."'";    
                                        $dispo=$conexion->createCommand($sql)->queryRow();
                                        $sql="SELECT * 
                                              FROM inventario_seriales a 
                                              JOIN inventario b ON (a.model_codig = b.model_codig)
                                              JOIN inventario_modelo c ON (b.model_codig = c.model_codig)
                                              WHERE seria_codig='".$asignacion['asign_opera']."'";
                                            
                                        $opera=$conexion->createCommand($sql)->queryRow();
                                        $sql="SELECT a.seria_codig, a.seria_numer
                                              FROM inventario_seriales a
                                              JOIN inventario b ON (a.inven_codig =b.inven_codig)
                                              JOIN inventario_modelo c ON (a.model_codig =c.model_codig)
                                              WHERE a.model_codig ='".$dispo['model_codig']."'
                                              AND a.inven_codig ='".$dispo['inven_codig']."'
                                              AND a.seria_codig not in (
                                                SELECT b.asign_dispo
                                                FROM solicitud_asignacion b
                                                UNION 
                                                SELECT c.asign_sdisp
                                                FROM solicitud_asignacion c)
                                              AND a.seria_codig <> '".$dispo['seria_codig']."'";

                                        $seriales=$conexion->createCommand($sql)->queryAll();
                                        $sql="SELECT a.seria_codig, a.seria_numer
                                              FROM inventario_seriales a
                                              JOIN inventario b ON (a.inven_codig =b.inven_codig)
                                              JOIN inventario_modelo c ON (a.model_codig =c.model_codig)
                                              WHERE a.model_codig ='".$opera['model_codig']."'
                                              AND a.seria_codig not in (
                                                SELECT b.asign_opera
                                                FROM solicitud_asignacion b
                                                UNION 
                                                SELECT c.asign_soper
                                                FROM solicitud_asignacion c)
                                              AND a.seria_codig <> '".$opera['seria_codig']."'";
                                        $tsim=$conexion->createCommand($sql)->queryAll();
                                        ?>
                                        <tr>
                                            <th >
                                                <?php 
                                                    echo $a; 
                                                    echo CHtml::hiddenField('asign['.$a.']', $asignacion['asign_codig'], $data,array('class' => 'form-control ', 'placeholder' => "Banco", 'prompt'=>'Seleccione...','id'=>'sdisp_'.$a.'')); 
                                                ?>       
                                            </th>
                                            <th ><?php echo $dispo['model_descr']; ?></th>
                                            <th ><?php echo $dispo['inven_descr']; ?></th>
                                            <th ><?php echo $dispo['seria_numer']; ?></th>
                                            <th ><?php echo $opera['model_descr']; ?></th>
                                            <th ><?php echo $opera['seria_numer']; ?></th>
                                            <th >
                                                <div class="form-group">
                                                    <?php 
                                                        $data=array();
                                                        $data=CHtml::listData($seriales,'seria_codig','seria_numer');
                                                        echo CHtml::dropDownList('sdisp['.$a.']', $asignacion['asign_sdisp'], $data,array('class' => 'form-control ', 'placeholder' => "Banco", 'prompt'=>'Seleccione...','id'=>'sdisp_'.$a.'')); 
                                                    ?>       
                                                </div>
                                            </th>
                                            <th >
                                                <div class="form-group">
                                                    <?php 
                                                        $data=array();
                                                        $data=CHtml::listData($tsim,'seria_codig','seria_numer');
                                                        echo CHtml::dropDownList('soper['.$a.']', $asignacion['asign_soper'], $data,array('class' => 'form-control ', 'placeholder' => "Banco", 'prompt'=>'Seleccione...','id'=>'soper_'.$a.'')); 
                                                    ?>       
                                                </div>
                                            </th>
                                            <script>
                                            $(document).ready(function () {
                                                $("#sdisp_<?php echo $a; ?>").select2({
                                                  minimumInputLength: 2,
                                                    allowClear: true
                                                    });
                                                $("#soper_<?php echo $a; ?>").select2({
                                                minimumInputLength: 2,
                                                    allowClear: true
                                                    });
                                                });
                                            </script>
                                        </tr>
                                        <?php
                                    }
                               ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
                
               
                

           

        </div><!-- form -->
    </div> 
    <div class="panel-footer"> 
         <!-- Button -->
        <div class="row controls">
            <div class="col-sm-4 ">
                <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
            </div>
            <div class="col-sm-4 ">
                <button type="reset" class="btn-block btn btn-default">Limpiar  </button>
            </div>
            <div class="col-sm-4 ">
                <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
            </div>
        </div>
    </div>
</div>
 </form>
<script>
$('#punid').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#canti').mask('#.##0',{reverse: true,maxlength:false});
        $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                descr: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Descripción" es obligatorio',
                        }
                    }
                },
                codig: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Código" es obligatorio',
                        }
                    }
                },
                punid: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Precio por Unidad" es obligatorio',
                        }
                    }
                },
                canti: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "cantidad" es obligatorio',
                        }
                    }
                },
                tunid: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Unidad" es obligatorio',
                        }
                    }
                },
                moned: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Moneda" es obligatorio',
                        }
                    }
                },
                /*obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Observaciones" es obligatorio',
                        }
                    }
                },*/



            }
        });
    });

    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'modificar',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>