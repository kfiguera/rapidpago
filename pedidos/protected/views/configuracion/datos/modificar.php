<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Registro</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Registro</a></li>
            
            <li class="active">Modificar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<form id='login-form' name='login-form' method="post">
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Datos Basicos del Cólegio</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            
                <div class="row hide">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Código</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php echo CHtml::hiddenField('codigo', $roles['regis_codig'], array('class' => 'form-control', 'placeholder' => "Código")); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Colegio *</label>
                          
                                <?php 
                                    $sql="SELECT * FROM colegio";
                                    $conexion=Yii::app()->db;
                                    $result=$conexion->createCommand($sql)->queryAll();
                                    $data=CHtml::listData($result,'coleg_codig','coleg_nombr');
                                    echo CHtml::dropDownList('coleg', $roles['coleg_codig'], $data, array('ajax' => array(
                                                'type' => 'POST',
                                                'url' => CController::createUrl('funciones/ListarColegio'),
                                                'dataType' => 'json',
                                                'success' => 'ListarColegio',
                                            ),'class' => 'form-control select2', 'placeholder' => "Nombre", 'prompt'=>'Seleccione...','required'=>'true')); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Sitio Web</label>
                         
                                <?php echo CHtml::textField('pgweb', $roles['regis_pgweb'], array('class' => 'form-control', 'placeholder' => "Sitio Web")); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Teléfono *</label>
                            <?php echo CHtml::textField('telef', $roles['regis_telef'], array('class' => 'form-control', 'placeholder' => "Teléfono")); ?>                        
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Dirección</label>
                            <?php echo CHtml::textArea('direc',$roles['regis_direc'], array('class' => 'form-control', 'placeholder' => "Dirección")); ?>
                        </div>
                    </div>
                </div>
        </div><!-- form -->
    </div>  
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Redes Sociales del Colegio</h3>
        </div>
        <div class="panel-body" >
            
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Facebook</label>
                          
                                <?php echo CHtml::textField('rede1', $roles['regis_rede1'], array('class' => 'form-control', 'placeholder' => "Facebook")); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Twitter</label>
                          
                                <?php echo CHtml::textField('rede2', $roles['regis_rede2'], array('class' => 'form-control', 'placeholder' => "Twitter")); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Instagram</label>
                          
                                <?php echo CHtml::textField('rede3', $roles['regis_rede3'], array('class' => 'form-control', 'placeholder' => "Instagram")); ?>

                        </div>
                    </div>
                   
                </div>
                
        </div><!-- form -->
    </div>  
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Contacto 1</h3>
        </div>
        <div class="panel-body" >
            
                <div class="row">
                    <div class="col-sm-4 hide ">        
                        <div class="form-group">
                            <label>RUT *</label>
                          
                                <?php echo CHtml::textField('cont1_ndocu', $roles['cont1_ndocu'], array('class' => 'form-control', 'placeholder' => "RUT")); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Primer Nombre *</label>
                          
                                <?php echo CHtml::textField('cont1_pnomb', $roles['cont1_pnomb'], array('class' => 'form-control', 'placeholder' => "Primer Nombre")); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Segundo Nombre</label>
                          
                                <?php echo CHtml::textField('cont1_snomb', $roles['cont1_snomb'], array('class' => 'form-control', 'placeholder' => "Segundo Nombre")); ?>

                        </div>
                    </div>
                   
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Primer Apellido *</label>
                          
                                <?php echo CHtml::textField('cont1_papel', $roles['cont1_papel'], array('class' => 'form-control', 'placeholder' => "Primer Apellido")); ?>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Segundo Apellido</label>
                          
                                <?php echo CHtml::textField('cont1_sapel', $roles['cont1_sapel'], array('class' => 'form-control', 'placeholder' => "Segundo Apellido")); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Correo</label>
                                <?php echo CHtml::textField('cont1_corre', $roles['cont1_corre'], array('class' => 'form-control', 'placeholder' => "Segundo Nombre", 'readonly'=>'true')); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Teléfono Celular </label>
                                <?php echo CHtml::textField('cont1_tcelu', $roles['cont1_tcelu'], array('class' => 'form-control', 'placeholder' => "Teléfono Celular")); ?>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Fecha de Nacimiento *</label>
                          
                                <?php echo CHtml::textField('cont1_fnaci', $this->funciones->transformarFecha_v($roles['cont1_fnaci']), array('class' => 'form-control', 'placeholder' => "Fecha de Nacimiento")); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Genero *</label>
                          
                                <?php 
                                    $sql="SELECT * FROM p_genero";
                                    $result=$conexion->createCommand($sql)->queryAll();
                                    $data=CHtml::listData($result,'gener_codig','gener_descr');
                                echo CHtml::dropDownList('cont1_gener', $roles['cont1_gener'], $data, array('class' => 'form-control', 'placeholder' => "Segundo Apellido", 'prompt'=>'Seleccione...')); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Curso *</label>
                                <?php echo CHtml::textField('cont1_curso', $roles['cont1_curso'], array('class' => 'form-control', 'placeholder' => "Curso")); ?>

                        </div>
                    </div>
                   
                </div>
                <div class="row">
                    <div class="col-sm-4 hide">        
                        <div class="form-group">
                            <label>Teléfono de Oficina </label>
                          
                                <?php echo CHtml::textField('cont1_tofic',$roles['cont1_tofic'], array('class' => 'form-control', 'placeholder' => "Teléfono de Oficina")); ?>

                        </div>
                    </div>
                   
                </div>
                
        </div><!-- form -->
    </div>  
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Contacto 2</h3>
        </div>
        <div class="panel-body" >
            
                <div class="row">
                    <div class="col-sm-4 hide ">        
                        <div class="form-group">
                            <label>RUT *</label>
                          
                                <?php echo CHtml::textField('cont2_ndocu', $roles['cont2_ndocu'], array('class' => 'form-control', 'placeholder' => "RUT")); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Primer Nombre *</label>
                          
                                <?php echo CHtml::textField('cont2_pnomb', $roles['cont2_pnomb'], array('class' => 'form-control', 'placeholder' => "Primer Nombre")); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Segundo Nombre</label>
                          
                                <?php echo CHtml::textField('cont2_snomb', $roles['cont2_snomb'], array('class' => 'form-control', 'placeholder' => "Segundo Nombre")); ?>

                        </div>
                    </div>
                   
                
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Primer Apellido *</label>
                          
                                <?php echo CHtml::textField('cont2_papel', $roles['cont2_papel'], array('class' => 'form-control', 'placeholder' => "Primer Apellido")); ?>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Segundo Apellido</label>
                          
                                <?php echo CHtml::textField('cont2_sapel', $roles['cont2_sapel'], array('class' => 'form-control', 'placeholder' => "Segundo Apellido")); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Correo</label>
                                <?php echo CHtml::textField('cont2_corre', $roles['cont2_corre'], array('class' => 'form-control', 'placeholder' => "Correo")); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Teléfono Celular *</label>
                                <?php echo CHtml::textField('cont2_tcelu', $roles['cont2_tcelu'], array('class' => 'form-control', 'placeholder' => "Teléfono Celular")); ?>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Fecha de Nacimiento *</label>
                          
                                <?php echo CHtml::textField('cont2_fnaci', $this->funciones->transformarFecha_v($roles['cont2_fnaci']), array('class' => 'form-control', 'placeholder' => "Fecha de Nacimiento")); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Genero *</label>
                          
                                <?php 
                                    $sql="SELECT * FROM p_genero";
                                    $result=$conexion->createCommand($sql)->queryAll();
                                    $data=CHtml::listData($result,'gener_codig','gener_descr');
                                echo CHtml::dropDownList('cont2_gener', $roles['cont2_gener'], $data, array('class' => 'form-control', 'placeholder' => "Segundo Apellido", 'prompt'=>'Seleccione...')); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Curso *</label>
                                <?php echo CHtml::textField('cont2_curso', $roles['cont2_curso'], array('class' => 'form-control', 'placeholder' => "Curso")); ?>

                        </div>
                    </div>
                   
                </div>
                <div class="row">
                    <div class="col-sm-4 hide">        
                        <div class="form-group">
                            <label>Teléfono de Oficina *</label>
                          
                                <?php echo CHtml::textField('cont2_tofic', $roles['cont2_tofic'], array('class' => 'form-control', 'placeholder' => "Teléfono de Oficina")); ?>

                        </div>
                    </div>
                    
                   
                </div>
        </div><!-- form -->
    </div>  
</div>
<div class="row">                    
    <div class="panel panel-default" >

       
        <div class="panel-body" >
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observaciones</label>
                                <?php echo CHtml::textArea('obser', $roles['regis_obser'], array('class' => 'form-control', 'placeholder' => "Observaciones")); ?>

                        </div>
                    </div>
                </div>
                <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-4 ">
                            <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <button type="reset" class="btn-block btn btn-default">Limpiar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                        </div>
                    </div>
                

            

        </div><!-- form -->
    </div>  
</div>
</form>
<script>
// Date Picker
    $(document).ready(function () {
        jQuery('#cont1_fnaci').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd/mm/yyyy'
        });
        jQuery('#cont2_fnaci').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd/mm/yyyy'
        });
    });
    
    jQuery('#fentr').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'

      });
    jQuery('#freci').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'

      });

    jQuery('#fvenc').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'

      });
    jQuery('#fpago').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'

      });
      
</script>
<script>
    $(document).ready(function () {
        $('#cont1_fnaci').mask('00/00/0000');
        $('#cont2_fnaci').mask('00/00/0000');
    });
</script>
<script>
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                coleg: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Colegio" es obligatorio',
                        }
                    }
                },
                /*pgweb: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Descripción" es obligatorio',
                        }
                    }
                },*/
                telef: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Teléfono" es obligatorio',
                        }
                    }
                },
                direc: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Dirección" es obligatorio',
                        }
                    }
                },
                /*rede1: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Descripción" es obligatorio',
                        }
                    }
                },
                rede2: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Descripción" es obligatorio',
                        }
                    }
                },
                rede3: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Descripción" es obligatorio',
                        }
                    }
                },*/
                cont1_ndocu: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "RUT" es obligatorio',
                        }
                    }
                },
                cont1_pnomb: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Primer Nombre" es obligatorio',
                        }
                    }
                },
                /*cont1_snomb: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Segundo Nombre" es obligatorio',
                        }
                    }
                },*/
                cont1_papel: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Primer Apellido" es obligatorio',
                        }
                    }
                },
                /*cont1_sapel: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Segundo Apellido" es obligatorio',
                        }
                    }
                },*/
                cont1_corre: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Correo" es obligatorio',
                        }
                    }
                },
                cont1_fnaci: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fecha de Nacimiento" es obligatorio',
                        }
                    }
                },
                cont1_gener: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Genero" es obligatorio',
                        }
                    }
                },
                cont1_tcelu: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Teléfono Celular" es obligatorio',
                        }
                    }
                },
                cont1_curso: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Curso" es obligatorio',
                        }
                    }
                },
                cont2_ndocu: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "RUT" es obligatorio',
                        }
                    }
                },
                cont2_pnomb: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Primer Nombre" es obligatorio',
                        }
                    }
                },
                /*cont2_snomb: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Segundo Nombre" es obligatorio',
                        }
                    }
                },*/
                cont2_papel: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Primer Apellido" es obligatorio',
                        }
                    }
                },
                /*cont2_sapel: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Segundo Apellido" es obligatorio',
                        }
                    }
                },*/
                cont2_corre: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Correo" es obligatorio',
                        },
                        emailAddress:{
                            message: 'Estimado(a) Usuario(a) el campo "Correo" debe poseer el siguiente formato: ejemplo@bordadostiempo.cl',  
                        }
                    }
                },
                cont2_fnaci: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fecha de Nacimiento" es obligatorio',
                        }
                    }
                },
                cont2_gener: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Genero" es obligatorio',
                        }
                    }
                },
                cont2_tcelu: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Teléfono Celular" es obligatorio',
                        }
                    }
                },
                cont2_curso: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Curso" es obligatorio',
                        }
                    }
                },
                /*obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Descripción" es obligatorio',
                        }
                    }
                },*/



            }
        });
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'modificar',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>
<script type="text/javascript">
    function ListarColegio(response){
        if (response['success'] == 'true') {
            document.getElementById("pgweb").value = response["pgweb"];
            document.getElementById("telef").value = response["telef"];
            document.getElementById("direc").value = response["direc"];
            document.getElementById("rede1").value = response["rede1"];
            document.getElementById("rede2").value = response["rede2"];
            document.getElementById("rede3").value = response["rede3"];

        } else {
            swal({ 
                title: "Error!",
                text: response['msg'],
                type: "error",
                confirmButtonText: "Cerrar",
                confirmButtonClass: "btn-danger"
            },function(){
                $("#guardar").removeAttr('disabled');
            });
        }
    }
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#cont1_tcelu').mask('+56ZZZZZZZZZZZ', {translation:  {'Z': {pattern: /[0-9]/, optional: true}}});
        $('#cont2_tcelu').mask('+56ZZZZZZZZZZZ', {translation:  {'Z': {pattern: /[0-9]/, optional: true}}});
    });
</script>