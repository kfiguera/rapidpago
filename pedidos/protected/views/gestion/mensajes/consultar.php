<?php $conexion=yii::app()->db; ?>
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Solicitudes</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Gestión de Solicitudes</a></li>
            <li>Mensajes </a></li>
            <li class="active">Consultar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Consultar Mensaje</h3>
        </div>
        <div class="panel-body" >
           
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row hide">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Código *</label>
                            <?php echo CHtml::textField('codig', $solicitud['mensa_codig'], array('class' => 'form-control', 'placeholder' => "Total a Solicitar")); ?>
                        </div>
                    </div>
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Mensaje *</label>
                            <?php echo CHtml::textField('mensa', $solicitud['mensa_cuerp'], array('class' => 'form-control', 'placeholder' => "Total a Solicitar")); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Asunto</label>
                            <?php echo CHtml::textField('asunt', $solicitud['mensa_asunt'], array('class' => 'form-control', 'placeholder' => "Total a Solicitar")); ?>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Estatus</label>
                            <?php 
                                $sql="SELECT * FROM p_parametros_estatus WHERE pesta_codig in (1,2)";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'pesta_codig','pesta_descr');
                                echo CHtml::dropDownList('pesta', $solicitud['pesta_codig'], $data, array('class' => 'form-control', 'placeholder' => "Estatus", 'prompt' => 'Seleccione')); ?>
                        </div>
                    </div>

                     
                    
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Cuerpo</label>
                            <div class="well">
                              <?php 
                              echo base64_decode($solicitud['mensa_cuerp']);
                              ?>  
                            </div>
                        </div>
                    </div>
                </div>
                
                    <!-- Button -->
                    <div class="row controls">
                        
                        <div class="col-sm-12 ">
                            <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                        </div>
                    </div>
            </form>
        </div><!-- form -->
    </div>  
</div>

<script>
$(document).ready(function () {
  
if($("#cuerpo").length > 0){
 tinymce.init({
   selector: "textarea#cuerpo",
   theme: "modern",
   height:300,
   contenteditable:false,
   plugins: [
   "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
   "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
   "save table contextmenu directionality emoticons template paste textcolor"
   ],
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 

 });    
 $( window ).load(function(){         
    tinymce.activeEditor.setMode(mode:readonly);
  });
 
} 

});
</script>