<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Registro y Documentacion</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Gestión de Solicitudes</a></li>
            <li>Mensajes </a></li>
            <li class="active">Listado</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

<?php
//Mostrar mensajes del controlador
        foreach (Yii::app()->user->getFlashes() as $key => $message) {
          echo '<div class="alert alert-' . $key . ' alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              ' . $message  .' </div>';          
        }
$connection = Yii::app()->db;
if(Yii::app()->user->id['usuario']['permiso']==1){
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Criterio de Busqueda</h3>
    </div>
    <div class="panel-body" >
        <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'pedidos', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            

        ?>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Número de Solicitud</label>
                    <?php echo CHtml::textField('numero', '', array('class' => 'form-control', 'placeholder' => "Nro del Solicitud")); ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Estatus</label>
                    <?php 
                        
                        $sql="SELECT * FROM rd_preregistro_estatus";
                        $result=$connection->createCommand($sql)->queryAll();
                        $data=CHtml::listData($result,'epedi_codig','epedi_descr');
                        echo CHtml::dropDownList('epedi', '', $data,array('class' => 'form-control', 'placeholder' => "Descripción", 'prompt'=>'Seleccione...')); ?>
                </div>
            </div>
           
        </div>
        
        <div class="row">
            <div class="col-sm-6">
                <button type="button" id="buscar" class="btn btn-block btn-info">Buscar</button>
            </div>
            <div class="col-sm-6">
                <button type="reset" class="btn btn-block btn-default">Limpiar</button>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div> 
<?php
}
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-sm-8">
                <h3 class="panel-title">Listado</h3>
                
            </div>
            <div class="col-sm-1 col-sm-offset-3">
                <a href="registrar" class=" btn btn-block btn-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Nuevo Mensaje" >Nuevo</a>
            </div>
        </div>
        
    </div>
    <div class="panel-body" >
        
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-pedido'>
                <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                #
                            </th>
                            <th>
                                Asunto
                            </th>
                            <th width="5px">
                                Estatus
                            </th>
                            <th width="5%">
                                &nbsp;
                            </th>
                            <th width="5%">
                                &nbsp;
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        $sql = "SELECT *
                                FROM solicitud_mensajes a
                                JOIN p_parametros_estatus b ON (a.pesta_codig = b.pesta_codig)
                                ORDER BY 1
                                ";    
                        

                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        $j=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                            $j++;
                        ?>
                        <tr>
                            <td><?php echo $i ?></td>

                            <td><?php echo $row['mensa_asunt'] ?></td>
                            <td><?php echo $row['pesta_descr'] ?></td>

                            <td>
                                <a href="consultar?c=<?php echo $row['mensa_codig'] ?>" class="btn btn-block btn-info"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Consultar" >
                                    <i class="fa fa-search"></i>
                                </a>
                            </td>
                            <td>
                                <a href="modificar?c=<?php echo $row['mensa_codig'] ?>" target="_blank" class="btn btn-block btn-info">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </td>
                        </tr>
                        <?php
                            }   
                        ?>
                    </tbody>
                </table>
            </div>

            
        </div>

    </div>
</div>  
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#buscar').click(function () {
        var formData = new FormData($("#pedidos")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-pedido').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>
