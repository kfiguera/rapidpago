<?php $conexion=yii::app()->db; ?>
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Solicitudes</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Gestión de Solicitudes</a></li>
            <li>Mensajes </a></li>
            <li class="active">Modificar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Modificar Mensaje</h3>
        </div>
        <div class="panel-body" >
           
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row hide">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Código *</label>
                            <?php echo CHtml::textField('codig', $solicitud['mensa_codig'], array('class' => 'form-control', 'placeholder' => "Total a Solicitar")); ?>
                        </div>
                    </div>
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Mensaje *</label>
                            <?php echo CHtml::textField('mensa', $solicitud['mensa_cuerp'], array('class' => 'form-control', 'placeholder' => "Total a Solicitar")); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Asunto</label>
                            <?php echo CHtml::textField('asunt', $solicitud['mensa_asunt'], array('class' => 'form-control', 'placeholder' => "Total a Solicitar")); ?>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Estatus</label>
                            <?php 
                                $sql="SELECT * FROM p_parametros_estatus WHERE pesta_codig in (1,2)";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'pesta_codig','pesta_descr');
                                echo CHtml::dropDownList('pesta', $solicitud['pesta_codig'], $data, array('class' => 'form-control', 'placeholder' => "Estatus", 'prompt' => 'Seleccione')); ?>
                        </div>
                    </div>

                     
                    
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Cuerpo</label>
                            <?php echo CHtml::textArea('cuerpo', base64_decode($solicitud['mensa_cuerp']), array('class' => 'form-control', 'placeholder' => "Mensaje")); ?>
                        </div>
                    </div>
                </div>
                
                    <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-4 ">
                            <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                        </div>
                    </div>
            </form>
        </div><!-- form -->
    </div>  
</div>

<script>
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                canti: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Total a Solicitar" es obligatorio',
                        }
                    }
                },
                mcant: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Cantidad de Modelos" es obligatorio',
                        }
                    }
                },
                color: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color del Cuerpo" es obligatorio',
                        }
                    }
                }



            }
        });
    });
    $('#guardar').click(function () {
        $('#mensa').val(tinymce.get('cuerpo').getContent());
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {

            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'modificar',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>
<script>
$(document).ready(function () {
  
if($("#cuerpo").length > 0){
 tinymce.init({
   selector: "textarea#cuerpo",
   theme: "modern",
   height:300,
   plugins: [
   "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
   "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
   "save table contextmenu directionality emoticons template paste textcolor"
   ],
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 

 });    
}  
});
</script>