<table  id='auditoria'  class="table table-bordered table-hover dataTable">
    <thead>
        <tr>
            <th width="2%">#</th>
            <th>Nombre</th> 
            <th>Nombre Personalizado</th> 
            <th>Apodo Espalda</th> 
            <th>Talla</th> 
            <th>Ajuste de Talla</th> 
            <th>Observaciones</th> 
            <th width="5%">Modificar</th>
            <th width="5%">Eliminar</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $sql = "SELECT a.*, b.pdeta_pasos, c.talla_descr 
                FROM pedido_pedidos_detalle_listado a
                JOIN pedido_pedidos_detalle b ON (a.pdeta_codig =b.pdeta_codig)
                JOIN pedido_talla c ON (a.talla_codig = c.talla_codig)
                WHERE a.pedid_codig='".$p."'
                  AND a.pdeta_codig='".$c."'";
                  
        $conexion = Yii::app()->db;
        $command = $conexion->createCommand($sql);
        $p_persona = $command->query();
        $i=0;
        while (($row = $p_persona->read()) !== false) {
            $i++;
            $ajustes=$this->funciones->VerAjusteTalla(json_decode($row['pdlis_tajus']));
        ?>
        <tr>
            <td class="tabla"><?php echo $i ?></td>
            <td class="tabla"><?php echo $row['pdlis_nombr']; ?></td>
            <td class="tabla"><?php echo $row['pdlis_npers']; ?></td>
            <td class="tabla"><?php echo $row['pdlis_aespa']; ?></td>
            <td class="tabla"><?php echo $row['talla_descr']; ?></td>
            <td class="tabla"><?php echo $ajustes; ?></td>
            <td class="tabla"><?php echo $row['pdlis_obser']; ?></td>
            <td class="tabla"><a href="../detalleListado/modificar?p=<?php echo $row['pedid_codig'] ?>&d=<?php echo $row['pdeta_codig'] ?>&s=<?php echo $row['pdeta_pasos'] ?>&c=<?php echo $row['pdlis_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-pencil"></i></a></td>
            <td class="tabla"><a href="../detalleListado/eliminar?p=<?php echo $row['pedid_codig'] ?>&d=<?php echo $row['pdeta_codig'] ?>&s=<?php echo $row['pdeta_pasos'] ?>&c=<?php echo $row['pdlis_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-close"></i></a></td>
        </tr>
        <?php
            }   
        ?>
    </tbody>
</table>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
    });
</script>
