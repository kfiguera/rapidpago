<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Detalle del Pedido</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Pedidos</a></li>
            <li><a href="#">Detalle</a></li>
            <li class="active">Registrar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <div class="row line-steps">
                 <div class="col-md-3 column-step <?php echo $ubicacion[0]; ?>">
                    <a href="modificar?c=<?php echo $solicitud['solic_codig']?>&s=1">
                        <div class="step-number">1 </div>
                    <div class="step-title">Modelo</div>
                    <div class="step-info">Seleccione los datos generales del modelo</div>
                 </div>

                 <div class="col-md-3 column-step <?php echo $ubicacion[1]; ?> ">
                    <div class="step-number">2</div>
                    <div class="step-title">Persona</div>
                    <div class="step-info">Listado de p_personas asociado al modelo</div>
                 </div>
                 <div class="col-md-3 column-step <?php echo $ubicacion[2]; ?>">
                    <div class="step-number">3</div>
                    <div class="step-title">Adicional</div>
                    <div class="step-info">Información adicional a Bordar Parte 1</div>
                 </div>
                 <div class="col-md-3 column-step <?php echo $ubicacion[3]; ?>">
                    <div class="step-number">4</div>
                    <div class="step-title">Adicional</div>
                    <div class="step-info">Información adicional a Bordar Parte 2</div>
                 </div>
              </div>
        </div>
    </div>
</div>
<form id='login-form' name='login-form' method="post">
<div class="row">   
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Detalles de la información a bordar</h3>
        </div>
        <div class="panel-body" >
           
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
                <div class="row hide">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Pedido *</label>
                            <?php 
                                echo CHtml::hiddenField('pedid', $p, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Código *</label>
                            <?php 
                                echo CHtml::hiddenField('codig', $c, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Paso *</label>
                            <?php 
                                echo CHtml::hiddenField('pasos', $pedidos['pdeta_pasos'], array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <label>Nombre Personalizado</label>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Fuente *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.fuent_codig, fuent_descr
                                      FROM pedido_fuente a
                                      GROUP BY 1,2
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                echo CHtml::dropDownList('npfue', $pedidos['pdeta_npfue'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-1" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                      FROM pedido_modelo_color a
                                      WHERE a.tmode_codig = '".$pedidos['tmode_codig']."'
                                      GROUP BY 1,2
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                                echo CHtml::dropDownList('npcol', $pedidos['pdeta_npcol'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-2" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Posición *</label>
                            <?php 
                                
                                $data=array('1'=>'DERECHO', '2'=>'IZQUIERDO', '4'=>'CENTRO');
                                echo CHtml::dropDownList('nppos', $pedidos['pdeta_nppos'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                            <span class="help-block">
                                <small>Nota: indicar teniendo el poleron puesto</small>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <label>Apodo Espalda</label>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Fuente *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.fuent_codig, fuent_descr
                                      FROM pedido_fuente a
                                      GROUP BY 1,2
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                echo CHtml::dropDownList('aefue', $pedidos['pdeta_aefue'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-3" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                      FROM pedido_modelo_color a
                                      WHERE a.tmode_codig = '".$pedidos['tmode_codig']."'
                                      GROUP BY 1,2
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                                echo CHtml::dropDownList('aecol', $pedidos['pdeta_aecol'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-4" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Posición *</label>
                            <?php 
                                
                                $data=array('1'=>'DERECHO', '2'=>'IZQUIERDO', '3'=>'AMBOS');
                                echo CHtml::dropDownList('aepos', $pedidos['pdeta_aepos'], $data, array('ajax' => array(
                                            'type' => 'POST',
                                            'url' => CController::createUrl('funciones/PedidosModelos'), // Controlador que devuelve las p_ciudades relacionadas
                                            'update' => '#model', // id del item que se actualizará
                                        ),'class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    
                </div>
                

            

        </div><!-- form -->
    </div>  
</div>
<div class="row">   
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Pecho</h3>
        </div>
        <div class="panel-body" >
           
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Posición *</label>
                            <?php 
                                
                                $data=array('1'=>'DERECHO', '2'=>'IZQUIERDO', '3'=>'AMBOS');
                                echo CHtml::dropDownList('pposi', $pedidos['pdeta_pposi'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <?php
                    switch ($pedidos['pdeta_pposi']) {
                        case '1':
                            $opcional['1']['disabled']='';
                            $opcional['2']['disabled']='true';
                            break;
                        case '2':
                            $opcional['1']['disabled']='true';
                            $opcional['2']['disabled']='';
                            break;
                        case '3':
                            $opcional['1']['disabled']='';
                            $opcional['2']['disabled']='';
                            break;
                        
                        default:
                            $opcional['1']['disabled']='true';
                            $opcional['2']['disabled']='true';
                            break;
                    }
                    ?>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Texto a Bordar Derecha *</label>
                            <?php 
                                
                                
                                echo CHtml::textField('ptder', $pedidos['pdeta_ptder'], array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['1']['disabled'])); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Texto a Bordar Izquierda *</label>
                            <?php 
                                
                                
                                echo CHtml::textField('ptizq', $pedidos['pdeta_ptizq'], array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['2']['disabled'])); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Fuente *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.fuent_codig, fuent_descr
                                      FROM pedido_fuente a
                                      GROUP BY 1,2
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                echo CHtml::dropDownList('pfuen', $pedidos['pdeta_pfuen'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-5" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Color *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                      FROM pedido_modelo_color a
                                      WHERE a.tmode_codig = '".$pedidos['tmode_codig']."'
                                      GROUP BY 1,2
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                                echo CHtml::dropDownList('pcolo', $pedidos['pdeta_pcolo'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-6" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>

                    
                    
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observaciones</label>
                            <?php 
                                echo CHtml::textArea('pobse', $pedidos['pdeta_pobse'], array('class' => 'form-control', 'placeholder' => "Observaciones",'prompt'=>'Seleccione...')); ?>
                                
                               
                        </div>
                    </div>
                    
                </div>
                

            

        </div><!-- form -->
    </div>  
</div>
<?php
$sql="SELECT * FROM pedido_modelo WHERE model_codig ='".$pedidos['model_codig']."'";
$modelo=$conexion->createCommand($sql)->queryRow();
switch ($modelo['model_gorro']) {
    case '1':
        $gorro='';
        break;
    
    default:
        $gorro='true';
        break;
}
?>

<div class="row">   
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Gorro</h3>
        </div>
        <div class="panel-body" >
           
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Posición *</label>
                            <?php 
                                
                                $data=array('1'=>'DERECHO', '2'=>'IZQUIERDO', '3'=>'AMBOS');
                                echo CHtml::dropDownList('gposi', $pedidos['pdeta_gposi'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$gorro)); ?>
                        </div>
                    </div>
                    
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Fuente *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.fuent_codig, fuent_descr
                                      FROM pedido_fuente a
                                      GROUP BY 1,2
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                echo CHtml::dropDownList('gfuen', $pedidos['pdeta_gfuen'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$gorro)); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-7" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                      FROM pedido_modelo_color a
                                      WHERE a.tmode_codig = '".$pedidos['tmode_codig']."'
                                      GROUP BY 1,2
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                                echo CHtml::dropDownList('gcolo', $pedidos['pdeta_gcolo'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$gorro)); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-8" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>

                    
                    
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observaciones</label>
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.fuent_codig, fuent_descr
                                      FROM pedido_fuente a
                                      GROUP BY 1,2
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                echo CHtml::textArea('gobse', $pedidos['pdeta_gobse'], array('class' => 'form-control', 'placeholder' => "Observaciones",'prompt'=>'Seleccione...','disabled'=>$gorro)); ?>
                                
                               
                        </div>
                    </div>
                    
                </div>
                

            

        </div><!-- form -->
    </div>  
</div>
<div class="row">                    
    <div class="panel panel-default" >
        <div class="panel-body">
            <div class="row controls">
                <div class="col-sm-4     ">
                    <a href="modificar?p=<?php echo $p?>&c=<?php echo $c?>&s=2" type="reset" class="btn-block btn btn-default">Volver </a>
                </div>
                <div class="col-sm-4 ">
                        <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                    </div>
                <div class="col-sm-4 ">
                    <button id="guardar" type="button" class="btn-block btn btn-info">Siguiente  </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hide" id="popover-img">
    <?php
        $sql="SELECT * FROM pedido_fuente where fuent_codig = '".$pedidos['pdeta_npfue']."'";
        $fuente=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$fuente['fuent_ruta'] ?>" class="img-responsive"></div>
<div class="hide" id="popover-img-2">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pdeta_npcol']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive">
</div>
<div class="hide" id="popover-img-3">
    <?php
        $sql="SELECT * FROM pedido_fuente where fuent_codig = '".$pedidos['pdeta_aefue']."'";
        $fuente=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$fuente['fuent_ruta'] ?>" class="img-responsive"></div>
<div class="hide" id="popover-img-4">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pdeta_aecol']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive">
</div>
<div class="hide" id="popover-img-5">
    <?php
        $sql="SELECT * FROM pedido_fuente where fuent_codig = '".$pedidos['pdeta_pfuen']."'";
        $fuente=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$fuente['fuent_ruta'] ?>" class="img-responsive"></div>
<div class="hide" id="popover-img-6">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pdeta_pcolo']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive">
</div>
<div class="hide" id="popover-img-7">
    <?php
        $sql="SELECT * FROM pedido_fuente where fuent_codig = '".$pedidos['pdeta_gfuen']."'";
        $fuente=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$fuente['fuent_ruta'] ?>" class="img-responsive"></div>
<div class="hide" id="popover-img-8">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pdeta_gcolo']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive">
</div>
</form>
<script>
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                                                npfue: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fuente" es obligatorio',
                        }
                    }
                },
                npcol: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color" es obligatorio',
                        }
                    }
                },
                nppos: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Posición" es obligatorio',
                        }
                    }
                },
                aefue: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fuente" es obligatorio',
                        }
                    }
                },
                aecol: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color" es obligatorio',
                        }
                    }
                },
                aepos: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Posición" es obligatorio',
                        }
                    }
                },
                pposi: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Posición" es obligatorio',
                        }
                    }
                },
                ptder: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar Derecha" es obligatorio',
                        }
                    }
                },
                ptizq: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar Izquierda" es obligatorio',
                        }
                    }
                },
                pfuen: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fuente" es obligatorio',
                        }
                    }
                },
                pcolo: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color" es obligatorio',
                        }
                    }
                },
                gposi: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Posición" es obligatorio',
                        }
                    }
                },
                gfuen: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fuente" es obligatorio',
                        }
                    }
                },
                gcolo: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color" es obligatorio',
                        }
                    }
                },



            }
        });
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'paso3',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        /*swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){*/
                            window.open('modificar?p=<?php echo $p; ?>&s=4&c=<?php echo $c; ?>', '_parent');
                        /*});*/
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>
<script type="text/javascript">
    $('#pposi').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '1':
                jQuery("#ptder").removeAttr('disabled');
                jQuery("#ptizq").attr('disabled','true');
                break;
            case '2':
                jQuery("#ptizq").removeAttr('disabled');
                jQuery("#ptder").attr('disabled','true');
                break;
            case '3':
                jQuery("#ptder").removeAttr('disabled');
                jQuery("#ptizq").removeAttr('disabled');
                break;
            default:
                jQuery("#ptder").attr('disabled','true');
                jQuery("#ptizq").attr('disabled','true');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#iopci').change(function () {
        var iopci = $(this).val();
        $.ajax({
            'type':'POST',
            'data':{'tmode':tmode.value},
            'url':'<?php echo CController::createUrl('funciones/PedidosColorOpcional'); ?>',
            'cache':false,
            'success':function(html){
                switch(iopci) {
                    case '1':
                        
                        jQuery("#opcional-1").removeClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#iclin").html(html);
                        jQuery("#iccie").html('');
                        jQuery("#icviv").html('');
                        jQuery("#ideta").removeAttr('disabled');
                        jQuery("#iclin").removeAttr('disabled');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").attr('disabled','true');

                        break;
                    case '2':
                        jQuery("#opcional-2").removeClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#iclin").html('');
                        jQuery("#iccie").html(html);
                        jQuery("#icviv").html('');
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").removeAttr('disabled');
                        jQuery("#icviv").attr('disabled','true');
                        break;
                    case '3':
                        jQuery("#opcional-3").removeClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#iclin").html('');
                        jQuery("#iccie").html('');
                        jQuery("#icviv").html(html);
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").removeAttr('disabled');
                        break;
                    default:
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#iclin").html(html);
                        jQuery("#iccie").html(html);
                        jQuery("#icviv").html(html);
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").attr('disabled','true');
                        break;
                }
                
            }
        });
    });
</script>
<script type="text/javascript">
    $('#ideta').change(function () {
        var ideta = $(this).val();
        switch(ideta) {
            case '1':
                jQuery("#iclin").removeAttr('disabled');
                break;
            default:
                jQuery("#iclin").attr('disabled','true');
                break;
        }
                
            
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-1').popover({
          html: true,
          content: function() {
            return $('#popover-img').html();
          },
          trigger: 'hover'
        });
});
$('#npfue').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerFuente'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-2').popover({
          html: true,
          content: function() {
            return $('#popover-img-2').html();
          },
          trigger: 'hover'
        });
});
$('#npcol').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-2").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-3').popover({
          html: true,
          content: function() {
            return $('#popover-img-3').html();
          },
          trigger: 'hover'
        });
});
$('#aefue').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerFuente'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-3").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-4').popover({
          html: true,
          content: function() {
            return $('#popover-img-4').html();
          },
          trigger: 'hover'
        });
});
$('#aecol').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-4").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-5').popover({
          html: true,
          content: function() {
            return $('#popover-img-5').html();
          },
          trigger: 'hover'
        });
});
$('#pfuen').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerFuente'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-5").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-6').popover({
          html: true,
          content: function() {
            return $('#popover-img-6').html();
          },
          trigger: 'hover'
        });
});
$('#pcolo').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-6").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-7').popover({
          html: true,
          content: function() {
            return $('#popover-img-7').html();
          },
          trigger: 'hover'
        });
});
$('#gfuen').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerFuente'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-7").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-8').popover({
          html: true,
          content: function() {
            return $('#popover-img-8').html();
          },
          trigger: 'hover'
        });
});
$('#gcolo').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-8").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>