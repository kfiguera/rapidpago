<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Detalle del Pedido</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Pedidos</a></li>
            <li><a href="#">Detalle</a></li>
            <li class="active">Registrar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <div class="row line-steps">
                <div class="col-md-6 column-step start active">
                    <a href="modificar?c=<?php echo $solicitud['solic_codig']?>&s=1">
                        <div class="step-number">1 </div>
                    <div class="step-title">Modelo</div>
                    <div class="step-info">Seleccione los datos generales del modelo</div>
                 </div>

                 <div class="col-md-6 column-step finish ">
                    <div class="step-number">2</div>
                    <div class="step-title">Persona</div>
                    <div class="step-info">Listado de p_personas asociado al modelo</div>
                 </div>
              </div>
        </div>
        <div class="panel-body" >
           
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row hide">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Pedido *</label>
                            <?php 
                                echo CHtml::hiddenField('pedid', $p, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Tipo *</label>
                            <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT tmode_codig, tmode_descr
                                      FROM pedido_modelo_tipo 
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'tmode_codig','tmode_descr');
                                echo CHtml::dropDownList('tmode', '', $data, array('ajax' => array(
                                            'type' => 'POST',
                                            'url' => CController::createUrl('funciones/PedidosModelos'), // Controlador que devuelve las p_ciudades relacionadas
                                            'update' => '#model', // id del item que se actualizará
                                        ),'class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Modelo *</label>
                            <?php 
                                echo CHtml::dropDownList('model', '', '', array('ajax' => array(
                                            'type' => 'POST',
                                            'url' => CController::createUrl('funciones/PedidosCategoria'), // Controlador que devuelve las p_ciudades relacionadas
                                            'update' => '#categ', // id del item que se actualizará
                                        ),'class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Categoria *</label>
                            <?php 
                                
                                echo CHtml::dropDownList('categ', '', '', array('ajax' => array(
                                            'type' => 'POST',
                                            'url' => CController::createUrl('funciones/PedidosOpcionales'), // Controlador que devuelve las p_ciudades relacionadas
                                            'update' => '#iopci', // id del item que se actualizará
                                        ),'class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 hide">        
                        <div class="form-group">
                            <label>Variante *</label>
                            <?php 
                                echo CHtml::textField('varia', '1', $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    
                     <!--div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color del Cuerpo *</label>
                            <div class="input-group">
                            <?php 
                                echo CHtml::dropDownList('color', '', '', array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-1" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div-->
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Cantidad a Solicitar *</label>
                            <?php echo CHtml::textField('canti', '', array('class' => 'form-control', 'placeholder' => "Cantidad a Solicitar")); ?>
                        </div>
                    </div>
                    <div class="col-sm-4 hide">        
                        <div class="form-group">
                            <label>Adicional con costo *</label>
                            <?php 
                                $data=array('1'=>'SI','2'=>'NO');
                                echo CHtml::dropDownList('opcio', '1', $data, array('ajax' => array(
                                            'type' => 'POST',
                                            'url' => CController::createUrl('funciones/PedidosOpcionales'), // Controlador que devuelve las p_ciudades relacionadas
                                            'update' => '#iopci', // id del item que se actualizará
                                        ),'class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Indicar Adicional *</label>
                            <?php 
                                echo CHtml::dropDownList('iopci', '', '', array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Indicar Segundo Adicional *</label>
                            <?php 

                                $data=$this->funciones->ListarOpcionales2($pedidos);

                                echo CHtml::dropDownList('iopc2', $pedidos['mopci_codi2'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['0']['disabled'])); ?>
                        </div>
                    </div>
                </div>
                <!-- OPCIONALES-->
                <div class="row hide">

                    
                    
                    <div class="hide" id="opcional-1">
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Indicar Detalles *</label>
                                <?php
                                    $data=array('1'=>'CON LINEA','2'=>'SIN LINEA'); 
                                    echo CHtml::dropDownList('ideta', '', $data, array('ajax' => array(
                                            'type' => 'POST',
                                            'url' => CController::createUrl('funciones/PedidosColorLinea'), // Controlador que devuelve las p_ciudades relacionadas
                                            'update' => '#iclin', // id del item que se actualizará
                                        ),'class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                            </div>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Indicar Color del la Linea *</label>
                                <div class="input-group">
                                    <?php

                                        echo CHtml::dropDownList('iclin', '', '', array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                    <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-2" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>  
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="hide" id="opcional-2">
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Indicar Color del Cierre *</label>
                                <div class="input-group">
                                <?php

                                    echo CHtml::dropDownList('iccie', '', '', array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                    <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-3" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>  
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="hide" id="opcional-3">
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Indicar Color del Vivo Bolsillo *</label>
                                <div class="input-group">
                                <?php

                                    echo CHtml::dropDownList('icviv', '', '', array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-4" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- FIN OPCIONALES-->
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Vista Previa *</label>
                            <button class="btn btn-block btn-info" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img_1" data-original-title="" title="Vista previa"><i class="fa fa-search"></i></button>
                            
                        </div>
                    </div>
                    <div class="col-sm-8">        
                        <div class="form-group">
                            <label>Mensaje</label>
                            <?php echo CHtml::textArea('mensa', '', array('class' => 'form-control', 'placeholder' => "Mensaje")); ?>
                        </div>
                    </div>
                </div>
                
                <!-- Button -->
                <div class="row controls">
                    <div class="col-sm-4 ">
                        <a href="listado?p=<?php echo $p?>" type="reset" class="btn-block btn btn-default">Volver </a>
                    </div>
                    <div class="col-sm-4 ">
                        <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                    </div>
                    <div class="col-sm-4 ">
                        <button id="guardar" type="button" class="btn-block btn btn-info">Siguiente  </button>
                    </div>
                </div>

            </form>

        </div><!-- form -->
    </div>  
</div>
<div class="hide" id="popover-img"></div>
<div class="hide" id="popover-img-2"></div>
<div class="hide" id="popover-img-3"></div>
<div class="hide" id="popover-img-4"></div>
<script type="text/javascript">
    jQuery('body').on('change','#categ',function(){
        jQuery.ajax({
            'type':'POST',
            'url':'<?php echo CController::createUrl('funciones/PedidosOpcionales2'); ?>',
            'cache':false,
            'data':jQuery(this).parents("form").serialize(),
            'success':function(html){
                jQuery("#iopc2").html(html)
            }
        });
        return false;
    });
</script>
<script>
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                tmode: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo" es obligatorio',
                        }
                    }
                },
                model: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Modelo" es obligatorio',
                        }
                    }
                },
                categ: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Categoria" es obligatorio',
                        }
                    }
                },
                varia: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Variante" es obligatorio',
                        }
                    }
                },
                color: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color del Cuerpo" es obligatorio',
                        }
                    }
                },
                canti: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Cantidad a Solicitar" es obligatorio',
                        }
                    }
                },
                opcio: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Opcional" es obligatorio',
                        }
                    }
                },
                iopci: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Indicar Opcional" es obligatorio',
                        }
                    }
                },
                /*ideta: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Indicar Detalles" es obligatorio',
                        }
                    }
                },
                iclin: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color del la Linea" es obligatorio',
                        }
                    }
                },
                iccie: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color del Cierre" es obligatorio',
                        }
                    }
                },
                icviv: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color del Vivo Bolsillo" es obligatorio',
                        }
                    }
                },*/



            }
        });
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'registrar',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('modificar?p=<?php echo $p; ?>&s=2&c='+response['c'], '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>
<script type="text/javascript">
    $('#opcio').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '1':
                jQuery("#iopci").removeAttr('disabled');
                break;
            default:
                jQuery("#opcional-3").addClass('hide');
                jQuery("#opcional-2").addClass('hide');
                jQuery("#opcional-1").addClass('hide');
                jQuery("#iopci").html('');
                jQuery("#iclin").html('');
                jQuery("#iccie").html('');
                jQuery("#icviv").html(''); 
                jQuery("#iopci").attr('disabled','true');
                jQuery("#ideta").attr('disabled','true');
                jQuery("#iclin").attr('disabled','true');
                jQuery("#iccie").attr('disabled','true');
                jQuery("#icviv").attr('disabled','true');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#iopci').change(function () {
        var iopci = $(this).val();
        $.ajax({
            'type':'POST',
            'data':{'tmode':tmode.value},
            'url':'<?php echo CController::createUrl('funciones/PedidosColorOpcional'); ?>',
            'cache':false,
            'success':function(html){
                switch(iopci) {
                    case '1':
                        
                        jQuery("#opcional-1").removeClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#iclin").html(html);
                        jQuery("#iccie").html('');
                        jQuery("#icviv").html('');
                        jQuery("#ideta").removeAttr('disabled');
                        jQuery("#iclin").removeAttr('disabled');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").attr('disabled','true');

                        break;
                    case '2':
                        jQuery("#opcional-2").removeClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#iclin").html('');
                        jQuery("#iccie").html(html);
                        jQuery("#icviv").html('');
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").removeAttr('disabled');
                        jQuery("#icviv").attr('disabled','true');
                        break;
                    case '3':
                        jQuery("#opcional-3").removeClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#iclin").html('');
                        jQuery("#iccie").html('');
                        jQuery("#icviv").html(html);
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").removeAttr('disabled');
                        break;
                    default:
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#iclin").html(html);
                        jQuery("#iccie").html(html);
                        jQuery("#icviv").html(html);
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").attr('disabled','true');
                        break;
                }
                
            }
        });
    });
</script>
<script type="text/javascript">
    $('#ideta').change(function () {
        var ideta = $(this).val();
        switch(ideta) {
            case '1':
                jQuery("#iclin").removeAttr('disabled');
                break;
            default:
                jQuery("#iclin").attr('disabled','true');
                break;
        }
                
            
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img_1').popover({
          html: true,
          content: function() {
            return $('#popover-img').html();
          },
          trigger: 'hover'
        });
});
$('#categ').change(function () {
        $('[data-toggle=popover]').popover('hide');
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerImagenModelo'); ?>",  
            method:"POST",  
            async:false,  
            data: $('#login-form').serialize(), 
            success:function(data){  
                jQuery("#popover-img").html(data);  
                $('[data-toggle=popover]').popover('show');
            }  
        });
        
        
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-2').popover({
          html: true,
          content: function() {
            return $('#popover-img-2').html();
          },
          trigger: 'hover'
        });
});
$('#iclin').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-2").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-3').popover({
          html: true,
          content: function() {
            return $('#popover-img-3').html();
          },
          trigger: 'hover'
        });
});
$('#iccie').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-3").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-4').popover({
          html: true,
          content: function() {
            return $('#popover-img-4').html();
          },
          trigger: 'hover'
        });
});
$('#icviv').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-4").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>