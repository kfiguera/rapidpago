<style>
    .image-preview-input {
        position: relative;
        overflow: hidden;
        margin: 0px;    
        color: #333;
        background-color: #fff;
        border-color: #ccc;    
    }
    .image-preview-input input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }
    .image-preview-input-title {
        margin-left:2px;
    }
</style> 
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Detalle del Pedido</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Pedidos</a></li>
            <li><a href="#">Detalle</a></li>
            <li class="active">Registrar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <div class="row line-steps">
                 <div class="col-md-3 column-step <?php echo $ubicacion[0]; ?>">
                    <a href="modificar?c=<?php echo $solicitud['solic_codig']?>&s=1">
                        <div class="step-number">1 </div>
                    <div class="step-title">Modelo</div>
                    <div class="step-info">Seleccione los datos generales del modelo</div>
                 </div>

                 <div class="col-md-3 column-step <?php echo $ubicacion[1]; ?> ">
                    <div class="step-number">2</div>
                    <div class="step-title">Persona</div>
                    <div class="step-info">Listado de p_personas asociado al modelo</div>
                 </div>
                 <div class="col-md-3 column-step <?php echo $ubicacion[2]; ?>">
                    <div class="step-number">3</div>
                    <div class="step-title">Adicional</div>
                    <div class="step-info">Información adicional a Bordar Parte 1</div>
                 </div>
                 <div class="col-md-3 column-step <?php echo $ubicacion[3]; ?>">
                    <div class="step-number">4</div>
                    <div class="step-title">Adicional</div>
                    <div class="step-info">Información adicional a Bordar Parte 2</div>
                 </div>
              </div>
        </div>
    </div>
</div>
<form id='login-form' name='login-form' method="post">
<div class="row">   
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Mangas</h3>
        </div>
        <div class="panel-body" >
           
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
                <div class="row hide">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Pedido *</label>
                            <?php 
                                echo CHtml::hiddenField('pedid', $p, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Código *</label>
                            <?php 
                                echo CHtml::hiddenField('codig', $c, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Paso *</label>
                            <?php 
                                echo CHtml::hiddenField('pasos', $pedidos['pdeta_pasos'], array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Posición *</label>
                            <?php 
                                
                                $data=array('1'=>'DERECHO', '2'=>'IZQUIERDO', '3'=>'AMBOS');
                                echo CHtml::dropDownList('mposi', $pedidos['pdeta_mposi'], $data, array('class' => 'form-control', 'placeholder' => "Posición",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <?php
                    switch ($pedidos['pdeta_mposi']) {
                        case '1':
                            $opcional['1']['disabled']='';
                            $opcional['2']['disabled']='true';
                            break;
                        case '2':
                            $opcional['1']['disabled']='true';
                            $opcional['2']['disabled']='';
                            break;
                        case '3':
                            $opcional['1']['disabled']='';
                            $opcional['2']['disabled']='';
                            break;
                        
                        default:
                            $opcional['1']['disabled']='true';
                            $opcional['2']['disabled']='true';
                            break;
                    }
                    ?>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Texto a Bordar Derecha *</label>
                            <?php 
                                
                                
                                echo CHtml::textField('mtder', $pedidos['pdeta_mtder'], array('class' => 'form-control', 'placeholder' => "Texto a Bordar Derecha",'prompt'=>'Seleccione...','disabled'=>$opcional['1']['disabled'])); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Texto a Bordar Izquierda *</label>
                            <?php 
                                
                                
                                echo CHtml::textField('mtizq', $pedidos['pdeta_mtizq'], array('class' => 'form-control', 'placeholder' => "Texto a Bordar Izquierda",'prompt'=>'Seleccione...','disabled'=>$opcional['2']['disabled'])); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Fuente *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.fuent_codig, fuent_descr
                                      FROM pedido_fuente a
                                      GROUP BY 1,2
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                echo CHtml::dropDownList('mfuen', $pedidos['pdeta_mfuen'], $data, array('class' => 'form-control', 'placeholder' => "Fuente",'prompt'=>'Seleccione...')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-1" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                      FROM pedido_modelo_color a
                                      WHERE a.tmode_codig = '".$pedidos['tmode_codig']."'
                                      GROUP BY 1,2
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                                echo CHtml::dropDownList('mcolo', $pedidos['pdeta_mcolo'], $data, array('class' => 'form-control', 'placeholder' => "Color",'prompt'=>'Seleccione...')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-2" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Lleva Imagen Bordada *</label>
                            <?php 
                                
                                $data=array('1'=>'SI', '2'=>'NO');
                                echo CHtml::dropDownList('limag', $pedidos['pdeta_limag'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    
                    
                </div>
                <?php
                if($pedidos['pdeta_limag']=='1'){
                    $opcional['3']['hide']='';
                }else{
                    $opcional['3']['hide']='hide';  
                }
                ?>
                <div class="row <?php echo $opcional['3']['hide'] ?>" id="opcional">
                        <div class="col-sm-12">        
                            <div class="numero1 form-group">
                                <label>Imagen Libre</label>
                                <div  class="input-group image-preview" data-placement="top" >  

                                    <img id="dynamic">
                                    <!-- image-preview-filename input [CUT FROM HERE]-->
                                    <?php 
                                        $ruta=$pedidos['pdeta_mrima'];
                                        $nombre=explode('/', $ruta);
                                        $nombre=end($nombre);
                                        if($nombre){
                                            $opcional['3']['hide']='';
                                            $opcional['3']['show']='true';
                                        }else{
                                            $opcional['3']['hide']='hide';
                                            $opcional['3']['show']='false';
                                        }
                                    ?>
                                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="true" value="<?php echo $nombre ?>" > <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-btn">
                                        <!-- image-preview-clear button -->
                                        <button type="button" class="btn btn-default image-preview-clear <?php echo $opcional['3']['hide'] ?>" >
                                            <span class="fa fa-times"></span> Limpiar
                                        </button>
                                        <!-- image-preview-input -->
                                        <div class="btn btn-default image-preview-input">
                                            <span class="fa fa-folder-open"></span>
                                            <span class="numero1 image-preview-input-title">Buscar</span>
                                            <input type="file" id="mrima" name="mrima"/> <!-- rename it -->
                                        </div>
                                    </span>
                               
                                </div> 
                            </div> 
                            <div class="hide" id="popover-numero-1">
                                <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$ruta ?>" class="img-responsive">
                            </div>
                            <?php
                                if($opcional['3']['show']=='true') { 
                            ?>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    var closebtn = $('<button/>', {
                                        type: "button",
                                        text: 'x',
                                        id: 'close-preview',
                                        style: 'font-size: initial;',
                                    });
                                    closebtn.attr("class","close pull-right");
                                    $('.numero1 .image-preview').popover({
                                      html: true,
                                      title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
                                      content: function() {
                                        return $('#popover-numero-1').html();
                                      },
                                      trigger: 'manual'
                                    });

                                    $(".numero1 .image-preview").popover("show");
                                });
                            </script>
                            <?php
                                }
                            ?>
                        </div>
                        
                    </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observaciones</label>
                            <?php 
                                echo CHtml::textArea('mobse', $pedidos['pdeta_mobse'], array('class' => 'form-control', 'placeholder' => "Observaciones",'prompt'=>'Seleccione...')); ?>
                                
                               
                        </div>
                    </div>
                    
                </div>
        </div><!-- form -->
    </div>  
</div>
<div class="row">   
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Espalda</h3>
        </div>
        <div class="panel-body" >
           
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
                <div class="row">
                    <div class="col-sm-12"> 
                        <label>Alto *</label>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Texto a Bordar *</label>
                            <?php 
                                
                                echo CHtml::textField('eatbo', $pedidos['pdeta_eatbo'], array('class' => 'form-control', 'placeholder' => "Texto a Bordar",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Fuente *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.fuent_codig, fuent_descr
                                      FROM pedido_fuente a
                                      GROUP BY 1,2
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                echo CHtml::dropDownList('eafue', $pedidos['pdeta_eafue'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-3" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                      FROM pedido_modelo_color a
                                      WHERE a.tmode_codig = '".$pedidos['tmode_codig']."'
                                      GROUP BY 1,2
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                                echo CHtml::dropDownList('eacol', $pedidos['pdeta_eacol'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-4" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"> 
                        <label>Bajo *</label>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Texto a Bordar *</label>
                            <?php 
                                
                                echo CHtml::textField('ebtbo', $pedidos['pdeta_ebtbo'], array('class' => 'form-control', 'placeholder' => "Texto a Bordar",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Fuente *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.fuent_codig, fuent_descr
                                      FROM pedido_fuente a
                                      GROUP BY 1,2
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                echo CHtml::dropDownList('ebfue', $pedidos['pdeta_ebfue'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-5" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                      FROM pedido_modelo_color a
                                      WHERE a.tmode_codig = '".$pedidos['tmode_codig']."'
                                      GROUP BY 1,2
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                                echo CHtml::dropDownList('ebcol', $pedidos['pdeta_ebcol'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-6" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"> 
                        <label>Apodo Espalda *</label>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Texto a Bordar *</label>
                            <?php 
                                
                                echo CHtml::textField('aetbo', $pedidos['pdeta_aetbo'], array('class' => 'form-control', 'placeholder' => "Texto a Bordar",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Fuente *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.fuent_codig, fuent_descr
                                      FROM pedido_fuente a
                                      GROUP BY 1,2
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                echo CHtml::dropDownList('aefue', $pedidos['pdeta_aefue'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-7" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                      FROM pedido_modelo_color a
                                      WHERE a.tmode_codig = '".$pedidos['tmode_codig']."'
                                      GROUP BY 1,2
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                                echo CHtml::dropDownList('aecol', $pedidos['pdeta_aecol'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-8" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                            <div class="numero2 form-group">
                                <label>Imagen Principal</label>
                                <div  class="input-group image-preview" data-placement="top" >  

                                    <img id="dynamic">
                                    <!-- image-preview-filename input [CUT FROM HERE]-->
                                    <?php 
                                        $ruta=$pedidos['pdeta_iprut'];
                                        $nombre=explode('/', $ruta);
                                        $nombre=end($nombre);
                                        if($nombre){
                                            $opcional['4']['hide']='';
                                            $opcional['4']['show']='true';
                                        }else{
                                            $opcional['4']['hide']='hide';
                                            $opcional['4']['show']='false';
                                        }
                                    ?>
                                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="true" value="<?php echo $nombre ?>" > <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-btn">
                                        <!-- image-preview-clear button -->
                                        <button type="button" class="btn btn-default image-preview-clear <?php echo $opcional['4']['hide']; ?>" >
                                            <span class="fa fa-times"></span> Limpiar
                                        </button>
                                        <!-- image-preview-input -->
                                        <div class="btn btn-default image-preview-input">
                                            <span class="fa fa-folder-open"></span>
                                            <span class="numero2 image-preview-input-title">Buscar</span>
                                            <input type="file" id="iprut" name="iprut"/> <!-- rename it -->
                                        </div>
                                    </span>
                               
                                </div> 
                            </div> 
                            <div class="hide" id="popover-numero-2">
                                <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$ruta ?>" class="img-responsive">
                            </div>
                            <?php
                                if($opcional['4']['show']=='true') { 
                            ?>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    var closebtn = $('<button/>', {
                                        type: "button",
                                        text: 'x',
                                        id: 'close-preview',
                                        style: 'font-size: initial;',
                                    });
                                    closebtn.attr("class","close pull-right");
                                    $('.numero2 .image-preview').popover({
                                      html: true,
                                      title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
                                      content: function() {
                                        return $('#popover-numero-2').html();
                                      },
                                      trigger: 'manual'
                                    });

                                    $(".numero2 .image-preview").popover("show");
                                });
                            </script>
                            <?php
                                }
                            ?>
                        </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observaciones</label>
                            <?php 
                                echo CHtml::textArea('eobse', $pedidos['pdeta_eobse'], array('class' => 'form-control', 'placeholder' => "Observaciones",'prompt'=>'Seleccione...')); ?>
                                
                               
                        </div>
                    </div>
                    
                </div>
                

            

        </div><!-- form -->
    </div>  
</div>
<div class="row">   
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Dibujo simple del poleron completo por ambos lados</h3>
        </div>
        <div class="panel-body" >
           
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
                <div class="row">
                    <div class="col-sm-6">        
                            <div class="numero3 form-group">
                                <label>Imagen Frontal</label>
                                <div  class="input-group image-preview" data-placement="top" >  

                                    <img id="dynamic">
                                    <!-- image-preview-filename input [CUT FROM HERE]-->
                                    <?php 
                                        $ruta=$pedidos['pdeta_rifro'];
                                        $nombre=explode('/', $ruta);
                                        $nombre=end($nombre);
                                        if($nombre){
                                            $opcional['5']['hide']='';
                                            $opcional['5']['show']='true';
                                        }else{
                                            $opcional['5']['hide']='hide';
                                            $opcional['5']['show']='false';
                                        }
                                    ?>
                                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="true" value="<?php echo $nombre ?>" > <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-btn">
                                        <!-- image-preview-clear button -->
                                        <button type="button" class="btn btn-default image-preview-clear <?php echo $opcional['5']['hide']; ?>" >
                                            <span class="fa fa-times"></span> Limpiar
                                        </button>
                                        <!-- image-preview-input -->
                                        <div class="btn btn-default image-preview-input">
                                            <span class="fa fa-folder-open"></span>
                                            <span class="numero3  image-preview-input-title">Buscar</span>
                                            <input type="file" id="rifro" name="rifro"/> <!-- rename it -->
                                        </div>
                                    </span>
                               
                                </div> 
                            </div> 
                            <div class="hide" id="popover-numero-3">
                                <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$ruta ?>" class="img-responsive">
                            </div>
                            <?php
                                if($opcional['5']['show']=='true') { 
                            ?>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    var closebtn = $('<button/>', {
                                        type: "button",
                                        text: 'x',
                                        id: 'close-preview',
                                        style: 'font-size: initial;',
                                    });
                                    closebtn.attr("class","close pull-right");
                                    $('.numero3 .image-preview').popover({
                                      html: true,
                                      title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
                                      content: function() {
                                        return $('#popover-numero-3').html();
                                      },
                                      trigger: 'manual'
                                    });

                                    $(".numero3 .image-preview").popover("show");
                                });
                            </script>
                            <?php
                                }
                            ?>
                        </div>
                    <div class="col-sm-6">        
                            <div class="numero4 form-group">
                                <label>Imagen Espalda</label>
                                <div  class="input-group image-preview" data-placement="top" >  

                                    <img id="dynamic">
                                    <!-- image-preview-filename input [CUT FROM HERE]-->
                                    <?php 
                                        $ruta=$pedidos['pdeta_riesp'];
                                        $nombre=explode('/', $ruta);
                                        $nombre=end($nombre);
                                        if($nombre){
                                            $opcional['6']['hide']='';
                                            $opcional['6']['show']='true';
                                        }else{
                                            $opcional['6']['hide']='hide';
                                            $opcional['6']['show']='false';
                                        }
                                    ?>
                                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="true" value="<?php echo $nombre ?>" > <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-btn">
                                        <!-- image-preview-clear button -->
                                        <button type="button" class="btn btn-default image-preview-clear <?php echo $opcional['6']['hide']; ?>" >
                                            <span class="fa fa-times"></span> Limpiar
                                        </button>
                                        <!-- image-preview-input -->
                                        <div class="btn btn-default image-preview-input">
                                            <span class="fa fa-folder-open"></span>
                                            <span class="numero4  image-preview-input-title">Buscar</span>
                                            <input type="file" id="riesp" name="riesp"/> <!-- rename it -->
                                        </div>
                                    </span>
                               
                                </div> 
                            </div> 
                            <div class="hide" id="popover-numero-4">
                                <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$ruta ?>" class="img-responsive">
                            </div>
                            <?php
                                if($opcional['6']['show']=='true') { 
                            ?>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    var closebtn = $('<button/>', {
                                        type: "button",
                                        text: 'x',
                                        id: 'close-preview',
                                        style: 'font-size: initial;',
                                    });
                                    closebtn.attr("class","close pull-right");
                                    $('.numero4 .image-preview').popover({
                                      html: true,
                                      title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
                                      content: function() {
                                        return $('#popover-numero-4').html();
                                      },
                                      trigger: 'manual'
                                    });

                                    $(".numero4 .image-preview").popover("show");
                                });
                            </script>
                            <?php
                                }
                            ?>
                        </div>
                </div>
        </div><!-- form -->
    </div>  
</div>
<div class="row">                    
    <div class="panel panel-default" >
        <div class="panel-body">
            <div class="row controls">
                <div class="col-sm-4 ">
                    <a href="modificar?p=<?php echo $p?>&c=<?php echo $c?>&s=3" type="reset" class="btn-block btn btn-default">Volver </a>
                </div>
                <div class="col-sm-4 ">
                        <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                    </div>
                <div class="col-sm-4 ">
                    <button id="guardar" type="button" class="btn-block btn btn-info">Finalizar  </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hide" id="popover-img">
    <?php
        $sql="SELECT * FROM pedido_fuente where fuent_codig = '".$pedidos['pdeta_mfuen']."'";
        $fuente=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$fuente['fuent_ruta'] ?>" class="img-responsive"></div>
<div class="hide" id="popover-img-2">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pdeta_mcolo']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive">
</div>
<div class="hide" id="popover-img-3">
    <?php
        $sql="SELECT * FROM pedido_fuente where fuent_codig = '".$pedidos['pdeta_eafue']."'";
        $fuente=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$fuente['fuent_ruta'] ?>" class="img-responsive"></div>
<div class="hide" id="popover-img-4">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pdeta_eacol']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive">
</div>
<div class="hide" id="popover-img-5">
    <?php
        $sql="SELECT * FROM pedido_fuente where fuent_codig = '".$pedidos['pdeta_ebfue']."'";
        $fuente=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$fuente['fuent_ruta'] ?>" class="img-responsive"></div>
<div class="hide" id="popover-img-6">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pdeta_ebcol']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive">
</div>
<div class="hide" id="popover-img-7">
    <?php
        $sql="SELECT * FROM pedido_fuente where fuent_codig = '".$pedidos['pdeta_aefue']."'";
        $fuente=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$fuente['fuent_ruta'] ?>" class="img-responsive"></div>
<div class="hide" id="popover-img-8">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pdeta_aecol']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive">
</div>
</form>
<script>
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {       
                mposi: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Posición" es obligatorio',
                        }
                    }
                },
                mtder: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar Derecha" es obligatorio',
                        }
                    }
                },
                mtizq: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar Izquierda" es obligatorio',
                        }
                    }
                },
                mfuen: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fuente" es obligatorio',
                        }
                    }
                },
                mcolo: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color" es obligatorio',
                        }
                    }
                },
                limag: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Lleva Imagen Bordada" es obligatorio',
                        }
                    }
                },
                eatbo: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Imagen Libre" es obligatorio',
                        }
                    }
                },
                eafue: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar" es obligatorio',
                        }
                    }
                },
                eacol: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fuente" es obligatorio',
                        }
                    }
                },
                ebtbo: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color" es obligatorio',
                        }
                    }
                },
                ebfue: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar" es obligatorio',
                        }
                    }
                },
                ebcol: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fuente" es obligatorio',
                        }
                    }
                },
                aetbo: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color" es obligatorio',
                        }
                    }
                },
                aefue: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar" es obligatorio',
                        }
                    }
                },
                aecol: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fuente" es obligatorio',
                        }
                    }
                },
                mrima: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color" es obligatorio',
                        }
                    }
                },
                iprut: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Imagen Principal" es obligatorio',
                        }
                    }
                },
                rifro: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Imagen Frontal" es obligatorio',
                        }
                    }
                },
                riesp: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Imagen Espalda" es obligatorio',
                        }
                    }
                }
            }
        });
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        var data = new FormData(jQuery('form')[0]);
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                url: 'paso4',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado?p=<?php echo $p; ?>', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>
<script>

    $(document).on('click', '#close-preview', function () {
        $('.numero1 .image-preview').popover('hide');
        // Hover befor close the preview
        $('.numero1 .image-preview').hover(
            function () {
                $('.image-preview').popover('hide');
            },
            function () {
                $('.image-preview').popover('hide');
            }
        );
    });
    $(function () {
        // Create the close button
        var closebtn = $('<button/>', {
            type: "button",
            text: 'x',
            id: 'close-preview',
            style: 'font-size: initial;',
        });
        closebtn.attr("class","close pull-right");
        $('.numero1 .image-preview').popover({
            trigger:'manual',
            html:true,
            title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
            content: "No hay imagen",
            placement:'top'
        });



        
        // Clear event
        $('.numero1 .image-preview-clear').click(function () {
            $('.numero1 .image-preview').attr("data-content", "").popover('hide');
            $('.numero1 .image-preview-filename').val("");
            $('.numero1 .image-preview-clear').hide();
            $('.numero1 .image-preview-input input:file').val("");
            $(".numero1 .image-preview-input-title").text("Buscar");
        });
        // Create the preview image
        $(".numero1 .image-preview-input input:file").change(function () {
            var img = $('<img/>', {
                id: 'dynamic',
                width: 250,
                height: 200
            });
            var file = this.files[0];
            var reader = new FileReader();
            // Set preview image into the popover data-content
            reader.onload = function (e) {
                $(".numero1 .image-preview-input-title").text("Cambiar");
                $(".numero1 .image-preview-clear").show();
                $(".numero1 .image-preview-filename").val(file.name);
                img.attr('src', e.target.result);
                $(".numero1 .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
                }
            reader.readAsDataURL(file);
        });
    });
</script>
<script>

    $(document).on('click', '#close-preview', function () {
        $('.numero2 .image-preview').popover('hide');
        // Hover befor close the preview
        $('.numero2 .image-preview').hover(
            function () {
                $('.image-preview').popover('hide');
            },
            function () {
                $('.image-preview').popover('hide');
            }
        );
    });
    $(function () {
        // Create the close button
        var closebtn = $('<button/>', {
            type: "button",
            text: 'x',
            id: 'close-preview',
            style: 'font-size: initial;',
        });
        closebtn.attr("class","close pull-right");
        $('.numero2 .image-preview').popover({
            trigger:'manual',
            html:true,
            title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
            content: "No hay imagen",
            placement:'top'
        });



        
        // Clear event
        $('.numero2 .image-preview-clear').click(function () {
            $('.numero2 .image-preview').attr("data-content", "").popover('hide');
            $('.numero2 .image-preview-filename').val("");
            $('.numero2 .image-preview-clear').hide();
            $('.numero2 .image-preview-input input:file').val("");
            $(".numero2 .image-preview-input-title").text("Buscar");
        });
        // Create the preview image
        $(".numero2 .image-preview-input input:file").change(function () {
            var img = $('<img/>', {
                id: 'dynamic',
                width: 250,
                height: 200
            });
            var file = this.files[0];
            var reader = new FileReader();
            // Set preview image into the popover data-content
            reader.onload = function (e) {
                $(".numero2 .image-preview-input-title").text("Cambiar");
                $(".numero2 .image-preview-clear").show();
                $(".numero2 .image-preview-filename").val(file.name);
                img.attr('src', e.target.result);
                $(".numero2 .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
                }
            reader.readAsDataURL(file);
        });
    });
</script>
<script>

    $(document).on('click', '#close-preview', function () {
        $('.numero3 .image-preview').popover('hide');
        // Hover befor close the preview
        $('.numero3 .image-preview').hover(
            function () {
                $('.image-preview').popover('hide');
            },
            function () {
                $('.image-preview').popover('hide');
            }
        );
    });
    $(function () {
        // Create the close button
        var closebtn = $('<button/>', {
            type: "button",
            text: 'x',
            id: 'close-preview',
            style: 'font-size: initial;',
        });
        closebtn.attr("class","close pull-right");
        $('.numero3 .image-preview').popover({
            trigger:'manual',
            html:true,
            title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
            content: "No hay imagen",
            placement:'top'
        });



        
        // Clear event
        $('.numero3 .image-preview-clear').click(function () {
            $('.numero3 .image-preview').attr("data-content", "").popover('hide');
            $('.numero3 .image-preview-filename').val("");
            $('.numero3 .image-preview-clear').hide();
            $('.numero3 .image-preview-input input:file').val("");
            $(".numero3 .image-preview-input-title").text("Buscar");
        });
        // Create the preview image
        $(".numero3 .image-preview-input input:file").change(function () {
            var img = $('<img/>', {
                id: 'dynamic',
                width: 250,
                height: 200
            });
            var file = this.files[0];
            var reader = new FileReader();
            // Set preview image into the popover data-content
            reader.onload = function (e) {
                $(".numero3 .image-preview-input-title").text("Cambiar");
                $(".numero3 .image-preview-clear").show();
                $(".numero3 .image-preview-filename").val(file.name);
                img.attr('src', e.target.result);
                $(".numero3 .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
                }
            reader.readAsDataURL(file);
        });
    });
</script>
<script>

    $(document).on('click', '#close-preview', function () {
        $('.numero4 .image-preview').popover('hide');
        // Hover befor close the preview
        $('.numero4 .image-preview').hover(
            function () {
                $('.image-preview').popover('hide');
            },
            function () {
                $('.image-preview').popover('hide');
            }
        );
    });
    $(function () {
        // Create the close button
        var closebtn = $('<button/>', {
            type: "button",
            text: 'x',
            id: 'close-preview',
            style: 'font-size: initial;',
        });
        closebtn.attr("class","close pull-right");
        $('.numero4 .image-preview').popover({
            trigger:'manual',
            html:true,
            title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
            content: "No hay imagen",
            placement:'top'
        });



        
        // Clear event
        $('.numero4 .image-preview-clear').click(function () {
            $('.numero4 .image-preview').attr("data-content", "").popover('hide');
            $('.numero4 .image-preview-filename').val("");
            $('.numero4 .image-preview-clear').hide();
            $('.numero4 .image-preview-input input:file').val("");
            $(".numero4 .image-preview-input-title").text("Buscar");
        });
        // Create the preview image
        $(".numero4 .image-preview-input input:file").change(function () {
            var img = $('<img/>', {
                id: 'dynamic',
                width: 250,
                height: 200
            });
            var file = this.files[0];
            var reader = new FileReader();
            // Set preview image into the popover data-content
            reader.onload = function (e) {
                $(".numero4 .image-preview-input-title").text("Cambiar");
                $(".numero4 .image-preview-clear").show();
                $(".numero4 .image-preview-filename").val(file.name);
                img.attr('src', e.target.result);
                $(".numero4 .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
                }
            reader.readAsDataURL(file);
        });
    });
</script>
<script type="text/javascript">
    $('#mposi').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '1':
                jQuery("#mtder").removeAttr('disabled');
                jQuery("#mtizq").attr('disabled','true');
                jQuery("#mtizq").val('');
                break;
            case '2':
                jQuery("#mtizq").removeAttr('disabled');
                jQuery("#mtder").attr('disabled','true');
                jQuery("#mtder").val('');
                break;
            case '3':
                jQuery("#mtder").removeAttr('disabled');
                jQuery("#mtizq").removeAttr('disabled');
                break;
            default:
                jQuery("#mtder").attr('disabled','true');
                jQuery("#mtizq").attr('disabled','true');
                jQuery("#mtder").val('');
                jQuery("#mtizq").val('');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#iopci').change(function () {
        var iopci = $(this).val();
        $.ajax({
            'type':'POST',
            'data':{'tmode':tmode.value},
            'url':'<?php echo CController::createUrl('funciones/PedidosColorOpcional'); ?>',
            'cache':false,
            'success':function(html){
                switch(iopci) {
                    case '1':
                        
                        jQuery("#opcional-1").removeClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#iclin").html(html);
                        jQuery("#iccie").html('');
                        jQuery("#icviv").html('');
                        jQuery("#ideta").removeAttr('disabled');
                        jQuery("#iclin").removeAttr('disabled');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").attr('disabled','true');

                        break;
                    case '2':
                        jQuery("#opcional-2").removeClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#iclin").html('');
                        jQuery("#iccie").html(html);
                        jQuery("#icviv").html('');
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").removeAttr('disabled');
                        jQuery("#icviv").attr('disabled','true');
                        break;
                    case '3':
                        jQuery("#opcional-3").removeClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#iclin").html('');
                        jQuery("#iccie").html('');
                        jQuery("#icviv").html(html);
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").removeAttr('disabled');
                        break;
                    default:
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#iclin").html(html);
                        jQuery("#iccie").html(html);
                        jQuery("#icviv").html(html);
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").attr('disabled','true');
                        break;
                }
                
            }
        });
    });
</script>
<script type="text/javascript">
    $('#limag').change(function () {
        var ideta = $(this).val();
        switch(ideta) {
            case '1':
                jQuery("#opcional").removeClass('hide');
                break;
            default:
                jQuery("#opcional").addClass('hide');

                break;
        }
                
            
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-1').popover({
          html: true,
          content: function() {
            return $('#popover-img').html();
          },
          trigger: 'hover'
        });
});
$('#mfuen').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerFuente'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-2').popover({
          html: true,
          content: function() {
            return $('#popover-img-2').html();
          },
          trigger: 'hover'
        });
});
$('#mcolo').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-2").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-3').popover({
          html: true,
          content: function() {
            return $('#popover-img-3').html();
          },
          trigger: 'hover'
        });
});
$('#eafue').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerFuente'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-3").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-4').popover({
          html: true,
          content: function() {
            return $('#popover-img-4').html();
          },
          trigger: 'hover'
        });
});
$('#eacol').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-4").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-5').popover({
          html: true,
          content: function() {
            return $('#popover-img-5').html();
          },
          trigger: 'hover'
        });
});
$('#ebfue').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerFuente'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-5").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-6').popover({
          html: true,
          content: function() {
            return $('#popover-img-6').html();
          },
          trigger: 'hover'
        });
});
$('#ebcol').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-6").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-7').popover({
          html: true,
          content: function() {
            return $('#popover-img-7').html();
          },
          trigger: 'hover'
        });
});
$('#aefue').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerFuente'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-7").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-8').popover({
          html: true,
          content: function() {
            return $('#popover-img-8').html();
          },
          trigger: 'hover'
        });
});
$('#aecol').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-8").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>