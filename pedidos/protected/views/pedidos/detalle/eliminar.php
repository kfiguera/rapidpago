<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Pedidos</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Pedidos</a></li>
            <li class="active">Eliminar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Eliminar Pedidos</h3>
        </div>
        <div class="panel-body">
            <div class="sttabs tabs-style-linebox">
            <nav>
              <ul>
                <li class="tab-current"><a href="#section-linebox-1"><span>Modelo</span></a></li>
                <li class=""><a href="#section-linebox-2"><span>Persona</span></a></li>
                <li class=""><a href="#section-linebox-3"><span>Adicional 1</span></a></li>
                <li class=""><a href="#section-linebox-4"><span>Adicional 2</span></a></li>
              </ul>
            </nav>
            <div class="content-wrap text-center">
                <section id="section-linebox-1" class="content-current">
                    <h2>Datos generales del modelo</h2>
                    <form id='login-form' name='login-form' method="post">
                        <div class="row hide">
                            <div class="col-sm-4">        
                                <div class="form-group">
                                    <label>Pedido *</label>
                                    <?php 
                                        echo CHtml::hiddenField('pedid', $p, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                                </div>
                            </div>
                            <div class="col-sm-4">        
                                <div class="form-group">
                                    <label>Código *</label>
                                    <?php 
                                        echo CHtml::hiddenField('codig', $c, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                                </div>
                            </div>
                            <div class="col-sm-4">        
                                <div class="form-group">
                                    <label>Paso *</label>
                                    <?php 
                                        echo CHtml::hiddenField('pasos', $pedidos['pdeta_pasos'], array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">        
                                <div class="form-group">
                                    <label>Tipo *</label>
                                    <?php 
                                        $conexion = Yii::app()->db;
                                        $sql="SELECT tmode_codig, tmode_descr
                                              FROM pedido_modelo_tipo 
                                              ORDER BY 1";
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'tmode_codig','tmode_descr');
                                        echo CHtml::dropDownList('tmode', $pedidos['tmode_codig'], $data, array('ajax' => array(
                                                    'type' => 'POST',
                                                    'url' => CController::createUrl('funciones/PedidosModelos'), // Controlador que devuelve las p_ciudades relacionadas
                                                    'update' => '#model', // id del item que se actualizará
                                                ),'class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                </div>
                            </div>
                            <div class="col-sm-4">        
                                <div class="form-group">
                                    <label>Modelo *</label>
                                    <?php 
                                    $sql="SELECT a.model_codig, a.model_descr
                                              FROM pedido_modelo a
                                              JOIN pedido_modelo_conjunto b ON (a.model_codig = b.model_codig)
                                              ORDER BY 1";
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'model_codig','model_descr');
                                        echo CHtml::dropDownList('model', $pedidos['model_codig'], $data, array('ajax' => array(
                                                    'type' => 'POST',
                                                    'url' => CController::createUrl('funciones/PedidosCategoria'), // Controlador que devuelve las p_ciudades relacionadas
                                                    'update' => '#categ', // id del item que se actualizará
                                                ),'class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                </div>
                            </div>
                            <div class="col-sm-4">        
                                <div class="form-group">
                                    <label>Categoria *</label>
                                    <?php 
                                        $sql="SELECT a.mcate_codig, a.mcate_descr
                                              FROM pedido_modelo_categoria a
                                              JOIN pedido_modelo_conjunto b ON (a.mcate_codig = b.mcate_codig)
                                              WHERE b.model_codig = '".$pedidos['model_codig']."'
                                              GROUP BY 1,2
                                              ORDER BY 1";
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'mcate_codig','mcate_descr');
                                        echo CHtml::dropDownList('categ', $pedidos['mcate_codig'], $data, array('ajax' => array(
                                                    'type' => 'POST',
                                                    'url' => CController::createUrl('funciones/PedidosVariante'), // Controlador que devuelve las p_ciudades relacionadas
                                                    'update' => '#varia', // id del item que se actualizará
                                                ),'class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 hide">        
                                <div class="form-group">
                                    <label>Variante *</label>
                                    <?php 
                                        $sql="SELECT a.mvari_codig, a.mvari_descr
                                              FROM pedido_modelo_variante a
                                              JOIN pedido_modelo_conjunto b ON (a.mvari_codig = b.mvari_codig)
                                              WHERE b.model_codig = '".$pedidos['model_codig']."'
                                                AND b.mcate_codig = '".$pedidos['mcate_codig']."'
                                              GROUP BY 1,2
                                              ORDER BY 1";
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'mvari_codig','mvari_descr');
                                        echo CHtml::dropDownList('varia', $pedidos['mvari_codig'], $data, array('ajax' => array(
                                                    'type' => 'POST',
                                                    'url' => CController::createUrl('funciones/PedidosColor'), // Controlador que devuelve las p_ciudades relacionadas
                                                    'update' => '#color', // id del item que se actualizará
                                                ),'class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                </div>
                            </div>
                            <div class="col-sm-4">        
                                <div class="form-group">
                                    <label>Vista Previa *</label>
                                    <button class="btn btn-block btn-info" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img_1" data-original-title="" title="Vista previa"><i class="fa fa-search"></i></button>
                                    
                                </div>
                            </div>
                             <div class="col-sm-4">        
                                <div class="form-group">
                                    <label>Color del Cuerpo *</label>
                                    <div class="input-group">
                                    <?php 
                                    $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                              FROM pedido_modelo_color a
                                              WHERE a.tmode_codig = '".$pedidos['tmode_codig']."'
                                              GROUP BY 1,2
                                              ORDER BY 1";
                                        $result=$conexion->createCommand($sql)->queryAll();

                                        $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                                        echo CHtml::dropDownList('color', $pedidos['mcolo_codig'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                        
                                        <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-1" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">        
                                <div class="form-group">
                                    <label>Cantidad a Solicitar *</label>
                                    <?php echo CHtml::textField('canti', $pedidos['pdeta_canti'], array('class' => 'form-control', 'placeholder' => "Cantidad a Solicitar",'disabled'=>'true')); ?>
                                </div>
                            </div>
                        </div>
                        <!-- OPCIONALES-->
                        <?php
                            switch ($pedidos['mopci_codig']) {
                                case '1':
                                    $opcional['1']['hide']='';
                                    $opcional['1']['disabled']='';
                                    $opcional['2']['hide']='hide';
                                    $opcional['2']['disabled']='true';
                                    $opcional['3']['hide']='hide';
                                    $opcional['3']['disabled']='disabled="true"';
                                    break;
                                case '2':
                                    $opcional['2']['hide']='';
                                    $opcional['2']['disabled']='';
                                    $opcional['1']['hide']='hide';
                                    $opcional['1']['disabled']='true';
                                    $opcional['3']['hide']='hide';
                                    $opcional['3']['disabled']='disabled="true"';
                                    break;
                                
                                default:
                                    $opcional['1']['hide']='hide';
                                    $opcional['1']['disabled']='true';
                                    $opcional['2']['hide']='hide';
                                    $opcional['2']['disabled']='disabled="true"';
                                    $opcional['3']['hide']='';
                                    $opcional['3']['disabled']='';
                                    break;
                            }
                            if($pedidos['pdeta_opci']=='1'){
                                $opcional['0']['hide']='';
                                $opcional['0']['disabled']='';
                            }
                            ?>
                        <div class="row">

                            <div class="col-sm-4">        
                                <div class="form-group">
                                    <label>Opcional *</label>
                                    <?php 
                                        $data=array('1'=>'SI','2'=>'NO');
                                        echo CHtml::dropDownList('opcio', $pedidos['pdeta_opci'], $data, array('ajax' => array(
                                                    'type' => 'POST',
                                                    'url' => CController::createUrl('funciones/PedidosOpcionales'), // Controlador que devuelve las p_ciudades relacionadas
                                                    'update' => '#iopci', // id del item que se actualizará
                                                ),'class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                </div>
                            </div>
                            <div class="col-sm-4">        
                                <div class="form-group">
                                    <label>Indicar Opcional *</label>
                                    <?php 
                                        $sql="SELECT a.mopci_codig, a.mopci_descr
                                              FROM pedido_modelo_opcional a
                                              JOIN pedido_modelo_conjunto b ON (a.mopci_codig = b.mopci_codig)
                                              WHERE b.model_codig = '".$pedidos['model_codig']."'
                                                AND b.mcate_codig = '".$pedidos['mcate_codig']."'
                                                AND b.mvari_codig = '".$pedidos['mvari_codig']."'
                                              GROUP BY 1,2
                                              ORDER BY 1";
                                        $result=$conexion->createCommand($sql)->queryAll();

                                        $data=CHtml::listData($result,'mopci_codig','mopci_descr');

                                        echo CHtml::dropDownList('iopci', $pedidos['mopci_codig'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['0']['disabled'],'disabled'=>'true')); ?>
                                </div>
                            </div>
                            <div class="<?php echo $opcional['1']['hide'] ?>" id="opcional-1">
                                <div class="col-sm-4">        
                                    <div class="form-group">
                                        <label>Indicar Detalles *</label>
                                        <?php
                                            $data=array('1'=>'CON LINEA','2'=>'SIN LINEA'); 
                                            echo CHtml::dropDownList('ideta', $pedidos['pdeta_dopci'], $data, array('ajax' => array(
                                                    'type' => 'POST',
                                                    'url' => CController::createUrl('funciones/PedidosColorLinea'), // Controlador que devuelve las p_ciudades relacionadas
                                                    'update' => '#iclin', // id del item que se actualizará
                                                ),'class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['1']['disabled'],'disabled'=>'true')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">        
                                    <div class="form-group">
                                        <label>Indicar Color del la Linea *</label>
                                        <div class="input-group">

                                        <?php
                                            $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                              FROM pedido_modelo_color a
                                              WHERE a.tmode_codig = '".$pedidos['tmode_codig']."'
                                              GROUP BY 1,2
                                              ORDER BY 1";

                                            $result=$conexion->createCommand($sql)->queryAll();

                                            $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');

                                            echo CHtml::dropDownList('iclin', $pedidos['pdeta_copci'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['1']['disabled'],'disabled'=>'true')); ?>
                                            <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-2" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="<?php echo $opcional['2']['hide'] ?>" id="opcional-2">
                                <div class="col-sm-4">        
                                    <div class="form-group">
                                        <label>Indicar Color del Cierre *</label>
                                        <div class="input-group">
                                        <?php
                                            echo CHtml::dropDownList('iccie', $pedidos['pdeta_copci'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['2']['disabled'],'disabled'=>'true')); ?>
                                        <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-3" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="<?php echo $opcional['3']['hide'] ?>" id="opcional-3">
                                <div class="col-sm-4">        
                                    <div class="form-group">
                                        <label>Indicar Color del Vivo Bolsillo *</label>
                                                                        <div class="input-group">

                                        <?php

                                            echo CHtml::dropDownList('icviv', $pedidos['pdeta_copci'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['3']['disabled'],'disabled'=>'true')); ?>
                                        <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-4" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- FIN OPCIONALES-->
                        <div class="row">
                            <div class="col-sm-12">        
                                <div class="form-group">
                                    <label>Mensaje</label>
                                    <?php echo CHtml::textArea('mensa', $pedidos['pdeta_mensa'], array('class' => 'form-control', 'placeholder' => "Mensaje",'disabled'=>'true')); ?>
                                </div>
                            </div>
                        </div>

                    </form>
                </section>
                <section id="section-linebox-2" class="">
                    <h2>Listado de p_personas asociado al modelo</h2>
                    <div class="row">
                        <div class="col-sm-12 table-responsive" id='listado-pedido'>
                            <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th width="2%">
                                            #
                                        </th>
                                        <th>
                                            Rut 
                                        </th>
                                        <th>
                                            Nombre
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $sql = "SELECT a.*, b.pdeta_pasos 
                                            FROM pedido_pedidos_detalle_listado a
                                            JOIN pedido_pedidos_detalle b ON (a.pdeta_codig =b.pdeta_codig)
                                            WHERE a.pedid_codig='".$p."'
                                              AND a.pdeta_codig='".$c."'";


                                    $command = $conexion->createCommand($sql);
                                    $p_persona = $command->query();
                                    $i=0;
                                    while (($row = $p_persona->read()) !== false) {
                                        $i++;
                                    ?>
                                    <tr>
                                        <td class="tabla"><?php echo $i ?></td>
                                        <td class="tabla"><?php echo $row['pdlis_ndocu'] ?></td>
                                        <td class="tabla"><?php echo $row['pdlis_nombr'] ?></td>
                                        

                                    </tr>
                                    <?php
                                        }   
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
                <section id="section-linebox-3" class="">
                    <h2>Información adicional a Bordar Parte 1</h2>
                    <h3>Detalles de la información a bordar</h3>
                    <div class="row">
                        <div class="col-sm-12">
                            <label>Nombre Personalizado</label>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Fuente *</label>
                                <div class="input-group">
                                <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT a.fuent_codig, fuent_descr
                                          FROM pedido_fuente a
                                          GROUP BY 1,2
                                          ORDER BY 1";
                                    $result=$conexion->createCommand($sql)->queryAll();

                                    $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                    echo CHtml::dropDownList('npfue', $pedidos['pdeta_npfue'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                    
                                    <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-1" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Color *</label>
                                <div class="input-group">
                                <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                          FROM pedido_modelo_color a
                                          WHERE a.tmode_codig = '".$pedidos['tmode_codig']."'
                                          GROUP BY 1,2
                                          ORDER BY 1";
                                    $result=$conexion->createCommand($sql)->queryAll();

                                    $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                                    echo CHtml::dropDownList('npcol', $pedidos['pdeta_npcol'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                    
                                    <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-2" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Posición *</label>
                                <?php 
                                    
                                    $data=array('1'=>'DERECHO', '2'=>'IZQUIERDO', '3'=>'AMBOS');
                                    echo CHtml::dropDownList('nppos', $pedidos['pdeta_nppos'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <label>Apodo Espalda</label>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Fuente *</label>
                                <div class="input-group">
                                <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT a.fuent_codig, fuent_descr
                                          FROM pedido_fuente a
                                          GROUP BY 1,2
                                          ORDER BY 1";
                                    $result=$conexion->createCommand($sql)->queryAll();

                                    $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                    echo CHtml::dropDownList('aefue', $pedidos['pdeta_aefue'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                    
                                    <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-3" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Color *</label>
                                <div class="input-group">
                                <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                          FROM pedido_modelo_color a
                                          WHERE a.tmode_codig = '".$pedidos['tmode_codig']."'
                                          GROUP BY 1,2
                                          ORDER BY 1";
                                    $result=$conexion->createCommand($sql)->queryAll();

                                    $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                                    echo CHtml::dropDownList('aecol', $pedidos['pdeta_aecol'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                    
                                    <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-4" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Posición *</label>
                                <?php 
                                    
                                    $data=array('1'=>'DERECHO', '2'=>'IZQUIERDO', '3'=>'AMBOS');
                                    echo CHtml::dropDownList('aepos', $pedidos['pdeta_aepos'], $data, array('ajax' => array(
                                                'type' => 'POST',
                                                'url' => CController::createUrl('funciones/PedidosModelos'), // Controlador que devuelve las p_ciudades relacionadas
                                                'update' => '#model', // id del item que se actualizará
                                            ),'class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                            </div>
                        </div>
                    </div>
                    <h3>Pecho</h3>
                    <div class="row">
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Posición *</label>
                                <?php 
                                    
                                    $data=array('1'=>'DERECHO', '2'=>'IZQUIERDO', '3'=>'AMBOS');
                                    echo CHtml::dropDownList('pposi', $pedidos['pdeta_pposi'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                            </div>
                        </div>
                        <?php
                        switch ($pedidos['pdeta_pposi']) {
                            case '1':
                                $opcional['1']['disabled']='';
                                $opcional['2']['disabled']='true';
                                break;
                            case '2':
                                $opcional['1']['disabled']='true';
                                $opcional['2']['disabled']='';
                                break;
                            case '3':
                                $opcional['1']['disabled']='';
                                $opcional['2']['disabled']='';
                                break;
                            
                            default:
                                $opcional['1']['disabled']='true';
                                $opcional['2']['disabled']='true';
                                break;
                        }
                        ?>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Texto a Bordar Derecha *</label>
                                <?php 
                                    
                                    
                                    echo CHtml::textField('ptder', $pedidos['pdeta_ptder'], array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['1']['disabled'],'disabled'=>'true')); ?>
                            </div>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Texto a Bordar Izquierda *</label>
                                <?php 
                                    
                                    
                                    echo CHtml::textField('ptizq', $pedidos['pdeta_ptizq'], array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['2']['disabled'],'disabled'=>'true')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">        
                            <div class="form-group">
                                <label>Fuente *</label>
                                <div class="input-group">
                                <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT a.fuent_codig, fuent_descr
                                          FROM pedido_fuente a
                                          GROUP BY 1,2
                                          ORDER BY 1";
                                    $result=$conexion->createCommand($sql)->queryAll();

                                    $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                    echo CHtml::dropDownList('pfuen', $pedidos['pdeta_pfuen'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                    
                                    <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-5" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">        
                            <div class="form-group">
                                <label>Color *</label>
                                <div class="input-group">
                                <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                          FROM pedido_modelo_color a
                                          WHERE a.tmode_codig = '".$pedidos['tmode_codig']."'
                                          GROUP BY 1,2
                                          ORDER BY 1";
                                    $result=$conexion->createCommand($sql)->queryAll();

                                    $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                                    echo CHtml::dropDownList('pcolo', $pedidos['pdeta_pcolo'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                    
                                    <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-6" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">        
                            <div class="form-group">
                                <label>Observaciones</label>
                                <?php 
                                    echo CHtml::textArea('pobse', $pedidos['pdeta_pobse'], array('class' => 'form-control', 'placeholder' => "Observaciones",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                    
                                   
                            </div>
                        </div>
                    </div>
                    <h3>Gorro</h3>
                    <div class="row">
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Posición *</label>
                                <?php 
                                    
                                    $data=array('1'=>'DERECHO', '2'=>'IZQUIERDO', '3'=>'AMBOS');
                                    echo CHtml::dropDownList('gposi', $pedidos['pdeta_gposi'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$gorro,'disabled'=>'true')); ?>
                            </div>
                        </div>
                        
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Fuente *</label>
                                <div class="input-group">
                                <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT a.fuent_codig, fuent_descr
                                          FROM pedido_fuente a
                                          GROUP BY 1,2
                                          ORDER BY 1";
                                    $result=$conexion->createCommand($sql)->queryAll();

                                    $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                    echo CHtml::dropDownList('gfuen', $pedidos['pdeta_gfuen'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$gorro,'disabled'=>'true')); ?>
                                    
                                    <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-7" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Color *</label>
                                <div class="input-group">
                                <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                          FROM pedido_modelo_color a
                                          WHERE a.tmode_codig = '".$pedidos['tmode_codig']."'
                                          GROUP BY 1,2
                                          ORDER BY 1";
                                    $result=$conexion->createCommand($sql)->queryAll();

                                    $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                                    echo CHtml::dropDownList('gcolo', $pedidos['pdeta_gcolo'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$gorro,'disabled'=>'true')); ?>
                                    
                                    <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-8" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">        
                            <div class="form-group">
                                <label>Observaciones</label>
                                <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT a.fuent_codig, fuent_descr
                                          FROM pedido_fuente a
                                          GROUP BY 1,2
                                          ORDER BY 1";
                                    $result=$conexion->createCommand($sql)->queryAll();

                                    $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                    echo CHtml::textArea('gobse', $pedidos['pdeta_gobse'], array('class' => 'form-control', 'placeholder' => "Observaciones",'prompt'=>'Seleccione...','disabled'=>$gorro,'disabled'=>'true')); ?>
                                    
                                   
                            </div>
                        </div>
                    </div>
                </section>
                <section id="section-linebox-4" class="">
                    <h2>Información adicional a Bordar Parte 2</h2>
                    <h3>Mangas</h3>
                    <div class="row">
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Posición *</label>
                                <?php 
                                    
                                    $data=array('1'=>'DERECHO', '2'=>'IZQUIERDO', '3'=>'AMBOS');
                                    echo CHtml::dropDownList('mposi', $pedidos['pdeta_mposi'], $data, array('class' => 'form-control', 'placeholder' => "Posición",'prompt'=>'Seleccione...')); ?>
                            </div>
                        </div>
                        <?php
                        switch ($pedidos['pdeta_mposi']) {
                            case '1':
                                $opcional['1']['disabled']='';
                                $opcional['2']['disabled']='true';
                                break;
                            case '2':
                                $opcional['1']['disabled']='true';
                                $opcional['2']['disabled']='';
                                break;
                            case '3':
                                $opcional['1']['disabled']='';
                                $opcional['2']['disabled']='';
                                break;
                            
                            default:
                                $opcional['1']['disabled']='true';
                                $opcional['2']['disabled']='true';
                                break;
                        }
                        ?>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Texto a Bordar Derecha *</label>
                                <?php 
                                    
                                    
                                    echo CHtml::textField('mtder', $pedidos['pdeta_mtder'], array('class' => 'form-control', 'placeholder' => "Texto a Bordar Derecha",'prompt'=>'Seleccione...','disabled'=>$opcional['1']['disabled'])); ?>
                            </div>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Texto a Bordar Izquierda *</label>
                                <?php 
                                    
                                    
                                    echo CHtml::textField('mtizq', $pedidos['pdeta_mtizq'], array('class' => 'form-control', 'placeholder' => "Texto a Bordar Izquierda",'prompt'=>'Seleccione...','disabled'=>$opcional['2']['disabled'])); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Fuente *</label>
                                <div class="input-group">
                                <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT a.fuent_codig, fuent_descr
                                          FROM pedido_fuente a
                                          GROUP BY 1,2
                                          ORDER BY 1";
                                    $result=$conexion->createCommand($sql)->queryAll();

                                    $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                    echo CHtml::dropDownList('mfuen', $pedidos['pdeta_mfuen'], $data, array('class' => 'form-control', 'placeholder' => "Fuente",'prompt'=>'Seleccione...')); ?>
                                    
                                    <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-1" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Color *</label>
                                <div class="input-group">
                                <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                          FROM pedido_modelo_color a
                                          WHERE a.tmode_codig = '".$pedidos['tmode_codig']."'
                                          GROUP BY 1,2
                                          ORDER BY 1";
                                    $result=$conexion->createCommand($sql)->queryAll();

                                    $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                                    echo CHtml::dropDownList('mcolo', $pedidos['pdeta_mcolo'], $data, array('class' => 'form-control', 'placeholder' => "Color",'prompt'=>'Seleccione...')); ?>
                                    
                                    <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-2" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Lleva Imagen Bordada *</label>
                                <?php 
                                    
                                    $data=array('1'=>'SI', '2'=>'NO');
                                    echo CHtml::dropDownList('limag', $pedidos['pdeta_limag'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                            </div>
                        </div> 
                    </div>
                    <?php
                    if($pedidos['pdeta_limag']=='1'){
                        $opcional['3']['hide']='';
                    }else{
                        $opcional['3']['hide']='hide';  
                    }
                    ?>
                    <div class="row <?php echo $opcional['3']['hide'] ?>" id="opcional">
                            <div class="col-sm-12">        
                                <div class="numero1 form-group">
                                    <label>Imagen Libre</label>
                                    <div  class="input-group image-preview" data-placement="top" >  

                                        <img id="dynamic">
                                        <!-- image-preview-filename input [CUT FROM HERE]-->
                                        <?php 
                                            $ruta=$pedidos['pdeta_mrima'];
                                            $nombre=explode('/', $ruta);
                                            $nombre=end($nombre);
                                            if($nombre){
                                                $opcional['3']['hide']='';
                                                $opcional['3']['show']='true';
                                            }else{
                                                $opcional['3']['hide']='hide';
                                                $opcional['3']['show']='false';
                                            }
                                        ?>
                                        <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="true" value="<?php echo $nombre ?>" > <!-- don't give a name === doesn't send on POST/GET -->
                                        <span class="input-group-btn">
                                            <!-- image-preview-clear button -->
                                            <button type="button" class="btn btn-default image-preview-clear <?php echo $opcional['3']['hide'] ?>" >
                                                <span class="fa fa-times"></span> Limpiar
                                            </button>
                                            <!-- image-preview-input -->
                                            <div class="btn btn-default image-preview-input">
                                                <span class="fa fa-folder-open"></span>
                                                <span class="numero1 image-preview-input-title">Buscar</span>
                                                <input type="file" id="mrima" name="mrima"/> <!-- rename it -->
                                            </div>
                                        </span>
                                   
                                    </div> 
                                </div> 
                                <div class="hide" id="popover-numero-1">
                                    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$ruta ?>" class="img-responsive">
                                </div>
                                <?php
                                    if($opcional['3']['show']=='true') { 
                                ?>
                                <script type="text/javascript">
                                    $(document).ready(function () {
                                        var closebtn = $('<button/>', {
                                            type: "button",
                                            text: 'x',
                                            id: 'close-preview',
                                            style: 'font-size: initial;',
                                        });
                                        closebtn.attr("class","close pull-right");
                                        $('.numero1 .image-preview').popover({
                                          html: true,
                                          title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
                                          content: function() {
                                            return $('#popover-numero-1').html();
                                          },
                                          trigger: 'manual'
                                        });

                                        $(".numero1 .image-preview").popover("show");
                                    });
                                </script>
                                <?php
                                    }
                                ?>
                            </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">        
                            <div class="form-group">
                                <label>Observaciones</label>
                                <?php 
                                    echo CHtml::textArea('mobse', $pedidos['pdeta_mobse'], array('class' => 'form-control', 'placeholder' => "Observaciones",'prompt'=>'Seleccione...')); ?>
                                    
                                   
                            </div>
                        </div>
                    </div>
                    <h3>Espalda</h3>
                    <div class="row">
                        <div class="col-sm-12"> 
                            <label>Alto *</label>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Texto a Bordar *</label>
                                <?php 
                                    
                                    echo CHtml::textField('eatbo', $pedidos['pdeta_eatbo'], array('class' => 'form-control', 'placeholder' => "Texto a Bordar",'prompt'=>'Seleccione...')); ?>
                            </div>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Fuente *</label>
                                <div class="input-group">
                                <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT a.fuent_codig, fuent_descr
                                          FROM pedido_fuente a
                                          GROUP BY 1,2
                                          ORDER BY 1";
                                    $result=$conexion->createCommand($sql)->queryAll();

                                    $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                    echo CHtml::dropDownList('eafue', $pedidos['pdeta_eafue'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                                    
                                    <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-3" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Color *</label>
                                <div class="input-group">
                                <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                          FROM pedido_modelo_color a
                                          WHERE a.tmode_codig = '".$pedidos['tmode_codig']."'
                                          GROUP BY 1,2
                                          ORDER BY 1";
                                    $result=$conexion->createCommand($sql)->queryAll();

                                    $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                                    echo CHtml::dropDownList('eacol', $pedidos['pdeta_eacol'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                                    
                                    <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-4" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12"> 
                            <label>Bajo *</label>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Texto a Bordar *</label>
                                <?php 
                                    
                                    echo CHtml::textField('ebtbo', $pedidos['pdeta_ebtbo'], array('class' => 'form-control', 'placeholder' => "Texto a Bordar",'prompt'=>'Seleccione...')); ?>
                            </div>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Fuente *</label>
                                <div class="input-group">
                                <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT a.fuent_codig, fuent_descr
                                          FROM pedido_fuente a
                                          GROUP BY 1,2
                                          ORDER BY 1";
                                    $result=$conexion->createCommand($sql)->queryAll();

                                    $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                    echo CHtml::dropDownList('ebfue', $pedidos['pdeta_ebfue'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                                    
                                    <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-5" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Color *</label>
                                <div class="input-group">
                                <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                          FROM pedido_modelo_color a
                                          WHERE a.tmode_codig = '".$pedidos['tmode_codig']."'
                                          GROUP BY 1,2
                                          ORDER BY 1";
                                    $result=$conexion->createCommand($sql)->queryAll();

                                    $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                                    echo CHtml::dropDownList('ebcol', $pedidos['pdeta_ebcol'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                                    
                                    <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-6" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12"> 
                            <label>Apodo Espalda *</label>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Texto a Bordar *</label>
                                <?php 
                                    
                                    echo CHtml::textField('aetbo', $pedidos['pdeta_aetbo'], array('class' => 'form-control', 'placeholder' => "Texto a Bordar",'prompt'=>'Seleccione...')); ?>
                            </div>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Fuente *</label>
                                <div class="input-group">
                                <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT a.fuent_codig, fuent_descr
                                          FROM pedido_fuente a
                                          GROUP BY 1,2
                                          ORDER BY 1";
                                    $result=$conexion->createCommand($sql)->queryAll();

                                    $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                    echo CHtml::dropDownList('aefue', $pedidos['pdeta_aefue'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                                    
                                    <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-7" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Color *</label>
                                <div class="input-group">
                                <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                          FROM pedido_modelo_color a
                                          WHERE a.tmode_codig = '".$pedidos['tmode_codig']."'
                                          GROUP BY 1,2
                                          ORDER BY 1";
                                    $result=$conexion->createCommand($sql)->queryAll();

                                    $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                                    echo CHtml::dropDownList('aecol', $pedidos['pdeta_aecol'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                                    
                                    <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-8" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">        
                                <div class="numero2 form-group">
                                    <label>Imagen Principal</label>
                                    <div  class="input-group image-preview" data-placement="top" >  

                                        <img id="dynamic">
                                        <!-- image-preview-filename input [CUT FROM HERE]-->
                                        <?php 
                                            $ruta=$pedidos['pdeta_iprut'];
                                            $nombre=explode('/', $ruta);
                                            $nombre=end($nombre);
                                            if($nombre){
                                                $opcional['4']['hide']='';
                                                $opcional['4']['show']='true';
                                            }else{
                                                $opcional['4']['hide']='hide';
                                                $opcional['4']['show']='false';
                                            }
                                        ?>
                                        <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="true" value="<?php echo $nombre ?>" > <!-- don't give a name === doesn't send on POST/GET -->
                                        <span class="input-group-btn">
                                            <!-- image-preview-clear button -->
                                            <button type="button" class="btn btn-default image-preview-clear <?php echo $opcional['4']['hide']; ?>" >
                                                <span class="fa fa-times"></span> Limpiar
                                            </button>
                                            <!-- image-preview-input -->
                                            <div class="btn btn-default image-preview-input">
                                                <span class="fa fa-folder-open"></span>
                                                <span class="numero2 image-preview-input-title">Buscar</span>
                                                <input type="file" id="iprut" name="iprut"/> <!-- rename it -->
                                            </div>
                                        </span>
                                   
                                    </div> 
                                </div> 
                                <div class="hide" id="popover-numero-2">
                                    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$ruta ?>" class="img-responsive">
                                </div>
                                <?php
                                    if($opcional['4']['show']=='true') { 
                                ?>
                                <script type="text/javascript">
                                    $(document).ready(function () {
                                        var closebtn = $('<button/>', {
                                            type: "button",
                                            text: 'x',
                                            id: 'close-preview',
                                            style: 'font-size: initial;',
                                        });
                                        closebtn.attr("class","close pull-right");
                                        $('.numero2 .image-preview').popover({
                                          html: true,
                                          title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
                                          content: function() {
                                            return $('#popover-numero-2').html();
                                          },
                                          trigger: 'manual'
                                        });

                                        $(".numero2 .image-preview").popover("show");
                                    });
                                </script>
                                <?php
                                    }
                                ?>
                            </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">        
                            <div class="form-group">
                                <label>Observaciones</label>
                                <?php 
                                    echo CHtml::textArea('eobse', $pedidos['pdeta_eobse'], array('class' => 'form-control', 'placeholder' => "Observaciones",'prompt'=>'Seleccione...')); ?>
                                    
                                   
                            </div>
                        </div>
                    </div>
                    <h3>Dibujo simple del poleron completo por ambos lados</h3>
                    <div class="row">
                        <div class="col-sm-6">        
                                <div class="numero3 form-group">
                                    <label>Imagen Frontal</label>
                                    <div  class="input-group image-preview" data-placement="top" >  

                                        <img id="dynamic">
                                        <!-- image-preview-filename input [CUT FROM HERE]-->
                                        <?php 
                                            $ruta=$pedidos['pdeta_rifro'];
                                            $nombre=explode('/', $ruta);
                                            $nombre=end($nombre);
                                            if($nombre){
                                                $opcional['5']['hide']='';
                                                $opcional['5']['show']='true';
                                            }else{
                                                $opcional['5']['hide']='hide';
                                                $opcional['5']['show']='false';
                                            }
                                        ?>
                                        <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="true" value="<?php echo $nombre ?>" > <!-- don't give a name === doesn't send on POST/GET -->
                                        <span class="input-group-btn">
                                            <!-- image-preview-clear button -->
                                            <button type="button" class="btn btn-default image-preview-clear <?php echo $opcional['5']['hide']; ?>" >
                                                <span class="fa fa-times"></span> Limpiar
                                            </button>
                                            <!-- image-preview-input -->
                                            <div class="btn btn-default image-preview-input">
                                                <span class="fa fa-folder-open"></span>
                                                <span class="numero3  image-preview-input-title">Buscar</span>
                                                <input type="file" id="rifro" name="rifro"/> <!-- rename it -->
                                            </div>
                                        </span>
                                   
                                    </div> 
                                </div> 
                                <div class="hide" id="popover-numero-3">
                                    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$ruta ?>" class="img-responsive">
                                </div>
                                <?php
                                    if($opcional['5']['show']=='true') { 
                                ?>
                                <script type="text/javascript">
                                    $(document).ready(function () {
                                        var closebtn = $('<button/>', {
                                            type: "button",
                                            text: 'x',
                                            id: 'close-preview',
                                            style: 'font-size: initial;',
                                        });
                                        closebtn.attr("class","close pull-right");
                                        $('.numero3 .image-preview').popover({
                                          html: true,
                                          title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
                                          content: function() {
                                            return $('#popover-numero-3').html();
                                          },
                                          trigger: 'manual'
                                        });

                                        $(".numero3 .image-preview").popover("show");
                                    });
                                </script>
                                <?php
                                    }
                                ?>
                            </div>
                        <div class="col-sm-6">        
                                <div class="numero4 form-group">
                                    <label>Imagen Espalda</label>
                                    <div  class="input-group image-preview" data-placement="top" >  

                                        <img id="dynamic">
                                        <!-- image-preview-filename input [CUT FROM HERE]-->
                                        <?php 
                                            $ruta=$pedidos['pdeta_riesp'];
                                            $nombre=explode('/', $ruta);
                                            $nombre=end($nombre);
                                            if($nombre){
                                                $opcional['6']['hide']='';
                                                $opcional['6']['show']='true';
                                            }else{
                                                $opcional['6']['hide']='hide';
                                                $opcional['6']['show']='false';
                                            }
                                        ?>
                                        <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="true" value="<?php echo $nombre ?>" > <!-- don't give a name === doesn't send on POST/GET -->
                                        <span class="input-group-btn">
                                            <!-- image-preview-clear button -->
                                            <button type="button" class="btn btn-default image-preview-clear <?php echo $opcional['6']['hide']; ?>" >
                                                <span class="fa fa-times"></span> Limpiar
                                            </button>
                                            <!-- image-preview-input -->
                                            <div class="btn btn-default image-preview-input">
                                                <span class="fa fa-folder-open"></span>
                                                <span class="numero4  image-preview-input-title">Buscar</span>
                                                <input type="file" id="riesp" name="riesp"/> <!-- rename it -->
                                            </div>
                                        </span>
                                   
                                    </div> 
                                </div> 
                                <div class="hide" id="popover-numero-4">
                                    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$ruta ?>" class="img-responsive">
                                </div>
                                <?php
                                    if($opcional['6']['show']=='true') { 
                                ?>
                                <script type="text/javascript">
                                    $(document).ready(function () {
                                        var closebtn = $('<button/>', {
                                            type: "button",
                                            text: 'x',
                                            id: 'close-preview',
                                            style: 'font-size: initial;',
                                        });
                                        closebtn.attr("class","close pull-right");
                                        $('.numero4 .image-preview').popover({
                                          html: true,
                                          title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
                                          content: function() {
                                            return $('#popover-numero-4').html();
                                          },
                                          trigger: 'manual'
                                        });

                                        $(".numero4 .image-preview").popover("show");
                                    });
                                </script>
                                <?php
                                    }
                                ?>
                            </div>
                    </div>
                </section>
            </div><!-- /content -->
            </div>
        </div>
        <div class="panel-footer" >
           
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
           

                
                    <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-6 ">
                            <a href="listado?p=<?php echo $p?>" type="reset" class="btn-block btn btn-default">Volver </a>
                        </div>
                        <div class="col-sm-6 ">
                            <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                        </div>
                    </div>
        </div><!-- form -->
    </div>  
</div>

<script>
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                canti: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Total a Solicitar" es obligatorio',
                        }
                    }
                },
                mcant: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Cantidad de Modelos" es obligatorio',
                        }
                    }
                },
                color: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color del Cuerpo" es obligatorio',
                        }
                    }
                }



            }
        });
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'eliminar',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>

<div class="hide" id="popover-img">
    <?php

        $tmode=$pedidos['tmode_codig'];
        $model=$pedidos['model_codig'];
        $categ=$pedidos['mcate_codig'];
        $sql="SELECT *
              FROM pedido_modelo_conjunto a
              WHERE a.tmode_codig ='".$tmode."'
                AND a.model_codig ='".$model."'
                AND a.mcate_codig ='".$categ."'";
        $conexion=Yii::app()->db;
        $data=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$data['mconj_ruta'] ?>" class="img-responsive"></div>
<script type="text/javascript">
$(document).ready(function(){
    $('#img_1').popover({
          html: true,
          content: function() {
            return $('#popover-img').html();
          },
          trigger: 'hover'
        });
});
</script>