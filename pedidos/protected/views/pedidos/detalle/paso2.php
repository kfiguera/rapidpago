<?php
    $conexion=Yii::app()->db;
?>
<style>
    .image-preview-input {
        position: relative;
        overflow: hidden;
        margin: 0px;    
        color: #333;
        background-color: #fff;
        border-color: #ccc;    
    }
    .image-preview-input input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }
    .image-preview-input-title {
        margin-left:2px;
    }
    .sweet-alert {
        padding-top: 25px;
        padding-bottom: : 25px;
        padding-right: 0px;
        padding-left: 0px;
    }
</style>  
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Detalle del Pedido</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Pedidos</a></li>
            <li><a href="#">Detalle</a></li>
            <li class="active">Registrar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <div class="row line-steps">
                 <div class="col-xs-6 column-step <?php echo $ubicacion[0]; ?>">
                    <a href="modificar?c=<?php echo $solicitud['solic_codig']?>&s=1">
                        <div class="step-number">1 </div>
                    <div class="step-title">Modelo</div>
                    <div class="step-info">Seleccione los datos generales del modelo</div>
                 </div>

                 <div class="col-xs-6 column-step <?php echo $ubicacion[1]; ?> ">
                    <div class="step-number">2</div>
                    <div class="step-title">Persona</div>
                    <div class="step-info">Listado de p_personas asociado al modelo</div>
                 </div>
                 <!--div class="col-md-3 column-step <?php echo $ubicacion[2]; ?>">
                    <div class="step-number">3</div>
                    <div class="step-title">Adicional</div>
                    <div class="step-info">Información adicional a Bordar Parte 1</div>
                 </div>
                 <div class="col-md-3 column-step <?php echo $ubicacion[3]; ?>">
                    <div class="step-number">4</div>
                    <div class="step-title">Adicional</div>
                    <div class="step-info">Información adicional a Bordar Parte 2</div>
                 </div-->
              </div>
        </div>
        <div class="panel-body" >
           
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row hide">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Pedido *</label>
                            <?php 
                                echo CHtml::hiddenField('pedid', $p, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Código *</label>
                            <?php 
                                echo CHtml::hiddenField('codig', $c, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Paso *</label>
                            <?php 
                                echo CHtml::hiddenField('pasos', $pedidos['pdeta_pasos'], array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Número de lista  *</label>
                            <?php 
                                echo CHtml::textField('ndocu', '', array('class' => 'form-control', 'placeholder' => "Número de lista ",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Nombre *</label>
                            <?php 
                                echo CHtml::textField('nombr', '', array('class' => 'form-control', 'placeholder' => "Nombre",'prompt'=>'Seleccione...')); ?>
                            <span class="help-block">
                                <small>Nota: No va Bordado</small>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Talla *</label>
                            <?php 
                                $sql="SELECT * FROM pedido_talla ORDER BY 1";
                                $result= $conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'talla_codig','talla_descr');
                                echo CHtml::dropDownList('talla', '', $data, array('class' => 'form-control', 'placeholder' => "Talla",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Ajuste de Talla *</label>
                            <?php 
                                $sql="SELECT a.*, b.tajus_descr grupo 
                                      FROM pedido_talla_ajuste a 
                                      JOIN pedido_talla_ajuste b ON (a.tajus_padre = b.tajus_codig) 
                                      WHERE a.tajus_super='0' ";
                                $result= $conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'tajus_codig','tajus_descr','grupo');
                                echo CHtml::dropDownList('tajus', '', $data, array('class' => 'select2 select2-multiple', 'placeholder' => "Ajuste de Talla",'prompt'=>'Seleccione...','multiple'=>'multiple')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Nombre o Apodo Personalizado *</label>
                            <?php 
                                echo CHtml::textField('npers', '', array('class' => 'form-control', 'placeholder' => "Nombre Personalizado",'prompt'=>'Seleccione...')); ?>
                            <span class="help-block">
                                <small>Nota: Nombre a bordar adelante</small>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Nombre o Apodo Espalda *</label>
                            <?php 
                                echo CHtml::textField('aespa', '', array('class' => 'form-control', 'placeholder' => "Apodo Espalda",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Electivo *</label>
                            <?php 
                                echo CHtml::textField('elect', '', array('class' => 'form-control', 'placeholder' => "Electivo",'prompt'=>'Seleccione...')); ?>
                            <span class="help-block">
                                <small>Ejemplo: Humanista, Científico</small>
                            </span>
                        </div>
                    </div>
                </div>    
                
                <!-- OPCIONALES-->
                <input class="form-control" placeholder="Porcentaje" readonly="readonly" type="hidden" value="" name="bookindex" id="bookindex" /> 
                    <div class="form-group">
                        <!-- OPCIONALES-->
                        <div class="row">
                            <div class="col-xs-4">        
                                    <label>Tipo de Imagen *</label>
                                    <?php 
                                        $sql="SELECT * FROM pedido_precio ORDER BY 1";
                                        $result= $conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'pprec_codig','pprec_descr');
                                
                                        echo CHtml::dropDownList('timag[0]', '', $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','id'=>'timag_0')); ?>
                            </div>
                            <div class="hide" id="opcional-1_0">
                                <div class="col-xs-7">        
                                        <label>Emoji *</label>
                                        <div class="input-group">
                                            <?php
                                            $sql="SELECT * FROM pedido_emoji order by 2";
                                            $result=$conexion->createCommand($sql)->queryAll();
                                            $data=CHtml::listData($result,'emoji_codig','emoji_descr');
                                            echo CHtml::dropDownList('emoji[0]', '', $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','id'=>'emoji_0' ,'disabled'=>'true')); ?>
                                            
                                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="login_0" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                            
                                        </div>
                                        <div class="hide" id="popover-img_0"></div>
                                   
                                </div>
                            </div>
                            <div class="hide" id="opcional-2_0">
                                <div class="col-xs-7">        
                                    <div class="numero1_0">
                                        <label>Imagen Libre</label>
                                        <div  class="input-group image-preview">  
                                            <img id="dynamic">
                                            <!-- image-preview-filename input [CUT FROM HERE]-->
                                            <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                            <span class="input-group-btn">
                                                <!-- image-preview-clear button -->
                                                <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                                    <span class="fa fa-times"></span> Limpiar
                                                </button>
                                                <!-- image-preview-input -->
                                                <div class="btn btn-default image-preview-input">
                                                    <span class="fa fa-folder-open"></span>
                                                    <span class="numero1_0 image-preview-input-title">Buscar</span>
                                                    <input type="file" id="image_0" name="image[0]"/> <!-- rename it -->
                                                </div>
                                            </span>
                                       
                                        </div> 
                                    </div> 

                                </div>
                                
                            </div>
                            <div class="hide" id="opcional-3_0">
                                <div class="col-xs-7">        
                                        <label>Simbolo Simple *</label>
                                        <div class="input-group">
                                            <?php
                                            $sql="SELECT * FROM pedido_simbolo_simple ";
                                            $result=$conexion->createCommand($sql)->queryAll();
                                            $data=CHtml::listData($result,'ssimp_codig','ssimp_descr');
                                            echo CHtml::dropDownList('simbo[0]', '', $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','id'=>'simbo_0' ,'disabled'=>'true')); ?>
                                            
                                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="login-2_0" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                            
                                        </div>
                                        <div class="hide" id="popover-img-2_0"></div>
                                   
                                </div>
                            </div>
                            <div class="hide" id="opcional-4_0">
                                <div class="col-xs-7">        
                                        <label>Texto a bordar *</label>
                                            <?php
                                            
                                            echo CHtml::textField('texto[0]', '', array('class' => 'form-control', 'placeholder' => "Texto a Bordar",'prompt'=>'Seleccione...','id'=>'texto_0' ,'disabled'=>'true')); ?>
                                            
                                   
                                </div>
                            </div>
                            <div class="col-xs-1">
                                <label>&nbsp;</label>
                                <br>
                                <button type="button" class="btn btn-success btn-block addButton"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <!-- FIN OPCIONALES-->
                    </div>
                    <!-- Plantilla -->
                    <div class="form-group hide" id="bookTemplate">
                        <div class="row">
                                                        <div class="col-xs-4">        
                                    <label>Tipo de Imagen *</label>
                                    <?php 
                                        $sql="SELECT * FROM pedido_precio ORDER BY 1";
                                        $result= $conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'pprec_codig','pprec_descr');
                                        echo CHtml::dropDownList('timag_', '', $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','id'=>'timag_')); ?>
                               
                            </div>
                            <div class="hide" id="opcional-1_">
                                <div class="col-xs-7">        
                                        <label>Emoji *</label>
                                        <div class="input-group">
                                            <?php
                                            $sql="SELECT * FROM pedido_emoji  order by 2";
                                            $result=$conexion->createCommand($sql)->queryAll();
                                            $data=CHtml::listData($result,'emoji_codig','emoji_descr');
                                            echo CHtml::dropDownList('emoji_', '', $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','id'=>'emoji_' ,'disabled'=>'true')); ?>
                                            
                                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="login_" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                            
                                        </div>
                                        <div class="hide" id="popover-img_"></div>
                                </div>
                            </div>
                            <div class="hide" id="opcional-2_">
                                <div class="col-xs-7">        
                                    <div class="numero1_">
                                        <label>Imagen Libre</label>
                                        <div  class="input-group image-preview">  
                                            <img id="dynamic">
                                            <!-- image-preview-filename input [CUT FROM HERE]-->
                                            <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                            <span class="input-group-btn">
                                                <!-- image-preview-clear button -->
                                                <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                                    <span class="fa fa-times"></span> Limpiar
                                                </button>
                                                <!-- image-preview-input -->
                                                <div class="btn btn-default image-preview-input">
                                                    <span class="fa fa-folder-open"></span>
                                                    <span class="numero1_ image-preview-input-title">Buscar</span>
                                                    <input type="file" id="image_" name="image_"/> <!-- rename it -->
                                                </div>
                                            </span>
                                       
                                        </div> 
                                    </div> 

                                </div>
                                
                            </div>
                            <div class="hide" id="opcional-3_">
                                <div class="col-xs-7">        
                                        <label>Simbolo Simple *</label>
                                        <div class="input-group">
                                            <?php
                                            $sql="SELECT * FROM pedido_simbolo_simple ";
                                            $result=$conexion->createCommand($sql)->queryAll();
                                            $data=CHtml::listData($result,'ssimp_codig','ssimp_descr');
                                            echo CHtml::dropDownList('simbo_', '', $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','id'=>'simbo_' ,'disabled'=>'true')); ?>
                                            
                                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="login-2_" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                            
                                        </div>
                                        <div class="hide" id="popover-img-2_"></div>
                                   
                                </div>
                                
                            </div>
                            <div class="hide" id="opcional-4_">
                                    <div class="col-xs-7">        
                                            <label>Texto a bordar *</label>
                                                <?php
                                                
                                                echo CHtml::textField('texto_', '', array('class' => 'form-control', 'placeholder' => "Texto a Bordar",'prompt'=>'Seleccione...','id'=>'texto_' ,'disabled'=>'true')); ?>
                                                
                                       
                                    </div>
                                </div>
                            <div class="col-xs-1">
                                <label>&nbsp;</label>
                                <br>
                                <div class="btn-group btn-group-justified">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-success addButton"><i class="fa fa-plus"></i></button>
                                    </div>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-danger removeButton"><i class="fa fa-minus"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- FIN OPCIONALES-->

                <!-- Emoji y Imagen Libre -->

                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observaciones</label>
                            <?php echo CHtml::textArea('obser', '', array('class' => 'form-control', 'placeholder' => "Observaciones")); ?>
                        </div>
                    </div>
                </div>
                
                <!-- Button -->
                <div class="row controls">
                   
                    <div class="col-sm-6 ">
                        <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                    </div>
                    <div class="col-sm-6 ">
                        <button id="guardar" type="button" class="btn-block btn btn-info">Guardar  </button>
                    </div>
                </div>

            </form>

        </div><!-- form -->
    </div>  
</div>

<div class="row">   
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Listado</h3>
        </div>
        <div class="panel-body" >
            
            <div class="row">
                <div class="col-sm-12 table-responsive" id='listado-pedido'>
                    <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                        <thead>
                            <tr>
                                <th width="2%">#</th>
                                <th>Nombre</th> 
                                <th>Nombre Personalizado</th> 
                                <th>Apodo Espalda</th> 
                                <th>Talla</th> 
                                <th>Ajuste de Talla</th> 
                                <th>Observaciones</th> 
                                <th width="5%">Modificar</th>
                                <th width="5%">Eliminar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sql = "SELECT a.*, b.pdeta_pasos, c.talla_descr
                                    FROM pedido_pedidos_detalle_listado a
                                    JOIN pedido_pedidos_detalle b ON (a.pdeta_codig =b.pdeta_codig)
                                    JOIN pedido_talla c ON (a.talla_codig = c.talla_codig)
                                    WHERE a.pedid_codig='".$p."'
                                      AND a.pdeta_codig='".$c."'
                                    ORDER BY pdlis_codig asc";


                            $command = $conexion->createCommand($sql);
                            $p_persona = $command->query();
                            $i=0;
                            while (($row = $p_persona->read()) !== false) {
                                $i++;
                                $ajustes=$this->funciones->VerAjusteTalla(json_decode($row['pdlis_tajus']));
                            ?>
                            <tr>
                                <td class="tabla"><?php echo $i ?></td>
                                <td class="tabla"><?php echo $row['pdlis_nombr']; ?></td>
                                <td class="tabla"><?php echo $row['pdlis_npers']; ?></td>
                                <td class="tabla"><?php echo $row['pdlis_aespa']; ?></td>
                                <td class="tabla"><?php echo $row['talla_descr']; ?></td>
                                <td class="tabla"><?php echo $ajustes; ?></td>
                                <td class="tabla"><?php echo $row['pdlis_obser']; ?></td>
                                <td class="tabla"><a href="../detalleListado/modificar?p=<?php echo $row['pedid_codig'] ?>&d=<?php echo $row['pdeta_codig'] ?>&s=<?php echo $row['pdeta_pasos'] ?>&c=<?php echo $row['pdlis_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-pencil"></i></a></td>
                                <td class="tabla"><a href="../detalleListado/eliminar?p=<?php echo $row['pedid_codig'] ?>&d=<?php echo $row['pdeta_codig'] ?>&s=<?php echo $row['pdeta_pasos'] ?>&c=<?php echo $row['pdlis_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-close"></i></a></td>
                            </tr>
                            <?php
                                }   
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div> 
</div> 
<div class="row">                    
    <div class="panel panel-default" >
        <div class="panel-body">
            <div class="row controls">
                <div class="col-sm-6 ">
                    <a href="modificar?p=<?php echo $p?>&c=<?php echo $c?>&s=1" type="reset" class="btn-block btn btn-default">Volver </a>
                </div>
                <div class="col-sm-6 ">
                    <button id="siguiente" type="button" class="btn-block btn btn-info">Finalizar  </button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var timag = {
            validators: {
                notEmpty: {
                    message: 'Estimado(a) Usuario(a) el campo "Tipo de Imagen" es obligatorio',
                }
            }
        },
        emoji = {
            validators: {
                notEmpty: {
                    message: 'Estimado(a) Usuario(a) el campo "Emoji" es obligatorio',
                }
            }
        },
        image = {
            validators: {
                notEmpty: {
                    message: 'Estimado(a) Usuario(a) el campo "Imagen Libre" es obligatorio',
                }
            }
        },
        simbo = {
            validators: {
                notEmpty: {
                    message: 'Estimado(a) Usuario(a) el campo "Simbolo" es obligatorio',
                }
            }
        },
        texto = {
            validators: {
                notEmpty: {
                    message: 'Estimado(a) Usuario(a) el campo "Texto a bordar" es obligatorio',
                }
            }
        },
        bookIndex = 0,
        contador = 0;
        
   </script>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                ndocu: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "RUT" es obligatorio',
                        }
                    }
                },
                nombr: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Nombre" es obligatorio',
                        }
                    }
                },
                talla: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Talla" es obligatorio',
                        }
                    }
                },
                npers: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Nombre Personalizado" es obligatorio',
                        }
                    }
                },
                /*aespa: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Apodo Espalda" es obligatorio',
                        }
                    }
                },*/
                /*elect: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Electivo" es obligatorio',
                        }
                    }
                },*/
                'timag[0]': timag,
                'emoji[0]': emoji,
                'simbo[0]': simbo,
                'image[0]': image,
                'texto[0]': texto,
            }
        });
    }).on('click', '.addButton', function() {
        //if(contador<4){ 
            contador++;
            //alert(contador);
            bookIndex++;
         
            document.getElementById('bookindex').value = bookIndex;
            var $template = $('#bookTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .attr('data-book-index', bookIndex)
                                .insertBefore($template);

            // Update the name attributes
            $clone
                .find('[name="timag_"]').attr('name', 'timag[' + bookIndex + ']').end()
                .find('[name="emoji_"]').attr('name', 'emoji[' + bookIndex + ']').end()
                .find('[name="simbo_"]').attr('name', 'simbo[' + bookIndex + ']').end()
                .find('[name="image_"]').attr('name', 'image[' + bookIndex + ']').end()

                .find('[name="texto_"]').attr('name', 'texto[' + bookIndex + ']').end()
                
                .find('[id="timag_"]').attr('id', 'timag_' + bookIndex ).end()
                .find('[id="opcional-1_"]').attr('id', 'opcional-1_' + bookIndex ).end()
                .find('[id="emoji_"]').attr('id', 'emoji_' + bookIndex ).end()
                .find('[id="simbo_"]').attr('id', 'simbo_' + bookIndex ).end()
                .find('[id="login_"]').attr('id', 'login_' + bookIndex ).end()
                .find('[id="login-2_"]').attr('id', 'login-2_' + bookIndex ).end()
                .find('[id="popover-img_"]').attr('id', 'popover-img_' + bookIndex ).end()
                .find('[id="popover-img-2_"]').attr('id', 'popover-img-2_' + bookIndex ).end()
                .find('[id="opcional-2_"]').attr('id', 'opcional-2_' + bookIndex ).end()
                .find('[id="opcional-3_"]').attr('id', 'opcional-3_' + bookIndex ).end()
                .find('[id="opcional-4_"]').attr('id', 'opcional-4_' + bookIndex ).end()
                .find('[id="image_"]').attr('id', 'image_' + bookIndex ).end()
                .find('[id="texto_"]').attr('id', 'texto_' + bookIndex ).end()
                .find('[class="numero1_"]').attr('class', 'numero1_' + bookIndex ).end();

            // Add new fields
            // Note that we also pass the validator rules for new field as the third parameter
            $('#login-form')
                .formValidation('addField', 'timag[' + bookIndex + ']', timag)
                .formValidation('addField', 'emoji[' + bookIndex + ']', emoji)
                .formValidation('addField', 'simbo[' + bookIndex + ']', simbo)
                .formValidation('addField', 'image[' + bookIndex + ']', image)
                .formValidation('addField', 'texto[' + bookIndex + ']', texto);
            

            //agregar ajax
            $('#login-form').append(
                '<script type="text/javascript">'+
                    '$("#timag_'+bookIndex+'").change(function () {'+
                        'var timag = $(this).val();'+
                        'if(timag==null || timag=="" || timag=="4"){'+
                            'jQuery("#opcional-3_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#opcional-4_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#opcional-2_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#opcional-1_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#image_'+bookIndex+'").attr("disabled","true");'+
                            'jQuery("#emoji_'+bookIndex+'").attr("disabled","true");'+
                            'jQuery("#simbo_'+bookIndex+'").attr("disabled","true");'+
                            'jQuery("#texto_'+bookIndex+'").attr("disabled","true");'+
                        '}else if(timag=="1"){'+

                            'jQuery("#opcional-4_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#opcional-1_'+bookIndex+'").removeClass("hide");'+
                            'jQuery("#opcional-2_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#opcional-3_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#emoji_'+bookIndex+'").removeAttr("disabled");'+
                            'jQuery("#image_'+bookIndex+'").attr("disabled","true");'+
                            'jQuery("#simbo_'+bookIndex+'").attr("disabled","true");'+
                            'jQuery("#texto_'+bookIndex+'").attr("disabled","true");'+
                        '}else if(timag=="3"){'+
                            'jQuery("#opcional-4_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#opcional-3_'+bookIndex+'").removeClass("hide");'+
                            'jQuery("#opcional-2_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#opcional-1_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#simbo_'+bookIndex+'").removeAttr("disabled");'+
                            'jQuery("#image_'+bookIndex+'").attr("disabled","true");'+
                            'jQuery("#emoji_'+bookIndex+'").attr("disabled","true");'+
                            'jQuery("#texto_'+bookIndex+'").attr("disabled","true");'+
                        '}else if(timag=="5"){'+
                            'jQuery("#opcional-4_'+bookIndex+'").removeClass("hide");'+
                            'jQuery("#opcional-2_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#opcional-1_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#opcional-3_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#texto_'+bookIndex+'").removeAttr("disabled");'+
                            'jQuery("#image_'+bookIndex+'").attr("disabled","true");'+
                            'jQuery("#emoji_'+bookIndex+'").attr("disabled","true");'+
                            'jQuery("#simbo_'+bookIndex+'").attr("disabled","true");'+
                        '}else {'+
                            'jQuery("#opcional-4_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#opcional-3_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#opcional-2_'+bookIndex+'").removeClass("hide");'+
                            'jQuery("#opcional-1_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#image_'+bookIndex+'").removeAttr("disabled");'+
                            'jQuery("#emoji_'+bookIndex+'").attr("disabled","true");'+
                            'jQuery("#simbo_'+bookIndex+'").attr("disabled","true");'+
                            'jQuery("#texto_'+bookIndex+'").attr("disabled","true");'+

                        '}'+
                    '});'+
                '<\/script>'
            );

            $('#login-form').append(
                '<script type="text/javascript">\n'+
                    '$(document).on("click", "#close-preview", function () {\n'+
                        '$(".numero1_'+bookIndex+' .image-preview").popover("hide");\n'+
                     
                        '$(".numero1_'+bookIndex+' .image-preview").hover(\n'+
                            'function () {\n'+
                                '$(".image-preview").popover("hide");\n'+
                            '},\n'+
                            'function () {\n'+
                                '$(".image-preview").popover("hide");\n'+
                            '}\n'+
                        ');\n'+
                    '});\n'+
                    '$(function () {\n'+
                       
                        'var closebtn = $("<button/>", {\n'+
                            'type: "button",\n'+
                            'text: "x",\n'+
                            'id: "close-preview",\n'+
                            'style: "font-size: initial;",\n'+
                        '});\n'+
                        'closebtn.attr("class","close pull-right");\n'+
                        '$(".numero1_'+bookIndex+' .image-preview").popover({\n'+
                            'trigger:"manual",\n'+
                            'html:true,\n'+
                            'title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,\n'+
                            'content: "No hay imagen",\n'+
                            'placement:"top"\n'+
                        '});\n'+
                       
                        '$(".numero1_'+bookIndex+' .image-preview-clear").click(function () {\n'+
                            '$(".numero1_'+bookIndex+' .image-preview").attr("data-content", "").popover("hide");\n'+
                            '$(".numero1_'+bookIndex+' .image-preview-filename").val("");\n'+
                            '$(".numero1_'+bookIndex+' .image-preview-clear").hide();\n'+
                            '$(".numero1_'+bookIndex+' .image-preview-input input:file").val("");\n'+
                            '$(".numero1_'+bookIndex+' .image-preview-input-title").text("Buscar");\n'+
                        '});\n'+
                       
                        '$(".numero1_'+bookIndex+' .image-preview-input input:file").change(function () {\n'+
                            'var img = $("<img/>", {\n'+
                                'id: "dynamic",\n'+
                                'width: 250,\n'+
                                'height: 200\n'+
                            '});\n'+
                            'var file = this.files[0];\n'+
                            'var reader = new FileReader();\n'+
                          
                            'reader.onload = function (e) {\n'+
                                '$(".numero1_'+bookIndex+' .image-preview-input-title").text("Cambiar");\n'+
                                '$(".numero1_'+bookIndex+' .image-preview-clear").show();\n'+
                                '$(".numero1_'+bookIndex+' .image-preview-filename").val(file.name);\n'+
                                'img.attr("src", e.target.result);\n'+
                                '$(".numero1_'+bookIndex+' .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");\n'+
                                '}\n'+
                            'reader.readAsDataURL(file);\n'+
                        '});\n'+
                    '});\n'+
                '<\/script>');
            $('#login-form').append('<script type="text/javascript">'+
            '$(document).ready(function(){'+
                '$("#login_'+bookIndex+'").popover({'+
                      'html: true,'+
                      'content: function() {'+
                        'return $("#popover-img_'+bookIndex+'").html();'+
                      '},'+
                      'trigger: "hover"'+
                    '});'+
            '});'+
            '$(document).ready(function(){'+
                '$("#login-2_'+bookIndex+'").popover({'+
                      'html: true,'+
                      'content: function() {'+
                        'return $("#popover-img-2_'+bookIndex+'").html();'+
                      '},'+
                      'trigger: "hover"'+
                    '});'+
            '});'+
            '$("#emoji_'+bookIndex+'").change(function () {'+
                    'var emoji = $(this).val();'+
                    '$.ajax({  '+
                        'url:"<?php echo CController::createUrl("funciones/PedidosEmoji"); ?>",  '+
                        'method:"POST",  '+
                        'async:false,  '+
                        'data:{id:emoji},  '+
                        'success:function(data){  '+
                            'jQuery("#popover-img_'+bookIndex+'").html(data);  '+
                        '}  '+
                    '});'+
                    '$("#login_'+bookIndex+'").popover("hide");'+
                '});'+
            '$("#simbo_'+bookIndex+'").change(function () {'+
                    'var emoji = $(this).val();'+
                    '$.ajax({  '+
                        'url:"<?php echo CController::createUrl("funciones/PedidosSimbolo"); ?>",  '+
                        'method:"POST",  '+
                        'async:false,  '+
                        'data:{id:emoji},  '+
                        'success:function(data){  '+
                            'jQuery("#popover-img-2_'+bookIndex+'").html(data);  '+
                        '}  '+
                    '});'+
                    '$("#login-2_'+bookIndex+'").popover("hide");'+
                '});'+
            '<\/script>');
           

       
    }).on('click', '.removeButton', function() {// Remove button click handler
        //alert(contador);
        contador=contador-1;
        var $row  = $(this).parents('.form-group'),
            index = $row.attr('data-book-index');
        // Remove fields
        $('#login-form')
            .formValidation('removeField', $row.find('[name="timag[' + index + ']"]'))
            .formValidation('removeField', $row.find('[name="emoji[' + index + ']"]'))
            .formValidation('removeField', $row.find('[name="texto[' + index + ']"]'))
            .formValidation('removeField', $row.find('[name="image[' + index + ']"]'));
        // Remove element containing the fields
        $row.remove();
    });
</script>
<script type="text/javascript">
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        var data = new FormData(jQuery('form')[0]);
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                url: '../detalleListado/registrar',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                            Paso2Listado();
                            document.getElementById('login-form').reset();
                            if(response['cantidad']=='<?php echo $pedidos['pdeta_canti']; ?>'){
                                Siguiente();
                            }else{
                                window.open('modificar?p=<?php echo $p; ?>&c=<?php echo $c; ?>&s=2', '_parent');
                            }
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                            Paso2Listado();
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>
<script type="text/javascript">
    function Paso2Listado() {
        $.ajax({
            url: 'Paso2Listado?p=<?php echo $p ?>&c=<?php echo $c ?>',
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            beforeSend: function () {
                $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
            },
            success: function (response) {
                    $('#wrapper').unblock();
                $("#listado-pedido").html(response);
            }
        });
    }
    function Siguiente(){

        var data = new FormData(jQuery('form')[0]);
        $.ajax({
            dataType: "json",
            url: 'paso2',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            beforeSend: function () {
                $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
            },
            success: function (response) {
                    $('#wrapper').unblock();
                if (response['success'] == 'true') {
                    $("#guardar").removeAttr('disabled');
                    /*window.open('modificar?p=<?php echo $p; ?>&s=3&c=<?php echo $c; ?>', '_parent');*/
                    /*swal({ 
                        title: "Exito!",
                        text: response['msg'],
                        type: "success",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-info"
                    },function(){
                        window.open('listado?p=<?php echo $p; ?>', '_parent');
                    });*/
                    swal({   
                        title: "Exito!",   
                        text: response['msg'],
                        type: "success",   
                        showCancelButton: true,   
                        confirmButtonText: "Finalizar Pedido",   
                        cancelButtonText: "Agregar otro Modelo",   
                        closeOnConfirm: false,   
                        closeOnCancel: false 
                    }, function(isConfirm){   
                        if (isConfirm) {     
                            window.open('../pedidos/modificar?c=<?php echo $p; ?>&s=1', '_parent');   
                        } else {     
                            window.open('registrar?p=<?php echo $p; ?>', '_parent');   
                        } 
                    });
                } else {
                    swal({ 
                        title: "Error!",
                        text: response['msg'],
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"
                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                }

            },error:function (response) {
                    $('#wrapper').unblock();
                swal({ 
                    title: "Error!",
                    text: "Error el ejecutar la operación",
                    type: "error",
                    confirmButtonText: "Cerrar",
                    confirmButtonClass: "btn-danger"

                },function(){
                    $("#guardar").removeAttr('disabled');
                });
                    
            }
        });
        
    }
</script>
<script type="text/javascript">
    $('#siguiente').click(function () { 
        var data = new FormData(jQuery('form')[0]);
        $.ajax({
            dataType: "json",
            url: 'paso2',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            beforeSend: function () {
                $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
            },
            success: function (response) {
                    $('#wrapper').unblock();
                if (response['success'] == 'true') {
                    $("#guardar").removeAttr('disabled');
                    /*window.open('modificar?p=<?php echo $p; ?>&s=3&c=<?php echo $c; ?>', '_parent');*/
                    /*swal({ 
                        title: "Exito!",
                        text: response['msg'],
                        type: "success",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-info"
                    },function(){
                        window.open('listado?p=<?php echo $p; ?>', '_parent');
                    });*/
                    swal({   
                        title: "Exito!",   
                        text: response['msg'],
                        type: "success",   
                        showCancelButton: true,   
                        confirmButtonText: "Finalizar Pedido",   
                        cancelButtonText: "Agregar otro Modelo",   
                        closeOnConfirm: false,   
                        closeOnCancel: false 
                    }, function(isConfirm){   
                        if (isConfirm) {     
                            window.open('../pedidos/modificar?c=<?php echo $p; ?>&s=1', '_parent');   
                        } else {     
                            window.open('registrar?p=<?php echo $p; ?>', '_parent');   
                        } 
                    });
                } else {
                    swal({ 
                        title: "Error!",
                        text: response['msg'],
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"
                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                }

            },error:function (response) {
                    $('#wrapper').unblock();
                swal({ 
                    title: "Error!",
                    text: "Error el ejecutar la operación",
                    type: "error",
                    confirmButtonText: "Cerrar",
                    confirmButtonClass: "btn-danger"

                },function(){
                    $("#guardar").removeAttr('disabled');
                });
                    
            }
        });
        Siguiente();
    });
</script>
<script type="text/javascript">
    $('#timag_0').change(function () {
        var timag = $(this).val();
        if(timag==null || timag=='' || timag=='4'){
            jQuery("#opcional-4_0").addClass('hide');
            jQuery("#opcional-3_0").addClass('hide');
            jQuery("#opcional-2_0").addClass('hide');
            jQuery("#opcional-1_0").addClass('hide');
            jQuery("#image_0").attr('disabled','true');
            jQuery("#emoji_0").attr('disabled','true');    
            jQuery("#simbo_0").attr('disabled','true');
            jQuery("#texto_0").attr('disabled','true');    
        }else if(timag=='1'){
            jQuery("#opcional-4_0").addClass('hide');
            jQuery("#opcional-1_0").removeClass('hide');
            jQuery("#opcional-2_0").addClass('hide');
            jQuery("#opcional-3_0").addClass('hide');
            jQuery("#emoji_0").removeAttr('disabled');
            jQuery("#image_0").attr('disabled','true');    
            jQuery("#texto_0").attr('disabled','true');
            jQuery("#simbo_0").attr('disabled','true'); 
        }else if(timag=='3'){
            jQuery("#opcional-4_0").addClass('hide');
            jQuery("#opcional-3_0").removeClass('hide');
            jQuery("#opcional-2_0").addClass('hide');
            jQuery("#opcional-1_0").addClass('hide');
            jQuery("#simbo_0").removeAttr('disabled');
            jQuery("#image_0").attr('disabled','true');    
            jQuery("#texto_0").attr('disabled','true');
            jQuery("#emoji_0").attr('disabled','true'); 
        }else if(timag=='5'){
            jQuery("#opcional-4_0").removeClass('hide');
            jQuery("#opcional-3_0").addClass('hide');
            jQuery("#opcional-2_0").addClass('hide');
            jQuery("#opcional-1_0").addClass('hide');
            jQuery("#texto_0").removeAttr('disabled');
            jQuery("#image_0").attr('disabled','true');    
            jQuery("#simbo_0").attr('disabled','true');
            jQuery("#emoji_0").attr('disabled','true'); 
        }else {
            jQuery("#opcional-4_0").addClass('hide');
            jQuery("#opcional-2_0").removeClass('hide');
            jQuery("#opcional-1_0").addClass('hide');
            jQuery("#opcional-3_0").addClass('hide');
            jQuery("#image_0").removeAttr('disabled');
            jQuery("#emoji_0").attr('disabled','true');
            jQuery("#texto_0").attr('disabled','true');
            jQuery("#simbo_0").attr('disabled','true'); 
        }
               
    });
</script>
<script type="text/javascript">
    $('#ideta').change(function () {
        var ideta = $(this).val();
        switch(ideta) {
            case '1':
                jQuery("#iclin").removeAttr('disabled');
                break;
            default:
                jQuery("#iclin").attr('disabled','true');
                break;
        }
                
            
    });
</script>
<script>

$(document).on('click', '#close-preview', function () {
    $('.numero1_0 .image-preview').popover('hide');
    // Hover befor close the preview
    $('.numero1_0 .image-preview').hover(
        function () {
            $('.image-preview').popover('hide');
        },
        function () {
            $('.image-preview').popover('hide');
        }
    );
});
$(function () {
    // Create the close button
    var closebtn = $('<button/>', {
        type: "button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    $('.numero1_0 .image-preview').popover({
        trigger:'manual',
        html:true,
        title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
        content: "No hay imagen",
        placement:'top'
    });



    
    // Clear event
    $('.numero1_0 .image-preview-clear').click(function () {
        $('.numero1_0 .image-preview').attr("data-content", "").popover('hide');
        $('.numero1_0 .image-preview-filename').val("");
        $('.numero1_0 .image-preview-clear').hide();
        $('.numero1_0 .image-preview-input input:file').val("");
        $(".numero1_0 .image-preview-input-title").text("Buscar");
    });
    // Create the preview image
    $(".numero1_0 .image-preview-input input:file").change(function () {
        var img = $('<img/>', {
            id: 'dynamic',
            width: 250,
            height: 200
        });
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".numero1_0 .image-preview-input-title").text("Cambiar");
            $(".numero1_0 .image-preview-clear").show();
            $(".numero1_0 .image-preview-filename").val(file.name);
            img.attr('src', e.target.result);
            $(".numero1_0 .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
            }
        reader.readAsDataURL(file);
    });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#login_0').popover({
          html: true,
          content: function() {
            return $('#popover-img_0').html();
          },
          trigger: 'hover'
        });
});
$('#emoji_0').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosEmoji'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img_0").html(data);  
            }  
        });
        $('#login_0').popover('hide');
    });
$(document).ready(function(){
    $('#login-2_0').popover({
          html: true,
          content: function() {
            return $('#popover-img-2_0').html();
          },
          trigger: 'hover'
        });
});
$('#simbo_0').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosSimbolo'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-2_0").html(data);  
            }  
        });
        $('#login-2_0').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#sa-params').click(function(){
        swal({   
            title: "Are you sure?",   
            text: "You will not be able to recover this imaginary file!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                swal("Deleted!", "Your imaginary file has been deleted.", "success");   
            } else {     
                swal("Cancelled", "Your imaginary file is safe :)", "error");   
            } 
        });
    });
 });
</script>

<!--script type="text/javascript">
  <script type="text/javascript">
    jQuery(document).ready(function(){
    jQuery('#emoji').popover({
    title: popoverContent,
    html: true,
    placement: 'right',
    trigger: 'hover'
    });
    });
</script>  

$('#emoji').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosEmoji'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:id},  
            success:function(data){  
                fetch_data = data;  
            }  
        });
        $('#emoji').popover({  
                title:fetch_data,  
                html:true,  
                trigger: 'focus', 
                placement:'right'  
           });       
    });
</script>
 <script>  
      $(document).ready(function(){  
           $('.emoji').popover({  
                title:fetchData,  
                html:true,  
                trigger: 'focus', 
                placement:'right'  
           });  
           function fetchData(){  
                var fetch_data = '';  
                var element = $(this);  
                var id = $(this).val();  
                $.ajax({  
                     url:"<?php echo CController::createUrl('funciones/PedidosEmoji'); ?>",  
                     method:"POST",  
                     async:false,  
                     data:{id:id},  
                     success:function(data){  
                          fetch_data = data;  
                     }  
                });  
                return fetch_data;  
           }  
      });  
 </script--> 