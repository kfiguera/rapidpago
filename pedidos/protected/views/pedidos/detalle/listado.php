<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Detalle de Pedido</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Pedidos</a></li>
            <li><a href="#">Detalle</a></li>
            <li class="active">Listado</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Datos del Pedido</h3>
    </div>
    <div class="panel-body" >
        <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'pedidos', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            $connection = Yii::app()->db;

        ?>
        <div class="row">
            <div class="col-sm-12 hide">        
                <div class="form-group">
                    <label>Código *</label>
                    <?php echo CHtml::hiddenField('codig', $pedidos['pedid_codig'], array('class' => 'form-control', 'placeholder' => "Total a Solicitar",'disabled'=>'true')); ?>
                </div>
            </div>
            
            
        </div>
        <div class="row">
            <div class="col-sm-4">        
                <div class="form-group">
                    <label>Número de Pedido *</label>
                    <?php echo CHtml::textField('numer', $pedidos['pedid_numer'], array('class' => 'form-control', 'placeholder' => "Total a Solicitar",'disabled'=>'true')); ?>
                </div>
            </div>
            <div class="col-sm-4">        
                <div class="form-group">
                    <label>Total a Solicitar *</label>
                    <?php echo CHtml::textField('canti', $pedidos['p_personas'], array('class' => 'form-control', 'placeholder' => "Total a Solicitar",'disabled'=>'true')); ?>
                </div>
            </div>
            <div class="col-sm-4">        
                <div class="form-group">
                    <label>Cantidad de modelos *</label>
                    <?php echo CHtml::textField('mcant', $pedidos['modelos'], array('class' => 'form-control', 'placeholder' => "Cantidad del Modelo",'disabled'=>'true')); ?>
                </div>
            </div>

             <div class="col-sm-4 hide">        
                <div class="form-group">
                    <label>Color del Cuerpo *</label>
                    <?php 
                        $conexion = Yii::app()->db;
                        $sql="SELECT mcolo_codig, concat( mcolo_numer,' - ',mcolo_descr) mcolo_descr
                              FROM pedido_modelo_color 
                              WHERE mcolo_codig = '".$pedidos['mcolo_codig']."'";
                        $result=$conexion->createCommand($sql)->queryAll();
                        $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                        echo CHtml::dropDownList('color', $pedidos['mcolo_codig'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                </div>
            </div>
            
        </div>
        
        
        <div class="row">
                       
            <div class="col-sm-6">
                <a href="../pedidos/listado" class="btn btn-block btn-default" >Volver</a>
            </div>
            <div class="col-sm-6">
                <?php 
                    if($pedidos['epedi_codig']=='1'){
                        if($pedidos['pedid_pasos']=='0'){
                            $pedidos['pedid_pasos']='1';
                        }
                        ?>
                        <a href="../pedidos/modificar?c=<?php echo $p; ?>&s=<?php echo $pedidos['pedid_pasos']; ?>" class="btn btn-block btn-info" >Modificar</a>
                        <?php
                    }else{
                        ?>
                        <a href="#" disabled class="disabled btn btn-block btn-info" >Modificar</a>
                        <?php
                    }
                ?>
                
            </div>
            
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div> 
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-sm-8">
                <h3 class="panel-title">Listado</h3>
            </div>
            <div class="col-sm-4">
                <?php 
                    if($pedidos['epedi_codig']=='1'){
                        ?>
                        <a href="registrar?p=<?php echo $p; ?>" class="btn btn-block btn-info" >Agregar Modelo</a>
                        <?php
                    }else{
                        ?>
                        <a href="#" disabled class="disabled btn btn-block btn-info" >Agregar Modelo</a>
                        <?php
                    }
                ?>
            </div>
        </div>
        
    </div>
    <div class="panel-body" >
        
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-pedido'>
                <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                #
                            </th>
                            <th>
                                Tipo 
                            </th>
                            <th>
                                Modelo
                            </th>
                            <th>
                                Categoria
                            </th>
                            <th>
                                Cantidad
                            </th>
                            <th width="5%">
                                Consultar
                            </th>
                            <th width="5%">
                                Modificar
                            </th>
                            <th width="5%">
                                Eliminar
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "SELECT *,(SELECT count(pdlis_codig) cantidad 
                                          FROM pedido_pedidos_detalle_listado f
                                          WHERE f.pdeta_codig = a.pdeta_codig) cantidad 
                         FROM pedido_pedidos_detalle a
                                JOIN pedido_modelo_tipo b ON (a.tmode_codig = b.tmode_codig)
                                JOIN pedido_modelo c ON (a.model_codig = c.model_codig)
                                JOIN pedido_modelo_categoria d ON (a.mcate_codig = d.mcate_codig)
                                /*JOIN pedido_modelo_color e ON (a.mcolo_codig = e.mcolo_codig)*/
                                WHERE a.pedid_codig='".$p."'";
                            
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                        ?>
                        <tr>
                            <td class="tabla"><?php echo $i ?></td>
                            <td class="tabla"><?php echo $row['tmode_descr'] ?></td>
                            <td class="tabla"><?php echo $row['model_descr'] ?></td>
                            <td class="tabla"><?php echo $row['mcate_descr'] ?></td>
                            <td class="tabla"><?php echo $row['cantidad'] ?></td>
                            <td class="tabla"><a href="consultar?p=<?php echo $row['pedid_codig'] ?>&c=<?php echo $row['pdeta_codig'] ?>&s=<?php echo $row['pdeta_pasos'] ?>" class="btn btn-block btn-info"><i class="fa fa-search"></i></a></td>
                            <?php 
                                if($pedidos['epedi_codig']=='1'){
                                    ?>
                                    <td class="tabla"><a href="modificar?p=<?php echo $row['pedid_codig'] ?>&c=<?php echo $row['pdeta_codig'] ?>&s=<?php echo $row['pdeta_pasos'] ?>" class="btn btn-block btn-info"><i class="fa fa-pencil"></i></a></td>
                                    <td class="tabla"><a href="eliminar?p=<?php echo $row['pedid_codig'] ?>&c=<?php echo $row['pdeta_codig'] ?>&s=<?php echo $row['pdeta_pasos'] ?>" class="btn btn-block btn-info"><i class="fa fa-close"></i></a></td>
                                    <?php
                                }else{
                                    ?>
                                    <td class="tabla"><a href="#" disabled class="disabled btn btn-block btn-info"><i class="fa fa-pencil"></i></a></td>
                                    <td class="tabla"><a href="#" disabled class="disabled btn btn-block btn-info"><i class="fa fa-close"></i></a></td>
                                    <?php
                                }
                                ?>
                            
                        </tr>
                        <?php
                            }   
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div> 
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Finalizar Pedido</h3>       
    </div>
    <div class="panel-body" >
        <div class="col-sm-12">
            <?php 
                if($pedidos['epedi_codig']=='1'){
                    ?>
                    <a href="../pedidos/enviar?c=<?php echo $p; ?>" class="btn btn-block btn-info" >Enviar</a>
                    <?php
                }else{
                    ?>
                    <a href="#" disabled class="disabled btn btn-block btn-info" >Enviar</a>
                    <?php
                }
            ?>            
        </div>
    </div>
</div>  


<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#buscar').click(function () {
        var formData = new FormData($("#pedidos")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-pedido').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>
