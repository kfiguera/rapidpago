<table  id='auditoria'  class="table table-bordered table-hover dataTable">
    <thead>
        <tr>
            <th width="2%">
                #
            </th>
            <th>
                Número de Listado
            </th>
            <th width="5%">
                Listados
            </th>
            <th width="5%">
                Consultar
            </th>
            <th width="5%">
                Modificar
            </th>
            <th width="5%">
                Eliminar
            </th>
        </tr>
    </thead>
    <tbody>
        <?php
        $sql = "SELECT * FROM factura_producto a 
                                JOIN inventario b ON (a.inven_codig=b.inven_codig)
                                 ".$condicion;
        $connection= yii::app()->db;
        $command = $connection->createCommand($sql);
        $p_persona = $command->query();
        $i=0;
        while (($row = $p_persona->read()) !== false) {
            $i++;
        ?>
        <tr>
                            <td class="tabla"><?php echo $i ?></td>
                            <td class="tabla"><?php echo $row['inven_descr'] ?></td>
                            <td class="tabla"><?php echo $row['fprod_canti'] ?></td>

                            <td class="tabla"><a href="consultar?f=<?php echo $f ?>&c=<?php echo $row['fprod_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-search"></i></a></td>
                            <td class="tabla"><a href="modificar?f=<?php echo $f ?>&c=<?php echo $row['fprod_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-pencil"></i></a></td>
                            <td class="tabla"><a href="eliminar?f=<?php echo $f ?>&c=<?php echo $row['fprod_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-close"></i></a></td>
                        </tr>
        <?php
            }   
        ?>
    </tbody>
</table>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable(); 
    });
</script>
