<style>
    .image-preview-input {
        position: relative;
        overflow: hidden;
        margin: 0px;    
        color: #333;
        background-color: #fff;
        border-color: #ccc;    
    }
    .image-preview-input input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }
    .image-preview-input-title {
        margin-left:2px;
    }
</style> 
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Listado</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Pedidos</a></li>
            <li><a href="#">Detalles</a></li>            
            <li><a href="#">Listado</a></li>
            <li class="active">Modificar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Modificar Listado</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row hide">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Pedido *</label>
                            <?php 
                                echo CHtml::hiddenField('pedid', $p, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Detalle *</label>
                            <?php 
                                echo CHtml::hiddenField('detal', $d, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Código *</label>
                            <?php 
                                echo CHtml::hiddenField('codig', $c, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>RUT *</label>
                            <?php 
                                echo CHtml::textField('ndocu', $lista['pdlis_ndocu'], array('class' => 'form-control', 'placeholder' => "RUT",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Nombre *</label>
                            <?php 
                                echo CHtml::textField('nombr', $lista['pdlis_nombr'], array('class' => 'form-control', 'placeholder' => "Nombre",'prompt'=>'Seleccione...')); ?>
                            <span class="help-block">
                                <small>Nota: No va Bordado</small>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Talla *</label>
                            <?php 
                                $sql="SELECT * FROM pedido_talla ORDER BY 1";
                                $result= $conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'talla_codig','talla_descr');
                                echo CHtml::dropDownList('talla', $lista['talla_codig'], $data, array('class' => 'form-control', 'placeholder' => "Talla",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Ajuste de Talla *</label>
                            <?php 
                                $sql="SELECT a.*, b.tajus_descr grupo 
                                      FROM pedido_talla_ajuste a 
                                      JOIN pedido_talla_ajuste b ON (a.tajus_padre = b.tajus_codig) 
                                      WHERE a.tajus_super='0' ";
                                $result= $conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'tajus_codig','tajus_descr','grupo');
                                var_dump(json_decode($lista['pdlis_tajus']));
                                echo CHtml::dropDownList('tajus', json_decode($lista['pdlis_tajus']), $data, array('class' => 'select2 select2-multiple', 'placeholder' => "Ajuste de Talla",'prompt'=>'Seleccione...','multiple'=>'multiple')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Nombre o Apodo Personalizado *</label>
                            <?php 
                                echo CHtml::textField('npers', $lista['pdlis_npers'], array('class' => 'form-control', 'placeholder' => "Nombre Personalizado",'prompt'=>'Seleccione...')); ?>
                            <span class="help-block">
                                <small>Nota: Nombre a bordar adelante</small>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Nombre o Apodo Espalda *</label>
                            <?php 
                                echo CHtml::textField('aespa', $lista['pdlis_aespa'], array('class' => 'form-control', 'placeholder' => "Apodo Espalda",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    
                </div>    
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Electivo *</label>
                            <?php 
                                echo CHtml::textField('elect', $lista['pdlis_elect'], array('class' => 'form-control', 'placeholder' => "Electivo",'prompt'=>'Seleccione...')); ?>
                            <span class="help-block">
                                <small>Ejemplo: Humanista, Científico</small>
                            </span>
                        </div>
                    </div>
                </div>
                <!-- OPCIONALES-->
                <input class="form-control" placeholder="Porcentaje" readonly="readonly" type="hidden" value="" name="bookindex" id="bookindex" /> 
                    
                        <!-- OPCIONALES-->
                        <?php
                            $sql="SELECT * 
                                  FROM pedido_detalle_listado_imagen
                                  WHERE pedid_codig='".$p."'
                                    AND pdlis_codig='".$lista['pdlis_codig']."' ";
                            $imagenPersona=$conexion->createCommand($sql)->query();
                            $a=0;
                            $row = $imagenPersona->read();
                            $i=0;
                            do{
                                $a=$row['pdlim_codig'];
                                switch ($row['pdlim_timag']) {
                                    case '1':
                                        $opcional['1']['hide']='';
                                        $opcional['1']['disabled']='';
                                        $opcional['2']['hide']='hide';
                                        $opcional['2']['disabled']='disabled="true"';
                                        $opcional['3']['hide']='hide';
                                        $opcional['3']['disabled']='true';
                                        $opcional['4']['hide']='hide';
                                        $opcional['4']['disabled']='true';
                                        break;
                                    case '2':
                                        $opcional['2']['hide']='';
                                        $opcional['2']['disabled']='';
                                        $opcional['1']['hide']='hide';
                                        $opcional['1']['disabled']='true';
                                        $opcional['3']['hide']='hide';
                                        $opcional['3']['disabled']='true';
                                        $opcional['4']['hide']='hide';
                                        $opcional['4']['disabled']='true';
                                        break;
                                    case '3':
                                        $opcional['3']['hide']='';
                                        $opcional['3']['disabled']='';
                                        $opcional['2']['hide']='hide';
                                        $opcional['2']['disabled']='disabled="true"';
                                        $opcional['1']['hide']='hide';
                                        $opcional['1']['disabled']='true';
                                        $opcional['4']['hide']='hide';
                                        $opcional['4']['disabled']='true';
                                        break;
                                    
                                    case '5':
                                        $opcional['4']['hide']='';
                                        $opcional['4']['disabled']='';
                                        $opcional['2']['hide']='hide';
                                        $opcional['2']['disabled']='disabled="true"';
                                        $opcional['1']['hide']='hide';
                                        $opcional['1']['disabled']='true';
                                        $opcional['3']['hide']='hide';
                                        $opcional['3']['disabled']='true';
                                        break;
                                    default:
                                        $opcional['1']['hide']='hide';
                                        $opcional['1']['disabled']='true';
                                        $opcional['2']['hide']='hide';
                                        $opcional['2']['disabled']='disabled="true"';
                                        $opcional['3']['hide']='hide';
                                        $opcional['3']['disabled']='true';
                                        $opcional['4']['hide']='hide';
                                        $opcional['4']['disabled']='true';
                                        break;
                                }
                                ?>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-4">        
                                            <label>Tipo de Imagen *</label>
                                            <?php 
                                                $sql="SELECT * FROM pedido_precio ORDER BY 1";
                                                $result= $conexion->createCommand($sql)->queryAll();
                                                $data=CHtml::listData($result,'pprec_codig','pprec_descr');
                                        
                                                echo CHtml::dropDownList('timag['.$a.']', $row['pdlim_timag'], $data, array('class' => 'form-control', 'placeholder' => "Tipo de Imagen",'prompt'=>'Seleccione...','id'=>'timag_'.$a.'')); ?>
                                        </div>
                                        <div class="<?php echo $opcional['1']['hide'] ?>" id="opcional-1_<?php echo $a; ?>">
                                            <div class="col-xs-7">        
                                                    <label>Emoji *</label>
                                                    <div class="input-group">
                                                        <?php
                                                        $sql="SELECT * FROM pedido_emoji order by 2";
                                                        $result=$conexion->createCommand($sql)->queryAll();
                                                        $data=CHtml::listData($result,'emoji_codig','emoji_descr');
                                                        echo CHtml::dropDownList('emoji['.$a.']', $row['emoji_codig'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','id'=>'emoji_'.$a, 'disabled'=> $opcional['1']['disabled'])); ?>
                                                        
                                                            <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="login_<?php echo $a; ?>" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                                        
                                                    </div>
                                                    <div class="hide" id="popover-img_<?php echo $a; ?>">
                                                        <?php
                                                            $sql="SELECT * FROM pedido_emoji where emoji_codig = '".$row['emoji_codig']."'";
                                                            $emoji=$conexion->createCommand($sql)->queryRow();
                                                            ?>
                                                        <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$emoji['emoji_ruta'] ?>" class="img-responsive">
                                                    </div>
                                               
                                            </div>
                                        </div>
                                        <div class="<?php echo $opcional['2']['hide'] ?>" id="opcional-2_<?php echo $a; ?>">
                                            <div class="col-xs-7">
                                                <div class="numero1_<?php echo $a; ?> form-group">
                                                    <label>Imagen Personalizada</label>
                                                    <div  class="input-group image-preview" data-placement="top" >  

                                                        <img id="dynamic">
                                                        <!-- image-preview-filename input [CUT FROM HERE]-->
                                                        <?php 
                                                            
                                                            $ruta=$row['pdlim_rimag'];
                                                            
                                                            $nombre=explode('/', $ruta);
                                                            $nombre=end($nombre);
                                                            if($nombre){
                                                                $opcional['1']['hide']='';
                                                                $opcional['1']['show']='true';
                                                            }else{
                                                                $opcional['1']['hide']='hide';
                                                                $opcional['1']['show']='false';
                                                            }
                                                        ?>
                                                        <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="true" value="<?php echo $nombre ?>" > <!-- don't give a name === doesn't send on POST/GET -->
                                                        <span class="input-group-btn">
                                                            <!-- image-preview-clear button -->
                                                            <button type="button" class="btn btn-default image-preview-clear <?php echo $opcional['1']['hide'] ?>" >
                                                                <span class="fa fa-times"></span> Limpiar
                                                            </button>
                                                            <!-- image-preview-input -->
                                                            <div class="btn btn-default image-preview-input">
                                                                <span class="fa fa-folder-open"></span>
                                                                <span class="numero1 image-preview-input-title">Buscar</span>
                                                                <input type="file" id="image_<?php echo $a; ?>" name="image[<?php echo $a; ?>]" /> <!-- rename it -->
                                                            </div>
                                                        </span>
                                                   
                                                    </div> 
                                                </div> 
                                                <div class="hide" id="popover-numero-<?php echo $a; ?>">
                                                    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$ruta ?>" class="img-responsive">
                                                </div>
                                                <?php
                                                    if($opcional['1']['show']=='true') { 
                                                ?>
                                                <script type="text/javascript">
                                                    $(document).ready(function () {
                                                        var closebtn = $('<button/>', {
                                                            type: "button",
                                                            text: 'x',
                                                            id: 'close-preview',
                                                            style: 'font-size: initial;',
                                                        });
                                                        closebtn.attr("class","close pull-right");
                                                        $('.numero1_<?php echo $a; ?> .image-preview').popover({
                                                          html: true,
                                                          title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
                                                          content: function() {
                                                            return $('#popover-numero-<?php echo $a; ?>').html();
                                                          },
                                                          trigger: 'manual'
                                                        });

                                                        $(".numero1_<?php echo $a; ?> .image-preview").popover("show");
                                                    });
                                                </script>
                                                <?php
                                                    }
                                                ?>        
                                                <script>

                                                    $(document).on('click', '#close-preview', function () {
                                                        $('.numero1_<?php echo $a; ?> .image-preview').popover('hide');
                                                        // Hover befor close the preview
                                                        $('.numero1_<?php echo $a; ?> .image-preview').hover(
                                                            function () {
                                                                $('.image-preview').popover('hide');
                                                            },
                                                            function () {
                                                                $('.image-preview').popover('hide');
                                                            }
                                                        );
                                                    });
                                                    $(function () {
                                                        // Create the close button
                                                        var closebtn = $('<button/>', {
                                                            type: "button",
                                                            text: 'x',
                                                            id: 'close-preview',
                                                            style: 'font-size: initial;',
                                                        });
                                                        closebtn.attr("class","close pull-right");
                                                        $('.numero1_<?php echo $a; ?> .image-preview').popover({
                                                            trigger:'manual',
                                                            html:true,
                                                            title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
                                                            content: "No hay imagen",
                                                            placement:'top'
                                                        });



                                                        
                                                        // Clear event
                                                        $('.numero1_<?php echo $a; ?> .image-preview-clear').click(function () {
                                                            $('.numero1_<?php echo $a; ?> .image-preview').attr("data-content", "").popover('hide');
                                                            $('.numero1_<?php echo $a; ?> .image-preview-filename').val("");
                                                            $('.numero1_<?php echo $a; ?> .image-preview-clear').hide();
                                                            $('.numero1_<?php echo $a; ?> .image-preview-input input:file').val("");
                                                            $(".numero1_<?php echo $a; ?> .image-preview-input-title").text("Buscar");
                                                        });
                                                        // Create the preview image
                                                        $(".numero1_<?php echo $a; ?> .image-preview-input input:file").change(function () {
                                                            var img = $('<img/>', {
                                                                id: 'dynamic',
                                                                width: 250,
                                                                height: 200
                                                            });
                                                            var file = this.files[0];
                                                            var reader = new FileReader();
                                                            // Set preview image into the popover data-content
                                                            reader.onload = function (e) {
                                                                $(".numero1_<?php echo $a; ?> .image-preview-input-title").text("Cambiar");
                                                                $(".numero1_<?php echo $a; ?> .image-preview-clear").show();
                                                                $(".numero1_<?php echo $a; ?> .image-preview-filename").val(file.name);
                                                                img.attr('src', e.target.result);
                                                                $(".numero1_<?php echo $a; ?> .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
                                                                }
                                                            reader.readAsDataURL(file);
                                                        });
                                                    });
                                                </script>
                                            </div>
                                        </div>
                                        <div class="<?php echo $opcional['3']['hide'] ?>" id="opcional-3_<?php echo $a; ?>">
                                            <div class="col-xs-7">        
                                                    <label>Simbolo Simple *</label>
                                                    <div class="input-group">
                                                        <?php
                                                        $sql="SELECT * FROM pedido_simbolo_simple ";
                                                        $result=$conexion->createCommand($sql)->queryAll();
                                                        $data=CHtml::listData($result,'ssimp_codig','ssimp_descr');
                                                        echo CHtml::dropDownList('simbo['.$a.']', $row['ssimp_codig'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','id'=>'simbo_'.$a.'' , 'disabled'=> $opcional['3']['disabled'])); ?>
                                                        
                                                            <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="login-2_<?php echo $a; ?>" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                                        
                                                    </div>
                                                    <div class="hide" id="popover-img-2_<?php echo $a; ?>">
                                                        <?php
                                                            $sql="SELECT * FROM pedido_simbolo_simple where ssimp_codig = '".$row['ssimp_codig']."'";
                                                            $emoji=$conexion->createCommand($sql)->queryRow();
                                                            ?>
                                                        <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$emoji['ssimp_ruta'] ?>" class="img-responsive">
                                                    </div>
                                               
                                            </div>
                                        </div>
                                        <div class="<?php echo $opcional['4']['hide'] ?>" id="opcional-4_<?php echo $a; ?>">
                                            <div class="col-xs-7">        
                                                    <label>Texto a bordar *</label>
                                                        <?php
                                                        
                                                        echo CHtml::textField('texto['.$a.']', $row['pdlim_texto'], array('class' => 'form-control', 'placeholder' => "Texto a Bordar",'prompt'=>'Seleccione...','id'=>'texto_'.$a.'',  'disabled'=> $opcional['4']['disabled'])); ?>
                                                        
                                               
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                            $('#timag_<?php echo $a; ?>').change(function () {
                                                var timag = $(this).val();
                                                if(timag==null || timag=='' || timag=='4'){
                                                    jQuery("#opcional-4_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#opcional-3_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#opcional-2_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#opcional-1_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#image_<?php echo $a; ?>").attr('disabled','true');
                                                    jQuery("#emoji_<?php echo $a; ?>").attr('disabled','true');    
                                                    jQuery("#simbo_<?php echo $a; ?>").attr('disabled','true');
                                                    jQuery("#texto_<?php echo $a; ?>").attr('disabled','true');    
                                                }else if(timag=='1'){
                                                    jQuery("#opcional-4_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#opcional-1_<?php echo $a; ?>").removeClass('hide');
                                                    jQuery("#opcional-2_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#opcional-3_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#emoji_<?php echo $a; ?>").removeAttr('disabled');
                                                    jQuery("#image_<?php echo $a; ?>").attr('disabled','true');    
                                                    jQuery("#texto_<?php echo $a; ?>").attr('disabled','true');
                                                    jQuery("#simbo_<?php echo $a; ?>").attr('disabled','true'); 
                                                }else if(timag=='3'){
                                                    jQuery("#opcional-4_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#opcional-3_<?php echo $a; ?>").removeClass('hide');
                                                    jQuery("#opcional-2_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#opcional-1_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#simbo_<?php echo $a; ?>").removeAttr('disabled');
                                                    jQuery("#image_<?php echo $a; ?>").attr('disabled','true');    
                                                    jQuery("#texto_<?php echo $a; ?>").attr('disabled','true');
                                                    jQuery("#emoji_<?php echo $a; ?>").attr('disabled','true'); 
                                                }else if(timag=='5'){
                                                    jQuery("#opcional-4_<?php echo $a; ?>").removeClass('hide');
                                                    jQuery("#opcional-3_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#opcional-2_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#opcional-1_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#texto_<?php echo $a; ?>").removeAttr('disabled');
                                                    jQuery("#image_<?php echo $a; ?>").attr('disabled','true');    
                                                    jQuery("#simbo_<?php echo $a; ?>").attr('disabled','true');
                                                    jQuery("#emoji_<?php echo $a; ?>").attr('disabled','true'); 
                                                }else {
                                                    jQuery("#opcional-4_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#opcional-2_<?php echo $a; ?>").removeClass('hide');
                                                    jQuery("#opcional-1_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#opcional-3_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#image_<?php echo $a; ?>").removeAttr('disabled');
                                                    jQuery("#emoji_<?php echo $a; ?>").attr('disabled','true');
                                                    jQuery("#texto_<?php echo $a; ?>").attr('disabled','true');
                                                    jQuery("#simbo_<?php echo $a; ?>").attr('disabled','true'); 
                                                }
                                                       
                                            });
                                        </script>
                                        <script type="text/javascript">
                                            $('#timag_<?php echo $a; ?>').change(function () {
                                                var timag = $(this).val();
                                                if(timag==null || timag=='' || timag=='4'){
                                                    jQuery("#opcional-4_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#opcional-3_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#opcional-2_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#opcional-1_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#image_<?php echo $a; ?>").attr('disabled','true');
                                                    jQuery("#emoji_<?php echo $a; ?>").attr('disabled','true');    
                                                    jQuery("#simbo_<?php echo $a; ?>").attr('disabled','true');
                                                    jQuery("#texto_<?php echo $a; ?>").attr('disabled','true');    
                                                }else if(timag=='1'){
                                                    jQuery("#opcional-4_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#opcional-1_<?php echo $a; ?>").removeClass('hide');
                                                    jQuery("#opcional-2_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#opcional-3_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#emoji_<?php echo $a; ?>").removeAttr('disabled');
                                                    jQuery("#image_<?php echo $a; ?>").attr('disabled','true');    
                                                    jQuery("#texto_<?php echo $a; ?>").attr('disabled','true');
                                                    jQuery("#simbo_<?php echo $a; ?>").attr('disabled','true'); 
                                                }else if(timag=='3'){
                                                    jQuery("#opcional-4_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#opcional-3_<?php echo $a; ?>").removeClass('hide');
                                                    jQuery("#opcional-2_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#opcional-1_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#simbo_<?php echo $a; ?>").removeAttr('disabled');
                                                    jQuery("#image_<?php echo $a; ?>").attr('disabled','true');    
                                                    jQuery("#texto_<?php echo $a; ?>").attr('disabled','true');
                                                    jQuery("#emoji_<?php echo $a; ?>").attr('disabled','true'); 
                                                }else if(timag=='5'){
                                                    jQuery("#opcional-4_<?php echo $a; ?>").removeClass('hide');
                                                    jQuery("#opcional-3_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#opcional-2_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#opcional-1_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#texto_<?php echo $a; ?>").removeAttr('disabled');
                                                    jQuery("#image_<?php echo $a; ?>").attr('disabled','true');    
                                                    jQuery("#simbo_<?php echo $a; ?>").attr('disabled','true');
                                                    jQuery("#emoji_<?php echo $a; ?>").attr('disabled','true'); 
                                                }else {
                                                    jQuery("#opcional-4_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#opcional-2_<?php echo $a; ?>").removeClass('hide');
                                                    jQuery("#opcional-1_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#opcional-3_<?php echo $a; ?>").addClass('hide');
                                                    jQuery("#image_<?php echo $a; ?>").removeAttr('disabled');
                                                    jQuery("#emoji_<?php echo $a; ?>").attr('disabled','true');
                                                    jQuery("#texto_<?php echo $a; ?>").attr('disabled','true');
                                                    jQuery("#simbo_<?php echo $a; ?>").attr('disabled','true'); 
                                                }
                                                       
                                            });
                                        </script>
                                        <script>

                                            $(document).on('click', '#close-preview', function () {
                                                $('.numero1_<?php echo $a; ?> .image-preview').popover('hide');
                                                // Hover befor close the preview
                                                $('.numero1_<?php echo $a; ?> .image-preview').hover(
                                                    function () {
                                                        $('.image-preview').popover('hide');
                                                    },
                                                    function () {
                                                        $('.image-preview').popover('hide');
                                                    }
                                                );
                                            });
                                            $(function () {
                                                // Create the close button
                                                var closebtn = $('<button/>', {
                                                    type: "button",
                                                    text: 'x',
                                                    id: 'close-preview',
                                                    style: 'font-size: initial;',
                                                });
                                                closebtn.attr("class","close pull-right");
                                                $('.numero1_<?php echo $a; ?> .image-preview').popover({
                                                    trigger:'manual',
                                                    html:true,
                                                    title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
                                                    content: "No hay imagen",
                                                    placement:'top'
                                                });



                                                
                                                // Clear event
                                                $('.numero1_<?php echo $a; ?> .image-preview-clear').click(function () {
                                                    $('.numero1_<?php echo $a; ?> .image-preview').attr("data-content", "").popover('hide');
                                                    $('.numero1_<?php echo $a; ?> .image-preview-filename').val("");
                                                    $('.numero1_<?php echo $a; ?> .image-preview-clear').hide();
                                                    $('.numero1_<?php echo $a; ?> .image-preview-input input:file').val("");
                                                    $(".numero1_<?php echo $a; ?> .image-preview-input-title").text("Buscar");
                                                });
                                                // Create the preview image
                                                $(".numero1_<?php echo $a; ?> .image-preview-input input:file").change(function () {
                                                    var img = $('<img/>', {
                                                        id: 'dynamic',
                                                        width: 250,
                                                        height: 200
                                                    });
                                                    var file = this.files[0];
                                                    var reader = new FileReader();
                                                    // Set preview image into the popover data-content
                                                    reader.onload = function (e) {
                                                        $(".numero1_<?php echo $a; ?> .image-preview-input-title").text("Cambiar");
                                                        $(".numero1_<?php echo $a; ?> .image-preview-clear").show();
                                                        $(".numero1_<?php echo $a; ?> .image-preview-filename").val(file.name);
                                                        img.attr('src', e.target.result);
                                                        $(".numero1_<?php echo $a; ?> .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
                                                        }
                                                    reader.readAsDataURL(file);
                                                });
                                            });
                                        </script>
                                        <script type="text/javascript">
                                            $(document).ready(function(){
                                                $('#login_<?php echo $a; ?>').popover({
                                                      html: true,
                                                      content: function() {
                                                        return $('#popover-img_<?php echo $a; ?>').html();
                                                      },
                                                      trigger: 'hover'
                                                    });
                                            });
                                            $('#emoji_<?php echo $a; ?>').change(function () {
                                                    var emoji = $(this).val();
                                                    $.ajax({  
                                                        url:"<?php echo CController::createUrl('funciones/PedidosEmoji'); ?>",  
                                                        method:"POST",  
                                                        async:false,  
                                                        data:{id:emoji},  
                                                        success:function(data){  
                                                            jQuery("#popover-img_<?php echo $a; ?>").html(data);  
                                                        }  
                                                    });
                                                    $('#login_<?php echo $a; ?>').popover('hide');
                                                });
                                            $(document).ready(function(){
                                                $('#login-2_<?php echo $a; ?>').popover({
                                                      html: true,
                                                      content: function() {
                                                        return $('#popover-img-2_<?php echo $a; ?>').html();
                                                      },
                                                      trigger: 'hover'
                                                    });
                                            });
                                            $('#simbo_<?php echo $a; ?>').change(function () {
                                                    var emoji = $(this).val();
                                                    $.ajax({  
                                                        url:"<?php echo CController::createUrl('funciones/PedidosSimbolo'); ?>",  
                                                        method:"POST",  
                                                        async:false,  
                                                        data:{id:emoji},  
                                                        success:function(data){  
                                                            jQuery("#popover-img-2_<?php echo $a; ?>").html(data);  
                                                        }  
                                                    });
                                                    $('#login-2_<?php echo $a; ?>').popover('hide');
                                                });
                                        </script>
                                        <?php
                                        if($i==0){
                                        ?>
                                        <div class="col-xs-1">
                                            <label>&nbsp;</label>
                                            <br>
                                            <button type="button" class="btn btn-success btn-block addButton"><i class="fa fa-plus"></i></button>
                                        </div>
                                        <?php
                                            }else{
                                        ?>
                                        <div class="col-xs-1">
                                            <label>&nbsp;</label>
                                            <br>
                                            <div class="btn-group btn-group-justified">
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-success addButton"><i class="fa fa-plus"></i></button>
                                                </div>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-danger removeButton"><i class="fa fa-minus"></i></button>
                                                </div>
                                            </div>
                                        </div>

                                        <?php
                                        }
                                        $a++;
                                        $i++;
                                    ?>
                                    </div>
                                </div>

                                <?php
                            }while(($row = $imagenPersona->read()) !== false);?>
                        
                        <!-- FIN OPCIONALES-->
                    <!-- Plantilla -->
                    <div class="form-group hide" id="bookTemplate">
                        <div class="row">
                                                        <div class="col-xs-4">        
                                    <label>Tipo de Imagen *</label>
                                    <?php 
                                        $sql="SELECT * FROM pedido_precio ORDER BY 1";
                                        $result= $conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'pprec_codig','pprec_descr');
                                        echo CHtml::dropDownList('timag_', '', $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','id'=>'timag_')); ?>
                               
                            </div>
                            <div class="hide" id="opcional-1_">
                                <div class="col-xs-7">        
                                        <label>Emoji *</label>
                                        <div class="input-group">
                                            <?php
                                            $sql="SELECT * FROM pedido_emoji order by 2 ";
                                            $result=$conexion->createCommand($sql)->queryAll();
                                            $data=CHtml::listData($result,'emoji_codig','emoji_descr');
                                            echo CHtml::dropDownList('emoji_', '', $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','id'=>'emoji_' ,'disabled'=>'true')); ?>
                                            
                                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="login_" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                            
                                        </div>
                                        <div class="hide" id="popover-img_"></div>
                                </div>
                            </div>
                            <div class="hide" id="opcional-2_">
                                <div class="col-xs-7">        
                                    <div class="numero1_">
                                        <label>Imagen Libre</label>
                                        <div  class="input-group image-preview">  
                                            <img id="dynamic">
                                            <!-- image-preview-filename input [CUT FROM HERE]-->
                                            <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                            <span class="input-group-btn">
                                                <!-- image-preview-clear button -->
                                                <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                                    <span class="fa fa-times"></span> Limpiar
                                                </button>
                                                <!-- image-preview-input -->
                                                <div class="btn btn-default image-preview-input">
                                                    <span class="fa fa-folder-open"></span>
                                                    <span class="numero1_ image-preview-input-title">Buscar</span>
                                                    <input type="file" id="image_" name="image_"/> <!-- rename it -->
                                                </div>
                                            </span>
                                       
                                        </div> 
                                    </div> 

                                </div>
                                
                            </div>
                            <div class="hide" id="opcional-3_">
                                <div class="col-xs-7">        
                                        <label>Simbolo Simple *</label>
                                        <div class="input-group">
                                            <?php
                                            $sql="SELECT * FROM pedido_simbolo_simple ";
                                            $result=$conexion->createCommand($sql)->queryAll();
                                            $data=CHtml::listData($result,'ssimp_codig','ssimp_descr');
                                            echo CHtml::dropDownList('simbo_', '', $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','id'=>'simbo_' ,'disabled'=>'true')); ?>
                                            
                                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="login-2_" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                            
                                        </div>
                                        <div class="hide" id="popover-img-2_"></div>
                                   
                                </div>
                                
                            </div>
                            <div class="hide" id="opcional-4_">
                                    <div class="col-xs-7">        
                                            <label>Texto a bordar *</label>
                                                <?php
                                                
                                                echo CHtml::textField('texto_', '', array('class' => 'form-control', 'placeholder' => "Texto a Bordar",'prompt'=>'Seleccione...','id'=>'texto_' ,'disabled'=>'true')); ?>
                                                
                                       
                                    </div>
                                </div>
                            <div class="col-xs-1">
                                <label>&nbsp;</label>
                                <br>
                                <div class="btn-group btn-group-justified">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-success addButton"><i class="fa fa-plus"></i></button>
                                    </div>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-danger removeButton"><i class="fa fa-minus"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- FIN OPCIONALES-->
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observaciones</label>
                            <?php echo CHtml::textArea('obser', $lista['pdlis_obser'], array('class' => 'form-control', 'placeholder' => "Observaciones")); ?>
                        </div>
                    </div>
                </div>
                <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-4 ">
                            <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <a href="../detalle/modificar?p=<?php echo $p; ?>&s=2&c=<?php echo $d; ?>" type="reset" class="btn-block btn btn-default">Volver </a>
                        </div>
                    </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
<script type="text/javascript">
    var timag = {
            validators: {
                notEmpty: {
                    message: 'Estimado(a) Usuario(a) el campo "Tipo de Imagen" es obligatorio',
                }
            }
        },
        emoji = {
            validators: {
                notEmpty: {
                    message: 'Estimado(a) Usuario(a) el campo "Emoji" es obligatorio',
                }
            }
        },
        image = {
            validators: {
                notEmpty: {
                    message: 'Estimado(a) Usuario(a) el campo "Imagen Libre" es obligatorio',
                }
            }
        },
        simbo = {
            validators: {
                notEmpty: {
                    message: 'Estimado(a) Usuario(a) el campo "Simbolo" es obligatorio',
                }
            }
        },
        texto = {
            validators: {
                notEmpty: {
                    message: 'Estimado(a) Usuario(a) el campo "Texto a bordar" es obligatorio',
                }
            }
        },
        bookIndex = <?php echo $a; ?>,
        contador = 0;
        
   </script>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                ndocu: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "RUT" es obligatorio',
                        }
                    }
                },
                nombr: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Nombre" es obligatorio',
                        }
                    }
                },
                talla: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Talla" es obligatorio',
                        }
                    }
                },
                npers: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Nombre Personalizado" es obligatorio',
                        }
                    }
                },
                /*aespa: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Apodo Espalda" es obligatorio',
                        }
                    }
                },*/
                /*elect: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Electivo" es obligatorio',
                        }
                    }
                },*/
                <?php
                $sql="SELECT * 
                      FROM pedido_detalle_listado_imagen
                      WHERE pedid_codig='".$p."'
                        AND pdlis_codig='".$lista['pdlis_codig']."' ";
                $imagenPersona=$conexion->createCommand($sql)->queryAll();
                foreach ($imagenPersona as $key => $value) {
                    ?>
                    'timag[<?php echo $value['pdlim_codig']?>]': timag,
                    'emoji[<?php echo $value['pdlim_codig']?>]': emoji,
                    'simbo[<?php echo $value['pdlim_codig']?>]': simbo,
                    'image[<?php echo $value['pdlim_codig']?>]': image,
                    'texto[<?php echo $value['pdlim_codig']?>]': texto,
                    <?php
                }
                ?>
                



            }
        });
    }).on('click', '.addButton', function() {
        //if(contador<4){ 
            contador++;
            //alert(contador);
            bookIndex++;
         
            document.getElementById('bookindex').value = bookIndex;
            var $template = $('#bookTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .attr('data-book-index', bookIndex)
                                .insertBefore($template);

            // Update the name attributes
            $clone
                .find('[name="timag_"]').attr('name', 'timag[' + bookIndex + ']').end()
                .find('[name="emoji_"]').attr('name', 'emoji[' + bookIndex + ']').end()
                .find('[name="simbo_"]').attr('name', 'simbo[' + bookIndex + ']').end()
                .find('[name="image_"]').attr('name', 'image[' + bookIndex + ']').end()

                .find('[name="texto_"]').attr('name', 'texto[' + bookIndex + ']').end()
                
                .find('[id="timag_"]').attr('id', 'timag_' + bookIndex ).end()
                .find('[id="opcional-1_"]').attr('id', 'opcional-1_' + bookIndex ).end()
                .find('[id="emoji_"]').attr('id', 'emoji_' + bookIndex ).end()
                .find('[id="simbo_"]').attr('id', 'simbo_' + bookIndex ).end()
                .find('[id="login_"]').attr('id', 'login_' + bookIndex ).end()
                .find('[id="login-2_"]').attr('id', 'login-2_' + bookIndex ).end()
                .find('[id="popover-img_"]').attr('id', 'popover-img_' + bookIndex ).end()
                .find('[id="popover-img-2_"]').attr('id', 'popover-img-2_' + bookIndex ).end()
                .find('[id="opcional-2_"]').attr('id', 'opcional-2_' + bookIndex ).end()
                .find('[id="opcional-3_"]').attr('id', 'opcional-3_' + bookIndex ).end()
                .find('[id="opcional-4_"]').attr('id', 'opcional-4_' + bookIndex ).end()
                .find('[id="image_"]').attr('id', 'image_' + bookIndex ).end()
                .find('[id="texto_"]').attr('id', 'texto_' + bookIndex ).end()
                .find('[class="numero1_"]').attr('class', 'numero1_' + bookIndex ).end();

            // Add new fields
            // Note that we also pass the validator rules for new field as the third parameter
            $('#login-form')
                .formValidation('addField', 'timag[' + bookIndex + ']', timag)
                .formValidation('addField', 'emoji[' + bookIndex + ']', emoji)
                .formValidation('addField', 'simbo[' + bookIndex + ']', simbo)
                .formValidation('addField', 'image[' + bookIndex + ']', image)
                .formValidation('addField', 'texto[' + bookIndex + ']', texto);
            

            //agregar ajax
            $('#login-form').append(
                '<script type="text/javascript">'+
                    '$("#timag_'+bookIndex+'").change(function () {'+
                        'var timag = $(this).val();'+
                        'if(timag==null || timag=="" || timag=="4"){'+
                            'jQuery("#opcional-3_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#opcional-4_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#opcional-2_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#opcional-1_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#image_'+bookIndex+'").attr("disabled","true");'+
                            'jQuery("#emoji_'+bookIndex+'").attr("disabled","true");'+
                            'jQuery("#simbo_'+bookIndex+'").attr("disabled","true");'+
                            'jQuery("#texto_'+bookIndex+'").attr("disabled","true");'+
                        '}else if(timag=="1"){'+

                            'jQuery("#opcional-4_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#opcional-1_'+bookIndex+'").removeClass("hide");'+
                            'jQuery("#opcional-2_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#opcional-3_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#emoji_'+bookIndex+'").removeAttr("disabled");'+
                            'jQuery("#image_'+bookIndex+'").attr("disabled","true");'+
                            'jQuery("#simbo_'+bookIndex+'").attr("disabled","true");'+
                            'jQuery("#texto_'+bookIndex+'").attr("disabled","true");'+
                        '}else if(timag=="3"){'+
                            'jQuery("#opcional-4_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#opcional-3_'+bookIndex+'").removeClass("hide");'+
                            'jQuery("#opcional-2_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#opcional-1_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#simbo_'+bookIndex+'").removeAttr("disabled");'+
                            'jQuery("#image_'+bookIndex+'").attr("disabled","true");'+
                            'jQuery("#emoji_'+bookIndex+'").attr("disabled","true");'+
                            'jQuery("#texto_'+bookIndex+'").attr("disabled","true");'+
                        '}else if(timag=="5"){'+
                            'jQuery("#opcional-4_'+bookIndex+'").removeClass("hide");'+
                            'jQuery("#opcional-2_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#opcional-1_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#opcional-3_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#texto_'+bookIndex+'").removeAttr("disabled");'+
                            'jQuery("#image_'+bookIndex+'").attr("disabled","true");'+
                            'jQuery("#emoji_'+bookIndex+'").attr("disabled","true");'+
                            'jQuery("#simbo_'+bookIndex+'").attr("disabled","true");'+
                        '}else {'+
                            'jQuery("#opcional-4_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#opcional-3_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#opcional-2_'+bookIndex+'").removeClass("hide");'+
                            'jQuery("#opcional-1_'+bookIndex+'").addClass("hide");'+
                            'jQuery("#image_'+bookIndex+'").removeAttr("disabled");'+
                            'jQuery("#emoji_'+bookIndex+'").attr("disabled","true");'+
                            'jQuery("#simbo_'+bookIndex+'").attr("disabled","true");'+
                            'jQuery("#texto_'+bookIndex+'").attr("disabled","true");'+

                        '}'+
                    '});'+
                '<\/script>'
            );

            $('#login-form').append(
                '<script type="text/javascript">\n'+
                    '$(document).on("click", "#close-preview", function () {\n'+
                        '$(".numero1_'+bookIndex+' .image-preview").popover("hide");\n'+
                     
                        '$(".numero1_'+bookIndex+' .image-preview").hover(\n'+
                            'function () {\n'+
                                '$(".image-preview").popover("hide");\n'+
                            '},\n'+
                            'function () {\n'+
                                '$(".image-preview").popover("hide");\n'+
                            '}\n'+
                        ');\n'+
                    '});\n'+
                    '$(function () {\n'+
                       
                        'var closebtn = $("<button/>", {\n'+
                            'type: "button",\n'+
                            'text: "x",\n'+
                            'id: "close-preview",\n'+
                            'style: "font-size: initial;",\n'+
                        '});\n'+
                        'closebtn.attr("class","close pull-right");\n'+
                        '$(".numero1_'+bookIndex+' .image-preview").popover({\n'+
                            'trigger:"manual",\n'+
                            'html:true,\n'+
                            'title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,\n'+
                            'content: "No hay imagen",\n'+
                            'placement:"top"\n'+
                        '});\n'+
                       
                        '$(".numero1_'+bookIndex+' .image-preview-clear").click(function () {\n'+
                            '$(".numero1_'+bookIndex+' .image-preview").attr("data-content", "").popover("hide");\n'+
                            '$(".numero1_'+bookIndex+' .image-preview-filename").val("");\n'+
                            '$(".numero1_'+bookIndex+' .image-preview-clear").hide();\n'+
                            '$(".numero1_'+bookIndex+' .image-preview-input input:file").val("");\n'+
                            '$(".numero1_'+bookIndex+' .image-preview-input-title").text("Buscar");\n'+
                        '});\n'+
                       
                        '$(".numero1_'+bookIndex+' .image-preview-input input:file").change(function () {\n'+
                            'var img = $("<img/>", {\n'+
                                'id: "dynamic",\n'+
                                'width: 250,\n'+
                                'height: 200\n'+
                            '});\n'+
                            'var file = this.files[0];\n'+
                            'var reader = new FileReader();\n'+
                          
                            'reader.onload = function (e) {\n'+
                                '$(".numero1_'+bookIndex+' .image-preview-input-title").text("Cambiar");\n'+
                                '$(".numero1_'+bookIndex+' .image-preview-clear").show();\n'+
                                '$(".numero1_'+bookIndex+' .image-preview-filename").val(file.name);\n'+
                                'img.attr("src", e.target.result);\n'+
                                '$(".numero1_'+bookIndex+' .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");\n'+
                                '}\n'+
                            'reader.readAsDataURL(file);\n'+
                        '});\n'+
                    '});\n'+
                '<\/script>');
            $('#login-form').append('<script type="text/javascript">'+
            '$(document).ready(function(){'+
                '$("#login_'+bookIndex+'").popover({'+
                      'html: true,'+
                      'content: function() {'+
                        'return $("#popover-img_'+bookIndex+'").html();'+
                      '},'+
                      'trigger: "hover"'+
                    '});'+
            '});'+
            '$(document).ready(function(){'+
                '$("#login-2_'+bookIndex+'").popover({'+
                      'html: true,'+
                      'content: function() {'+
                        'return $("#popover-img-2_'+bookIndex+'").html();'+
                      '},'+
                      'trigger: "hover"'+
                    '});'+
            '});'+
            '$("#emoji_'+bookIndex+'").change(function () {'+
                    'var emoji = $(this).val();'+
                    '$.ajax({  '+
                        'url:"<?php echo CController::createUrl("funciones/PedidosEmoji"); ?>",  '+
                        'method:"POST",  '+
                        'async:false,  '+
                        'data:{id:emoji},  '+
                        'success:function(data){  '+
                            'jQuery("#popover-img_'+bookIndex+'").html(data);  '+
                        '}  '+
                    '});'+
                    '$("#login_'+bookIndex+'").popover("hide");'+
                '});'+
            '$("#simbo_'+bookIndex+'").change(function () {'+
                    'var emoji = $(this).val();'+
                    '$.ajax({  '+
                        'url:"<?php echo CController::createUrl("funciones/PedidosSimbolo"); ?>",  '+
                        'method:"POST",  '+
                        'async:false,  '+
                        'data:{id:emoji},  '+
                        'success:function(data){  '+
                            'jQuery("#popover-img-2_'+bookIndex+'").html(data);  '+
                        '}  '+
                    '});'+
                    '$("#login-2_'+bookIndex+'").popover("hide");'+
                '});'+
            '<\/script>');
           

       
    }).on('click', '.removeButton', function() {// Remove button click handler
        //alert(contador);
        contador=contador-1;
        var $row  = $(this).parents('.form-group'),
            index = $row.attr('data-book-index');
        // Remove fields
        $('#login-form')
            .formValidation('removeField', $row.find('[name="timag[' + index + ']"]'))
            .formValidation('removeField', $row.find('[name="emoji[' + index + ']"]'))
            .formValidation('removeField', $row.find('[name="texto[' + index + ']"]'))
            .formValidation('removeField', $row.find('[name="image[' + index + ']"]'));
        // Remove element containing the fields
        $row.remove();
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        var data = new FormData(jQuery('form')[0]);
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                url: 'modificar',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel"></div>'
                        , css: {
                            border: '1px solid #fff'
                        }
                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                            window.open('../detalle/modificar?p=<?php echo $p; ?>&s=2&c=<?php echo $d; ?>', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>


<script type="text/javascript">
    $('#ideta').change(function () {
        var ideta = $(this).val();
        switch(ideta) {
            case '1':
                jQuery("#iclin").removeAttr('disabled');
                break;
            default:
                jQuery("#iclin").attr('disabled','true');
                break;
        }
                
            
    });
</script>

<script type="text/javascript">
    $('#ideta').change(function () {
        var ideta = $(this).val();
        switch(ideta) {
            case '1':
                jQuery("#iclin").removeAttr('disabled');
                break;
            default:
                jQuery("#iclin").attr('disabled','true');
                break;
        }
                
            
    });
</script>
