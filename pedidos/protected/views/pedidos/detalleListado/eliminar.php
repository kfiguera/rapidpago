<style>
    .image-preview-input {
        position: relative;
        overflow: hidden;
        margin: 0px;    
        color: #333;
        background-color: #fff;
        border-color: #ccc;    
    }
    .image-preview-input input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }
    .image-preview-input-title {
        margin-left:2px;
    }
</style> 
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Listado</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Pedidos</a></li>
            <li><a href="#">Detalles</a></li>            
            <li><a href="#">Listado</a></li>
            <li class="active">Eliminar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Eliminar Listado</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row hide">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Pedido *</label>
                            <?php 
                                echo CHtml::hiddenField('pedid', $p, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Detalle *</label>
                            <?php 
                                echo CHtml::hiddenField('detal', $d, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Código *</label>
                            <?php 
                                echo CHtml::hiddenField('codig', $c, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>RUT *</label>
                            <?php 
                                echo CHtml::textField('ndocu', $lista['pdlis_ndocu'], array('class' => 'form-control', 'placeholder' => "RUT",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Nombre *</label>
                            <?php 
                                echo CHtml::textField('nombr', $lista['pdlis_nombr'], array('class' => 'form-control', 'placeholder' => "Nombre",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                            <span class="help-block">
                                <small>Nota: No va Bordado</small>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Talla *</label>
                            <?php 
                                $sql="SELECT * FROM pedido_talla ORDER BY 1";
                                $result= $conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'talla_codig','talla_descr');
                                echo CHtml::dropDownList('talla', $lista['talla_codig'], $data, array('class' => 'form-control', 'placeholder' => "Talla",'prompt'=>'Seleccione...','disabled'=>'true')); ?>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Ajuste de Talla *</label>
                            <?php 
                                $sql="SELECT a.*, b.tajus_descr grupo 
                                      FROM pedido_talla_ajuste a 
                                      JOIN pedido_talla_ajuste b ON (a.tajus_padre = b.tajus_codig) 
                                      WHERE a.tajus_super='0' ";
                                $result= $conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'tajus_codig','tajus_descr','grupo');
                                echo CHtml::dropDownList('tajus', json_decode($lista['pdlis_tajus']), $data, array('class' => 'select2 select2-multiple', 'placeholder' => "Ajuste de Talla",'prompt'=>'Seleccione...','multiple'=>'multiple','disabled'=>'true')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Nombre o Apodo Personalizado *</label>
                            <?php 
                                echo CHtml::textField('npers', $lista['pdlis_npers'], array('class' => 'form-control', 'placeholder' => "Nombre Personalizado",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                            <span class="help-block">
                                <small>Nota: Nombre a bordar adelante</small>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Nombre o Apodo Espalda *</label>
                            <?php 
                                echo CHtml::textField('aespa', $lista['pdlis_aespa'], array('class' => 'form-control', 'placeholder' => "Apodo Espalda",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                        </div>
                    </div>
                    
                </div>    
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Electiva *</label>
                            <?php 
                                echo CHtml::textField('elect', $lista['pdlis_elect'], array('class' => 'form-control', 'placeholder' => "Electiva",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                            <span class="help-block">
                                <small>Ejemplo: Humanista, Científico</small>
                            </span>
                        </div>
                    </div>
                </div>
                <!-- OPCIONALES-->
                <div class="row">

                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Tipo de Imagen *</label>
                            <?php 
                                $data=array('1'=>'Emoji','2'=>'Imagen Libre','3'=>'Sin Imagen');
                                echo CHtml::dropDownList('timag', $lista['pdlis_timag'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                        </div>
                    </div>
                    <?php
                    switch ($lista['pdlis_timag']) {
                        case '1':
                            $opcional['1']['hide']='';
                            $opcional['1']['disabled']='';
                            $opcional['2']['hide']='hide';
                            $opcional['2']['disabled']='disabled="true"';
                            break;
                        case '2':
                            $opcional['2']['hide']='';
                            $opcional['2']['disabled']='';
                            $opcional['1']['hide']='hide';
                            $opcional['1']['disabled']='disabled="true"';
                            break;
                        
                        default:
                            $opcional['1']['hide']='hide';
                            $opcional['1']['disabled']='true';
                            $opcional['2']['hide']='hide';
                            $opcional['2']['disabled']='disabled="true"';
                            break;
                    }
                    ?>
                    <div class="<?php echo $opcional['1']['hide'] ?>" id="opcional-1">
                        <div class="col-sm-8">        
                            <div class="form-group">
                                <label>Emoji *</label>
                                <div class="input-group">
                                    <?php

                                    $sql="SELECT * FROM pedido_emoji ";
                                    $result=$conexion->createCommand($sql)->queryAll();
                                    $data=CHtml::listData($result,'emoji_codig','emoji_descr');
                                    echo CHtml::dropDownList('emoji', $lista['emoji_codig'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                    
                                        <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="login" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                    
                                </div>

                                <div class="hide" id="popover-img">
                                    <?php
                                    $sql="SELECT * FROM pedido_emoji where emoji_codig = '".$lista['emoji_codig']."'";
                                    $emoji=$conexion->createCommand($sql)->queryRow();
                                    ?><img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$emoji['emoji_ruta'] ?>" class="img-responsive"></div>
                            </div>
                        </div>
                    </div>
                    <div class="<?php echo $opcional['2']['hide'] ?>" id="opcional-2">
                        <div class="col-sm-8">        
                            <div class="numero1">
                                <label>Imagen Libre</label>
                                <div  class="input-group image-preview" data-placement="top" >  

                                    <img id="dynamic">
                                    <!-- image-preview-filename input [CUT FROM HERE]-->
                                    <?php 
                                        $ruta=$lista['pdlis_rimag'];
                                        $nombre=explode('/', $ruta);
                                        $nombre=end($nombre);

                                    ?>
                                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="true" value="<?php echo $nombre ?>" > <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-btn">
                                        <!-- image-preview-clear button -->
                                        <button disabled='true' type="button" class="btn btn-default image-preview-clear" >
                                            <span class="fa fa-times"></span> Limpiar
                                        </button>
                                        <!-- image-preview-input -->
                                        <div disabled='true' class="disabled btn btn-default image-preview-input">
                                            <span disabled='true' class="fa fa-folder-open"></span>
                                            <span disabled='true' class="numero1 image-preview-input-title">Buscar</span>
                                            <input disabled='true' type="file" id="image" name="image"/> <!-- rename it -->
                                        </div>
                                    </span>
                               
                                </div> 
                            </div> 
                            <div class="hide" id="popover-img-2">
                                <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$ruta ?>" class="img-responsive">
                            </div>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    var closebtn = $('<button/>', {
                                        type: "button",
                                        text: 'x',
                                        id: 'close-preview',
                                        style: 'font-size: initial;',
                                    });
                                    closebtn.attr("class","close pull-right");
                                    $('.numero1 .image-preview').popover({
                                      html: true,
                                      title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
                                      content: function() {
                                        return $('#popover-img-2').html();
                                      },
                                      trigger: 'manual'
                                    });

                                    $(".numero1 .image-preview").popover("show");
                                });
                            </script>
                        </div>
                        
                    </div>
                </div>
                <!-- FIN OPCIONALES-->
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observaciones</label>
                            <?php echo CHtml::textArea('obser', '', array('class' => 'form-control', 'placeholder' => "Observaciones",'disabled'=>'true')); ?>
                        </div>
                    </div>
                </div>
                <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-6 ">
                            <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                        </div>
                        <div class="col-sm-6 ">
                            <a href="../detalle/modificar?p=<?php echo $p; ?>&s=2&c=<?php echo $d; ?>" type="reset" class="btn-block btn btn-default">Volver </a>
                        </div>
                    </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                canti: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Total a Solicitar" es obligatorio',
                        }
                    }
                },
                mcant: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Cantidad de Modelos" es obligatorio',
                        }
                    }
                },
                color: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color del Cuerpo" es obligatorio',
                        }
                    }
                }



            }
        });
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        var data = new FormData(jQuery('form')[0]);
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                url: 'eliminar',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                            window.open('../detalle/modificar?p=<?php echo $p; ?>&s=2&c=<?php echo $d; ?>', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>

<script type="text/javascript">
    $('#timag').change(function () {
        var timag = $(this).val();
        switch(timag) {
            case '1':
                jQuery("#opcional-1").removeClass('hide');
                jQuery("#opcional-2").addClass('hide');
                jQuery("#emoji").removeAttr('disabled');
                jQuery("#image").attr('disabled','true');
                break;
            case '2':
                jQuery("#opcional-2").removeClass('hide');
                jQuery("#opcional-1").addClass('hide');
                jQuery("#image").removeAttr('disabled');
                jQuery("#emoji").attr('disabled','true');
                break;
            default:
                jQuery("#opcional-2").addClass('hide');
                jQuery("#opcional-1").addClass('hide');
                jQuery("#image").attr('disabled','true');
                jQuery("#emoji").attr('disabled','true');
                break;
        }         
    });
</script>
<script type="text/javascript">
    $('#ideta').change(function () {
        var ideta = $(this).val();
        switch(ideta) {
            case '1':
                jQuery("#iclin").removeAttr('disabled');
                break;
            default:
                jQuery("#iclin").attr('disabled','true');
                break;
        }
                
            
    });
</script>
<script>

$(document).on('click', '#close-preview', function () {
    $('.numero1 .image-preview').popover('hide');
    // Hover befor close the preview
    $('.numero1 .image-preview').hover(
        function () {
            $('.image-preview').popover('hide');
        },
        function () {
            $('.image-preview').popover('hide');
        }
    );
});
$(function () {
    // Create the close button
    var closebtn = $('<button/>', {
        type: "button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    $('.numero1 .image-preview').popover({
        trigger:'manual',
        html:true,
        title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
        content: "No hay imagen",
        placement:'top'
    });



    
    // Clear event
    $('.numero1 .image-preview-clear').click(function () {
        $('.numero1 .image-preview').attr("data-content", "").popover('hide');
        $('.numero1 .image-preview-filename').val("");
        $('.numero1 .image-preview-clear').hide();
        $('.numero1 .image-preview-input input:file').val("");
        $(".numero1 .image-preview-input-title").text("Buscar");
    });
    // Create the preview image
    $(".numero1 .image-preview-input input:file").change(function () {
        var img = $('<img/>', {
            id: 'dynamic',
            width: 250,
            height: 200
        });
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".numero1 .image-preview-input-title").text("Cambiar");
            $(".numero1 .image-preview-clear").show();
            $(".numero1 .image-preview-filename").val(file.name);
            img.attr('src', e.target.result);
            $(".numero1 .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
            }
        reader.readAsDataURL(file);
    });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('[data-toggle=popover]').popover({
          html: true,
          content: function() {
            return $('#popover-img').html();
          },
          trigger: 'hover'
        });
});
$('#emoji').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosEmoji'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>