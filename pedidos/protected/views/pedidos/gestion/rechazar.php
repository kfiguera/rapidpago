<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Pedidos</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Pedidos</a></li>
            <li class="active">Rechazar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Rechazar Pago</h3>
        </div>
        <div class="panel-body" >
           
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row hide">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Código *</label>
                            <?php 
                                
                                echo CHtml::textField('codig', $pagos['pagos_codig'], array('class' => 'form-control', 'placeholder' => "Código", 'prompt' => 'Seleccione...')); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Pedido *</label>
                            <?php 
                                $sql = "SELECT * FROM pedido_pedidos a 
                                        JOIN rd_preregistro_estatus b ON (a.epedi_codig = b.epedi_codig)";
                                $conexion = Yii::app()->db;
                                $result = $conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'pedid_codig','pedid_numer');
                                echo CHtml::dropDownList('pedid', $pagos['pedid_codig'], $data, array('class' => 'form-control', 'placeholder' => "Pedido", 'prompt' => 'Seleccione...','disabled'=>'true')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Forma de Pago *</label>
                            <?php 
                                $sql = "SELECT * FROM p_pago_tipo a ";
                                $conexion = Yii::app()->db;
                                $result = $conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'ptipo_codig','ptipo_descr');
                                echo CHtml::dropDownList('fpago', $pagos['ptipo_codig'], $data, array('class' => 'form-control', 'placeholder' => "Pedido", 'prompt' => 'Seleccione...','disabled'=>'true')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Referencia *</label>
                            <?php echo CHtml::textField('refer', $pagos['pagos_refer'], array('class' => 'form-control', 'placeholder' => "Referencia",'disabled'=>'true')); ?>
                        </div>
                    </div>

                                        
                </div>
                <div class="row">                 
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Monto *</label>
                            <?php echo CHtml::textField('monto', $this->funciones->transformarMonto_v($pagos['pagos_monto'],0), array('class' => 'form-control', 'placeholder' => "Monto",'disabled'=>'true')); ?>
                        </div>
                    </div>
                    <div class="col-sm-8">        
                            <div class="numero1 form-group">
                                <label>Imagen Libre</label>
                                <div  class="input-group image-preview" data-placement="top" >  

                                    <img id="dynamic">
                                    <!-- image-preview-filename input [CUT FROM HERE]-->
                                    <?php 
                                        $ruta=$pagos['pagos_rimag'];
                                        $nombre=explode('/', $ruta);
                                        $nombre=end($nombre);
                                        if($nombre){
                                            $opcional['1']['hide']='';
                                            $opcional['1']['show']='true';
                                        }else{
                                            $opcional['1']['hide']='hide';
                                            $opcional['1']['show']='false';
                                        }
                                    ?>
                                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="true" value="<?php echo $nombre ?>"> <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-btn">
                                        <!-- image-preview-clear button -->
                                        <button type="button" class="btn btn-default image-preview-clear" style="display: none;" >
                                            <span class="fa fa-time "></span> Limpiar
                                        </button>
                                        <!-- image-preview-input -->
                                        <div class="btn btn-default image-preview-input">
                                            <span class="fa fa-folder-open"></span>
                                            <span class="numero1 image-preview-input-title">Buscar</span>
                                            <input type="file" id="prima" name="prima"/> <!-- rename it -->
                                        </div>
                                    </span>
                               
                                </div> 
                            </div> 
                            <div class="hide" id="popover-numero-1">
                                <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$ruta ?>" class="img-responsive">
                            </div>
                            <?php
                                if($opcional['1']['show']=='true') { 
                            ?>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    var closebtn = $('<button/>', {
                                        type: "button",
                                        text: 'x',
                                        id: 'close-preview',
                                        style: 'font-size: initial;',
                                    });
                                    closebtn.attr("class","close pull-right");
                                    $('.numero1 .image-preview').popover({
                                      html: true,
                                      title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
                                      content: function() {
                                        return $('#popover-numero-1').html();
                                      },
                                      trigger: 'manual'
                                    });

                                    $(".numero1 .image-preview").popover("show");
                                });
                            </script>
                            <?php
                                }
                            ?>
                        </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observaciones</label>
                            <?php echo CHtml::textArea('obser', $pagos['pagos_obser'], array('class' => 'form-control', 'placeholder' => "Observaciones",'disabled'=> $disabled,'disabled'=>'true')); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Motivo</label>
                            <?php echo CHtml::textArea('motiv', '', array('class' => 'form-control', 'placeholder' => "Motivo",)); ?>
                        </div>
                    </div>
                </div>
                
                    <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-6 ">
                            <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                        </div>
                        <div class="col-sm-6 ">
                            <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                        </div>
                    </div>

            </form>

        </div><!-- form -->
    </div>  
</div>
<script>
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                canti: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Total a Solicitar" es obligatorio',
                        }
                    }
                },
                mcant: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Cantidad de Modelos" es obligatorio',
                        }
                    }
                },
                color: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color del Cuerpo" es obligatorio',
                        }
                    }
                }



            }
        });
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        var data = new FormData(jQuery('form')[0]);
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                url: 'rechazar',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>
<script>

    $(document).on('click', '#close-preview', function () {
        $('.numero1 .image-preview').popover('hide');
        // Hover befor close the preview
        $('.numero1 .image-preview').hover(
            function () {
                $('.image-preview').popover('hide');
            },
            function () {
                $('.image-preview').popover('hide');
            }
        );
    });
    $(function () {
        // Create the close button
        var closebtn = $('<button/>', {
            type: "button",
            text: 'x',
            id: 'close-preview',
            style: 'font-size: initial;',
        });
        closebtn.attr("class","close pull-right");
        $('.numero1 .image-preview').popover({
            trigger:'manual',
            html:true,
            title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
            content: "No hay imagen",
            placement:'top'
        });



        
        // Clear event
        $('.numero1 .image-preview-clear').click(function () {
            $('.numero1 .image-preview').attr("data-content", "").popover('hide');
            $('.numero1 .image-preview-filename').val("");
            $('.numero1 .image-preview-clear').hide();
            $('.numero1 .image-preview-input input:file').val("");
            $(".numero1 .image-preview-input-title").text("Buscar");
        });
        // Create the preview image
        $(".numero1 .image-preview-input input:file").change(function () {
            var img = $('<img/>', {
                id: 'dynamic',
                width: 250
            });
            var file = this.files[0];
            var reader = new FileReader();
            // Set preview image into the popover data-content
            reader.onload = function (e) {
                $(".numero1 .image-preview-input-title").text("Cambiar");
                $(".numero1 .image-preview-clear").show();
                $(".numero1 .image-preview-filename").val(file.name);
                img.attr('src', e.target.result);
                $(".numero1 .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
                }
            reader.readAsDataURL(file);
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#monto').mask('#.##0',{reverse: true,maxlength:false});
    });
</script>