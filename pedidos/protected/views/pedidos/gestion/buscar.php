<table id='auditoria' class="table table-bordered table-hover dataTable">
    <thead>
        <tr>
            <th width="2%">
                #
            </th>
            <th>
                Nro Pedido
            </th>
            <th>
                Total a Solicitar
            </th>
            <th>
                Total de Modelos
            </th>
            <th>
                Estatus
            </th>
              <th width="5%">
                Detalle
            </th>
             <th width="5%">
                Personas
            </th>
            <th width="5%">
                Consultar
            </th>
            <th width="5%">
                Modificar
            </th>
            <th width="5%">
                Eliminar
            </th>
        </tr>
    </thead>

    <tbody>
        <?php
        $sql = "SELECT * FROM pedido_pedidos a 
                JOIN rd_preregistro_estatus b ON (a.epedi_codig = b.epedi_codig)
                ".$condicion;
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $p_persona = $command->query();
        $i=0;
        while (($row = $p_persona->read()) !== false) {
            $i++;
        ?>
        <tr>
            <td class="tabla"><?php echo $i ?></td>
            <td class="tabla"><?php echo $row['pedid_numer'] ?></td>
            <td class="tabla"><?php echo $row['pedid_total'] ?></td>
            <td class="tabla"><?php echo $row['pedid_cmode'] ?></td>
            <td class="tabla"><?php echo $row['epedi_descr'] ?></td>

            <td class="tabla"><a href="detalle?c=<?php echo $row['pedid_codig'] ?>&mod=<?php echo $row['model_codig']?>&numero=<?php echo $row['pedid_nume']?>&color=<?php echo $row['pedid_cuerpo']?>" class="btn btn-block btn-info"><i class="glyphicon glyphicon-list-alt"></i></a></td>

            <td class="tabla"><a href="lista?c=<?php echo $row['pedid_codig'] ?>&mod=<?php echo $row['model_codig']?>&numero=<?php echo $row['pedid_nume']?>&categoria=<?php echo $row['categ_codig']?>" class="btn btn-block btn-info"><i class="glyphicon glyphicon-user"></i></a></td>

            <td class="tabla"><a href="consultar?c=<?php echo $row['pedid_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-search"></i></a></td>
            <td class="tabla"><a href="modificar?c=<?php echo $row['pedid_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-pencil"></i></a></td>
            <td class="tabla"><a href="eliminar?c=<?php echo $row['pedid_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-close"></i></a></td>
        </tr>
        <?php
            }   
        ?>
    </tbody>
</table>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable(); 
    });
</script>
