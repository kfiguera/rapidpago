<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Gestión de Pedidos</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Pedidos</a></li>
            <li><a href="#">Gestión</a></li>
            <li class="active">Listado</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Criterio de Busqueda</h3>
    </div>
    <div class="panel-body" >
        <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'pedidos', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            $connection = Yii::app()->db;

        ?>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Número de Pedido</label>
                    <?php echo CHtml::textField('numero', '', array('class' => 'form-control', 'placeholder' => "Nro del Pedido")); ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Estatus</label>
                    <?php 
                        
                        $sql="SELECT * FROM rd_preregistro_estatus";
                        $result=$connection->createCommand($sql)->queryAll();
                        $data=CHtml::listData($result,'epedi_codig','epedi_descr');
                        echo CHtml::dropDownList('epedi', '', $data,array('class' => 'form-control', 'placeholder' => "Descripción", 'prompt'=>'Seleccione...')); ?>
                </div>
            </div>
           
        </div>
        
        <div class="row">
            <div class="col-sm-6">
                <button type="button" id="buscar" class="btn btn-block btn-info">Buscar</button>
            </div>
            <div class="col-sm-6">
                <button type="reset" class="btn btn-block btn-default">Limpiar</button>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div> 
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Listado</h3>
    </div>
    <div class="panel-body" >
        
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-pedido'>
                <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                #
                            </th>
                            <th>
                                Nro Pago
                            </th>
                            <th>
                                Fecha de Entrega
                            </th>
                            <th>
                                Estatus
                            </th>
                            <th width="5%">
                                Asignar Fecha de Entrega
                            </th>
                            <th width="5%">
                                Actualizar Estatus
                            </th>
                            <th width="5%">
                                Corte
                            </th>
                            <th width="5%">
                                Diseño
                            </th>
                            <th width="5%">
                                Bordados
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        
                        $sql = "SELECT * FROM pedido_pedidos a 
                                JOIN rd_preregistro_estatus b ON (a.epedi_codig = b.epedi_codig)
                                WHERE a.epedi_codig not IN('1','2')
                                ";
                   
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                        ?>
                        <tr>
                            <td class="tabla"><?php echo $i ?></td>
                            <td class="tabla"><?php echo $row['pedid_numer'] ?></td>
                            <td class="tabla"><?php echo $row['pedid_fentr'] ?></td>
                            <td class="tabla"><?php echo $row['epedi_descr'] ?></td>
                            
                            <?php
                            if($row['epedi_codig']==7){
                                ?>
                                <td class="tabla"><a href="fechaEntrega?c=<?php echo $row['pedid_codig'] ?>"  class="btn btn-block btn-info"><i class="fa fa-calendar"></i></a></td>
                                <td class="tabla"><a href="#"  class="btn btn-block btn-info disabled" disabled><i class="fa fa-edit"></i></a></td>
                                
                                
                                <td class="tabla"><a href="#" class="btn btn-block btn-success disabled"><i class="fa fa-cut"></i></a></td>
                                <td class="tabla"><a href="#"  class="btn btn-block btn-danger disabled"><i class="fa fa-paint-brush"></i></a></td>
                                <td class="tabla"><a href="../../reportes/pedidos/bordados?c=<?php echo $row['pedid_codig'] ?>" target="_blank" class="btn btn-block btn-info"><i class="fa fa-file-pdf-o"></i></a></td>
                                <?php
                            }else{
                                ?>
                                <td class="tabla"><a href="#" target="_blank" class="btn btn-block btn-info disabled" disabled><i class="fa fa-calendar" ></i></a></td>
                                <td class="tabla"><a href="modificar?c=<?php echo $row['pedid_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-edit"></i></a></td>
                                <td class="tabla"><a href="corte?c=<?php echo $row['pedid_codig'] ?>" class="btn btn-block btn-success"><i class="fa fa-cut"></i></a></td>
                                <td class="tabla"><a href="diseno?c=<?php echo $row['pedid_codig'] ?>" class="btn btn-block btn-danger"><i class="fa fa-paint-brush"></i></a></td>
                                <td class="tabla"><a href="../../reportes/pedidos/bordados?c=<?php echo $row['pedid_codig'] ?>" target="_blank" class="btn btn-block btn-info"><i class="fa fa-file-pdf-o"></i></a></td>
                                <?php
                            }
                            ?>
                            
                        </tr>
                        <?php
                            }   
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
</div>  
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#buscar').click(function () {
        var formData = new FormData($("#pedidos")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-pedido').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>
