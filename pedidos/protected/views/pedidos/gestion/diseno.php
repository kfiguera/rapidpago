<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Gestión de Pedidos</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Pedidos</a></li>
            <li><a href="#">Gestión</a></li>
            <li><a href="#">Diseño</a></li>
            <li class="active">Listado</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Datos del Pedido</h3>
    </div>
    <div class="panel-body" >
        <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'pedidos', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            $connection = Yii::app()->db;

        ?>
        <div class="row">
            <div class="col-sm-12 hide">        
                <div class="form-group">
                    <label>Código *</label>
                    <?php echo CHtml::hiddenField('codig', $pedidos['pedid_codig'], array('class' => 'form-control', 'placeholder' => "Total a Solicitar",'disabled'=>'true')); ?>
                </div>
            </div>
            
            
        </div>
        <div class="row">
            <div class="col-sm-4">        
                <div class="form-group">
                    <label>Número de Pedido *</label>
                    <?php echo CHtml::textField('numer', $pedidos['pedid_numer'], array('class' => 'form-control', 'placeholder' => "Total a Solicitar",'disabled'=>'true')); ?>
                </div>
            </div>
            <div class="col-sm-4">        
                <div class="form-group">
                    <label>Fecha de Entrega *</label>
                    <?php echo CHtml::textField('fentr', $pedidos['pedid_fentr'], array('class' => 'form-control', 'placeholder' => "Fecha de Entrega",'disabled'=>'true')); ?>
                </div>
            </div>
            <div class="col-sm-4">        
                <div class="form-group">
                    <label>Estatus *</label>
                    <?php $conexion = Yii::app()->db;
                        $sql="SELECT *
                              FROM rd_preregistro_estatus
                              WHERE epedi_codig = '".$pedidos['epedi_codig']."'";
                        $result=$conexion->createCommand($sql)->queryAll();
                        $data=CHtml::listData($result,'epedi_codig','epedi_descr');
                        echo CHtml::dropDownList('epedi', $pedidos['epedi_codig'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true'));?>
                </div>
            </div>

             <div class="col-sm-4 hide">        
                <div class="form-group">
                    <label>Color del Cuerpo *</label>
                    <?php 
                        $conexion = Yii::app()->db;
                        $sql="SELECT mcolo_codig, concat( mcolo_numer,' - ',mcolo_descr) mcolo_descr
                              FROM pedido_modelo_color 
                              WHERE mcolo_codig = '".$pedidos['mcolo_codig']."'";
                        $result=$conexion->createCommand($sql)->queryAll();
                        $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                        echo CHtml::dropDownList('color', $pedidos['mcolo_codig'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                </div>
            </div>
            
        </div>
        
        
        <div class="row">
                       
            <div class="col-sm-6">
                <a href="listado" class="btn btn-block btn-default" >Volver</a>
            </div>
            <div class="col-sm-6 hide">
                <a href="../../reportes/pedidos/corte?c=<?php echo $pedidos['pedid_codig'] ?>" target="_blank"  class="btn btn-block btn-info" >Imprimir</a>
            </div>
            
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div>  
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Imagenes del Pedido</h3>
    </div>
    <div class="panel-body" >
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-pedido'>
                <table class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                #
                            </th>
                            <th>
                                Descripción
                            </th>
                            <th>
                                Diseñador
                            </th>
                            <th>
                                Fecha de Asignación
                            </th>
                            <th>
                                Fecha de Entrega
                            </th>
                            <th>
                                Fecha de Recepción
                            </th>
                            <th>
                                Estatus
                            </th>
                            <th width="5%">
                                Vista previa
                            </th>
                            <th width="5%">
                                Descargar
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $sql="SELECT * FROM pedido_pedidos_imagen a
                                  WHERE pedid_codig='".$pedidos['pedid_codig']."'
                                    AND ptima_codig=5";
                            $pimagenes=$connection->createCommand($sql)->queryAll();
                            $a=1;
                            foreach ($pimagenes as $key => $imagen) {
                                $sql="SELECT * FROM pedido_diseno a
                                  JOIN p_trabajador b ON (a.traba_codig = b.traba_codig)
                                  JOIN p_persona c ON (b.perso_codig = c.perso_codig)
                                  JOIN pedido_corte_estatu d ON (a.ecort_codig = d.ecort_codig)
                                  WHERE pedid_codig='".$pedidos['pedid_codig']."'
                                    AND a.disen_orden='".$imagen['pimag_orden']."'";
                                $pdisen=$connection->createCommand($sql)->queryRow();
                                ?>
                                <tr>
                                <td><?php echo $a; ?></td>
                                <td><?php echo $imagen['pimag_descr']; ?></td>
                                <td class="tabla"><?php echo $pdisen['perso_pnomb'].' '.$pdisen['perso_papel'] ?></td>
                                <td class="tabla"><?php echo $pdisen['disen_fcrea'] ?></td>
                                <td class="tabla"><?php echo $pdisen['disen_fentr'] ?></td>
                                <td class="tabla"><?php echo $pdisen['disen_freci'] ?></td>
                                <td class="tabla"><?php echo $pdisen['ecort_descr'] ?></td>
                                <td><button class="btn btn-block btn-info" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img_<?php echo $a; ?>" data-original-title="" title="Vista previa"><i class="fa fa-search"></i></button>
                                <div class="hide" id="popover-img_<?php echo $a; ?>">
                                    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$imagen['pimag_ruta'] ?>" class="img-responsive">
                                    
                                </div>
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                        $('#img_<?php echo $a; ?>').popover({
                                              html: true,
                                              content: function() {
                                                return $('#popover-img_<?php echo $a; ?>').html();
                                              },
                                              trigger: 'hover'
                                            });
                                    });
                                </script>
                                </td>
                                <td class="tabla"><a href="../../funciones/descargar?a=<?php echo $imagen['pimag_ruta'] ?>" target="_blank" class="btn btn-block btn-info"><i class="fa fa-download "></i></a></td>
                                </tr>
                               <?php 
                               $a++;
                            }
                            
                        ?>

                        <?php
                            for($a=1;$a<=6;$a++){

                                $sql="SELECT * FROM pedido_diseno a
                                  JOIN p_trabajador b ON (a.traba_codig = b.traba_codig)
                                  JOIN p_persona c ON (b.perso_codig = c.perso_codig)
                                  JOIN pedido_corte_estatu d ON (a.ecort_codig = d.ecort_codig)
                                  WHERE a.pedid_codig='".$pedidos['pedid_codig']."'
                                    AND a.disen_orden='".$a."'";
                                $pdisen[$a]=$connection->createCommand($sql)->queryRow();
                            }
                            
                        ?>
                                                
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Imagenes por Persona</h3>
    </div>
    <div class="panel-body" >
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-pedido'>
                <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                #
                            </th>
                            <th>
                                Nombre
                            </th>
                            <th>
                                Tipo de Imagen
                            </th>
                            <th>
                                Diseñador
                            </th>
                            <th>
                                Fecha de Asignación
                            </th>
                            <th>
                                Fecha de Entrega
                            </th>
                            <th>
                                Fecha de Recepión
                            </th>
                            <th>
                                Estatus
                            </th>
                            <th width="5%">
                                Vista previa
                            </th>
                            <th width="5%">
                                Descargar
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql="SELECT * 
                            FROM pedido_detalle_listado_imagen a 
                            JOIN pedido_pedidos_detalle_listado b ON (a.pdlis_codig = b.pdlis_codig)
                            JOIN pedido_precio c ON (a.pdlim_timag = c.pprec_codig)
                            LEFT JOIN pedido_emoji d ON (a.emoji_codig = d.emoji_codig)
                            LEFT JOIN pedido_simbolo_simple e ON (a.ssimp_codig = e.ssimp_codig)
                            WHERE a.pedid_codig = '".$pedidos['pedid_codig']."'
                              AND pdlim_timag <> '4'";
                        
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        $j=100;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                            $j++;
                            
                            if($row['pdlim_timag']=='1'){
                                $ruta=$row['emoji_ruta'];
                            }else if($row['pdlim_timag']=='3'){
                                $ruta=$row['ssimp_ruta'];
                            }else{
                                $ruta=$row['pdlim_rimag'];
                            }
                            $sql="SELECT * FROM pedido_diseno a
                                  JOIN p_trabajador b ON (a.traba_codig = b.traba_codig)
                                  JOIN p_persona c ON (b.perso_codig = c.perso_codig)
                                  JOIN pedido_corte_estatu d ON (a.ecort_codig = d.ecort_codig)
                                  WHERE a.pdlim_codig='".$row['pdlim_codig']."'";
                            $disen=$connection->createCommand($sql)->queryRow();                            
                        ?>
                        <tr>
                            <td class="tabla"><?php echo $i ?></td>
                            <td class="tabla"><?php echo $row['pdlis_nombr'] ?></td>
                            <td class="tabla"><?php echo $row['pprec_descr'] ?></td>
                            <td class="tabla"><?php echo $disen['perso_pnomb'].' '.$disen['perso_papel'] ?></td>
                            <td class="tabla"><?php echo $disen['disen_fcrea'] ?></td>
                            <td class="tabla"><?php echo $disen['disen_fentr'] ?></td>
                            <td class="tabla"><?php echo $disen['disen_freci'] ?></td>
                            <td class="tabla"><?php echo $disen['ecort_descr'] ?></td>
                            <td><button class="btn btn-block btn-info" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img_<?php echo $j ?>" data-original-title="" title="Vista previa"><i class="fa fa-search"></i></button>
                            <div class="hide" id="popover-img_<?php echo $j ?>">
                                <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$ruta ?>" class="img-responsive">
                                
                            </div>
                            <script type="text/javascript">
                                $(document).ready(function(){
                                    $('#img_<?php echo $j ?>').popover({
                                          html: true,
                                          content: function() {
                                            return $('#popover-img_<?php echo $j ?>').html();
                                          },
                                          trigger: 'hover'
                                        });
                                });
                            </script>
                            </td>
                            <td class="tabla"><a href="../../funciones/descargar?a=<?php echo $ruta ?>" target="_blank" class="btn btn-block btn-info"><i class="fa fa-download "></i></a></td>
                        </tr>
                        <?php
                        }
                        ?>

                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <div class="row">
                
                <?php
                if($pedidos['epedi_codig']=='3'){
                ?>
                <div class="col-sm-6">
                    <a href="DisenoAsignar?c=<?php echo $pedidos['pedid_codig'] ?>" class="btn btn-block btn-info" >Asignar Diseñador</a>
                </div>
                <div class="col-sm-6">
                    <a href="DisenoEntrega?c=<?php echo $pedidos['pedid_codig'] ?>" class="btn btn-block btn-info" >Entrega</a>
                </div>
                <?php  
                }else{
                ?>
                <div class="col-sm-6">
                    <a href="#" class="btn btn-block btn-info disabled" >Asignar Diseñador</a>
                </div>
                <div class="col-sm-6">
                    <a href="#" class="btn btn-block btn-info disabled" >Entrega</a>
                </div>
                <?php 
                }
            ?>
            </div>
    </div>
</div>  
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#buscar').click(function () {
        var formData = new FormData($("#pedidos")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-pedido').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>
