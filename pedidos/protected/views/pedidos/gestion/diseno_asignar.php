<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Gestión de Pedidos</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Pedidos</a></li>
            <li>Gestión</a></li>
            <li>Corte</a></li>
            <li class="active">Asignar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

<form id='login-form' name='login-form' method="post">
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Imagenes del Pedido</h3>
    </div>
    <div class="panel-body" >
        <div class="row hide">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Código</label>
                    <?php 
                        
                        echo CHtml::textField('pedid', $pedidos['pedid_codig'],array('class' => 'form-control', 'prompt'=>'Seleccione...')); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Diseñador</label>
                    <?php 
                        $connection = Yii::app()->db;
                        $sql="SELECT *, concat(b.perso_pnomb,' ',b.perso_papel) nombre FROM p_trabajador a
                              JOIN p_persona b ON (a.perso_codig = b.perso_codig)";
                        $result=$connection->createCommand($sql)->queryAll();
                        $data=CHtml::listData($result,'traba_codig','nombre');
                        echo CHtml::dropDownList('traba', '', $data,array('class' => 'form-control', 'prompt'=>'Seleccione...')); ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Fecha de Entrega</label>
                    <?php echo CHtml::textField('fentr', '', array('class' => 'form-control', 'placeholder' => "Fecha de Entrega")); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-pedido'>
                <table id='pedidos' class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                <input type="checkbox" name="select_all" value="1" id="example-select-all-2">
                            </th>
                            <th>
                                Descripción
                            </th>
                            <th>
                                Diseñador
                            </th>
                            <th>
                                Fecha de Entrega
                            </th>
                            <th>
                                Estatus
                            </th>
                            <th width="5%">
                                Vista previa
                            </th>
                            <th width="5%">
                                Descargar
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $sql="SELECT * FROM pedido_pedidos_imagen a
                                  WHERE pedid_codig='".$pedidos['pedid_codig']."'
                                    AND ptima_codig=5
                                    AND a.pimag_orden NOT IN (SELECT e.disen_orden FROM pedido_diseno e WHERE a.pedid_codig=e.pedid_codig)";
                            $pimagenes=$connection->createCommand($sql)->queryAll();
                            $a=1;
                            foreach ($pimagenes as $key => $imagen) {
                                $sql="SELECT * FROM pedido_diseno a
                                  JOIN p_trabajador b ON (a.traba_codig = b.traba_codig)
                                  JOIN p_persona c ON (b.perso_codig = c.perso_codig)
                                  JOIN pedido_corte_estatu d ON (a.ecort_codig = d.ecort_codig)
                                  WHERE pedid_codig='".$pedidos['pedid_codig']."'
                                    AND a.disen_orden='".$imagen['pimag_orden']."'";
                                $pdisen=$connection->createCommand($sql)->queryRow();
                                ?>
                                <tr>
                                <td class="tabla"><input type="checkbox" name="pid[<?php echo $imagen['pimag_orden']; ?>]" value="<?php echo $imagen['pimag_orden']; ?>" id="pid_<?php echo $imagen['pimag_orden']; ?>"></td>
                                <td><?php echo $imagen['pimag_descr']; ?></td>
                                <td class="tabla"><?php echo $pdisen['perso_pnomb'].' '.$pdisen['perso_papel'] ?></td>
                                <td class="tabla"><?php echo $pdisen['disen_fentr'] ?></td>
                                <td class="tabla"><?php echo $pdisen['ecort_descr'] ?></td>
                                <td><button class="btn btn-block btn-info" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img_<?php echo $a; ?>" data-original-title="" title="Vista previa"><i class="fa fa-search"></i></button>
                                <div class="hide" id="popover-img_<?php echo $a; ?>">
                                    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$imagen['pimag_ruta'] ?>" class="img-responsive">
                                    
                                </div>
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                        $('#img_<?php echo $a; ?>').popover({
                                              html: true,
                                              content: function() {
                                                return $('#popover-img_<?php echo $a; ?>').html();
                                              },
                                              trigger: 'hover'
                                            });
                                    });
                                </script>
                                </td>
                                <td class="tabla"><a href="../../funciones/descargar?a=<?php echo $imagen['pimag_ruta'] ?>" target="_blank" class="btn btn-block btn-info"><i class="fa fa-download "></i></a></td>
                                </tr>
                               <?php 
                               $a++;
                            }
                        ?>
                        <?php
                            for($a=1;$a<=6;$a++){

                                $sql="SELECT * FROM pedido_diseno a
                                  JOIN p_trabajador b ON (a.traba_codig = b.traba_codig)
                                  JOIN p_persona c ON (b.perso_codig = c.perso_codig)
                                  JOIN pedido_corte_estatu d ON (a.ecort_codig = d.ecort_codig)
                                  WHERE a.pedid_codig='".$pedidos['pedid_codig']."'
                                    AND a.disen_orden='".$a."'";
                                $pdisen[$a]=$connection->createCommand($sql)->queryRow();
                            }
                        ?>
                                          
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Asignación</h3>
    </div>
    <div class="panel-body" >
        
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-p_persona'>
                <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                <input type="checkbox" name="select_all" value="1" id="example-select-all">
                            </th>
                            <th>
                                Nombre
                            </th>
                            <th>
                                Tipo de Imagen
                            </th>
                            <th>
                                Diseñador
                            </th>
                            <th>
                                Fecha de Entrega
                            </th>
                            <th>
                                Estatus
                            </th>
                            <th width="5%">
                                Vista previa
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql="SELECT * 
                            FROM pedido_detalle_listado_imagen a 
                            JOIN pedido_pedidos_detalle_listado b ON (a.pdlis_codig = b.pdlis_codig)
                            JOIN pedido_precio c ON (a.pdlim_timag = c.pprec_codig)
                            LEFT JOIN pedido_emoji d ON (a.emoji_codig = d.emoji_codig)
                            LEFT JOIN pedido_simbolo_simple e ON (a.ssimp_codig = e.ssimp_codig)

                            WHERE a.pedid_codig = '".$pedidos['pedid_codig']."'
                              AND pdlim_timag <> '4'
                              AND a.pdlim_codig NOT IN (SELECT e.pdlim_codig FROM pedido_diseno e)";

                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        
                        $i=0;
                        $j=100;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                            $j++;
                            
                            if($row['pdlim_timag']=='1'){
                                $ruta=$row['emoji_ruta'];
                            }else if($row['pdlim_timag']=='3'){
                                $ruta=$row['ssimp_ruta'];
                            }else{
                                $ruta=$row['pdlim_rimag'];
                            }
                            
                        ?>
                        <tr>
                            <td class="tabla"><input type="checkbox" name="id[<?php echo $i ?>]" value="<?php echo $row['pdlim_codig'] ?>" id="id_<?php echo $i ?>"></td>
                            <td class="tabla"><?php echo $row['pdlis_nombr'] ?></td>
                            <td class="tabla"><?php echo $row['pprec_descr'] ?></td>
                            <td class="tabla"></td>
                            <td class="tabla"></td>
                            <td class="tabla"></td>
                            <td><button class="btn btn-block btn-info" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img_<?php echo $j ?>" data-original-title="" title="Vista previa"><i class="fa fa-search"></i></button>
                            <div class="hide" id="popover-img_<?php echo $j ?>">
                                <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$ruta ?>" class="img-responsive">
                                
                            </div>
                            <script type="text/javascript">
                                $(document).ready(function(){
                                    $('#img_<?php echo $j ?>').popover({
                                          html: true,
                                          content: function() {
                                            return $('#popover-img_<?php echo $j ?>').html();
                                          },
                                          trigger: 'hover'
                                        });
                                });
                            </script>
                            </td>
                        </tr>
                        <?php
                            }   
                        ?>
                    </tbody>
                </table>
            </div>
            
        </div>
        <div class="row">
                <div class="col-sm-4 ">
                    <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                </div>
                <div class="col-sm-4 ">
                    <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                </div>
                <div class="col-sm-4 ">
                    <a href="diseno?c=<?php echo $c; ?>" type="reset" class="btn-block btn btn-default">Volver </a>
                </div>
            </div>
    </div>
</div>
</form>  
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function (){
   var table = $('#pedidos').DataTable({
      
      'columnDefs': [{
         'targets': 0,
         'searchable': false,
         'orderable': false,
         'className': 'dt-body-center',
         
      }],
      'order': [[1, 'asc']]
   });

   // Handle click on "Select all" control
   $('#example-select-all-2').on('click', function(){
      // Get all rows with search applied
      var rows = table.rows({ 'search': 'applied' }).nodes();
      // Check/uncheck checkboxes for all rows in the table
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
   });

   // Handle click on checkbox to set state of "Select all" control
   $('#pedidos tbody').on('change', 'input[type="checkbox"]', function(){
      // If checkbox is not checked
      if(!this.checked){
         var el = $('#example-select-all-2').get(0);
         // If "Select all" control is checked and has 'indeterminate' property
         if(el && el.checked && ('indeterminate' in el)){
            // Set visual state of "Select all" control
            // as 'indeterminate'
            el.indeterminate = true;
         }
      }
   });

   // Handle form submission event
   $('#frm-example').on('submit', function(e){
      var form = this;

      // Iterate over all checkboxes in the table
      table.$('input[type="checkbox"]').each(function(){
         // If checkbox doesn't exist in DOM
         if(!$.contains(document, this)){
            // If checkbox is checked
            if(this.checked){
               // Create a hidden element
               $(form).append(
                  $('<input>')
                     .attr('type', 'hidden')
                     .attr('name', this.name)
                     .val(this.value)
               );
            }
         }
      });
   });

});
</script>
<script type="text/javascript">
    $(document).ready(function (){
   var table = $('#auditoria').DataTable({
      
      'columnDefs': [{
         'targets': 0,
         'searchable': false,
         'orderable': false,
         'className': 'dt-body-center',
         
      }],
      'order': [[1, 'asc']]
   });

   // Handle click on "Select all" control
   $('#example-select-all').on('click', function(){
      // Get all rows with search applied
      var rows = table.rows({ 'search': 'applied' }).nodes();
      // Check/uncheck checkboxes for all rows in the table
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
   });

   // Handle click on checkbox to set state of "Select all" control
   $('#auditoria tbody').on('change', 'input[type="checkbox"]', function(){
      // If checkbox is not checked
      if(!this.checked){
         var el = $('#example-select-all').get(0);
         // If "Select all" control is checked and has 'indeterminate' property
         if(el && el.checked && ('indeterminate' in el)){
            // Set visual state of "Select all" control
            // as 'indeterminate'
            el.indeterminate = true;
         }
      }
   });

   // Handle form submission event
   $('#frm-example').on('submit', function(e){
      var form = this;

      // Iterate over all checkboxes in the table
      table.$('input[type="checkbox"]').each(function(){
         // If checkbox doesn't exist in DOM
         if(!$.contains(document, this)){
            // If checkbox is checked
            if(this.checked){
               // Create a hidden element
               $(form).append(
                  $('<input>')
                     .attr('type', 'hidden')
                     .attr('name', this.name)
                     .val(this.value)
               );
            }
         }
      });
   });

});
</script>
<script>
    $(document).ready(function () {
       
        $('#buscar').click(function () {
        var formData = new FormData($("#p_personas")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-p_persona').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>
<script>
    $(document).ready(function () {
        $('#cantidad').mask('#.##0',{reverse: true,maxlength:false});
        $('#punidad').mask('#.##0,00',{reverse: true,maxlength:false});
    });
</script>

<script>
// Date Picker
    $(document).ready(function () {
        $('#fentr').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd/mm/yyyy'
        });
        $('#hasta').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd/mm/yyyy'
        });
    }); 
</script>

<script type="text/javascript">
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'disenoAsignar',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('diseno?c=<?php echo $c; ?>', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>