<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Gestión de Pedidos</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Pedidos</a></li>
            <li>Gestión</a></li>
            <li>Corte</a></li>
            <li class="active">Entrega</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

<form id='login-form' name='login-form' method="post">

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Entrega</h3>
    </div>
    <div class="panel-body" >
        
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-p_persona'>
                <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                <input type="checkbox" name="select_all" value="1" id="example-select-all">
                            </th>
                            <th>
                                Nro pedido
                            </th>
                            <th>
                                Modelo
                            </th>
                            <th>
                                Nombre
                            </th>
                            <th>
                                Talla
                            </th>

                            <th>
                                Ajuste de Talla
                            </th>
                            <th>
                                Observaciones
                            </th>
                            <th>
                                Trabajador
                            </th>
                            <th>
                                Fecha de Entrega
                            </th>
                            <th>
                                Estatus
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "SELECT * FROM pedido_pedidos a 
                                JOIN pedido_pedidos_detalle b ON (a.pedid_codig = b.pedid_codig)
                                JOIN pedido_pedidos_detalle_listado c ON (a.pedid_codig = c.pedid_codig)
                                JOIN pedido_talla d ON (c.talla_codig = d.talla_codig)
                                JOIN pedido_modelo e ON (b.model_codig = e.model_codig)
                                JOIN pedido_modelo_tipo f ON (b.tmode_codig = f.tmode_codig)
                                JOIN pedido_corte g ON (c.pdlis_codig = g.pdlis_codig)
                                WHERE a.pedid_codig = '".$pedidos['pedid_codig']."'
                                  AND g.ecort_codig = '1';
                                ";
                        $connection = Yii::app()->db;
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                            $sql="SELECT * FROM pedido_corte a
                                  JOIN p_trabajador b ON (a.traba_codig = b.traba_codig)
                                  JOIN p_persona c ON (b.perso_codig = c.perso_codig)
                                  JOIN pedido_corte_estatu d ON (a.ecort_codig = d.ecort_codig)
                                  WHERE a.pdlis_codig='".$row['pdlis_codig']."'";
                            $corte=$connection->createCommand($sql)->queryRow();
                        ?>
                        <tr>
                            <td class="tabla"><input type="checkbox" name="id[<?php echo $i ?>]" value="<?php echo $row['pdlis_codig'] ?>" id="id_<?php echo $i ?>"></td>
                            <td class="tabla"><?php echo $row['pedid_numer'] ?></td>
                            <td class="tabla"><?php echo $row['tmode_descr'].' '.$row['model_descr'] ?></td>
                            <td class="tabla"><?php echo $row['pdlis_nombr'] ?></td>
                            <td class="tabla"><?php echo $row['talla_descr'] ?></td>
                            <td class="tabla"><?php echo $row['pdlis_ajuste'] ?></td>
                            <td class="tabla"><?php echo $row['pdlis_obser'] ?></td>
                            <td class="tabla"><?php echo $corte['perso_pnomb'].' '.$corte['perso_papel'] ?></td>
                            <td class="tabla"><?php echo $corte['corte_fentr'] ?></td>
                            <td class="tabla"><?php echo $corte['ecort_descr'] ?></td>
                        </tr>
                        <?php
                            }   
                        ?>
                    </tbody>
                </table>
            </div>
            
        </div>
        <div class="row">
                <div class="col-sm-4 ">
                    <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                </div>
                <div class="col-sm-4 ">
                    <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                </div>
                <div class="col-sm-4 ">
                    <a href="corte?c=<?php echo $c; ?>" type="reset" class="btn-block btn btn-default">Volver </a>
                </div>
            </div>
    </div>
</div>
</form>  
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function (){
   var table = $('#auditoria').DataTable({
      
      'columnDefs': [{
         'targets': 0,
         'searchable': false,
         'orderable': false,
         'className': 'dt-body-center',
         
      }],
      'order': [[1, 'asc']]
   });

   // Handle click on "Select all" control
   $('#example-select-all').on('click', function(){
      // Get all rows with search applied
      var rows = table.rows({ 'search': 'applied' }).nodes();
      // Check/uncheck checkboxes for all rows in the table
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
   });

   // Handle click on checkbox to set state of "Select all" control
   $('#auditoria tbody').on('change', 'input[type="checkbox"]', function(){
      // If checkbox is not checked
      if(!this.checked){
         var el = $('#example-select-all').get(0);
         // If "Select all" control is checked and has 'indeterminate' property
         if(el && el.checked && ('indeterminate' in el)){
            // Set visual state of "Select all" control
            // as 'indeterminate'
            el.indeterminate = true;
         }
      }
   });

   // Handle form submission event
   $('#frm-example').on('submit', function(e){
      var form = this;

      // Iterate over all checkboxes in the table
      table.$('input[type="checkbox"]').each(function(){
         // If checkbox doesn't exist in DOM
         if(!$.contains(document, this)){
            // If checkbox is checked
            if(this.checked){
               // Create a hidden element
               $(form).append(
                  $('<input>')
                     .attr('type', 'hidden')
                     .attr('name', this.name)
                     .val(this.value)
               );
            }
         }
      });
   });

});
</script>
<script>
    $(document).ready(function () {
       
        $('#buscar').click(function () {
        var formData = new FormData($("#p_personas")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-p_persona').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>
<script>
    $(document).ready(function () {
        $('#cantidad').mask('#.##0',{reverse: true,maxlength:false});
        $('#punidad').mask('#.##0,00',{reverse: true,maxlength:false});
    });
</script>

<script>
// Date Picker
    $(document).ready(function () {
        $('#fentr').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd/mm/yyyy'
        });
        $('#hasta').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd/mm/yyyy'
        });
    }); 
</script>

<script type="text/javascript">
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'corteEntrega',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('corte?c=<?php echo $c; ?>', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>