<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Gestión de Pedidos</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Pedidos</a></li>
            <li><a href="#">Gestión</a></li>
            <li><a href="#">Corte</a></li>
            <li class="active">Listado</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Datos del Pedido</h3>
    </div>
    <div class="panel-body" >
        <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'pedidos', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            $connection = Yii::app()->db;

        ?>
        <div class="row">
            <div class="col-sm-12 hide">        
                <div class="form-group">
                    <label>Código *</label>
                    <?php echo CHtml::hiddenField('codig', $pedidos['pedid_codig'], array('class' => 'form-control', 'placeholder' => "Total a Solicitar",'disabled'=>'true')); ?>
                </div>
            </div>
            
            
        </div>
        <div class="row">
            <div class="col-sm-4">        
                <div class="form-group">
                    <label>Número de Pedido *</label>
                    <?php echo CHtml::textField('numer', $pedidos['pedid_numer'], array('class' => 'form-control', 'placeholder' => "Total a Solicitar",'disabled'=>'true')); ?>
                </div>
            </div>
            <div class="col-sm-4">        
                <div class="form-group">
                    <label>Fecha de Entrega *</label>
                    <?php echo CHtml::textField('fentr', $pedidos['pedid_fentr'], array('class' => 'form-control', 'placeholder' => "Fecha de Entrega",'disabled'=>'true')); ?>
                </div>
            </div>
            <div class="col-sm-4">        
                <div class="form-group">
                    <label>Estatus *</label>
                    <?php $conexion = Yii::app()->db;
                        $sql="SELECT *
                              FROM rd_preregistro_estatus
                              WHERE epedi_codig = '".$pedidos['epedi_codig']."'";
                        $result=$conexion->createCommand($sql)->queryAll();
                        $data=CHtml::listData($result,'epedi_codig','epedi_descr');
                        echo CHtml::dropDownList('epedi', $pedidos['epedi_codig'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true'));?>
                </div>
            </div>

             <div class="col-sm-4 hide">        
                <div class="form-group">
                    <label>Color del Cuerpo *</label>
                    <?php 
                        $conexion = Yii::app()->db;
                        $sql="SELECT mcolo_codig, concat( mcolo_numer,' - ',mcolo_descr) mcolo_descr
                              FROM pedido_modelo_color 
                              WHERE mcolo_codig = '".$pedidos['mcolo_codig']."'";
                        $result=$conexion->createCommand($sql)->queryAll();
                        $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                        echo CHtml::dropDownList('color', $pedidos['mcolo_codig'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                </div>
            </div>
            
        </div>
        
        
        <div class="row">
                       
            <div class="col-sm-6">
                <a href="listado" class="btn btn-block btn-default" >Volver</a>
            </div>
            <div class="col-sm-6">
                <a href="../../reportes/pedidos/corte?c=<?php echo $pedidos['pedid_codig'] ?>" target="_blank"  class="btn btn-block btn-info" >Imprimir</a>
            </div>
            
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div>  
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Listado</h3>
    </div>
    <div class="panel-body" >
        
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-pedido'>
                <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                #
                            </th>
                            <th>
                                Nro pedido
                            </th>
                            <th>
                                Modelo
                            </th>
                            <th>
                                Nombre
                            </th>
                            <th>
                                Talla
                            </th>

                            <th>
                                Ajuste de Talla
                            </th>
                            <th>
                                Observaciones
                            </th>
                            <th>
                                Trabajador
                            </th>
                            <th>
                                Fecha de Entrega
                            </th>
                            <th>
                                Estatus
                            </th>
                            <!--th width="5%">
                                Modificar
                            </th>
                            <th width="5%">
                                Imprimir
                            </th>
                            <th width="5%">
                                Corte
                            </th>
                            <th width="5%">
                                Diseño
                            </th-->
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        $sql = "SELECT * FROM pedido_pedidos a 
                                JOIN pedido_pedidos_detalle b ON (a.pedid_codig = b.pedid_codig)
                                JOIN pedido_pedidos_detalle_listado c ON (a.pedid_codig = c.pedid_codig)
                                JOIN pedido_talla d ON (c.talla_codig = d.talla_codig)
                                JOIN pedido_modelo e ON (b.model_codig = e.model_codig)
                                JOIN pedido_modelo_tipo f ON (b.tmode_codig = f.tmode_codig)
                                WHERE a.pedid_codig = '".$pedidos['pedid_codig']."'
                                ";
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                            $sql="SELECT * FROM pedido_corte a
                                  JOIN p_trabajador b ON (a.traba_codig = b.traba_codig)
                                  JOIN p_persona c ON (b.perso_codig = c.perso_codig)
                                  JOIN pedido_corte_estatu d ON (a.ecort_codig = d.ecort_codig)
                                  WHERE a.pdlis_codig='".$row['pdlis_codig']."'";
                            $corte=$connection->createCommand($sql)->queryRow();
                        ?>
                        <tr>
                            <td class="tabla"><?php echo $i ?></td>
                            <td class="tabla"><?php echo $row['pedid_numer'] ?></td>
                            <td class="tabla"><?php echo $row['tmode_descr'].' '.$row['model_descr'] ?></td>
                            <td class="tabla"><?php echo $row['pdlis_nombr'] ?></td>
                            <td class="tabla"><?php echo $row['talla_descr'] ?></td>
                            <td class="tabla"><?php echo $row['pdlis_ajuste'] ?></td>
                            <td class="tabla"><?php echo $row['pdlis_obser'] ?></td>
                            <td class="tabla"><?php echo $corte['perso_pnomb'].' '.$corte['perso_papel'] ?></td>
                            <td class="tabla"><?php echo $corte['corte_fentr'] ?></td>
                            <td class="tabla"><?php echo $corte['ecort_descr'] ?></td>
                            <!--td class="tabla"><a href="fechaEntrega?c=<?php echo $row['pedid_codig'] ?>" target="_blank" class="btn btn-block btn-info"><i class="fa fa-calendar"></i></a></td>
                            <td class="tabla"><a href="../../reportes/pedidos/pagos?c=<?php echo $row['pagos_codig'] ?>" target="_blank" class="btn btn-block btn-info"><i class="fa fa-file-pdf-o"></i></a></td>
                            
                            <td class="tabla"><a href="corte?c=<?php echo $row['pagos_codig'] ?>" target="_blank" class="btn btn-block btn-success"><i class="fa fa-cut"></i></a></td>
                            <td class="tabla"><a href="diseno?c=<?php echo $row['pagos_codig'] ?>" target="_blank" class="btn btn-block btn-danger"><i class="fa fa-paint-brush"></i></a></td-->
                        </tr>
                        <?php
                            }   
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="panel-footer">
        <div class="row">
            <?php
                if($pedidos['epedi_codig']=='3'){
                ?>
                <div class="col-sm-6">
                    <a href="CorteAsignar?c=<?php echo $pedidos['pedid_codig'] ?>" class="btn btn-block btn-info" >Asignar Trabajador</a>
                </div>
                <div class="col-sm-6">
                    <a href="CorteEntrega?c=<?php echo $pedidos['pedid_codig'] ?>" class="btn btn-block btn-info" >Entrega</a>
                </div>
                <?php  
                }else{
                ?>
                <div class="col-sm-6">
                    <a href="#" class="btn btn-block btn-info disabled" >Asignar Trabajador</a>
                </div>
                <div class="col-sm-6">
                    <a href="#" class="btn btn-block btn-info disabled" >Entrega</a>
                </div>
                <?php 
                }
            ?>
                
            </div>
    </div>
</div>  
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#buscar').click(function () {
        var formData = new FormData($("#pedidos")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-pedido').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>
