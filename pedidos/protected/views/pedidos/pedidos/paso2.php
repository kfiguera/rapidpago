<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Detalle del Pedido</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Pedidos</a></li>
            <li><a href="#">Detalle</a></li>
            <li class="active">Registrar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <div class="row line-steps">
                  <div class="col-md-4 column-step <?php echo $ubicacion[0]; ?>">
                    <a href="modificar?c=<?php echo $solicitud['solic_codig']?>&s=1">
                        <div class="step-number">1 </div>
                    <div class="step-title">Pedido</div>
                    <div class="step-info">Detalles del Pedido</div>
                 </div>

                 <div class="col-md-4 column-step <?php echo $ubicacion[1]; ?> ">
                    <div class="step-number">2</div>
                    <div class="step-title">Adicional</div>
                    <div class="step-info">Información Adicional a Bordar Parte 1</div>
                 </div>
                 <div class="col-md-4 column-step <?php echo $ubicacion[2]; ?> ">
                    <div class="step-number">3</div>
                    <div class="step-title">Adicional</div>
                    <div class="step-info">Información Adicional a Bordar Parte 2</div>
                 </div>
              </div>
        </div>
    </div>
</div>
<form id='login-form' name='login-form' method="post">

<div class="row">   
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Pecho</h3>
        </div>
        <div class="panel-body" >
           
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
                <div class="row hide">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Pedido *</label>
                            <?php 
                                echo CHtml::hiddenField('pedid', $p, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Código *</label>
                            <?php 
                                echo CHtml::hiddenField('codig', $c, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Paso *</label>
                            <?php 
                                echo CHtml::hiddenField('pasos', $pedidos['pedid_pasos'], array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color del Cuerpo *</label>
                            <?php 
                                echo CHtml::hiddenField('ccuer', $pedidos['pedid_ccuer'], array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color de las Mangas *</label>
                            <?php 
                                echo CHtml::hiddenField('cmang', $pedidos['pedid_cmang'], array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Fuente *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.fuent_codig, fuent_descr
                                      FROM pedido_fuente a
                                      GROUP BY 1,2
                                      ORDER BY fuent_descr";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                echo CHtml::dropDownList('pfuen', $pedidos['pedid_pfuen'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-1" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                    FROM pedido_modelo_color a
                                    WHERE tmode_codig='1'
                                    AND mcolo_numer<>'0'
                                    GROUP BY 1,2
                                    ORDER BY 2";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                                echo CHtml::dropDownList('pcolo', $pedidos['pedid_pcolo'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-2" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Posición *</label>
                            <?php 
                                
                                $data=array('1'=>'DERECHO', '2'=>'IZQUIERDO', '3'=>'AMBOS');
                                echo CHtml::dropDownList('pposi', $pedidos['pedid_pposi'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                                <span class="help-block">
                                    <small>Nota: indicar teniendo el poleron puesto</small>
                                </span>
                        </div>
                    </div>
                </div>
                <?php
                    switch ($pedidos['pedid_pposi']) {
                        case '1':
                            $posicion['p']['d']['infor']['hide']='';
                            $posicion['p']['d']['infor']['disabled']='';
                            $posicion['p']['i']['infor']['hide']='hide';
                            $posicion['p']['i']['infor']['disabled']='true';
                            break;
                        case '2':
                            $posicion['p']['d']['infor']['hide']='hide';
                            $posicion['p']['d']['infor']['disabled']='true';
                            $posicion['p']['i']['infor']['hide']='';
                            $posicion['p']['i']['infor']['disabled']='';
                            break;
                        case '3':
                            $posicion['p']['d']['infor']['hide']='';
                            $posicion['p']['d']['infor']['disabled']='';
                            $posicion['p']['i']['infor']['hide']='';
                            $posicion['p']['i']['infor']['disabled']='';
                            break;
                        default:
                            $posicion['p']['d']['infor']['hide']='hide';
                            $posicion['p']['d']['infor']['disabled']='true';
                            $posicion['p']['i']['infor']['hide']='hide';
                            $posicion['p']['i']['infor']['disabled']='true';
                            break;
                    }
                    switch ($pedidos['pedid_pibde']) {
                        case '2':
                            $posicion['p']['d']['texto']['hide']='';
                            $posicion['p']['d']['texto']['disabled']='';
                            $posicion['p']['d']['timag']['hide']='hide';
                            $posicion['p']['d']['timag']['disabled']='true';
                            break;
                        case '1':
                            $posicion['p']['d']['texto']['hide']='hide';
                            $posicion['p']['d']['texto']['disabled']='true';
                            $posicion['p']['d']['timag']['hide']='';
                            $posicion['p']['d']['timag']['disabled']='';
                            break;
                        case '3':
                            $posicion['p']['d']['texto']['hide']='';
                            $posicion['p']['d']['texto']['disabled']='';
                            $posicion['p']['d']['timag']['hide']='';
                            $posicion['p']['d']['timag']['disabled']='';
                            break;
                        default:
                            $posicion['p']['d']['texto']['hide']='hide';
                            $posicion['p']['d']['texto']['disabled']='true';
                            $posicion['p']['d']['timag']['hide']='hide';
                            $posicion['p']['d']['timag']['disabled']='true';
                            break;
                    }
                    switch ($pedidos['pedid_ptide']) {
                        case '5':
                            $posicion['p']['d']['image']['hide']='';
                            $posicion['p']['d']['image']['disabled']='';
                            break;
                        default:
                            $posicion['p']['d']['image']['hide']='hide';
                            $posicion['p']['d']['image']['disabled']='disabled';
                            break;
                    }
                    ?>
                <div class="row">
                    <div id="p-d">
                        <div id="p-d-infor" class=" <?php echo $posicion['p']['d']['infor']['hide']; ?> col-sm-4">        
                            <div class="form-group">
                                <label>Información a Bordar Derecha *</label>
                                <?php             
                                $data=array('1'=>'IMAGEN', '2'=>'TEXTO', '3'=>'AMBOS');
                                echo CHtml::dropDownList('pibde', $pedidos['pedid_pibde'], $data, array('class' => 'form-control', 'placeholder' => "Texto a Bordar Derecha",'prompt'=>'Seleccione...','disabled'=>$posicion['p']['d']['infor']['disabled'])); ?>
                            </div>
                    
                        </div>
                        <div id="p-d-texto" class="<?php echo $posicion['p']['d']['texto']['hide']; ?> col-sm-4">        
                            <div class="form-group">
                                <label>Texto a Bordar Derecha *</label>
                                <?php 
                                    echo CHtml::textField('ptbde', $pedidos['pedid_ptbde'], array('class' => 'form-control', 'placeholder' => "Texto a Bordar Derecha",'prompt'=>'Seleccione...','disabled'=>$posicion['p']['d']['texto']['disabled'])); ?>
                            </div>
                        </div>
                        <div id="p-d-timag" class="<?php echo $posicion['p']['d']['timag']['hide']; ?> col-sm-4">        
                                <div class="form-group">
                                    <label>Tipo de Imagen Derecha *</label>
                                    <?php 
                                        $sql="SELECT * FROM pedido_pedidos_imagen_tipo WHERE ptima_codig <>'4'";
                                        $conexion = Yii::app()->db;
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'ptima_codig','ptima_descr');
                                        echo CHtml::dropDownList('ptide', $pedidos['pedid_ptide'], $data,array('class' => 'form-control', 'placeholder' => "Texto a Bordar Derecha",'prompt'=>'Seleccione...','disabled'=>$posicion['p']['d']['timag']['disabled'])); ?>
                                </div>
                            </div>  
                        <div id="p-d-image" class="<?php echo $posicion['p']['d']['image']['hide']; ?> col-sm-12">        
                            <div class="numero1 form-group">
                                <label>Imagen a Bordar Derecha</label>
                                <div  class="input-group image-preview" data-placement="top" >  

                                    <img id="dynamic">
                                    <!-- image-preview-filename input [CUT FROM HERE]-->
                                    <?php 
                                        $sql="SELECT * FROM pedido_pedidos_imagen WHERE pedid_codig ='".$c."' and pimag_orden=1";
                                        $result=$conexion->createCommand($sql)->queryRow();
                                        $ruta=$result['pimag_ruta'];
                                        
                                        $nombre=explode('/', $ruta);
                                        $nombre=end($nombre);
                                        if($nombre){
                                            $opcional['1']['hide']='';
                                            $opcional['1']['show']='true';
                                        }else{
                                            $opcional['1']['hide']='hide';
                                            $opcional['1']['show']='false';
                                        }
                                    ?>
                                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="true" value="<?php echo $nombre ?>" > <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-btn">
                                        <!-- image-preview-clear button -->
                                        <button type="button" class="btn btn-default image-preview-clear <?php echo $opcional['1']['hide'] ?>" >
                                            <span class="fa fa-times"></span> Limpiar
                                        </button>
                                        <!-- image-preview-input -->
                                        <div class="btn btn-default image-preview-input">
                                            <span class="fa fa-folder-open"></span>
                                            <span class="numero1 image-preview-input-title">Buscar</span>
                                            <input type="file" id="pride" name="pride" <?php echo $posicion['p']['d']['image']['disabled']; ?>/> <!-- rename it -->
                                        </div>
                                    </span>
                               
                                </div> 
                            </div> 
                            <div class="hide" id="popover-numero-1">
                                <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$ruta ?>" class="img-responsive">
                            </div>
                            <?php
                                if($opcional['1']['show']=='true') { 
                            ?>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    var closebtn = $('<button/>', {
                                        type: "button",
                                        text: 'x',
                                        id: 'close-preview',
                                        style: 'font-size: initial;',
                                    });
                                    closebtn.attr("class","close pull-right");
                                    $('.numero1 .image-preview').popover({
                                      html: true,
                                      title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
                                      content: function() {
                                        return $('#popover-numero-1').html();
                                      },
                                      trigger: 'manual'
                                    });

                                    $(".numero1 .image-preview").popover("show");
                                });
                            </script>
                            <?php
                                }
                            ?>
                        </div> 
                    </div>
                    <?php
                    
                    switch ($pedidos['pedid_pibiz']) {
                        case '2':
                            $posicion['p']['i']['texto']['hide']='';
                            $posicion['p']['i']['texto']['disabled']='';
                            $posicion['p']['i']['timag']['hide']='hide';
                            $posicion['p']['i']['timag']['disabled']='true';
                            break;
                        case '1':
                            $posicion['p']['i']['texto']['hide']='hide';
                            $posicion['p']['i']['texto']['disabled']='true';
                            $posicion['p']['i']['timag']['hide']='';
                            $posicion['p']['i']['timag']['disabled']='';
                            break;
                        case '3':
                            $posicion['p']['i']['texto']['hide']='';
                            $posicion['p']['i']['texto']['disabled']='';
                            $posicion['p']['i']['timag']['hide']='';
                            $posicion['p']['i']['timag']['disabled']='';
                            break;
                        default:
                            $posicion['p']['i']['texto']['hide']='hide';
                            $posicion['p']['i']['texto']['disabled']='true';
                            $posicion['p']['i']['timag']['hide']='hide';
                            $posicion['p']['i']['timag']['disabled']='true';
                            break;
                    }
                    switch ($pedidos['pedid_ptiiz']) {
                        case '5':
                            $posicion['p']['i']['image']['hide']='';
                            $posicion['p']['i']['image']['disabled']='';
                            break;
                        default:
                            $posicion['p']['i']['image']['hide']='hide';
                            $posicion['p']['i']['image']['disabled']='disabled';
                            break;
                    }
                    ?>
                    <div id="p-i">
                        <div id="p-i-infor" class="<?php echo $posicion['p']['i']['infor']['hide']; ?> col-sm-4">        
                            <div class="form-group">
                                <label>Información a Bordar Izquierda *</label>
                                <?php             
                                $data=array('1'=>'IMAGEN', '2'=>'TEXTO', '3'=>'AMBOS');
                                echo CHtml::dropDownList('pibiz', $pedidos['pedid_pibiz'], $data, array('class' => 'form-control', 'placeholder' => "Información a Bordar Izquierda",'prompt'=>'Seleccione...','disabled'=>$posicion['p']['i']['infor']['disabled'])); ?>
                            </div>
                    
                        </div>
                        <div id="p-i-texto" class="<?php echo $posicion['p']['i']['texto']['hide']; ?> col-sm-4">        
                            <div class="form-group">
                                <label>Texto a Bordar Izquierda *</label>
                                <?php 
                                    echo CHtml::textField('ptbiz', $pedidos['pedid_ptbiz'], array('class' => 'form-control', 'placeholder' => "Texto a Bordar Izquierda",'prompt'=>'Seleccione...','disabled'=>$posicion['p']['i']['texto']['disabled'] )); ?>
                            </div>
                        </div>
                        <div id="p-i-timag" class="<?php echo $posicion['p']['i']['timag']['hide']; ?> col-sm-4">        
                                <div class="form-group">
                                    <label>Tipo de Imagen Izquierda *</label>
                                    <?php 
                                        $sql="SELECT * FROM pedido_pedidos_imagen_tipo WHERE ptima_codig <>'4'";
                                        $conexion = Yii::app()->db;
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'ptima_codig','ptima_descr');
                                        echo CHtml::dropDownList('ptiiz', $pedidos['pedid_ptiiz'], $data,array('class' => 'form-control', 'placeholder' => "Texto a Bordar Izquierda",'prompt'=>'Seleccione...','disabled'=>$posicion['p']['i']['texto']['disabled'] )); ?>
                                </div>
                            </div>  
                        <div id="p-i-image" class=" <?php echo $posicion['p']['i']['image']['hide']; ?> col-sm-12">        
                            <div class="numero2 form-group">
                                <label>Imagen a Bordar Izquierda</label>
                                <div  class="input-group image-preview" data-placement="top" >  

                                    <img id="dynamic">
                                    <!-- image-preview-filename input [CUT FROM HERE]-->
                                    <?php 
                                        $sql="SELECT * FROM pedido_pedidos_imagen WHERE pedid_codig ='".$c."' and pimag_orden=2";
                                        $result=$conexion->createCommand($sql)->queryRow();
                                        $ruta=$result['pimag_ruta'];
                                        $nombre=explode('/', $ruta);
                                        $nombre=end($nombre);
                                        if($nombre){
                                            $opcional['2']['hide']='';
                                            $opcional['2']['show']='true';
                                        }else{
                                            $opcional['2']['hide']='hide';
                                            $opcional['2']['show']='false';
                                        }
                                    ?>
                                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="true" value="<?php echo $nombre ?>" > <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-btn">
                                        <!-- image-preview-clear button -->
                                        <button type="button" class="btn btn-default image-preview-clear <?php echo $opcional['2']['hide'] ?>" >
                                            <span class="fa fa-times"></span> Limpiar
                                        </button>
                                        <!-- image-preview-input -->
                                        <div class="btn btn-default image-preview-input">
                                            <span class="fa fa-folder-open"></span>
                                            <span class="numero2 image-preview-input-title">Buscar</span>
                                            <input type="file" id="priiz" name="priiz" <?php echo $posicion['p']['i']['image']['disabled']?>/> <!-- rename it -->
                                        </div>
                                    </span>
                               
                                </div> 
                            </div> 
                            <div class="hide" id="popover-numero-2">
                                <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$ruta ?>" class="img-responsive">
                            </div>
                            <?php
                                if($opcional['2']['show']=='true') { 
                            ?>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    var closebtn = $('<button/>', {
                                        type: "button",
                                        text: 'x',
                                        id: 'close-preview',
                                        style: 'font-size: initial;',
                                    });
                                    closebtn.attr("class","close pull-right");
                                    $('.numero2 .image-preview').popover({
                                      html: true,
                                      title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
                                      content: function() {
                                        return $('#popover-numero-2').html();
                                      },
                                      trigger: 'manual'
                                    });

                                    $(".numero2 .image-preview").popover("show");
                                });
                            </script>
                            <?php
                                }
                            ?>
                        </div> 
                    </div>
                </div>
                <div class="row">
                    
                
                
                    
                </div>
                 <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observaciones</label>
                            <?php 
                                echo CHtml::textArea('pobse', $pedidos['pedid_pobse'], array('class' => 'form-control', 'placeholder' => "Observaciones",'prompt'=>'Seleccione...')); ?>
                                
                               
                        </div>
                    </div>
                    
                </div>

            

        </div><!-- form -->
    </div>  
</div>
<?php
if($pedidos['gorro']=='1'){
    $gorro='';
}else{
    $gorro='true';
}
?>

<div class="row">   
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Gorro (dejar en blanco si no tiene información a bordar)</h3>
        </div>
        <div class="panel-body" >
           
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
                <div class="row">
                    
                    
                    
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Fuente *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.fuent_codig, fuent_descr
                                      FROM pedido_fuente a
                                      GROUP BY 1,2
                                      ORDER BY fuent_descr";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                echo CHtml::dropDownList('gfuen', $pedidos['pedid_gfuen'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$gorro)); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-7" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                    FROM pedido_modelo_color a
                                    WHERE tmode_codig='1'
                                    AND mcolo_numer<>'0'
                                    GROUP BY 1,2
                                    ORDER BY 2";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                                echo CHtml::dropDownList('gcolo', $pedidos['pedid_gcolo'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$gorro)); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-8" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Posición *</label>
                            <?php 
                                
                                $data=array('1'=>'DERECHO', '2'=>'IZQUIERDO', '3'=>'AMBOS');
                                echo CHtml::dropDownList('gposi', $pedidos['pedid_gposi'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                                <span class="help-block">
                                    <small>Nota: indicar teniendo el poleron puesto</small>
                                </span>
                        </div>
                    </div>
                </div>
                <?php
                    switch ($pedidos['pedid_gposi']) {
                        case '1':
                            $posicion['g']['d']['infor']['hide']='';
                            $posicion['g']['d']['infor']['disabled']='';
                            $posicion['g']['i']['infor']['hide']='hide';
                            $posicion['g']['i']['infor']['disabled']='true';
                            break;
                        case '2':
                            $posicion['g']['d']['infor']['hide']='hide';
                            $posicion['g']['d']['infor']['disabled']='true';
                            $posicion['g']['i']['infor']['hide']='';
                            $posicion['g']['i']['infor']['disabled']='';
                            break;
                        case '3':
                            $posicion['g']['d']['infor']['hide']='';
                            $posicion['g']['d']['infor']['disabled']='';
                            $posicion['g']['i']['infor']['hide']='';
                            $posicion['g']['i']['infor']['disabled']='';
                            break;
                        default:
                            $posicion['g']['d']['infor']['hide']='hide';
                            $posicion['g']['d']['infor']['disabled']='true';
                            $posicion['g']['i']['infor']['hide']='hide';
                            $posicion['g']['i']['infor']['disabled']='true';
                            break;
                    }
                    switch ($pedidos['pedid_gibde']) {
                        case '2':
                            $posicion['g']['d']['texto']['hide']='';
                            $posicion['g']['d']['texto']['disabled']='';
                            $posicion['g']['d']['timag']['hide']='hide';
                            $posicion['g']['d']['timag']['disabled']='true';
                            break;
                        case '1':
                            $posicion['g']['d']['texto']['hide']='hide';
                            $posicion['g']['d']['texto']['disabled']='true';
                            $posicion['g']['d']['timag']['hide']='';
                            $posicion['g']['d']['timag']['disabled']='';
                            break;
                        case '3':
                            $posicion['g']['d']['texto']['hide']='';
                            $posicion['g']['d']['texto']['disabled']='';
                            $posicion['g']['d']['timag']['hide']='';
                            $posicion['g']['d']['timag']['disabled']='';
                            break;
                        default:
                            $posicion['g']['d']['texto']['hide']='hide';
                            $posicion['g']['d']['texto']['disabled']='true';
                            $posicion['g']['d']['timag']['hide']='hide';
                            $posicion['g']['d']['timag']['disabled']='true';
                            break;
                    }
                    switch ($pedidos['pedid_gtide']) {
                        case '5':
                            $posicion['g']['d']['image']['hide']='';
                            $posicion['g']['d']['image']['disabled']='';
                            break;
                        default:
                            $posicion['g']['d']['image']['hide']='hide';
                            $posicion['g']['d']['image']['disabled']='disabled';
                            break;
                    }
                                  
                    switch ($pedidos['pedid_gibiz']) {
                        case '2':
                            $posicion['g']['i']['texto']['hide']='';
                            $posicion['g']['i']['texto']['disabled']='';
                            $posicion['g']['i']['timag']['hide']='hide';
                            $posicion['g']['i']['timag']['disabled']='true';
                            break;
                        case '1':
                            $posicion['g']['i']['texto']['hide']='hide';
                            $posicion['g']['i']['texto']['disabled']='true';
                            $posicion['g']['i']['timag']['hide']='';
                            $posicion['g']['i']['timag']['disabled']='';
                            break;
                        case '3':
                            $posicion['g']['i']['texto']['hide']='';
                            $posicion['g']['i']['texto']['disabled']='';
                            $posicion['g']['i']['timag']['hide']='';
                            $posicion['g']['i']['timag']['disabled']='';
                            break;
                        default:
                            $posicion['g']['i']['texto']['hide']='hide';
                            $posicion['g']['i']['texto']['disabled']='true';
                            $posicion['g']['i']['timag']['hide']='hide';
                            $posicion['g']['i']['timag']['disabled']='true';
                            break;
                    }
                    switch ($pedidos['pedid_gtiiz']) {
                        case '5':
                            $posicion['g']['i']['image']['hide']='';
                            $posicion['g']['i']['image']['disabled']='';
                            break;
                        default:
                            $posicion['g']['i']['image']['hide']='hide';
                            $posicion['g']['i']['image']['disabled']='disabled';
                            break;
                    }
                    if($gorro=='true'){
                        $posicion['g']['d']['infor']['hide']='hide';
                        $posicion['g']['d']['infor']['disabled']='true';
                        $posicion['g']['i']['infor']['hide']='hide';
                        $posicion['g']['i']['infor']['disabled']='true';
                        $posicion['g']['d']['texto']['hide']='hide';
                        $posicion['g']['d']['texto']['disabled']='true';
                        $posicion['g']['d']['timag']['hide']='hide';
                        $posicion['g']['d']['timag']['disabled']='true';
                        $posicion['g']['d']['image']['hide']='hide';
                        $posicion['g']['d']['image']['disabled']='disabled';
                        $posicion['g']['i']['texto']['hide']='hide';
                        $posicion['g']['i']['texto']['disabled']='true';
                        $posicion['g']['i']['timag']['hide']='hide';
                        $posicion['g']['i']['timag']['disabled']='true';
                        $posicion['g']['i']['image']['hide']='hide';
                        $posicion['g']['i']['image']['disabled']='disabled';
                    }
                ?>
                <div class="row">
                    <div id="g-d">
                        <div id="g-d-infor" class="<?php echo $posicion['g']['d']['infor']['hide']; ?> col-sm-4">        
                            <div class="form-group">
                                <label>Información a Bordar Derecha *</label>
                                <?php             
                                $data=array('1'=>'IMAGEN', '2'=>'TEXTO', '3'=>'AMBOS');
                                echo CHtml::dropDownList('gibde', $pedidos['pedid_gibde'], $data, array('class' => 'form-control', 'placeholder' => "Texto a Bordar Derecha",'prompt'=>'Seleccione...','disabled'=>$posicion['g']['d']['infor']['disabled'])); ?>
                            </div>
                    
                        </div>
                        <div id="g-d-texto" class="<?php echo $posicion['g']['d']['texto']['hide']; ?> col-sm-4">        
                            <div class="form-group">
                                <label>Texto a Bordar Derecha *</label>
                                <?php 
                                    echo CHtml::textField('gtbde', $pedidos['pedid_gtbde'], array('class' => 'form-control', 'placeholder' => "Texto a Bordar Derecha",'prompt'=>'Seleccione...','disabled'=>$posicion['g']['d']['texto']['disabled'])); ?>
                            </div>
                        </div>
                        <div id="g-d-timag" class="<?php echo $posicion['g']['d']['timag']['hide']; ?> col-sm-4">        
                                <div class="form-group">
                                    <label>Tipo de Imagen Derecha *</label>
                                    <?php 
                                        $sql="SELECT * FROM pedido_pedidos_imagen_tipo WHERE ptima_codig <>'4'";
                                        $conexion = Yii::app()->db;
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'ptima_codig','ptima_descr');
                                        echo CHtml::dropDownList('gtide', $pedidos['pedid_gtide'], $data,array('class' => 'form-control', 'placeholder' => "Texto a Bordar Derecha",'prompt'=>'Seleccione...','disabled'=>$posicion['g']['d']['timag']['disabled'])); ?>
                                </div>
                            </div>  
                        <div id="g-d-image" class="<?php echo $posicion['g']['d']['timag']['hide']; ?> col-sm-12">        
                            <div class="numero3 form-group">
                                <label>Imagen a Bordar Derecha</label>
                                <div  class="input-group image-preview" data-placement="top" >  

                                    <img id="dynamic">
                                    <!-- image-preview-filename input [CUT FROM HERE]-->
                                    <?php 
                                        $sql="SELECT * FROM pedido_pedidos_imagen WHERE pedid_codig ='".$c."' and pimag_orden=3";
                                        $result=$conexion->createCommand($sql)->queryRow();
                                        $ruta=$result['pimag_ruta'];
                                        $nombre=explode('/', $ruta);
                                        $nombre=end($nombre);
                                        if($nombre){
                                            $opcional['3']['hide']='';
                                            $opcional['3']['show']='true';
                                        }else{
                                            $opcional['3']['hide']='hide';
                                            $opcional['3']['show']='false';
                                        }
                                    ?>
                                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="true" value="<?php echo $nombre ?>" > <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-btn">
                                        <!-- image-preview-clear button -->
                                        <button type="button" class="btn btn-default image-preview-clear <?php echo $opcional['3']['hide'] ?>" >
                                            <span class="fa fa-times"></span> Limpiar
                                        </button>
                                        <!-- image-preview-input -->
                                        <div class="btn btn-default image-preview-input">
                                            <span class="fa fa-folder-open"></span>
                                            <span class="numero3 image-preview-input-title">Buscar</span>
                                            <input type="file" id="gride" name="gride" <?php echo $posicion['g']['d']['timag']['disabled']; ?>/> <!-- rename it -->
                                        </div>
                                    </span>
                               
                                </div> 
                            </div> 
                            <div class="hide" id="popover-numero-3">
                                <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$ruta ?>" class="img-responsive">
                            </div>
                            <?php
                                if($opcional['3']['show']=='true') { 
                            ?>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    var closebtn = $('<button/>', {
                                        type: "button",
                                        text: 'x',
                                        id: 'close-preview',
                                        style: 'font-size: initial;',
                                    });
                                    closebtn.attr("class","close pull-right");
                                    $('.numero3 .image-preview').popover({
                                      html: true,
                                      title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
                                      content: function() {
                                        return $('#popover-numero-3').html();
                                      },
                                      trigger: 'manual'
                                    });

                                    $(".numero3 .image-preview").popover("show");
                                });
                            </script>
                            <?php
                                }
                            ?>
                        </div> 
                    </div>
                    <div id="g-i">
                        <div id="g-i-infor" class="<?php echo $posicion['g']['i']['infor']['hide']; ?> col-sm-4">        
                            <div class="form-group">
                                <label>Información a Bordar Izquierda *</label>
                                <?php             
                                $data=array('1'=>'IMAGEN', '2'=>'TEXTO', '3'=>'AMBOS');
                                echo CHtml::dropDownList('gibiz', $pedidos['pedid_gibiz'], $data, array('class' => 'form-control', 'placeholder' => "Información a Bordar Izquierda",'prompt'=>'Seleccione...','disabled'=>$posicion['g']['i']['infor']['disabled'])); ?>
                            </div>
                    
                        </div>
                        <div id="g-i-texto" class="<?php echo $posicion['g']['i']['texto']['hide']; ?> col-sm-4">        
                            <div class="form-group">
                                <label>Texto a Bordar Izquierda *</label>
                                <?php 
                                    echo CHtml::textField('gtbiz', $pedidos['pedid_gtbiz'], array('class' => 'form-control', 'placeholder' => "Texto a Bordar Izquierda",'prompt'=>'Seleccione...','disabled'=>$posicion['g']['i']['texto']['disabled'])); ?>
                            </div>
                        </div>
                        <div id="g-i-timag" class="<?php echo $posicion['g']['i']['timag']['hide']; ?> col-sm-4">        
                                <div class="form-group">
                                    <label>Tipo de Imagen Izquierda *</label>
                                    <?php 
                                        $sql="SELECT * FROM pedido_pedidos_imagen_tipo WHERE ptima_codig <>'4'";
                                        $conexion = Yii::app()->db;
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'ptima_codig','ptima_descr');
                                        echo CHtml::dropDownList('gtiiz', $pedidos['pedid_gtiiz'], $data,array('class' => 'form-control', 'placeholder' => "Texto a Bordar Izquierda",'prompt'=>'Seleccione...','disabled'=>$posicion['g']['i']['timag']['disabled'])); ?>
                                </div>
                            </div>  
                        <div id="g-i-image" class="<?php echo $posicion['g']['i']['timag']['hide']; ?> col-sm-12">        
                            <div class="numero4 form-group">
                                <label>Imagen a Bordar Izquierda</label>
                                <div  class="input-group image-preview" data-placement="top" >  

                                    <img id="dynamic">
                                    <!-- image-preview-filename input [CUT FROM HERE]-->
                                    <?php 
                                        $sql="SELECT * FROM pedido_pedidos_imagen WHERE pedid_codig ='".$c."' and pimag_orden=4";
                                        $result=$conexion->createCommand($sql)->queryRow();
                                        $ruta=$result['pimag_ruta'];
                                        $nombre=explode('/', $ruta);
                                        $nombre=end($nombre);
                                        if($nombre){
                                            $opcional['4']['hide']='';
                                            $opcional['4']['show']='true';
                                        }else{
                                            $opcional['4']['hide']='hide';
                                            $opcional['4']['show']='false';
                                        }
                                    ?>
                                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="true" value="<?php echo $nombre ?>" > <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-btn">
                                        <!-- image-preview-clear button -->
                                        <button type="button" class="btn btn-default image-preview-clear <?php echo $opcional['4']['hide'] ?>" >
                                            <span class="fa fa-times"></span> Limpiar
                                        </button>
                                        <!-- image-preview-input -->
                                        <div class="btn btn-default image-preview-input">
                                            <span class="fa fa-folder-open"></span>
                                            <span class="numero4 image-preview-input-title">Buscar</span>
                                            <input type="file" id="griiz" name="griiz" <?php echo $posicion['g']['i']['timag']['disabled']; ?>/> <!-- rename it -->
                                        </div>
                                    </span>
                               
                                </div> 
                            </div> 
                            <div class="hide" id="popover-numero-4">
                                <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$ruta ?>" class="img-responsive">
                            </div>
                            <?php
                                if($opcional['4']['show']=='true') { 
                            ?>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    var closebtn = $('<button/>', {
                                        type: "button",
                                        text: 'x',
                                        id: 'close-preview',
                                        style: 'font-size: initial;',
                                    });
                                    closebtn.attr("class","close pull-right");
                                    $('.numero4 .image-preview').popover({
                                      html: true,
                                      title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
                                      content: function() {
                                        return $('#popover-numero-4').html();
                                      },
                                      trigger: 'manual'
                                    });

                                    $(".numero4 .image-preview").popover("show");
                                });
                            </script>
                            <?php
                                }
                            ?>
                        </div> 
                    </div>
                </div>
                <div class="row">
                    
                <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Orientación del Texto *</label>
                            <?php 
                                
                                $data=array('5'=>'HORIZONTAL', '6'=>'VERTICAL');
                                echo CHtml::dropDownList('gorie', $pedidos['pedid_gorie'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$gorro)); ?>
                                <span class="help-block">
                                    <small>Nota: indicar teniendo el poleron puesto</small>
                                </span>
                        </div>
                    </div>
                <?php

                switch ($pedidos['pedid_glima']) {
                        case '1':
                            $opcional['3']['hide']='';
                            $opcional['3']['disabled']='';
                            $opcional['4']['hide']='hide';
                            $opcional['4']['disabled']='true';
                            break;
                        case '2':
                            $opcional['3']['hide']='hide';
                            $opcional['3']['disabled']='true';
                            $opcional['4']['hide']='';
                            $opcional['4']['disabled']='';
                            break;
                        
                        default:
                            $opcional['3']['hide']='hide';
                            $opcional['3']['disabled']='true';
                            $opcional['4']['hide']='hide';
                            $opcional['4']['disabled']='true';
                            break;
                    }
                    ?>
               
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observaciones</label>
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.fuent_codig, fuent_descr
                                      FROM pedido_fuente a
                                      GROUP BY 1,2
                                      ORDER BY fuent_descr";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                echo CHtml::textArea('gobse', $pedidos['pedid_gobse'], array('class' => 'form-control', 'placeholder' => "Observaciones",'prompt'=>'Seleccione...','disabled'=>$gorro)); ?>
                                
                               
                        </div>
                    </div>
                    
                </div>

            

        </div><!-- form -->
    </div>  
</div>
<div class="row">   
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Mangas (dejar en blanco si no tiene información a bordar)</h3>
        </div>
        <div class="panel-body" >
           
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
                <div class="row hide">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Pedido *</label>
                            <?php 
                                echo CHtml::hiddenField('pedid', $p, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Código *</label>
                            <?php 
                                echo CHtml::hiddenField('codig', $c, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Paso *</label>
                            <?php 
                                echo CHtml::hiddenField('pasos', $pedidos['pedid_pasos'], array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color del Cuerpo *</label>
                            <?php 
                                echo CHtml::hiddenField('ccuer', $pedidos['pedid_ccuer'], array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color de las Mangas *</label>
                            <?php 
                                echo CHtml::hiddenField('cmang', $pedidos['pedid_cmang'], array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Fuente *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.fuent_codig, fuent_descr
                                    FROM pedido_fuente a
                                    GROUP BY 1,2
                                    ORDER BY fuent_descr";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                echo CHtml::dropDownList('mfuen', $pedidos['pedid_mfuen'], $data, array('class' => 'form-control', 'placeholder' => "Fuente",'prompt'=>'Seleccione...')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-4" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                  FROM pedido_modelo_color a
                                  WHERE tmode_codig='1'
                                  AND mcolo_numer<>'0'
                                  GROUP BY 1,2
                                  ORDER BY 2";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                                echo CHtml::dropDownList('mcolo', $pedidos['pedid_mcolo'], $data, array('class' => 'form-control', 'placeholder' => "Color",'prompt'=>'Seleccione...')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-5" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Posición *</label>
                            <?php 
                                
                                $data=array('1'=>'DERECHO', '2'=>'IZQUIERDO', '3'=>'AMBOS');
                                echo CHtml::dropDownList('mposi', $pedidos['pedid_mposi'], $data, array('class' => 'form-control', 'placeholder' => "Posición",'prompt'=>'Seleccione...')); ?>
                            <span class="help-block">
                                <small>Nota: indicar teniendo el poleron puesto</small>
                            </span>
                        </div>
                    </div>
                    
                </div>
                <?php
                    switch ($pedidos['pedid_mposi']) {
                        case '1':
                            $posicion['m']['d']['infor']['hide']='';
                            $posicion['m']['d']['infor']['disabled']='';
                            $posicion['m']['i']['infor']['hide']='hide';
                            $posicion['m']['i']['infor']['disabled']='true';
                            break;
                        case '2':
                            $posicion['m']['d']['infor']['hide']='hide';
                            $posicion['m']['d']['infor']['disabled']='true';
                            $posicion['m']['i']['infor']['hide']='';
                            $posicion['m']['i']['infor']['disabled']='';
                            break;
                        case '3':
                            $posicion['m']['d']['infor']['hide']='';
                            $posicion['m']['d']['infor']['disabled']='';
                            $posicion['m']['i']['infor']['hide']='';
                            $posicion['m']['i']['infor']['disabled']='';
                            break;
                        default:
                            $posicion['m']['d']['infor']['hide']='hide';
                            $posicion['m']['d']['infor']['disabled']='true';
                            $posicion['m']['i']['infor']['hide']='hide';
                            $posicion['m']['i']['infor']['disabled']='true';
                            break;
                    }
                    switch ($pedidos['pedid_mibde']) {
                        case '2':
                            $posicion['m']['d']['texto']['hide']='';
                            $posicion['m']['d']['texto']['disabled']='';
                            $posicion['m']['d']['timag']['hide']='hide';
                            $posicion['m']['d']['timag']['disabled']='true';
                            break;
                        case '1':
                            $posicion['m']['d']['texto']['hide']='hide';
                            $posicion['m']['d']['texto']['disabled']='true';
                            $posicion['m']['d']['timag']['hide']='';
                            $posicion['m']['d']['timag']['disabled']='';
                            break;
                        case '3':
                            $posicion['m']['d']['texto']['hide']='';
                            $posicion['m']['d']['texto']['disabled']='';
                            $posicion['m']['d']['timag']['hide']='';
                            $posicion['m']['d']['timag']['disabled']='';
                            break;
                        default:
                            $posicion['m']['d']['texto']['hide']='hide';
                            $posicion['m']['d']['texto']['disabled']='true';
                            $posicion['m']['d']['timag']['hide']='hide';
                            $posicion['m']['d']['timag']['disabled']='true';
                            break;
                    }
                    switch ($pedidos['pedid_mtide']) {
                        case '5':
                            $posicion['m']['d']['image']['hide']='';
                            $posicion['m']['d']['image']['disabled']='';
                            break;
                        default:
                            $posicion['m']['d']['image']['hide']='hide';
                            $posicion['m']['d']['image']['disabled']='disabled';
                            break;
                    }
                    ?>
                <div class="row">
                    <div id="m-d">
                        <div id="m-d-infor" class=" <?php echo $posicion['m']['d']['infor']['hide']; ?> col-sm-4">        
                            <div class="form-group">
                                <label>Información a Bordar Derecha *</label>
                                <?php             
                                $data=array('1'=>'IMAGEN', '2'=>'TEXTO', '3'=>'AMBOS');
                                echo CHtml::dropDownList('mibde', $pedidos['pedid_mibde'], $data, array('class' => 'form-control', 'placeholder' => "Texto a Bordar Derecha",'prompt'=>'Seleccione...','disabled'=>$posicion['m']['d']['infor']['disabled'])); ?>
                            </div>
                    
                        </div>
                        <div id="m-d-texto" class="<?php echo $posicion['m']['d']['texto']['hide']; ?> col-sm-4">        
                            <div class="form-group">
                                <label>Texto a Bordar Derecha *</label>
                                <?php 
                                    echo CHtml::textField('mtbde', $pedidos['pedid_mtbde'], array('class' => 'form-control', 'placeholder' => "Texto a Bordar Derecha",'prompt'=>'Seleccione...','disabled'=>$posicion['m']['d']['texto']['disabled'])); ?>
                            </div>
                        </div>
                        <div id="m-d-timag" class="<?php echo $posicion['m']['d']['timag']['hide']; ?> col-sm-4">        
                                <div class="form-group">
                                    <label>Tipo de Imagen Derecha *</label>
                                    <?php 
                                        $sql="SELECT * FROM pedido_pedidos_imagen_tipo WHERE ptima_codig <>'4'";
                                        $conexion = Yii::app()->db;
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'ptima_codig','ptima_descr');
                                        echo CHtml::dropDownList('mtide', $pedidos['pedid_mtide'], $data,array('class' => 'form-control', 'placeholder' => "Texto a Bordar Derecha",'prompt'=>'Seleccione...','disabled'=>$posicion['m']['d']['timag']['disabled'])); ?>
                                </div>
                            </div>  
                        <div id="m-d-image" class="<?php echo $posicion['m']['d']['image']['hide']; ?> col-sm-12">        
                            <div class="numero5 form-group">
                                <label>Imagen a Bordar Derecha</label>
                                <div  class="input-group image-preview" data-placement="top" >  

                                    <img id="dynamic">
                                    <!-- image-preview-filename input [CUT FROM HERE]-->
                                    <?php 
                                        $sql="SELECT * FROM pedido_pedidos_imagen WHERE pedid_codig ='".$c."' and pimag_orden=5";
                                        $result=$conexion->createCommand($sql)->queryRow();
                                        $ruta=$result['pimag_ruta'];
                                        
                                        $nombre=explode('/', $ruta);
                                        $nombre=end($nombre);
                                        if($nombre){
                                            $opcional['5']['hide']='';
                                            $opcional['5']['show']='true';
                                        }else{
                                            $opcional['5']['hide']='hide';
                                            $opcional['5']['show']='false';
                                        }
                                    ?>
                                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="true" value="<?php echo $nombre ?>" > <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-btn">
                                        <!-- image-preview-clear button -->
                                        <button type="button" class="btn btn-default image-preview-clear <?php echo $opcional['5']['hide'] ?>" >
                                            <span class="fa fa-times"></span> Limpiar
                                        </button>
                                        <!-- image-preview-input -->
                                        <div class="btn btn-default image-preview-input">
                                            <span class="fa fa-folder-open"></span>
                                            <span class="numero5 image-preview-input-title">Buscar</span>
                                            <input type="file" id="mride" name="mride" <?php echo $posicion['m']['d']['image']['disabled']; ?>/> <!-- rename it -->
                                        </div>
                                    </span>
                               
                                </div> 
                            </div> 
                            <div class="hide" id="popover-numero-1">
                                <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$ruta ?>" class="img-responsive">
                            </div>
                            <?php
                                if($opcional['5']['show']=='true') { 
                            ?>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    var closebtn = $('<button/>', {
                                        type: "button",
                                        text: 'x',
                                        id: 'close-preview',
                                        style: 'font-size: initial;',
                                    });
                                    closebtn.attr("class","close pull-right");
                                    $('.numero5 .image-preview').popover({
                                      html: true,
                                      title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
                                      content: function() {
                                        return $('#popover-numero-1').html();
                                      },
                                      trigger: 'manual'
                                    });

                                    $(".numero5 .image-preview").popover("show");
                                });
                            </script>
                            <?php
                                }
                            ?>
                        </div> 
                    </div>
                    <?php
                    
                    switch ($pedidos['pedid_mibiz']) {
                        case '2':
                            $posicion['m']['i']['texto']['hide']='';
                            $posicion['m']['i']['texto']['disabled']='';
                            $posicion['m']['i']['timag']['hide']='hide';
                            $posicion['m']['i']['timag']['disabled']='true';
                            break;
                        case '1':
                            $posicion['m']['i']['texto']['hide']='hide';
                            $posicion['m']['i']['texto']['disabled']='true';
                            $posicion['m']['i']['timag']['hide']='';
                            $posicion['m']['i']['timag']['disabled']='';
                            break;
                        case '3':
                            $posicion['m']['i']['texto']['hide']='';
                            $posicion['m']['i']['texto']['disabled']='';
                            $posicion['m']['i']['timag']['hide']='';
                            $posicion['m']['i']['timag']['disabled']='';
                            break;
                        default:
                            $posicion['m']['i']['texto']['hide']='hide';
                            $posicion['m']['i']['texto']['disabled']='true';
                            $posicion['m']['i']['timag']['hide']='hide';
                            $posicion['m']['i']['timag']['disabled']='true';
                            break;
                    }
                    switch ($pedidos['pedid_mtiiz']) {
                        case '5':
                            $posicion['m']['i']['image']['hide']='';
                            $posicion['m']['i']['image']['disabled']='';
                            break;
                        default:
                            $posicion['m']['i']['image']['hide']='hide';
                            $posicion['m']['i']['image']['disabled']='disabled';
                            break;
                    }
                    ?>
                    <div id="m-i">
                        <div id="m-i-infor" class="<?php echo $posicion['m']['i']['infor']['hide']; ?> col-sm-4">        
                            <div class="form-group">
                                <label>Información a Bordar Izquierda *</label>
                                <?php             
                                $data=array('1'=>'IMAGEN', '2'=>'TEXTO', '3'=>'AMBOS');
                                echo CHtml::dropDownList('mibiz', $pedidos['pedid_mibiz'], $data, array('class' => 'form-control', 'placeholder' => "Información a Bordar Izquierda",'prompt'=>'Seleccione...','disabled'=>$posicion['m']['i']['infor']['disabled'])); ?>
                            </div>
                    
                        </div>
                        <div id="m-i-texto" class="<?php echo $posicion['m']['i']['texto']['hide']; ?> col-sm-4">        
                            <div class="form-group">
                                <label>Texto a Bordar Izquierda *</label>
                                <?php 
                                    echo CHtml::textField('mtbiz', $pedidos['pedid_mtbiz'], array('class' => 'form-control', 'placeholder' => "Texto a Bordar Izquierda",'prompt'=>'Seleccione...','disabled'=>$posicion['m']['i']['texto']['disabled'] )); ?>
                            </div>
                        </div>
                        <div id="m-i-timag" class="<?php echo $posicion['m']['i']['timag']['hide']; ?> col-sm-4">        
                                <div class="form-group">
                                    <label>Tipo de Imagen Izquierda *</label>
                                    <?php 
                                        $sql="SELECT * FROM pedido_pedidos_imagen_tipo WHERE ptima_codig <>'4'";
                                        $conexion = Yii::app()->db;
                                        $result=$conexion->createCommand($sql)->queryAll();
                                        $data=CHtml::listData($result,'ptima_codig','ptima_descr');
                                        echo CHtml::dropDownList('mtiiz', $pedidos['pedid_mtiiz'], $data,array('class' => 'form-control', 'placeholder' => "Texto a Bordar Izquierda",'prompt'=>'Seleccione...','disabled'=>$posicion['m']['i']['timag']['disabled'] )); ?>
                                </div>
                            </div>  
                        <div id="m-i-image" class=" <?php echo $posicion['m']['i']['image']['hide']; ?> col-sm-12">        
                            <div class="numero6 form-group">
                                <label>Imagen a Bordar Izquierda</label>
                                <div  class="input-group image-preview" data-placement="top" >  

                                    <img id="dynamic">
                                    <!-- image-preview-filename input [CUT FROM HERE]-->
                                    <?php 
                                        $sql="SELECT * FROM pedido_pedidos_imagen WHERE pedid_codig ='".$c."' and pimag_orden=6";
                                        $result=$conexion->createCommand($sql)->queryRow();
                                        $ruta=$result['pimag_ruta'];
                                        $nombre=explode('/', $ruta);
                                        $nombre=end($nombre);
                                        if($nombre){
                                            $opcional['6']['hide']='';
                                            $opcional['6']['show']='true';
                                        }else{
                                            $opcional['6']['hide']='hide';
                                            $opcional['6']['show']='false';
                                        }
                                    ?>
                                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="true" value="<?php echo $nombre ?>" > <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-btn">
                                        <!-- image-preview-clear button -->
                                        <button type="button" class="btn btn-default image-preview-clear <?php echo $opcional['6']['hide'] ?>" >
                                            <span class="fa fa-times"></span> Limpiar
                                        </button>
                                        <!-- image-preview-input -->
                                        <div class="btn btn-default image-preview-input">
                                            <span class="fa fa-folder-open"></span>
                                            <span class="numero6 image-preview-input-title">Buscar</span>
                                            <input type="file" id="mriiz" name="mriiz" <?php echo $posicion['m']['i']['image']['disabled']?>/> <!-- rename it -->
                                        </div>
                                    </span>
                               
                                </div> 
                            </div> 
                            <div class="hide" id="popover-numero-6">
                                <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$ruta ?>" class="img-responsive">
                            </div>
                            <?php
                                if($opcional['6']['show']=='true') { 
                            ?>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    var closebtn = $('<button/>', {
                                        type: "button",
                                        text: 'x',
                                        id: 'close-preview',
                                        style: 'font-size: initial;',
                                    });
                                    closebtn.attr("class","close pull-right");
                                    $('.numero6 .image-preview').popover({
                                      html: true,
                                      title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
                                      content: function() {
                                        return $('#popover-numero-6').html();
                                      },
                                      trigger: 'manual'
                                    });

                                    $(".numero6 .image-preview").popover("show");
                                });
                            </script>
                            <?php
                                }
                            ?>
                        </div> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observaciones</label>
                            <?php 
                                echo CHtml::textArea('mobse', $pedidos['pedid_mobse'], array('class' => 'form-control', 'placeholder' => "Observaciones",'prompt'=>'Seleccione...')); ?>
                                
                               
                        </div>
                    </div>
                    
                </div>
        </div><!-- form -->
    </div>  
</div>
<div class="row">                    
    <div class="panel panel-default" >
        <div class="panel-body">
            <div class="row controls">
                <div class="col-sm-4     ">
                    <a href="modificar?p=<?php echo $p?>&c=<?php echo $c?>&s=1" type="reset" class="btn-block btn btn-default">Volver </a>
                </div>
                <div class="col-sm-4 ">
                        <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                    </div>
                <div class="col-sm-4 ">
                    <button id="guardar" type="button" class="btn-block btn btn-info">Siguiente  </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hide" id="popover-img">
    <?php
        $sql="SELECT * FROM pedido_fuente where fuent_codig = '".$pedidos['pedid_npfue']."'";
        $fuente=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$fuente['fuent_ruta'] ?>" class="img-responsive"></div>
<div class="hide" id="popover-img-2">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_npcol']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive">
</div>
<div class="hide" id="popover-img-3">
    <?php
        $sql="SELECT * FROM pedido_fuente where fuent_codig = '".$pedidos['pedid_aefue']."'";
        $fuente=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$fuente['fuent_ruta'] ?>" class="img-responsive"></div>
<div class="hide" id="popover-img-4">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_mcolo']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive">
</div>
<div class="hide" id="popover-img-5">
    <?php
        $sql="SELECT * FROM pedido_fuente where fuent_codig = '".$pedidos['pedid_mfuen']."'";
        $fuente=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$fuente['fuent_ruta'] ?>" class="img-responsive"></div>
<div class="hide" id="popover-img-6">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_pcolo']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive">
</div>
<div class="hide" id="popover-img-7">
    <?php
        $sql="SELECT * FROM pedido_fuente where fuent_codig = '".$pedidos['pedid_gfuen']."'";
        $fuente=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$fuente['fuent_ruta'] ?>" class="img-responsive"></div>
<div class="hide" id="popover-img-8">
    <?php
        $sql="SELECT * FROM pedido_modelo_color where mcolo_codig = '".$pedidos['pedid_gcolo']."'";
        $color=$conexion->createCommand($sql)->queryRow();
        ?>
    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$color['mcolo_ruta'] ?>" class="img-responsive">
</div>
</form>
<script>
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                /*npfue: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fuente" es obligatorio',
                        }
                    }
                },
                npcol: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color" es obligatorio',
                        }
                    }
                },*/
                nppos: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Posición" es obligatorio',
                        }
                    }
                },
                /*aefue: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fuente" es obligatorio',
                        }
                    }
                },
                aecol: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color" es obligatorio',
                        }
                    }
                },*/
                aepos: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Posición" es obligatorio',
                        }
                    }
                },
                pposi: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Posición" es obligatorio',
                        }
                    }
                },
                ptder: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar Derecha" es obligatorio',
                        }
                    }
                },
                ptizq: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar Izquierda" es obligatorio',
                        }
                    }
                },
                /*pfuen: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fuente" es obligatorio',
                        }
                    }
                },*/
                pcolo: {
                    validators: {
                        /*notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color" es obligatorio',
                        },*/
                        different: {
                            field: 'ccuer',
                            message: 'Estimado(a) Usuario(a) el campo "Color" Debe ser diferente al "Color del Cuerpo" debido a el texto no se va a notar'
                        }
                    }

                },
                pibiz: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Información a Bordar Izquierda" es obligatorio',
                        }
                    }

                },
                ptbiz: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar Izquierda" es obligatorio',
                        }
                    }

                },
                ptiiz: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Imagen a Bordar Izquierda',
                        }
                    }

                },
                /*priiz: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Imagen a Bordar Izquierda" es obligatorio',
                        }
                    }

                },*/
                pibde: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Información a Bordar Derecha" es obligatorio',
                        }
                    }

                },
                ptbde: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar Derecha" es obligatorio',
                        }
                    }

                },
                ptide: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Imagen a Bordar Derecha" es obligatorio',
                        }
                    }

                },
                /*pride: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Imagen a Bordar Derecha" es obligatorio',
                        }
                    }

                },*/
                gibiz: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Información a Bordar Izquierda" es obligatorio',
                        }
                    }

                },
                gtbiz: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar Izquierda" es obligatorio',
                        }
                    }

                },
                gtiiz: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Imagen a Bordar Izquierda',
                        }
                    }

                },
                /*griiz: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Imagen a Bordar Izquierda" es obligatorio',
                        }
                    }

                },*/
                gibde: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Información a Bordar Derecha" es obligatorio',
                        }
                    }

                },
                gtbde: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar Derecha" es obligatorio',
                        }
                    }

                },
                gtide: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Imagen a Bordar Derecha" es obligatorio',
                        }
                    }

                },
                /*
                gride: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Imagen a Bordar Derecha" es obligatorio',
                        }
                    }

                },*/
                /*gposi: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Posición" es obligatorio',
                        }
                    }
                },
                gfuen: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fuente" es obligatorio',
                        }
                    }
                },*/
                gcolo: {
                    validators: {
                        /*notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color" es obligatorio',
                        },*/
                        different: {
                            field: 'ccuer',
                            message: 'Estimado(a) Usuario(a) el campo "Color" Debe ser diferente al "Color del Cuerpo" debido a el texto no se va a notar'
                        }
                    }
                },
                /*mposi: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Posición" es obligatorio',
                        }
                    }
                },
                mtder: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar Derecha" es obligatorio',
                        }
                    }
                },
                mtizq: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar Izquierda" es obligatorio',
                        }
                    }
                },
                mfuen: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fuente" es obligatorio',
                        }
                    }
                },*/
                mtder: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar Derecha" es obligatorio',
                        }
                    }
                },
                mtizq: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar Izquierda" es obligatorio',
                        }
                    }
                },
                /*mfuen: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fuente" es obligatorio',
                        }
                    }
                },*/
                mcolo: {
                    validators: {
                        /*notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color" es obligatorio',
                        },*/
                        different: {
                            field: 'ccuer',
                            message: 'Estimado(a) Usuario(a) el campo "Color" Debe ser diferente al "Color del Cuerpo" debido a el texto no se va a notar'
                        }
                    }

                },
                mibiz: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Información a Bordar Izquierda" es obligatorio',
                        }
                    }

                },
                mtbiz: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar Izquierda" es obligatorio',
                        }
                    }

                },
                mtiiz: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Imagen a Bordar Izquierda',
                        }
                    }

                },
                /*mriiz: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Imagen a Bordar Izquierda" es obligatorio',
                        }
                    }

                },*/
                mibde: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Información a Bordar Derecha" es obligatorio',
                        }
                    }

                },
                mtbde: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Texto a Bordar Derecha" es obligatorio',
                        }
                    }

                },
                mtide: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Imagen a Bordar Derecha" es obligatorio',
                        }
                    }

                },
                /*mride: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Imagen a Bordar Derecha" es obligatorio',
                        }
                    }

                },*/


            }
        });
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        var data = new FormData(jQuery('form')[0]);
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                url: 'paso2',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        /*swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){*/
                            window.open('modificar?c=<?php echo $c; ?>&s=3', '_parent');
                        /*});*/
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>
<script type="text/javascript">
    $('#plima').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '1':
                jQuery("#opcional-1").removeClass('hide');
                jQuery("#prima").removeAttr('disabled');
                jQuery("#opcional-2").addClass('hide');
                jQuery("#ptbor").attr('disabled','true');
                break;
            case '2':
                jQuery("#opcional-1").addClass('hide');
                jQuery("#prima").attr('disabled','true');
                jQuery("#opcional-2").removeClass('hide');
                jQuery("#ptbor").removeAttr('disabled');
                break;
            default:
                jQuery("#opcional-1").addClass('hide');
                jQuery("#prima").attr('disabled','true');
                jQuery("#opcional-2").addClass('hide');
                jQuery("#ptbor").attr('disabled','true');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#glima').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
             case '1':
                jQuery("#opcional-3").removeClass('hide');
                jQuery("#grima").removeAttr('disabled');
                jQuery("#opcional-4").addClass('hide');
                jQuery("#gtbor").attr('disabled','true');
                break;
            case '2':
                jQuery("#opcional-3").addClass('hide');
                jQuery("#grima").attr('disabled','true');
                jQuery("#opcional-4").removeClass('hide');
                jQuery("#gtbor").removeAttr('disabled');
                break;
            default:
                jQuery("#opcional-3").addClass('hide');
                jQuery("#grima").attr('disabled','true');
                jQuery("#opcional-4").addClass('hide');
                jQuery("#gtbor").attr('disabled','true');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#iopci').change(function () {
        var iopci = $(this).val();
        $.ajax({
            'type':'POST',
            'data':{'tmode':tmode.value},
            'url':'<?php echo CController::createUrl('funciones/PedidosColorOpcional'); ?>',
            'cache':false,
            'success':function(html){
                switch(iopci) {
                    case '1':
                        
                        jQuery("#opcional-1").removeClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#iclin").html(html);
                        jQuery("#iccie").html('');
                        jQuery("#icviv").html('');
                        jQuery("#ideta").removeAttr('disabled');
                        jQuery("#iclin").removeAttr('disabled');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").attr('disabled','true');

                        break;
                    case '2':
                        jQuery("#opcional-2").removeClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#iclin").html('');
                        jQuery("#iccie").html(html);
                        jQuery("#icviv").html('');
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").removeAttr('disabled');
                        jQuery("#icviv").attr('disabled','true');
                        break;
                    case '3':
                        jQuery("#opcional-3").removeClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#iclin").html('');
                        jQuery("#iccie").html('');
                        jQuery("#icviv").html(html);
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").removeAttr('disabled');
                        break;
                    default:
                        jQuery("#opcional-3").addClass('hide');
                        jQuery("#opcional-2").addClass('hide');
                        jQuery("#opcional-1").addClass('hide');
                        jQuery("#iclin").html(html);
                        jQuery("#iccie").html(html);
                        jQuery("#icviv").html(html);
                        jQuery("#ideta").attr('disabled','true');
                        jQuery("#iclin").attr('disabled','true');
                        jQuery("#iccie").attr('disabled','true');
                        jQuery("#icviv").attr('disabled','true');
                        break;
                }
                
            }
        });
    });
</script>
<script type="text/javascript">
    $('#ideta').change(function () {
        var ideta = $(this).val();
        switch(ideta) {
            case '1':
                jQuery("#iclin").removeAttr('disabled');
                break;
            default:
                jQuery("#iclin").attr('disabled','true');
                break;
        }
                
            
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-1').popover({
          html: true,
          content: function() {
            return $('#popover-img').html();
          },
          trigger: 'hover'
        });
});
$('#pfuen').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerFuente'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-2').popover({
          html: true,
          content: function() {
            return $('#popover-img-2').html();
          },
          trigger: 'hover'
        });
});
$('#pcolo').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-2").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-7').popover({
          html: true,
          content: function() {
            return $('#popover-img-7').html();
          },
          trigger: 'hover'
        });
});
$('#gfuen').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerFuente'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-7").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-8').popover({
          html: true,
          content: function() {
            return $('#popover-img-8').html();
          },
          trigger: 'hover'
        });
});
$('#gcolo').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-8").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-4').popover({
          html: true,
          content: function() {
            return $('#popover-img-4').html();
          },
          trigger: 'hover'
        });
});
$('#mfuen').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerFuente'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-4").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-5').popover({
          html: true,
          content: function() {
            return $('#popover-img-5').html();
          },
          trigger: 'hover'
        });
});
$('#mcolo').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-5").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script>

    $(document).on('click', '#close-preview', function () {
        $('.numero1 .image-preview').popover('hide');
        // Hover befor close the preview
        $('.numero1 .image-preview').hover(
            function () {
                $('.image-preview').popover('hide');
            },
            function () {
                $('.image-preview').popover('hide');
            }
        );
    });
    $(function () {
        // Create the close button
        var closebtn = $('<button/>', {
            type: "button",
            text: 'x',
            id: 'close-preview',
            style: 'font-size: initial;',
        });
        closebtn.attr("class","close pull-right");
        $('.numero1 .image-preview').popover({
            trigger:'manual',
            html:true,
            title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
            content: "No hay imagen",
            placement:'top'
        });



        
        // Clear event
        $('.numero1 .image-preview-clear').click(function () {
            $('.numero1 .image-preview').attr("data-content", "").popover('hide');
            $('.numero1 .image-preview-filename').val("");
            $('.numero1 .image-preview-clear').hide();
            $('.numero1 .image-preview-input input:file').val("");
            $(".numero1 .image-preview-input-title").text("Buscar");
        });
        // Create the preview image
        $(".numero1 .image-preview-input input:file").change(function () {
            var img = $('<img/>', {
                id: 'dynamic',
                width: 250,
                height: 200
            });
            var file = this.files[0];
            var reader = new FileReader();
            // Set preview image into the popover data-content
            reader.onload = function (e) {
                $(".numero1 .image-preview-input-title").text("Cambiar");
                $(".numero1 .image-preview-clear").show();
                $(".numero1 .image-preview-filename").val(file.name);
                img.attr('src', e.target.result);
                $(".numero1 .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
                }
            reader.readAsDataURL(file);
        });
    });
</script>
<script>

    $(document).on('click', '#close-preview', function () {
        $('.numero2 .image-preview').popover('hide');
        // Hover befor close the preview
        $('.numero2 .image-preview').hover(
            function () {
                $('.image-preview').popover('hide');
            },
            function () {
                $('.image-preview').popover('hide');
            }
        );
    });
    $(function () {
        // Create the close button
        var closebtn = $('<button/>', {
            type: "button",
            text: 'x',
            id: 'close-preview',
            style: 'font-size: initial;',
        });
        closebtn.attr("class","close pull-right");
        $('.numero2 .image-preview').popover({
            trigger:'manual',
            html:true,
            title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
            content: "No hay imagen",
            placement:'top'
        });



        
        // Clear event
        $('.numero2 .image-preview-clear').click(function () {
            $('.numero2 .image-preview').attr("data-content", "").popover('hide');
            $('.numero2 .image-preview-filename').val("");
            $('.numero2 .image-preview-clear').hide();
            $('.numero2 .image-preview-input input:file').val("");
            $(".numero2 .image-preview-input-title").text("Buscar");
        });
        // Create the preview image
        $(".numero2 .image-preview-input input:file").change(function () {
            var img = $('<img/>', {
                id: 'dynamic',
                width: 250,
                height: 200
            });
            var file = this.files[0];
            var reader = new FileReader();
            // Set preview image into the popover data-content
            reader.onload = function (e) {
                $(".numero2 .image-preview-input-title").text("Cambiar");
                $(".numero2 .image-preview-clear").show();
                $(".numero2 .image-preview-filename").val(file.name);
                img.attr('src', e.target.result);
                $(".numero2 .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
                }
            reader.readAsDataURL(file);
        });
    });
</script>
<script>

    $(document).on('click', '#close-preview', function () {
        $('.numero3 .image-preview').popover('hide');
        // Hover befor close the preview
        $('.numero3 .image-preview').hover(
            function () {
                $('.image-preview').popover('hide');
            },
            function () {
                $('.image-preview').popover('hide');
            }
        );
    });
    $(function () {
        // Create the close button
        var closebtn = $('<button/>', {
            type: "button",
            text: 'x',
            id: 'close-preview',
            style: 'font-size: initial;',
        });
        closebtn.attr("class","close pull-right");
        $('.numero3 .image-preview').popover({
            trigger:'manual',
            html:true,
            title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
            content: "No hay imagen",
            placement:'top'
        });



        
        // Clear event
        $('.numero3 .image-preview-clear').click(function () {
            $('.numero3 .image-preview').attr("data-content", "").popover('hide');
            $('.numero3 .image-preview-filename').val("");
            $('.numero3 .image-preview-clear').hide();
            $('.numero3 .image-preview-input input:file').val("");
            $(".numero3 .image-preview-input-title").text("Buscar");
        });
        // Create the preview image
        $(".numero3 .image-preview-input input:file").change(function () {
            var img = $('<img/>', {
                id: 'dynamic',
                width: 250,
                height: 200
            });
            var file = this.files[0];
            var reader = new FileReader();
            // Set preview image into the popover data-content
            reader.onload = function (e) {
                $(".numero3 .image-preview-input-title").text("Cambiar");
                $(".numero3 .image-preview-clear").show();
                $(".numero3 .image-preview-filename").val(file.name);
                img.attr('src', e.target.result);
                $(".numero3 .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
                }
            reader.readAsDataURL(file);
        });
    });
</script>
<script>

    $(document).on('click', '#close-preview', function () {
        $('.numero4 .image-preview').popover('hide');
        // Hover befor close the preview
        $('.numero4 .image-preview').hover(
            function () {
                $('.image-preview').popover('hide');
            },
            function () {
                $('.image-preview').popover('hide');
            }
        );
    });
    $(function () {
        // Create the close button
        var closebtn = $('<button/>', {
            type: "button",
            text: 'x',
            id: 'close-preview',
            style: 'font-size: initial;',
        });
        closebtn.attr("class","close pull-right");
        $('.numero4 .image-preview').popover({
            trigger:'manual',
            html:true,
            title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
            content: "No hay imagen",
            placement:'top'
        });



        
        // Clear event
        $('.numero4 .image-preview-clear').click(function () {
            $('.numero4 .image-preview').attr("data-content", "").popover('hide');
            $('.numero4 .image-preview-filename').val("");
            $('.numero4 .image-preview-clear').hide();
            $('.numero4 .image-preview-input input:file').val("");
            $(".numero4 .image-preview-input-title").text("Buscar");
        });
        // Create the preview image
        $(".numero4 .image-preview-input input:file").change(function () {
            var img = $('<img/>', {
                id: 'dynamic',
                width: 250,
                height: 200
            });
            var file = this.files[0];
            var reader = new FileReader();
            // Set preview image into the popover data-content
            reader.onload = function (e) {
                $(".numero4 .image-preview-input-title").text("Cambiar");
                $(".numero4 .image-preview-clear").show();
                $(".numero4 .image-preview-filename").val(file.name);
                img.attr('src', e.target.result);
                $(".numero4 .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
                }
            reader.readAsDataURL(file);
        });
    });
</script>
<script>

    $(document).on('click', '#close-preview', function () {
        $('.numero5 .image-preview').popover('hide');
        // Hover befor close the preview
        $('.numero5 .image-preview').hover(
            function () {
                $('.image-preview').popover('hide');
            },
            function () {
                $('.image-preview').popover('hide');
            }
        );
    });
    $(function () {
        // Create the close button
        var closebtn = $('<button/>', {
            type: "button",
            text: 'x',
            id: 'close-preview',
            style: 'font-size: initial;',
        });
        closebtn.attr("class","close pull-right");
        $('.numero5 .image-preview').popover({
            trigger:'manual',
            html:true,
            title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
            content: "No hay imagen",
            placement:'top'
        });



        
        // Clear event
        $('.numero5 .image-preview-clear').click(function () {
            $('.numero5 .image-preview').attr("data-content", "").popover('hide');
            $('.numero5 .image-preview-filename').val("");
            $('.numero5 .image-preview-clear').hide();
            $('.numero5 .image-preview-input input:file').val("");
            $(".numero5 .image-preview-input-title").text("Buscar");
        });
        // Create the preview image
        $(".numero5 .image-preview-input input:file").change(function () {
            var img = $('<img/>', {
                id: 'dynamic',
                width: 250,
                height: 200
            });
            var file = this.files[0];
            var reader = new FileReader();
            // Set preview image into the popover data-content
            reader.onload = function (e) {
                $(".numero5 .image-preview-input-title").text("Cambiar");
                $(".numero5 .image-preview-clear").show();
                $(".numero5 .image-preview-filename").val(file.name);
                img.attr('src', e.target.result);
                $(".numero5 .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
                }
            reader.readAsDataURL(file);
        });
    });
</script>
<script>

    $(document).on('click', '#close-preview', function () {
        $('.numero6 .image-preview').popover('hide');
        // Hover befor close the preview
        $('.numero6 .image-preview').hover(
            function () {
                $('.image-preview').popover('hide');
            },
            function () {
                $('.image-preview').popover('hide');
            }
        );
    });
    $(function () {
        // Create the close button
        var closebtn = $('<button/>', {
            type: "button",
            text: 'x',
            id: 'close-preview',
            style: 'font-size: initial;',
        });
        closebtn.attr("class","close pull-right");
        $('.numero6 .image-preview').popover({
            trigger:'manual',
            html:true,
            title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
            content: "No hay imagen",
            placement:'top'
        });



        
        // Clear event
        $('.numero6 .image-preview-clear').click(function () {
            $('.numero6 .image-preview').attr("data-content", "").popover('hide');
            $('.numero6 .image-preview-filename').val("");
            $('.numero6 .image-preview-clear').hide();
            $('.numero6 .image-preview-input input:file').val("");
            $(".numero6 .image-preview-input-title").text("Buscar");
        });
        // Create the preview image
        $(".numero6 .image-preview-input input:file").change(function () {
            var img = $('<img/>', {
                id: 'dynamic',
                width: 250,
                height: 200
            });
            var file = this.files[0];
            var reader = new FileReader();
            // Set preview image into the popover data-content
            reader.onload = function (e) {
                $(".numero6 .image-preview-input-title").text("Cambiar");
                $(".numero6 .image-preview-clear").show();
                $(".numero6 .image-preview-filename").val(file.name);
                img.attr('src', e.target.result);
                $(".numero6 .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
                }
            reader.readAsDataURL(file);
        });
    });
</script>
<script type="text/javascript">
    function cambiarAtributo(id,campo,p_estados) {
        if(p_estados=='1') {
            jQuery(id).removeClass('hide');
            if(campo!=''){
                jQuery(campo).removeAttr('disabled');
                
            }
        }else{
            jQuery(id).addClass('hide');
            if(campo!=''){
                jQuery(campo).attr('disabled','true');
                jQuery(campo).val(''); 
            }
        }
    }
</script>
<script type="text/javascript">
    $('#pposi').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '1':
                cambiarAtributo('#p-d-infor','#pibde','1');
                cambiarAtributo('#p-i-infor','#pibiz','2');
                cambiarAtributo('#p-i-texto','#ptbiz','2');
                cambiarAtributo('#p-i-timag','#ptiiz','2');
                cambiarAtributo('#p-i-image','#priiz','2');
                break;
            case '2':
                cambiarAtributo('#p-i-infor','#pibiz','1');
                cambiarAtributo('#p-d-infor','#pibde','2');
                cambiarAtributo('#p-d-texto','#ptbde','2');
                cambiarAtributo('#p-d-timag','#ptide','2');
                cambiarAtributo('#p-d-image','#pride','2');
                break;
            case '3':
                cambiarAtributo('#p-d-infor','#pibde','1');
                cambiarAtributo('#p-i-infor','#pibiz','1');
                break;
            default:
                cambiarAtributo('#p-i-infor','#pibiz','2');
                cambiarAtributo('#p-i-texto','#ptbiz','2');
                cambiarAtributo('#p-i-timag','#ptiiz','2');
                cambiarAtributo('#p-i-image','#priiz','2');
                cambiarAtributo('#p-d-infor','#pibde','2');
                cambiarAtributo('#p-d-texto','#ptbde','2');
                cambiarAtributo('#p-d-timag','#ptide','2');
                cambiarAtributo('#p-d-image','#pride','2');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#pibde').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '1':
                cambiarAtributo('#p-d-timag','#ptide','1');
                cambiarAtributo('#p-d-texto','#ptbde','2');
                break;
            case '2':
                cambiarAtributo('#p-d-texto','#ptbde','1');
                cambiarAtributo('#p-d-timag','#ptide','2');
                cambiarAtributo('#p-d-image','#pride','2');
                break;
            case '3':
                cambiarAtributo('#p-d-texto','#ptbde','1');
                cambiarAtributo('#p-d-timag','#ptide','1');
                break;
            default:
                cambiarAtributo('#p-d-texto','#ptbde','2');
                cambiarAtributo('#p-d-timag','#ptide','2');
                cambiarAtributo('#p-d-image','#pride','2');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#ptide').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '5':
                cambiarAtributo('#p-d-image','#pride','1');
                break;
            default:
                cambiarAtributo('#p-d-image','#pride','2');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#pibiz').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '1':
                cambiarAtributo('#p-i-timag','#ptiiz','1');
                cambiarAtributo('#p-i-texto','#ptbiz','2');
                break;
            case '2':
                cambiarAtributo('#p-i-texto','#ptbiz','1');
                cambiarAtributo('#p-i-timag','#ptiiz','2');
                cambiarAtributo('#p-i-image','#priiz','2');
                break;
            case '3':
                cambiarAtributo('#p-i-texto','#ptbiz','1');
                cambiarAtributo('#p-i-timag','#ptiiz','1');
                break;
            default:
                cambiarAtributo('#p-i-texto','#ptbiz','2');
                cambiarAtributo('#p-i-timag','#ptiiz','2');
                cambiarAtributo('#p-i-image','#priiz','2');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#ptiiz').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '5':
                cambiarAtributo('#p-i-image','#priiz','1');
                break;
            default:
                cambiarAtributo('#p-i-image','#priiz','2');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#gposi').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '1':
                cambiarAtributo('#g-d-infor','#gibde','1');
                cambiarAtributo('#g-i-infor','#gibiz','2');
                cambiarAtributo('#g-i-texto','#gtbiz','2');
                cambiarAtributo('#g-i-timag','#gtiiz','2');
                cambiarAtributo('#g-i-image','#griiz','2');
                break;
            case '2':
                cambiarAtributo('#g-i-infor','#gibiz','1');
                cambiarAtributo('#g-d-infor','#gibde','2');
                cambiarAtributo('#g-d-texto','#gtbde','2');
                cambiarAtributo('#g-d-timag','#gtide','2');
                cambiarAtributo('#g-d-image','#gride','2');
                break;
            case '3':
                cambiarAtributo('#g-d-infor','#gibde','1');
                cambiarAtributo('#g-i-infor','#gibiz','1');
                break;
            default:
                cambiarAtributo('#g-i-infor','#gibiz','2');
                cambiarAtributo('#g-i-texto','#gtbiz','2');
                cambiarAtributo('#g-i-timag','#gtiiz','2');
                cambiarAtributo('#g-i-image','#griiz','2');
                cambiarAtributo('#g-d-infor','#gibde','2');
                cambiarAtributo('#g-d-texto','#gtbde','2');
                cambiarAtributo('#g-d-timag','#gtide','2');
                cambiarAtributo('#g-d-image','#gride','2');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#gibde').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '1':
                cambiarAtributo('#g-d-timag','#gtide','1');
                cambiarAtributo('#g-d-texto','#gtbde','2');
                break;
            case '2':
                cambiarAtributo('#g-d-texto','#gtbde','1');
                cambiarAtributo('#g-d-timag','#gtide','2');
                cambiarAtributo('#g-d-image','#gride','2');
                break;
            case '3':
                cambiarAtributo('#g-d-texto','#gtbde','1');
                cambiarAtributo('#g-d-timag','#gtide','1');
                break;
            default:
                cambiarAtributo('#g-d-texto','#gtbde','2');
                cambiarAtributo('#g-d-timag','#gtide','2');
                cambiarAtributo('#g-d-image','#gride','2');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#gtide').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '5':
                cambiarAtributo('#g-d-image','#gride','1');
                break;
            default:
                cambiarAtributo('#g-d-image','#gride','2');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#gibiz').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '1':
                cambiarAtributo('#g-i-timag','#gtiiz','1');
                cambiarAtributo('#g-i-texto','#gtbiz','2');
                break;
            case '2':
                cambiarAtributo('#g-i-texto','#gtbiz','1');
                cambiarAtributo('#g-i-timag','#gtiiz','2');
                cambiarAtributo('#g-i-image','#griiz','2');
                break;
            case '3':
                cambiarAtributo('#g-i-texto','#gtbiz','1');
                cambiarAtributo('#g-i-timag','#gtiiz','1');
                break;
            default:
                cambiarAtributo('#g-i-texto','#gtbiz','2');
                cambiarAtributo('#g-i-timag','#gtiiz','2');
                cambiarAtributo('#g-i-image','#griiz','2');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#gtiiz').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '5':
                cambiarAtributo('#g-i-image','#griiz','1');
                break;
            default:
                cambiarAtributo('#g-i-image','#griiz','2');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#mposi').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '1':
                cambiarAtributo('#m-d-infor','#mibde','1');
                cambiarAtributo('#m-i-infor','#mibiz','2');
                cambiarAtributo('#m-i-texto','#mtbiz','2');
                cambiarAtributo('#m-i-timag','#mtiiz','2');
                cambiarAtributo('#m-i-image','#mriiz','2');
                break;
            case '2':
                cambiarAtributo('#m-i-infor','#mibiz','1');
                cambiarAtributo('#m-d-infor','#mibde','2');
                cambiarAtributo('#m-d-texto','#mtbde','2');
                cambiarAtributo('#m-d-timag','#mtide','2');
                cambiarAtributo('#m-d-image','#mride','2');
                break;
            case '3':
                cambiarAtributo('#m-d-infor','#mibde','1');
                cambiarAtributo('#m-i-infor','#mibiz','1');
                break;
            default:
                cambiarAtributo('#m-i-infor','#mibiz','2');
                cambiarAtributo('#m-i-texto','#mtbiz','2');
                cambiarAtributo('#m-i-timag','#mtiiz','2');
                cambiarAtributo('#m-i-image','#mriiz','2');
                cambiarAtributo('#m-d-infor','#mibde','2');
                cambiarAtributo('#m-d-texto','#mtbde','2');
                cambiarAtributo('#m-d-timag','#mtide','2');
                cambiarAtributo('#m-d-image','#mride','2');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#mibde').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '1':
                cambiarAtributo('#m-d-timag','#mtide','1');
                cambiarAtributo('#m-d-texto','#mtbde','2');
                break;
            case '2':
                cambiarAtributo('#m-d-texto','#mtbde','1');
                cambiarAtributo('#m-d-timag','#mtide','2');
                cambiarAtributo('#m-d-image','#mride','2');
                break;
            case '3':
                cambiarAtributo('#m-d-texto','#mtbde','1');
                cambiarAtributo('#m-d-timag','#mtide','1');
                break;
            default:
                cambiarAtributo('#m-d-texto','#mtbde','2');
                cambiarAtributo('#m-d-timag','#mtide','2');
                cambiarAtributo('#m-d-image','#mride','2');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#mtide').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '5':
                cambiarAtributo('#m-d-image','#mride','1');
                break;
            default:
                cambiarAtributo('#m-d-image','#mride','2');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#mibiz').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '1':
                cambiarAtributo('#m-i-timag','#mtiiz','1');
                cambiarAtributo('#m-i-texto','#mtbiz','2');
                break;
            case '2':
                cambiarAtributo('#m-i-texto','#mtbiz','1');
                cambiarAtributo('#m-i-timag','#mtiiz','2');
                cambiarAtributo('#m-i-image','#mriiz','2');
                break;
            case '3':
                cambiarAtributo('#m-i-texto','#mtbiz','1');
                cambiarAtributo('#m-i-timag','#mtiiz','1');
                break;
            default:
                cambiarAtributo('#m-i-texto','#mtbiz','2');
                cambiarAtributo('#m-i-timag','#mtiiz','2');
                cambiarAtributo('#m-i-image','#mriiz','2');
                break;
        }
    });
</script>
<script type="text/javascript">
    $('#mtiiz').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '5':
                cambiarAtributo('#m-i-image','#mriiz','1');
                break;
            default:
                cambiarAtributo('#m-i-image','#mriiz','2');
                break;
        }
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-1').popover({
          html: true,
          content: function() {
            return $('#popover-img').html();
          },
          trigger: 'hover'
        });
});
$('#mfuen').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerFuente'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#img-2').popover({
          html: true,
          content: function() {
            return $('#popover-img-2').html();
          },
          trigger: 'hover'
        });
});
$('#mcolo').change(function () {
        var emoji = $(this).val();
        $.ajax({  
            url:"<?php echo CController::createUrl('funciones/PedidosVerColor'); ?>",  
            method:"POST",  
            async:false,  
            data:{id:emoji},  
            success:function(data){  
                jQuery("#popover-img-2").html(data);  
            }  
        });
        $('[data-toggle=popover]').popover('hide');
    });
</script>
<!--script type="text/javascript">
    $('#gposi').change(function () {
        var opcio = $(this).val();
        switch(opcio) {
            case '1':
                jQuery("#gtder").removeAttr('disabled');
                jQuery("#gtizq").attr('disabled','true');
                break;
            case '2':
                jQuery("#gtizq").removeAttr('disabled');
                jQuery("#gtder").attr('disabled','true');
                break;
            case '3':
                jQuery("#gtder").removeAttr('disabled');
                jQuery("#gtizq").removeAttr('disabled');
                break;
            default:
                jQuery("#gtder").attr('disabled','true');
                jQuery("#gtizq").attr('disabled','true');
                break;
        }
    });
</script-->