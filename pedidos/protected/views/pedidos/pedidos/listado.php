<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Pedidos</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Pedidos</a></li>
            <li class="active">Listado</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

<?php
//Mostrar mensajes del controlador
        foreach (Yii::app()->user->getFlashes() as $key => $message) {
          echo '<div class="alert alert-' . $key . ' alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              ' . $message  .' </div>';          
        }
$connection = Yii::app()->db;
if(Yii::app()->user->id['usuario']['permiso']==1){
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Criterio de Busqueda</h3>
    </div>
    <div class="panel-body" >
        <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'pedidos', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            

        ?>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Número de Pedido</label>
                    <?php echo CHtml::textField('numero', '', array('class' => 'form-control', 'placeholder' => "Nro del Pedido")); ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Estatus</label>
                    <?php 
                        
                        $sql="SELECT * FROM rd_preregistro_estatus";
                        $result=$connection->createCommand($sql)->queryAll();
                        $data=CHtml::listData($result,'epedi_codig','epedi_descr');
                        echo CHtml::dropDownList('epedi', '', $data,array('class' => 'form-control', 'placeholder' => "Descripción", 'prompt'=>'Seleccione...')); ?>
                </div>
            </div>
           
        </div>
        
        <div class="row">
            <div class="col-sm-6">
                <button type="button" id="buscar" class="btn btn-block btn-info">Buscar</button>
            </div>
            <div class="col-sm-6">
                <button type="reset" class="btn btn-block btn-default">Limpiar</button>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div> 
<?php
}
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-sm-8">
                <h3 class="panel-title">Listado</h3>
            </div>
            <div class="col-sm-4">
                <a href="registrar" class="btn btn-block btn-info" >Nuevo Pedido</a>
            </div>
        </div>
        
    </div>
    <div class="panel-body" >
        
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-pedido'>
                <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                #
                            </th>
                            <th>
                                Nro Pedido
                            </th>
                            <th>
                                Usuario
                            </th>
                            <th>
                                Colegio
                            </th>
                            <th>
                                Total a Solicitar
                            </th>
                            <th>
                                Total de Modelos
                            </th>
                            <th>
                                Fecha de Registro
                            </th>
                            <th>
                                Estatus
                            </th>
                            <th width="5%">
                                Consultar
                            </th>
                            <th width="5%">
                                Modificar
                            </th>
                            <th width="5%">
                                Eliminar
                            </th>
                            <th width="5%">
                                Imprimir
                            </th>
                            <?php
                                if(Yii::app()->user->id['usuario']['permiso']==1){
                            ?>
                            <th width="5%">
                                Costos Adicionales
                            </th>
                            <th width="5%">
                                Deducciones
                            </th>
                            <th width="5%">
                                Aprobar
                            </th>
                            <th width="5%">
                                Rechazar
                            </th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        if(Yii::app()->user->id['usuario']['permiso']==1){
                            $sql = "SELECT *, 
                                    (SELECT COUNT(pdeta_codig) FROM pedido_pedidos_detalle e WHERE a.pedid_codig = e.pedid_codig) modelos, 
                                    (SELECT COUNT(pdlis_codig) FROM pedido_pedidos_detalle_listado f WHERE a.pedid_codig = f.pedid_codig) p_personas 
                                FROM pedido_pedidos a 
                                JOIN rd_preregistro_estatus b ON (a.epedi_codig = b.epedi_codig)
                                JOIN seguridad_usuarios c ON (a.usuar_codig = c.usuar_codig)
                                LEFT JOIN colegio d ON (c.coleg_codig = d.coleg_codig)";
                   
                        }else{
                            $sql = "SELECT *, 
                                    (SELECT COUNT(pdeta_codig) FROM pedido_pedidos_detalle e WHERE a.pedid_codig = e.pedid_codig) modelos, 
                                    (SELECT COUNT(pdlis_codig) FROM pedido_pedidos_detalle_listado f WHERE a.pedid_codig = f.pedid_codig) p_personas  FROM pedido_pedidos a 
                                    JOIN rd_preregistro_estatus b ON (a.epedi_codig = b.epedi_codig)
                                    JOIN seguridad_usuarios c ON (a.usuar_codig = c.usuar_codig)
                                    LEFT JOIN colegio d ON (c.coleg_codig = d.coleg_codig)
                                    WHERE a.usuar_codig = '".Yii::app()->user->id['usuario']['codigo']."'";
                             
                        }
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        $j=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                            
                        ?>
                        <tr>
                            <td class="tabla"><?php echo $i ?></td>
                            <td class="tabla"><?php echo $row['pedid_numer'] ?></td>
                            <td class="tabla"><?php echo $row['usuar_login'] ?></td>
                            <td class="tabla"><?php echo $row['coleg_nombr'] ?></td>
                            <td class="tabla"><?php echo $row['p_personas'] ?></td>
                            <td class="tabla"><?php echo $row['modelos'] ?></td>
                            <td class="tabla"><?php echo $row['pedid_fcrea'] ?></td>
                            <td class="tabla"><?php echo $row['epedi_descr'] ?></td>

                            <!--td class="tabla"><a href="../detalle/listado?p=<?php echo $row['pedid_codig'] ?>" class="btn btn-block btn-info"><i class="glyphicon glyphicon-list-alt"></i></a></td-->
                            <td class="tabla"><a href="consultar?c=<?php echo $row['pedid_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-search"></i></a></td>
                            <td class="tabla"><a href="../detalle/listado?p=<?php echo $row['pedid_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-pencil"></i></a></td>
                            <?php 
                                if($row['epedi_codig']=='1'){
                                    ?>
                                    
                                    <td class="tabla"><a href="eliminar?c=<?php echo $row['pedid_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-close"></i></a></td>
                                    <?php
                                }else{
                                    ?>
                                     <td class="tabla"><a href="#" disabled class="disabled btn btn-block btn-info"><i class="fa fa-close"></i></a></td>
                                    <?php
                                }
                            ?>
                            <?php
                                if($row['epedi_codig']>2 or Yii::app()->user->id['usuario']['permiso']==1){
                            ?>
                            <td class="tabla"><a href="../../reportes/pedidos/pdf?c=<?php echo $row['pedid_codig'] ?>" target="_blank" class="btn btn-block btn-info"><i class="fa fa-file-pdf-o"></i></a></td>
                            <?php
                                }else{
                            ?>
                            <td class="tabla"><a href="#" target="_blank" class="disabled btn btn-block btn-info" disabled><i class="fa fa-file-pdf-o"></i></a></td>
                            <?php
                                }
                                if(Yii::app()->user->id['usuario']['permiso']==1){
                            ?>
                            <td class="tabla"><a href="costos?c=<?php echo $row['pedid_codig'] ?>" target="_blank" class="btn btn-block btn-info"><i class="fa fa-money"></i></a></td>
                            <td class="tabla"><a href="deduccion?c=<?php echo $row['pedid_codig'] ?>" target="_blank" class="btn btn-block btn-info"><i class="fa fa-money"></i></a></td>
                            <td class="tabla"><a href="aprobar?c=<?php echo $row['pedid_codig'] ?>" target="_blank" class="btn btn-block btn-success"><i class="fa fa-check"></i></a></td>
                            <td class="tabla"><a href="rechazar?c=<?php echo $row['pedid_codig'] ?>" target="_blank" class="btn btn-block btn-danger"><i class="fa fa-close"></i></a></td>
                            <?php } ?>
                        </tr>
                        <?php
                            }   
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>  
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#buscar').click(function () {
        var formData = new FormData($("#pedidos")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-pedido').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>
