<?php
//Mostrar Errores del controlador
foreach (Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="alert alert-' . $key . '">' . $message . "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> </div>\n";
}
?> 
<div id="loginbox" style="margin-top:50px;" class="mainbox col-md-12">           
    <style>
        

        .image-preview-input {
            position: relative;
            overflow: hidden;
            margin: 0px;    
            color: #333;
            background-color: #fff;
            border-color: #ccc;    
        }
        .image-preview-input input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            padding: 0;
            font-size: 20px;
            cursor: pointer;
            opacity: 0;
            filter: alpha(opacity=0);
        }
        .image-preview-input-title {
            margin-left:2px;
        }
    </style>  
    <h1 class="titulo">Recarga</h1>
    <div class="panel panel-danger" >


        <div style="padding-top:30px" class="panel-body" >

            <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>


            <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'subir-p_personas', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            ?>
            <div class="col-sm-6 hide">
                    <div class="form-group">  
                    
                    <?php
                    
                      echo CHtml::hiddenField('uvenc_codig',  $param['uvenc_codigs'], array('prompt' => 'Seleccione...', "class" => "form-control ")); 

                    echo CHtml::hiddenField('o', $_REQUEST['o'], array('prompt' => 'Seleccione...', "class" => "form-control "));

                    ?>

                    </div>
                </div>   
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">  
                    <!-- image-preview-filename input [CUT FROM HERE]-->

                    <label for="clvarea">Forma de Pago</label>
                    <?php
                        $sql="SELECT * FROM admin_tgenerica 
                        WHERE tgene_tipod = 'FORPA' and tgene_codig not in (35,38)";
                        $conexion=Yii::app()->db;
                        $formapago=$conexion->createCommand($sql)->query();
                        $datos=CHtml::listData($formapago,'tgene_codig','tgene_descr');

                          echo CHtml::dropDownList('tgene_forpa', $param['tgene_forpas'], $datos, array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                        ?>

                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">  
                    <!-- image-preview-filename input [CUT FROM HERE]-->

                    <label for="clvarea">Monto</label>
                    <?php
                    
                      echo CHtml::textField('uvenc_monto', $param['uvenc_montos'], array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                    ?>

                    </div>
                </div>
                 
               
            </div>
                
            
            
           

            <div class="row">
                <!-- Button -->
                <div class="controls">
                    <div class="col-sm-6">
                        <button id="subir" name="subir" value="subir" type="button" class="btn-block btn btn-uven"><span class="fa fa-upload"></span> Guardar  </button>

                    </div>
                    <div class="col-sm-6">
                        <button id="subir" name="limpiar" value="limpiar" type="reset" class="btn-block btn btn-default"><span class="fa fa-eraser"></span> Limpiar  </button>

                    </div>
                </div>
            </div>              

            <?php $this->endWidget(); ?>

        </div><!-- form -->
    </div>  
</div>

                   
<script type="text/javascript">
    //$('#tarif_kiloms').mask('999.999.99');
    

    $(document).ready(function () {

        $('#subir-p_personas').bootstrapValidator({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                banco_numer: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        regexp: {
                            regexp: /^[0-9{4}\.]+$/,
                             message: 'Escriba un número de 4 dígitos'
                        }

                    }
                },
                banco_descr: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        regexp: {
                            regexp: /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]*$/,
                            message: 'Estimado(a) Usuario(a) el campo "Descripción" Solo puede poseer letras.'
                        }

                    }
                },
                banco_urlur: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        regexp: {
                            regexp: /^[a-zA-Z\.\s]*$/,
                            message: 'Estimado(a) Usuario(a) el campo "URL" Solo puede poseer letras.'
                        }

                    }
                },
            }
        });
    });
    $('#subir').click(function () {
        var formData = new FormData($("#subir-p_personas")[0]);
        $('#subir-p_personas').bootstrapValidator('validate'); //secondary validation using Bootstrap Validator      
        var bootstrapValidator = $('#subir-p_personas').data('bootstrapValidator');
        if (bootstrapValidator.isValid()) {
            $.ajax({
                'dataType': "json",
                'type': 'post',
                'url': 'lista',
                'data': formData,
                'cache': false,
                'contentType': false,
                'processData': false,
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    //alert(response['success']);
                    $("#body").removeClass("loading");
                    if (response['success'] == 'true') {
                        $("#body").removeClass("loading");
                        bootbox.dialog({
                            message: response['msg'],
                            title: "Exito!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                    callback: function(){
                                       window.open('lista', '_parent');
                                    }
                                },
                            }
                        });
                            $('.close').click(function(){
                                 window.open('lista', '_parent');
                             });
                    } else {
                        $("#body").removeClass("loading");
                        bootbox.dialog({
                            message: response['msg'],
                            title: "Información!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                },
                            }
                        });
                        
                        $("#subir").removeAttr('disabled');
                    }

                }
            });
        }
    });
</script>