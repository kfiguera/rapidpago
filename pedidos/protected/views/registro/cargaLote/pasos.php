<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <div class="row line-steps">
                  <div class="col-md-3 column-step <?php echo $ubicacion[0]; ?>">
                    <?php if($ubicacion[0]) { ?>
                    <a href="modificar?c=<?php echo $solicitud['prere_codig']?>&s=1">
                    <?php } ?> 
                        <div class="step-number">1 </div>
                        <div class="step-title">Pre-Registro</div>
                        <div class="step-info">Datos del Comercio</div>
                    <?php if($ubicacion[0]) { ?>
                        </a>
                    <?php } ?> 
                 </div>

                 <div class="col-md-2 column-step <?php echo $ubicacion[1]; ?> ">
                    <?php if($ubicacion[1]) { ?>
                    <a href="modificar?c=<?php echo $solicitud['prere_codig']?>&s=2">
                    <?php } ?> 
                        <div class="step-number">2</div>
                        <div class="step-title">Pre-Registro</div>
                        <div class="step-info">Datos de alifiación y dispositivo</div>
                    <?php if($ubicacion[1]) { ?>
                        </a>
                    <?php } ?>
                 </div>

                 <div class="col-md-2 column-step <?php echo $ubicacion[2]; ?> ">
                    <?php if($ubicacion[2]) { ?>
                    <a href="modificar?c=<?php echo $solicitud['prere_codig']?>&s=3">
                    <?php } ?> 
                        <div class="step-number">3</div>
                        <div class="step-title">Pre-Registro</div>
                        <div class="step-info">Datos de los Representantes Legales</div>
                    <?php if($ubicacion[2]) { ?>
                        </a>
                    <?php } ?>
                 </div>

                 <div class="col-md-2 column-step <?php echo $ubicacion[3]; ?> ">
                    <?php if($ubicacion[3]) { ?>
                    <a href="modificar?c=<?php echo $solicitud['prere_codig']?>&s=4">
                    <?php } ?>
                        <div class="step-number">4</div>
                        <div class="step-title">Pre-Registro</div>
                        <div class="step-info">Datos Adicionales</div>
                    <?php if($ubicacion[3]) { ?>
                        </a>
                    <?php } ?>
                 </div>

                 <div class="col-md-3 column-step <?php echo $ubicacion[4]; ?> ">
                    <?php if($ubicacion[4]!="finish ") { ?>
                        <a href="modificar?c=<?php echo $solicitud['prere_codig']?>&s=5">
                    <?php } ?>    
                            <div class="step-number">5</div>
                            <div class="step-title">Documentos</div>
                            <div class="step-info">Carga de Documentos Digitales</div>

                    <?php if($ubicacion[4]!="finish ") { ?>
                        </a>
                    <?php } ?>
                 </div>
              </div>
        </div>
    </div>
</div>