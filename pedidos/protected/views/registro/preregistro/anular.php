<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Solicitudes</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Registro y Documentación</a></li>
            <li><a href="#">Solicitudes</a></li>
            <li class="active">Anular</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php
                $conexion=Yii::app()->db;
            ?>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Datos de la Solicitud</h3>
        </div>
        <div class="panel-body">
            <div class="sttabs tabs-style-linebox">
            <nav>
              <ul>
                <li class="tab-current">
                    <a href="#section-linebox-1">
                        <span>DATOS DEL COMERCIO</span>
                    </a>
                </li>
                <li class="">
                    <a href="#section-linebox-2">
                        <span>DATOS DE AFILIACIÓN</span>
                    </a>
                </li>
                <li class="">
                    <a href="#section-linebox-3">
                        <span>CONFIGURACIÓN DEL EQUIPO</span>
                    </a>
                </li>

                <li class="">
                    <a href="#section-linebox-4">
                        <span>REPRESENTANTES LEGALES</span>
                    </a>
                </li>

                <li class="">
                    <a href="#section-linebox-5">
                        <span>DATOS ADICIONALES</span>
                    </a>
                </li>
                <li class="">
                    <a href="#section-linebox-6">
                        <span>DOCUMENTOS DIGITALES</span>
                    </a>
                </li>
                
              </ul>
            </nav>
            <?php
            $sql="SELECT * FROM p_actividad_comercial
                    WHERE acome_codig='".$solicitud['acome_codig']."'";
            $acomercial=$conexion->createCommand($sql)->queryRow();
            $sql="SELECT * FROM p_estados
                    WHERE estad_codig='".$solicitud['estad_codig']."'";
            $p_estados=$conexion->createCommand($sql)->queryRow();
            $sql="SELECT * FROM p_municipio
                    WHERE munic_codig='".$solicitud['munic_codig']."'";
            $p_municipio=$conexion->createCommand($sql)->queryRow();
            $sql="SELECT * FROM p_parroquia
                    WHERE parro_codig='".$solicitud['parro_codig']."'";
            $p_parroquia=$conexion->createCommand($sql)->queryRow();
            $sql="SELECT * FROM p_ciudades
                    WHERE ciuda_codig='".$solicitud['ciuda_codig']."'";
            $ciudad=$conexion->createCommand($sql)->queryRow();
            ?>
            <div class="content-wrap text-center">
                <section id="section-linebox-1" class="content-current">
                    <table class="table table-bordered table-hover">
                        <tr>
                            <th>RIF del Comercio * </th>
                            <td><?php echo $solicitud['prere_rifco']?></td>
                            <th>Razon Social* </th>
                            <td><?php echo $solicitud['prere_rsoci']?></td>
                            <th>Nombre Fantasia o Comercial *</th>
                            <td><?php echo $solicitud['prere_nfant']?></td>
                        </tr>
                        <tr>
                            <th>Actividad Comercial * </th>
                            <td><?php echo $acomercial['acome_descr']?></td>
                            <th>Teléfono Movil * </th>
                            <td><?php echo $solicitud['prere_tmovi']?></td>
                            <th>Teléfono Fijo</th>
                            <td><?php echo $solicitud['prere_tfijo']?></td>
                        </tr>
                        <tr>
                            <th>Estado *  </th>
                            <td><?php echo $p_estados['estad_descr']; ?></td>
                            <th>Municipio * </th>
                            <td><?php echo $p_municipio['munic_descr']; ?></td>
                            <th>Parroquia *</th>
                            <td><?php echo $p_parroquia['parro_descr']; ?></td>
                        </tr>
                        <tr>
                            <th>Ciudad *  </th>
                            <td><?php echo $ciudad['ciuda_descr']; ?></td>
                            <th>Correo Electronico * </th>
                            <td><?php echo $solicitud['prere_celec']; ?></td>
                            <th>Dirección Fiscal  *</th>
                            <td><?php echo $solicitud['prere_dfisc']; ?></td>
                        </tr>
                    </table>
                </section>
                <section id="section-linebox-2" class="">
                    <?php
                    $sql="SELECT * FROM p_banco 
                          WHERE banco_codig='".$solicitud['banco_codig']."'";
                    $banco=$conexion->createCommand($sql)->queryRow();
                    ?>
                    <table class="table table-bordered table-hover">
                        <tr>
                            <th>Banco Afiliado * </th>
                            <td><?php echo $banco['banco_descr']; ?></td>
                        </tr>
                        <tr>
                            <th>Código Afiliado * </th>
                            <td><?php echo $solicitud['prere_cafil']; ?> </td>
                        </tr>
                        <tr>
                            <th>Numero de Cuenta *</th>
                            <td><?php echo $solicitud['prere_ncuen']; ?></td>
                        </tr>
                    </table>
                </section>
                <section id="section-linebox-3" class="">
                    <?php
                    $sql="SELECT * FROM rd_preregistro_configuracion_equipo a
                          JOIN p_operador_telefonico b ON (a.otele_codig = b.otele_codig)
                          WHERE prere_codig='".$solicitud['prere_codig']."'";
                    $cequipo=$conexion->createCommand($sql)->queryAll();
                    ?>
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Operador Telefonico</th>
                                <th>Cantidad </th>
                            </tr>    
                        </thead>
                        <tbody>
                            <?php
                                foreach ($cequipo as $key => $value) {
                                   ?>
                                   <tr> 
                                        <td><?php echo $value['otele_descr']; ?></td>
                                        <td><?php echo $value['cequi_canti']; ?></td>
                                    </tr> 
                                   <?php 
                                   $total+=$value['cequi_canti'];
                                }
                            ?>
                             
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>TOTAL</th>
                                <th><?php echo $total?></th>
                            </tr>  
                        </tfoot>
                    </table>               
                </section>
                <section id="section-linebox-4" class="">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>RIF</th>
                                <th>Nombre </th>
                                <th>Tipo de Documento</th>
                                <th>Número Documento</th>
                                <th>Cargo</th>
                                <th>Teléfono Movil</th>
                                <th>Teléfono Fijo</th>
                                <th>Correo Electrónico</th>
                            </tr>    
                        </thead>
                        <tbody>
                             <?php
                                $sql="SELECT * FROM rd_preregistro_representante_legal a
                                      JOIN p_documento_tipo b ON (a.tdocu_codig = b.tdocu_codig)
                                      WHERE prere_codig='".$solicitud['prere_codig']."'";
                                $rlegal=$conexion->createCommand($sql)->queryAll();
                                ?>
                            <?php
                                foreach ($rlegal as $key => $value) {
                                   ?>
                                   <tr> 
                                        <td><?php echo $value['rlega_rifrl']; ?></td>
                                        <td><?php echo $value['rlega_nombr'].' '.$value['rlega_apell']; ?></td>
                                        <td><?php echo $value['tdocu_descr']; ?></td>
                                        <td><?php echo $value['rlega_ndocu']; ?></td>
                                        <td><?php echo $value['rlega_cargo']; ?></td>
                                        <td><?php echo $value['rlega_tmovi']; ?></td>
                                        <td><?php echo $value['rlega_tfijo']; ?></td>
                                        <td><?php echo $value['rlega_corre']; ?></td>

                                    </tr> 
                                   <?php 
                                   $total+=$value['cequi_canti'];
                                }
                            ?>
                               
                        </tbody>
                    </table>               
                </section>
                <section id="section-linebox-5" class="">
                     <?php
                    $sql="SELECT * FROM p_solicitud_origen
                          WHERE orige_codig='".$solicitud['orige_codig']."'";
                    $origen=$conexion->createCommand($sql)->queryRow();
                    $vip= array('1' => 'SI', '2' => 'NO');
                    ?>
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Fecha de Soliciud</th>
                                <th>Origen </th>
                                <th>Cliente VIP</th>
                                <th>Observaciones</th>
                            </tr>    
                        </thead>
                        <tbody>
                            <tr>
                                <td><?php echo $this->funciones->transformarFecha_v($solicitud['prere_fsoli'])?></td>
                                <td><?php echo $origen['orige_descr']?></td>
                                <td><?php echo $vip[$solicitud['prere_clvip']]?></td>
                                <td><?php echo $solicitud['prere_obser']?></td>
                            </tr>    
                        </tbody>
                    </table>               
                </section>
                <section id="section-linebox-6" class="">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>DOCUMENTO</th>
                                <th>VER</th>
                            </tr>    
                        </thead>
                        <tbody>
                            <?php
                                $sql="SELECT * FROM rd_preregistro_documento_digital a
                                      JOIN rd_preregistro_documento_digital_tipo b ON (a.dtipo_codig = b.dtipo_codig)
                                      WHERE prere_codig='".$solicitud['prere_codig']."'";
                                $rlegal=$conexion->createCommand($sql)->queryAll();
                                ?>
                            <?php
                                foreach ($rlegal as $key => $value) {
                                   ?>
                                   <tr> 
                                        <td><?php echo $value['dtipo_descr']; ?></td>
                                        <td><a class="btn btn-info btn-block" href="/<?php echo $value['docum_ruta']; ?>" target="_blank"><i class="fa fa-search"></i></td>

                                    </tr> 
                                   <?php 
                                   $total+=$value['cequi_canti'];
                                }
                            ?>    
                        </tbody>
                    </table>               
                </section>
            </div><!-- /content -->
            </div>

        </div>
    </div>
    </div>  
</div>
<form id='login-form' name='login-form' method="post">

<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Anular Solicitud</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            
                <div class="row hide">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Código</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php echo CHtml::hiddenField('codigo', $roles['usuar_codig'], array('class' => 'form-control', 'placeholder' => "Código")); ?>

                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row " id='motivo'>
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Motivo</label>
                                <?php echo CHtml::dropDownList('motiv', '',array('1'=>'Motivo 1', '2' =>'Motivo 2'), array('class' => 'form-control', 'prompt' => "Seleccione")); ?>

                        </div>
                    </div>
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observación</label>
                                <?php echo CHtml::textArea('obser','', array('class' => 'form-control', 'placeholder' => "Motivo")); ?>

                        </div>
                    </div>
                </div>
                <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-4 ">
                            <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                        </div>
                    </div>
        </div><!-- form -->
    </div>  
</div>
</form>
<script type="text/javascript">
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                accio: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Acción" es obligatorio',
                        }
                    }
                },
                motiv: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Motivo" es obligatorio',
                        }
                    }
                },
                obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Observaciones" es obligatorio',
                        }
                    }
                },

            }
        });
    })
</script>
<script type="text/javascript">
    
    $(document).ready(function () {
        $('#guardar').click(function () {
            $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
            var formValidation = $('#login-form').data('formValidation');
            if (formValidation.isValid()) {
                $.ajax({
                    dataType: "json",
                    data: $('#login-form').serialize(),
                    url: 'verificar',
                    type: 'post',
                    beforeSend: function () {
                        $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                    },
                    success: function (response) {
                    $('#wrapper').unblock();
                        if (response['success'] == 'true') {
                            swal({ 
                                title: "Exito!",
                                text: response['msg'],
                                type: "success",
                                confirmButtonText: "Cerrar",
                                confirmButtonClass: "btn-info"
                            },function(){
                                window.open('listado', '_parent');
                            });
                        } else {
                            swal({ 
                                title: "Error!",
                                text: response['msg'],
                                type: "error",
                                confirmButtonText: "Cerrar",
                                confirmButtonClass: "btn-danger"
                            },function(){
                                $("#guardar").removeAttr('disabled');
                            });
                        }

                    },error:function (response) {
                    $('#wrapper').unblock();
                        swal({ 
                            title: "Error!",
                            text: "Error el ejecutar la operación",
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"

                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                            
                    }
                });
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#accio").change(function () {
            var accion = $(this).val();
            if(accion=='2'){
                $("#motivo").removeClass("hide");
                $("#motiv").removeAttr("disabled");
            }else{
                $("#motivo").addClass("hide");
                $("#motiv").attr("disabled","true");
            }
            
        });
    });
</script>