<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Acceso</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Registro</a></li>
            <li><a href="#">Acceso</a></li>
            <li class="active">Verificar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<form id='login-form' name='login-form' method="post">
                
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Verificar Acceso</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            
                <div class="row hide">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Código</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php echo CHtml::hiddenField('codigo', $roles['acces_corre'], array('class' => 'form-control', 'placeholder' => "Código")); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Correo</label>
                                <?php echo CHtml::textField('acces_corre', $roles['acces_corre'], array('class' => 'form-control', 'placeholder' => "Segundo Nombre", 'readonly'=>'true', "disabled"=>"true")); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Número de Documento *</label>
                          
                                <?php echo CHtml::textField('acces_ndocu', $roles['acces_ndocu'], array('class' => 'form-control', 'placeholder' => "RUT", "disabled"=>"true")); ?>

                        </div>
                    </div>
                    
                   
                
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Primer Nombre *</label>
                          
                                <?php echo CHtml::textField('acces_pnomb', $roles['acces_pnomb'], array('class' => 'form-control', 'placeholder' => "Primer Nombre", "disabled"=>"true")); ?>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Segundo Nombre</label>
                          
                                <?php echo CHtml::textField('acces_snomb', $roles['acces_snomb'], array('class' => 'form-control', 'placeholder' => "Segundo Nombre", "disabled"=>"true")); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Primer Apellido *</label>
                          
                                <?php echo CHtml::textField('acces_papel', $roles['acces_papel'], array('class' => 'form-control', 'placeholder' => "Primer Apellido", "disabled"=>"true")); ?>

                        </div>
                    </div>
                    
                    
                   
                
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Segundo Apellido</label>
                          
                                <?php echo CHtml::textField('acces_sapel', $roles['acces_sapel'], array('class' => 'form-control', 'placeholder' => "Segundo Apellido", "disabled"=>"true")); ?>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Telefono Celular *</label>
                          
                                <?php echo CHtml::textField('acces_tcelu',$roles['acces_tmovi'], array('class' => 'form-control', 'placeholder' => "Telefono Celular *", "disabled"=>"true")); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Rif del comercio *</label>
                          
                                <?php echo CHtml::textField('acces_rifco',$roles['acces_rifco'], array('class' => 'form-control', 'placeholder' => "Telefono Celular *", "disabled"=>"true")); ?>

                        </div>
                    </div>
                    
                   
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Razon Social</label>
                                <?php echo CHtml::textArea('obser', $roles['acces_rsoci'], array('class' => 'form-control', 'placeholder' => "Nombre de colegio o Agrupación", "disabled"=>"true")); ?>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Accion</label>
                                <?php echo CHtml::dropDownList('accio', '',array('1'=>'Aprobar', '2' =>'Rechazar'), array('class' => 'form-control', 'prompt' => "Seleccione")); ?>

                        </div>
                    </div>
                </div>
                <div class="row hide" id='motivo'>
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Motivo</label>
                                <?php echo CHtml::textArea('motiv','', array('class' => 'form-control', 'placeholder' => "Motivo")); ?>

                        </div>
                    </div>
                </div>
                <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-4 ">
                            <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                        </div>
                    </div>
        </div><!-- form -->
    </div> 
</form>
<script type="text/javascript">
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                accio: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Acción" es obligatorio',
                        }
                    }
                },
                motiv: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Motivo" es obligatorio',
                        }
                    }
                },
                /*obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Observaciones" es obligatorio',
                        }
                    }
                },*/



            }
        });
    })
</script>
<script type="text/javascript">
    
    $(document).ready(function () {
        $('#guardar').click(function () {
            $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
            var formValidation = $('#login-form').data('formValidation');
            if (formValidation.isValid()) {
                $.ajax({
                    dataType: "json",
                    data: $('#login-form').serialize(),
                    url: 'verificar',
                    type: 'post',
                    beforeSend: function () {
                        $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                    },
                    success: function (response) {
                    $('#wrapper').unblock();
                        if (response['success'] == 'true') {
                            swal({ 
                                title: "Exito!",
                                text: response['msg'],
                                type: "success",
                                confirmButtonText: "Cerrar",
                                confirmButtonClass: "btn-info"
                            },function(){
                                window.open('listado', '_parent');
                            });
                        } else {
                            swal({ 
                                title: "Error!",
                                text: response['msg'],
                                type: "error",
                                confirmButtonText: "Cerrar",
                                confirmButtonClass: "btn-danger"
                            },function(){
                                $("#guardar").removeAttr('disabled');
                            });
                        }

                    },error:function (response) {
                    $('#wrapper').unblock();
                        swal({ 
                            title: "Error!",
                            text: "Error el ejecutar la operación",
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"

                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                            
                    }
                });
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#accio").change(function () {
            var accion = $(this).val();
            if(accion=='2'){
                $("#motivo").removeClass("hide");
                $("#motiv").removeAttr("disabled");
            }else{
                $("#motivo").addClass("hide");
                $("#motiv").attr("disabled","true");
            }
            
        });
    });
</script>