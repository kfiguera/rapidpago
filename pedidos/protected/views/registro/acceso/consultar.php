<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Acceso</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Registro</a></li>
            <li><a href="#">Acceso</a></li>
            <li class="active">Consultar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<form id='login-form' name='login-form' method="post">
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Consultar Acceso</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            
                <div class="row hide">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Código</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php echo CHtml::hiddenField('codigo', $roles['usuar_codig'], array('class' => 'form-control', 'placeholder' => "Código")); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8">        
                        <div class="form-group">
                            <label>Correo</label>
                                <?php echo CHtml::textField('acces_corre', $roles['acces_corre'], array('class' => 'form-control', 'placeholder' => "Segundo Nombre", 'readonly'=>'true', "disabled"=>"true")); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Número de Documento *</label>
                          
                                <?php echo CHtml::textField('acces_ndocu', $roles['acces_ndocu'], array('class' => 'form-control', 'placeholder' => "RUT", "disabled"=>"true")); ?>

                        </div>
                    </div>
                    
                   
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Primer Nombre *</label>
                          
                                <?php echo CHtml::textField('acces_pnomb', $roles['acces_pnomb'], array('class' => 'form-control', 'placeholder' => "Primer Nombre", "disabled"=>"true")); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Segundo Nombre</label>
                          
                                <?php echo CHtml::textField('acces_snomb', $roles['acces_snomb'], array('class' => 'form-control', 'placeholder' => "Segundo Nombre", "disabled"=>"true")); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Primer Apellido *</label>
                          
                                <?php echo CHtml::textField('acces_papel', $roles['acces_papel'], array('class' => 'form-control', 'placeholder' => "Primer Apellido", "disabled"=>"true")); ?>

                        </div>
                    </div>
                    
                    
                   
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Segundo Apellido</label>
                          
                                <?php echo CHtml::textField('acces_sapel', $roles['acces_sapel'], array('class' => 'form-control', 'placeholder' => "Segundo Apellido", "disabled"=>"true")); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Telefono Celular *</label>
                          
                                <?php echo CHtml::textField('acces_tcelu',$roles['acces_tmovi'], array('class' => 'form-control', 'placeholder' => "Telefono Celular *", "disabled"=>"true")); ?>

                        </div>
                    </div>
                    
                   
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Razon Social</label>
                                <?php echo CHtml::textArea('obser', $roles['acces_rsoci'], array('class' => 'form-control', 'placeholder' => "Nombre de colegio o Agrupación", "disabled"=>"true")); ?>

                        </div>
                    </div>
                </div>
                <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-12 ">
                            <a href="listado" type="reset" class="btn-block btn btn-info">Volver </a>
                        </div>
                    </div>
                

            

        </div><!-- form -->
    </div>  
</div>
</form>