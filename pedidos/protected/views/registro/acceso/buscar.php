<table id='auditoria' class="table table-bordered table-hover dataTable">
    <thead>
        <tr>
            <th width="2%">
                #
            </th>
            <th>
                Usuario
            </th>
            <th>
                Nombre
            </th>
            <th>
                Estatus
            </th>
            <th width="5%">
                Consultar
            </th>
            <th width="5%">
                Verificar
            </th>
        </tr>
    </thead>

    <tbody>
        <?php
        $sql = "SELECT * FROM seguridad_usuarios a
                                JOIN p_persona b ON (a.perso_codig = b.perso_codig)
                                JOIN registro c ON (a.usuar_codig = c.usuar_codig)
                                JOIN seguridad_usuarios_estatus d ON (a.uesta_codig = d.uesta_codig)
                                WHERE urole_codig = '2'
                                AND a.uesta_codig = '2' ".$condicion;
                                
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $p_persona = $command->query();
        $i=0;
        while (($row = $p_persona->read()) !== false) {
            $i++;
        ?>
        <tr>
            <td class="tabla"><?php echo $i ?></td>
            <td class="tabla"><?php echo $row['usuar_login'] ?></td>
            <td class="tabla"><?php echo $row['perso_pnomb'].' '.$row['perso_papel'] ?></td>
            <td class="tabla"><?php echo $row['uesta_descr'] ?></td>
            <td class="tabla"><a href="consultar?c=<?php echo $row['usuar_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-search"></i></a></td>
            <td class="tabla"><a href="verificar?c=<?php echo $row['usuar_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-pencil"></i></a></td>
        </tr>
        <?php
            }   
        ?>
    </tbody>
</table>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable(); 
    });
</script>
