<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Solicitudes</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Registro y Documentación</a></li>
            <li><a href="#">Solicitudes</a></li>
            <li class="active">Listado</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

<?php
//Mostrar mensajes del controlador
        foreach (Yii::app()->user->getFlashes() as $key => $message) {
          echo '<div class="alert alert-' . $key . ' alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              ' . $message  .' </div>';          
        }
$connection = Yii::app()->db;
if(Yii::app()->user->id['usuario']['permiso']==1){
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Criterio de Busqueda</h3>
    </div>
    <div class="panel-body" >
        <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'pedidos', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            

        ?>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Número de Solicitud</label>
                    <?php echo CHtml::textField('numero', '', array('class' => 'form-control', 'placeholder' => "Nro del Solicitud")); ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Estatus</label>
                    <?php 
                        
                        $sql="SELECT * FROM rd_preregistro_estatus";
                        $result=$connection->createCommand($sql)->queryAll();
                        $data=CHtml::listData($result,'epedi_codig','epedi_descr');
                        echo CHtml::dropDownList('epedi', '', $data,array('class' => 'form-control', 'placeholder' => "Descripción", 'prompt'=>'Seleccione...')); ?>
                </div>
            </div>
           
        </div>
        
        <div class="row">
            <div class="col-sm-6">
                <button type="button" id="buscar" class="btn btn-block btn-info">Buscar</button>
            </div>
            <div class="col-sm-6">
                <button type="reset" class="btn btn-block btn-default">Limpiar</button>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div> 
<?php
}
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-sm-8">
                <h3 class="panel-title">Listado</h3>
            </div>
            <?php
                if(Yii::app()->user->id['usuario']['permiso']!=8 and Yii::app()->user->id['usuario']['permiso']!=3){
            ?>
            <div class="col-sm-4">
                <a href="registrar" class="btn btn-block btn-info" >Nueva Solicitud</a>
            </div>
            <?php
                }
            ?>
        </div>
        
    </div>
    <div class="panel-body" >
        
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-pedido'>
                <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                #
                            </th>
                            <th>
                                Nro Solicitud
                            </th>
                            <th>
                                RIF del Comercio
                            </th>
                            <th>
                                Código de Afiliado
                            </th>
                            <th>
                                Banco
                            </th>
                            <th>
                                Cliente
                            </th>
                            <th>
                                Contacto
                            </th>
                            <th>
                                Total POS a Solicitar
                            </th>
                            <th>
                                Usuario de Registro
                            </th>
                            <th>
                                Fecha de Registro
                            </th>
                            <th width="5px">
                                Estatus
                            </th>
                            <th width="5%">
                                &nbsp;
                            </th>
                            <?php
                                if(Yii::app()->user->id['usuario']['permiso']!=8){
                            ?>
                            <th width="5%">
                                &nbsp;
                            </th>
                            <?php
                                if(Yii::app()->user->id['usuario']['permiso']==1 or Yii::app()->user->id['usuario']['permiso']==14 or Yii::app()->user->id['usuario']['permiso']==11){
                            ?>
                            <th width="5%">
                                &nbsp;
                            </th>
                            <?php
                                }
                            }
                            ?>
                            <!--th width="5%">
                                Anular
                            </th-->
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        /*if(Yii::app()->user->id['usuario']['permiso']==1){
                            $sql = "SELECT *, 
                                    (SELECT COUNT(pdeta_codig) FROM pedido_pedidos_detalle e WHERE a.pedid_codig = e.pedid_codig) modelos, 
                                    (SELECT COUNT(pdlis_codig) FROM pedido_pedidos_detalle_listado f WHERE a.pedid_codig = f.pedid_codig) p_personas 
                                FROM pedido_pedidos a 
                                JOIN rd_preregistro_estatus b ON (a.epedi_codig = b.epedi_codig)
                                JOIN seguridad_usuarios c ON (a.usuar_codig = c.usuar_codig)
                                LEFT JOIN colegio d ON (c.coleg_codig = d.coleg_codig)";
                   
                        }else{
                            $sql = "SELECT *, 
                                    (SELECT COUNT(pdeta_codig) FROM pedido_pedidos_detalle e WHERE a.pedid_codig = e.pedid_codig) modelos, 
                                    (SELECT COUNT(pdlis_codig) FROM pedido_pedidos_detalle_listado f WHERE a.pedid_codig = f.pedid_codig) p_personas  FROM pedido_pedidos a 
                                    JOIN rd_preregistro_estatus b ON (a.epedi_codig = b.epedi_codig)
                                    JOIN seguridad_usuarios c ON (a.usuar_codig = c.usuar_codig)
                                    LEFT JOIN colegio d ON (c.coleg_codig = d.coleg_codig)
                                    WHERE a.usuar_codig = '".Yii::app()->user->id['usuario']['codigo']."'";
                             
                        }*/
                        if(Yii::app()->user->id['usuario']['permiso']==12){
                            $sql = "SELECT *
                                FROM rd_preregistro a 
                                JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                                JOIN seguridad_usuarios c ON (a.usuar_codig = c.usuar_codig)
                                JOIN p_persona d ON (c.perso_codig = d.perso_codig)
                                WHERE a.estat_codig in (0,1,2,3,5)
                                  AND a.usuar_codig = '".Yii::app()->user->id['usuario']['codigo']."'
                                ";
                        }else{
                            $sql = "SELECT *
                                FROM rd_preregistro a 
                                JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                                JOIN seguridad_usuarios c ON (a.usuar_codig = c.usuar_codig)
                                JOIN p_persona d ON (c.perso_codig = d.perso_codig)
                                WHERE a.estat_codig in (0,1,2,3,5)
                                  AND (orige_codig=2 or a.usuar_codig in (
                                  SELECT usuar_codig FROM seguridad_usuarios WHERE urole_codig = '12'))

                                ";
                        }

                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        $j=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                            $j++;

                            $sql="SELECT * 
                                  FROM solicitud_trayectoria 
                                  WHERE prere_codig = '".$row['prere_codig']."'
                                    AND estat_codig = '".$row['estat_codig']."'";
                            $trayectoria = $connection->createCommand($sql)->queryRow();

                            $inicio=$trayectoria['traye_finic'].' '.$trayectoria['traye_hinic'];
                            $fin=date('Y-m-d H:i:s');
                            $diff=$this->funciones->diferenciaHoras($inicio,$fin);

                            $horas=($diff->days * 24 )  + ( $diff->h );
                            $semaforo=$this->funciones->semaforo($connection,$row['estat_codig'],$horas);
                            
                            $sql = "SELECT sum(cequi_canti) cantidad
                                FROM rd_preregistro_configuracion_equipo a 
                                WHERE prere_codig='".$row['prere_codig'] ."'";
                            $cequipo = $connection->createCommand($sql)->queryRow();
                            
                            $sql = "SELECT *
                                FROM p_banco 
                                WHERE banco_codig='".$row['banco_codig'] ."'";
                            $banco = $connection->createCommand($sql)->queryRow();
                            $sql = "SELECT *
                                FROM seguridad_usuarios a 
                                JOIN p_persona b ON (a.perso_codig = b.perso_codig)
                                WHERE usuar_codig='".$row['usuar_codig'] ."'";
                            $usuario = $connection->createCommand($sql)->queryRow();
                        ?>
                        <tr>
                            <td class="tabla"><?php echo $i ?></td>

                            <td class="tabla"><?php echo $row['prere_numer'] ?></td>
                            <td class="tabla"><?php echo $row['prere_rifco'] ?></td>
                            <td class="tabla"><?php echo $row['prere_cafil'] ?></td>
                            <td class="tabla"><?php echo $banco['banco_descr'] ?></td>
                            <td class="tabla"><?php echo $row['prere_rsoci'] ?></td>
                            <td class="tabla"><?php echo $row['prere_celec'] ?></td>
                            <td class="tabla"><?php echo $cequipo['cantidad'] ?></td>

                            <td class="tabla"><?php echo $row['perso_pnomb'].' '.$row['perso_papel'] ?></td>
                            <td class="tabla"><?php echo $this->funciones->transformarFecha_v($row['prere_fcrea']).' '.$row['prere_hcrea'] ?></td>
                            <td class="tabla"><?php echo $semaforo.' '.$row['estat_descr'] ?></td>

                            <!--td class="tabla"><a href="../detalle/listado?p=<?php echo $row['prere_codig'] ?>" class="btn btn-block btn-info"><i class="glyphicon glyphicon-list-alt"></i></a></td-->
                            <td class="tabla">
                                <a href="consultar?c=<?php echo $row['prere_codig'] ?>" class="btn btn-block btn-info"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Consultar" >
                                    <i class="fa fa-search"></i>
                                </a>
                            </td>
                            <?php
                                if(Yii::app()->user->id['usuario']['permiso']!=8){
                            ?>
                            <td class="tabla">
                                <a href="modificar?c=<?php echo $row['prere_codig'] ?>&s=<?php echo $row['prere_pasos'] ?>" class="btn btn-block btn-info"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Modificar" >
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </td>                          
                            
<?php
                                if(Yii::app()->user->id['usuario']['permiso']==1 or Yii::app()->user->id['usuario']['permiso']==14 or Yii::app()->user->id['usuario']['permiso']==11){
                            ?>

                            <td class="tabla">
                                <a href="verificar?c=<?php echo $row['prere_codig'] ?>" class="btn btn-block btn-warning"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Verificar">
                                    <i class="fa fa-check-square-o"></i>
                                </a>
                            </td>
<?php
                                }
                            }
                            ?>
                            <!--td class="tabla">
                                <a href="anular?c=<?php echo $row['prere_codig'] ?>" target="_blank" class="btn btn-block btn-danger">
                                    <i class="fa fa-close"></i>
                                </a>
                            </td-->
                        </tr>
                        <?php
                            }   
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>  
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#buscar').click(function () {
        var formData = new FormData($("#pedidos")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-pedido').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>
