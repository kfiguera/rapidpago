<table id='auditoria' class="table table-bordered table-hover dataTable">
    <thead>
        <tr>
            <th width="2%">
                #
            </th>
            <th>
                Nro Pedido
            </th>
            <th>
                Usuario
            </th>
            <th>
                Colegio
            </th>
            <th>
                Total a Solicitar
            </th>
            <th>
                Total de Modelos
            </th>
            <th>
                Fecha de Registro
            </th>
            <th>
                Estatus
            </th>
            <th width="5%">
                Consultar
            </th>
            <th width="5%">
                Modificar
            </th>
            <th width="5%">
                Eliminar
            </th>
            <th width="5%">
                Imprimir
            </th>
            <?php
                if(Yii::app()->user->id['usuario']['permiso']==1){
            ?>
            <th width="5%">
                Costos Adicionales
            </th>
            <th width="5%">
                Deducciones
            </th>
            <th width="5%">
                Aprobar
            </th>
            <th width="5%">
                Rechazar
            </th>
            <?php } ?>
        </tr>
    </thead>

    <tbody>
        <?php
        $sql = "SELECT *, 
                    (SELECT COUNT(pdeta_codig) FROM pedido_pedidos_detalle e WHERE a.pedid_codig = e.pedid_codig) modelos, 
                    (SELECT COUNT(pdlis_codig) FROM pedido_pedidos_detalle_listado f WHERE a.pedid_codig = f.pedid_codig) p_personas 
                FROM pedido_pedidos a 
                JOIN rd_preregistro_estatus b ON (a.epedi_codig = b.epedi_codig)
                JOIN seguridad_usuarios c ON (a.usuar_codig = c.usuar_codig)
                LEFT JOIN colegio d ON (c.coleg_codig = d.coleg_codig)
                ".$condicion;
   
        
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $p_persona = $command->query();
        $i=0;
        while (($row = $p_persona->read()) !== false) {
            $i++;
        ?>
        <tr>
            <td class="tabla"><?php echo $i ?></td>
            <td class="tabla"><?php echo $row['pedid_numer'] ?></td>
            <td class="tabla"><?php echo $row['usuar_login'] ?></td>
            <td class="tabla"><?php echo $row['coleg_nombr'] ?></td>
            <td class="tabla"><?php echo $row['p_personas'] ?></td>
            <td class="tabla"><?php echo $row['modelos'] ?></td>
            <td class="tabla"><?php echo $row['epedi_descr'] ?></td>

            <td class="tabla"><a href="consultar?c=<?php echo $row['pedid_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-search"></i></a></td>
            <td class="tabla"><a href="../detalle/listado?p=<?php echo $row['pedid_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-pencil"></i></a></td>
            <?php 
                if($row['epedi_codig']=='1'){
                    ?>
                    
                    <td class="tabla"><a href="eliminar?c=<?php echo $row['pedid_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-close"></i></a></td>
                    <?php
                }else{
                    ?>
                     <td class="tabla"><a href="#" disabled class="disabled btn btn-block btn-info"><i class="fa fa-close"></i></a></td>
                    <?php
                }
            ?>
            <?php
                if($row['epedi_codig']>2 or Yii::app()->user->id['usuario']['permiso']==1){
            ?>
            <td class="tabla"><a href="../../reportes/pedidos/pdf?c=<?php echo $row['pedid_codig'] ?>" target="_blank" class="btn btn-block btn-info"><i class="fa fa-file-pdf-o"></i></a></td>
            <?php
                }else{
            ?>
            <td class="tabla"><a href="#" target="_blank" class="disabled btn btn-block btn-info" disabled><i class="fa fa-file-pdf-o"></i></a></td>
            <?php
                }
                if(Yii::app()->user->id['usuario']['permiso']==1){
            ?>
            <td class="tabla"><a href="costos?c=<?php echo $row['pedid_codig'] ?>" target="_blank" class="btn btn-block btn-info"><i class="fa fa-money"></i></a></td>
            <td class="tabla"><a href="deduccion?c=<?php echo $row['pedid_codig'] ?>" target="_blank" class="btn btn-block btn-info"><i class="fa fa-money"></i></a></td>
            <td class="tabla"><a href="aprobar?c=<?php echo $row['pedid_codig'] ?>" target="_blank" class="btn btn-block btn-success"><i class="fa fa-check"></i></a></td>
            <td class="tabla"><a href="rechazar?c=<?php echo $row['pedid_codig'] ?>" target="_blank" class="btn btn-block btn-danger"><i class="fa fa-close"></i></a></td>
        </tr>
        <?php
            }   
        ?>
    </tbody>
</table>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable(); 
    });
</script>
