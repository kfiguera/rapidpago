<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Tramitación</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Tramitación</a></li>
            <li><a href="#">Pagos</a></li>
            <li class="active">Listado</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php
$connection = Yii::app()->db;
if(Yii::app()->user->id['usuario']['permiso']==1){
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Criterio de Busqueda</h3>
    </div>
    <div class="panel-body" >
        <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'pedidos', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            $connection = Yii::app()->db;

        ?>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Número de Solicitud</label>
                    <?php echo CHtml::textField('numero', '', array('class' => 'form-control', 'placeholder' => "Nro del Solicitud")); ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>Estatus</label>
                    <?php 
                        
                        $sql="SELECT * FROM solicitud_estatus_pago";
                        $result=$connection->createCommand($sql)->queryAll();
                        $data=CHtml::listData($result,'epago_codig','epago_descr');
                        echo CHtml::dropDownList('epago', '', $data,array('class' => 'form-control', 'placeholder' => "Descripción", 'prompt'=>'Seleccione...')); ?>
                </div>
            </div>
           
        </div>
        
        <div class="row">
            <div class="col-sm-6">
                <button type="button" id="buscar" class="btn btn-block btn-info">Buscar</button>
            </div>
            <div class="col-sm-6">
                <button type="reset" class="btn btn-block btn-default">Limpiar</button>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div>
<?php
}
?> 
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-sm-8">
                <h3 class="panel-title">Listado</h3>
            </div>
            <div class="col-sm-4">
                <a href="registrar" class="btn btn-block btn-info" >Nuevo Pago</a>
            </div>
        </div>
    </div>
    <div class="panel-body" >
        
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-pedido'>
                <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                #
                            </th>
                            <th>
                                Nro Solicitud
                            </th>
                            <th>
                                Monto Solicitud
                            </th>
                            <th>
                                Razon Social
                            </th>
                            <th>
                                Nombre Fantasia
                            </th>
                            <th>
                                Nro Pago
                            </th>
                            <th>
                                Monto
                            </th>
                            <th>
                                Referencia
                            </th>
                            <th>
                                Forma de Pago
                            </th>
                            <th>
                                Motivo de Rechazo
                            </th>
                            <th>
                                Ciente VIP
                            </th>
                            <th>
                                Usuario de Registro
                            </th>
                            <th>
                                Fecha de Registro
                            </th>
                            <th>
                                Estatus
                            </th>
                            <th width="5%">
                                &nbsp;
                            </th>
                            <th width="5%">
                                &nbsp;
                            </th>
                            <?php
                                if(Yii::app()->user->id['usuario']['permiso']==1 or Yii::app()->user->id['usuario']['permiso']==2){
                            ?>
                            <th width="5%">
                                &nbsp;
                            </th>
                            <th width="5%">
                                &nbsp;
                            </th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        //if(Yii::app()->user->id['usuario']['permiso']==1){
                            $sql = $this->funciones->SqlListadoPagos('10,29');
                        //echo "$sql";
                        
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                            $j++;
                            $sql="SELECT * FROM cliente where clien_codig='".$row['clien_codig']."'";
                            $cliente=$connection->createCommand($sql)->queryRow();


                            $vip=array('1'=>'SI', '2'=>'NO');
                        ?>
                        <tr>
                            <td class="tabla"><?php echo $i ?></td>

                            <td class="tabla"><a  href="consultar?c=<?php echo $row['solic_codig'] ?>"><?php echo $row['solic_numer'] ?></a></td>
                            <td class="tabla"><?php echo $this->funciones->transformarMonto_v($row['solic_monto'],2);//$row['coleg_nombr'] ?></td>

                            <td class="tabla"><?php echo $cliente['clien_rsoci'];//$row['coleg_nombr'] ?></td>
                            <td class="tabla"><?php echo $cliente['clien_nfant']//$row['usuar_login'] ?></td>
                            <td class="tabla"><?php echo $row['pagos_numer'] ?></td>
                            <td class="tabla"><?php echo $this->funciones->transformarMonto_v($row['pagos_monto'],2) ?></td>
                            <td class="tabla"><?php echo $row['pagos_refer'] ?></td>
                            <td class="tabla"><?php echo $row['ptipo_descr'] ?></td>

                            <td class="tabla"><?php echo $row['pagos_robse'] ?></td>
                            <td class="tabla"><?php echo $vip[$row['solic_clvip']] ?></td>
                            <td class="tabla"><?php echo $row['perso_pnomb'].' '.$row['perso_papel'] ?></td>
                            <td><?php echo $this->funciones->transformarFecha_v($row['solic_fcrea']).' '.$row['solic_hcrea'] ?></td>
                            <td class="tabla"><span class="<?php echo $etiqueta; ?>"><?php echo $row['epago_descr'] ?></span></td>

                            <!--td class="tabla"><a href="../detalle/listado?p=<?php echo $row['solic_codig'] ?>" class="btn btn-block btn-info"><i class="glyphicon glyphicon-list-alt"></i></a></td-->
                            <td class="tabla"><a href="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$row['pagos_rimag'] ?>" target="_blank" class="btn btn-block btn-info"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Consultar" ><i class="fa fa-search"></i></a></td>
                            <td class="tabla"><a href="../../reportes/tramitacion/cotizacion?c=<?php echo $row['solic_codig'] ?>" target="_blank" class="btn btn-block btn-info"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Imprimir Cotización" ><i class="fa fa-file-pdf-o"></i></a></td>
                            <?php
                                if(Yii::app()->user->id['usuario']['permiso']==1 or Yii::app()->user->id['usuario']['permiso']==2){
                            ?>
                                <td class="tabla"><a href="aprobar?c=<?php echo $row['pagos_codig'] ?>" class="btn btn-block btn-success"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Aprobar" ><i class="fa fa-check"></i></a></td>
                                <td class="tabla"><a href="rechazar?c=<?php echo $row['pagos_codig'] ?>" class="btn btn-block btn-danger"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Rechazar" ><i class="fa fa-close"></i></a></td>
                            <?php } ?>
                        </tr>
                        <?php
                            }   
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>  
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#buscar').click(function () {
        var formData = new FormData($("#pedidos")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-pedido').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>
