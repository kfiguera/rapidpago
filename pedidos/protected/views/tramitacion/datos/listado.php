<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Registro</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Registro</a></li>
            
            <li class="active">Listado</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php
$connection = Yii::app()->db;
if(Yii::app()->user->id['usuario']['permiso']==1){
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Criterio de Busqueda</h3>
    </div>
    <div class="panel-body" >
        <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'p_personas', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            

        ?>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Colegio</label>
                    <input type="text" class="form-control" name="colegio" id="colegio" placeholder="Colegio">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Usuario</label>
                    <input type="text" class="form-control" name="usuario" id="usuario" placeholder="Usuario">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Estatus</label>
                    <?php 
                        $sql="SELECT * FROM registro_estatu";
                        $result=$connection->createCommand($sql)->queryAll();
                        $data=CHtml::listData($result,'eregi_codig','eregi_descr');
                        echo CHtml::dropDownList('estatus', '', $data,array('class' => 'form-control', 'placeholder' => "Estatus", 'prompt'=>'Seleccione...')); ?>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-6">
                <button type="button" id="buscar" class="btn btn-block btn-info">Buscar</button>
            </div>
            <div class="col-sm-6">
                <button type="reset" class="btn btn-block btn-default">Limpiar</button>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div> 
<?php
}
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Listado</h3>
    </div>
    <div class="panel-body" >
        
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-p_persona'>
                <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                #
                            </th>
                            <th>
                                Colegio
                            </th>
                            <th>
                                Usuario
                            </th>
                            <th>
                                Estatus
                            </th>
                            <th width="5%">
                                Consultar
                            </th>
                            <th width="5%">
                                Modificar
                            </th>
                            <?php 
                                if(Yii::app()->user->id['usuario']['permiso']==1){
                                    
                            ?>
                            <th width="5%">
                                Verificar
                            </th>
                            <?php
                                }
                            ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(Yii::app()->user->id['usuario']['permiso']==1){
                            $sql = "SELECT * FROM registro a
                                JOIN registro_estatu b ON (a.eregi_codig = b.eregi_codig)
                                JOIN seguridad_usuarios c ON (a.usuar_codig = c.usuar_codig)
                                LEFT JOIN colegio d ON (a.coleg_codig = d.coleg_codig)
                                WHERE a.usuar_codig = c.usuar_codig
                                AND c.uesta_codig <> '2'";
                       
                            }else{
                                 $sql = "SELECT * FROM registro a
                                JOIN registro_estatu b ON (a.eregi_codig = b.eregi_codig)
                                JOIN seguridad_usuarios c ON (a.usuar_codig = c.usuar_codig)
                                LEFT JOIN colegio d ON (a.coleg_codig = d.coleg_codig)
                                WHERE a.usuar_codig = c.usuar_codig
                                AND a.usuar_codig = '".Yii::app()->user->id['usuario']['codigo']."'
                                AND c.uesta_codig <> '2'";
                                 
                            }
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                        ?>
                        <tr>
                            <td class="tabla"><?php echo $i ?></td>
                            <td class="tabla"><?php echo $row['coleg_nombr'] ?></td>
                            <td class="tabla"><?php echo $row['usuar_login'] ?></td>
                            <td class="tabla"><?php echo $row['eregi_descr'] ?></td>
                            <td class="tabla"><a href="consultar?c=<?php echo $row['regis_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-search"></i></a></td>
                        <?php 
                            switch ($row["eregi_codig"]) {
                                case '1';
                                case '9':
                                    ?>
                                        <td class="tabla"><a href="modificar?c=<?php echo $row['regis_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-pencil"></i></a></td>         
                                        <?php 
                                            if(Yii::app()->user->id['usuario']['permiso']==1){
                                        ?>
                                        <td class="tabla"><a href="#" class="btn btn-block btn-info disabled"><i class="fa fa-edit"></i></a></td>
                                        <?php
                                            }   
                                        ?>           
                                    <?php
                                    break;
                                case '2':
                                    ?>
                                        <td class="tabla"> <a href="#" class="btn btn-block btn-info disabled"><i class="fa fa-pencil"></i></a></td> 
                                        <?php 
                                            if(Yii::app()->user->id['usuario']['permiso']==1){
                                        ?>          
                                        <td class="tabla"> <a href="
                                            verificar?c=<?php echo $row['regis_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-edit"></i></a></td>         
                                        <?php
                                            }   
                                        ?>
                                    <?php
                                    break;
                                case '3':
                                    ?>
                                        <td class="tabla"> <a href="#" class="btn btn-block btn-info disabled"><i class="fa fa-pencil"></i></a></td> 
                                        <?php 
                                            if(Yii::app()->user->id['usuario']['permiso']==1){
                                        ?>  
                                        <td class="tabla"> <a href="#" class="btn btn-block btn-info disabled"><i class="fa fa-edit"></i></a></td>
                                        <?php
                                            } 
                                default:
                                    # code...
                                    break;
                            }
                            
                        ?>
                            
                        </tr>
                        <?php
                            }   
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>  
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#buscar').click(function () {
        var formData = new FormData($("#p_personas")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-p_persona').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>
<script>
$('#cantidad').mask('#.##0',{reverse: true,maxlength:false});
$('#punidad').mask('#.##0,00',{reverse: true,maxlength:false});

</script>
