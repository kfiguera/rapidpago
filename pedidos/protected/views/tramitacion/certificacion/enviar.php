<?php $conexion = Yii::app()->db; ?>
<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Solicitudes</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Solicitudes</a></li>
            <li class="active">Enviar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Enviar Solicitudes</h3>
        </div>
        <div class="panel-body">
            <div class="sttabs tabs-style-linebox">
            <nav>
              <ul>
                <li class="tab-current"><a href="#section-linebox-1"><span>Adicional 1</span></a></li>
                <li class=""><a href="#section-linebox-2"><span>Adicional 2</span></a></li>
                <li class=""><a href="#section-linebox-3"><span>Adicional 3</span></a></li>
              </ul>
            </nav>
            <div class="content-wrap text-center">
                <section id="section-linebox-1" class="content-current">
                    <h2>Información Adicional a Bordar Parte 1</h2>
                    <form id='login-form' name='login-form' method="post">
                        <div class="row hide">
                            <div class="col-sm-4">        
                                <div class="form-group">
                                    <label>Pedido *</label>
                                    <?php 
                                        echo CHtml::hiddenField('pedid', $p, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                </div>
                            </div>
                            <div class="col-sm-4">        
                                <div class="form-group">
                                    <label>Código *</label>
                                    <?php 
                                        echo $c;
                                        echo CHtml::hiddenField('codig', $c, array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...')); ?>
                                </div>
                            </div>
                            <div class="col-sm-4">        
                                <div class="form-group">
                                    <label>Paso *</label>
                                    <?php 
                                        echo CHtml::hiddenField('pasos', $pedidos['pedid_pasos'], array('class' => 'form-control', 'placeholder' => "pedido",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">        
                                <div class="form-group">
                                    <label>Color del Cuerpo *</label>
                                    <div class="input-group">

                                    <?php
                                        $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                          FROM pedido_modelo_color a
                                          GROUP BY 1,2
                                          ORDER BY 1";

                                        $result=$conexion->createCommand($sql)->queryAll();

                                        $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');

                                        echo CHtml::dropDownList('ccuer', $pedidos['pedid_ccuer'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['1']['disabled'],'disabled'=>'true')); ?>
                                        <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-1" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">        
                                <div class="form-group">
                                    <label>Color de los Bolsillos *</label>
                                    <div class="input-group">

                                    <?php
                                        $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                          FROM pedido_modelo_color a
                                          GROUP BY 1,2
                                          ORDER BY 1";

                                        $result=$conexion->createCommand($sql)->queryAll();

                                        $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');

                                        echo CHtml::dropDownList('cbols', $pedidos['pedid_cbols'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['1']['disabled'],'disabled'=>'true')); ?>
                                        <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-2" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">        
                                <div class="form-group">
                                    <label>Color de las Mangas *</label>
                                    <div class="input-group">

                                    <?php
                                        $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                          FROM pedido_modelo_color a
                                          GROUP BY 1,2
                                          ORDER BY 1";

                                        $result=$conexion->createCommand($sql)->queryAll();

                                        $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');

                                        echo CHtml::dropDownList('cmang', $pedidos['pedid_cmang'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['1']['disabled'],'disabled'=>'true')); ?>
                                        <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-3" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">        
                                <div class="form-group">
                                    <label>Color Exterior del Gorro *</label>
                                    <div class="input-group">

                                    <?php
                                        $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                          FROM pedido_modelo_color a
                                          GROUP BY 1,2
                                          ORDER BY 1";

                                        $result=$conexion->createCommand($sql)->queryAll();

                                        $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');

                                        echo CHtml::dropDownList('cegor', $pedidos['pedid_cegor'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['1']['disabled'],'disabled'=>'true')); ?>
                                        <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-4" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">        
                                <div class="form-group">
                                    <label>Color Interior del Gorro *</label>
                                    <div class="input-group">

                                    <?php
                                        $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                          FROM pedido_modelo_color a
                                          GROUP BY 1,2
                                          ORDER BY 1";

                                        $result=$conexion->createCommand($sql)->queryAll();

                                        $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');

                                        echo CHtml::dropDownList('cigor', $pedidos['pedid_cigor'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['1']['disabled'],'disabled'=>'true')); ?>
                                        <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-5" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">        
                                <div class="form-group">
                                    <label>Color Puños y Pretina *</label>
                                    <div class="input-group">

                                    <?php
                                        $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                          FROM pedido_modelo_color a
                                          GROUP BY 1,2
                                          ORDER BY 1";

                                        $result=$conexion->createCommand($sql)->queryAll();

                                        $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');

                                        echo CHtml::dropDownList('cppre', $pedidos['pedid_cppre'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['1']['disabled'],'disabled'=>'true')); ?>
                                        <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-6" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">        
                                <div class="form-group">
                                    <label>Cierre *</label>
                                    <?php 
                                        $data=array('1'=>'SI','2'=>'NO');
                                        echo CHtml::dropDownList('cierr', $pedidos['pedid_cierr'], $data, array('ajax' => array(
                                                    'type' => 'POST',
                                                    'url' => CController::createUrl('funciones/SolicitudesOpcionales'), // Controlador que devuelve las p_ciudades relacionadas
                                                    'update' => '#iopci', // id del item que se actualizará
                                                ),'class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                </div>
                            </div>
                            <div class="col-sm-4">        
                                <div class="form-group">
                                    <label>Color Cierre *</label>
                                    <div class="input-group">

                                    <?php
                                        $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                          FROM pedido_modelo_color a
                                          GROUP BY 1,2
                                          ORDER BY 1";

                                        $result=$conexion->createCommand($sql)->queryAll();

                                        $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');

                                        echo CHtml::dropDownList('ccier', $pedidos['pedid_ccier'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$opcional['1']['disabled'],'disabled'=>'true')); ?>
                                        <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-7" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">        
                                <div class="form-group">
                                    <label>Broches *</label>
                                    <?php 

                                        $data=array('1'=>'SIN BROCHE','2'=>'CRUDO','3'=>'GRIS','4'=>'BLANCO');
                                        echo CHtml::dropDownList('broch', $pedidos['pedid_broch'], $data, array('ajax' => array(
                                                    'type' => 'POST',
                                                    'url' => CController::createUrl('funciones/SolicitudesOpcionales'), // Controlador que devuelve las p_ciudades relacionadas
                                                    'update' => '#iopci', // id del item que se actualizará
                                                ),'class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                </div>
                            </div>
                        </div>

                    </form>
                </section>
                <section id="section-linebox-2" class="">

                    <h2>Información adicional a Bordar Parte 2</h2>
                    <h3>Pecho</h3>
                    <div class="row">
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Posición *</label>
                                <?php 
                                    
                                    $data=array('1'=>'DERECHO', '2'=>'IZQUIERDO', '3'=>'AMBOS');
                                    echo CHtml::dropDownList('pposi', $pedidos['pedid_pposi'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                            </div>
                        </div>
                    
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Fuente *</label>
                                <div class="input-group">
                                <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT a.fuent_codig, fuent_descr
                                          FROM pedido_fuente a
                                          GROUP BY 1,2
                                          ORDER BY 1";
                                    $result=$conexion->createCommand($sql)->queryAll();

                                    $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                    echo CHtml::dropDownList('pfuen', $pedidos['pedid_pfuen'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                    
                                    <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-13" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Color *</label>
                                <div class="input-group">
                                <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                          FROM pedido_modelo_color a
                                          GROUP BY 1,2
                                          ORDER BY 1";
                                    $result=$conexion->createCommand($sql)->queryAll();

                                    $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                                    echo CHtml::dropDownList('pcolo', $pedidos['pedid_pcolo'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                    
                                    <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-14" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Lleva Imagen Bordada *</label>
                                <?php 
                                    
                                    $data=array('1'=>'SI', '2'=>'NO');
                                    echo CHtml::dropDownList('plima', $pedidos['pedid_plima'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                            </div>
                        </div>
                        
                        
                    
                        <?php
                        if($pedidos['pedid_plima']=='1'){
                            $opcional['1']['hide']='';
                        }else{
                            $opcional['1']['hide']='hide';  
                        }
                        ?>
                        <div class=" <?php echo $opcional['1']['hide'] ?>" id="opcional-1">
                            <div class="col-sm-8">        
                                <div class="numero1 form-group">
                                    <label>Imagen Libre</label>
                                    <div  class="input-group image-preview" data-placement="top" >  

                                        <img id="dynamic">
                                        <!-- image-preview-filename input [CUT FROM HERE]-->
                                        <?php 
                                            $ruta=$pedidos['pedid_prima'];
                                            $nombre=explode('/', $ruta);
                                            $nombre=end($nombre);
                                            if($nombre){
                                                $opcional['1']['hide']='';
                                                $opcional['1']['show']='true';
                                            }else{
                                                $opcional['1']['hide']='hide';
                                                $opcional['1']['show']='false';
                                            }
                                        ?>
                                        <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="true" value="<?php echo $nombre ?>" > <!-- don't give a name === doesn't send on POST/GET -->
                                        <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-numero-1" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                                                           
                                    </div> 
                                </div> 
                                <div class="hide" id="popover-numero-1">
                                    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$ruta ?>" class="img-responsive">
                                </div>
                                
                            </div>
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">        
                            <div class="form-group">
                                <label>Observaciones</label>
                                <?php 
                                    echo CHtml::textArea('pobse', $pedidos['pedid_pobse'], array('class' => 'form-control', 'placeholder' => "Observaciones",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                    
                                   
                            </div>
                        </div> 
                    </div>
                    <hr>
                    <h3>Gorro</h3>
                    <div class="row">
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Posición *</label>
                                <?php 
                                    
                                    $data=array('1'=>'DERECHO', '2'=>'IZQUIERDO', '3'=>'AMBOS');
                                    echo CHtml::dropDownList('gposi', $pedidos['pedid_gposi'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$gorro,'disabled'=>'true')); ?>
                            </div>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Fuente *</label>
                                <div class="input-group">
                                <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT a.fuent_codig, fuent_descr
                                          FROM pedido_fuente a
                                          GROUP BY 1,2
                                          ORDER BY 1";
                                    $result=$conexion->createCommand($sql)->queryAll();

                                    $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                    echo CHtml::dropDownList('gfuen', $pedidos['pedid_gfuen'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$gorro,'disabled'=>'true')); ?>
                                    
                                    <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-15" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Color *</label>
                                <div class="input-group">
                                <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                          FROM pedido_modelo_color a
                                          GROUP BY 1,2
                                          ORDER BY 1";
                                    $result=$conexion->createCommand($sql)->queryAll();

                                    $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                                    echo CHtml::dropDownList('gcolo', $pedidos['pedid_gcolo'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>$gorro,'disabled'=>'true')); ?>
                                    
                                    <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-16" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">        
                            <div class="form-group">
                                <label>Lleva Imagen Bordada *</label>
                                <?php 
                                    
                                    $data=array('1'=>'SI', '2'=>'NO');
                                    echo CHtml::dropDownList('glima', $pedidos['pedid_glima'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                            </div>
                        </div>
                        <?php
                        if($pedidos['pedid_glima']=='1'){
                            $opcional['2']['hide']='';
                        }else{
                            $opcional['2']['hide']='hide';  
                        }
                        ?>
                        <div class="<?php echo $opcional['2']['hide'] ?>" id="opcional-2">
                            <div class="col-sm-8">        
                                <div class="numero2 form-group">
                                    <label>Imagen Libre</label>
                                    <div  class="input-group image-preview" data-placement="top" >  

                                        <img id="dynamic">
                                        <!-- image-preview-filename input [CUT FROM HERE]-->
                                        <?php 
                                            $ruta=$pedidos['pedid_grima'];
                                            $nombre=explode('/', $ruta);
                                            $nombre=end($nombre);
                                            if($nombre){
                                                $opcional['2']['hide']='';
                                                $opcional['2']['show']='true';
                                            }else{
                                                $opcional['2']['hide']='hide';
                                                $opcional['2']['show']='false';
                                            }
                                        ?>
                                        <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="true" value="<?php echo $nombre ?>" > <!-- don't give a name === doesn't send on POST/GET -->
                                        <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-numero-2" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                                   
                                    </div> 
                                </div> 
                                <div class="hide" id="popover-numero-2">
                                    <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$ruta ?>" class="img-responsive">
                                </div>
                                
                            </div>
                            
                        </div> 
                    </div>
                    <div class="row">
                        <div class="col-sm-12">        
                            <div class="form-group">
                                <label>Observaciones</label>
                                <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT a.fuent_codig, fuent_descr
                                          FROM pedido_fuente a
                                          GROUP BY 1,2
                                          ORDER BY 1";
                                    $result=$conexion->createCommand($sql)->queryAll();

                                    $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                    echo CHtml::textArea('gobse', $pedidos['pedid_gobse'], array('class' => 'form-control', 'placeholder' => "Observaciones",'prompt'=>'Seleccione...','disabled'=>$gorro,'disabled'=>'true')); ?>
                                    
                                   
                            </div>
                        </div>  
                    </div>
                </section>
                <section id="section-linebox-3" class="">                <h3>Mangas</h3>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Posición *</label>
                            <?php 
                                
                                $data=array('1'=>'DERECHO', '2'=>'IZQUIERDO', '3'=>'AMBOS');
                                echo CHtml::dropDownList('mposi', $pedidos['pedid_mposi'], $data, array('class' => 'form-control', 'placeholder' => "Posición",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                        </div>
                    </div>
                    <?php
                    switch ($pedidos['pedid_mposi']) {
                        case '1':
                            $opcional['1']['disabled']='';
                            $opcional['2']['disabled']='true';
                            break;
                        case '2':
                            $opcional['1']['disabled']='true';
                            $opcional['2']['disabled']='';
                            break;
                        case '3':
                            $opcional['1']['disabled']='';
                            $opcional['2']['disabled']='';
                            break;
                        
                        default:
                            $opcional['1']['disabled']='true';
                            $opcional['2']['disabled']='true';
                            break;
                    }
                    ?>
                    
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Fuente *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.fuent_codig, fuent_descr
                                      FROM pedido_fuente a
                                      GROUP BY 1,2
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                echo CHtml::dropDownList('mfuen', $pedidos['pedid_mfuen'], $data, array('class' => 'form-control', 'placeholder' => "Fuente",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-17" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Color *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                      FROM pedido_modelo_color a
                                      GROUP BY 1,2
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                                echo CHtml::dropDownList('mcolo', $pedidos['pedid_mcolo'], $data, array('class' => 'form-control', 'placeholder' => "Color",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-18" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Lleva Imagen Bordada *</label>
                            <?php 
                                
                                $data=array('1'=>'SI', '2'=>'NO');
                                echo CHtml::dropDownList('limag', $pedidos['pedid_limag'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                        </div>
                    </div>
                    
                    
                
                <?php
                if($pedidos['pedid_limag']=='1'){
                    $opcional['3']['hide']='';
                }else{
                    $opcional['3']['hide']='hide';  
                }
                ?>
                <div class=" <?php echo $opcional['3']['hide'] ?>" id="opcional">
                        <div class="col-sm-8">        
                            <div class="numero1 form-group">
                                <label>Imagen Libre</label>
                                <div  class="input-group image-preview" data-placement="top" >  

                                    <img id="dynamic">
                                    <!-- image-preview-filename input [CUT FROM HERE]-->
                                    <?php 
                                        $ruta=$pedidos['pedid_mrima'];
                                        $nombre=explode('/', $ruta);
                                        $nombre=end($nombre);
                                        if($nombre){
                                            $opcional['3']['hide']='';
                                            $opcional['3']['show']='true';
                                        }else{
                                            $opcional['3']['hide']='hide';
                                            $opcional['3']['show']='false';
                                        }
                                    ?>
                                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="true" value="<?php echo $nombre ?>" > <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-numero-3" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                               
                                </div> 
                            </div> 
                            <div class="hide" id="popover-numero-3">
                                <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$ruta ?>" class="img-responsive">
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observaciones</label>
                            <?php 
                                echo CHtml::textArea('mobse', $pedidos['pedid_mobse'], array('class' => 'form-control', 'placeholder' => "Observaciones",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                
                               
                        </div>
                    </div>
                    
                </div>
                <hr>
                <h3>Espalda</h3>
                <div class="row">
                    <div class="col-sm-12"> 
                        <label>Alto *</label>
                    </div>
                    
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Fuente *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.fuent_codig, fuent_descr
                                      FROM pedido_fuente a
                                      GROUP BY 1,2
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                echo CHtml::dropDownList('eafue', $pedidos['pedid_eafue'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-19" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Color *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                      FROM pedido_modelo_color a
                                      GROUP BY 1,2
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                                echo CHtml::dropDownList('eacol', $pedidos['pedid_eacol'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-20" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Texto a Bordar *</label>
                            <?php 
                                
                                echo CHtml::textarea('eatbo', $pedidos['pedid_eatbo'], array('class' => 'form-control', 'placeholder' => "Texto a Bordar",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"> 
                        <label>Bajo *</label>
                    </div>
                    
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Fuente *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.fuent_codig, fuent_descr
                                      FROM pedido_fuente a
                                      GROUP BY 1,2
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                echo CHtml::dropDownList('ebfue', $pedidos['pedid_ebfue'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-21" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Color *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                      FROM pedido_modelo_color a
                                      GROUP BY 1,2
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                                echo CHtml::dropDownList('ebcol', $pedidos['pedid_ebcol'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-22" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Texto a Bordar *</label>
                            <?php 
                                
                                echo CHtml::textArea('ebtbo', $pedidos['pedid_ebtbo'], array('class' => 'form-control', 'placeholder' => "Texto a Bordar",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12"> 
                        <label>Apodo Espalda *</label>
                    </div>

                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Fuente *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.fuent_codig, fuent_descr
                                      FROM pedido_fuente a
                                      GROUP BY 1,2
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'fuent_codig','fuent_descr');
                                echo CHtml::dropDownList('aefue', $pedidos['pedid_aefue'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-23" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Color *</label>
                            <div class="input-group">
                            <?php 
                            $conexion = Yii::app()->db;
                            $sql="SELECT a.mcolo_codig, concat(a.mcolo_numer,' - ',a.mcolo_descr) mcolo_descr
                                      FROM pedido_modelo_color a
                                      GROUP BY 1,2
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();

                                $data=CHtml::listData($result,'mcolo_codig','mcolo_descr');
                                echo CHtml::dropDownList('aecol', $pedidos['pedid_aecol'], $data, array('class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                
                                <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-24" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                            <div class="numero2 form-group">
                                <label>Imagen Principal</label>
                                <div  class="input-group image-preview" data-placement="top" >  

                                    <img id="dynamic">
                                    <!-- image-preview-filename input [CUT FROM HERE]-->
                                    <?php 
                                        $ruta=$pedidos['pedid_iprut'];
                                        $nombre=explode('/', $ruta);
                                        $nombre=end($nombre);
                                        if($nombre){
                                            $opcional['4']['hide']='';
                                            $opcional['4']['show']='true';
                                        }else{
                                            $opcional['4']['hide']='hide';
                                            $opcional['4']['show']='false';
                                        }
                                    ?>
                                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="true" value="<?php echo $nombre ?>" > <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-numero-4" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                               
                                </div> 
                            </div> 
                            <div class="hide" id="popover-numero-4">
                                <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$ruta ?>" class="img-responsive">
                            </div>

                        </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observaciones</label>
                            <?php 
                                echo CHtml::textArea('eobse', $pedidos['pedid_eobse'], array('class' => 'form-control', 'placeholder' => "Observaciones",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                                
                               
                        </div>
                    </div>
                    
                </div>
                <hr>
                <h3 >Dibujo simple del poleron completo por ambos lados</h3>
                <div class="row">
                    <div class="col-sm-6">        
                            <div class="numero3 form-group">
                                <label>Imagen Frontal</label>
                                <div  class="input-group image-preview" data-placement="top" >  

                                    <img id="dynamic">
                                    <!-- image-preview-filename input [CUT FROM HERE]-->
                                    <?php 
                                        $ruta=$pedidos['pedid_rifro'];
                                        $nombre=explode('/', $ruta);
                                        $nombre=end($nombre);
                                        if($nombre){
                                            $opcional['5']['hide']='';
                                            $opcional['5']['show']='true';
                                        }else{
                                            $opcional['5']['hide']='hide';
                                            $opcional['5']['show']='false';
                                        }
                                    ?>
                                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="true" value="<?php echo $nombre ?>" > <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-numero-5" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                               
                                </div> 
                            </div> 
                            <div class="hide" id="popover-numero-5">
                                <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$ruta ?>" class="img-responsive">
                            </div>
                           
                        </div>
                    <div class="col-sm-6">        
                            <div class="numero4 form-group">
                                <label>Imagen Espalda</label>
                                <div  class="input-group image-preview" data-placement="top" >  

                                    <img id="dynamic">
                                    <!-- image-preview-filename input [CUT FROM HERE]-->
                                    <?php 
                                        $ruta=$pedidos['pedid_riesp'];
                                        $nombre=explode('/', $ruta);
                                        $nombre=end($nombre);
                                        if($nombre){
                                            $opcional['6']['hide']='';
                                            $opcional['6']['show']='true';
                                        }else{
                                            $opcional['6']['hide']='hide';
                                            $opcional['6']['show']='false';
                                        }
                                    ?>
                                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="true" value="<?php echo $nombre ?>" > <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-addon" data-placement="top" data-toggle="popover" data-container="body" type="button" data-html="true" href="#" id="img-numero-6" data-original-title="" title="Vista previa"><i class="fa fa-search"></i> Vista previa</span>
                               
                                </div> 
                            </div> 
                            <div class="hide" id="popover-numero-6">
                                <img src="<?php echo Yii::app()->request->getBaseUrl(true).'/'.$ruta ?>" class="img-responsive">
                            </div>
                            
                        </div>
                </div>
                </section>
            </div><!-- /content -->
                </div>
            </div>
        
        <div class="panel-footer">
            <div class="row controls">
                <div class="col-sm-6 ">
                    <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                </div>
                <div class="col-sm-6 ">
                    <button id="guardar" type="button" class="btn-block btn btn-info">Enviar  </button>
                </div>
            </div>
        </div>
    </div>  
</div>

<script>
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                canti: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Total a Solicitar" es obligatorio',
                        }
                    }
                },
                mcant: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Cantidad de Modelos" es obligatorio',
                        }
                    }
                },
                color: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Color del Cuerpo" es obligatorio',
                        }
                    }
                }



            }
        });
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'enviar',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('../../reportes/pedidos/pdf?c=<?php echo $c; ?>', '_blank');
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>