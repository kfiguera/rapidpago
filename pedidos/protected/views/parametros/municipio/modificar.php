<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Municipios</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Parametros</a></li>
            <li><a href="#">Municipios</a></li>
            <li class="active">Modificar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Modficar Municipios</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row hide">
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>codigo</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <?php echo CHtml::hiddenField('codig', $roles['munic_codig'], array('class' => 'form-control', 'placeholder' => "Correo")); ?>

                            </div>
                        </div>
                    </div>
                    
                </div>

                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Paises *</label>
                            <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT a.paise_codig, a.paise_descr
                                      FROM p_paises a
                                      JOIN p_estados b ON (a.paise_codig = b.paise_codig)
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'paise_codig','paise_descr');
                                echo CHtml::dropDownList('paise', $roles['paise_codig'], $data, array('ajax' => array(
                                            'type' => 'POST',
                                            'url' => CController::createUrl('funciones/ComboEstado'), // Controlador que devuelve las p_ciudades relacionadas
                                            'update' => '#estad', // id del item que se actualizará
                                        ),'class' => 'form-control', 'placeholder' => "Color del Cuerpo",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Estados *</label>
                            <?php 
                                $sql="SELECT a.estad_codig, a.estad_descr
                                      FROM p_estados a 
                                      WHERE paise_codig='".$roles['paise_codig']."'
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'estad_codig','estad_descr');
                                echo CHtml::dropDownList('estad', $roles['estad_codig'], $data, array('class' => 'form-control', 'placeholder' => "Paises",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Descripción</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php echo CHtml::textField('descr', $roles['munic_descr'], array('class' => 'form-control', 'placeholder' => "Descripción")); ?>

                            </div>
                        </div>
                    </div>
                    
                </div>
                
                
                    <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-4 ">
                            <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                        </div>
                    </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
<script>
    $(document).ready(function () {
        $('#fnaci').mask('00/00/0000');
        $('#desde').mask('00/00/0000');
        $('#cambi').mask('#.##0,00',{reverse: true,maxlength:false}); 
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                
                descr: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Descripción" es obligatorio',
                        }
                    }
                },
                valor: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Siglas" es obligatorio',
                        }
                    }
                },
                cambi: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tasa de Cambio" es obligatorio',
                        }
                    }
                },


            }
        });
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'modificar',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>