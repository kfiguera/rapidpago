<table id='auditoria' class="table table-bordered table-hover dataTable">
    <thead>
        <tr>
            <th width="2%">
                #
            </th>
            <th>
                Cédula
            </th>
            <th>
                Nombre
            </th>
            <th>
                Apellido
            </th>
            <th width="5%">
                Consultar
            </th>
            <th width="5%">
                Modificar
            </th>
            <th width="5%">
                Eliminar
            </th>
        </tr>
    </thead>

    <tbody>
        <?php
        $sql = "SELECT * FROM p_persona a ".$condicion;
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $p_persona = $command->query();
        $i=0;
        while (($row = $p_persona->read()) !== false) {
            $i++;
        ?>
        <tr>
            <td class="tabla"><?php echo $i ?></td>
            <td class="tabla"><?php echo $row['nacio_value'] . '-' . $row['perso_cedul'] ?></td>
            <td class="tabla"><?php echo $row['perso_pnomb'] . ' ' . $row['perso_snomb'] ?></td>
            <td class="tabla"><?php echo $row['perso_papel'] . ' ' . $row['perso_sapel'] ?></td>
            <td class="tabla"><a href="consultar?c=<?php echo $row['perso_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-search"></i></a></td>
            <td class="tabla"><a href="modificar?c=<?php echo $row['perso_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-pencil"></i></a></td>
            <td class="tabla"><a href="eliminar?c=<?php echo $row['perso_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-close"></i></a></td>

        </tr>
        <?php
            }   
        ?>
    </tbody>
</table>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable(); 
    });
</script>
