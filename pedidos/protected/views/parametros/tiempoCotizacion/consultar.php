<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Tiempo Cotización</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Parametros</a></li>
            <li><a href="#">Tiempo Cotización</a></li>
            <li class="active">Consultar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Consultar Tiempo Cotización</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Valor</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php 
                                echo CHtml::textField('value', $roles['tcoti_value'], array('class' => 'form-control', 'placeholder' => "Value", 'disabled'=>'true')); ?>

                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Estatus</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php 
                                    $sql="SELECT * FROM p_parametros_estatus";
                                    
                                    $result=$conexion->createCommand($sql)->queryAll();
                                    $data=CHtml::listData($result,'pesta_codig','pesta_descr');
                                    echo CHtml::dropDownList('estat', $roles['pesta_codig'], $data, array('class' => 'form-control', 'prompt' => "Seleccione...", 'disabled'=>'true')); ?>

                            </div>
                        </div>
                    </div>
                    
                </div>
               
                
                    <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-12">
                            <a href="listado" type="reset" class="btn-block btn btn-info">Volver </a>
                        </div>
                    </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
