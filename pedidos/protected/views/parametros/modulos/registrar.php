<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Modulos</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Parametros</a></li>
            <li><a href="#">Modulos</a></li>
            <li class="active">Registrar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Registrar Modulos</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Código</label>
                            
                                <?php 
                                echo CHtml::textField('codig', $roles['modul_codig'], array('class' => 'form-control', 'placeholder' => "Código")); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Padre</label>
                                <?php
                                $sql = "SELECT * FROM seguridad_modulos WHERE modul_padre=0";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'modul_codig','modul_descr');
                                echo CHtml::dropDownlist('padre', $roles['modul_padre'],$data, array('class' => 'form-control select2', 'placeholder' => "Código",'prompt'=>'Seleccione...')); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Descripción</label>
                            
                                <?php 
                                echo CHtml::textField('descr', $roles['modul_descr'], array('class' => 'form-control', 'placeholder' => "Descripción")); ?>

                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Ruta</label>
                            
                                <?php 
                                echo CHtml::textField('ruta', $roles['modul_ruta'], array('class' => 'form-control', 'placeholder' => "Ruta")); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Orden</label>
                            
                                <?php 
                                echo CHtml::textField('orden', $roles['modul_orden'], array('class' => 'form-control', 'placeholder' => "Orden")); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Estatus</label>
                                <?php
                                $sql = "SELECT * FROM p_modulos_estatus";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'emodu_codig','emodu_descr');
                                echo CHtml::dropDownlist('estat', $roles['modul_padre'],$data, array('class' => 'form-control', 'placeholder' => "Código",'prompt'=>'Seleccione...')); ?>

                        </div>
                    </div>                    
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Superior</label>
                            
                                <?php
                                $sql = "SELECT * FROM p_modulos_estatus";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=array('0'=>'No','1'=>'Si');
                                echo CHtml::dropDownlist('super', $roles['modul_bpadr'],$data, array('class' => 'form-control', 'placeholder' => "Código",'prompt'=>'Seleccione...')); ?>

                        </div>
                    </div>
                    <div class="col-sm-8">        
                        <div class="form-group">
                            <label>Icono</label>
                            
                                <?php 
                                echo CHtml::textField('icono', $roles['modul_icono'], array('class' => 'form-control', 'placeholder' => "Orden")); ?>

                        </div>
                    </div>
                                       
                </div>
                
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observación</label>
                            
                                <?php 
                                echo CHtml::textArea('obser', $roles['modul_obser'], array('class' => 'form-control', 'placeholder' => "Observación")); ?>

                        </div>
                    </div>
                    
                </div>
                
                
                    <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-4 ">
                            <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                        </div>
                    </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
<script>
    $(document).ready(function () {
        $('#fnaci').mask('00/00/0000');
        $('#desde').mask('00/00/0000');
        $('#cambi').mask('#.##0,00',{reverse: true,maxlength:false});    
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                descr: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Descripción" es obligatorio',
                        }
                    }
                },value: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Siglas" es obligatorio',
                        }
                    }
                }
                ,cambi: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tasa de Cambio" es obligatorio',
                        }
                    }
                }




            }
        });
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'registrar',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>