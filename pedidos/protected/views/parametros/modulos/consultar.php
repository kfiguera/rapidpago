<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Modulos</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Parametros</a></li>
            <li><a href="#">Modulos</a></li>
            <li class="active">Consultar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Consultar Modulos</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Código</label>
                            
                                <?php 
                                echo CHtml::textField('codig', $roles['modul_codig'], array('class' => 'form-control', 'placeholder' => "Código", 'disabled'=>'true')); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Padre</label>
                                <?php
                                $sql = "SELECT * FROM seguridad_modulos WHERE modul_padre=0";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'modul_codig','modul_descr');
                                echo CHtml::dropDownlist('padre', $roles['modul_padre'],$data, array('class' => 'form-control', 'placeholder' => "Código", 'disabled'=>'true','prompt'=>'Seleccione...')); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Descripción</label>
                            
                                <?php 
                                echo CHtml::textField('descr', $roles['modul_descr'], array('class' => 'form-control', 'placeholder' => "Descripción", 'disabled'=>'true')); ?>

                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Ruta</label>
                            
                                <?php 
                                echo CHtml::textField('ruta', $roles['modul_ruta'], array('class' => 'form-control', 'placeholder' => "Ruta", 'disabled'=>'true')); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Orden</label>
                            
                                <?php 
                                echo CHtml::textField('orden', $roles['modul_orden'], array('class' => 'form-control', 'placeholder' => "Orden", 'disabled'=>'true')); ?>

                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Estatus</label>
                                <?php
                                $sql = "SELECT * FROM p_modulos_estatus";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'emodu_codig','emodu_descr');
                                echo CHtml::dropDownlist('estat', $roles['modul_padre'],$data, array('class' => 'form-control', 'placeholder' => "Código", 'disabled'=>'true','prompt'=>'Seleccione...')); ?>

                        </div>
                    </div>                    
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Superior</label>
                            
                                <?php
                                $sql = "SELECT * FROM p_modulos_estatus";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=array('0'=>'No','1'=>'Si');
                                echo CHtml::dropDownlist('estat', $roles['modul_bpadr'],$data, array('class' => 'form-control', 'placeholder' => "Código", 'disabled'=>'true','prompt'=>'Seleccione...')); ?>

                        </div>
                    </div>
                    <div class="col-sm-8">        
                        <div class="form-group">
                            <label>Icono</label>
                            
                                <?php 
                                echo CHtml::textField('icono', $roles['modul_icono'], array('class' => 'form-control', 'placeholder' => "Orden", 'disabled'=>'true')); ?>

                        </div>
                    </div>
                                       
                </div>
                
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Observación</label>
                                <?php 
                                echo CHtml::textArea('obser', $roles['modul_obser'], array('class' => 'form-control', 'placeholder' => "Observación", 'disabled'=>'true')); ?>

                        </div>
                    </div>
                    
                </div>
                
               
                
                    <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-12">
                            <a href="listado" type="reset" class="btn-block btn btn-info">Volver </a>
                        </div>
                    </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
