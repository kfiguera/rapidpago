<div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Persona</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
          <ol class="breadcrumb">
            <li><a href="#">Parametros</a></li>
            <li><a href="#">Persona</a></li>
            <li class="active">Listado</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
<div class="row">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Criterio de Busqueda</h3>
        </div>
        <div class="panel-body" >
            <?php
                $form = $this->beginWidget('CActiveForm', array('id' => 'p_personas', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            ?>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Nacionalidad</label>
                        <select class="form-control" name="p_nacionalidad" id="p_nacionalidad">
                            <option value="">Seleccione</option>
                            <option value="E">Extranjero</option>
                            <option value="V">Venezolana</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Cédula</label>
                        <input type="text" class="form-control" name="cedula" id="cedula" placeholder="Cédula">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Nombres</label>
                        <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombres">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Apellidos</label>
                        <input type="text" class="form-control" name="apellido" id="apellido" placeholder="Apellidos">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <button type="button" id="buscar" class="btn btn-block btn-info">Buscar</button>
                </div>
                <div class="col-sm-4">
                    <button type="reset" class="btn btn-block btn-default">Limpiar</button>
                </div>
                <div class="col-sm-4">
                    <a href="registrar" class="btn btn-block btn-info" >Nuevo</a>
                </div>
            </div>

            <?php $this->endWidget(); ?>

        </div><!-- form -->
    </div> 
</div>
<div class="row">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Listado</h3>
        </div>
        <div class="panel-body" >
            
            <div class="row">
                <div class="col-sm-12 table-responsive" id='listado-p_persona'>
                    <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                        <thead>
                            <tr>
                                <th width="2%">
                                    #
                                </th>
                                <th>
                                    Cédula
                                </th>
                                <th>
                                    Nombre
                                </th>
                                <th>
                                    Apellido
                                </th>
                                <th width="5%">
                                    Consultar
                                </th>
                                <th width="5%">
                                    Modificar
                                </th>
                                <th width="5%">
                                    Eliminar
                                </th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                            $sql = "SELECT * FROM p_persona";
                            
                            $connection = Yii::app()->db;
                            $command = $connection->createCommand($sql);
                            $p_persona = $command->query();
                            $i=0;
                            while (($row = $p_persona->read()) !== false) {
                                $i++;
                            ?>
                            <tr>
                                <td class="tabla"><?php echo $i ?></td>
                                <td class="tabla"><?php echo $row['nacio_value'] . '-' . number_format($row['perso_cedul'],0,',','.') ?></td>
                                <td class="tabla"><?php echo $row['perso_pnomb'] . ' ' . $row['perso_snomb'] ?></td>
                                <td class="tabla"><?php echo $row['perso_papel'] . ' ' . $row['perso_sapel'] ?></td>
                                <td class="tabla"><a href="consultar?c=<?php echo $row['perso_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-search"></i></a></td>
                                <td class="tabla"><a href="modificar?c=<?php echo $row['perso_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-pencil"></i></a></td>
                                <td class="tabla"><a href="eliminar?c=<?php echo $row['perso_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-close"></i></a></td>

                            </tr>
                            <?php
                                }   
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>  
</div>  
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#buscar').click(function () {
        var formData = new FormData($("#p_personas")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-p_persona').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>
<script>
$('#cedula').mask('#.##0',{reverse: true,maxlength:false});

</script>
