<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Precio de Venta</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Parametros</a></li>
            <li><a href="#">Precio de Venta</a></li>
            <li class="active">Consultar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Consultar Precio de Venta</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <div class="form-group">
                                <label>Modelo *</label>
                                <?php
                                    $sql="SELECT *
                                      FROM inventario a
                                      JOIN inventario_seriales b ON (a.inven_codig = b.inven_codig AND b.almac_codig = '1')
                                      WHERE a.tinve_codig='1'
                                      ORDER BY 2";

                                    $result=$conexion->createCommand($sql)->queryAll();

                                    $data=CHtml::listData($result,'inven_codig','inven_descr');

                                    echo CHtml::dropDownList('inven', $roles['inven_codig'], $data, array('class' => 'form-control', 'placeholder' => "Modelo",'prompt'=>'Seleccione...','id'=>'inven_'.$i.'', 'readonly' => 'true')); ?>
                                   
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Monto</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php 
                                echo CHtml::textField('value', $this->funciones->transformarMonto_v($roles['pvent_value'],2), array('class' => 'form-control', 'placeholder' => "Monto", 'readonly' => 'true')); ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Estatus</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php 
                                    $sql="SELECT * FROM p_parametros_estatus WHERE pesta_codig in (1,2)";
                                    
                                    $result=$conexion->createCommand($sql)->queryAll();
                                    $data=CHtml::listData($result,'pesta_codig','pesta_descr');
                                    echo CHtml::dropDownList('estat', $roles['pesta_codig'], $data, array('class' => 'form-control', 'prompt' => "Seleccione...")); ?>

                            </div>
                        </div>
                    </div>
                </div>
               
                
                    <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-12">
                            <a href="listado" type="reset" class="btn-block btn btn-info">Volver </a>
                        </div>
                    </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
