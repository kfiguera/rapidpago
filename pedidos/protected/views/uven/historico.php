
<?php
//Mostrar Errores del controlador
foreach (Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="alert alert-' . $key . '">' . $message . "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> </div>\n";
}
?> 
<div id="loginbox" style="margin-top:50px;" >
    <div class="modal"></div>    
    <h1 class="titulo">Histórico de Servicios</h1> 



    <div class="panel panel-danger " >


        <div style="padding-top:30px" class="panel-body" >

            <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
            
            <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'subir-p_personas', 'htmlOptions' => array('method' => 'post', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')));
            $user = Yii::app()->user->id->tgene_permi;
            $conexion=yii::app()->db;
            $tipo='2';
            $usuartipo = Yii::app()->user->id['usuario']->usuar_tipou ;
            $usuarcodigo = Yii::app()->user->id['usuario']->usuar_codig ;
            $sql = "SELECT s.perso_client, s.perso_taxis, 
            s.servi_fcrea fecha, c.usuar_apell as apel, 
            c.usuar_nombr as cliente, t.usuar_apell as aplt, 
            t.usuar_nombr as taxista, s.servi_costo as costo, 
            s.servi_rutas as ruta, sta.tgene_descr as rd_preregistro_estatus, 
            c.usuar_tipou as tipocliente 
            FROM admin_servicios as s 
            inner join admin_usuarios c on (s.perso_client = c.usuar_codig) 
            inner join admin_tgenerica sta on (s.tgene_statu = sta.tgene_codig)
            left join admin_usuarios t on (s.perso_taxis = t.usuar_codig) 
            where  s.tgene_statu = 7 ";
            switch ($usuartipo) {
                case '1':
                case '4':
                    $sql.=" and s.perso_client = ".$usuarcodigo;
                    break;
                case '3':
                    $sql.=" and s.perso_taxis = ".$usuarcodigo;
                    break;
                default:
                    # code...
                    break;
            }
        
             //echo $sql;
            
            
            ?>
            <div class="table-responsive">
                <table id="auditoria" class="table table-bordered table-hover dataTable" style="width: 100%">
                <thead>
                    <tr>
                        <?php
                        //usuario pasajero o corporativo
                        if (Yii::app()->user->id['usuario']->usuar_tipou == '1' || Yii::app()->user->id['usuario']->usuar_tipou == '3'){
                        ?>
                        <th width="2%">
                            #
                        </th>
                        <th width="10%">
                            Fecha
                        </th>
                        <th width="40%">
                            Trayecto
                        </th>
                        <th width="10%">
                            Costo
                        </th>
                        <th width="15%">
                            Taxista
                        </th>
                        <th width="15%">
                            Estatus
                        </th>
                        <?php
                        //usuario taxista
                    }elseif (Yii::app()->user->id['usuario']->usuar_tipou == '2' ){
                        ?>
                        <th width="2%">
                            #
                        </th>
                        <th width="8%">
                            Fecha
                        </th>
                        <th width="10%">
                            Tipo de Cliente
                        </th>
                        <th width="10%">
                            Cliente
                        </th>
                        <th width="40%">
                            Trayecto
                        </th>
                        <th width="10%">
                            Costo
                        </th>
                        
                        <th width="20%">
                            Estatus
                        </th>
                        <?php
                    }elseif (Yii::app()->user->id['usuario']->usuar_tipou == '4' ){
                        //usuario administrador
                        ?>
                        <th width="2%">
                            #
                        </th>
                        <th width="8%">
                            Fecha
                        </th>
                        <th width="10%">
                            Taxista
                        </th>
                        <th width="10%">
                            Tipo de Cliente
                        </th>
                        <th width="10%">
                            Cliente
                        </th>
                        <th width="30%">
                            Trayecto
                        </th>
                        <th width="10%">
                            Costo
                        </th>
                        <th width="20%">
                            Estatus
                        </th>

                        <?php
                    }
                        //echo $sql;
                        $resultado=$conexion->createCommand($sql)->queryRow();
                        
                        ?>
                        
                    </tr>
                </thead>

                <tbody>
                    <?php                   
                    
                    $result=$conexion->createCommand($sql)->query();
                    $i=0;

                    while (($row=$result->read())!=false) {
                       $i++;
                       $taxista = "Sin asignar";
                       if($taxista==''){
                            $taxista = $row['taxista']." ".$row['aplt'];
                       }
                       switch ($usuartipo) {
                           case '1':
                           case '3':
                               echo "<tr>";
                               echo "<td>".$i."</td>";
                               echo "<td>".$row['fecha']."</td>";
                               echo "<td>".$row['ruta']."</td>";
                               echo "<td>".number_format($row['costo'],2,',','.')."</td>";
                               echo "<td>".$taxista."</td>";
                               echo "<td>".$row['estatus']."</td>";
                               echo "</tr>";
                               break;
                           case '2':
                               echo "<tr>";
                               echo "<td>".$i."</td>";
                               echo "<td>".$row['fecha']."</td>";
                               if($row['tipocliente'] ==1){
                                echo "<td>Pasajero</td>";
                               }else{
                                echo "<td>Coporativo</td>";
                               }
                               echo "<td>".$row['cliente']." ".$row['apel']."</td>";
                               echo "<td>".$row['ruta']."</td>";
                               echo "<td>".number_format($row['costo'],2,',','.')."</td>";
                               
                               echo "<td>".$estatu."</td>";
                               echo "</tr>";
                               break;
                            case '4':
                                echo "<tr>";
                                echo "<td>".$i."</td>";
                                echo "<td>".$row['fecha']."</td>";
                                echo "<td>".$row['cliente']." ".$row['apel']."</td>";
                                if($row['tipocliente'] ==1){
                                    echo "<td>Pasajero</td>";
                                }else{
                                    echo "<td>Coporativo</td>";
                                }
                                echo "<td>".$row['taxista']." ".$row['aplt']."</td>";
                                echo "<td>".$row['ruta']."</td>";
                                echo "<td>".number_format($row['costo'],2,',','.')."</td>";
                                
                                echo "<td>".$estatu."</td>";
                                echo "</tr>";
                                break;
                           default:
                               # code...
                               break;
                       }
                    }
                    ?>
                </tbody>
            </table>
            </div>
            

            <?php $this->endWidget(); ?>

        </div><!-- form -->
    </div>  
</div>

<!-- Modal -->
<style type="text/css">
    .modal-header{
        background-color: #FFBF1C;
        color: #000;
    }
</style>

<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <div id='form-modal'>
            
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#aprobar').click(function (e) {
            var formData = new FormData($("#form-modal")[0]);
            e.preventDefault();
            $.ajax({
                dataType: "json",
                url: 'actualizarauditoria',
                type: 'POST',
                cache: false,
                data: formData,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    //alert(response['success']);
                    if (response['success'] == 'true') {
                        bootbox.dialog({
                            message: response['msg'],
                            title: "Exito!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                    callback: function(){
                                       window.open('seguridad_usuarios', '_parent');
                                    }
                                },
                            }
                        });
                        $('.close').click( function(){
                            window.open('seguridad_usuarios?t=' + t, '_parent');
                        });
                    } else {

                        bootbox.dialog({
                            message: response['msg'],
                            title: "Información!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                },
                            }
                        });
                        setTimeout(function () {
                            $('.close').click();
                        }, 1000);
                        //alert('error');
                        // $("#subir").removeAttr('disabled');
                    }

                }
            });

        });
        

    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find("#form-modal").load(link.attr("href"));
    });
</script>
