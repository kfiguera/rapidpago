

<div class="registrar" >
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="text-center">Tarifas</h3>
                </div>
                
                <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        
                        <tr>
                            <th class="text-center titulo-costo" width="34%" >Tarifa</th>
                            <th class="text-center titulo-costo" width="33%" >Costo</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                    $sql='select * from admin_tarifador where tgene_statu=1';
                    $conexion=Yii::app()->db;
                    $row=$conexion->createCommand($sql)->queryRow();

                    echo '<tr>';
                        echo '<th>Costo por Kilómetro</th>';
                        echo '<td> Bs. '.$row["tarif_kilom"].'</td>';
                    echo '</tr>';
                    echo '<tr>';
                        echo '<th>Hora diurna desde</th>';
                        echo '<td> Bs. '.$row["tarif_diurn"].'</td>';
                    echo '</tr>';
                    echo '<tr>';
                        echo '<th>Diurno</th>';
                        echo '<td> Bs. '.$row["tarif_diude"].'</td>';
                    echo '</tr>';
                    echo '<tr>';
                        echo '<th>Hora nocturna desde</th>';
                        echo '<td> Bs. '.$row["tarif_noctu"].'</td>';
                    echo '</tr>';
                    echo '<tr>';
                        echo '<th>Nocturno</th>';
                        echo '<td> Bs. '.$row["tarif_nocde"].'</td>';
                    echo '</tr>';
                    echo '<tr>';
                        echo '<th>Fin de semana</th>';
                        echo '<td> Bs. '.$row["tarif_finse"].'</td>';
                    echo '</tr>';
                    echo '<tr>';
                        echo '<th>Feriado</th>';
                        echo '<td> Bs. '.$row["tarif_feria"].'</td>';
                    echo '</tr>';
                    echo '<tr>';
                        echo '<th>Multiparada</th>';
                        echo '<td> Bs. '.$row["tarif_param"].'</td>';
                    echo '</tr>';
                    echo '<tr>';
                        echo '<th>Multipasajero</th>';
                        echo '<td> Bs. '.$row["tarif_pasam"].'</td>';
                    echo '</tr>';
                    echo '<tr>';
                        echo '<th>Maleta extra</th>';
                        echo '<td> Bs. '.$row["tarif_malet"].'</td>';
                    echo '</tr>';
                    echo '<tr>';
                        echo '<th>Mascota</th>';
                        echo '<td> Bs. '.$row["tarif_masco"].'</td>';
                    echo '</tr>';
                    echo '<tr>';
                        echo '<th>Tiempo espera</th>';
                        echo '<td> Bs. '.$row["tarif_esper"].'</td>';
                    echo '</tr>';
                    echo '<tr>';
                        echo '<th>Encomienda</th>';
                        echo '<td> Bs. '.$row["tarif_encom"].'</td>';
                    echo '</tr>';
                ?>
                    </tbody>
                </table>
                </div>  
                

                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
          $('[data-toggle="popover"]').popover({ trigger: "hover" })
        })
        
        $("#modal").on("show.bs.modal", function (e) {
          var link = $(e.relatedTarget);
          $(this).find(".modal-body").load(link.attr("href"));
        });
    </script>