<!--<script type="text/javascript">
    var int=self.setInterval("refresh()",6000);
    function refresh()
    {
        location.reload(true);
    }
</script>-->
<?php
//Mostrar Errores del controlador

foreach (Yii::app()->user->getFlashes() as $key => $message) {
  
    ?>
    <script>
    bootbox.dialog({
        message: '<?php echo $message;?>',
        title: "<?php echo $key; ?>",
        buttons: {
            danger: {
                label: "Cerrar",
                className: "btn-uven",
            },
        }
    });
    </script>
    <?php

}
?> 
<!--button class="btn btn-warning callback">With Callback</button-->
<?php
$conexion=yii::app()->db;
$sql="SELECT max(servi_codig) servi_codig FROM admin_servicios where perso_client = '".yii::app()->user->id['usuario']->usuar_codig."' and servi_statu in (1)";

$codigo=$conexion->createCommand($sql)->queryRow();
echo '<form id="form-modal3">';
echo '<input type="hidden" id="tipo" name="t" value="3">';
echo '<input type="hidden" id="codi" name="c" value="'.$codigo['servi_codig'].'">';
echo '<input type="hidden" id="oper" name="operacion" value="D">';
echo '</form>';
if($codigo["servi_codig"]){
   ?> 
   <script type="text/javascript">
        buscarTaxista();
        
        function anularServicio(){
            var data = {
                t: $('#tipo').val(),
                c: $('#codi').val(),
                operacion: $('#oper').val()
            };
            $.ajax({
                dataType: "json",
                url: 'EstatusServicio',
                type: 'POST',
                cache: false,
                data: data,
                timeout: 45000,
                beforeSend: function () {
                    waitingDialog.show('Procesando su solicitud...',{
                        progressType: 'warning',
                        //onHide: function () {alert('Callback!');}
                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    waitingDialog.hide();
                    if (response['success'] == 'true') {
                        bootbox.dialog({
                            message: response['msg'],
                                title: "Exito!",
                                buttons: {
                                    danger: {
                                        label: "Cerrar",
                                        className: "btn-uven",
                                        callback: function(){
                                            window.open('servicio', '_parent');
                                        }
                                    },
                                }
                            });
                            $('.close').click( function(){
                                window.open('servicio', '_parent');
                            });

                            
                        } else {
                            bootbox.dialog({
                                message: response['msg'],
                                    title: "Información!",
                                    buttons: {
                                        danger: {
                                            label: "Cerrar",
                                            className: "btn-uven",
                                        },
                                    }
                                });
                            
                        }
                    },error: function (response) {
                        waitingDialog.hide();
                        bootbox.dialog({
                            message: 'Error al ejecutar el proceso',
                                title: "Información!",
                                buttons: {
                                    danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                },
                            }
                        });
                    }
                });
        }
        function buscarTaxista(){
            var data = {
                servicio: $('#codi').val()
            };
            $.ajax({
                dataType: "json",
                url: 'buscarTaxista',
                type: 'POST',
                cache: false,
                data: data,
                timeout: 45000,
                beforeSend: function () {
                    waitingDialog.show('Buscando Taxista...',{
                        progressType: 'warning',
                        //onHide: function () {alert('Callback!');}
                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    waitingDialog.hide();
                    if (response['success'] == 'true') {
                        bootbox.dialog({
                            message: response['msg'],
                                title: "Exito!",
                                buttons: {
                                    danger: {
                                        label: "Cerrar",
                                        className: "btn-uven",
                                        callback: function(){
                                            window.open('servicio', '_parent');
                                        }
                                    },
                                }
                            });
                            $('.close').click( function(){
                                window.open('servicio', '_parent');
                            });
                            
                        } else {
                            bootbox.dialog({
                                message: response['msg'],
                                    title: "Información!",
                                    buttons: {
                                        danger: {
                                            label: "Cerrar",
                                            className: "btn-uven",
                                        },
                                    }
                                });
                            
                        }
                    },error: function (response) {
                        waitingDialog.hide();
                        bootbox.dialog({
                            message: 'No se encontro al Taxista',
                                title: "Información!",
                                buttons: {
                                    danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                    callback:  function(){
                                        $('#confirmar').modal('show');
                                    }
                                },
                            }
                        });
                        $('.close').click( function(){
                            $('#confirmar').modal('show');
                        });
                    }
                });
        }
            
        //});
        </script>   
   <?php
}
?>
<!-- Modal -->
<div class="modal fade" id="confirmar" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            <p class="text-center">En este momento no hay taxista disponble.
            <br>
            ¿Desea continuar esperando?<p>
            </div>
            <div class="modal-footer">
            <button  id='si' type="button" class="btn btn-uven" data-dismiss="modal" onclick="buscarTaxista()">Si</button>
                <button  id='cerrar' type="button" class="btn btn-uven" data-dismiss="modal" onclick="anularServicio()">No</button>

            </div>
        </div>
    </div>
</div>
<div id="loginbox" style="margin-top:50px;" >
    <div class="modal"></div>    
    <h1 class="titulo">Servicios de Taxi</h1> 



    <div class="panel panel-danger " >


        <div style="padding-top:30px" class="panel-body" >

            <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
            
            <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'subir-p_personas', 'htmlOptions' => array('method' => 'post', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')));
             $user = Yii::app()->user->id->tgene_permi;
             $conexion=yii::app()->db;
             $tipo='2';

            if (Yii::app()->user->id['usuario']->usuar_tipou == '1' or Yii::app()->user->id['usuario']->usuar_tipou == '3'){
                $sql="SELECT * FROM admin_servicios 
                where perso_client = ".Yii::app()->user->id['usuario']->usuar_codig." and servi_statu in (1,4) ";
            }else{
                $sql="SELECT * FROM admin_servicios 
                where perso_taxis = ".Yii::app()->user->id['usuario']->usuar_codig." and servi_statu in (1,4)";
                $result=$conexion->createCommand($sql)->queryRow();
                if(!$result){
                    $sql="SELECT * FROM admin_servicios 
                    where perso_taxis = ".Yii::app()->user->id['usuario']->usuar_codig." and servi_statu in (4)";
                    $result=$conexion->createCommand($sql)->queryRow();
                    if(!$result){
                        $sql="SELECT * FROM admin_servicios 
                        where perso_taxis = 0 and  perso_client <> 0 and servi_statu in (1)";
                        $result=$conexion->createCommand($sql)->queryRow();
                        
                        $tipo='1';
                    }else{
                        $tipo='1';
                    }
                    $tipo='1';
                }else{
                    $tipo='3';
                }
            }
            //echo $sql;
            ?>
            <div class="table-responsive">
                <table id="auditoria" class="table table-bordered table-hover dataTable" style="width: 100%">
                <thead>
                    <tr>
                        <th width="2%">
                            #
                        </th>
                        <th width="60%">
                            Trayecto
                        </th>
                        <th width="15%">
                            Costo
                        </th>
                        <th width="10%">
                            Estatus
                        </th>
                        <?php

                        //echo $sql;
                        $resultado=$conexion->createCommand($sql)->queryRow();
                        
                        if($tipo=='1'){
                            if($resultado['servi_statu']=='3'){
                                echo '<th width="7.5%">Cerrar</th>';
                            }else{
                                echo '<th width="13%">Ver</th>';
                            }
                        }else if($tipo=='2'){
                            
                            echo '<th width="7.5%">Ver</th>';
                            echo '<th width="7.5%">Anular</th>';
                            
                        }else if($tipo=='3'){
                            echo '<th width="7.5%">Ver</th>';
                            echo '<th width="7.5%">Anular</th>';
                            echo '<th width="7.5%">Culminar</th>';
                        }

                        ?>
                        
                    </tr>
                </thead>

                <tbody>
                    <?php                   
                    
                    $result=$conexion->createCommand($sql)->query();
                    $i=0;
                    while (($row=$result->read())!=false) {
                       $i++;
                       switch ($row['servi_statu']) {
                            case '1':
                                if(Yii::app()->user->id['usuario']->usuar_tipou == '1' or Yii::app()->user->id['usuario']->usuar_tipou == '3'){
                                    $estatu='Ubicando Taxista';
                                }else{
                                    $estatu='Pendiente';
                                }
                               
                               break;
                            case '2':
                               $estatu='Realizado';
                               break;
                            case '3':
                               $estatu='Anulado';
                               $disabled='disabled';
                               break;
                            case '4':
                               $estatu='Taxista Ubicado';
                               break;
                            default:
                               $estatu='N/D';
                               break;
                       }
                       echo "<tr>";
                       echo "<td>".$i."</td>";
                       echo "<td>".$row['servi_rutas']."</td>";
                       echo "<td>".number_format($row['servi_costo'],2,',','.')."</td>";

                       echo "<td>".$estatu."</td>";
                       if (Yii::app()->user->id['usuario']->usuar_tipou == '1' or Yii::app()->user->id['usuario']->usuar_tipou == '3')
                        {
                             echo '<td><a href="ServicioContenidoModal?c='.$row['servi_codig'].'&t=1" data-remote="false" data-toggle="modal" data-target="#modal" title="Consultar" class="btn btn-uven btn-block '.$disabled.'"><span class="fa fa-search"></span></a>';
                             echo '<td>';
                                echo '<a href="ServicioContenidoModal?c='.$row['servi_codig'].'&t=3&o=D" data-remote="false" data-toggle="modal" data-target="#modal" title="Anular" class="btn btn-uven btn-block '.$disabled.'">
                                            <span class="fa fa-close"></span>
                                        </a>';
                                echo '</td>';
                        }else{
                            //valido si el servicio está anulado 
                            //el taxsista solo puede cerrar el servicio
                            //para que no lo vea más en la lista
                            if($row['servi_statu']==3){
                                echo '<td>';
                                echo '<a href="ServicioContenidoModal?c='.$row['servi_codig'].'&t=3&o=X" data-remote="false" data-toggle="modal" data-target="#modal" title="Cerrar" class="btn btn-uven btn-block ">
                                            <span class="fa fa-close"></span>
                                        </a>';
                                echo '</td>';
                            }else{

                                echo '<td>';
                                echo '<a href="ServicioContenidoModal?c='.$row['servi_codig'].'&t=2" data-remote="false" data-toggle="modal" data-target="#modal" title="Consultar" class="btn btn-uven btn-block"><span class="fa fa-search"></span></a>';
                                echo '</td>';
                                if($tipo>='2'){
                                    echo '<td>';
                                    echo '<a href="ServicioContenidoModal?c='.$row['servi_codig'].'&t=3&o=U" data-remote="false" data-toggle="modal" data-target="#modal" title="Anular" class="btn btn-uven btn-block">
                                                <span class="fa fa-close"></span>
                                            </a>';
                                    echo '</td>';
                                }
                                if($tipo>'2'){
                                    echo '<td>';
                                    echo '<a href="ServicioContenidoModal?c='.$row['servi_codig'].'&t=4&o=F" data-remote="false" data-toggle="modal" data-target="#modal" title="Finalizar" class="btn btn-uven btn-block">
                                                <span class="fa fa-check"></span>
                                            </a>';
                                    echo '</td>';
                                }
                            }
                        } 
                       
                       
                       echo "</tr>";
                    }
                    ?>
                </tbody>
            </table>
            </div>
            

            <?php $this->endWidget(); ?>

        </div><!-- form -->
    </div>  
</div>

<!-- Modal -->
<style type="text/css">
    .modal-header{
        background-color: #FFBF1C;
        color: #000;
    }
</style>

<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <div id='form-modal'>
            
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#aprobar').click(function (e) {
            var formData = new FormData($("#form-modal")[0]);
            e.preventDefault();
            $.ajax({
                dataType: "json",
                url: 'actualizarauditoria',
                type: 'POST',
                cache: false,
                data: formData,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    //alert(response['success']);
                    if (response['success'] == 'true') {
                        bootbox.dialog({
                            message: response['msg'],
                            title: "Exito!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                    callback: function(){
                                       window.open('seguridad_usuarios', '_parent');
                                    }
                                },
                            }
                        });
                        $('.close').click( function(){
                            window.open('seguridad_usuarios?t=' + t, '_parent');
                        });
                    } else {

                        bootbox.dialog({
                            message: response['msg'],
                            title: "Información!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                },
                            }
                        });
                        setTimeout(function () {
                            $('.close').click();
                        }, 1000);
                        //alert('error');
                        // $("#subir").removeAttr('disabled');
                    }

                }
            });

        });
        

    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find("#form-modal").load(link.attr("href"));
    });
</script>
