<style>
#mapa {
   height: 563px;
   border-radius: 5px;
}
</style>
<div class="registrar" >
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="titulos">Solicita tu <span style="color: #FFCD00;">taxi</span> aquí!</h2>
                    </div>
                    <div class="panel-body">
                        <div class="col-xs-12">
                            <form class="form-horizontal" id='pedir-taxi' action="costoservicio">
                                <?php
                                $hide='';
                                $pasajero='checked';
                                $corporativo='';
                                if(yii::app()->user->id['usuario']->usuar_tipou==1){
                                    $hide='hide';
                                    $pasajero='checked';
                                }else if(yii::app()->user->id['usuario']->usuar_tipou==3){
                                    $hide='hide';
                                    $corporativo='checked';
                                }
                                ?>
                                <div class="form-group <?php echo $hide; ?>">
                                    <div data-toggle="buttons">
                                        <label class="btn btn-uven-2 btn-block-2 active"> 
                                                <b>
                                                    <input type="radio" name="q2" value="0"  <?php echo $pasajero; ?>>
                                                    <i class="fa fa-user"></i> 
                                                    Pasajero
                                                </b>
                                        </label>
                                        <label class="btn btn-uven-2 btn-block-2">
                                      
                                                <b>
                                                    <input type="radio" name="q2" value="1" <?php echo $corporativo; ?>>
                                                    <i class="fa fa-users"></i>
                                                    Corporativo
                                                </b>
                                        </label>
                                    </div>
                                </div>
                                

                                <div class="form-group ">
                                    <label>Origen</label>
                                    <div class="input-group">
                                        <input class="form-control selectpicker"  id="autocomplete" name="autocomplete" placeholder="Ingrese el origen" onFocus="geolocate()" type="text" >
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button" data-container="body" data-toggle="popover" data-placement="right" data-content="El origen es mi ubicacion actual." id='actualpos'><i class="fa fa-map-marker" aria-hidden="true" ></i></button>
                                        </span>
                                    </div>
                                </div>
                                
                                <div class="form-group" >
                                    <label>Punto de referencia</label>
                                        <textarea class="form-control" id="servi_refer" name="punto_referencia" placeholder="Ingrese el punto de referencia" ></textarea>
                                </div>
                                
                                <div class="form-group">
                                    <label>Destino</label>
                                    <div class="input-group">
                                        <input class="form-control" type="text" id="destino_0" name="destino[0]" placeholder="Ingrese el destino" onFocus="geolocate()">
                                        <span class="input-group-btn">

                                            
                                        <button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>
                                        </span>
                                    </div>
                                </div>


                                <!-- The option field template containing an option field and a Remove button -->
                                <div class="form-group hide" id="bookTemplate">
                                
                                        <label>Destino</label>

                                    <div class="input-group">
                                        <input class="form-control" type="text" id="destino_" name="destino_" placeholder="Ingrese el destino" onFocus="geolocate()">

                                        <span class="input-group-btn">
                                        <button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>
                                        <button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="controls">
                                                <button  id='buscar' type="reset" class="btn btn-uven-2 btn-block-2">
                                                <b> <i class="fa fa-search"></i> Limpiar</b>
                                                </button>
                                                <button type="button" id="aqui-vamos" class="btn btn-uven-2 btn-block-2 active">
                                                <b> <i class="fa fa-car"></i> Aquí Vamos!</b>
                                                </button>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-8 hide">
                
                <div id="mapa" class="mapa">
                    
                </div>
                
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
          $('[data-toggle="popover"]').popover({ trigger: "hover" })
        })
    </script>
     <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAoCHVnBZdXzHqw-ipIFEW_by1cJSmcLbg&libraries=places&callback=initAutocomplete"
        async defer></script>
    <script>
      var cordori, corddes, geocoder, placeSearch, autocomplete, autocompleteDes;

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        var options = {
            types: ['geocode'],
            componentRestrictions: {country: "ve"}
        };
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),options);

         autocompleteDes = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('destino_0')),options);

        
        //autocomplete.addListener('place_changed', fillInAddress);
      }

      function assignAutoCompl(id){
         var options = {
            types: ['geocode'],
            componentRestrictions: {country: "ve"}
        };
        var _autocomplete = new google.maps.places.Autocomplete(document.getElementById(id),options);
        /*_autocomplete.setTypes(['geocode']);*/
        
        }

    
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }


    </script>
   

<script>
$(document).ready(function() {
    var origen = {
            validators: {
                notEmpty: {
                    message: 'El Origen es obligatorio'
                }
            }
        },
        destino = {
            validators: {
                notEmpty: {
                    message: 'El destino es obligatorio'
                }
            }
        },
        bookIndex = 0;

    $('#pedir-taxi')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'autocomplete': origen,
                'destino[0]': destino
            }
        })

        // Add button click handler
        .on('click', '.addButton', function() {
            bookIndex++;
            var $template = $('#bookTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .attr('data-book-index', bookIndex)
                                .insertBefore($template);

            // Update the name attributes
            $clone
                .find('[name="destino_"]').attr('name', 'destino[' + bookIndex + ']').end()
                .find('[id="destino_"]').attr('id', 'destino_' + bookIndex).end();

            // Add new fields
            // Note that we also pass the validator rules for new field as the third parameter
            $('#pedir-taxi').formValidation('addField', 'destino[' + bookIndex + ']', destino);
            assignAutoCompl('destino_' + bookIndex);
        })

        // Remove button click handler
        .on('click', '.removeButton', function() {
            var $row  = $(this).parents('.form-group'),
                index = $row.attr('data-book-index');

            // Remove fields
            $('#bookForm')
                .formValidation('removeField', $row.find('[name="destino[' + index + ']"]'));

            // Remove element containing the fields
            $row.remove();
        });
});
</script>
<script type="text/javascript">
    $('#aqui-vamos').click(function () {
        var formData = new FormData($("#pedir-taxi")[0]);
        $('#pedir-taxi').formValidation('validate'); //secondary validation using Bootstrap Validator     

        var formValidation = $('#pedir-taxi').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                'dataType': "json",
                'data': formData,
                'url': 'VerificarRuta',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    //alert(response['success']);
                    $("#body").removeClass("loading");
                    if (response['success'] == 'true') {
                        window.open('costoservicio?' + $('#pedir-taxi').serialize(), '_parent');
                    } else {
                        $("#body").removeClass("loading");
                        bootbox.dialog({
                            message: response['msg'],
                            title: "Información!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn btn-uven",
                                },
                            }
                        });
                        
                        $("#subir").removeAttr('disabled');
                    }

                }
            });
        }
    });
</script>
<script type="text/javascript">
    
 
    $( "#actualpos" ).click( function(e) {
        e.preventDefault();
        /* Chrome need SSL! */
        /*var is_chrome = /chrom(e|ium)/.test( navigator.userAgent.toLowerCase() );
        var is_ssl    = 'https:' == document.location.protocol;
        if( is_chrome && ! is_ssl ){
            return false;
        }*/
    if(navigator.geolocation) {
            /* HTML5 Geolocation */
        navigator.geolocation.getCurrentPosition(
            function( position ){ // success cb
 
                /* Current Coordinate */
                var lat = position.coords.latitude;
                var lng = position.coords.longitude;
                var google_map_pos = new google.maps.LatLng( lat, lng );
                /* Use Geocoder to get address */
                var google_maps_geocoder = new google.maps.Geocoder();
                google_maps_geocoder.geocode(
                    { 'latLng': google_map_pos },
                    function( results, status ) {
                        if ( status == google.maps.GeocoderStatus.OK && results[0] ) {
                            //console.log( results[1].formatted_address );
                            document.getElementById("autocomplete").value = results[0].formatted_address;
                        }
                    }
                );
            },
            function error(err){ // fail cb
                //console.warn('ERROR(' + err.code + '): ' + err.message);
                bootbox.dialog({
                    message: 'No se pudo encontrar su ubicación actual, revise los p_permisos de su navegador',
                    title: "Información!",
                    buttons: {
                        danger: {
                            label: "Cerrar",
                            className: "btn btn-uven",
                        },
                    }
                });
            }
        );
    } else {
        bootbox.dialog({
                    message: 'La geolocalización no esta soportada por su navegador',
                    title: "Información!",
                    buttons: {
                        danger: {
                            label: "Cerrar",
                            className: "btn btn-uven",
                        },
                    }
                });
        //alert("Geolocation is not supported by this browser.");
    }
});
</script>