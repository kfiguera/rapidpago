<?php
//Mostrar Errores del controlador
foreach (Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="alert alert-' . $key . '">' . $message . "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> </div>\n";
}
?> 
<div id="loginbox" style="margin-top:50px;" class="mainbox col-md-12">           
    <style>
        .image-preview-input {
            position: relative;
            overflow: hidden;
            margin: 0px;    
            color: #333;
            background-color: #fff;
            border-color: #ccc;    
        }
        .image-preview-input input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            padding: 0;
            font-size: 20px;
            cursor: pointer;
            opacity: 0;
            filter: alpha(opacity=0);
        }
        .image-preview-input-title {
            margin-left:2px;
        }
    </style>  
    <h1 class="titulo">Cambiar Password</h1>
    <div class="panel panel-danger" >


        <div style="padding-top:30px" class="panel-body" >

            <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>


            <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'subir-p_personas', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            ?>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">  
                        <!-- image-preview-filename input [CUT FROM HERE]-->

                        <label for="clvarea">Contraseña Actual</label>
                        <?php
                        
                          echo CHtml::passwordField('usua_actuals', '', array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                        ?>

                        </div>
                    </div>
                </div>
                <div class="row">
                   <div class="col-sm-6">
                        <div class="form-group">  
                        <!-- image-preview-filename input [CUT FROM HERE]-->

                        <label for="clvarea">Contraseña Nueva</label>
                        <?php
                        
                          echo CHtml::passwordField('usua_passws', '', array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                        ?>

                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">  
                        <!-- image-preview-filename input [CUT FROM HERE]-->

                        <label for="clvarea">Confirmar Contraseña</label>
                        <?php
                        
                          echo CHtml::passwordField('ccontra', '', array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                        ?>

                        </div>
                    </div>
                </div>

            
            <div class="row">
                <!-- Button -->
                <div class="controls">
                    <div class="col-sm-6">
                        <button id="subir" name="subir" value="subir" type="button" class="btn-block btn btn-uven"><span class="fa fa-upload"></span> Guardar  </button>

                    </div>
                    <div class="col-sm-6">
                        <button id="subir" name="limpiar" value="limpiar" type="reset" class="btn-block btn btn-default"><span class="fa fa-eraser"></span> Limpiar  </button>

                    </div>
                </div>
            </div>              

            <?php $this->endWidget(); ?>

        </div><!-- form -->
    </div>  
</div>
<script>

$(document).on('click', '#close-preview', function () {
    $('.numero1 .image-preview').popover('hide');
    // Hover befor close the preview
    $('.numero1 .image-preview').hover(
        function () {
            $('.image-preview').popover('hide');
        },
        function () {
            $('.image-preview').popover('hide');
        }
    );
});
$(function () {
    // Create the close button
    var closebtn = $('<button/>', {
        type: "button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    $('.numero1 .image-preview').popover({
        trigger:'manual',
        html:true,
        title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
        content: "No hay imagen",
        placement:'top'
    });
    // Clear event
    $('.numero1 .image-preview-clear').click(function () {
        $('.numero1 .image-preview').attr("data-content", "").popover('hide');
        $('.numero1 .image-preview-filename').val("");
        $('.numero1 .image-preview-clear').hide();
        $('.numero1 .image-preview-input input:file').val("");
        $(".numero1 .image-preview-input-title").text("Buscar");
    });
    // Create the preview image
    $(".numero1 .image-preview-input input:file").change(function () {
        var img = $('<img/>', {
            id: 'dynamic',
            width: 250,
            height: 200
        });
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".numero1 .image-preview-input-title").text("Cambiar");
            $(".numero1 .image-preview-clear").show();
            $(".numero1 .image-preview-filename").val(file.name);
            img.attr('src', e.target.result);
            $(".numero1 .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
            }
        reader.readAsDataURL(file);
    });
});
</script>
<script>$(document).on('click', '#close-preview', function () {
    $('.numero2 .image-preview').popover('hide');
    // Hover befor close the preview
    $('.numero2 .image-preview').hover(
            function () {
                $('.image-preview').popover('hide');
            },
            function () {
                $('.image-preview').popover('hide');
            }
    );
});
$(function () {
    // Create the close button
    var closebtn = $('<button/>', {
        type: "button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });

    // Clear event
    $('.numero2 .image-preview-clear').click(function () {
        $('.numero2 .image-preview').attr("data-content", "").popover('hide');
        $('.numero2 .image-preview-filename').val("");
        $('.numero2 .image-preview-clear').hide();
        $('.numero2 .image-preview-input input:file').val("");
        $(".numero2 .image-preview-input-title").text("Buscar");
    });
    // Create the preview image
    $(".numero2 .image-preview-input input:file").change(function () {
        var img = $('<img/>', {
            id: 'dynamic',
            width: 250,
            height: 200
        });
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".numero2 .image-preview-input-title").text("Cambiar");
            $(".numero2 .image-preview-clear").show();
            $(".numero2 .image-preview-filename").val(file.name);
            img.attr('src', e.target.result);
        }
        reader.readAsDataURL(file);
    });
});</script>
<script type="text/javascript">
    //$('#telefono').mask('(9999)999.99.99');
    

    $(document).ready(function () {

        $('#subir-p_personas').bootstrapValidator({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                usuar_actuals: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        identical: {
                            field: 'usuar_passws',
                            message: 'La Contraseña y Su Confirmación NO deben ser iguales'
                        },
                        different: {
                            field: 'correo',
                            message: 'La Contraseña no puede ser igual al Correo'
                        },
                        regexp: {
                            regexp: /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/,
                            message: 'Estimado(a) Usuario(a) el campo "Confirmar Contraseña" debe tener entre 8 y 16 caracteres, al menos un dígito, al menos una minúscula y al menos una mayúscula. Debe poseer el siguiente formato: w3Unpocodet0d0'
                        }

                    }
                    
                },
                usuar_passws: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        identical: {
                            field: 'ccontra',
                            message: 'La Contraseña y Su Confirmación deben ser iguales'
                        },
                        different: {
                            field: 'correo',
                            message: 'La Contraseña no puede ser igual al Correo'
                        },
                        regexp: {
                            regexp: /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/,
                            message: 'Estimado(a) Usuario(a) el campo "Confirmar Contraseña" debe tener entre 8 y 16 caracteres, al menos un dígito, al menos una minúscula y al menos una mayúscula. Debe poseer el siguiente formato: w3Unpocodet0d0'
                        }

                    }
                    
                },
                ccontra: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        identical: {
                            field: 'contra',
                            message: 'La Contraseña y Su Confirmación deben ser iguales'
                        },
                        different: {
                            field: 'correo',
                            message: 'La Contraseña no puede ser igual al Correo'
                        },
                        regexp: {
                            regexp: /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/,
                            message: 'Estimado(a) Usuario(a) el campo "Confirmar Contraseña" debe tener entre 8 y 16 caracteres, al menos un dígito, al menos una minúscula y al menos una mayúscula. Debe poseer el siguiente formato: w3Unpocodet0d0'
                        }

                    }
                },
            }
        });
    });
    $('#subir').click(function () {
        var formData = new FormData($("#subir-p_personas")[0]);
        $('#subir-p_personas').bootstrapValidator('validate'); //secondary validation using Bootstrap Validator      
        var bootstrapValidator = $('#subir-p_personas').data('bootstrapValidator');
        if (bootstrapValidator.isValid()) {
            $.ajax({
                'dataType': "json",
                'url': 'cambiaPasswordIncluir',
                'type': 'post',
                'data': formData,
                'cache': false,
                'contentType': false,
                'processData': false,
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    //alert(response['success']);
                    $("#body").removeClass("loading");
                    if (response['success'] == 'true') {
                        $("#body").removeClass("loading");
                        bootbox.dialog({
                            message: response['msg'],
                            title: "Exito!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                    callback: function(){
                                       window.open('cambiarPassword', '_parent');
                                    }
                                },
                            }
                        });
                       $('.close').click( function(){
                            window.open('cambiarPassword', '_parent');
                        });
                    } else {
                        $("#body").removeClass("loading");
                        bootbox.dialog({
                            message: response['msg'],
                            title: "Información!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                },
                            }
                        });
                        
                        $("#subir").removeAttr('disabled');
                    }

                }
            });
        }
    });
</script>