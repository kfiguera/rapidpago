    <div class="registrar" >
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="titulos">Solicita tu <span style="color: #FFCD00;">taxi</span> aquí!</h2>
                    </div>
                    <div class="panel-body">
                        <div class="col-xs-12">
                            <form class="form-horizontal" id='pedir-taxi'>
                                <div class="form-group">
                                    <div data-toggle="buttons">
                                        <label class="btn btn-uven-2 btn-block-2 active">       
                                            <h3>
                                                <b>
                                                    <input type="radio" name="q2" value="0" checked>
                                                    <i class="fa fa-user"></i> 
                                                    Pasajero
                                                </b>
                                            </h3>
                                        </label>
                                        <label class="btn btn-uven-2 btn-block-2">
                                            <h3>
                                                <b>
                                                    <input type="radio" name="q2" value="1">
                                                    <i class="fa fa-users"></i>
                                                    Corporativo
                                                </b>
                                            </h3>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Ciudad</label>
                                    <select class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
                                        <option>Seleccione</option>
                                        <option>Caracas</option>
                                        <option>Maracay</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Origen</label>
                                    <div class="input-group">
                                        <select class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
                                            <option>Ubicación Actual</option>
                                            <option>Elija Origen</option>
                                        </select>
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button" data-container="body" data-toggle="popover" data-placement="right" data-content="El origen es mi ubicacion actual."><i class="fa fa-map-marker" aria-hidden="true"></i></button>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Destino</label>
                                    <div class="input-group">
                                        <select name="destino[]" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
                                            <option>Elija Destino</option>
                                        </select>
                                        <span class="input-group-btn">

                                            
                                        <button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>
                                        </span>
                                    </div>
                                </div>

                                <!-- The option field template containing an option field and a Remove button -->
                                <div class="form-group hide" id="optionTemplate">
                                
                                        <label>Destino</label>

                                    <div class="input-group">
                                        <select name="destino[]" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
                                            <option>Elija Destino</option>
                                        </select>

                                        <span class="input-group-btn">
                                        <button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="controls">
                                        <button class="btn btn-block btn-uven-2">
                                        <h3><b> <i class="fa fa-car"></i> Aquí Vamos!</b></h3>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
          $('[data-toggle="popover"]').popover({ trigger: "hover" })
        })
    </script>
    <script>
$(document).ready(function() {
    // The maximum number of options
    var MAX_OPTIONS = 5;

    $('#pedir-taxi')
        .bootstrapValidator({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                question: {
                    validators: {
                        notEmpty: {
                            message: 'The question required and cannot be empty'
                        }
                    }
                },
                'destino[]': {
                    validators: {
                        notEmpty: {
                            message: 'The option required and cannot be empty'
                        },
                        stringLength: {
                            max: 100,
                            message: 'The option must be less than 100 characters long'
                        }
                    }
                }
            }
        })

        // Add button click handler
        .on('click', '.addButton', function() {
            var $template = $('#optionTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .insertBefore($template),
                $option   = $clone.find('[name="destino[]"]');

            // Add new field
            $('#pedir-taxi').bootstrapValidator('addField', $option);
        })

        // Remove button click handler
        .on('click', '.removeButton', function() {
            var $row    = $(this).parents('.form-group'),
                $option = $row.find('[name="destino[]"]');

            // Remove element containing the option
            $row.remove();

            // Remove field
            $('#pedir-taxi').bootstrapValidator('removeField', $option);
        })

        // Called after adding new field
        .on('added.field.fv', function(e, data) {
            // data.field   --> The field name
            // data.element --> The new field element
            // data.options --> The new field options

            if (data.field === 'option[]') {
                if ($('#pedir-taxi').find(':visible[name="destino[]"]').length >= MAX_OPTIONS) {
                    $('#pedir-taxi').find('.addButton').attr('disabled', 'disabled');
                }
            }
        })

        // Called after removing the field
        .on('removed.field.fv', function(e, data) {
           if (data.field === 'option[]') {
                if ($('#pedir-taxi').find(':visible[name="destino[]"]').length < MAX_OPTIONS) {
                    $('#pedir-taxi').find('.addButton').removeAttr('disabled');
                }
            }
        });
});
</script>
