<!--<script type="text/javascript">
    var int=self.setInterval("refresh()",6000);
    function refresh()
    {
        location.reload(true);
    }
</script>-->
<?php
//Mostrar Errores del controlador

foreach (Yii::app()->user->getFlashes() as $key => $message) {
    ?>
    <script>
        bootbox.dialog({
            message: '<?php echo $message;?>',
            title: "<?php echo $key; ?>",
            buttons: {
                danger: {
                    label: "Cerrar",
                    className: "btn-uven",
                },
            }
        });
    </script>
    <?php
}
?> 
<form id='servicio'>
<div id="loginbox" style="margin-top:50px;" >
    <div class="modal"></div>    
    <h1 class="titulo">Servicios de Taxi</h1> 
        <?php
        $form = $this->beginWidget('CActiveForm', array('id' => 'subir-p_personas', 'htmlOptions' => array('method' => 'post', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')));
             

            ?>

            <input type="hidden" name="c" value="<?php echo Yii::app()->user->id['usuario']->usuar_codig; ?>">
             
           
    <div class="panel panel-danger " >


        <div style="padding-top:30px" class="panel-body" >

            <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
            
            <?php
            $conexion=yii::app()->db;
            $permiso = Yii::app()->user->id->tgene_permi;
            $sql="SELECT * FROM admin_servicios s 
            inner join admin_tgenerica sta on (s.tgene_statu = sta.tgene_codig) 
            where perso_client = ".Yii::app()->user->id['usuario']->usuar_codig." and tgene_statu not in (7)";

            $res = $conexion->createCommand($sql)->queryRow();
            if($res){
                $estatus = $res['tgene_descr'];
            }
                
            
           ?>
            <div class="table-responsive">
                <table id="auditoria" class="table table-bordered table-hover dataTable" style="width: 100%">
                <thead>
                    <tr>
                        <th width="2%">
                            #
                        </th>
                        <th width="60%">
                            Trayecto
                        </th>
                        <th width="15%">
                            Costo
                        </th>
                        <th width="20%">
                            Estatus
                        </th>
                        <?php
                        switch ($res['tgene_statu']) {
                           case '1':
                               # servicio pendiente de pago
                               ?>
                                    <th width="10%">
                                        Pagar
                                    </th>
                                    <th width="10%">
                                        Anular
                                    </th>
                                <?php
                               break;
                            case '2':
                               # El pasajero solo puede anular el servicio
                                ?>
                                    <th width="10%">
                                        Ver
                                    </th>
                                    <th width="10%">
                                        Anular
                                    </th>
                                    
                                 <?php
                               break;
                            case '3':
                               # El pasajero solo puede anular el servicio
                                ?>
                                    <th width="10%">
                                        Anular
                                    </th>
                                 <?php
                               break;
                           case '4':
                               # El pasajero solo puede anular el servicio
                                ?>
                                    <th width="10%">
                                        Ver
                                    </th>
                                    <th width="10%">
                                        Anular
                                    </th>
                                <?php
                               break;
                            case '5':
                               # El pasajero solo puede anular el servicio
                                ?>
                                    <th width="10%">
                                        Cerrar
                                    </th>
                                    
                                <?php
                               break;
                           case '6':
                               # El pasajero anuló el servicio
                            ?>
                            <th width="10%">
                                Evaluar Servicio
                            </th>
                            
                             <?php
                               break;     
                           default:
                               # code...
                               break;
                       }
                       ?>

                        
                    </tr>
                </thead>

                <tbody>
                    <?php                   
                    
                    $result=$conexion->createCommand($sql)->query();
                    $i=0;
                    while (($row=$result->read())!=false) {
                       $i++;
                       
                       echo "<tr>";
                       echo "<td>".$i."</td>";
                       echo "<td>".$row['servi_rutas']."</td>";
                       echo "<td>".number_format($row['servi_costo'],2,',','.')."</td>";

                       echo "<td>".$estatus."</td>";

                       switch ($row['tgene_statu']) {
                           case '1':
                               # servicio pendiente de pago
                           //tipo 1 indica que debe seleccionar la forma de pago
                           //tipo dos es que desea cancelar por transferencia
                                echo '<td><a href="FormaPagoPasajeroModal?c='.$row['servi_codig'].'&t=1" data-remote="false" data-toggle="modal" data-target="#modal" title="Consultar" class="btn btn-uven btn-block "><span class="fa fa-search"></span></a>';
                                echo '</td>';
                            //o=D indica que es el cliente quien lo anula
                                echo '<td>';
                                echo '<a href="ServicioContenidoModal?c='.$row['servi_codig'].'&t=3&o=D" data-remote="false" data-toggle="modal" data-target="#modal" title="Anular" class="btn btn-uven btn-block '.$disabled.'">
                                            <span class="fa fa-close"></span>
                                        </a>';
                                echo '</td>';

                               break;
                            case '2':
                               # pago por transferencia validado
                                echo '<td>';
                                echo '<a href="ServicioContenidoModal?c='.$row['servi_codig'].'&t=3&o=D" data-remote="false" data-toggle="modal" data-target="#modal" title="Anular" class="btn btn-uven btn-block '.$disabled.'">
                                            <span class="fa fa-close"></span>
                                        </a>';
                                echo '</td>';
                                echo "<td>Espere confirmación</td>";
                               break;
                             case '3':
                               # Ver y anular el servicio...
                                echo '<td>';
                                echo '<a href="ServicioContenidoModal?c='.$row['servi_codig'].'&t=3&o=D" data-remote="false" data-toggle="modal" data-target="#modal" title="Anular" class="btn btn-uven btn-block '.$disabled.'">
                                            <span class="fa fa-close"></span>
                                        </a>';
                                echo '</td>';
                               break;
                             case '4':
                               # Ver o anular el servicio...
                                echo '<td><a href="ServicioContenidoModal?c='.$row['servi_codig'].'&t=1" data-remote="false" data-toggle="modal" data-target="#modal" title="Consultar" class="btn btn-uven btn-block '.$disabled.'"><span class="fa fa-search"></span></a>';
                                echo '<td>';
                                echo '<a href="ServicioContenidoModal?c='.$row['servi_codig'].'&t=3&o=D" data-remote="false" data-toggle="modal" data-target="#modal" title="Anular" class="btn btn-uven btn-block '.$disabled.'">
                                            <span class="fa fa-close"></span>
                                        </a>';
                                echo '</td>';
                               break;
                            case '5':
                               # Ver o anular el servicio...
                                echo '<td>';
                                echo '<a href="ServicioContenidoModal?c='.$row['servi_codig'].'&t=3&o=X" data-remote="false" data-toggle="modal" data-target="#modal" title="Cerrar" class="btn btn-uven btn-block "><span class="fa fa-close"></span>
                                        </a>';
                                echo '</td>';
                               
                               break;
                             case '6':
                               # evaluar el servicio...
                               //o=E ndica que el cliente desea evaluar el servicio
                                    echo '<td>';
                                    echo '<a href="ServicioContenidoModal?c='.$row['servi_codig'].'&t=5&o=E" data-remote="false" data-toggle="modal" data-target="#modal" title="Evaluar" class="btn btn-uven btn-block "><span class="fa fa-check"></span>
                                            </a>';
                                    echo '</td>';
                               break;
                           
                           default:
                               # code...
                               break;
                       }
                       
                       echo "</tr>";
                    }
                    ?>
                </tbody>
            </table>
            </div>
            
           
            <?php $this->endWidget(); ?>

        </div><!-- form -->

    </div>  
</div>
</form>
<!-- Modal -->
<style type="text/css">
    .modal-header{
        background-color: #FFBF1C;
        color: #000;
    }
</style>

<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <div id='form-modal'>
            
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="confirmar" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            <p class="text-center">Lo sentimos, no hay taxista disponble en este momento.
            <br>
            ¿Desea continuar esperando?<p>
            </div>
            <div class="modal-footer">
            <button  id='si' type="button" class="btn btn-uven" data-dismiss="modal" onclick="buscarTaxista()">Si</button>
                <button  id='cerrar' type="button" class="btn btn-uven" data-dismiss="modal" onclick="anularServicio()">No</button>

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="verificando_pago" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                <div class="timing">
                    <div id="contador_pago" data-timer="120"></div>
                </div>
                </div>
            </div>
            </div>
            <div class="modal-footer">
            <h4 class="text-center">Verificando su pago.</h4>    
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="buscando_taxista" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                <div class="timing">
                    <div id="contador_taxi" data-timer="45"></div>
                </div>
                </div>
            </div>
            </div>
            <div class="modal-footer">
            <h4 class="text-center">Buscando Taxista.</h4>    
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.cambiastatus').click(function (e) {

            //alert($(this).val());
            e.preventDefault();
            var formData = new FormData($("#servicio")[0]);
            $.ajax({
                dataType: "json",
                url: 'cambiaEstatus',
                type: 'GET',
                cache: false,
                data: $('#servicio').serialize()+'&estatus='+$(this).val() ,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    //alert(response['success']);
                    if (response['success'] == 'true') {
                        //activar el botón
                          window.open('servicio?estatus=' + response['estatus'], '_parent');

                    } else {

                        bootbox.dialog({
                            message: response['msg'],
                            title: "Información!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                },
                            }
                        });
                        //alert('error');
                        // $("#subir").removeAttr('disabled');
                    }

                }
            });

        });
        //****************************************




        $('#auditoria').DataTable();
        $('#aprobar').click(function (e) {
            var formData = new FormData($("#form-modal")[0]);
            e.preventDefault();
            $.ajax({
                dataType: "json",
                url: 'actualizarauditoria',
                type: 'POST',
                cache: false,
                data: formData,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    //alert(response['success']);
                    if (response['success'] == 'true') {
                        bootbox.dialog({
                            message: response['msg'],
                            title: "Exito!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                    callback: function(){
                                       window.open('seguridad_usuarios', '_parent');
                                    }
                                },
                            }
                        });
                        $('.close').click( function(){
                            window.open('seguridad_usuarios?t=' + t, '_parent');
                        });
                    } else {

                        bootbox.dialog({
                            message: response['msg'],
                            title: "Información!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                },
                            }
                        });
                        setTimeout(function () {
                            $('.close').click();
                        }, 1000);
                        //alert('error');
                        // $("#subir").removeAttr('disabled');
                    }

                }
            });

        });
        

    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find("#form-modal").load(link.attr("href"));
    });
    if (Notification) {
        Notification.requestPermission();
    }
    /*
switch (Notification.permission) {
    case "granted":
        alert("¡Tenemos permiso para enviar notificaciones!");
        break;
    case "denied":
        alert("¡No tenemos permiso para enviar notificaciones!");
        break;
    default:
        alert("No se le ha preguntado al usuario, o éste no ha contp_estados.");
        break;
    


}*/
var options = {
    icon: "http://localhost/html/uven/admin/app/assets/img/logo.png"
};
 
var notif = new Notification("Ejemplo de notificación", options);    
notif.show;

</script>
<!--script>
    var option_modal={show: true,backdrop: 'static',keyboard: false},
    option_verificando_pago = {
        start: true,
        circle_bg_color: "#FFCD00",
        use_background: true,
        bg_width: 1,
        fg_width: 0.02,
        total_duration: 120,
        time: {
            Days: { show: false },
            Hours: { show: false },
            Minutes: { show: false },
            Seconds: { 
                show: true,
                text: "Segundos",
                color: "#000000" 
            }
        }
    },
    option_buscando_taxista={   
        start: true,
        circle_bg_color: "#FFCD00",
        use_background: true,
        bg_width: 1,
        fg_width: 0.02,
        total_duration: 45,
        time: {
            Days: { show: false },
            Hours: { show: false },
            Minutes: { show: false },
            Seconds: { show: true, text: "Segundos", color: "#000000" }
        }
    };
    /*$("#verificando_pago").modal(option_modal); 
    $("#contador_pago").TimeCircles(option_verificando_pago); */ 
    </script-->

<?php
/*
$conexion=yii::app()->db;
$sql="SELECT max(servi_codig) servi_codig FROM admin_servicios where perso_client = '".yii::app()->user->id['usuario']->usuar_codig."' and servi_statu in (1)";

$codigo=$conexion->createCommand($sql)->queryRow();
echo '<form id="form-modal3">';
echo '<input type="hidden" id="tipo" name="t" value="3">';
echo '<input type="hidden" id="codi" name="c" value="'.$codigo['servi_codig'].'">';
echo '<input type="hidden" id="oper" name="operacion" value="D">';
echo '</form>';
if($codigo["servi_codig"]){
   ?> 
   <script type="text/javascript">
        var option_modal={show: true,backdrop: 'static',keyboard: false},
        option_verificando_pago = {
            start: false,
            animation: "ticks",
            circle_bg_color: "#FFCD00",
            use_background: true,
            "bg_width": 0.4,
            "fg_width": 0.06333333333333334,
            total_duration: 120,
            count_past_zero: false,
            time: {
                Days: { show: false },
                Hours: { show: false },
                Minutes: { show: false },
                Seconds: { 
                    show: true,
                    text: "Segundos",
                    color: "#000000" 
                }
            }

        },
        option_buscando_taxista={   
            start: false,
            animation: "ticks",
            circle_bg_color: "#FFCD00",
            use_background: true,
            "bg_width": 0.4,
            "fg_width": 0.06333333333333334,
            total_duration: 45,
            count_past_zero: false,
            time: {
                Days: { show: false },
                Hours: { show: false },
                Minutes: { show: false },
                Seconds: { show: true, text: "Segundos", color: "#000000" }
            }
        };
        buscarTaxista();
        
        function anularServicio(){
            var data = {
                t: $('#tipo').val(),
                c: $('#codi').val(),
                operacion: $('#oper').val()
            };
            $.ajax({
                dataType: "json",
                url: 'EstatusServicio',
                type: 'POST',
                cache: false,
                data: data,
                timeout: 45000,
                beforeSend: function () {
                    waitingDialog.show('Procesando su solicitud...',{
                        progressType: 'warning',
                        //onHide: function () {alert('Callback!');}
                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    waitingDialog.hide();
                    if (response['success'] == 'true') {
                        bootbox.dialog({
                            message: response['msg'],
                                title: "Exito!",
                                buttons: {
                                    danger: {
                                        label: "Cerrar",
                                        className: "btn-uven",
                                        callback: function(){
                                            window.open('servicio', '_parent');
                                        }
                                    },
                                }
                            });
                            $('.close').click( function(){
                                window.open('servicio', '_parent');
                            });

                            
                        } else {
                            bootbox.dialog({
                                message: response['msg'],
                                    title: "Información!",
                                    buttons: {
                                        danger: {
                                            label: "Cerrar",
                                            className: "btn-uven",
                                        },
                                    }
                                });
                            
                        }
                    },error: function (response) {
                        waitingDialog.hide();
                        bootbox.dialog({
                            message: 'Error al ejecutar el proceso',
                                title: "Información!",
                                buttons: {
                                    danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                },
                            }
                        });
                    }
                });
        }
        function ModalTaxi(accion){
            if(accion==1){
                $("#buscando_taxista").modal(option_modal);
                $("#contador_taxi").TimeCircles(option_buscando_taxista);
                $("#contador_taxi").TimeCircles().restart()
            }else{
                $("#buscando_taxista").modal('hide');
                $("#contador_taxi").TimeCircles().stop()
            }
            
        }
        function buscarTaxista(){
            
            var data = {
                servicio: $('#codi').val()
            };
            $.ajax({
                dataType: "json",
                url: 'buscarTaxista',
                type: 'POST',
                cache: false,
                data: data,
                timeout: 45000,
                beforeSend: function () {
                    ModalTaxi(1);
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    ModalTaxi(2);
                    if (response['success'] == 'true') {
                        bootbox.dialog({
                            message: response['msg'],
                                title: "Exito!",
                                buttons: {
                                    danger: {
                                        label: "Cerrar",
                                        className: "btn-uven",
                                        callback: function(){
                                            window.open('servicio', '_parent');
                                        }
                                    },
                                }
                            });
                            $('.close').click( function(){
                                window.open('servicio', '_parent');
                            });
                        } else {
                            bootbox.dialog({
                                message: response['msg'],
                                    title: "Información!",
                                    buttons: {
                                        danger: {
                                            label: "Cerrar",
                                            className: "btn-uven",
                                            callback: function( e){
                                                $('#confirmar').modal('show');
                                            }
                                        },
                                    }
                                });
                            $('.close').click(function(){
                                $('#confirmar').modal('show');
                            });
                            
                        }
                    },error: function (response) {
                        ModalTaxi(2);
                        $('#confirmar').modal('show');
                    }
                });
        }
            
        //});
        </script>   
   <?php
}*/
?>
