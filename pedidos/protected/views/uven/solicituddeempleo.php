<!--<script type="text/javascript">
    var int=self.setInterval("refresh()",6000);
    function refresh()
    {
        location.reload(true);
    }
</script>-->
<?php
//Mostrar Errores del controlador

foreach (Yii::app()->user->getFlashes() as $key => $message) {
  
    ?>
    <script>
    bootbox.dialog({
        message: '<?php echo $message;?>',
        title: "<?php echo $key; ?>",
        buttons: {
            danger: {
                label: "Cerrar",
                className: "btn-uven",
            },
        }
    });
    </script>
    <?php

}
?> 
<div id="loginbox" style="margin-top:50px;" >
    <div class="modal"></div>    
    <h1 class="titulo">Solicitud de Empleo</h1> 



    <div class="panel panel-danger " >


        <div style="padding-top:30px" class="panel-body" >

            <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
            
            <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'subir-p_personas', 'htmlOptions' => array('method' => 'post', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')));
             $user = Yii::app()->user->id->tgene_permi;
             $conexion=yii::app()->db;
             $tipo='2';
             $sql="SELECT *, date_format(usuar_fcrea,'%d/%m/%Y')  fecha FROM admin_usuarios  
             where usuar_tipou = 2";
             $result=$conexion->createCommand($sql)->queryRow();
             if(!$result){
                
             }
            
            //echo $sql;
            ?>
            <div class="table-responsive">
                <table id="auditoria" class="table table-bordered table-hover dataTable" style="width: 100%">
                <thead>
                    <tr>
                        <th width="2%">
                            #
                        </th>
                        <th width="20%">
                            Fecha solicitud
                        </th>
                        <th width="30%">
                            Solicitante
                        </th>
                        <th width="30%">
                            Correo electrónico
                        </th>
                        <th width="15%">
                            Teléfono
                        </th>

                        <th width="10%">
                            Estatus
                        </th>
                        <?php

                        //echo $sql;
                        $resultado=$conexion->createCommand($sql)->queryRow();
                        
                        
                        echo '<th width="7.5%">Ver</th>';
                      
                        ?>
                        
                    </tr>
                </thead>

                <tbody>
                    <?php                   
                    
                    $result=$conexion->createCommand($sql)->query();
                    $i=0;
                    $disabled='';
                    while (($row=$result->read())!=false) {
                       $i++;
                       switch ($row['usuar_statu']) {
                            case '0':
                               $estatu='Pendiente';
                               break;
                            case '1':
                               $estatu='Aprobado';
                               $disabledA='disabled';
                               $disabledR='';
                               break;
                            case '2':
                               $estatu='Rechazado';
                               $disabledA='';
                               $disabledR='disabled';
                               break;
                            
                            default:
                               $estatu='N/D';
                               break;
                       }



                       echo "<tr>";
                       echo "<td>".$i."</td>";
                       echo "<td>".$row['fecha']."</td>";
                       echo "<td>".$row['usuar_nombr']." ".$row['usuar_apell']."</td>";
                       echo "<td>".$row['usuar_login']."</td>";
                       echo "<td>".$row['usuar_telef']."</td>";
                       echo "<td>".$estatu."</td>";
                       echo '<td>';
                       echo '<a href="SolicitudEmpleoModal?c='.$row['usuar_codig'].'&t='.$row['usuar_statu'].'" data-remote="false" data-toggle="modal" data-target="#modal" title="Consultar" class="btn btn-uven btn-block"><span class="fa fa-search"></span></a>';

                       echo '</td>';
                      
                       echo "</tr>";
                    }
                    ?>
                </tbody>
            </table>
            </div>
            

            <?php $this->endWidget(); ?>

        </div><!-- form -->
    </div>  
</div>

<!-- Modal -->
<style type="text/css">
    .modal-header{
        background-color: #FFBF1C;
        color: #000;
    }
</style>

<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <div id='form-modal'>
            
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#aprobar').click(function (e) {
            var formData = new FormData($("#form-modal")[0]);
            e.preventDefault();
            $.ajax({
                dataType: "json",
                url: 'actualizarauditoria',
                type: 'POST',
                cache: false,
                data: formData,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    //alert(response['success']);
                    if (response['success'] == 'true') {
                        bootbox.dialog({
                            message: response['msg'],
                            title: "Exito!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                    callback: function(){
                                       window.open('seguridad_usuarios', '_parent');
                                    }
                                },
                            }
                        });
                        $('.close').click( function(){
                            window.open('seguridad_usuarios?t=' + t, '_parent');
                        });
                    } else {

                        bootbox.dialog({
                            message: response['msg'],
                            title: "Información!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                },
                            }
                        });
                        setTimeout(function () {
                            $('.close').click();
                        }, 1000);
                        //alert('error');
                        // $("#subir").removeAttr('disabled');
                    }

                }
            });

        });
        

    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find("#form-modal").load(link.attr("href"));
    });
</script>
