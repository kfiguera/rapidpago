<?php
//Mostrar Errores del controlador
foreach (Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="alert alert-' . $key . '">' . $message . "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> </div>\n";
}
?> 
<div id="loginbox" style="margin-top:50px;" >
    <?php
    /* echo '<pre>';
      var_dump($model);
      echo '</pre>'; */
    ?>
    <div class="modal"></div>    
    <?php
        switch ($_GET['t']) {
            case '1':
            ?>
                <h1 class="titulo">Pasajero</h1> 
            <?php
                break;
            case '2':
                ?>
                <h1 class="titulo">Taxista</h1> 
            <?php
                break;
            case '3':
               ?>
                <h1 class="titulo">Corporativo</h1> 
            <?php
                break;
            case '4':
                ?>
                <h1 class="titulo">Administrador</h1> 
            <?php
                break;
            default:
                ?>
                <h1 class="titulo">Usuarios</h1> 
            <?php
                break;
        }
    ?>



    <div class="panel panel-danger " >


        <div style="padding-top:30px" class="panel-body" >

            <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
            <div class="row">
                <div class="col-md-2">
                    <a href="./UsuariosIncluir?t=<?php echo $_GET['t']?>&o=I" class="btn btn-uven btn-block">Nuevo</a>
                </div>
            </div>
             <br></br>
            <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'subir-p_personas', 'htmlOptions' => array('method' => 'post', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')));
             $user = Yii::app()->user->id->tgene_permi;
            ?>
            <div class="table-responsive">
                <table id="auditoria" class="table table-bordered table-hover dataTable" style="width: 100%">
                <thead>
                    <tr>
                        <th width="2%">
                            #
                        </th>
                        <th>
                            Usuario
                        </th>
                        <th>
                            Nombre
                        </th>
                        <?php 
                            if ($_GET['t'] == '')
                            {
                        ?>
                        <th>
                            Tipo
                        </th>
                        <?php
                        }
                        ?>
                        <th width="10%">
                            Ver
                        </th>
                        <th width="10%">
                            Modificar
                        </th>
                        <th width="10%">
                            Eliminar
                        </th>
                        
                    </tr>
                </thead>

                <tbody>
                    <?php
                    if ($_GET['t'] != '')
                    {
                         $sql="SELECT * FROM admin_usuarios where usuar_tipou = ".$_GET['t'];
                    }else{

                         $sql="SELECT * FROM admin_usuarios ";
                    }                   
                    $conexion=yii::app()->db;
                    $result=$conexion->createCommand($sql)->query();
                    $i=0;
                    while (($row=$result->read())!=false) {
                       $i++;
                       echo "<tr>";
                       echo "<td>".$i."</td>";
                       echo "<td>".$row['usuar_login']."</td>";
                       echo "<td>".$row['usuar_nombr'].' '.$row['usuar_apell']."</td>";
                       
                       if ($_GET['t'] == '')
                            {

                                switch ($row['usuar_tipou']) {
                                    case '1':
                                        echo "<td>Pasajero</td>";
                                        break;
                                    case '2':
                                        echo "<td>Taxista</td>";
                                        break;
                                    case '3':
                                        echo "<td>Corporativo</td>";
                                        break;
                                    case '4':
                                        echo "<td>Administrador</td>";
                                        break;
                                    default:
                                        # code...
                                        break;
                                }
                        }
                       echo "<td><a href='./seguridad_usuariosConsultar?t=".$row['usuar_tipou']."&c=".$row['usuar_login']."'class='btn btn-uven btn-block'><i class='fa fa-search'></i></a></td>";
                       echo "<td><a href='./seguridad_usuariosModificar?t=".$row['usuar_tipou']."&c=".$row['usuar_login']."' class='btn btn-uven btn-block'><i class='fa fa-pencil'></i></a></td>";
                       
                        echo "<td><a href='modal2?t=".$row['usuar_tipou']."&c=".$row['usuar_login']."' data-remote='false' data-toggle='modal' data-target='#modal' class='btn btn-uven btn-block'><i class='fa fa-close'></i></a></td>";
                       echo "</tr>";
                    }
                    ?>
                </tbody>
            </table>
            </div>
            

            <?php $this->endWidget(); ?>

        </div><!-- form -->
    </div>  
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body  ">
                    <div class="row">
                        <div class="col-xs-6">
                            <h4>¿Está realmente seguro de eliminar el Usuario?</h4>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button id="subir" name="subir" value="subir" type="button" class="btn btn-danger"><span class="fa fa-close"></span> Eliminar  </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>

            <div id="modal-body">
                
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div id="modal-body">
                
            </div>
                
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('button[aprobar="true"]').click(function (e) {
            e.preventDefault();
            aux = $(this);
            var archivo = $(this).attr('archivo');
            var cedula = $(this).attr('cedula');
            var rd_preregistro_estatus = $(this).attr('estatus');
            while (aux.prop('tagName') != "TR")
                aux = aux.parent();
            $.ajax({
                dataType: "json",
                url: 'actualizarauditoria?cedula=' + cedula + '&estatus=' + rd_preregistro_estatus + '&nombre=' + archivo,
                type: 'get',
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    //alert(response['success']);
                    if (response['success'] == 'true') {
                        bootbox.dialog({
                            message: response['msg'],
                            title: "Exito!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                    callback: function(){
                                       window.open('seguridad_usuarios', '_parent');
                                    }
                                },
                            }
                        });
                        $('.close').click( function(){
                            window.open('seguridad_usuarios?t=' + t, '_parent');
                        });
                    } else {

                        bootbox.dialog({
                            message: response['msg'],
                            title: "Información!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                },
                            }
                        });
                        setTimeout(function () {
                            $('.close').click();
                        }, 1000);
                        //alert('error');
                        // $("#subir").removeAttr('disabled');
                    }

                }
            });

        });

    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find("#modal-body").load(link.attr("href"));
    });
</script>
