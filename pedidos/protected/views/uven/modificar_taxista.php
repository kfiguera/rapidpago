<?php
//Mostrar Errores del controlador
foreach (Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="alert alert-' . $key . '">' . $message . "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> </div>\n";
}
?> 
<div id="loginbox" style="margin-top:50px;" class="mainbox col-md-12">           
    <style>
        

        .image-preview-input {
            position: relative;
            overflow: hidden;
            margin: 0px;    
            color: #333;
            background-color: #fff;
            border-color: #ccc;    
        }
        .image-preview-input input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            padding: 0;
            font-size: 20px;
            cursor: pointer;
            opacity: 0;
            filter: alpha(opacity=0);
        }
        .image-preview-input-title {
            margin-left:2px;
        }
    </style>  
    <h1 class="titulo">Modificar Taxista</h1>
    <div class="panel panel-danger" >


        <div style="padding-top:30px" class="panel-body" >

            <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>


            <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'subir-p_personas', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            
            ?>
                <div class="row hide">
                    
                    <div class="col-sm-6">
                        <div class="form-group">  
                        <!-- image-preview-filename input [CUT FROM HERE]-->

                        <label for="clvarea">Estatus</label>
                        <?php
                        
                          echo CHtml::hiddenField('status', $model['usuar_status'], array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                        ?>

                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">  
                        <!-- image-preview-filename input [CUT FROM HERE]-->

                        <label for="clvarea">Tipo</label>
                        <?php
                        
                          echo CHtml::hiddenField('tipo', $model['usuar_tipou'], array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                        ?>

                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group"> 
                                <label for="clvarea">&nbsp;Foto</label>
                                <img src="<?php echo Yii::app()->getBaseUrl(true).'/'.$model["usuar_rfoto"] ?>" class="img-responsive img-thumbnail center-block" style=" margin: 0 auto;">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            
                            
                            <div class="col-sm-6">
                                <div class="form-group">  
                                <!-- image-preview-filename input [CUT FROM HERE]-->

                                <label for="clvarea">Correo</label>
                                <?php
                                
                                  echo CHtml::textField('correo', $model['usuar_login'], array('prompt' => 'Seleccione...', "class" => "form-control ",'readonly'=>'readonly')) 
                                ?>

                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">  
                                <!-- image-preview-filename input [CUT FROM HERE]-->

                                <label for="clvarea">Cédula</label>
                                <?php
                                
                                  echo CHtml::textField('cirif', $model['usuar_cirif'], array('prompt' => 'Seleccione...', "class" => "form-control ", "style"=>"text-transform:uppercase;", "onkeyup"=>"javascript:this.value=this.value.toUpperCase();")) 
                                ?>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            
                        
                            <div class="col-sm-6">
                                <div class="form-group">  
                                <!-- image-preview-filename input [CUT FROM HERE]-->

                                <label for="clvarea">Nombre</label>
                                <?php
                                
                                  echo CHtml::textField('nombre', $model['usuar_nombr'], array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                                ?>

                                </div>
                            </div>
                        
                            <div class="col-sm-6">
                                <div class="form-group">  
                                <!-- image-preview-filename input [CUT FROM HERE]-->

                                <label for="clvarea">Apellido</label>
                                <?php
                                
                                  echo CHtml::textField('apellido', $model['usuar_apell'], array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                                ?>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="numero1">
                                    <label>Foto</label>
                                    <div  class="input-group image-preview">  
                                        <img id="dynamic">
                                        <!-- image-preview-filename input [CUT FROM HERE]-->
                                        <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                        <span class="input-group-btn">
                                            <!-- image-preview-clear button -->
                                            <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                                <span class="fa fa-times"></span> Limpiar
                                            </button>
                                            <!-- image-preview-input -->
                                            <div class="btn btn-default image-preview-input">
                                                <span class="fa fa-folder-open"></span>
                                                <span class="numero1 image-preview-input-title">Buscar</span>
                                                <input type="file" accept="text/csv" id="archivo" name="archivo"/> <!-- rename it -->
                                            </div>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">  
                                <!-- image-preview-filename input [CUT FROM HERE]-->

                               <label for="clvarea">Teléfono</label>
                                <?php
                                
                                  echo CHtml::textField('telefono', $model['usuar_telef'], array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                                ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                
            
                
                <!--<div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">  
                        

                        <label for="clvarea">Contraseña</label>
                        <?php
                        
                          //echo CHtml::passwordField('contra', '', array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                        ?>

                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">  
                        

                        <label for="clvarea">Confirmar Contraseña</label>
                        <?php
                        
                         // echo CHtml::passwordField('ccontra', '', array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                        ?>

                        </div>
                    </div>
                </div>-->
        </div>
    </div> 
    <h1 class="titulo">Vehículo</h1>

    <div class="panel panel-danger" >


        <div style="padding-top:30px" class="panel-body" >  
            <div class="row">
                <div class="col-md-4">
                    <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group"> 
                                <label for="clvarea">&nbsp;Foto</label>
                                <img src="<?php echo Yii::app()->getBaseUrl(true).'/'.$model["vehic_rfoto"] ?>" class="img-responsive img-thumbnail center-block" style="margin: 0 auto;">
                                </div>
                            </div>
                        </div>
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">  
                            <!-- image-preview-filename input [CUT FROM HERE]-->

                            <label for="clvarea">Marca</label>
                            <?php
                            
                              $sql="SELECT * FROM admin_tgenerica WHERE tgene_tipod ='MAR'";
                                $conexion=Yii::app()->db;
                                $marca=$conexion->createCommand($sql)->query();
                                $datos=CHtml::listData($marca,'tgene_codig','tgene_descr');

                                  echo CHtml::dropDownList('marca', $model['tgene_marca'], $datos, array('ajax' => array(
                                        'type' => 'POST',
                                        'data' => array('marca' => 'js:this.value' ),
                                        'url' => CController::createUrl('generarModelo'),
                                        'update' => '#modelo'
                                    ),'prompt' => 'Seleccione...', "class" => "form-control ")) 
                                            
                            ?>

                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">  
                           
                            <label for="clvarea">Modelo</label>
                            <?php
                      
                        echo  CHtml::dropDownList('modelo',$model['tdepe_descr'], CHtml::listData(Tgenerica::model()->findAll("tgene_tipod ='NUC' ORDER BY tgene_descr ASC"), 'tgene_codig', 'tgene_descr'),array( 'prompt' => 'Seleccione...', "class" => "form-control "))
                          //echo CHtml::dropDownList('modelo', '','', array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                       
                            ?>

                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">  
                            <!-- image-preview-filename input [CUT FROM HERE]-->

                            <label for="clvarea">Color</label>
                            <?php
                            $sql="SELECT * FROM admin_tgenerica WHERE tgene_tipod ='COL'";
                        $conexion=Yii::app()->db;
                        $color=$conexion->createCommand($sql)->query();
                        $datos=CHtml::listData($color,'tgene_codig','tgene_descr');  
                              echo CHtml::dropDownList('color', $model['tgene_color'], $datos, array('prompt' => 'Seleccione...',"class" => "form-control ")) 
                            ?>

                            </div>
                        </div>
                        
                    
                        <div class="col-sm-6">
                            <div class="form-group">  
                            <!-- image-preview-filename input [CUT FROM HERE]-->

                            <label for="clvarea">Tipo</label>
                            <?php
                            $sql="SELECT * FROM admin_tgenerica WHERE tgene_tipod ='TIP'";
                        $conexion=Yii::app()->db;
                        $tipo=$conexion->createCommand($sql)->query();
                        $datos=CHtml::listData($tipo,'tgene_codig','tgene_descr');   

                              echo CHtml::dropDownList('tipo2', $model['tgene_tipov'],$datos, array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                            ?>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">  
                            <!-- image-preview-filename input [CUT FROM HERE]-->

                            <label for="clvarea">Año</label>
                            <?php
                              
                        $datos=array();
                        for ($i=date('Y'); $i >= 1900 ; $i--) { 
                            $datos[$i]=$i;
                        }  
                              echo CHtml::dropDownList('ano', $model['vehic_anoca'],$datos, array('prompt' => 'Seleccione...',"class" => "form-control ")) 
                            ?>

                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">  
                            <!-- image-preview-filename input [CUT FROM HERE]-->

                            <label for="clvarea">Placa</label>
                            <?php
                            
                              echo CHtml::textfield('placa', $model['vehic_placa'], array('prompt' => 'Seleccione...','readonly'=>'readonly' , "class" => "form-control ")) 
                            ?>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-sm-12">
                        <div class="numero2">
                            <label>Foto</label>
                            <div style="margin-bottom: 25px" class="input-group image-preview">  
                                <!-- image-preview-filename input [CUT FROM HERE]-->
                                
                                <input type="text" class="numero2 form-control image-preview-filename" id="nombre2" name="nombre2" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                <span class=" numero2 input-group-btn">
                                    <!-- image-preview-clear button -->
                                    <button type="button" class="numero2 btn btn-default image-preview-clear" style="display:none;">
                                        <span class="numero2 fa fa-times"></span> Limpiar
                                    </button>
                                    <!-- image-preview-input -->
                                    <div class="numero2 btn btn-default image-preview-input">
                                        <span class="numero2 fa fa-folder-open"></span>
                                        <span class="numero2 image-preview-input-title">Buscar</span>
                                        <input type="file" id="archivo2" name="archivo2"/> <!-- rename it -->
                                    </div>
                                </span>
                                <!-- /input-group image-preview [TO HERE]--> 
                            </div>
                           
                        </div>
                    </div>
                </div>  
                </div>
            </div>
            
            <div class="row">
                <!-- Button -->
                <div class="controls">
                    <div class="col-sm-6">
                        <button id="subir" name="subir" value="subir" type="button" class="btn-block btn btn-uven"><span class="fa fa-upload"></span> Guardar  </button>

                    </div>
                    <div class="col-sm-6">
                        <button id="subir" name="limpiar" value="limpiar" type="reset" class="btn-block btn btn-default"><span class="fa fa-eraser"></span> Limpiar  </button>

                    </div>
                </div>
            </div>              

            <?php $this->endWidget(); ?>

        </div><!-- form -->
    </div>  
</div>
<script>

$(document).on('click', '#close-preview', function () {
    $('.numero1 .image-preview').popover('hide');
    // Hover befor close the preview
    $('.numero1 .image-preview').hover(
        function () {
            $('.image-preview').popover('hide');
        },
        function () {
            $('.image-preview').popover('hide');
        }
    );
});
$(function () {
    // Create the close button
    var closebtn = $('<button/>', {
        type: "button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    $('.numero1 .image-preview').popover({
        trigger:'manual',
        html:true,
        title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
        content: "No hay imagen",
        placement:'top'
    });
    // Clear event
    $('.numero1 .image-preview-clear').click(function () {
        $('.numero1 .image-preview').attr("data-content", "").popover('hide');
        $('.numero1 .image-preview-filename').val("");
        $('.numero1 .image-preview-clear').hide();
        $('.numero1 .image-preview-input input:file').val("");
        $(".numero1 .image-preview-input-title").text("Buscar");
    });
    // Create the preview image
    $(".numero1 .image-preview-input input:file").change(function () {
        var img = $('<img/>', {
            id: 'dynamic',
            width: 250,
            height: 200
        });
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".numero1 .image-preview-input-title").text("Cambiar");
            $(".numero1 .image-preview-clear").show();
            $(".numero1 .image-preview-filename").val(file.name);
            img.attr('src', e.target.result);
            $(".numero1 .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
            }
        reader.readAsDataURL(file);
    });
});
</script>
<script>

$(document).on('click', '#close-preview', function () {
    $('.numero2 .image-preview').popover('hide');
    // Hover befor close the preview
    $('.numero2 .image-preview').hover(
        function () {
            $('.image-preview').popover('hide');
        },
        function () {
            $('.image-preview').popover('hide');
        }
    );
});
$(function () {
    // Create the close button
    var closebtn = $('<button/>', {
        type: "button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });
    closebtn.attr("class","close pull-right");
    $('.numero2 .image-preview').popover({
        trigger:'manual',
        html:true,
        title: "<strong>Vista previa</strong>"+$(closebtn)[0].outerHTML,
        content: "No hay imagen",
        placement:'top'
    });
    // Clear event
    $('.numero2 .image-preview-clear').click(function () {
        $('.numero2 .image-preview').attr("data-content", "").popover('hide');
        $('.numero2 .image-preview-filename').val("");
        $('.numero2 .image-preview-clear').hide();
        $('.numero2 .image-preview-input input:file').val("");
        $(".numero2 .image-preview-input-title").text("Buscar");
    });
    // Create the preview image
    $(".numero2 .image-preview-input input:file").change(function () {
        var img = $('<img/>', {
            id: 'dynamic',
            width: 250,
            height: 200
        });
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".numero2 .image-preview-input-title").text("Cambiar");
            $(".numero2 .image-preview-clear").show();
            $(".numero2 .image-preview-filename").val(file.name);
            img.attr('src', e.target.result);
            $(".numero2 .image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
            }
        reader.readAsDataURL(file);
    });
});
</script>
<script type="text/javascript">
    $('#telefono').mask('(9999)999.99.99');
    
    $(document).ready(function () {

        $('#subir-p_personas').bootstrapValidator({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                apellido: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        regexp: {
                            regexp: /^[a-zA-ZñÑ\s]*$/,
                            message: 'Estimado(a) Usuario(a) el campo "Apellido" Solo puede poseer letras.'
                        }

                    }
                },
                nombre: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        regexp: {
                            regexp: /^[a-zA-ZñÑ\s]*$/,
                            message: 'Estimado(a) Usuario(a) el campo "Nombre" Solo puede poseer letras.'
                        }

                    }
                },
                cirif: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        regexp: {
                            regexp: /^\d{5,12}$/,
                            message: 'Estimado(a) Usuario(a) el campo "Cédula" Solo puede poseer números.'
                        }

                    }
                },
                /*foto: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                    }
                },*/
                telefono: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        regexp: {                                              
                            regexp: /^\(?(?:[0]{1}[2,4]{1}\d{2}?)\)?\d\d\d[- .]?\d\d[- .]?\d\d$/,
                            message: 'Escriba un número teléfonico de 11 dígitos'
                        }

                    }
                },
                correo: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        identical: {
                            field: 'ccorreo',
                            message: 'El Correo y Su Confirmacion deben ser iguales'
                        },
                        regexp: {
                            regexp: /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/,
                            message: 'Estimado(a) Usuario(a) el campo "Correo" debe poseer el siguiente formato: ejemplo@uven.com.ve'
                        }

                    }
                },
                ccorreo: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        identical: {
                            field: 'correo',
                            message: 'El Correo y Su Confirmacion deben ser iguales'
                        },
                        regexp: {
                            regexp: /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/,
                            message: 'Estimado(a) Usuario(a) el campo "Confirmar Correo" debe poseer el siguiente formato: ejemplo@ejemplo.com'
                        }

                    }
                },
                /*contra: {
                    validators: {
                       
                        identical: {
                            field: 'ccontra',
                            message: 'La Contraseña y Su Confirmacion deben ser iguales'
                        },
                        different: {
                            field: 'correo',
                            message: 'La Contraseña no puede ser igual al Correo'
                        },
                        regexp: {
                            regexp: /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/,
                            message: 'Estimado(a) Usuario(a) el campo "Confirmar Contraseña" debe tener al entre 8 y 16 caracteres, al menos un dígito, al menos una minúscula y al menos una mayúscula. debe poseer el siguiente formato: w3Unpocodet0d0'
                        }

                    }
                    
                },
                ccontra: {
                    validators: {
                       
                        identical: {
                            field: 'contra',
                            message: 'La Contraseña y Su Confirmacion deben ser iguales'
                        },
                        different: {
                            field: 'correo',
                            message: 'La Contraseña no puede ser igual al Correo'
                        },
                        regexp: {
                            regexp: /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/,
                            message: 'Estimado(a) Usuario(a) el campo "Confirmar Contraseña" debe tener al entre 8 y 16 caracteres, al menos un dígito, al menos una minúscula y al menos una mayúscula. debe poseer el siguiente formato: w3Unpocodet0d0'
                        }

                    }
                },*/
            }
        });
    });
    $('#subir').click(function () {
        var formData = new FormData($("#subir-p_personas")[0]);
        $('#subir-p_personas').bootstrapValidator('validate'); //secondary validation using Bootstrap Validator      
        var bootstrapValidator = $('#subir-p_personas').data('bootstrapValidator');
        if (bootstrapValidator.isValid()) {
            $.ajax({
                'dataType': "json",
                'data': formData,
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    //alert(response['success']);
                    $("#body").removeClass("loading");
                    if (response['success'] == 'true') {
                        $("#body").removeClass("loading");
                        bootbox.dialog({
                            message: response['msg'],
                            title: "Exito!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                    callback: function(){
                                       window.open('seguridad_usuarios', '_parent');
                                    }
                                },
                            }
                        });
                            $('.close').click(function(){
                                 window.open('seguridad_usuarios', '_parent');
                             });
                    } else {
                        $("#body").removeClass("loading");
                        bootbox.dialog({
                            message: response['msg'],
                            title: "Información!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                },
                            }
                        });
                        
                        $("#subir").removeAttr('disabled');
                    }

                }
            });
        }
    });
</script>