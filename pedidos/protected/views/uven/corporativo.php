<?php
//Mostrar Errores del controlador
foreach (Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="alert alert-' . $key . '">' . $message . "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> </div>\n";
}
?> 
<div id="loginbox" style="margin-top:50px;" class="mainbox col-md-12">           
    <style>
        

        .image-preview-input {
            position: relative;
            overflow: hidden;
            margin: 0px;    
            color: #333;
            background-color: #fff;
            border-color: #ccc;    
        }
        .image-preview-input input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            padding: 0;
            font-size: 20px;
            cursor: pointer;
            opacity: 0;
            filter: alpha(opacity=0);
        }
        .image-preview-input-title {
            margin-left:2px;
        }
    </style>  
    <?php
        $form = $this->beginWidget('CActiveForm', array('id' => 'subir-p_personas', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
        ?>
    <h1 class="titulo">Corporativo</h1>
    <div class="panel panel-danger" >


        <div style="padding-top:30px" class="panel-body" >

            <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>


            
                <div class="row hide">
                    
                    <div class="col-sm-6">
                        <div class="form-group">  
                        <!-- image-preview-filename input [CUT FROM HERE]-->

                        <label for="clvarea">Estatus</label>
                        <?php
                        
                          echo CHtml::hiddenField('status', '1', array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                        ?>

                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">  
                        <!-- image-preview-filename input [CUT FROM HERE]-->

                        <label for="clvarea">Tipo</label>
                        <?php
                        
                          echo CHtml::hiddenField('tipo', $tipo, array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                        ?>

                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">  
                        <!-- image-preview-filename input [CUT FROM HERE]-->

                        <label for="clvarea">Apellido</label>
                        <?php
                        
                          echo CHtml::hiddenField('apellido', '', array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                        ?>

                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="numero1">
                <label>Foto (Foto Carnet)</label>
                <div  class="input-group image-preview">  
                    <img id="dynamic">
                    <!-- image-preview-filename input [CUT FROM HERE]-->
                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                    <span class="input-group-btn">
                        <!-- image-preview-clear button -->
                        <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                            <span class="fa fa-times"></span> Limpiar
                        </button>
                        <!-- image-preview-input -->
                        <div class="btn btn-default image-preview-input">
                            <span class="fa fa-folder-open"></span>
                            <span class="numero1 image-preview-input-title">Buscar</span>
                            <input type="file" accept="text/csv" id="archivo" name="archivo"/> <!-- rename it -->
                        </div>
                    </span>
                    <!-- /input-group image-preview [TO HERE]--> 
                </div>
               
            </div>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-sm-12">
                        <div class="form-group">  
                        <!-- image-preview-filename input [CUT FROM HERE]-->

                        <label for="clvarea">Nombre de la Empresa</label>
                        <?php
                        
                          echo CHtml::textField('nombre', '', array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                        ?>

                        </div>
                    </div>
                    
                </div>
            <div class="row">
                <div class="col-sm-6">
                        <div class="form-group">  
                        <!-- image-preview-filename input [CUT FROM HERE]-->

                        <label for="clvarea">RIF</label>
                        <div class="row">
                            <div class="col-xs-6">
                            <?php
                        
                              echo CHtml::dropDownList('nacio', '',array('J'=>'J','G'=>'G'), array('prompt' => 'Elija', "class" => " form-control ")) 
                            ?>
                            </div>
                            <div class="col-xs-6">
                            <?php
                        
                              echo CHtml::textField('cirif', '', array('prompt' => 'Elija', "class" => "form-control ")) 
                            ?>
                            </div>
                        </div>

                        
                            
                        </div>
                        
                        

                        </div>
                    <div class="col-sm-6">
                        <div  class="form-group">  
                        <!-- image-preview-filename input [CUT FROM HERE]-->

                        <label for="clvarea">Teléfono</label>
                        <?php
                        
                          echo CHtml::textField('telefono', '', array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                        ?>

                        </div>
                    </div>
                    </div>
                    
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">  
                        <!-- image-preview-filename input [CUT FROM HERE]-->

                        <label for="clvarea">Correo</label>
                        <?php
                        
                          echo CHtml::textField('correo', '', array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                        ?>

                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">  
                        <!-- image-preview-filename input [CUT FROM HERE]-->

                        <label for="clvarea">Confirmar Correo</label>
                        <?php
                        
                          echo CHtml::textField('ccorreo', '', array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                        ?>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">  
                        <!-- image-preview-filename input [CUT FROM HERE]-->

                        <label for="clvarea">Contraseña</label>
                        <?php
                        
                          echo CHtml::passwordField('contra', '', array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                        ?>

                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">  
                        <!-- image-preview-filename input [CUT FROM HERE]-->

                        <label for="clvarea">Confirmar Contraseña</label>
                        <?php
                        
                          echo CHtml::passwordField('ccontra', '', array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                        ?>

                        </div>
                    </div>
                </div>

            
            <!--<div class="row">
                
                <div class="controls">
                    <div class="col-sm-6">
                        <button id="subir" name="subir" value="subir" type="submit" class="btn-block btn btn-uven"><span class="fa fa-upload"></span> Guardar  </button>

                    </div>
                    <div class="col-sm-6">
                        <button id="subir" name="limpiar" value="limpiar" type="reset" class="btn-block btn btn-default"><span class="fa fa-eraser"></span> Limpiar  </button>

                    </div>
                </div>
            </div>  -->            

           

        </div><!-- form -->
    </div>
    <h1 class="titulo">Tarjeta de crédito</h1>
    <div class="panel panel-danger" >
        <div style="padding-top:30px" class="panel-body" >

            <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                
                
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">  
                        <!-- image-preview-filename input [CUT FROM HERE]-->

                        <label for="clvarea">Tipo de tarjeta</label>
                        <?php
                        
                          echo CHtml::dropDownList('tipo_tarjeta', $param['tarje_tipot'], array("1"=>"American Express","2"=>"Master","3"=>"Visa"), array('prompt' => 'Seleccione...', "class" => "form-control ")) 

                        ?>

                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">  
                        <!-- image-preview-filename input [CUT FROM HERE]-->

                        <label for="clvarea">Número de tarjeta</label>
                        <?php
                        
                          echo CHtml::textField('numero_tarjeta', $param['tarje_numer'], array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                        ?>

                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">  
                        <!-- image-preview-filename input [CUT FROM HERE]-->

                        <label for="clvarea">CVC</label>
                        <?php
                        
                          echo CHtml::textField('codigo_tarjeta', $param['tarje_coccv'], array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                        ?>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">  
                        <!-- image-preview-filename input [CUT FROM HERE]-->

                        <label for="clvarea">Fecha de vencimiento</label>
                        <?php
                        
                          echo CHtml::textField('fecha_tarjeta', $param['tarje_fvenc'], array('prompt' => 'Seleccione...', "class" => "form-control ")) 
                        ?>


                        </div>
                    </div>
                </div>

            <div style="margin-top:10px" class="form-group">
                <!-- Button -->
                <div class="row controls">
                    <div class="col-xs-12">
                        <div class="btn-group btn-group-justified" role="group" aria-label="...">

                            <div class="btn-group" role="group">
                                <button id="subir" name="subir" value="subir" type="button" class="btn-block btn btn-uven"><span class="fa fa-upload"></span> Guardar  </button>
                            </div>
                            <div class="btn-group" role="group">
                                <button id="subir" name="limpiar" value="limpiar" type="reset" class="btn-block btn btn-default"><span class="fa fa-eraser"></span> Limpiar  </button>
                            </div> 
                        </div> 
                    </div>
                </div>
            </div>                
        </div><!-- form -->
    </div>
    <?php $this->endWidget(); ?>
</div>
<script>$(document).on('click', '#close-preview', function () {
    $('.numero1 .image-preview').popover('hide');
    // Hover befor close the preview
    $('.numero1 .image-preview').hover(
            function () {
                $('.image-preview').popover('hide');
            },
            function () {
                $('.image-preview').popover('hide');
            }
    );
});
$(function () {
    // Create the close button
    var closebtn = $('<button/>', {
        type: "button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });

    // Clear event
    $('.numero1 .image-preview-clear').click(function () {
        $('.numero1 .image-preview').attr("data-content", "").popover('hide');
        $('.numero1 .image-preview-filename').val("");
        $('.numero1 .image-preview-clear').hide();
        $('.numero1 .image-preview-input input:file').val("");
        $(".numero1 .image-preview-input-title").text("Buscar");
    });
    // Create the preview image
    $(".numero1 .image-preview-input input:file").change(function () {
        var img = $('<img/>', {
            id: 'dynamic',
            width: 250,
            height: 200
        });
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".numero1 .image-preview-input-title").text("Cambiar");
            $(".numero1 .image-preview-clear").show();
            $(".numero1 .image-preview-filename").val(file.name);
            img.attr('src', e.target.result);
        }
        reader.readAsDataURL(file);
    });
});
</script>
<script>$(document).on('click', '#close-preview', function () {
    $('.numero2 .image-preview').popover('hide');
    // Hover befor close the preview
    $('.numero2 .image-preview').hover(
            function () {
                $('.image-preview').popover('hide');
            },
            function () {
                $('.image-preview').popover('hide');
            }
    );
});
$(function () {
    // Create the close button
    var closebtn = $('<button/>', {
        type: "button",
        text: 'x',
        id: 'close-preview',
        style: 'font-size: initial;',
    });

    // Clear event
    $('.numero2 .image-preview-clear').click(function () {
        $('.numero2 .image-preview').attr("data-content", "").popover('hide');
        $('.numero2 .image-preview-filename').val("");
        $('.numero2 .image-preview-clear').hide();
        $('.numero2 .image-preview-input input:file').val("");
        $(".numero2 .image-preview-input-title").text("Buscar");
    });
    // Create the preview image
    $(".numero2 .image-preview-input input:file").change(function () {
        var img = $('<img/>', {
            id: 'dynamic',
            width: 250,
            height: 200
        });
        var file = this.files[0];
        var reader = new FileReader();
        // Set preview image into the popover data-content
        reader.onload = function (e) {
            $(".numero2 .image-preview-input-title").text("Cambiar");
            $(".numero2 .image-preview-clear").show();
            $(".numero2 .image-preview-filename").val(file.name);
            img.attr('src', e.target.result);
        }
        reader.readAsDataURL(file);
    });
});</script>
<script type="text/javascript">
    $('#telefono').mask('(9999)999.99.99');
    $('#fecha_tarjeta').mask('9999/99');
    $(document).ready(function () {

        $('#subir-p_personas').bootstrapValidator({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                /*apellido: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        regexp: {
                            regexp: /^[a-zA-ZñÑ\s]*$/,
                            message: 'Estimado(a) Usuario(a) el campo "Apellido" Solo puede poseer letras.'
                        }

                    }
                },*/
                numero_tarjeta:{
                    validators: {
                        creditCard: {
                            message: 'Estimado(a) Usuario(a) no es un número de tarjeta valido.'
                        }

                    }
                },
                codigo_tarjeta:{
                    validators: {
                        cvv: {
                            creditCardField: 'numero_tarjeta',
                            message: 'Estimado(a) Usuario(a) el código de la tarjeta no es valido.'
                        },

                    }
                },
                fecha_tarjeta: {
                    verbose: false,
                    validators: {
                        regexp: {
                            message: 'La fecha de vencimiento debe ser AAAA/MM',
                            regexp: /^\d{4}\/\d{1,2}$/
                        },/*
                        callback: {
                            message: 'La fecha de vencimiento ha expirado',
                            callback: function(value, validator, $field) {
                                var sections = value.split('/');
                                alert(sections.length);
                                if (sections.length !== 2) {
                                    return {
                                        valid: false,
                                        message: 'La fecha de vencimiento no es valida'
                                    };
                                }

                                var year         = parseInt(sections[0], 10),
                                    month        = parseInt(sections[1], 10),
                                    currentMonth = new Date().getMonth() + 1,
                                    currentYear  = new Date().getFullYear();

                                if (month <= 0 || month > 12 || year > currentYear + 10) {
                                    return {
                                        valid: false,
                                        message: 'La fecha de vencimiento no es valida'
                                    };
                                }

                                if (year < currentYear || (year == currentYear && month < currentMonth)) {
                                    // The date is expired
                                    return {
                                        valid: false,
                                        message: 'La fecha de vencimiento ha expirado'
                                    };
                                }

                                return true;
                            }
                        }*/
                    }
                },
                nombre: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        regexp: {
                            regexp: /^[a-zA-ZñÑ\s\w\W]*$/,
                            message: 'Estimado(a) Usuario(a) el campo "Nombre" Solo puede poseer letras.'
                        }

                    }
                },
                nacio: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        }

                    }
                },
                cirif: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        regexp: {
                            regexp: /^\d{5,11}$/,
                            message: 'Estimado(a) Usuario(a) el campo "RIF" Solo puede poseer números.'
                        }

                    }
                },
                /*foto: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                    }
                },*/
                telefono: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        regexp: {                                              
                            regexp: /^\(?(?:[0]{1}[2,4]{1}\d{2}?)\)?\d\d\d[- .]?\d\d[- .]?\d\d$/,
                            message: 'Escriba un número teléfonico de 11 dígitos'
                        }

                    }
                },
                correo: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        identical: {
                            field: 'ccorreo',
                            message: 'El Correo y Su Confirmacion deben ser iguales'
                        },
                        regexp: {
                            regexp: /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/,
                            message: 'Estimado(a) Usuario(a) el campo "Correo" debe poseer el siguiente formato: ejemplo@uven.com.ve'
                        }

                    }
                },
                ccorreo: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        identical: {
                            field: 'correo',
                            message: 'El Correo y Su Confirmacion deben ser iguales'
                        },
                        regexp: {
                            regexp: /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/,
                            message: 'Estimado(a) Usuario(a) el campo "Confirmar Correo" debe poseer el siguiente formato: ejemplo@ejemplo.com'
                        }

                    }
                },
                contra: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        identical: {
                            field: 'ccontra',
                            message: 'La Contraseña y Su Confirmacion deben ser iguales'
                        },
                        different: {
                            field: 'correo',
                            message: 'La Contraseña no puede ser igual al Correo'
                        },
                        regexp: {
                            regexp: /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/,
                            message: 'Estimado(a) Usuario(a) el campo "Confirmar Contraseña" debe tener al entre 8 y 16 caracteres, al menos un dígito, al menos una minúscula y al menos una mayúscula. debe poseer el siguiente formato: w3Unpocodet0d0'
                        }

                    }
                    
                },
                ccontra: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        identical: {
                            field: 'contra',
                            message: 'La Contraseña y Su Confirmacion deben ser iguales'
                        },
                        different: {
                            field: 'correo',
                            message: 'La Contraseña no puede ser igual al Correo'
                        },
                        regexp: {
                            regexp: /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/,
                            message: 'Estimado(a) Usuario(a) el campo "Confirmar Contraseña" debe tener al entre 8 y 16 caracteres, al menos un dígito, al menos una minúscula y al menos una mayúscula. debe poseer el siguiente formato: w3Unpocodet0d0'
                        }

                    }
                },
            }
        });
    });
    $('#subir').click(function () {
        var formData = new FormData($("#subir-p_personas")[0]);
        $('#subir-p_personas').bootstrapValidator('validate'); //secondary validation using Bootstrap Validator      
        var bootstrapValidator = $('#subir-p_personas').data('bootstrapValidator');
        if (bootstrapValidator.isValid()) {
            $.ajax({
                'dataType': "json",
                'data': formData,
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    //alert(response['success']);
                    $("#body").removeClass("loading");
                    if (response['success'] == 'true') {
                        $("#body").removeClass("loading");
                        bootbox.dialog({
                            message: response['msg'],
                            title: "Exito!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                    callback: function(){
                                       window.open('seguridad_usuarios', '_parent');
                                    }
                                },
                            }
                        });
                        $('.close').click( function(){
                            window.open('seguridad_usuarios?t=' + t, '_parent');
                        });
                    } else {
                        $("#body").removeClass("loading");
                        bootbox.dialog({
                            message: response['msg'],
                            title: "Información!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                },
                            }
                        });
                        
                        $("#subir").removeAttr('disabled');
                    }

                }
            });
        }
    });
</script>