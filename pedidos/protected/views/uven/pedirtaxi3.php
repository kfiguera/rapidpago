<style>
#mapa {
   height: 563px;
   border-radius: 5px;
}
</style>
<div class="registrar" >
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="titulos">Solicita tu <span style="color: #FFCD00;">taxi</span> aquí!</h2>
                    </div>
                    <div class="panel-body">
                        <div class="col-xs-12">
                            <form class="form-horizontal" id='pedir-taxi' action="costoservicio">
                                <div class="form-group">
                                    <div data-toggle="buttons">
                                        <label class="btn btn-uven-2 btn-block-2 active">       
                                            <h3>
                                                <b>
                                                    <input type="radio" name="q2" value="0" checked>
                                                    <i class="fa fa-user"></i> 
                                                    Pasajero
                                                </b>
                                            </h3>
                                        </label>
                                        <label class="btn btn-uven-2 btn-block-2">
                                            <h3>
                                                <b>
                                                    <input type="radio" name="q2" value="1">
                                                    <i class="fa fa-users"></i>
                                                    Corporativo
                                                </b>
                                            </h3>
                                        </label>
                                    </div>
                                </div>
                                
                                <div class="form-group ">
                                    <label>Origen</label>
                                    <input class="form-control selectpicker"  id="autocomplete" name="autocomplete" placeholder="Ingrese el origen" onFocus="geolocate()" type="text" >
                                       
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>Destino</label>
                                        <input class="form-control" type="text" id="autocompleteDes" name="autocompleteDes[]" placeholder="Ingrese el destino" onFocus="geolocate()">
                                    </div>
                                       
                                    <div class="col-sm-4">
                                        
                                        <label>Distancia</label>
                                            <input class="form-control" type="text" id="distancia" name="distancia[]">
                                    </div>
                                    <div class="col-sm-4">
                                        <label>Tiempo</label>
                                            <input class="form-control" type="text" id="tiempo" name="tiempo[]">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>Destino</label>
                                        <input class="form-control" type="text" id="autocompleteDes" name="autocompleteDes[]" placeholder="Ingrese el destino" onFocus="geolocate()">
                                    </div>
                                       
                                    <div class="col-sm-4">
                                        
                                        <label>Distancia</label>
                                            <input class="form-control" type="text" id="distancia" name="distancia[]" >
                                    </div>
                                    <div class="col-sm-4">
                                        <label>Tiempo</label>
                                            <input class="form-control" type="text" id="tiempo" name="tiempo[]">
                                    </div>
                                </div>

                                <div class="form-group hide">
                                    <label>Origen</label>
                                    <div class="input-group">
                                        <select class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
                                            <option>Ubicación Actual</option>
                                            <option>Elija Origen</option>
                                        </select>
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button" data-container="body" data-toggle="popover" data-placement="right" data-content="El origen es mi ubicacion actual."><i class="fa fa-map-marker" aria-hidden="true"></i></button>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group hide">
                                    <label>Destino</label>
                                    <div class="input-group">
                                        <select name="destino[]" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
                                            <option>Elija Destino</option>
                                        </select>
                                        <span class="input-group-btn">

                                            
                                        <button  class="btn btn-default addButton"><i class="fa fa-plus"></i></button>
                                        </span>
                                    </div>
                                </div>


                                <!-- The option field template containing an option field and a Remove button -->
                                <div class="form-group hide" id="optionTemplate">
                                
                                        <label>Destino</label>

                                    <div class="input-group">
                                        <select name="destino[]" class="form-control selectpicker" data-show-subtext="true" data-live-search="true">
                                            <option>Elija Destino</option>
                                        </select>

                                        <span class="input-group-btn">
                                        <button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="controls">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <button  id='buscar' type="button" class="btn btn-block btn-uven-2">
                                                <h4><b> <i class="fa fa-search"></i> Buscar</b></h4>
                                                </button>
                                            </div>
                                            <div class="col-sm-6">
                                                <button type="submit" class="btn btn-block btn-uven-2">
                                                <h4><b> <i class="fa fa-car"></i> Aquí Vamos!</b></h4>
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div id="mapa" class="mapa">
                    
                </div>
                
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
          $('[data-toggle="popover"]').popover({ trigger: "hover" })
        })
    </script>
    <script>
      var cordori;  
      var corddes;  
        
      var placeSearch, autocomplete, autocompleteDes;
      
      

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

         autocompleteDes = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocompleteDes')),
            {types: ['geocode']});

        
        //autocomplete.addListener('place_changed', fillInAddress);
      }

    
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }

      var divMapa = document.getElementById('mapa');
     // navigator.geolocation.getCurrentPosition(fn_ok, fn_error);
      function fn_error(){ }
      function fn_ok(rta){
        
        
        var lat = rta.coords.latitude;
        var lon = rta.coords.longitude;

        
        var gLatLon = new google.maps.LatLng(lat,lon);

        // objConfigobjeto de conf del mapa
        var objConfig = {
          zoom: 17,
          center: gLatLon
        }
        var gMapa = new google.maps.Map(divMapa,objConfig); 

        

        var gCoder = new google.maps.Geocoder(); 
        var gCoderD = new google.maps.Geocoder(); 


        var start = document.getElementById('autocomplete').value;
        var objInformacionOrigen ={
          address: start
        }

        gCoder.geocode(objInformacionOrigen, fn_coder);

        function fn_coder(datos){
            var coordenadasOri = datos[0].geometry.location;
             //objeto latitud longitud de google
             // gometry es una propiedad
            var configOrigen = {
              map: gMapa,
              animation: google.maps.Animation.DROP,
              position: coordenadasOri,
              draggable: true,
              title: 'Origen'
            }
            cordori =  coordenadasOri;
            //alert('Origen: ' + coordenadasOri);


           // marcador en google maps
           var gMarkerDV = new google.maps.Marker(configOrigen);
           //gMarkerDV.setIcon('bandera.png');  //cambia el icono del marker
           var objHTML={
            content: '<div style="height: 150px; width: 300px"><h2>Casa de Wilpia</h2><h3>Taxista disponible</h3><p>Seleccionar <a href="http://systempw.com.ve/uven/uven2">Seleccionar Taxista</a></p></div>'

          }
          var gIW = new google.maps.InfoWindow(objHTML);

          google.maps.event.addListener(gMarkerDV, 'click',function(){
            gIW.open(mapa,gMarkerDV);

          });//1ro qué objeto asociado al evento, 2do qué evento quiero asignar, 3ro la función que se asocia al click del objeto

        }


        var end = document.getElementById('autocompleteDes').value;
          var objInformacionDestino ={
            address: end
          }

        gCoderD.geocode(objInformacionDestino, fn_coderD);
        
        function fn_coderD(datosd){
            var coordenadasDes = datosd[0].geometry.location;
             //objeto latitud longitud de google
             // gometry es una propiedad
            var configDestino = {
              map: gMapa,
              animation: google.maps.Animation.DROP,
              position: coordenadasDes,
              draggable: true,
              title: 'Destino'
            }
            corddes =  coordenadasDes;
            //alert('Destino: ' + coordenadasDes);
        }

        // objConfigMarker objeto de conf del marcador
        var objConfigMarker = {
         /* position: gLatLon, //lat y long
          map: gMapa, //mapa donde se va a mostrar
          animation: google.maps.Animation.DROP,
          title: "Mi casa"*/
        }
        // marcador en google maps
        var gMarker = new google.maps.Marker(objConfigMarker);

        var objConfigDR ={
          map: gMapa,
          suppressMArkers: true
        }

        var objConfigDS = {
          origin: objInformacionOrigen.address, //latitud longitud o string de domicilio
          destination: objInformacionDestino.address,
          travelMode: google.maps.TravelMode.DRIVING
        }
        var ds = new google.maps.DirectionsService();
        var dr = new google.maps.DirectionsRenderer(objConfigDR);

        //trazar la ruta
        ds.route(objConfigDS,fnRutear )
        dr.setMap(gMapa);
        dr.setPanel(document.getElementById('right-panel'));
        function fnRutear(resultados, status){
          //muestra la línea entre origen y destino
          if(status=='OK'){
            dr.setDirections(resultados);

          }else{
            alert('Error' + status);
          }

        }
calculateDistances(objInformacionOrigen.address, objInformacionDestino.address);
        

        
      
    }


    function callback(response, status) {
            if (status!==google.maps.DistanceMatrixStatus.OK) {
                _googleError('Error was: ' + status);
            } else {
                var origins = response.originAddresses;

                for (var i = 0; i < origins.length; i++) {
                    var results = response.rows[i].elements;
                    for (var j = 0; j < results.length; j++) {
                            $("#distancia").val(results[j].distance.text);
                            $("#tiempo").val(results[j].duration.text);
                       //Other stuff that works here
                    }
                }
            }
        }

        function calculateDistances(start, end) {
            var service = new google.maps.DistanceMatrixService();
            service.getDistanceMatrix(
                {
                    origins: [start],
                    destinations: [end],
                    travelMode: google.maps.TravelMode.DRIVING,
                    unitSystem: google.maps.UnitSystem.METRIC,
                    avoidHighways: false,
                    avoidTolls: false
                }, callback);
        }
    $('#buscar').click(function(){
        navigator.geolocation.getCurrentPosition(fn_ok, fn_error);
        


    });

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAoCHVnBZdXzHqw-ipIFEW_by1cJSmcLbg&libraries=places&callback=initAutocomplete"
        async defer></script>
    <!--script>
$(document).ready(function() {
    // The maximum number of options
    var MAX_OPTIONS = 5;

    $('#pedir-taxi')
        .bootstrapValidator({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                question: {
                    validators: {
                        notEmpty: {
                            message: 'The question required and cannot be empty'
                        }
                    }
                },
                'destino[]': {
                    validators: {
                        notEmpty: {
                            message: 'The option required and cannot be empty'
                        },
                        stringLength: {
                            max: 100,
                            message: 'The option must be less than 100 characters long'
                        }
                    }
                }
            }
        })

        // Add button click handler
        .on('click', '.addButton', function() {
            var $template = $('#optionTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .insertBefore($template),
                $option   = $clone.find('[name="destino[]"]');

            // Add new field
            $('#pedir-taxi').bootstrapValidator('addField', $option);
        })

        // Remove button click handler
        .on('click', '.removeButton', function() {
            var $row    = $(this).parents('.form-group'),
                $option = $row.find('[name="destino[]"]');

            // Remove element containing the option
            $row.remove();

            // Remove field
            $('#pedir-taxi').bootstrapValidator('removeField', $option);
        })

        // Called after adding new field
        .on('added.field.fv', function(e, data) {
            // data.field   -> The field name
            // data.element -> The new field element
            // data.options -> The new field options

            if (data.field === 'option[]') {
                if ($('#pedir-taxi').find(':visible[name="destino[]"]').length >= MAX_OPTIONS) {
                    $('#pedir-taxi').find('.addButton').attr('disabled', 'disabled');
                }
            }
        })

        // Called after removing the field
        .on('removed.field.fv', function(e, data) {
           if (data.field === 'option[]') {
                if ($('#pedir-taxi').find(':visible[name="destino[]"]').length < MAX_OPTIONS) {
                    $('#pedir-taxi').find('.addButton').removeAttr('disabled');
                }
            }
        });
});



</script-->
<!--script type="text/javascript">
    $(document).ready(function () {

        $('#subir-p_personas').bootstrapValidator({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                apellido: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        regexp: {
                            regexp: /^[a-zA-ZñÑ\s]*$/,
                            message: 'Estimado(a) Usuario(a) el campo "Apellido" Solo puede poseer letras.'
                        }

                    }
                },
                nombre: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        regexp: {
                            regexp: /^[a-zA-ZñÑ\s]*$/,
                            message: 'Estimado(a) Usuario(a) el campo "Nombre" Solo puede poseer letras.'
                        }

                    }
                },
                cirif: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        regexp: {
                            regexp: /^\d{5,12}$/,
                            message: 'Estimado(a) Usuario(a) el campo "Cédula" Solo puede poseer números.'
                        }

                    }
                },
                /*foto: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                    }
                },*/
                telefono: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        regexp: {                                              
                            regexp: /^\(?(?:[0]{1}[2,4]{1}\d{2}?)\)?\d\d\d[- .]?\d\d[- .]?\d\d$/,
                            message: 'Escriba un número teléfonico de 11 dígitos'
                        }

                    }
                },
                correo: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        identical: {
                            field: 'ccorreo',
                            message: 'El Correo y Su Confirmacion deben ser iguales'
                        },
                        regexp: {
                            regexp: /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/,
                            message: 'Estimado(a) Usuario(a) el campo "Correo" debe poseer el siguiente formato: ejemplo@uven.com.ve'
                        }

                    }
                },
                ccorreo: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        identical: {
                            field: 'correo',
                            message: 'El Correo y Su Confirmacion deben ser iguales'
                        },
                        regexp: {
                            regexp: /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/,
                            message: 'Estimado(a) Usuario(a) el campo "Confirmar Correo" debe poseer el siguiente formato: ejemplo@ejemplo.com'
                        }

                    }
                },
                contra: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        identical: {
                            field: 'ccontra',
                            message: 'La Contraseña y Su Confirmacion deben ser iguales'
                        },
                        different: {
                            field: 'correo',
                            message: 'La Contraseña no puede ser igual al Correo'
                        },
                        regexp: {
                            regexp: /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/,
                            message: 'Estimado(a) Usuario(a) el campo "Confirmar Contraseña" debe tener al entre 8 y 16 caracteres, al menos un dígito, al menos una minúscula y al menos una mayúscula. debe poseer el siguiente formato: w3Unpocodet0d0'
                        }

                    }
                    
                },
                ccontra: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es obligatorio',
                        },
                        identical: {
                            field: 'contra',
                            message: 'La Contraseña y Su Confirmacion deben ser iguales'
                        },
                        different: {
                            field: 'correo',
                            message: 'La Contraseña no puede ser igual al Correo'
                        },
                        regexp: {
                            regexp: /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/,
                            message: 'Estimado(a) Usuario(a) el campo "Confirmar Contraseña" debe tener al entre 8 y 16 caracteres, al menos un dígito, al menos una minúscula y al menos una mayúscula. debe poseer el siguiente formato: w3Unpocodet0d0'
                        }

                    }
                },
            }
        });
    });
    $('#subir').click(function () {
        var formData = new FormData($("#subir-p_personas")[0]);
        $('#subir-p_personas').bootstrapValidator('validate'); //secondary validation using Bootstrap Validator      
        var bootstrapValidator = $('#subir-p_personas').data('bootstrapValidator');
        if (bootstrapValidator.isValid()) {
            $.ajax({
                'dataType': "json",
                'data': formData,
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    //alert(response['success']);
                    $("#body").removeClass("loading");
                    if (response['success'] == 'true') {
                            window.open('costoservicio?c=' + response['codigo'], '_parent');
                    } else {
                        $("#body").removeClass("loading");
                        bootbox.dialog({
                            message: response['msg'],
                            title: "Información!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                },
                            }
                        });
                        
                        $("#subir").removeAttr('disabled');
                    }

                }
            });
        }
    });
</script-->
