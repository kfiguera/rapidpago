<?php
//Mostrar Errores del controlador
foreach (Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="alert alert-' . $key . '">' . $message . "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> </div>\n";
}
?> 
<div id="loginbox" style="margin-top:50px;" class="mainbox col-md-12">           
    <style>
        

        .image-preview-input {
            position: relative;
            overflow: hidden;
            margin: 0px;    
            color: #333;
            background-color: #fff;
            border-color: #ccc;    
        }
        .image-preview-input input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            padding: 0;
            font-size: 20px;
            cursor: pointer;
            opacity: 0;
            filter: alpha(opacity=0);
        }
        .image-preview-input-title {
            margin-left:2px;
        }
    </style>  
    <h1 class="titulo">Consultar Usuarios</h1>
    <div class="panel panel-danger" >


        <div style="padding-top:30px" class="panel-body" >

            <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>


            <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'subir-p_personas', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            ?>
            <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">  
                        <!-- image-preview-filename input [CUT FROM HERE]-->

                        <label for="clvarea">Correo</label>
                        <?php
                        
                          echo CHtml::textField('correo', $model['usuar_login'], array('prompt' => 'Seleccione...',"readonly"=>"readonly", "class" => "form-control ")) 
                        ?>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">  
                        <!-- image-preview-filename input [CUT FROM HERE]-->

                        <label for="clvarea">Nombre</label>
                        <?php
                        
                          echo CHtml::textField('nombre', $model['usuar_nombr'], array('prompt' => 'Seleccione...',"readonly"=>"readonly", "class" => "form-control ")) 
                        ?>

                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">  
                        <!-- image-preview-filename input [CUT FROM HERE]-->

                        <label for="clvarea">Apellido</label>
                        <?php
                        
                          echo CHtml::textField('apellido', $model['usuar_apell'], array('prompt' => 'Seleccione...',"readonly"=>"readonly", "class" => "form-control ")) 
                        ?>

                        </div>
                    </div>
                </div>
            <div class="row">
                    <div class="col-sm-6">
                        <div  class="form-group">  
                        <!-- image-preview-filename input [CUT FROM HERE]-->

                        <label for="clvarea">Teléfono</label>
                        <?php
                        
                          echo CHtml::textField('telefono', $model['usuar_telef'], array('prompt' => 'Seleccione...',"readonly"=>"readonly", "class" => "form-control ")) 
                        ?>

                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="numero1">
                <label>Foto</label>
                <div  class="input-group image-preview">  
                    <img id="dynamic">
                    <!-- image-preview-filename input [CUT FROM HERE]-->
                    <input type="text" class="form-control image-preview-filename" id="nombre" name="nombre" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                    <span class="input-group-btn">
                        <!-- image-preview-clear button -->
                        <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                            <span class="fa fa-times"></span> Limpiar
                        </button>
                        <!-- image-preview-input -->
                        <div class="btn btn-default image-preview-input">
                            <span class="fa fa-folder-open"></span>
                            <span class="numero1 image-preview-input-title">Buscar</span>
                            <input type="file" accept="text/csv" id="archivo" name="archivo"/> <!-- rename it -->
                        </div>
                    </span>
                    <!-- /input-group image-preview [TO HERE]--> 
                </div>
               
            </div>
                    </div>
                </div>
                
                
            <div class="row">
                <!-- Button -->
                <div class="controls">
                    
                    <div class="col-sm-6 col-sm-offset-3">
                        <button type="button" class="btn-block btn btn-default" onclick="history.back()"><span class="fa fa-logout"></span> Volver  </button>

                    </div>
                </div>
            </div>              

            <?php $this->endWidget(); ?>

        </div><!-- form -->
    </div>  
</div>
<script>$(document).on('click', '#close-preview', function () {
                        $('.numero1 .image-preview').popover('hide');
                        // Hover befor close the preview
                        $('.numero1 .image-preview').hover(
                                function () {
                                    $('.image-preview').popover('hide');
                                },
                                function () {
                                    $('.image-preview').popover('hide');
                                }
                        );
                    });
                    $(function () {
                        // Create the close button
                        var closebtn = $('<button/>', {
                            type: "button",
                            text: 'x',
                            id: 'close-preview',
                            style: 'font-size: initial;',
                        });

                        // Clear event
                        $('.numero1 .image-preview-clear').click(function () {
                            $('.numero1 .image-preview').attr("data-content", "").popover('hide');
                            $('.numero1 .image-preview-filename').val("");
                            $('.numero1 .image-preview-clear').hide();
                            $('.numero1 .image-preview-input input:file').val("");
                            $(".numero1 .image-preview-input-title").text("Buscar");
                        });
                        // Create the preview image
                        $(".numero1 .image-preview-input input:file").change(function () {
                            var img = $('<img/>', {
                                id: 'dynamic',
                                width: 250,
                                height: 200
                            });
                            var file = this.files[0];
                            var reader = new FileReader();
                            // Set preview image into the popover data-content
                            reader.onload = function (e) {
                                $(".numero1 .image-preview-input-title").text("Cambiar");
                                $(".numero1 .image-preview-clear").show();
                                $(".numero1 .image-preview-filename").val(file.name);
                                img.attr('src', e.target.result);
                            }
                            reader.readAsDataURL(file);
                        });
                    });
                    </script>
                    <script>$(document).on('click', '#close-preview', function () {
                        $('.numero2 .image-preview').popover('hide');
                        // Hover befor close the preview
                        $('.numero2 .image-preview').hover(
                                function () {
                                    $('.image-preview').popover('hide');
                                },
                                function () {
                                    $('.image-preview').popover('hide');
                                }
                        );
                    });
                    $(function () {
                        // Create the close button
                        var closebtn = $('<button/>', {
                            type: "button",
                            text: 'x',
                            id: 'close-preview',
                            style: 'font-size: initial;',
                        });

                        // Clear event
                        $('.numero2 .image-preview-clear').click(function () {
                            $('.numero2 .image-preview').attr("data-content", "").popover('hide');
                            $('.numero2 .image-preview-filename').val("");
                            $('.numero2 .image-preview-clear').hide();
                            $('.numero2 .image-preview-input input:file').val("");
                            $(".numero2 .image-preview-input-title").text("Buscar");
                        });
                        // Create the preview image
                        $(".numero2 .image-preview-input input:file").change(function () {
                            var img = $('<img/>', {
                                id: 'dynamic',
                                width: 250,
                                height: 200
                            });
                            var file = this.files[0];
                            var reader = new FileReader();
                            // Set preview image into the popover data-content
                            reader.onload = function (e) {
                                $(".numero2 .image-preview-input-title").text("Cambiar");
                                $(".numero2 .image-preview-clear").show();
                                $(".numero2 .image-preview-filename").val(file.name);
                                img.attr('src', e.target.result);
                            }
                            reader.readAsDataURL(file);
                        });
                    });</script>
