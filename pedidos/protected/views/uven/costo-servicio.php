

<div class="registrar" >
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="titulos">Costo del Servicio</h2>
                    <h4 class="text-center"><?php echo date('d/m/Y - h:i A')?></h4>
                </div>
                <?php 
                
                   /* echo "<pre>";
                    var_dump($tabla);
                    var_dump($total);
                    var_dump($tarifas);
                    
                    echo "</pre>"; 
                    */
                    
                   // $ruta = '';
                   
                ?>
                <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        
                        <tr>
                            <th class="text-center titulo-costo" width="34%" >Origen</th>
                            <th class="text-center titulo-costo" width="33%" >Destino</th>
                            <th class="text-center titulo-costo" width="11%" >Distancia</th>
                            <th class="text-center titulo-costo" width="11%" >Tiempo</th>
                            <th class="text-center titulo-costo" width="11%" >Costo</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        $i=0;
                        $ruta='';
                        foreach ($tabla as $key => $value) {
                            if($i!=0){
                                $ruta .= '; ';   
                            }
                            $ruta .= $value['autocomplete'].' - '.$value['autocompleteDes'];
                            echo '<tr>';
                            echo '<td class="gris">'.$value['autocomplete'].'</td>';
                            echo '<td class="gris">'.$value['autocompleteDes'].'</td>';
                            echo '<td class="gris text-right">'.number_format($value['distancia'],2,',','.').' km</td>';
                            echo '<td class="gris text-right">'.$this->conversorSegundosHoras($value['tiempo']).'</td>';
                            echo '<td class="gris text-right"> Bs.'.number_format($value['costo'],2,',','.').'</td>';
                            echo '</tr>';
                            $i++;
                        }
                        //echo $ruta;
                    ?>
                    </tbody>
                </table>
                </div>  
                <table class="table table-bordered table-hover" >
                       
                    <tfoot>
                    <tr>
                        <td colspan="3"></td>
                        </tr>
                        <tr>
                            <td  class="text-center titulo-costo" width="34%"><b>Tipo de tarifa</b></td>
                            <td  class="text-center titulo-costo" width="33%"><b>Fin de Semana</b></td>
                            <td  class="text-center titulo-costo" width="33%"><b>Feriado</b></td>
                        </tr>

                        

                        <tr>
                            <td class="text-center gris" ><?php echo $pasajero;  ?></td>
                            <td class="text-center gris" > <?php 

                            echo number_format($tarifas["tarif_finse"],2,',','.');

                             ?></td>
                            <td class="text-center gris" ><?php echo number_format($tarifas["tarif_feria"],2,',','.'); ?></td>
                            
                        </tr>
                        <td colspan="3"></td>
                        </tr>
                        <tr>
                            <td  class="text-center titulo-costo" ><b>Multiparada</b></td>
                             <td  class="text-center titulo-costo" ><b>Distancia</b></td>
                            <td  class="text-center titulo-costo" ><b>Tiempo estimado</b></td>
                            
                            
                        </tr>
                        <tr>
                            <td class="text-center gris" ><?php echo number_format($tarifas["tarif_param"],2,',','.'); ?></td>
                            <td class="text-center gris" ><?php echo number_format($total['distancia'],2,',','.'); ?> km</td>
                            <td class="text-center gris" ><?php echo  $this->conversorSegundosHoras($total['tiempo']); ?></td>
                            
                        </tr>
                    </tfoot>
                    
                </table>
                

                    <div class="panel-body text-center ">
                    <!--<h1>Costo Base Bs. <?php //echo number_format($total['costo'],2,',','.'); ?></h1>-->
                     <h1>Costo Base Bs. <?php 
                     
                     echo number_format($totalcosto,2,',','.'); ?></h1>
                </div>
                    <div class="panel-body">
                        <div class="col-xs-12">
                            <form id='pedir-taxi'>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label>Maleta Extra</label>
                                            <select class="form-control" name="maleta" id="maleta">
                                                <option value="">Seleccione</option>
                                                <option value="1">Si - Bs. <?php echo number_format($tarifas["tarif_malet"],2,',','.'); ?></option>
                                                <option value="2" selected="">No</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label>Mascota</label>
                                            <select class="form-control" name="mascota" id="mascota">
                                                <option value="">Seleccione</option>
                                                <option value="1">Si - Bs. <?php echo number_format($tarifas["tarif_masco"],2,',','.'); ?></option>
                                                <option value="2" selected="">No</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label>Tiempo de espera</label>
                                            <select class="form-control" name="espera" id="espera">
                                                <option value="" selected>0 min</option>
                                                <option value="10">10 min - Bs. <?php echo 10 * number_format($tarifas["tarif_esper"],2,',','.'); ?></option>
                                                 <option value="20">20 min - Bs. <?php echo 20 * number_format($tarifas["tarif_esper"],2,',','.'); ?></option>
                                                <option value="30">30 min - Bs. <?php echo 30 * number_format($tarifas["tarif_esper"],2,',','.'); ?></option>
                                                <option value="40">40 min - Bs. <?php echo 40 * number_format($tarifas["tarif_esper"],2,',','.'); ?></option>
                                                <option value="50">50 min - Bs. <?php echo 50 * number_format($tarifas["tarif_esper"],2,',','.'); ?></option>
                                                <option value="60">60 min - Bs. <?php echo 60 * number_format($tarifas["tarif_esper"],2,',','.'); ?></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            
                                            <label>Cantidad de pasajeros</label>
                                            <select class="form-control" name="cantidad" id="cantidad">
                                                <option value="" selected="">1 Pasajero</option>
                                                 <option value="2">2 pasajeros - Bs. <?php echo 2 * number_format($tarifas["tarif_pasam"],2,',','.'); ?></option>
                                                <option value="3">3 pasajeros - Bs. <?php echo 3 * number_format($tarifas["tarif_pasam"],2,',','.'); ?></option>
                                                <option value="4">4 pasajeros - Bs. <?php echo 4 * number_format($tarifas["tarif_pasam"],2,',','.'); ?></option>
                                            </select>
                                            
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="section-gris">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h4 class="recargo">Recargos adicionales aplican. <a href="../../../uven2/terminos-y-condiciones">Ver condiciones del servicio</a></h4>
                                            <br>
                                        </div>
                                    </div>
                                    <div class="row hide">
                                        <div class="col-xs-12">
                                            <div class="form-group text-center">
                                                <label>Forma de Pago</label>
                                                <br>
                                                <label class="radio-inline">
                                                    <input type="radio" name="forma" id="forma1" value="1"> Pago en l&iacute;nea
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="forma" id="forma1" value="2"> Tranferencia
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="forma" id="forma2" value="3"> Efectivo
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="forma" id="forma2" value="0" checked=""> Otro
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center" id='titulo-actualizar'>
                                    <h1 id='total-titulo'>Estimado en Bs. <?php echo number_format($totalcosto,2,',','.') ?>
                                        
                                    </h1>
                                    <input type="hidden" name="total" id="total" value="<?php echo $totalcosto ?>">
                                    

                                </div>
                                <input type="hidden" name="base" id="base" value="<?php echo $totalcosto ?>">
                                    
                                    <input type="hidden" name="ruta" id="ruta" value="<?php echo $ruta ?>">
                                    <input type="hidden" name="accesorios" id="ruta" value="<?php echo $accesorios ?>">
                                    <input type="hidden" name="punto_referencia" id="total" value="<?php echo $_REQUEST['punto_referencia'] ?>">
                                <div class="row" style="padding-bottom: 15px;">
                                   
                                        <a href='./pedirtaxi2' class="btn btn-block-2 btn-uven-2">
                                        <b> <i class="fa fa-undo"></i> Volver</b>
                                        </a>
                                  
                                    
                                        <button class="btn btn-block-2 btn-uven-2 active " id="confirmar-servicio">
                                        <b> <i class="fa fa-check"></i> Confirmar</b>
                                        </button>
                                  

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
          $('[data-toggle="popover"]').popover({ trigger: "hover" })
        })
        
        $("#modal").on("show.bs.modal", function (e) {
          var link = $(e.relatedTarget);
          $(this).find(".modal-body").load(link.attr("href"));
        });
    </script>
    <script>
$(document).ready(function() {
    // The maximum number of options
    var MAX_OPTIONS = 5;

    $('#pedir-taxi')
        .bootstrapValidator({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                question: {
                    validators: {
                        notEmpty: {
                            message: 'The question required and cannot be empty'
                        }
                    }
                },
                'destino[]': {
                    validators: {
                        notEmpty: {
                            message: 'The option required and cannot be empty'
                        },
                        stringLength: {
                            max: 100,
                            message: 'The option must be less than 100 characters long'
                        }
                    }
                }
            }
        })

        // Add button click handler
        .on('click', '.addButton', function() {
            var $template = $('#optionTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .insertBefore($template),
                $option   = $clone.find('[name="destino[]"]');

            // Add new field
            $('#pedir-taxi').bootstrapValidator('addField', $option);
        })

        // Remove button click handler
        .on('click', '.removeButton', function() {
            var $row    = $(this).parents('.form-group'),
                $option = $row.find('[name="destino[]"]');

            // Remove element containing the option
            $row.remove();

            // Remove field
            $('#pedir-taxi').bootstrapValidator('removeField', $option);
        })

        // Called after adding new field
        .on('added.field.fv', function(e, data) {
            // data.field   --> The field name
            // data.element --> The new field element
            // data.options --> The new field options

            if (data.field === 'option[]') {
                if ($('#pedir-taxi').find(':visible[name="destino[]"]').length >= MAX_OPTIONS) {
                    $('#pedir-taxi').find('.addButton').attr('disabled', 'disabled');
                }
            }
        })

        // Called after removing the field
        .on('removed.field.fv', function(e, data) {
           if (data.field === 'option[]') {
                if ($('#pedir-taxi').find(':visible[name="destino[]"]').length < MAX_OPTIONS) {
                    $('#pedir-taxi').find('.addButton').removeAttr('disabled');
                }
            }
        });
});



function calcular(){
    var formData = new FormData($("#pedir-taxi")[0]);
    $.ajax({
        'data': formData,
        'url': 'calcularMonto',
        'type': 'post',
        'cache': false,
        'contentType': false,
        'processData': false,
        beforeSend: function () {
            $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
        },
        success: function (html) {
            $('#titulo-actualizar').html(html);
        }
    });
}

$('#maleta').change(function () {
    calcular();
});
$('#mascota').change(function () {
    calcular();
});
$('#espera').change(function () {
    calcular();
});
$('#cantidad').change(function () {
    calcular();
});
$('#confirmar-servicio').click(function () {
        var formData = new FormData($("#pedir-taxi")[0]);
            $.ajax({
                'dataType': "json",
                'data': formData,
                'url': 'guardarCosto',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    //alert(response['success']);
                    $("#body").removeClass("loading");
                    if (response['success'] == 'true') {
                        $("#body").removeClass("loading");
                        bootbox.dialog({
                            message: response['msg'],
                            title: "Exito!",
                            buttons: {
                                danger: {
                                    label: "Continuar",
                                    className: "btn-uven",
                                    callback: function(){
                                       window.open(response['ruta'], '_parent');
                                    }
                                },
                            }
                        });
                            $('.close').click(function(){
                                 window.open(response['ruta'], '_parent');
                             });
                    } else {
                        $("#body").removeClass("loading");
                        bootbox.dialog({
                            message: response['msg'],
                            title: "Información!",
                            buttons: {
                                danger: {
                                    label: "Cerrar",
                                    className: "btn-uven",
                                },
                            }
                        });
                        
                        $("#subir").removeAttr('disabled');
                    }

                }
            });
    });
</script>