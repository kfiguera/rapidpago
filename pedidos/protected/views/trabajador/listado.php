<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Trabajador</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Trabajador</a></li>
            <li class="active">Listado</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Criterio de Busqueda</h3>
    </div>
    <div class="panel-body" >
        <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'p_personas', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            $connection = Yii::app()->db;

        ?>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Cédula</label>
                    <input type="text" class="form-control" name="cedul" id="cedul" placeholder="Cédula">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Nombre</label>
                    <input type="text" class="form-control" name="nombr" id="nombr" placeholder="Nombre">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Apellido</label>
                    <input type="text" class="form-control" name="apell" id="apell" placeholder="Apellido">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Código</label>
                    <input type="text" class="form-control" name="codig" id="codig" placeholder="Código">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Tipo de Trabajador</label>
                    <?php
                        $conexion=yii::app()->db;
                        $sql="SELECT * FROM p_trabajador_tipo";
                        $result=$conexion->createCommand($sql)->queryAll();
                        $data=CHtml::listData($result,'ttipo_codig','ttipo_descr');
                        echo CHtml::dropDownList('ttrab', '',$data , array('class' => 'form-control', 'placeholder' => "Tipo de Unidad","prompt"=>'Seleccione')); ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Estatus del Trabajador</label>
                    <?php
                        $conexion=yii::app()->db;
                        $sql="SELECT * FROM p_trabajador_estatus";
                        $result=$conexion->createCommand($sql)->queryAll();
                        $data=CHtml::listData($result,'etrab_codig','etrab_descr');
                        echo CHtml::dropDownList('etrab', '',$data , array('class' => 'form-control', 'placeholder' => "Tipo de Unidad","prompt"=>'Seleccione')); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <button type="button" id="buscar" class="btn btn-block btn-info">Buscar</button>
            </div>
            <div class="col-sm-4">
                <button type="reset" class="btn btn-block btn-default">Limpiar</button>
            </div>
            <div class="col-sm-4">
                <a href="registrar" class="btn btn-block btn-info" >Nuevo</a>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div> 
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Listado</h3>
    </div>
    <div class="panel-body" >
        
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-p_persona'>
                <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                #
                            </th>
                            <th>
                                Código
                            </th>
                            <th>
                                Cédula
                            </th>
                            <th>
                                Nombre 
                            </th>
                            
                            <th>
                                Tipo de Trabajador
                            </th>
                            <th>
                                Estatus
                            </th>
                            <th width="5%">
                                Consultar
                            </th>
                            <th width="5%">
                                Modificar
                            </th>
                            <th width="5%">
                                Eliminar
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "SELECT a.traba_codig, a.traba_ctrab, b.perso_cedul, b.perso_pnomb, b.perso_papel, c.ttipo_descr, d.etrab_descr 
                                FROM p_trabajador a 
                                JOIN p_persona b ON (a.perso_codig=b.perso_codig) 
                                JOIN p_trabajador_tipo c ON (a.ttipo_codig=c.ttipo_codig)
                                JOIN p_trabajador_estatus d ON (a.etrab_codig=d.etrab_codig)";
                        
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                        ?>
                        <tr>
                            <td class="tabla"><?php echo $i ?></td>
                            <td class="tabla"><?php echo $row['traba_codig'] ?></td>
                            <td class="tabla"><?php echo $row['perso_cedul'] ?></td>
                            <td class="tabla"><?php echo $row['perso_pnomb'].' '.$row['perso_papel'] ?></td>
                            <td class="tabla"><?php echo $row['ttipo_descr'] ?></td>
                            <td class="tabla"><?php echo $row['etrab_descr'] ?></td>
                            <td class="tabla"><a href="consultar?c=<?php echo $row['traba_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-search"></i></a></td>
                            <td class="tabla"><a href="modificar?c=<?php echo $row['traba_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-pencil"></i></a></td>
                            <td class="tabla"><a href="eliminar?c=<?php echo $row['traba_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-close"></i></a></td>
                        </tr>
                        <?php
                            }   
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>  
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#buscar').click(function () {
        var formData = new FormData($("#p_personas")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-p_persona').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>
