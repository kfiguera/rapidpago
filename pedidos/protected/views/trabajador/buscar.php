<table id='auditoria' class="table table-bordered table-hover dataTable">
    <thead>
        <tr>
            <th width="2%">
                #
            </th>
            <th>
                Código
            </th>
            <th>
                Cédula
            </th>
            <th>
                Nombre 
            </th>
            
            <th>
                Tipo de Trabajador
            </th>
            <th>
                Estatus
            </th>
            <th width="5%">
                Consultar
            </th>
            <th width="5%">
                Modificar
            </th>
            <th width="5%">
                Eliminar
            </th>
        </tr>
    </thead>

    <tbody>
        <?php
        $sql = "SELECT a.traba_codig, a.traba_ctrab, b.perso_cedul, b.perso_pnomb, b.perso_papel, c.ttipo_descr, d.etrab_descr 
                FROM p_trabajador a 
                JOIN p_persona b ON (a.perso_codig=b.perso_codig) 
                JOIN p_trabajador_tipo c ON (a.ttipo_codig=c.ttipo_codig)
                JOIN p_trabajador_estatus d ON (a.etrab_codig=d.etrab_codig) 
                ".$condicion;
        //echo $sql;
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $p_persona = $command->query();
        $i=0;
        while (($row = $p_persona->read()) !== false) {
            $i++;
        ?>
        <tr>
            <td class="tabla"><?php echo $i ?></td>
            <td class="tabla"><?php echo $row['traba_codig'] ?></td>
            <td class="tabla"><?php echo $row['perso_cedul'] ?></td>
            <td class="tabla"><?php echo $row['perso_pnomb'].' '.$row['perso_papel'] ?></td>
            <td class="tabla"><?php echo $row['ttipo_descr'] ?></td>
            <td class="tabla"><?php echo $row['etrab_descr'] ?></td>
            <td class="tabla"><a href="consultar?c=<?php echo $row['traba_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-search"></i></a></td>
            <td class="tabla"><a href="modificar?c=<?php echo $row['traba_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-pencil"></i></a></td>
            <td class="tabla"><a href="eliminar?c=<?php echo $row['traba_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-close"></i></a></td>
        </tr>
        <?php
            }   
        ?>
    </tbody>
</table>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable(); 
    });
</script>
