<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Trabajador</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Trabajador</a></li>
            <li class="active">Modificar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Modificar Trabajdor</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                 <div class="row hide">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Código</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php echo CHtml::hiddenField('codig', $roles['traba_codig'], array('class' => 'form-control', 'placeholder' => "Descripción")); ?>

                            </div>
                        </div>
                    </div>
                </div>
                 <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Persona</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <?php 
                                $conexion=yii::app()->db;
                                $sql="SELECT perso_codig, perso_pnomb, perso_papel FROM p_persona";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $i=0;
                                foreach ($result as $key => $value) {
                                    $perso[$i]['codigo']=$value['perso_codig']; 
                                    $perso[$i]['nombre']=$value['perso_pnomb'].' '.$value['perso_papel'];
                                    $i++;
                                }
                                $data=CHtml::listData($perso,'codigo','nombre');
                                echo CHtml::dropDownList('perso', $roles['perso_codig'],$data , array('class' => 'form-control', 'placeholder' => "Moneda","prompt"=>'Seleccione')); ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Tipo de Trabajador</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-list"></i></span>
                                <?php 
                                    $conexion=yii::app()->db;
                                    $sql="SELECT * FROM p_trabajador_tipo";
                                    $result=$conexion->createCommand($sql)->queryAll();
                                    $data=CHtml::listData($result,'ttipo_codig','ttipo_descr');
                                    echo CHtml::dropDownList('ttipo',  $roles['ttipo_codig'],$data , array('class' => 'form-control', 'placeholder' => "Moneda","prompt"=>'Seleccione')); ?>

                            </div>
                        </div>
                    </div> 
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Correo Electrónico</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <?php 

                                    echo CHtml::textField('corre',  $roles['traba_corre'], array('class' => 'form-control', 'placeholder' => "Correo Electrónico")); ?>

                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">   
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Teléfono</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                <?php 

                                    echo CHtml::textField('telef',  $roles['traba_telef'], array('class' => 'form-control', 'placeholder' => "Teléfono")); ?>

                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Estatus del Trabajador</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-list"></i></span>
                                <?php 
                                    $conexion=yii::app()->db;
                                    $sql="SELECT * FROM p_trabajador_estatus";
                                    $result=$conexion->createCommand($sql)->queryAll();
                                    $data=CHtml::listData($result,'etrab_codig','etrab_descr');
                                    echo CHtml::dropDownList('etrab',  $roles['etrab_codig'],$data , array('class' => 'form-control', 'placeholder' => "Tipo de Unidad","prompt"=>'Seleccione')); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Dirección</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-home"></i></span>
                                <?php 

                                    echo CHtml::textArea('direc',  $roles['traba_direc'], array('class' => 'form-control', 'placeholder' => "Dirección")); ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Observaciones</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <?php 

                                    echo CHtml::textArea('obser',  $roles['traba_obser'], array('class' => 'form-control', 'placeholder' => "Observaciones")); ?>

                            </div>
                        </div>
                    </div>
                    
                </div>
                
                
                    <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-4 ">
                            <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                        </div>
                    </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
<script>
        $('#salar').mask('#.##0,00',{reverse: true,maxlength:false});

    $('#punid').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#canti').mask('#.##0',{reverse: true,maxlength:false});
$('#desde').mask('00/00/0000');
$('#telef').mask('+5600000000009999999');

jQuery('#fnaci').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'

      });
    $(document).ready(function () {
       $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                etrab: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Estatus del Trabajador" es obligatorio',
                        }
                    }
                },perso: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Persona" es obligatorio',
                        }
                    }
                },
                ctrab: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Código de Trabajador" es obligatorio',
                        }
                    }
                },
                ttipo: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Trabajador" es obligatorio',
                        }
                    }
                },
                corre: {
                    validators: {
                        /*notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Correo Electrónico" es obligatorio',
                        },*/emailAddress: {
                            message: 'Estimado(a) Usuario(a) debe ingresar un correo electronico valido: ejemplo@smartwebtools.net'
                        }
                    }
                },
                telef: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Teléfono" es obligatorio',
                        }
                    }
                },
                banco: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Banco" es obligatorio',
                        }
                    }
                },
                tcuen: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Cuenta" es obligatorio',
                        }
                    }
                },
                ncuen: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Número de Cuenta" es obligatorio',
                        }
                    }
                },
                direc: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Direccion" es obligatorio',
                        }
                    }
                },
                comis: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Porcentaje de Comisión" es obligatorio',
                        }
                    }
                },
                /*obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Observaciones" es obligatorio',
                        }
                    }
                },*/



            }
        });
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'modificar',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>