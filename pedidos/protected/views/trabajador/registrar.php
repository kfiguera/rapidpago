<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Trabajador</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Trabajador</a></li>
            <li class="active">Registrar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Registrar Trabajador</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row hide">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Código de Trabajador</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                <?php echo CHtml::textField('ctrab', '0', array('class' => 'form-control', 'placeholder' => "Código")); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>RUT</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <?php echo CHtml::textField('cedula', '', array('class' => 'form-control', 'placeholder' => "RUT")); ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Primer Nombre</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <?php echo CHtml::textField('pnomb', '', array('class' => 'form-control', 'placeholder' => "Primer Nombre")); ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Segundo Nombre</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <?php echo CHtml::textField('snomb', '', array('class' => 'form-control', 'placeholder' => "Segundo Nombre")); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    
                    
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Primer Apellido</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <?php echo CHtml::textField('papel', '', array('class' => 'form-control', 'placeholder' => "Primer Apellido")); ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Segundo Apellido</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <?php echo CHtml::textField('sapel', '', array('class' => 'form-control', 'placeholder' => "Segundo Apellido")); ?>

                            </div>
                        </div>
                    </div>
                
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Fecha de Nacimiento</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                <?php echo CHtml::textField('fnaci', '', array('class' => 'form-control', 'placeholder' => "Fecha de Nacimiento")); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Genero</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                <select class="form-control" name="gener" id="gener">
                                    <option value=''>Seleccione</option>
                                    <?php
                                        $sql="SELECT * FROM p_genero";
                                        $p_genero=$conexion->createCommand($sql)->query();
                                        while (($gener = $p_genero->read()) !== false) {
                                            echo "<option value='".$gener['gener_value']."'>".$gener['gener_descr']."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                                        
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Teléfono</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                <?php 

                                    echo CHtml::textField('telef', '', array('class' => 'form-control', 'placeholder' => "Teléfono")); ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Tipo de Trabajador</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-list"></i></span>
                                <?php 
                                    $conexion=yii::app()->db;
                                    $sql="SELECT * FROM p_trabajador_tipo";
                                    $result=$conexion->createCommand($sql)->queryAll();
                                    $data=CHtml::listData($result,'ttipo_codig','ttipo_descr');
                                    echo CHtml::dropDownList('ttipo', '',$data , array('class' => 'form-control', 'placeholder' => "Moneda","prompt"=>'Seleccione')); 
                                ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Correo Electrónico</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <?php 

                                    echo CHtml::textField('corre', '', array('class' => 'form-control', 'placeholder' => "Correo Electrónico")); ?>

                            </div>
                        </div>
                    </div>
                </div>
                
                
                <div class="row hide">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Estatus del Trabajador</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                <?php 

                                    echo CHtml::hiddenField('etrab', '1', array('class' => 'form-control', 'placeholder' => "Estatus del Trabajador")); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Dirección</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-home"></i></span>
                                <?php 

                                    echo CHtml::textArea('direc', '', array('class' => 'form-control', 'placeholder' => "Dirección")); ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Observaciones</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <?php 

                                    echo CHtml::textArea('obser', '', array('class' => 'form-control', 'placeholder' => "Observaciones")); ?>

                            </div>
                        </div>
                    </div>
                    
                </div>
                
                <!-- Button -->
                <div class="row controls">
                    <div class="col-sm-4 ">
                        <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                    </div>
                    <div class="col-sm-4 ">
                        <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                    </div>
                    <div class="col-sm-4 ">
                        <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                    </div>
                </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
<script>
$('#punid').mask('#.##0,00',{reverse: true,maxlength:false});
$('#salar').mask('#.##0,00',{reverse: true,maxlength:false});
$('#canti').mask('#.##0',{reverse: true,maxlength:false});
$('#ncuen').mask('00000000000000000000');
$('#telef').mask('+5600000000009999999');
$('#cedula').mask('#.##0',{reverse: true,maxlength:false});
jQuery('#fnaci').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'

      });
        $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {

                etrab: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Nacionalidad" es obligatorio',
                        }
                    }
                },
                cedula: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Cedula" es obligatorio',
                        }

                    }
                },
                pnomb: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Primer Nombre" es obligatorio',
                        }
                    }
                },
                /*snomb: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Segundo Nombre" es obligatorio',
                        }
                    }
                },*/
                papel: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Primer Apellido" es obligatorio',
                        }
                    }
                },
                /*sapel: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Segundo Apellido" es obligatorio',
                        }
                    }
                },*/
                fnaci: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Fecha de Nacimiento" es obligatorio',
                        },
                        date: {
                            message: 'La Fecha de Nacimiento debe poseer el formato: DD/MM/YYYY',
                            format: 'DD/MM/YYYY',
                        }
                    }
                },
                gener: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Genero" es obligatorio',
                        }
                    }
                },
                desde: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Residente desde" es obligatorio',
                        }
                    }
                },
                numer: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Casa N°" es obligatorio',
                        }
                    }
                },
                ecivi: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Estado Civil" es obligatorio',
                        }
                    }
                },
                ctrab: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Código de Trabajador" es obligatorio',
                        }
                    }
                },
                ttipo: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Trabajador" es obligatorio',
                        }
                    }
                },
                corre: {
                    validators: {
                        /*notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Correo Electrónico" es obligatorio',
                        },*/emailAddress: {
                            message: 'Estimado(a) Usuario(a) debe ingresar un correo electronico valido: ejemplo@smartwebtools.net'
                        }
                    }
                },
                telef: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Teléfono" es obligatorio',
                        }
                    }
                },
                banco: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Banco" es obligatorio',
                        }
                    }
                },
                tcuen: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Cuenta" es obligatorio',
                        }
                    }
                },
                ncuen: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Número de Cuenta" es obligatorio',
                        }
                    }
                },
                direc: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Direccion" es obligatorio',
                        }
                    }
                },
                comis: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Porcentaje de Comisión" es obligatorio',
                        }
                    }
                },
                /*obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Observaciones" es obligatorio',
                        }
                    }
                },*/



            }
        });
    });

    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'registrar',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>