<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Consigurar Asignación</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Seguridad</a></li>
            <li><a href="#">Consigurar Asignación</a></li>
            <li class="active">Consultar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Consultar Consigurar Asignación</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                  <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Rol *</label>
                            <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT srole_codig, srole_descr
                                      FROM seguridad_roles 
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'srole_codig','srole_descr');
                                echo CHtml::dropDownList('usuar', $roles['srole_codig'], $data, array('class' => 'form-control', 'placeholder' => "Paises",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Modulo *</label>
                            <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT modul_codig, modul_descr
                                      FROM seguridad_modulos 
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'modul_codig','modul_descr');
                                echo CHtml::dropDownList('modul', $roles['modul_codig'], $data, array('class' => 'form-control', 'placeholder' => "Paises",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Acceso *</label>
                            <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT spacc_codig, spacc_descr
                                      FROM seguridad_permisos_acceso 
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'spacc_codig','spacc_descr');
                                echo CHtml::dropDownList('spacc', $roles['spacc_codig'], $data, array('class' => 'form-control', 'placeholder' => "Paises",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                        </div>
                    </div>
                </div>  
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Nivel *</label>
                            <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT spniv_codig, spniv_descr
                                      FROM seguridad_permisos_nivel
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'spniv_codig','spniv_descr');
                                echo CHtml::dropDownList('spniv', $roles['spniv_codig'], $data, array('class' => 'form-control', 'placeholder' => "Paises",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                        </div>
                    </div>
                    <div class="col-sm-8">        
                        <div class="form-group">
                            <label>Permisos *</label>
                            <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT spniv_codig, spniv_descr
                                      FROM seguridad_permisos_nivel
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=array('0' => 'Consultar','1' => 'Ingresar','2' => 'Modificar','3' => 'Eliminar' );
                                $permisos = array(0 => $roles['sprol_consu'], 1 => $roles['sprol_ingre'], 2=> $roles['sprol_modif'], 3 => $roles['sprol_elimi'], );
                                $i=0;
                                foreach ($permisos as $key => $value) {
                                    if ($value=='1') {
                                        $permi[$i]=$key;
                                    }
                                    $i++;
                                }
                                echo CHtml::dropDownList('permi', $permi, $data, array('class' => 'select2 select2-multiple', 'placeholder' => "Permisos",'prompt'=>'Seleccione...','multiple'=>'multiple','disabled'=>'true')); ?>
                        </div>
                    </div>
                </div> 
                
               
                
                    <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-12">
                            <a href="listado" type="reset" class="btn-block btn btn-info">Volver </a>
                        </div>
                    </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
