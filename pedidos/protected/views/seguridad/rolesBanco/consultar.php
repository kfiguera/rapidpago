<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Permisos Roles</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Parametros</a></li>
            <li><a href="#">Permisos Roles</a></li>
            <li class="active">Consultar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Consultar Permisos Roles</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                  <div class="row">
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Rol *</label>
                            <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT srole_codig, srole_descr
                                      FROM seguridad_roles 
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'srole_codig','srole_descr');
                                echo CHtml::dropDownList('usuar', $roles['srole_codig'], $data, array('class' => 'form-control', 'placeholder' => "Paises",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Modulo *</label>
                            <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT banco_codig, banco_descr
                                      FROM p_banco 
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'banco_codig','banco_descr');
                                echo CHtml::dropDownList('modul', $roles['banco_codig'], $data, array('class' => 'form-control', 'placeholder' => "Paises",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                        </div>
                    </div>
                   
                </div>  
                
                
               
                
                    <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-12">
                            <a href="listado" type="reset" class="btn-block btn btn-info">Volver </a>
                        </div>
                    </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
