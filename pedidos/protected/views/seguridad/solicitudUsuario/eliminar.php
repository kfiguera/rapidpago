<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Solicitudes por Usuario</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Seguridad</a></li>
            <li><a href="#">Solicitudes por Usuario</a></li>
            <li class="active">Eliminar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Eliminar Solicitudes por Usuario</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
               

            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
               <div class="row hide">
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>codigo</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <?php echo CHtml::hiddenField('codig', $roles['sprol_codig'], array('class' => 'form-control', 'placeholder' => "Correo")); ?>

                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Rol *</label>
                            <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT srole_codig, srole_descr
                                      FROM seguridad_roles 
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'srole_codig','srole_descr');
                                echo CHtml::dropDownList('usuar', $roles['srole_codig'], $data, array('class' => 'form-control', 'placeholder' => "Paises",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Modulo *</label>
                            <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT modul_codig, modul_descr
                                      FROM seguridad_modulos 
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'modul_codig','modul_descr');
                                echo CHtml::dropDownList('modul', $roles['modul_codig'], $data, array('class' => 'form-control', 'placeholder' => "Paises",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Acceso *</label>
                            <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT spacc_codig, spacc_descr
                                      FROM seguridad_permisos_acceso 
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'spacc_codig','spacc_descr');
                                echo CHtml::dropDownList('spacc', $roles['spacc_codig'], $data, array('class' => 'form-control', 'placeholder' => "Paises",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                        </div>
                    </div>
                </div>  
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Nivel *</label>
                            <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT spniv_codig, spniv_descr
                                      FROM seguridad_permisos_nivel
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'spniv_codig','spniv_descr');
                                echo CHtml::dropDownList('spniv', $roles['spniv_codig'], $data, array('class' => 'form-control', 'placeholder' => "Paises",'prompt'=>'Seleccione...','disabled'=>'true')); ?>
                        </div>
                    </div>
                    <div class="col-sm-8">        
                        <div class="form-group">
                            <label>Permisos *</label>
                            <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT spniv_codig, spniv_descr
                                      FROM seguridad_permisos_nivel
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=array('0' => 'Consultar','1' => 'Ingresar','2' => 'Modificar','3' => 'Eliminar' );
                                $permisos = array(0 => $roles['sprol_consu'], 1 => $roles['sprol_ingre'], 2=> $roles['sprol_modif'], 3 => $roles['sprol_elimi'], );
                                $i=0;
                                foreach ($permisos as $key => $value) {
                                    if ($value=='1') {
                                        $permi[$i]=$key;
                                    }
                                    $i++;
                                }
                                echo CHtml::dropDownList('permi', $permi, $data, array('class' => 'select2 select2-multiple', 'placeholder' => "Permisos",'prompt'=>'Seleccione...','multiple'=>'multiple','disabled'=>'true')); ?>
                        </div>
                    </div>
                </div> 
                    <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-6 ">
                            <button id="guardar" type="button" class="btn-block btn btn-info">Eliminar  </button>
                        </div>
                        <div class="col-sm-6">
                            <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                        </div>
                    </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
<script>
$('#fnaci').mask('00/00/0000');
$('#desde').mask('00/00/0000');
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                corre: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Rol(a) el campo "Correo" es obligatorio',
                        },
                        regexp: {
                            regexp: /^[A-Za-z0-9._%+-]+\@[a-zA-Z0-9.-]+\.[A-Za-z]{2,4}$/,
                            message: 'Estimado(a) Rol(a) el campo "Correo" debe poseer el siguiente formato: ejemplo@gmail.com'
                        }, identical: {
                            field: 'ccorre',
                            message: 'Estimado(a) Rol(a) el campo "Correo" y su confirmacion no son iguales'
                        }
                    }
                },ccorre: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Rol(a) el campo "Confirmar Correo" es obligatorio',
                        },
                        regexp: {
                            regexp: /^[A-Za-z0-9._%+-]+\@[a-zA-Z0-9.-]+\.[A-Za-z]{2,4}$/,
                            message: 'Estimado(a) Rol(a) el campo "Confirmar Correo"  debe poseer el siguiente formato: ejemplo@gmail.com'
                        }, identical: {
                            field: 'corre',
                            message: 'Estimado(a) Rol(a) el campo "Correo" y su confirmacion no son iguales'
                        }
                    }
                },contra: {
                    validators: {
                        /*notEmpty: {
                            message: 'Estimado(a) Rol(a) el campo "Contraseña" es obligatorio',
                        },*/
                        identical: {
                            field: 'ccontra',
                            message: 'Estimado(a) Rol(a) el campo "Contraseña" y su confirmacion no son iguales'
                        }
                    }
                },ccontra: {
                    validators: {
                        /*notEmpty: {
                            message: 'Estimado(a) Rol(a) el campo "Confirmar Contraseña" es obligatorio',
                        },*/
                        identical: {
                            field: 'contra',
                            message: 'Estimado(a) Rol(a) el campo "Contraseña" y su confirmacion no son iguales'
                        }
                    }
                },perso: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Rol(a) el campo "Persona" es obligatorio',
                        }
                    }
                },
                nivel: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Rol(a) el campo "Nivel" es obligatorio',
                        }
                    }
                },
                /*obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Rol(a) el campo "Observaciones" es obligatorio',
                        }
                    }
                },*/



            }
        });
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'eliminar',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>