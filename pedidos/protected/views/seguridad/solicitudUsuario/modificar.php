<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Solicitudes por Usuario</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Seguridad</a></li>
            <li><a href="#">Solicitudes por Usuario</a></li>
            <li class="active">Modificar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Modificar Solicitudes por Usuario</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row hide">
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>codigo</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <?php echo CHtml::hiddenField('codig', $roles['sprol_codig'], array('class' => 'form-control', 'placeholder' => "Correo")); ?>

                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Rol *</label>
                            <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT srole_codig, srole_descr
                                      FROM seguridad_roles 
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'srole_codig','srole_descr');
                                echo CHtml::dropDownList('usuar', $roles['srole_codig'], $data, array('class' => 'form-control select2', 'placeholder' => "Paises",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Modulo *</label>
                            <?php 
                                $conexion = Yii::app()->db;
                                
                                $sql="SELECT b.* 
                                      FROM seguridad_modulos b
                                      WHERE b.modul_codig NOT IN (
                                        SELECT a.modul_codig 
                                        FROM seguridad_permisos_roles a 
                                        WHERE a.srole_codig='".$roles['srole_codig']."'
                                      ) 
                                      UNION
                                      SELECT b.* 
                                      FROM seguridad_modulos b
                                      WHERE b.modul_codig ='".$roles['modul_codig']."'
                                      ORDER by 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'modul_codig','modul_descr');
                                echo CHtml::dropDownList('modul', $roles['modul_codig'], $data, array('class' => 'form-control select2', 'placeholder' => "Paises",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Acceso *</label>
                            <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT spacc_codig, spacc_descr
                                      FROM seguridad_permisos_acceso 
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'spacc_codig','spacc_descr');
                                echo CHtml::dropDownList('spacc', $roles['spacc_codig'], $data, array('class' => 'form-control', 'placeholder' => "Paises",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                </div>  
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Nivel *</label>
                            <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT spniv_codig, spniv_descr
                                      FROM seguridad_permisos_nivel
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'spniv_codig','spniv_descr');
                                echo CHtml::dropDownList('spniv', $roles['spniv_codig'], $data, array('class' => 'form-control', 'placeholder' => "Paises",'prompt'=>'Seleccione...')); ?>
                        </div>
                    </div>
                    <div class="col-sm-8">        
                        <div class="form-group">
                            <label>Permisos *</label>
                            <?php 
                                $conexion = Yii::app()->db;
                                $sql="SELECT spniv_codig, spniv_descr
                                      FROM seguridad_permisos_nivel
                                      ORDER BY 1";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=array('0' => 'Consultar','1' => 'Ingresar','2' => 'Modificar','3' => 'Eliminar' );
                                $permisos = array(0 => $roles['sprol_consu'], 1 => $roles['sprol_ingre'], 2=> $roles['sprol_modif'], 3 => $roles['sprol_elimi'], );
                                $i=0;
                                foreach ($permisos as $key => $value) {
                                    if ($value=='1') {
                                        $permi[$i]=$key;
                                    }
                                    $i++;
                                }
                                echo CHtml::dropDownList('permi', $permi, $data, array('class' => 'select2 select2-multiple', 'placeholder' => "Permisos",'prompt'=>'Seleccione...','multiple'=>'multiple')); ?>
                        </div>
                    </div>
                </div> 
                
                
                
                    <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-4 ">
                            <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                        </div>
                    </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
<script>
    $(document).ready(function () {
        $('#fnaci').mask('00/00/0000');
        $('#desde').mask('00/00/0000');
        $('#cambi').mask('#.##0,00',{reverse: true,maxlength:false}); 
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                
                descr: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Rol(a) el campo "Descripción" es obligatorio',
                        }
                    }
                },padre: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Rol(a) el campo "Padre" es obligatorio',
                        }
                    }
                }
                ,nivel: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Rol(a) el campo "Nivel" es obligatorio',
                        }
                    }
                },


            }
        });
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'modificar',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>

<script type="text/javascript">
    $('#usuar').change(function () {
        $.ajax({
            'type':'POST',
            'data':{'usuar':this.value},
            'url':'<?php echo CController::createUrl('funciones/seguridadModulos'); ?>',
            'cache':false,
            'success':function(html){
                jQuery("#modul").html(html)
            }
        });
    });
</script>