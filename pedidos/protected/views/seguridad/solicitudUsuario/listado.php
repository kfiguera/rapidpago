<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Solicitudes por Usuario</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Seguridad</a></li>
            <li><a href="#">Solicitudes por Usuario</a></li>
            <li class="active">Listado</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php $connection= yii::app()->db; ?>
<div class="row hide">
    <div class="col-sm-3">
        <a href="asignacion" class="btn btn-rounded btn-info btn-block btn-border">
            <i class="fa fa-plus-circle fa-2x"></i>
            <br>Asignar Solicitudes
        </a>

    </div>
    <div class="col-sm-6">
        <a href="reasignacion" class="btn btn-info btn-block">
            Reasignar Solicitudes
        </a>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Listado</h3>
    </div>
    <div class="panel-body" >
        
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-p_persona'>
                <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                #
                            </th>
                            <th>
                                Solicitud
                            </th>
                            <th>
                                Razon Social
                            </th>
                            <th>
                                VIP
                            </th>
                            <th>
                                Usuario Asignado
                            </th>

                            <th width="5%">
                                &nbsp;
                            </th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        $sql = "SELECT *
                                FROM solicitud a 
                                JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                                JOIN cliente c ON (a.clien_codig = c.clien_codig)
                                JOIN seguridad_usuarios d ON (a.usuar_codig = d.usuar_codig)
                                JOIN p_persona e ON (d.perso_codig = e.perso_codig)
                                WHERE a.estat_codig >= 6 
                                AND a.estat_codig not in (27,31,32,33)
                                ORDER BY solic_clvip, prere_codig";
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                            $vip = array('1' => 'SI', '2' => 'NO');
                            $organ=$this->funciones->organigrama();
                            $sql = "SELECT *
                                FROM solicitud_usuario a
                                JOIN seguridad_usuarios b ON (a.usuar_codig =b.usuar_codig and b.organ_codig in (".$organ."))
                                JOIN p_persona c ON (b.perso_codig = c.perso_codig)
                                WHERE solic_codig = '".$row['solic_codig']."'
                                ";
                            $asignado = $connection->createCommand($sql)->queryRow();

                        ?>
                        <tr>
                            <td class="tabla"><?php echo $i ?></td>
                            <td class="tabla"><?php echo $row['solic_numer'] ?></td>
                            <td class="tabla"><?php echo $row['clien_rsoci'] ?></td>
                            <td class="tabla"><?php echo $vip[$row['solic_clvip']] ?></td>
                            
                            <td class="tabla"><?php echo $asignado['perso_pnomb'].' '.$asignado['perso_papel'] ?></td>
                            
                            <td class="tabla"><a href="consultar?c=<?php echo $row['solic_codig'] ?>" class="btn btn-block btn-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Consultar" ><i class="fa fa-search"></i></a></td>

                        </tr>
                        <?php
                            }   
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
    <div class="panel-footer">
        <div class="row">
            <div class="col-sm-6">
                <a href="asignacion" class="btn btn-info btn-block">
                    Asignar Solicitudes
                </a>

            </div>
            <div class="col-sm-6">
                <a href="reasignacion" class="btn btn-info btn-block">
                    Reasignar Solicitudes
                </a>
            </div>
        </div>
    </div>
</div>  
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#buscar').click(function () {
        var formData = new FormData($("#p_personas")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-p_persona').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>
