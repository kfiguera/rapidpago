<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Solicitudes por Usuario</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Seguridad</a></li>
            <li><a href="#">Solicitudes por Usuario</a></li>
            <li class="active">Asignar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php
    $connection = Yii::app()->db;
?>
<form id='login-form' name='login-form' method="post">

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Aprobar Solicitud</h3>
    </div>
    <div class="panel-body" >
        
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-p_persona'>
                <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                <input type="checkbox" name="select_all" value="1" id="example-select-all">
                            </th>
                            <th>
                                Nro Solicitud
                            </th>
                            <th>
                                Razón Social
                            </th>
                            
                            <th>
                                Ciente VIP
                            </th>
                            
                            <th width="5px">
                                Estatus
                            </th>
                            <th width="5%">
                                &nbsp;
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $organ=$this->funciones->organigrama();
                        $sql = "SELECT *
                                FROM solicitud a 
                                JOIN rd_preregistro_estatus b ON (a.estat_codig = b.estat_codig)
                                JOIN cliente c ON (a.clien_codig = c.clien_codig)
                                JOIN seguridad_usuarios d ON (a.usuar_codig = d.usuar_codig)
                                JOIN p_persona e ON (d.perso_codig = e.perso_codig)
                                WHERE a.estat_codig >= 6 
                                AND a.estat_codig not in (27,31,32,33)
                                AND a.solic_codig not in (SELECT solic_codig
                                FROM solicitud_usuario f
                                JOIN seguridad_usuarios g ON (f.usuar_codig =g.usuar_codig and g.organ_codig in (".$organ."))
                                WHERE f.solic_codig = a.solic_codig
                                GROUP BY solic_codig)
                                ORDER BY solic_clvip, prere_codig";

                        
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;
                        while (($row = $p_persona->read()) !== false) {
                            
                            $j++;
                            $sql="SELECT * 
                                  FROM solicitud_trayectoria 
                                  WHERE solic_codig = '".$row['solic_codig']."'
                                    AND estat_codig = '".$row['estat_codig']."'";

                            $trayectoria = $connection->createCommand($sql)->queryRow();

                            $inicio=$trayectoria['traye_finic'].' '.$trayectoria['traye_hinic'];
                            $fin=date('Y-m-d H:i:s');
                            $diff=$this->funciones->diferenciaHoras($inicio,$fin);
                            $horas=($diff->days * 24 )  + ( $diff->h );
                            $semaforo=$this->funciones->semaforo($connection,$row['estat_codig'],$horas);
                            
                            $sql = "SELECT sum(cequi_canti) cantidad
                                FROM solicitud_equipos a 
                                WHERE solic_codig='".$row['solic_codig'] ."'";
                            $cequipo = $connection->createCommand($sql)->queryRow();
                            
                            $sql = "SELECT *
                                FROM p_banco 
                                WHERE banco_codig='".$row['banco_codig'] ."'";
                            $banco = $connection->createCommand($sql)->queryRow();

                            $sql = "SELECT *
                                FROM solicitud_terminal 
                                WHERE solic_codig='".$row['solic_codig'] ."'";
                            $terminal = $connection->createCommand($sql)->queryRow();
                            if ($terminal) {
                                $termi='SI';
                            }else{
                                $termi='NO';
                            }
                            $vip=array('1'=>'SI', '2'=>'NO');
                            
                        ?>
                        <tr>
                            

                            <td><input type="checkbox" name="id[<?php echo $i ?>]" value="<?php echo $row['solic_codig'] ?>" id="id_<?php echo $i ?>"></td>
                            <td><?php echo $row['solic_numer'] ?></td>
                            <td><?php echo $row['clien_rsoci'] ?></td>
                            <td class="tabla"><?php echo $vip[$row['solic_clvip']] ?></td>
                            <td><?php echo $semaforo.' '.$row['estat_descr'] ?></td>

                            <!--td><a href="../detalle/listado?p=<?php echo $row['prere_codig'] ?>" class="btn btn-block btn-info"><i class="glyphicon glyphicon-list-alt"></i></a></td-->
                            <td>
                                <a href="consultar?c=<?php echo $row['solic_codig'] ?>" class="btn btn-block btn-info"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Consultar" >
                                    <i class="fa fa-search"></i>
                                </a>
                            </td>
                        </tr>
                        <?php
                        $i++;
                            }   
                        ?>
                    </tbody>
                </table>
            </div>
            
        </div>
        
    </div>
    <hr>
    <div class="panel-body" >    
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Trabajador</label>
                    <?php
                        $organ = $this->funciones->organigrama();
                        $sql = "SELECT a.usuar_codig, CONCAT(b.perso_pnomb,' ',b.perso_papel) nombre
                                FROM seguridad_usuarios a
                                JOIN p_persona b ON (a.perso_codig = b.perso_codig)
                                AND a.pesta_codig = '1'
                                WHERE a.organ_codig in (".$organ.")";
                        $result = $connection->createCommand($sql)->queryAll();
                        $data = CHtml::listData($result,'usuar_codig','nombre');
                        echo CHtml::dropDownList('traba','',$data,array('class' => 'form-control', 'placeholder' => "Actividad Comercial",'prompt'=>'Seleccione...'));
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <div class="row">
            <div class="col-sm-4 ">
                <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
            </div>
            <div class="col-sm-4 ">
                <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
            </div>
            <div class="col-sm-4 ">
                <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
            </div>
        </div>    
    </div>
</div>
</form>  
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function (){
   var table = $('#auditoria').DataTable({
      
      'columnDefs': [{
         'targets': 0,
         'searchable': false,
         'orderable': false,
         'className': 'dt-body-center',
         
      }],
      'order': [[1, 'asc']]
   });

   // Handle click on "Select all" control
   $('#example-select-all').on('click', function(){
      // Get all rows with search applied
      var rows = table.rows({ 'search': 'applied' }).nodes();
      // Check/uncheck checkboxes for all rows in the table
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
   });

   // Handle click on checkbox to set state of "Select all" control
   $('#auditoria tbody').on('change', 'input[type="checkbox"]', function(){
      // If checkbox is not checked
      if(!this.checked){
         var el = $('#example-select-all').get(0);
         // If "Select all" control is checked and has 'indeterminate' property
         if(el && el.checked && ('indeterminate' in el)){
            // Set visual state of "Select all" control
            // as 'indeterminate'
            el.indeterminate = true;
         }
      }
   });

   // Handle form submission event
   $('#frm-example').on('submit', function(e){
      var form = this;

      // Iterate over all checkboxes in the table
      table.$('input[type="checkbox"]').each(function(){
         // If checkbox doesn't exist in DOM
         if(!$.contains(document, this)){
            // If checkbox is checked
            if(this.checked){
               // Create a hidden element
               $(form).append(
                  $('<input>')
                     .attr('type', 'hidden')
                     .attr('name', this.name)
                     .val(this.value)
               );
            }
         }
      });
   });

});
</script>
<script>
    $(document).ready(function () {
       
        $('#buscar').click(function () {
        var formData = new FormData($("#p_personas")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-p_persona').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>
<script>
    $(document).ready(function () {
        $('#cantidad').mask('#.##0',{reverse: true,maxlength:false});
        $('#punidad').mask('#.##0,00',{reverse: true,maxlength:false});
    });
</script>

<script>
// Date Picker
    $(document).ready(function () {
        $('#desde').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd/mm/yyyy'
        });
        $('#hasta').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd/mm/yyyy'
        });
    }); 
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                traba: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Trabajador" es obligatorio',
                        }
                    }
                },



            }
        });
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'asignacion',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>