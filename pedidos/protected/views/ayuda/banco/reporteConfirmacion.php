<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Ayuda</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Ayuda</a></li>
            <li><a href="#">Reportes</a></li>
            <li><a href="#">Carga de Parametros Confirmadas por el Banco</a></li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

pvent_
 <div class="panel panel-default">
                     <div class="panel-heading">Ayuda</div>

                  <div class="panel-wrapper collapse in">
                     <div class="panel-body">
                        <h3 class="box-title">Carga de Parametros Confirmadas por el Banco</h3>
                        <p>
                           Opción del Sistema que permie imprimie reprte con las solicitudes cuya carga de parámetros fue cargada por el banco, tal cmo se muestra enla siguiente imagén:</p>
                        <img src='/files/ayuda/banco/r_confirmacion.png' class='img-responsive center-block'>
                        <p>
                            Se debera tildar el boron "Imprimir", automaticmente se genera el reporte con todas las solicitudes, como se muestra en la siguiente imagen: 
                        </p>
                       <img src='/files/ayuda/banco/pdf_confirmacion.png' class='img-responsive center-block'>
                     </div>
                  </div>
                  <div class="panel-footer">
                    <div class="row">
                      <div class="col-xs-6"><a href="reporteCertificado" class="btn btn-info">Solicitudes Certificadas por el Banco</a></div>
                      <div class="col-xs-6"><a href="cambiarContrasena" class="btn btn-success" style="float:right;">Cambio de Contraseña</a></div>
                    </div>
                 </div>
