<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Ayuda</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Ayuda</a></li>
            <li><a href="#">Banco</a></li>
            <li><a href="#">Certificación de Banco</a></li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

 <div class="panel panel-default">
                     <div class="panel-heading">Ayuda</div>

                  <div class="panel-wrapper collapse in">
                     <div class="panel-body">
                        <h3 class="box-title">Certificación de Banco</h3>
                        <p>
                           Opción del Sistema en l cual se muestra un listado de todas las solicitudes provenientes del área de tramitación que se hayan generado al banco y aun este en espera de la certificacón del mismo, dichas solicitudes tendran un estatus de "En Espera de Certificación", tal cmo se muestra enla siguiente imagén:</p>
                        <img src='/files/ayuda/banco/certificacion.png' class='img-responsive center-block'>
                        <p>
                            Dentro de esta Pantalla se pueden realizar las siguientes acciones: 
                        </p>
                       <h3>
                           Consultar
                       </h3>
                       <p>
                           Opción que permite Consultar los datos de la solicitud seleccionada, esta opción como su nombre lo indica es de solo consulta, no se pueden modificar los datos.
                       </p>
                  <img src='/files/ayuda/banco/consulta_datos_sol.png' class='img-responsive center-block'>

                       <h3>
                           Verificar
                       </h3>
                       <p>
                           Opción que permite procesar (Aprovar) o rechazar la certificación de las solicitudes pendientes, como se muestra en la siguiente imagen:
                       </p>
                  <img src='/files/ayuda/banco/verificar_sol.png' class='img-responsive center-block'>
                  <p>
                      En la Opción <strong>"Número de Terminal/Acción"</strong>, se selecciona si se aprueba o no l solicitud, si la opción seleccionada el "Rechazar" se debera colocar el motivo del rechazo.
                  </p>
                  <p>
                      Una vez seleccionada la opción, tildar el botón <strong>"Continuar"</strong>, par procesar la solicitud.
                  </p>
                     </div>
                  </div>
                  <div class="panel-footer">
                    <div class="row">
                      <div class="col-xs-6"><a href="inicio" class="btn btn-info";">Acceso al Sistema</a></div>
                      <div class="col-xs-6"><a href="cargaParametros" class="btn btn-success" style="float:right;">Carga de Parámetros</a></div>
                    </div>
                 </div>
