<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Ayuda</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Ayuda</a></li>
            <li><a href="#">Banco</a></li>
            <li><a href="#">Carga de Parámetros</a></li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>


 <div class="panel panel-default">
                     <div class="panel-heading">Ayuda</div>

                  <div class="panel-wrapper collapse in">
                     <div class="panel-body">
                        <h3 class="box-title">Carga de Parámetros</h3>
                        <p>
                           Esta opción permite realizar la certificación bancaria de la asignación de dispositivos para un determinado cliente. Al seleccionar esta opción se visualiza pantalla que muestra el listado de solicitudes pendientes por cargar parámetros, como se muestra en la imagen:
                           </p>
                        <img src='/files/ayuda/banco/carga_parametro.png' class='img-responsive center-block'>
                        <p>
                            Dentro de esta Pantalla se pueden realizar las siguientes acciones: 
                        </p>
                       <h3>
                           Consultar
                       </h3>
                       <p>
                           Opción que permite Consultar los datos de la solicitud seleccionada, esta opción como su nombre lo indica es de solo consulta, no se pueden modificar los datos.
                       </p>
                  <img src='/files/ayuda/banco/consulta_carga_par.png' class='img-responsive center-block'>

                       <h3>
                           Generar Archivo CSV
                       </h3>
                       <p>
                           Opción que lista todas las solicitudes cuyo estatus es “Equipo Asignado”, estas solicitudes se deberánseleccionar para generar el CSV que será enviado al banco para su posterior certificación. Una vezseleccionada esta opción se muestra la siguiente pantalla:
                       </p>
                  <img src='/files/ayuda/banco/generar_csv.png' class='img-responsive center-block'>
                  <p>
                      Una vez seleccionada las solicitudes se tilda el botón “Continuar” el sistema procesa, actualiza el estatus a “Archivo Generado” y genera de forma automática el archivo CSV, el cual presenta la siguiente estructura:
                  </p>
                  <img src='/files/ayuda/banco/csv.png' class='img-responsive center-block'>

                  <h3>
                           Reversar Archivo CSV
                       </h3>
                       <p>
                           Esta opción permite reversar el archivo CSV previamente generados en el sistema. Al seleccionar esta opción se muestra el listado de archivos generados, como se muestra en la siguiente imagen:
                       </p>
                  <img src='/files/ayuda/banco/reverso_csv.png' class='img-responsive center-block'>

                  <h3>
                           Aprobar Solicitudes
                       </h3>
                       <p>
                           OEsta opción permite aprobar las solicitudes cuyo archivo CSV fue generado, certificado por el banco y asociado en número de terminal que el banco género. Al seleccionar esta opción se muestra la siguiente pantalla:
                       </p>
                  <img src='/files/ayuda/banco/aprobar_csv.png' class='img-responsive center-block'>
                  <p>
                      Una vez aprobada la asignación se actualiza el estatus a “Confirmado por el Banco” y pasa al siguiente proceso que es Solicitudes Pendientes por Verificar.
                  </p>

                     </div>
                  </div>
                    <div class="panel-footer">
                    <div class="row">
                      <div class="col-xs-6"><a href="certificacionBanco" class="btn btn-info">Certificación del Banco</a></div>
                      <div class="col-xs-6"><a href="reporteCertificado" class="btn btn-success" style="float:right;">Reporte/Solicitudes Certificadas por el Banco</a></div>
                    </div>
                 </div>
