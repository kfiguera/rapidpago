<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Ayuda</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Ayuda</a></li>
            <li><a href="#">Autogestión</a></li>
            <li><a href="#">Pre Registro</a></li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>


 <div class="panel panel-default">
                     <div class="panel-heading">Ayuda</div>

                  <div class="panel-wrapper collapse in">
                     <div class="panel-body">
                        <h3 class="box-title">Pre Registro</h3>
                        <p>
                           Opción que permite a los clientes ingresar en el sistema el pre-registro de sus solicitudes.</p>
                        <p>
                           Al seleccionar esta opción se visualiza la pantalla de para realizar consultas, donde  se deberá colocar una palabra clave en el campo “buscar” como por ejemplo; Número Solicitud y/o Estatus, automáticamente el sistema mostrará un listado de solicitudes que tenga registrada el cliente con esas características, como se muestra en la imagen:
                        </p>
                        <img src='/files/ayuda/autogestion/busqueda_pre.png' class='img-responsive center-block'>
                        <p>
                           En esta parte de la pantalla podrán Consultar y Modificar el registro.
                        </p>
                        <p>
                           Para un nuevo registro seleccionamos el botón “Nueva Solicitud”, automáticamente el sistema te muestra la siguiente imagen en la que deben llenar los campos requeridos, luego seleccionamos “Continuar” y automáticamente el sistema guarda el registro.
                        </p>
                        <img src='/files/ayuda/autogestion/datos_comercio.png' class='img-responsive center-block'>
                        <p>
                           Los campos que se deben llenar en el Formulario son los Siguientes:
                        </p>
                        <ol>
                           <li id="comercio"> Datos Del Comercio</li>
                           <ul>
                              <li><strong>RIF del Comercio *:</strong> indica el Registro Único de Información Fiscal (RIF) del cliente que realizó la solicitud.</li>
                              <li><strong>Razón Social*:</strong> indica denominación por la cual se conoce colectivamente a una empresa. Nombre del cliente.</li>
                              <li><strong>Nombre Fantasía o Comercial *:</strong> indica el nombre popular de la empresa.</li>
                              <li><strong>Actividad Comercial *:</strong> indica el proceso o la acción que lleva a cabo la institución o cliente a nivel comercial.</li>
                              <li><strong>Teléfono Móvil *:</strong> indica el número móvil de contacto del cliente.</li>
                              <li><strong>Teléfono Fijo:</strong> indica número de contacto fijo del cliente.</li>
                              <li><strong>Estado *:</strong> opción que permite seleccionar de una lista el Estado de ubicación del cliente o comercio.</li>
                              <li><strong>Municipio *:</strong> opción que permite seleccionar el municipio al que pertenece el cliente o comercio. Dicho municipio está atado al estado seleccionado anteriormente.</li>
                              <li><strong>Parroquia *:</strong> opción que permite seleccionar la parroquia asociada al estado y municipio del cliente o comercio.</li>
                              <li><strong>Ciudad *:</strong> opción que permite seleccionar la ciudad a la que pertenece el cliente o comercio.</li>
                              <li><strong>Zona Postal *:</strong> indica número asignado que se debe adjuntar a la dirección de domicilio del cliente o comercio.</li>
                              <li><strong>Correo Electrónico *:</strong> indica la dirección Email del cliente o comercio.</li>
                              <li><strong>Dirección Fiscal *:</strong> indica lugar de localización o domicilio del cliente o comercio.</li>
                           </ul>

                           <li>Datos de Afiliación</li>

                           <ul>
                             <li><strong>Banco Afiliado *:</strong> opción que permite seleccionar el banco correspondiente al cliente o comercio.</li> 
                             <li><strong>Código Afiliado *:</strong> indica el código (8 dígitos) de afiliación que el banco asocia al cliente o comercio.</li>
                             <li><strong>Número de Cuenta *:</strong> indica el número de cuenta del cliente o comercio. Este campo corresponde a 20 dígitos.</li>
                           </ul>
                           </ol>

                        <p>
                           Una vez se hayan completados los campos se tilda el botón siguiente, inmediatamente el sistema te envía al siguiente paso para continuar el registro.
                        </p>
                     </div>
                  </div>
                    <div class="panel-footer">
                    <div class="row">
                      <div class="col-xs-6"><a href="registroDocumentacion" class="btn btn-info">Registro y Documentación</a></div>
                      <div class="col-xs-6"><a href="datosAfiliacion" class="btn btn-success" style="float:right;">Datos de Afiliación y Dispositivos</a></div>
                    </div>
                 </div>
