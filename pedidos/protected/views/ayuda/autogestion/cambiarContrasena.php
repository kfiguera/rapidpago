<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Ayuda</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Ayuda</a></li>
            <li><a href="#">Banco</a></li>
            <li><a href="#">Cambiar Contraseña</a></li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>


 <div class="panel panel-default">
                     <div class="panel-heading">Ayuda</div>

                  <div class="panel-wrapper collapse in">
                     <div class="panel-body">
                        <h3 class="box-title">Cambiar Contraseña</h3>
                        <p>
                           A través de esta opción el usuario puede cambiar la clave de acceso cuando lo crea necesario. Esta Opción se encuentra en la pantalla de login del sistema y dentro de la pantalla principal, como se muestra en la siguiente imagen:</p>
                        <img src='/files/ayuda/banco/contrasena.png' class='img-responsive center-block'>
                        <p>
                            Al seleccionar esta opción se muestr la siguiente pantalla:
                        </p>
                        <img src='/files/ayuda/banco/cambio_cont.png' class='img-responsive center-block'>
                        <p>
                            Los Datos que debe ingresar son los siguientes:
                        </p>
                        <ul>
                            <li><strong>Contraseña:</strong> ingrese la contraseña o clave actual de acceso al sistema.</li>
                            <li><strong>Nueva Contraseña:</strong> ingrese la nueva contraseña de acceso al sistema.</li>
                            <li><strong>Confirmar Contraseña:</strong> repita la nueva contraseña.</li>
                        </ul>
                       
                     </div>
                  </div>
                  <div class="panel-footer">
                    <div class="row">
                      <div class="col-xs-12"><a href="documentosDigitales" class="btn btn-info">Documentos Digitales</a></div>
                      
                    </div>
                 </div>
