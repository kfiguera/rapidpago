<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Ayuda</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Ayuda</a></li>
            <li><a href="#">Banco</a></li>
            <li><a href="#">Login</a></li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

 <div class="panel panel-default">
                     <div class="panel-heading">Ayuda</div>

                  <div class="panel-wrapper collapse in">
                     <div class="panel-body">
                        <h3 class="box-title">Login</h3>
                        <p>
                           Para acceder al sistema el cliente deberá ingresar a la pantalla de Login, desde la ruta antes http://rapidpago.smartwebtools.net/site/login, en el proceso de registro.</p>
                        <img src='/files/ayuda/banco/login.png' class='img-responsive center-block'>
                        <p>
                            Los datos solicitados para el Ingreso son:
                        </p>
                        <ul>
                           <li><strong>Usuario:</strong> esta opción permite ingresar el usuario (correo electrónico) registrado en el sistema de Pedidos.</li>
                            <li><strong>Contraseña:</strong> opción que permite ingresar la contraseña o password de acceso al sistema de Pedidos, que le fue enviado por correo cuando se aprobó el registro.</li>
                            <li><strong>¿Olvidaste tu Contraseña?:</strong> ?: esta opción se selecciona en caso de que el usuario no recuerde la contraseña de acceso al sistema.</li>
                  </ul>
                  <p>
                     Una vez se ingresen los datos anteriores, seleccionamos la opción <strong>“Ingresar” </strong>, para acceder al sistema. </p>
                     </div>
                  </div>
                  <div class="panel-footer">
                    <div class="row">
                      <div class="col-xs-6"><a href="registroCliente" class="btn btn-info">Registro de Cliente</a></div>
                      <div class="col-xs-6"><a href="inicio" class="btn btn-success" style="float:right;">Inicio</a></div>
                    </div>
                 </div>
