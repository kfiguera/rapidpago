<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Ayuda</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Ayuda</a></li>
            <li><a href="#">Autogestión</a></li>
            <li><a href="#">Datos de los Representantes Legales</a></li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>


 <div class="panel panel-default">
                     <div class="panel-heading">Ayuda</div>

                  <div class="panel-wrapper collapse in">
                     <div class="panel-body">
                        <h3 class="box-title">Datos de los Representantes Legales</h3>
                        <p>
                           Opción que permite registrar los datos de los representantes legales de la Empresa o Cliente que esta realizando la solicitud.
                         </p>
                        <img src='/files/ayuda/autogestion/representante_legal.png' class='img-responsive center-block'>
                        <p>
                            Los campos que se deben llenar en el Formulario son los Siguientes: 
                        </p>
                       <ul>
                              <li><strong>RIF *:</strong> indica el Registro Único de Información Fiscal (RIF) del representante legal del cliente o comercio.</li>
                              <li><strong>Nombre *: </strong> indica en nombre completo del representante legal.</li>
                              <li><strong>Apellido *:  </strong> indica los apellidos del representante legal del cliente o comercio.</li>
                              <li><strong>Tipo de Documento *: </strong> indica el tipo de documento de identidad del representante legal a registrar; venezolano (V), Extranjero (E), Pasaporte (P). </li>
                              <li><strong>C.I/Pasaporte *: </strong> indica el número de cédula de identidad o pasaporte (según la opción seleccionada anteriormente) del representante legal.</li>
                              <li><strong>Cargo *:  </strong> indica el cargo (laboral) asignado al representante legal en la empresa o comercio.</li>
                              <li><strong>Teléfono Móvil *: </strong> indica el número móvil de contacto del representante legal.</li>
                              <li><strong>Teléfono Fijo *: </strong> indica número de contacto fijo del representante legal.</li>
                              <li><strong>Correo Electrónico *: </strong> indica la dirección Email del representante legal.</li>
                           <li><strong>Más *: </strong> Boton que permite agregar un registro adicional de otro representante.</li>
                           </ul>
                        <p>
                           Una vez se hayan completados los campos se tilda el botón siguiente, inmediatamente el sistema te envía al siguiente paso para continuar el registro.
                        </p>
                     </div>
                  </div>
                  <div class="panel-footer">
                    <div class="row">
                      <div class="col-xs-6"><a href="datosAfiliacion" class="btn btn-info">Datos de Afiliación  </a></div>
                      <div class="col-xs-6"><a href="datosAdicionales" class="btn btn-success" style="float:right;">Datos Adicionales</a></div>
                    </div>
                 </div>
