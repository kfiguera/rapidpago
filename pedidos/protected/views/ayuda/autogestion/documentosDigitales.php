<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Ayuda</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Ayuda</a></li>
            <li><a href="#">Autogestión</a></li>
            <li><a href="#">Carga de Documentos Digitales</a></li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>


 <div class="panel panel-default">
                     <div class="panel-heading">Ayuda</div>

                  <div class="panel-wrapper collapse in">
                     <div class="panel-body">
                        <h3 class="box-title">Carga de Documentos Digitales</h3>
                        <p>
                           Esta opción del registro de la solicitud permite cargar en el sistema los diferentes requisitos solicitados al comercio para poder procesar la solicitud. Estos documentos deben ser con extnsión JPG, PNG y PDF. Los documentos requeridos son:
                         </p>
                         <ul>
                              <li>Planilla de Solicitud</li>
                              <li>Documento Constitutivo.</li>
                              <li>Número de Cuenta.</li>
                              <li>RIF del Comercio.</li>
                              <li>Cédula/Pasaporte.</li>
                              <li>RIF de los Representantes Legales.</li>
                              <li>Contrato.</li>
                           </ul>
                        <img src='/files/ayuda/autogestion/documentos_digitales.png' class='img-responsive center-block'>
                           <p>
                           Una vez registrado el último paso, seleccionar el botón siguiente, automáticamente el sistema valida que todos los documentos estén cargados y registra la solicitud.
                        </p>
                        <p>
                           Finalizado el proceso de Pre-Registro por el cliente, el personal de Rapid pago se encargara de verificar, aprobar y/o Rechazar y procesar la solicitud.
                        </p>
                     </div>
                  </div>
                  <div class="panel-footer">
                    <div class="row">
                      <div class="col-xs-6"><a href="datosAdicionales" class="btn btn-info">Datos Adicionales</a></div>
                      <div class="col-xs-6"><a href="cambiarContrasena" class="btn btn-success" style="float:right;">Cambiar Contraseña</a></div>
                    </div>
                 </div>
