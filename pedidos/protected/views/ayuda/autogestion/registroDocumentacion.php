<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Ayuda</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Ayuda</a></li>
            <li><a href="#">Autogestión</a></li>
            <li><a href="#">Registro y Documentación</a></li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

 <div class="panel panel-default">
                     <div class="panel-heading">Ayuda</div>

                  <div class="panel-wrapper collapse in">
                     <div class="panel-body">
                        <h3 class="box-title">Registro y Documentación</h3>
                        <p>
                           Módulo cuya finalidad es permitir al cliente de RapidPago registrar sus diferentes solicitudes, registrar la documentación y a su vez llevar un control del estatus de su solicitud.</p>
                        <img src='/files/ayuda/autogestion/registro_documentacion.png' class='img-responsive center-block'>
                        </p>
                        <li><a href="preRegistro">Pre-Registro</a></li>
                           <ul>
                              <li><a href="preRegistro#comercio">Datos del Comercio</a></li>
                              <li><a href="datosAfiliacion">Datos de Afiliación y Dispositivos/Configuración Del Equipo</a></li>
                              <li><a href="representanteLegal">Datos De Los Representantes Legales</a></li>
                              <li><a href="datosAdicionales">Datos Adicionales</a></li>
                              <li><a href="documentosDigitales">Carga De Documentos Digitales</a></li>
                           </ul>
                        </ul>
                     </div>
                  </div>
                  <div class="panel-footer">
                    <div class="row">
                      <div class="col-xs-6"><a href="inicio" class="btn btn-info">Inicio</a></div>
                      <div class="col-xs-6"><a href="preRegistro" class="btn btn-success" style="float:right;">Pre-Registro</a></div>
                    </div>
                 </div>
