<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Ayuda</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Ayuda</a></li>
            <li><a href="#">Autogestión</a></li>
            <li><a href="#">Registro Cliente</a></li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>

 <div class="panel panel-default">
                     <div class="panel-heading">Ayuda</div>

                  <div class="panel-wrapper collapse in">
                     <div class="panel-body">
                        <h3 class="box-title">Registro del Cliente</h3>
                        <p>
                           Para tener acceso al Sistema de Información para la Gestión Operativa (SIGO), ingrese la siguiente ruta: <a href="http://autogestion.smartwebtools.net/site/login">http://autogestion.smartwebtools.net/site/login</a> en el navegador de internet de su preferencia, lw aparecera la pantalla de login, seleccione la opción <strong>"Nuevo Cliente"</strong>, automáticamente le aparecerá la siguiente ventana.</p>
                        <img src='/files/ayuda/autogestion/registro_cliente.png' class='img-responsive center-block'>
                        <p>
                            Los datos solicitados en el formulario son:
                        </p>
                        <ul>
                         <li><strong>Nacionalidad:</strong> permite seleccionar la nacionalidad del cliente, esta puede ser; Venezolano (V) o Extranjero (E).</li>
                         <li><strong>Cédula:</strong> opción que permite registrar el número de documento de identidad del cliente.</li>
                         <li><strong>Primer Nombre:</strong> indica el primer nombre del cliente, este campo es obligatorio.</li>
                         <li><strong>Segundo Nombre:</strong> indica el segundo nombre del cliente, si lo posee.</li>
                         <li><strong>Primer Apellido:</strong> indica el primer apellido del cliente, este campo es obligatorio.</li>
                         <li><strong>Segundo Apellido:</strong> indica el segundo apellido del cliente, si lo posee.</li>
                         <li><strong>Teléfono Celular:</strong> indica el número telefónico de contacto del cliente.</li>
                         <li><strong>Correo Electrónico:</strong> indica la dirección email del cliente.</li>
                         <li><strong>Confirmar Correo Electrónico:</strong> opción que permite ingresar nuevamente la dirección email, validando que sea igual que la anterior.</li>
                         <li><strong>RIF del Comercio:</strong> indica el Registro Único de Información Fiscal del cliente.</li>
                         <li><strong>Razón Social:</strong> indica el nombre o denominación  legal del cliente (empresa).</li>
                        </ul>
                  <p>
                     Una vez registrado, automáticamente llegara un correo electrónico indicando la Bienvenida a nuestro Sistema de Autogestión, como se muestra en la imagen:
                  </p>
                  <img src='/files/ayuda/autogestion/correo_registro.png' class='img-responsive center-block'>
                  <p>
                     Ahora el cliente deberá esperar que su solicitud de registro sea aprobada por el Área de Autogestión de RapidPago.</p>
                  <p>
                     Una vez aprobada la solicitud por el área de Autogestión, le llegar un correo electrónico al cliente, indicándole que su solicitud fue aprobada e indicando su usuario y clave de acceso.
                  </p>
                  <img src='/files/ayuda/autogestion/aprobacion_acceso.png' class='img-responsive center-block'>
                  <p>
                     Con estos datos ya el cliente puede ingresar al sistema y realizar el proceso de Pre-Registro.
                  </p>
                     </div>
                  </div>
                  <div class="panel-footer">
                    <div class="row">
                      <div class="col-xs-12"><a href="login" class="btn btn-success" style="float:right;">Login</a></div>

                    </div>
                 </div>
