<table  id='auditoria'  class="table table-bordered table-hover dataTable">
    <thead>
        <tr>
            <th width="2%">
                #
            </th>
            <th>
                Motivo
            </th>
            <th>
                Fecha
            </th>
            <th>
                Factura
            </th>
            <th>
                Referencia
            </th>
            <th>
                Monto
            </th>
            <th>
                Banco
            </th>
            <th width="5%">
                Consultar
            </th>
            <th width="5%">
                Modificar
            </th>
            <th width="5%">
                Eliminar
            </th>
        </tr>
    </thead>
    <tbody>
        <?php
        $sql = "SELECT * FROM egresos a
                JOIN p_banco b ON (a.banco_codig = b.banco_codig) ".$condicion;
        $connection= yii::app()->db;
        $command = $connection->createCommand($sql);
        $p_persona = $command->query();
        $i=0;
        while (($row = $p_persona->read()) !== false) {
            $i++;
        ?>
        <tr>
            <td class="tabla"><?php echo $i ?></td>
            <td class="tabla"><?php echo $row['egres_motiv'] ?></td>
            <td class="tabla"><?php echo $this->funciones->TransformarFecha_v($row['egres_fegre']) ?></td>
            <td class="tabla"><?php echo $this->funciones->TransformarMonto_v($row['egres_nfact'],0) ?></td>
            
            <td class="tabla"><?php echo $row['egres_refer'] ?></td>
            <td class="tabla"><?php echo $this->funciones->TransformarMonto_v($row['egres_monto'],2) ?></td>
            <td class="tabla"><?php echo $row['banco_descr'] ?></td>
            <td class="tabla"><a href="consultar?c=<?php echo $row['egres_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-search"></i></a></td>
            <td class="tabla"><a href="modificar?c=<?php echo $row['egres_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-pencil"></i></a></td>
            <td class="tabla"><a href="eliminar?c=<?php echo $row['egres_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-close"></i></a></td>
        </tr>
        <?php
            }   
        ?>
    </tbody>
</table>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable(); 
    });
</script>
