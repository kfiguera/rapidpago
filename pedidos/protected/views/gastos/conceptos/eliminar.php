<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Conceptos de la nomina</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Nomina</a></li>           
            <li><a href="#">Conceptos</a></li>
            <li class="active">Eliminar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">   
<form id='login-form' name='login-form' method="post">                 
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Eliminar Concepto</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            
                 <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Código</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                <?php echo CHtml::textField('codig', $conce['conce_codig'], array('class' => 'form-control', 'placeholder' => "Descripción","readonly"=>"readonly")); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Descripción</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-list"></i></span>
                                <?php echo CHtml::textField('descr', $conce['conce_descr'], array('class' => 'form-control', 'placeholder' => "Descripción",'disabled'=>'disabled')); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Valor</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-edit"></i></span>
                                <?php echo CHtml::textArea('valor', $conce['conce_value'], array('class' => 'form-control', 'placeholder' => "Valor",'disabled'=>'disabled')); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Tipo de Conceto</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-list"></i></span>
                                <?php 
                                $conexion=yii::app()->db;
                                $sql="SELECT tconc_codig, tconc_descr
                                      FROM nomina_concepto_tipo a ";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'tconc_codig','tconc_descr');
                                echo CHtml::dropDownList('tconc', $conce['tconc_codig'],$data , array('class' => 'form-control', 'placeholder' => "Tipo de Concepto",'disabled'=>'disabled')); ?>

                            </div>
                        </div>
                    </div>
                    
                    
                </div>
                <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-6 ">
                            <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                        </div>
                        <div class="col-sm-6 ">
                            <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                        </div>
                    </div>
            </div>
        </div>
    </form>
                
                
                
                
                
                    
                


        </div><!-- form -->
    </div>  
</div>
<script>
        $('#salar').mask('#.##0,00',{reverse: true,maxlength:false});

    $('#bonif_canti').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#canti').mask('#.##0',{reverse: true,maxlength:false});
$('#desde').mask('00/00/0000');

    $(document).ready(function () {
       $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                etrab: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Estatus del Trabajador" es obligatorio',
                        }
                    }
                },perso: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Persona" es obligatorio',
                        }
                    }
                },
                ctrab: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Código de Trabajador" es obligatorio',
                        }
                    }
                },
                ttipo: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Trabajador" es obligatorio',
                        }
                    }
                },
                corre: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Correo Electrónico" es obligatorio',
                        }
                    }
                },
                telef: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Teléfono" es obligatorio',
                        }
                    }
                },
                banco: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Banco" es obligatorio',
                        }
                    }
                },
                tcuen: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Cuenta" es obligatorio',
                        }
                    }
                },
                ncuen: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Número de Cuenta" es obligatorio',
                        }
                    }
                },
                direc: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Direccion" es obligatorio',
                        }
                    }
                },
                /*obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Observaciones" es obligatorio',
                        }
                    }
                },*/



            }
        });
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'eliminar',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>
<script type="text/javascript">
    $('#dferi').change(function () {
        var dferi = $(this).val();
        if(dferi=='1'){
            $('#dferi_canti').removeAttr('disabled');
        }else{
            $('#dferi_canti').prop('disabled', true);;
        }

    });
    $('#hextr').change(function () {
        var hextr = $(this).val();
        if(hextr=='1'){
            $('#hextr_canti').removeAttr('disabled');
        }else{
            $('#hextr_canti').prop('disabled', true);;
        }

    });
    $('#bonif').change(function () {
        var bonif = $(this).val();
        if(bonif=='1'){
            $('#bonif_canti').removeAttr('disabled');
        }else{
            $('#bonif_canti').prop('disabled', true);;
        }

    });
   $('#ntrab').change(function () {
        var ntrab = $(this).val();
        if(ntrab=='1'){
            $('#ntrab_canti').removeAttr('disabled');
        }else{
            $('#ntrab_canti').prop('disabled', true);;
        }

    });
</script>
