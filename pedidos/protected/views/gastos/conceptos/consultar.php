<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Conceptos de la nomina</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Nomina</a></li>           
            <li><a href="#">Conceptos</a></li>
            <li class="active">Consultar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">   
<form id='login-form' name='login-form' method="post">                 
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Consultar Concepto</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            
                 <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Código</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-hashtag"></i></span>
                                <?php echo CHtml::textField('codig', $conce['conce_codig'], array('class' => 'form-control', 'placeholder' => "Descripción","readonly"=>"readonly")); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Descripción</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-list"></i></span>
                                <?php echo CHtml::textField('descr', $conce['conce_descr'], array('class' => 'form-control', 'placeholder' => "Descripción",'disabled'=>'disabled')); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Valor</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-edit"></i></span>
                                <?php echo CHtml::textArea('valor', $conce['conce_value'], array('class' => 'form-control', 'placeholder' => "Valor",'disabled'=>'disabled')); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Tipo de Conceto</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-list"></i></span>
                                <?php 
                                $conexion=yii::app()->db;
                                $sql="SELECT tconc_codig, tconc_descr
                                      FROM nomina_concepto_tipo a ";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'tconc_codig','tconc_descr');
                                echo CHtml::dropDownList('tconc', $conce['tconc_codig'],$data , array('class' => 'form-control', 'placeholder' => "Tipo de Concepto",'disabled'=>'disabled')); ?>

                            </div>
                        </div>
                    </div>
                    
                    
                </div>
                <!-- Button -->
                <div class="row controls">
                    <div class="col-sm-12">
                        <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                    </div>
                </div>
            </div>
        </div>
    </form>
                
                
                
                
                
                    
                


        </div><!-- form -->
    </div>  
</div>