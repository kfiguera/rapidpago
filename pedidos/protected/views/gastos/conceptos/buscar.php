<table  id='auditoria'  class="table table-bordered table-hover dataTable">
    <thead>
        <tr>
            <th width="2%">
                #
            </th>
            <th>
                Descripción
            </th>
            <th>
                Tipo de Concepto
            </th>
            <th width="5%">
                Consultar
            </th>
            <th width="5%">
                Modificar
            </th>
            <th width="5%">
                Eliminar
            </th>
        </tr>
    </thead>
    <tbody>
        <?php
        $sql = "SELECT a.conce_codig, a.conce_descr, a.conce_value, b.tconc_descr
                FROM nomina_concepto a 
                JOIN nomina_concepto_tipo b ON (a.tconc_codig = b.tconc_codig)
                WHERE a.nomin_codig = '".Yii::app()->session['nomina']['codigo']."' ".$condicion;
        $connection=Yii::app()->db;
        $command = $connection->createCommand($sql);
        $p_persona = $command->query();
        $i=0;

        while (($row = $p_persona->read()) !== false) {
            $i++;
        ?>
        <tr>
            <td class="tabla"><?php echo $i ?></td>
            <td class="tabla"><?php echo $row['conce_descr'] ?></td>
            <td class="tabla"><?php echo $row['tconc_descr'] ?></td>
            <td class="tabla"><a href="consultar?c=<?php echo $row['conce_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-search"></i></a></td>
            <td class="tabla"><a href="modificar?c=<?php echo $row['conce_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-pencil"></i></a></td>
            <td class="tabla"><a href="eliminar?c=<?php echo $row['conce_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-close"></i></a></td>
        </tr>
        <?php
            }   
        ?>
    </tbody>
</table>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable(); 
    });
</script>
