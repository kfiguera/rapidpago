<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Asignación de Personal</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            
            <li><a href="#">Nomina</a></li>            
            <li><a href="#">Perido <?php echo Yii::app()->session['nomina']['periodo']['perio_ano']?></a></li>            
            <li>Asignación</li>      
            <li class="active">Modificar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">   
<form id='login-form' name='login-form' method="post">                 
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Datos Basicos</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            
                 <div class="row hide">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Código</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php echo CHtml::hiddenField('codig', $asign['asign_codig'], array('class' => 'form-control', 'placeholder' => "Descripción")); ?>

                            </div>
                        </div>
                    </div>
                </div>
                 <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Trabajador</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <?php 
                                $conexion=yii::app()->db;
                                $sql="SELECT traba_codig, perso_cedul, perso_pnomb, perso_papel 
                                      FROM p_trabajador a 
                                      JOIN p_persona b ON (a.perso_codig = b.perso_codig)
                                      WHERE a.traba_codig = '".$asign['traba_codig']."'";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $i=0;
                                foreach ($result as $key => $value) {
                                    $perso[$i]['codigo']=$value['traba_codig']; 
                                    $perso[$i]['nombre']=$this->funciones->transformarMonto_v($value['perso_cedul'],0).' - '.$value['perso_pnomb'].' '.$value['perso_papel'];
                                    $i++;
                                }
                                $data=CHtml::listData($perso,'codigo','nombre');
                                echo CHtml::dropDownList('traba', $asign['traba_codig'],$data , array('class' => 'form-control', 'placeholder' => "Moneda","readonly"=>'true')); ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Dias Laborados</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                <?php 

                                echo CHtml::textField('dtrab', $asign['asing_dtrab'], array('class' => 'form-control', 'placeholder' => "Dias Laborados")); ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Salario Base</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                <?php 
                                    $sql="SELECT * 
                                          FROM nomina_resumen  a
                                          WHERE nomin_codig= '".$asign["nomin_codig"]."' AND perio_codig = '".$asign["perio_codig"]."' AND traba_codig='".$asign["traba_codig"]."' AND conce_codig='1' ";
                                    $resum=$conexion->createCommand($sql)->queryRow();
                                    $salar=$this->funciones->transformarMonto_v($resum['resum_monto'],2);
                                    echo CHtml::textField('salar', $salar , array('class' => 'form-control', 'placeholder' => "Salario Base")); ?>

                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Asignaciones</h3>
        </div>
        <div class="panel-body" >  
            <div class="row"> 
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Dias Feriados</label>
                            <div class="row">
                                <div class="col-sm-4">
                                    <?php 
                                        $options=array();
                                        $options['class']='form-control';
                                        $options['placeholder']='Cantidad';
                                        if($asign["asign_dferi"]!='1'){
                                            $options['disabled']=true;
                                        }
                                        $data=array('1'=>'Si', '2'=>'No');
                                        echo CHtml::dropDownList('dferi', $asign["asign_dferi"],$data , array('class' => 'form-control', 'placeholder' => "Moneda")); 
                                    ?>
                                </div>
                                <div class="col-sm-8">
                                    <?php 
                                        
                                        echo CHtml::textField('dferi_canti', $asign["asign_cdfer"],$options); 
                                    ?>                                  
                                </div>
                            </div>
                        </div>
                    </div>   
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Horas Extras</label>
                            <div class="row">
                                <div class="col-sm-4">
                                    <?php 
                                        $options=array();
                                        $options['class']='form-control';
                                        $options['placeholder']='Cantidad';
                                        if($asign["asign_dferi"]!='1'){
                                            $options['disabled']=true;
                                        }
                                        $data=array('1'=>'Si', '2'=>'No');
                                        echo CHtml::dropDownList('hextr', $asign["asign_hexrt"],$data , array('class' => 'form-control', 'placeholder' => "Moneda")); 
                                    ?>
                                </div>
                                <div class="col-sm-8">
                                    <?php 
                                        echo CHtml::textField('hextr_canti', $asign["asign_chext"], $options); 
                                    ?>                                  
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Bonificaciones</label>
                            <div class="row">
                                <div class="col-sm-4">
                                    <?php
                                        $options=array();
                                        $options['class']='form-control';
                                        $options['placeholder']='Cantidad';
                                        if($asign["asign_bonif"]!='1'){
                                            $options['disabled']=true;
                                        } 
                                        $data=array('1'=>'Si', '2'=>'No');
                                        echo CHtml::dropDownList('bonif', $asign["asign_bonif"],$data , array('class' => 'form-control', 'placeholder' => "Moneda")); 
                                    ?>
                                </div>
                                <div class="col-sm-8">
                                    <?php 
                                    $sql="SELECT * 
                                          FROM nomina_resumen  a
                                          WHERE nomin_codig= '".$asign["nomin_codig"]."' AND perio_codig = '".$asign["perio_codig"]."' AND traba_codig='".$asign["traba_codig"]."' AND conce_codig='4' ";
                                    $resum=$conexion->createCommand($sql)->queryRow();
                                    $bonif=$this->funciones->transformarMonto_v($resum['resum_monto'],2);
                                        echo CHtml::textField('bonif_canti', $bonif, $options); 
                                    ?>                                  
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Bono Alimenticio</label>
                            <?php 
                                $data=array('1'=>'Si', '2'=>'No');
                                echo CHtml::dropDownList('balim', $asign["asign_balim"],$data , array('class' => 'form-control', 'placeholder' => "Moneda")); 
                            ?>
                                
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>  
        <div class="panel panel-default" >
            <div class="panel-heading" >
                <h3 class="panel-title">Deducciones</h3>
            </div>
            <div class="panel-body" >  
                <div class="row">    
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Dias no trabajados</label>
                            <div class="row">
                                <div class="col-sm-4">
                                    <?php 
                                        $options=array();
                                        $options['class']='form-control';
                                        $options['placeholder']='Cantidad';
                                        if($asign["asign_ntrab"]!='1'){
                                            $options['disabled']=true;
                                        }
                                        $data=array('1'=>'Si', '2'=>'No');
                                        echo CHtml::dropDownList('ntrab', $asign["asign_ntrab"] ,$data , array('class' => 'form-control', 'placeholder' => "Moneda")); 
                                    ?>
                                </div>
                                <div class="col-sm-8">
                                    <?php 
                                        echo CHtml::textField('ntrab_canti', $asign["asign_cntra"],$options); 
                                    ?>                                  
                                </div>
                            </div>
                        </div>
                    </div>   
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Seguro Social</label>
                            <div class="row">
                                <div class="col-sm-4">
                                    <?php 
                                        $data=array('1'=>'Si');
                                        echo CHtml::dropDownList('ssoci', '1',$data , array('class' => 'form-control', 'placeholder' => "Moneda", 'readonly' => 'true' )); 
                                    ?>
                                </div>
                                <div class="col-sm-8">
                                    <?php 
                                        echo CHtml::textField('ssoci_canti', '2', array('class' => 'form-control', 'placeholder' => "Cantidad", 'readonly' => 'true')); 
                                    ?>                                  
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>FAOV</label>
                            <div class="row">
                                <div class="col-sm-4">
                                    <?php 
                                        $data=array('1'=>'Si');
                                        echo CHtml::dropDownList('faov', '1',$data , array('class' => 'form-control', 'placeholder' => "Moneda", 'readonly' => 'true' )); 
                                    ?>
                                </div>
                                <div class="col-sm-8">
                                    <?php 
                                        echo CHtml::textField('faov_canti', '0,5', array('class' => 'form-control', 'placeholder' => "Cantidad", 'readonly' => 'true')); 
                                    ?>                                  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>INCE</label>
                            <div class="row">
                                <div class="col-sm-4">
                                    <?php 
                                        $data=array('2'=>'No');
                                        echo CHtml::dropDownList('ince', '2',$data , array('class' => 'form-control', 'placeholder' => "Moneda", 'readonly' => 'true' )); 
                                    ?>
                                </div>
                                <div class="col-sm-8">
                                    <?php 
                                        echo CHtml::textField('ince_canti', '0,5', array('class' => 'form-control', 'placeholder' => "Cantidad", 'readonly' => 'true')); 
                                    ?>                                  
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>SPF</label>
                            <div class="row">
                                <div class="col-sm-4">
                                    <?php 
                                        $data=array('2'=>'No');
                                        echo CHtml::dropDownList('spf', '2',$data , array('class' => 'form-control', 'placeholder' => "Moneda", 'readonly' => 'true' )); 
                                    ?>
                                </div>
                                <div class="col-sm-8">
                                    <?php 
                                        echo CHtml::textField('spf_canti', '0,5', array('class' => 'form-control', 'placeholder' => "Cantidad", 'readonly' => 'true')); 
                                    ?>                                  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default" >
            <div class="panel-heading" >
                <h3 class="panel-title">Observaciones</h3>
            </div>
            <div class="panel-body" >  
                <div class="row">
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <?php 

                                    echo CHtml::textArea('obser',  $asign['asign_obser'], array('class' => 'form-control', 'placeholder' => "Observaciones")); ?>

                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-4 ">
                            <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <button class="btn-block btn btn-default" onclick= "$(':input','#login-form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected')">Limpiar  </button>
                        </div>
                        <div class="col-sm-4 ">
                            <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                        </div>
                    </div>
            </div>
        </div>
    </form>
                
                
                
                
                
                    
                


        </div><!-- form -->
    </div>  
</div>
<script>
        $('#salar').mask('#.##0,00',{reverse: true,maxlength:false});

    $('#bonif_canti').mask('#.##0,00',{reverse: true,maxlength:false});
    $('#canti').mask('#.##0',{reverse: true,maxlength:false});
$('#desde').mask('00/00/0000');

    $(document).ready(function () {
       $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                etrab: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Estatus del Trabajador" es obligatorio',
                        }
                    }
                },perso: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Persona" es obligatorio',
                        }
                    }
                },
                ctrab: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Código de Trabajador" es obligatorio',
                        }
                    }
                },
                ttipo: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Trabajador" es obligatorio',
                        }
                    }
                },
                corre: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Correo Electrónico" es obligatorio',
                        }
                    }
                },
                telef: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Teléfono" es obligatorio',
                        }
                    }
                },
                banco: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Banco" es obligatorio',
                        }
                    }
                },
                tcuen: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Tipo de Cuenta" es obligatorio',
                        }
                    }
                },
                ncuen: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Número de Cuenta" es obligatorio',
                        }
                    }
                },
                direc: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Direccion" es obligatorio',
                        }
                    }
                },
                /*obser: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Observaciones" es obligatorio',
                        }
                    }
                },*/



            }
        });
    });
    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'modificar',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>
<script type="text/javascript">
    $('#dferi').change(function () {
        var dferi = $(this).val();
        if(dferi=='1'){
            $('#dferi_canti').removeAttr('disabled');
        }else{
            $('#dferi_canti').prop('disabled', true);;
        }

    });
    $('#hextr').change(function () {
        var hextr = $(this).val();
        if(hextr=='1'){
            $('#hextr_canti').removeAttr('disabled');
        }else{
            $('#hextr_canti').prop('disabled', true);;
        }

    });
    $('#bonif').change(function () {
        var bonif = $(this).val();
        if(bonif=='1'){
            $('#bonif_canti').removeAttr('disabled');
        }else{
            $('#bonif_canti').prop('disabled', true);;
        }

    });
   $('#ntrab').change(function () {
        var ntrab = $(this).val();
        if(ntrab=='1'){
            $('#ntrab_canti').removeAttr('disabled');
        }else{
            $('#ntrab_canti').prop('disabled', true);;
        }

    });
</script>
