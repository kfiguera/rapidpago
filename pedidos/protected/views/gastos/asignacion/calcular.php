<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Asignación de Personal</h4>

    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Nomina</a></li>            
            <li><a href="#">Perido <?php echo Yii::app()->session['nomina']['periodo']['perio_ano']?></a></li>            
            <li>Asignación</li>            
            <li class="active">Calcular</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Calcular Nomina</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row hide">
                    <div class="col-sm-12">        
                             <div class="form-group">
                            <label>Nomina</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <?php 
                                 
                                    echo CHtml::textField('nomin',Yii::app()->session['nomina']['codigo'] , array('class' => 'form-control', 'placeholder' => "Nomina",'prompt'=>'Seleccione')); ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">        
                        <div class="form-group">
                            <label>Periodo</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <?php 
                                    echo CHtml::textField('perio',Yii::app()->session['nomina']['periodo']['perio_codig'] , array('class' => 'form-control', 'placeholder' => "Periodo",'prompt'=>'Seleccione')); ?>

                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Fecha Inicio Periodo</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <?php 
                                    $finic=$this->funciones->transformarFecha_v(Yii::app()->session['nomina']['periodo']['perio_finic']);
                                    echo CHtml::textField('finic',$finic , array('class' => 'form-control', 'placeholder' => "Periodo",'prompt'=>'Seleccione', 'readonly'=>'true')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Fecha Fin Periodo</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <?php 
                                    $ffina=$this->funciones->transformarFecha_v(Yii::app()->session['nomina']['periodo']['perio_ffina']);
                                    echo CHtml::textField('ffina',$ffina , array('class' => 'form-control', 'placeholder' => "Periodo",'prompt'=>'Seleccione', 'readonly'=>'true')); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Cantidad de Trabajadores</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <?php 
                                    $sql="SELECT count(traba_codig) cantidad
                                        FROM nomina_asignacion c
                                        WHERE  c.nomin_codig = '".Yii::app()->session['nomina']['codigo']."'
                                        AND c.perio_codig = '".Yii::app()->session['nomina']['periodo']['perio_codig']."'";
                                    $canti=$conexion->createCommand($sql)->queryRow();
                                    echo CHtml::textField('canti',$canti['cantidad'], array('class' => 'form-control', 'placeholder' => "Cantidad",'prompt'=>'Seleccione', 'readonly'=>'true')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">        
                        <div class="form-group">
                            <label>Monto del Periodo</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                 <?php 
                                    $sql="SELECT perio_monto monto
                                        FROM nomina_periodo c
                                        WHERE  c.nomin_codig = '".Yii::app()->session['nomina']['codigo']."'
                                        AND c.perio_codig = '".Yii::app()->session['nomina']['periodo']['perio_codig']."'";
                                    $monto=$conexion->createCommand($sql)->queryRow();
                                    echo CHtml::textField('monto',$this->funciones->transformarMonto_v($monto['monto'],2) , array('class' => 'form-control', 'placeholder' => "Periodo",'prompt'=>'Seleccione', 'readonly'=>'true')); ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <!-- Button -->
                <div class="row controls">
                    <div class="col-sm-6 ">
                        <button id="guardar" type="button" class="btn-block btn btn-info">Continuar  </button>
                    </div>
                    <div class="col-sm-6 ">
                        <a href="listado" type="reset" class="btn-block btn btn-default">Volver </a>
                    </div>
                </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
<script>



        $(document).ready(function () {
        $('#login-form').formValidation({
            message: 'No es un valor valido',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            /*addOns: {
                reCaptcha2: {
                    element: 'captchaContainer',
                    theme: 'light',
                    //siteKey: '6LcXoxYTAAAAAOBeyOGeupmwPK8LFR6WjMZtPm7j',//sipred.tss.gob.ve
                    siteKey: '6LfBoBYTAAAAAD9KeUF4fC2pDfr1b-OMFzxttmSz',//192.168.1.168
                    timeout: 120,
                    message: 'The captcha is not valid'
                }
            },*/
            fields: {
                traba: {
                    validators: {
                        notEmpty: {
                            message: 'Estimado(a) Usuario(a) el campo "Trabajador" es obligatorio',
                        }
                    }
                }



            }
        });
    });

    $('#guardar').click(function () {
        $('#login-form').formValidation('resetForm');
            $('#login-form').formValidation('validate'); //secondary validation using Bootstrap Validator      
        var formValidation = $('#login-form').data('formValidation');
        if (formValidation.isValid()) {
            $.ajax({
                dataType: "json",
                data: $('#login-form').serialize(),
                url: 'calcular',
                type: 'post',
                beforeSend: function () {
                    $('#wrapper').block({
                        message: '<div class="cssload-speeding-wheel m-t-20"></div>'+
                                  '<h2>Cargando</h2><h3>Por favor, espere.</h3>'

                    });
                },
                success: function (response) {
                    $('#wrapper').unblock();
                    if (response['success'] == 'true') {
                        swal({ 
                            title: "Exito!",
                            text: response['msg'],
                            type: "success",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-info"
                        },function(){
                            window.open('listado', '_parent');
                        });
                    } else {
                        swal({ 
                            title: "Error!",
                            text: response['msg'],
                            type: "error",
                            confirmButtonText: "Cerrar",
                            confirmButtonClass: "btn-danger"
                        },function(){
                            $("#guardar").removeAttr('disabled');
                        });
                    }

                },error:function (response) {
                    $('#wrapper').unblock();
                    swal({ 
                        title: "Error!",
                        text: "Error el ejecutar la operación",
                        type: "error",
                        confirmButtonText: "Cerrar",
                        confirmButtonClass: "btn-danger"

                    },function(){
                        $("#guardar").removeAttr('disabled');
                    });
                        
                }
            });
        }
    });
</script>
<script>
// Date Picker
    
    jQuery('#fegre').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'

      });
      
</script>