<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Asignación de Personal</h4>

    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Nomina</a></li>            
            <li><a href="#">Perido <?php echo Yii::app()->session['nomina']['periodo']['perio_ano']?></a></li>            
            <li>Asignación</li>            
            <li class="active">Listado</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Criterio de Busqueda</h3>
    </div>
    <div class="panel-body" >
        <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'p_personas', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            $connection = Yii::app()->db;

        ?>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Año</label>
                    <?php 
                        for ($i=2018; $i <= date('Y'); $i++) { 
                            $data[$i]=$i;
                        }
                        echo CHtml::dropDownList('ano', '',$data, array('class' => 'form-control', 'prompt' => "Seleccione")); ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Mes</label>
                    <?php 
                        $sql="SELECT * FROM meses order by 1";
                        $result=$connection->createCommand($sql)->queryAll();
                        $data=CHtml::listData($result,'meses_codig','meses_descr');
                        echo CHtml::dropDownList('meses', '',$data, array('class' => 'form-control', 'prompt' => "Seleccione")); 
                    ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Estatus</label>
                    <?php 
                        $sql="SELECT * FROM nomina_periodo_estatus order by 1";
                        $result=$connection->createCommand($sql)->queryAll();
                        $data=CHtml::listData($result,'eperi_codig','eperi_descr');
                        echo CHtml::dropDownList('meses', '',$data, array('class' => 'form-control', 'prompt' => "Seleccione")); 
                    ?>
                </div>
            </div>
            
        </div>
        
        <div class="row">
            <div class="col-sm-4">
                <button type="button" id="buscar" class="btn btn-block btn-info">Buscar</button>
            </div>
            <div class="col-sm-4">
                <button type="reset" class="btn btn-block btn-default">Limpiar</button>
            </div>
            <div class="col-sm-4">
                <a href="../nomina/listado" class="btn btn-block btn-default" >Volver</a>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div> 
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Listado</h3>
    </div>
    <div class="panel-body" >
        
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-p_persona'>
                <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                #
                            </th>
                            <th>
                                Cédula 
                            </th>
                            <th>
                                Nombre
                            </th>
                            <th>
                                Total a Pagar
                            </th>
                            <th width="5%">
                                Consultar
                            </th>
                            <th width="5%">
                                Modificar
                            </th>
                            <th width="5%">
                                Recibo de Pago
                            </th>
                            <th width="5%">
                                Eliminar
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "SELECT a.asign_codig, c.perso_cedul, c.perso_pnomb, c.perso_papel, (SELECT SUM(resum_monto) FROM nomina_resumen e WHERE  a.traba_codig=e.traba_codig and a.perio_codig=d.perio_codig and  a.nomin_codig=d.nomin_codig ) monto
                                FROM nomina_asignacion a 
                                JOIN p_trabajador b ON (a.traba_codig=b.traba_codig)
                                JOIN p_persona c ON (b.perso_codig = c.perso_codig)
                                JOIN nomina_periodo d ON (a.perio_codig=d.perio_codig)
                                WHERE a.nomin_codig = '".Yii::app()->session['nomina']['codigo']."'
                                AND a.perio_codig = '".Yii::app()->session['nomina']['periodo']['perio_codig']."'";
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;

                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                        ?>
                        <tr>
                            <td class="tabla"><?php echo $i ?></td>
                            <td class="tabla"><?php echo $this->funciones->transformarMonto_v($row['perso_cedul'],0) ?></td>
                            <td class="tabla"><?php echo $row['perso_pnomb'].' '.$row['perso_papel'] ?></td>
                            <td class="tabla"><?php echo  $this->funciones->transformarMonto_v($row['monto'],2) ?></td>
                            <td class="tabla"><a href="consultar?c=<?php echo $row['asign_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-search "></i></a></td>
                            <td class="tabla"><a href="modificar?c=<?php echo $row['asign_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-pencil"></i></a></td>
                            <td class="tabla"><a href="<?php echo Yii::app()->request->getBaseUrl(true).'/reportes/nomina/recibopago?c='.$row['asign_codig']; ?>" class="btn btn-block btn-info"><i class="fa fa-pencil"></i></a></td>
                            <td class="tabla"><a href="eliminar?c=<?php echo $row['asign_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-close"></i></a></td>
                        </tr>
                        <?php
                            }   
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <a href="registrar" class="btn btn-block btn-info" >Agregar Trabajador</a>
            </div>
            <div class="col-sm-4">
                <a href="calcular" class="btn btn-block btn-info" >Calcular Nómina</a>
            </div>
            <div class="col-sm-4">
                <a href="reversar" class="btn btn-block btn-info" >Reversar Nómina</a>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-sm-4">
                <a href="<?php echo Yii::app()->request->getBaseUrl(true).'/gastos/conceptos/listado'?>" class="btn btn-block btn-info" >Conceptos</a>
            </div>
        </div>
    </div>
</div>  
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#buscar').click(function () {
        var formData = new FormData($("#p_personas")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-p_persona').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>
