<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Nomina</h4>

    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Nomina</a></li>            
            <li>Periodos</li>            
            <li class="active">Listado</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Criterio de Busqueda</h3>
    </div>
    <div class="panel-body" >
        <?php
            $form = $this->beginWidget('CActiveForm', array('id' => 'p_personas', 'htmlOptions' => array('method' => 'post', 'enctype' => 'multipart/form-data')));
            $connection = Yii::app()->db;

        ?>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Año</label>
                    <?php 
                        for ($i=2018; $i <= date('Y'); $i++) { 
                            $data[$i]=$i;
                        }
                        echo CHtml::dropDownList('ano', '',$data, array('class' => 'form-control', 'prompt' => "Seleccione")); ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Mes</label>
                    <?php 
                        $sql="SELECT * FROM meses order by 1";
                        $result=$connection->createCommand($sql)->queryAll();
                        $data=CHtml::listData($result,'meses_codig','meses_descr');
                        echo CHtml::dropDownList('meses', '',$data, array('class' => 'form-control', 'prompt' => "Seleccione")); 
                    ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Estatus</label>
                    <?php 
                        $sql="SELECT * FROM nomina_periodo_estatus order by 1";
                        $result=$connection->createCommand($sql)->queryAll();
                        $data=CHtml::listData($result,'eperi_codig','eperi_descr');
                        echo CHtml::dropDownList('meses', '',$data, array('class' => 'form-control', 'prompt' => "Seleccione")); 
                    ?>
                </div>
            </div>
            
        </div>
        
        <div class="row">
            <div class="col-sm-4">
                <button type="button" id="buscar" class="btn btn-block btn-info">Buscar</button>
            </div>
            <div class="col-sm-4">
                <button type="reset" class="btn btn-block btn-default">Limpiar</button>
            </div>
            <div class="col-sm-4">
                <a href="registrar" class="btn btn-block btn-info" >Nuevo</a>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div> 
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Listado</h3>
    </div>
    <div class="panel-body" >
        
        <div class="row">
            <div class="col-sm-12 table-responsive" id='listado-p_persona'>
                <table  id='auditoria'  class="table table-bordered table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="2%">
                                #
                            </th>
                            <th>
                                Año 
                            </th>
                            <th>
                                Mes
                            </th>
                            <th>
                                Trabajadores
                            </th>
                            <th>
                                Estatus
                            </th>
                            <th width="5%">
                                Continuar
                            </th>
                            <th width="5%">
                                Cerrar
                            </th>
                            <th width="5%">
                                Abrir
                            </th>
                            <th width="5%">
                                Eliminar
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "SELECT * , (SELECT count(*) FROM nomina_asignacion d WHERE a.nomin_codig = d.nomin_codig and a.perio_codig = d.perio_codig) cantidad
                                FROM nomina_periodo a 
                                JOIN meses b ON (a.meses_codig = b.meses_codig)
                                JOIN nomina_periodo_estatus c ON (a.eperi_codig = c.eperi_codig)";
                        $command = $connection->createCommand($sql);
                        $p_persona = $command->query();
                        $i=0;

                        while (($row = $p_persona->read()) !== false) {
                            $i++;
                        ?>
                        <tr>
                            <td class="tabla"><?php echo $i ?></td>
                            <td class="tabla"><?php echo $row['perio_ano'] ?></td>
                            <td class="tabla"><?php echo $row['meses_descr'] ?></td>
                            <td class="tabla"><?php echo $row['cantidad'] ?></td>
                            <td class="tabla"><?php echo $row['eperi_descr'] ?></td>
                            <td class="tabla"><a href="seleccionar?c=<?php echo $row['perio_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-sign-in "></i></a></td>
                            <td class="tabla"><a href="cerrar?c=<?php echo $row['perio_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-lock"></i></a></td>
                            <td class="tabla"><a href="abrir?c=<?php echo $row['perio_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-unlock"></i></a></td>
                            <td class="tabla"><a href="eliminar?c=<?php echo $row['perio_codig'] ?>" class="btn btn-block btn-info"><i class="fa fa-close"></i></a></td>
                        </tr>
                        <?php
                            }   
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row hide">
            <div class="col-sm-4">
                <a href="<?php echo Yii::app()->request->getBaseUrl(true).'/gastos/conceptos/listado'?>" class="btn btn-block btn-info" >Conceptos</a>
            </div>
        </div>
    </div>
</div>  
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button  id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Información</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">

                <button id='cerrar' type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#auditoria').DataTable();
        $('#buscar').click(function () {
        var formData = new FormData($("#p_personas")[0]);
            $.ajax({
                'data': formData,
                'url': 'buscar',
                'type': 'post',
                'cache': false,
                'contentType': false,
                'processData': false,
                success: function (html) {
                  $('#listado-p_persona').html(html);
                }
            });
        });
    });
    $("#modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-body").load(link.attr("href"));
    });
</script>
