<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Servicios</h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
            <li><a href="#">Servicios</a></li>
            <li class="active">Consultar</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">                    
    <div class="panel panel-default" >

        <div class="panel-heading" >
            <h3 class="panel-title">Consultar Tipo de Pago</h3>
        </div>
        <div class="panel-body" >
            <?php
                $conexion=Yii::app()->db;
            ?>
            <div style="display:none" id="login-alert" class="alert col-sm-12"></div>
            <form id='login-form' name='login-form' method="post">
                <div class="row">
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Descripción</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php 
                                echo CHtml::textField('descr', $roles['servi_descr'], array('class' => 'form-control', 'placeholder' => "Descripción", 'disabled'=>'true')); ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Precio</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                                <?php echo CHtml::textField('preci', $this->funciones->TransformarMonto_v($roles['servi_preci'],2), array('class' => 'form-control', 'placeholder' => "Descripción", 'disabled'=>'true')); ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">        
                        <div class="form-group">
                            <label>Moneda</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <?php 
                                $sql="SELECT * FROM p_moneda";
                                $result=$conexion->createCommand($sql)->queryAll();
                                $data=CHtml::listData($result,'moned_codig','moned_descr');

                                echo CHtml::dropDownList('moned', $roles['moned_codig'], $data, array('class' => 'form-control', 'placeholder' => "Descripción",'prompt'=>'Seleccione...', 'disabled'=>'true')); ?>

                            </div>
                        </div>
                    </div>
                </div>
                
               
                
                    <!-- Button -->
                    <div class="row controls">
                        <div class="col-sm-12">
                            <a href="listado" type="reset" class="btn-block btn btn-info">Volver </a>
                        </div>
                    </div>
                

            </form>

        </div><!-- form -->
    </div>  
</div>
