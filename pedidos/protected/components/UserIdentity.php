<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

    private $_id;
    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    public function authenticate() {



        
        $users = Usuarios::model()->findByAttributes(array('usuar_login' => $this->username));
        // here I use Email as user name which comes from database
        
        
        if ($users === null) {
            //$this->_id = 'user Null';
            //$this->errorCode = self::ERROR_USERNAME_INVALID;
          $err = "El usuario o contraseña no son validos";
          $this->errorCode = $err;
        } else if ($users->usuar_passw !== md5($this->password)) {            // here I compare db password with passwod field
            $this->_id = $this->username;
            $err = "El usuario o contraseña no son validos";

            $this->errorCode = $err;

        }else if ($users->uesta_codig == 2 ) {          // here I compare db password with passwod field
            $err = "Debe verificar su cuenta de correo";
            $this->errorCode = $err;
           
        }else if ($users->uesta_codig == 9 ) {          // here I compare db password with passwod field
            $err = "Usuario bloqueado contacte con el administrador";
            $this->errorCode = $err;
        }else if ($users->urole_codig != 1 and (date("w")==6 or date("w")==0 )) {
            $err = "Acceso disponible unicamente en horario laboral";
            $this->errorCode = $err; 
        }else{
            //$this->_id = $users['tusua_corre'];
            $init = new DateTime();
            $date = new DateTime();
            $inicio= $date->format('Y-m-d H:i:s');
            $finic= $date->format('Y-m-d');
            $hinic= $date->format('H:i:s');

            //$date->modify('+1 hours');
            $date->modify('+20 minutes');

            $vencimiento= $date->format('Y-m-d H:i:s');
            
            $fvenc= $date->format('Y-m-d');
            $hvenc= $date->format('H:i:s');
            
            $conexion=Yii::app()->db;
            
            $cadena=$users["usuar_codig"].$users["usuar_login"].$inicio;
            
            $hash=password_hash($cadena, PASSWORD_BCRYPT);
            
            $sql="SELECT * FROM seguridad_sesiones a
                  WHERE usuar_codig='".$users["usuar_codig"]."'
                    AND sesio_codig = (
                        SELECT max(sesio_codig)
                        FROM seguridad_sesiones b 
                        WHERE a.usuar_codig = b.usuar_codig)";
            $sesion=$conexion->createCommand($sql)->queryRow(); 

            if($sesion){
                if($sesion['pesta_codig']=='1'){

                    $desde=new DateTime($sesion['sesio_finic'].' '.$sesion['sesio_hinic']);
                    $hasta=new DateTime($sesion['sesio_fvenc'].' '.$sesion['sesio_hvenc']);
                    if($desde <= $init AND $init <= $hasta){
                        $err = "Ya posee una sesión activa";
                        $this->errorCode = $err;
                        
                        return $this->errorCode;

                        exit();
                    }
                }
            }
            /*$desde=new DateTime($sesion['sesio_finic'].' '.$sesion['sesio_hinic']);
            $hasta=new DateTime($sesion['sesio_fvenc'].' '.$sesion['sesio_hvenc']);
            if($desde <= $init AND $init <= $hasta){
                $err = "Ya posee una sesión activa";
                $this->errorCode = $err;
                return $this->errorCode;
                exit();
            }*/


            $sql="INSERT INTO seguridad_sesiones(sesio_numer, sesio_finic, sesio_hinic, sesio_fvenc, sesio_hvenc, pesta_codig, usuar_codig, sesio_fcrea, sesio_hcrea) 
              VALUES ('".$hash."','".$finic."','".$hinic."','".$fvenc."','".$hvenc."','1','".$users["usuar_codig"]."','".$finic."','".$hinic."')";
            $res=$conexion->createCommand($sql)->execute();

            if($res){
                $sql="SELECT * FROM seguridad_sesiones WHERE sesio_numer='".$hash."'";
                $sesion=$conexion->createCommand($sql)->queryRow();              
            }

            $perso = Persona::model()->findByAttributes(array('perso_codig' => $users["perso_codig"]));

            $datos['codigo']=$users["usuar_codig"];
            $datos['usuario']=$users["usuar_login"];
            $datos['estatus']=$users["uesta_codig"];
            $datos['permiso']=$users["urole_codig"];
            $datos['nombre']=$perso["perso_pnomb"].' '.$perso["perso_papel"];
            $datos['sesion']=$sesion["sesio_numer"];
            unset($users);
            unset($perso);
            unset($sesion);
            $this->_id['usuario'] = $datos;
            $this->setState('title', $datos['nombre']);
            $this->errorCode = self::ERROR_NONE;

        }
       // var_dump($this->errorCode);
        return $this->errorCode;



          /* $users=array(
          // username => password
          'demo'=>'demo',
          'admin'=>'admin',
          );
          if(!isset($users[$this->username]))
          $this->errorCode=self::ERROR_USERNAME_INVALID;
          elseif($users[$this->username]!==$this->password)
          $this->errorCode=self::ERROR_PASSWORD_INVALID;
          else
          $this->errorCode=self::ERROR_NONE;
          return !$this->errorCode; */
    }
    public function getId()
    {
        return $this->_id;
    }

}
