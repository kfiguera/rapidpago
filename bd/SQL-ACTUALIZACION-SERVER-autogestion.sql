-- MySQL Workbench Synchronization
-- Generated: 2019-12-12 19:48
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Kevin A. Figuera

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

CREATE TABLE IF NOT EXISTS `rapidpago_desarrollo_autogestion`.`p_codigo_area` (
  `carea_codig` INT(11) NOT NULL AUTO_INCREMENT,
  `carea_value` VARCHAR(20) NOT NULL,
  `carea_tipos` INT(11) NOT NULL,
  `usuar_codig` INT(11) NOT NULL,
  `carea_fcrea` DATE NOT NULL,
  `carea_hcrea` TIME NOT NULL,
  PRIMARY KEY (`carea_codig`),
  UNIQUE INDEX `carea_value` (`carea_value` ASC) ,
  INDEX `usuar_codig` (`usuar_codig` ASC) )
ENGINE = InnoDB
AUTO_INCREMENT = 77
DEFAULT CHARACTER SET = utf8mb4;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
