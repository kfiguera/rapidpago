-- MySQL Workbench Synchronization
-- Generated: 2019-12-12 17:50
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Kevin A. Figuera

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

CREATE TABLE IF NOT EXISTS `rapidpago`.`cliente_profit` (
  `cprof_codig` INT(11) NOT NULL AUTO_INCREMENT,
  `clien_codig` INT(11) NOT NULL,
  `pesta_codig` INT(11) NOT NULL,
  `usuar_codig` INT(11) NOT NULL,
  `cprof_fcrea` DATE NOT NULL,
  `cprof_hcrea` TIME NOT NULL,
  PRIMARY KEY (`cprof_codig`),
  INDEX `clien_codig` (`clien_codig` ASC),
  INDEX `usuar_codig` (`usuar_codig` ASC),
  INDEX `pesta_codig` (`pesta_codig` ASC),
  CONSTRAINT `cliente_profit_ibfk_1`
    FOREIGN KEY (`usuar_codig`)
    REFERENCES `rapidpago`.`seguridad_usuarios` (`usuar_codig`),
  CONSTRAINT `cliente_profit_ibfk_2`
    FOREIGN KEY (`clien_codig`)
    REFERENCES `rapidpago`.`cliente` (`clien_codig`),
  CONSTRAINT `cliente_profit_ibfk_3`
    FOREIGN KEY (`pesta_codig`)
    REFERENCES `rapidpago`.`p_parametros_estatus` (`pesta_codig`))
ENGINE = InnoDB
AUTO_INCREMENT = 15
DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE IF NOT EXISTS `rapidpago`.`cliente_vportal` (
  `cvpor_codig` INT(11) NOT NULL AUTO_INCREMENT,
  `clien_codig` INT(11) NOT NULL,
  `pesta_codig` INT(11) NOT NULL,
  `usuar_codig` INT(11) NOT NULL,
  `cvpor_fcrea` DATE NOT NULL,
  `cvpor_hcrea` TIME NOT NULL,
  PRIMARY KEY (`cvpor_codig`),
  INDEX `clien_codig` (`clien_codig` ASC),
  INDEX `pesta_codig` (`pesta_codig` ASC),
  INDEX `usuar_codig` (`usuar_codig` ASC),
  CONSTRAINT `cliente_vportal_ibfk_1`
    FOREIGN KEY (`clien_codig`)
    REFERENCES `rapidpago`.`cliente` (`clien_codig`),
  CONSTRAINT `cliente_vportal_ibfk_2`
    FOREIGN KEY (`pesta_codig`)
    REFERENCES `rapidpago`.`p_parametros_estatus` (`pesta_codig`),
  CONSTRAINT `cliente_vportal_ibfk_3`
    FOREIGN KEY (`usuar_codig`)
    REFERENCES `rapidpago`.`seguridad_usuarios` (`usuar_codig`))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE IF NOT EXISTS `rapidpago`.`config_asignacion` (
  `casig_codig` INT(11) NOT NULL AUTO_INCREMENT,
  `srole_codig` INT(11) NOT NULL,
  `orige_codig` INT(11) NOT NULL,
  `solic_clvip` INT(11) NOT NULL,
  `usuar_codig` INT(11) NOT NULL,
  `casig_fcrea` INT(11) NOT NULL,
  `casig_hcrea` INT(11) NOT NULL,
  PRIMARY KEY (`casig_codig`),
  INDEX `srole_codig` (`srole_codig` ASC),
  INDEX `orige_codig` (`orige_codig` ASC),
  INDEX `usuar_codig` (`usuar_codig` ASC),
  CONSTRAINT `config_asignacion_ibfk_1`
    FOREIGN KEY (`orige_codig`)
    REFERENCES `rapidpago`.`p_solicitud_origen` (`orige_codig`),
  CONSTRAINT `config_asignacion_ibfk_2`
    FOREIGN KEY (`srole_codig`)
    REFERENCES `rapidpago`.`seguridad_roles` (`srole_codig`),
  CONSTRAINT `config_asignacion_ibfk_3`
    FOREIGN KEY (`usuar_codig`)
    REFERENCES `rapidpago`.`seguridad_usuarios` (`usuar_codig`))
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE IF NOT EXISTS `rapidpago`.`p_codigo_area` (
  `carea_codig` INT(11) NOT NULL AUTO_INCREMENT,
  `carea_value` VARCHAR(20) NOT NULL,
  `carea_tipos` INT(11) NOT NULL,
  `usuar_codig` INT(11) NOT NULL,
  `carea_fcrea` DATE NOT NULL,
  `carea_hcrea` TIME NOT NULL,
  PRIMARY KEY (`carea_codig`),
  UNIQUE INDEX `carea_value` (`carea_value` ASC),
  INDEX `usuar_codig` (`usuar_codig` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 77
DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE IF NOT EXISTS `rapidpago`.`p_notificacion_estatus` (
  `enoti_codig` INT(11) NOT NULL AUTO_INCREMENT,
  `enoti_titul` VARCHAR(200) NOT NULL,
  `enoti_descr` TEXT NOT NULL,
  `estat_codig` INT(11) NOT NULL,
  `pesta_codig` INT(11) NOT NULL,
  `usuar_codig` INT(11) NOT NULL,
  `enoti_fcrea` DATE NOT NULL,
  `enoti_hcrea` TIME NOT NULL,
  PRIMARY KEY (`enoti_codig`),
  INDEX `estat_codig` (`estat_codig` ASC),
  INDEX `pesta_codig` (`pesta_codig` ASC),
  INDEX `usuar_codig` (`usuar_codig` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE IF NOT EXISTS `rapidpago`.`p_precio_venta` (
  `pvent_codig` INT(11) NOT NULL AUTO_INCREMENT,
  `pvent_value` DOUBLE NOT NULL,
  `inven_codig` INT(11) NOT NULL,
  `pesta_codig` INT(11) NOT NULL,
  `moned_codig` INT(11) NOT NULL,
  `usuar_codig` INT(11) NOT NULL,
  `pvent_fcrea` DATE NOT NULL,
  `pvent_hcrea` TIME NOT NULL,
  PRIMARY KEY (`pvent_codig`),
  INDEX `inven_codig` (`inven_codig` ASC),
  INDEX `pesta_codig` (`pesta_codig` ASC),
  INDEX `moned_codig` (`moned_codig` ASC),
  INDEX `usuar_codig` (`usuar_codig` ASC),
  CONSTRAINT `p_precio_venta_ibfk_1`
    FOREIGN KEY (`inven_codig`)
    REFERENCES `rapidpago`.`inventario` (`inven_codig`),
  CONSTRAINT `p_precio_venta_ibfk_2`
    FOREIGN KEY (`moned_codig`)
    REFERENCES `rapidpago`.`p_moneda` (`moned_codig`),
  CONSTRAINT `p_precio_venta_ibfk_3`
    FOREIGN KEY (`pesta_codig`)
    REFERENCES `rapidpago`.`p_parametros_estatus` (`pesta_codig`),
  CONSTRAINT `p_precio_venta_ibfk_4`
    FOREIGN KEY (`usuar_codig`)
    REFERENCES `rapidpago`.`seguridad_usuarios` (`usuar_codig`))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8mb4;

ALTER TABLE `rapidpago`.`rd_preregistro_configuracion_equipo` 
ADD COLUMN `inven_codig` INT(11) NOT NULL AFTER `prere_codig`,
ADD INDEX `inven_codig` (`inven_codig` ASC);
;

CREATE TABLE IF NOT EXISTS `rapidpago`.`seguridad_roles_banco` (
  `rbanc_codig` INT(11) NOT NULL AUTO_INCREMENT,
  `srole_codig` INT(11) NOT NULL,
  `banco_codig` INT(11) NOT NULL,
  `usuar_codig` INT(11) NOT NULL,
  `rbanc_fcrea` DATE NOT NULL,
  `rbanc_hcrea` TIME NOT NULL,
  PRIMARY KEY (`rbanc_codig`),
  INDEX `srole_codig` (`srole_codig` ASC),
  INDEX `banco_codig` (`banco_codig` ASC),
  INDEX `usuar_codig` (`usuar_codig` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8mb4;

ALTER TABLE `rapidpago`.`seguridad_usuarios` 
ADD COLUMN `spniv_codig` INT(11) NOT NULL AFTER `uesta_codig`,
ADD COLUMN `organ_codig` INT(11) NOT NULL AFTER `spniv_codig`,
ADD COLUMN `pesta_codig` INT(11) NOT NULL AFTER `organ_codig`,
ADD INDEX `spniv_codig` (`spniv_codig` ASC),
ADD INDEX `pesta_codig` (`pesta_codig` ASC),
ADD INDEX `organ_codig` (`organ_codig` ASC);
;

CREATE TABLE IF NOT EXISTS `rapidpago`.`solicitud_anulacion` (
  `sanul_codig` INT(11) NOT NULL AUTO_INCREMENT,
  `prere_codig` INT(11) NOT NULL,
  `pesta_codig` INT(11) NOT NULL,
  `sanul_obser` TEXT NOT NULL,
  `usuar_codig` INT(11) NOT NULL,
  `sanul_fcrea` DATE NOT NULL,
  `sanul_hcrea` TIME NOT NULL,
  PRIMARY KEY (`sanul_codig`),
  INDEX `solic_codig` (`prere_codig` ASC),
  INDEX `pesta_codig` (`pesta_codig` ASC),
  INDEX `usuar_codig` (`usuar_codig` ASC),
  CONSTRAINT `solicitud_anulacion_ibfk_1`
    FOREIGN KEY (`pesta_codig`)
    REFERENCES `rapidpago`.`p_parametros_estatus` (`pesta_codig`),
  CONSTRAINT `solicitud_anulacion_ibfk_2`
    FOREIGN KEY (`prere_codig`)
    REFERENCES `rapidpago`.`rd_preregistro` (`prere_codig`),
  CONSTRAINT `solicitud_anulacion_ibfk_3`
    FOREIGN KEY (`usuar_codig`)
    REFERENCES `rapidpago`.`seguridad_usuarios` (`usuar_codig`))
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE IF NOT EXISTS `rapidpago`.`solicitud_certificacion_banco` (
  `cbanc_codig` INT(11) NOT NULL AUTO_INCREMENT,
  `solic_codig` INT(11) NOT NULL,
  `banco_codig` INT(11) NOT NULL,
  `cbanc_cafil` INT(11) NOT NULL,
  `cbanc_ncuen` INT(11) NOT NULL,
  `cbanc_termi` TEXT NOT NULL,
  `accio_codig` INT(11) NOT NULL,
  `cbanc_obser` TEXT NOT NULL,
  `usuar_codig` INT(11) NOT NULL,
  `cbanc_fcrea` DATE NOT NULL,
  `cbanc_hcrea` TIME NOT NULL,
  PRIMARY KEY (`cbanc_codig`),
  INDEX `solic_codig` (`solic_codig` ASC),
  INDEX `usuar_codig` (`usuar_codig` ASC),
  INDEX `accio_codig` (`accio_codig` ASC),
  INDEX `banco_codig` (`banco_codig` ASC),
  CONSTRAINT `solicitud_certificacion_banco_ibfk_1`
    FOREIGN KEY (`accio_codig`)
    REFERENCES `rapidpago`.`p_accion` (`accio_codig`),
  CONSTRAINT `solicitud_certificacion_banco_ibfk_2`
    FOREIGN KEY (`banco_codig`)
    REFERENCES `rapidpago`.`p_banco` (`banco_codig`),
  CONSTRAINT `solicitud_certificacion_banco_ibfk_3`
    FOREIGN KEY (`solic_codig`)
    REFERENCES `rapidpago`.`solicitud` (`solic_codig`),
  CONSTRAINT `solicitud_certificacion_banco_ibfk_4`
    FOREIGN KEY (`usuar_codig`)
    REFERENCES `rapidpago`.`seguridad_usuarios` (`usuar_codig`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE IF NOT EXISTS `rapidpago`.`solicitud_confirmacion_banco` (
  `bconf_codig` INT(11) NOT NULL AUTO_INCREMENT,
  `solic_codig` INT(11) NOT NULL,
  `banco_codig` INT(11) NOT NULL,
  `accio_codig` INT(11) NOT NULL,
  `bconf_obser` TEXT NOT NULL,
  `usuar_codig` INT(11) NOT NULL,
  `bconf_fcrea` INT(11) NOT NULL,
  `bconf_hcrea` INT(11) NOT NULL,
  PRIMARY KEY (`bconf_codig`),
  INDEX `solic_codig` (`solic_codig` ASC),
  INDEX `banco_codig` (`banco_codig` ASC),
  INDEX `accio_codig` (`accio_codig` ASC),
  INDEX `usuar_codig` (`usuar_codig` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE IF NOT EXISTS `rapidpago`.`solicitud_eliminacion` (
  `selim_codig` INT(11) NOT NULL AUTO_INCREMENT,
  `solic_codig` INT(11) NOT NULL,
  `pesta_codig` INT(11) NOT NULL,
  `selim_obser` TEXT NOT NULL,
  `usuar_codig` INT(11) NOT NULL,
  `selim_fcrea` DATE NOT NULL,
  `selim_hcrea` TIME NOT NULL,
  PRIMARY KEY (`selim_codig`),
  INDEX `usuar_codig` (`usuar_codig` ASC),
  INDEX `pesta_codig` (`pesta_codig` ASC),
  INDEX `solic_codig` (`solic_codig` ASC),
  CONSTRAINT `solicitud_eliminacion_ibfk_1`
    FOREIGN KEY (`pesta_codig`)
    REFERENCES `rapidpago`.`p_parametros_estatus` (`pesta_codig`),
  CONSTRAINT `solicitud_eliminacion_ibfk_2`
    FOREIGN KEY (`usuar_codig`)
    REFERENCES `rapidpago`.`seguridad_usuarios` (`usuar_codig`),
  CONSTRAINT `solicitud_eliminacion_ibfk_3`
    FOREIGN KEY (`solic_codig`)
    REFERENCES `rapidpago`.`solicitud` (`solic_codig`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8mb4;

ALTER TABLE `rapidpago`.`solicitud_equipos` 
ADD COLUMN `inven_codig` INT(11) NOT NULL AFTER `solic_codig`,
ADD INDEX `inven_codig` (`inven_codig` ASC);
;

CREATE TABLE IF NOT EXISTS `rapidpago`.`solicitud_mensajes` (
  `mensa_codig` INT(11) NOT NULL AUTO_INCREMENT,
  `mensa_asunt` VARCHAR(200) NOT NULL,
  `mensa_cuerp` TEXT NOT NULL,
  `pesta_codig` INT(11) NOT NULL,
  `usuar_codig` INT(11) NOT NULL,
  `mensa_fcrea` DATE NOT NULL,
  `mensa_hcrea` TIME NOT NULL,
  PRIMARY KEY (`mensa_codig`),
  INDEX `usuar_codig` (`usuar_codig` ASC),
  INDEX `pesta_codig` (`pesta_codig` ASC),
  CONSTRAINT `solicitud_mensajes_ibfk_1`
    FOREIGN KEY (`pesta_codig`)
    REFERENCES `rapidpago`.`p_parametros_estatus` (`pesta_codig`),
  CONSTRAINT `solicitud_mensajes_ibfk_2`
    FOREIGN KEY (`usuar_codig`)
    REFERENCES `rapidpago`.`seguridad_usuarios` (`usuar_codig`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_bin;

CREATE TABLE IF NOT EXISTS `rapidpago`.`solicitud_usuario` (
  `solus_codig` INT(11) NOT NULL AUTO_INCREMENT,
  `solic_codig` INT(11) NOT NULL,
  `usuar_codig` INT(11) NOT NULL,
  `solus_ucrea` INT(11) NOT NULL,
  `solus_fcrea` DATE NOT NULL,
  `solus_hcrea` TIME NOT NULL,
  PRIMARY KEY (`solus_codig`),
  INDEX `solic_codig` (`solic_codig` ASC),
  INDEX `usuar_codig` (`usuar_codig` ASC),
  INDEX `solus_ucrea` (`solus_ucrea` ASC),
  CONSTRAINT `solicitud_usuario_ibfk_1`
    FOREIGN KEY (`usuar_codig`)
    REFERENCES `rapidpago`.`seguridad_usuarios` (`usuar_codig`),
  CONSTRAINT `solicitud_usuario_ibfk_2`
    FOREIGN KEY (`solus_ucrea`)
    REFERENCES `rapidpago`.`seguridad_usuarios` (`usuar_codig`),
  CONSTRAINT `solicitud_usuario_ibfk_3`
    FOREIGN KEY (`solic_codig`)
    REFERENCES `rapidpago`.`solicitud` (`solic_codig`))
ENGINE = InnoDB
AUTO_INCREMENT = 40
DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE IF NOT EXISTS `rapidpago`.`solicitud_usuario_notificacion` (
  `sunot_codig` INT(11) NOT NULL AUTO_INCREMENT,
  `enoti_codig` INT(11) NOT NULL,
  `solic_codig` INT(11) NOT NULL,
  `sunot_leido` INT(11) NOT NULL,
  `usuar_codig` INT(11) NOT NULL,
  `sunot_usuar` INT(11) NOT NULL,
  `sunot_fcrea` DATE NOT NULL,
  `sunot_hcrea` TIME NOT NULL,
  PRIMARY KEY (`sunot_codig`),
  INDEX `enoti_codig` (`enoti_codig` ASC),
  INDEX `solic_codig` (`solic_codig` ASC),
  INDEX `usuar_codig` (`usuar_codig` ASC),
  INDEX `sunot_usuar` (`sunot_usuar` ASC),
  CONSTRAINT `solicitud_usuario_notificacion_ibfk_1`
    FOREIGN KEY (`enoti_codig`)
    REFERENCES `rapidpago`.`p_notificacion_estatus` (`enoti_codig`),
  CONSTRAINT `solicitud_usuario_notificacion_ibfk_2`
    FOREIGN KEY (`solic_codig`)
    REFERENCES `rapidpago`.`solicitud` (`solic_codig`),
  CONSTRAINT `solicitud_usuario_notificacion_ibfk_3`
    FOREIGN KEY (`sunot_usuar`)
    REFERENCES `rapidpago`.`seguridad_usuarios` (`usuar_codig`),
  CONSTRAINT `solicitud_usuario_notificacion_ibfk_4`
    FOREIGN KEY (`usuar_codig`)
    REFERENCES `rapidpago`.`seguridad_usuarios` (`usuar_codig`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8mb4;

ALTER TABLE `rapidpago`.`rd_preregistro_configuracion_equipo` 
ADD CONSTRAINT `rd_preregistro_configuracion_equipo_ibfk_2`
  FOREIGN KEY (`inven_codig`)
  REFERENCES `rapidpago`.`inventario` (`inven_codig`);

ALTER TABLE `rapidpago`.`solicitud_equipos` 
ADD CONSTRAINT `solicitud_equipos_ibfk_4`
  FOREIGN KEY (`inven_codig`)
  REFERENCES `rapidpago`.`inventario` (`inven_codig`);


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
